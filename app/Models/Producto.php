<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\ParametroProductos as ParametroProductos;

use DB;
use Carbon\Carbon;

class Producto extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producto';

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function getNumBolsasProducir(){
        

        $parametro = Parametro::findOrFail(1);
        $capacidad_cuartofrio = $parametro->capacidad_cuartofrio;
        $produccion_diaria = $parametro->produccion_diaria;

        $parametro_producto = ParametroProductos::where('producto_id', $this->id)->firstOrFail();
        $capacidad_maxima_producto = $parametro_producto->capacidad;


        $inventario = Inventario::where('producto_id', $this->id)->firstOrFail();
        $inventario_actual = $inventario->cantidad_disponible;

        $total_pedidos = $this->getTotalPedidosPendientesHoy();

        /*

        Calculo Meta del Dia a Producir
        Capacidad Maxima - (Inventario Actual - Pedidos Pendientes HOY)
        
        */

        $producir = $capacidad_maxima_producto - ($inventario_actual-$total_pedidos);

        return $producir;
    }

    public function getTotalPedidosPendientesHoy() {

        $auxDate1 = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $auxDate2 = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addDay())->formatLocalized('%Y-%m-%d');

        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

        $pedidos = DB::table('pedido_productos')
            ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
            ->selectRaw('sum(pedido_productos.cantidad_requerida) as cantidad_requerida')
            ->where([
                        ['pedido.status', '<', 3],
                        ['pedido_productos.producto_id', '=', $this->id]
                    ])
            ->where('pedido.fecha_entrega', '<', $auxDate2)
            //->whereBetween('pedido.fecha_entrega', array($auxDate1, $auxDate2) )
            ->get()->pluck('cantidad_requerida');
        
        return (int) array_sum($pedidos->toArray());
    }

 }
