<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Dashboard extends Model
{
    protected $table = 'cliente_establecimiento';
    use SoftDeletes;

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente')->withTrashed(); 
    }


    /**
     * Get zona.
     */

    public function zona()
    {
        return $this->belongsTo('App\Models\ClienteZona', 'cliente_zona_id', 'id')->withTrashed();
    }

    /**
     * Get forma_pago.
     */
    public function forma_pago()
    {
        return $this->belongsTo('App\Models\FormaPago', 'forma_pago_id', 'id')->withTrashed();
    }

    /**
     * Get condicion_pago.
     */
    public function condicion_pago()
    {
        return $this->belongsTo('App\Models\CondicionPago', 'condicion_pago_id', 'id')->withTrashed();
    }

    /**
     * Get Datos Fiscales.
     */
    public function datos_fiscales()
    {
        return $this->belongsTo('App\Models\ClienteDatosFiscales', 'cliente_datos_fiscales_id', 'id');
    }

    /**
     * Get tipos.
     */

    public function tipos()
    {
        return $this->belongsToMany('App\Models\ClienteTipoEstablecimiento', 'cliente_establecimiento_has_tipo', 'establecimiento_id', 'tipo_est_id')->withTrashed();
    }

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get direccion.
     */
    public function direccion()
    {
        return $this->calle . ' #'. $this->numero_interior . ', ' . $this->colonia. ', ' . $this->municipio;
    }

    public function contactos() {

        return $this->belongsToMany('App\Models\ClienteContacto', 'cliente_contacto_has_establecimiento', 'cliente_est_id', 'cliente_contacto_id');
    }

    /**
     * Get datos pedidos.
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'cliente_establecimiento_id', 'id')->withTrashed();
    }
    /**
     * Get datos cortes.
     */
    public function cortes()
    {
        return $this->hasMany('App\Models\Corte', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get datos conservadores.
     */
    public function conservadores()
    {
        return $this->hasMany('App\Models\Conservador', 'cliente_establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get datos cuartos frios.
     */
    /*
    public function cuartosfrios()
    {
        return $this->hasMany('App\Models\CuartoFrio', 'establecimiento_id', 'id');
    }
    */

    /**
     * Get cuarto frio.
     */
    public function cuartofrio()
    {
        return $this->belongsTo('App\Models\CuartoFrio', 'cuartofrio_id', 'id')->withTrashed();
    }

    public function capacidad_maxima() {

        $capacidad_conservadores = 0;
        if( count($this->conservadores) > 0 ) {
            foreach ($this->conservadores as $conservador) {
                $capacidad_conservadores+= $conservador->modelo->capacidad;
            }
        }

        $capacidad_cuartofrio = 0;
        if( count($this->cuartofrio) > 0 ) {
            foreach ($this->cuartofrio->productos as $producto) {
                $capacidad_cuartofrio+= $producto->capacidad;
            }
        }
    return $capacidad_conservadores + $capacidad_cuartofrio;
    }

    public function productos_disponibles() {

        $productos_array = [];
        if( count($this->conservadores) > 0 ) {
            foreach ($this->conservadores as $conservador) {
                $productos_array[] = $conservador->producto_id;
            }
        }

        if( count($this->cuartofrio) > 0 ) {
            foreach ($this->cuartofrio->productos as $producto) {
                $productos_array[] = $producto->producto_id;
            }
        }
    return $productos_array;
    }

    public function saldo_porcobrar(){

        $porcobrar = 0.00;
        foreach ($this->pedidos as $pedido) {

            if (  ($pedido->status == 'programado' || $pedido->status == 'entregado') &&  !$pedido->es_incobrable  ) {

                $dias_condicion = $pedido->condicion_pago->dias;

                if ($dias_condicion != 0) {
                    //Revisar fecha pedido con fecha actual
                    
                    $fecha_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->fecha_entrega)->formatLocalized('%Y-%m-%d');
                    $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

                    $array_fi = explode('-', $fecha_entrega);
                    $array_ff = explode('-', $now);

                    $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                    $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);

                    
                    $diff = $dt_fi->diffInDays($dt_ff, false);
                    if ($diff > $dias_condicion) {
                        # Ya se pasó la fecha de pago para el pedido
                        $porcobrar+=$pedido->total;
                    }

                } else {
                    $porcobrar+=$pedido->total;
                }

            }
        }

        return number_format($porcobrar, 2, '.', ',');
    }

    public function getCapacidadRecomendada($tipo) {

        $capacidad_total = $this->capacidad_maxima();

        switch ($tipo) {
            case 'minima':
                return round($capacidad_total*.1);
                break;
            
            case 'maxima':
                return $capacidad_total;
                break;

            case 'punto_reorden':
                return round($capacidad_total*.4);
                break;
        }
    }

    public function getColorInventario() {

        switch ($this->status_inventario) {
            case '0':
                return '<i class="fa fa-circle text-danger"></i>';
                break;
            case '1':
                return '<i class="fa fa-circle text-warning"></i>';
                break;
            case '2':
                return '<i class="fa fa-circle" style="color:#1ab368;"></i>';
                break;
            default:
                return '<i class="fa fa-circle"></i>';
                break;
        }
        
    }

    public function ultimo_corte() {

        $ultimo = '';
        foreach ($this->cortes as $corte) {
            $cantidad_actual=$corte->cantidad_actual;
            $ultimo=$corte->created_at;
        }

        if ($ultimo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultimo)->formatLocalized('%d/%m/%Y %H:%M');
            return '<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="'.$cantidad_actual.' bolsas">'.$formatted.'</span>';
        } else {
            return 'Sin registros.';
        }
    }

    public function fecha_ultimo_corte() {

        $ultimo = '';
        foreach ($this->cortes as $corte) {
            $cantidad_actual=$corte->cantidad_actual;
            $ultimo=$corte->created_at;
        }

        if ($ultimo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultimo)->formatLocalized('%d/%m/%Y');
            return $formatted;
        } else {
            return '';
        }
    }

    public function proximo_pedido() {

        $proximo = '';

        $pedidos = Pedido::where('cliente_establecimiento_id',$this->id)->where('status','programado')->where('fecha_entrega','>',Carbon::now())->orderBy('fecha_entrega', 'DESC')->get();

        foreach ($pedidos as $pedido) {
            $proximo=$pedido->fecha_entrega;
            $pedido_id=$pedido->id;
        }

        if ($proximo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $proximo)->formatLocalized('%d/%m/%Y %H:%M');
            $url = url('pedidos/'.$pedido_id);
            return '<a href="'.$url.'">'.$formatted.'</a>';
        } else {
            return 'Sin próximos pedidos.';
        }
    }

 /* OBTENIENDO INFORMACION PORTAL DE CLIENTES*/
    public function clientes_nombre(){
        $nombre = DB::table('cliente_contacto')
        ->join('cliente', 'cliente.id', '=', 'cliente_contacto.cliente_id')
        ->join('cliente_establecimiento', 'cliente_establecimiento.cliente_id', '=', 'cliente.id')
        #->join('cliente_establecimiento', 'cliente_establecimiento.id', '=', 'pedido.cliente_establecimiento_id')
        #->join('cliente', 'cliente.id', '=', 'pedido.cliente_id')
        ->select('*')
        #->where([
                #['cliente_establecimiento.cliente_id', '=', 53]
                #['pedido.status', '!=', 'entregado']
                #,])
            ->get()->toArray();
            //dd($nombre);
            return max($nombre)->nombre;
    }
    public function fecha_pedido(){
        $pedidos = DB::table('pedido')
        #->join('cliente', 'cliente.id', '=', 'cliente_contacto.cliente_id')
        ->join('cliente_establecimiento', 'cliente_establecimiento.id', '=', 'pedido.cliente_establecimiento_id')
        #->join('cliente_establecimiento', 'cliente_establecimiento.id', '=', 'pedido.cliente_establecimiento_id')
        #->join('cliente', 'cliente.id', '=', 'pedido.cliente_id')
        ->select('pedido.fecha_entrega')
        /*->where([
                ['cliente_establecimiento.cliente_id', '=', 53]
               # ['pedido.status', '!=', 'entregado']
                ,])*/
            ->get()->toArray();
            //dd($pedidos);
            return max($pedidos)->fecha_entrega;
    }

        public function corte_establecimiento(){
        $corte = DB::table('cliente_establecimiento')
        #->join('cliente', 'cliente.id', '=', 'cliente_contacto.cliente_id')
        ->join('corte', 'corte.establecimiento_id', '=', 'cliente_establecimiento.id')
        #->join('cliente_establecimiento', 'cliente_establecimiento.id', '=', 'pedido.cliente_establecimiento_id')
        #->join('cliente', 'cliente.id', '=', 'pedido.cliente_id')
        ->select('*')
        /*->where([
                ['cliente_establecimiento.cliente_id', '=', 53]
               # ['pedido.status', '!=', 'entregado']
                ,])*/
            ->get()->toArray();
            //dd($corte);
            return max($corte)->created_at;
    }

public function producto_entregado(){
    $producto_admin = DB::table('cliente_establecimiento')
        ->join('corte', 'cliente_establecimiento.id', '=', 'corte.establecimiento_id')
        ->select('corte.created_at')
        ->where('cliente_establecimiento.cliente_id', '=', 53)

        ->get();
        //dd($producto_admin);
        return $producto_admin;
    }

        
    public function getKilosTotales() {

        return floatval($this->producto->peso)*floatval($this->cantidad_disponible);
        
    }

}



