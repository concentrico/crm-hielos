<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Auth;
use Carbon\Carbon;
use PDF;

use App\Models\PedidoProductos as PedidoProductos;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;

class Pedido extends Model 
{
    use SoftDeletes;

    /**
     * The table associated with the model. 
     *
     * @var string
     */
    protected $table = 'pedido';

    /**
     * Get Cliente.
     */
    public function cliente(){ 
        return $this->belongsTo('App\Models\Cliente')->withTrashed();
    }

    /**
     * Get Establecimiento.
     */

    public function establecimiento(){
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'cliente_establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por(){
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por(){
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    } 
    
    /**
     * Get productos pedido
    **/
    public function productos() {
        //return $this->hasMany('App\Models\PedidoProductos', 'pedido_id', 'id')->withTrashed();
        return $this->hasMany('App\Models\PedidoProductos', 'pedido_id', 'id');
    }

    /**
     * Get pagos pedido
    **/
    public function pagos() {
        return $this->hasMany('App\Models\Pago', 'pedido_id', 'id');
        //return $this->hasMany('App\Models\Pago', 'pedido_id', 'id')->withTrashed();
    }

    /**
     * Get facturas.
     */
    public function facturas()
    {
        return $this->hasMany('App\Models\PedidoFacturas', 'pedido_id', 'id');
        //return $this->hasMany('App\Models\PedidoFacturas', 'pedido_id', 'id')->withTrashed();
    }

    /**
     * Get forma_pago.
    */
    public function forma_pago() {
        return $this->belongsTo('App\Models\FormaPago', 'forma_pago_id', 'id')->withTrashed();
    }

    /**
     * Get condicion_pago.
    */
    public function condicion_pago() {
        return $this->belongsTo('App\Models\CondicionPago', 'condicion_pago_id', 'id')->withTrashed();
    }

    /**
     * Get chofer
    */
    public function chofer() {
        return $this->belongsTo('App\User', 'chofer_id', 'id')->withTrashed();
    }

    /**
     * Get contacto cliente
    */
    public function cliente_firma() {
        return $this->belongsTo('App\Models\ClienteContacto', 'firmado_por', 'id')->withTrashed();
    }

    public function total() {
        /*
        $total = 0.0;
        foreach ($this->productos as $producto) {
            $total+= $producto->subtotal;
        }
        return $total;
        */
        return $this->total;
    }

    public function proxima_entrega() {
        $dia_entrega = $this->dias_entrega;

        //dd(Carbon::create(2017, 4, 15, 1)->dayOfWeek);
        //echo new Carbon('next wednesday');

        // 0: Domingo, 6: Sabado

        if ($dia_entrega == '7') {
            if (Carbon::now()->dayOfWeek == '0') {
                # HOY
                $proxima = Carbon::now()->formatLocalized('%d/%m/%Y');
                return $proxima;
            }
        } else {
            if (Carbon::now()->dayOfWeek == $dia_entrega) {
                # HOY
                $proxima = Carbon::now()->formatLocalized('%d/%m/%Y');
                return $proxima;
            }
        }

        switch ($dia_entrega) {
            case '1':
                # Lunes
                $lunes = new Carbon('next monday'); 
                $proxima = $lunes->formatLocalized('%d/%m/%Y');
                break;
            case '2':
                # Martes
                $martes = new Carbon('next tuesday'); 
                $proxima = $martes->formatLocalized('%d/%m/%Y');
                break;
            case '3':
                # Miércoles
                $miercoles = new Carbon('next wednesday'); 
                $proxima = $miercoles->formatLocalized('%d/%m/%Y');
                break;
            case '4':
                # Jueves
                $jueves = new Carbon('next thursday'); 
                $proxima = $jueves->formatLocalized('%d/%m/%Y');
                break;
            case '5':
                # Viernes
                $viernes = new Carbon('next friday'); 
                $proxima = $viernes->formatLocalized('%d/%m/%Y');
                break;
            case '6':
                # Sábado
                $sabado = new Carbon('next saturday'); 
                $proxima = $sabado->formatLocalized('%d/%m/%Y');
                break;
            case '7':
                # Domingo
                $domingo = new Carbon('next sunday'); 
                $proxima = $domingo->formatLocalized('%d/%m/%Y');
                break;
            default:
                # Cualquier día
                $proxima = Carbon::now()->addDays(1)->formatLocalized('%d/%m/%Y');
                break;
        }

    return $proxima;
    }
    
    public function getTotalBolsas() {
        $total=0;
        foreach ($this->productos as $producto) {
            if ($producto->cantidad_recibida != '' || $producto->cantidad_recibida != null) 
                $total+= $producto->cantidad_recibida;
            else
                $total+= $producto->cantidad_requerida;
        }
        return $total;
    }

    public function proximo_pedido() {

        $proximo = '';

        $pedidos = Pedido::where('cliente_establecimiento_id',$this->id)->where('status','programado')->where('fecha_entrega','>',Carbon::now())->orderBy('fecha_entrega', 'DESC')->get();

        foreach ($pedidos as $pedido) {
            $proximo=$pedido->fecha_entrega;
            $pedido_id=$pedido->id;
        }

        if ($proximo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $proximo)->formatLocalized('%d/%m/%Y %H:%M');
            $url = url('pedidos/'.$pedido_id);
            return '<a href="'.$url.'">'.$formatted.'</a>';
        } else {
            return 'Sin próximos pedidos.';
        }
    }

    /* COBRANZA */
    /**
     * Get status cobranza.
     */
    public function estatus_cobranza() {

        switch ($this->status_cobranza) {
            case '1': // Pendiente
                $aux = '<span class="label label-danger" style="display:inline-block;">Pendiente</span>';
            break;
            case '2': // Pago Parcial
                $aux = '<span class="label label-warning" style="display:inline-block;">Pago Parcial</span>';
            break;
            case '3': // Liquidado
                $aux = '<span class="label label-success" style="display:inline-block;">Liquidado</span>';
            break;
            case '4': // Incobrable
                $aux = '<span class="label label-default" style="display:inline-block;">Incobrable</span>';
            break;
        }
        
        return $aux;
    }

    /**
     * Get status facturacion.
     */
    public function estatus_facturacion() {

        switch ($this->status_facturacion) {
            case 1: // No Facturado
                $aux = '<span class="label label-danger" style="display:inline-block;">No Facturado</span>';
            break;
            case 2: // Facturado Parcialmente
                $aux =  '<span class="label label-warning" style="display:inline-block;">Facturado Parcialmente</span>';
            break;
            case 3: // Facturado
                $aux = '<span class="label label-success" style="display:inline-block;">Facturado</span>';
            break;
            case 4: // Infacturable
                $aux = '<span class="label label-default" style="display:inline-block;">Infacturable</span>';
            break;
            }
        
        return $aux;
    }

    /**
     * Get estatus pendientes.
     */
    public function tiene_pendientes()
    {
        $aux = false;
        if ($this->status == 3 && !$this->incobrable && $this->status_facturacion == 4) {
            # INFACTURABLES
            $pendiente_cobrar = $this->total - $this->pagos->sum('monto');
            if($pendiente_cobrar > 0){
                $aux = true; 
            }

        } else if ( $this->status == 3 && !$this->incobrable ) {
            # SI REQUIERE FACTURA

            $pendiente_facturar = $this->total - $this->facturas->sum('total');
            $pendiente_cobrar = $this->total - $this->pagos->sum('monto');

            
            if($pendiente_facturar > 0 || $pendiente_cobrar > 0){
                $aux = true; 
            }

        }
        
        return $aux;
    }

    /**
     * Get monto por cobrar.
     */
    public function por_cobrar()
    {
        $por_cobrar_pedido=0.00;
        $porcobrar=0.00;

        if ($this->status == 3 && !$this->incobrable && $this->status_facturacion == 4) {
            # Obtener el saldo vencido a partir del total del pedido y pagos.
            
            $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

            //Fecha de Vencimiento en base a la Fecha de Entregado/Recibido + Dias 
            $fecha_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $this->entregado_at);
            $fecha_vencimiento = (clone $fecha_entrega);
            $fecha_vencimiento = $fecha_vencimiento->addDays($this->condicion_pago->dias);
            $fecha2 = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_vencimiento)->formatLocalized('%Y-%m-%d');

            $array_fi = explode('-', $fecha2);
            $array_ff = explode('-', $now);

            $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
            $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
            /* Si hoy > fecha vencimiento*/

            $diff = $dt_fi->diffInDays($dt_ff, false);
            if ($diff > 0) {
                # Ya se pasó la fecha de pago para la factura
                $porcobrar=$this->total;
            }

            if ($porcobrar > 0 ) {
                if ($this->status_cobranza < 3) {
                    # Saldo pendiente
                    $por_cobrar_pedido = $porcobrar - $this->pagos->sum('monto');
                }
            }


        } else if ( $this->status == 3 && !$this->incobrable ) {
            # Obtener el saldo vencido a partir de las facturas del pedido y pagos.

            foreach ($this->facturas as $factura) {
                //Revisar si ya se venció la factura
                
                $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                $fecha_vencimiento = Carbon::createFromFormat('Y-m-d H:i:s', $factura->fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                $array_fi = explode('-', $fecha_vencimiento);
                $array_ff = explode('-', $now);

                $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                /* Si hoy > fecha vencimiento*/

                $diff = $dt_fi->diffInDays($dt_ff, false);
                if ($diff > 0) {
                    # Ya se pasó la fecha de pago para la factura
                    $porcobrar+=$factura->total;
                }

            }

            if ($porcobrar > 0 ) {
                if ($this->status_cobranza < 3) {
                    # Saldo pendiente
                    $por_cobrar_pedido = $porcobrar - $this->pagos->sum('monto');
                }
            }

        }
        
    return $por_cobrar_pedido;
    }

    public function checkFacturasVencidas() {
        $vencidas = false;
        $porcobrar=0.00;

        if ( $this->status == 3 && !$this->incobrable ) {

            foreach ($this->facturas as $factura) {
                //Revisar si ya se venció la factura
                
                $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                $fecha_vencimiento = Carbon::createFromFormat('Y-m-d H:i:s', $factura->fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                $array_fi = explode('-', $fecha_vencimiento);
                $array_ff = explode('-', $now);

                $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                /* Si hoy > fecha vencimiento*/

                $diff = $dt_fi->diffInDays($dt_ff, false);
                if ($diff > 0) {
                    # Ya se pasó la fecha de pago para la factura
                    $porcobrar+=$factura->total;
                }

            }

            if ($porcobrar > 0 ) {
                if ($this->status_cobranza < 3) {
                    # Saldo pendiente
                    $vencidas = true;
                }
            }

        }
        return $vencidas;
    }

    public function updateTotales()
    {
        $total=0.0;
        $subtotal = 0.0;
        $iva = 0.0;
        $total_mas_iva = 0.0;
        $total_iva = 0.0;
        $totales = [];
        $imp = [];
        foreach($this->productos AS $producto){

            if ($producto->cantidad_recibida != '' || $producto->cantidad_recibida != null) {
                # Subtotal en base a lo recibido
                $subtotal = floatval($producto->cantidad_recibida)*floatval($producto->precio_unitario);
                $producto->subtotal = $subtotal;
                $producto->update();
            } else {
                # Subtotal en base a lo requerido
                $subtotal = floatval($producto->cantidad_requerida)*floatval($producto->precio_unitario);
                $producto->subtotal = $subtotal;
                $producto->update();
            }

            $totales[] = $subtotal;
            $total += $subtotal;
        }

        foreach($this->productos AS $producto){
            $producto = Producto::findOrFail($producto->producto_id);
            $iva_producto = $producto->iva;
            $imp[] = $iva_producto;
        }

        for ($i = 0; $i < count($totales); $i++) {
         if( $totales[$i] != '') {
            $mas_iva = $totales[$i]*($imp[$i]);
            $total_iva += $mas_iva;
            $total_mas_iva += $totales[$i]+$mas_iva;
          } else {
            $total_mas_iva += 0.0;
            $total_iva += 0.0;
          }
        }

        $this->subtotal = $total;
        $this->iva = $total_iva;
        $this->total = $total_mas_iva;
        $this->updated_by = Auth::user()->id;
        $this->update();

    }

    /**
     * Get estatus label.
     */
    public function calendar_color(){
        $aux = '';
        $status_colors = ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565'];

        switch($this->status){
            case '1' : 
                $aux = '#1ab394';
                break;
            case '2' : 
                $aux = '#1c84c6';
                break;
            case '3' :
                $aux = '#23c6c8';
                break;
            case '4' :
                $aux = '#f8ac59';
                break;
            case '5' :
                $aux = '#ed5565';
                break;
        }
        return $aux;
    } 

    /** Genera Nota de Remision */

    public function generaRemision(){

        
    }

    /** Enviar Nota de Remision */

    public function enviarRemision(){

        
    }

    public function registrarMovimientoInventario($tipo_movimiento, $movimiento, $item_id) {

        DB::beginTransaction();

        $producto_pedido = PedidoProductos::findOrFail($item_id);
        $producto_id = $producto_pedido->producto_id;
        $inventario = Inventario::where('producto_id', $producto_id)->first();
        //$producto_pedido = PedidoProductos::where('pedido_id', $this->id)->where('producto_id', $producto_id)->first();

        if ($tipo_movimiento == 'entrada') {

            if ($this->status == 2) { //si es de en camino a programado
                $cantidad = $producto_pedido->cantidad_requerida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            } else if ($this->status == 3) { //si es de entregado a devolucion
                $cantidad = $producto_pedido->cantidad_recibida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $inv_entrada = new InventarioEntradas($entrada);
            $inventario->entradas()->save($inv_entrada);

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)+$cantidad;
            $inventario->cantidad_disponible = $cant_nueva;

        } else if($tipo_movimiento == 'salida') {


            if ($this->status == 1) { //si es de programado a en camino
                $cantidad = $producto_pedido->cantidad_requerida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

            } else if ($this->status == 3) { //si es de entregado a devolucion
                $cantidad = $producto_pedido->cantidad_recibida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)-$cantidad;

            if ($cant_nueva > 0) {
                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $cantidad;
                $inv_salida = new InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);
            }

        }

        $inventario->update();
        DB::commit();

    }
}
