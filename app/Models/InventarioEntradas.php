<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventarioEntradas extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = ['id','producto_id', 'cantidad_entrada', 'motivo','created_by', 'updated_by'];
    protected $table = 'inventario_entradas';

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get Inventario.
     */
    public function inventario()
    {
        return $this->belongsTo('App\Models\Inventario');
    }

    /**
     * Get producto
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();
    }

}