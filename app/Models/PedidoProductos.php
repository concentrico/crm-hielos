<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoProductos extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pedido_productos';

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get producto.
    */
    public function producto() {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido');
    }
}
