<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parametro extends Model
{
    protected $table ='parametro';
    
    public $timestamps=false;

    /**
     * Get productos cuarto frio
    **/
    public function productos() {
        return $this->hasMany('App\Models\ParametroProductos', 'parametro_id', 'id');
    }
}