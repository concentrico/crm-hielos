<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogInventario extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_inventario';

    /**
     * Get Entrada.
     */
    public function inventario()
    {
        return $this->belongsTo('App\Models\Inventario', 'inventario_id', 'id')->withTrashed();
    }

    /**
     * Get Entrada.
     */
    public function entrada()
    {
        return $this->belongsTo('App\Models\InventarioEntradas', 'entrada_id', 'id')->withTrashed();
    }


    /**
     * Get Salida.
     */
    public function salida()
    {
        return $this->belongsTo('App\Models\InventarioSalidas', 'salida_id', 'id')->withTrashed();
    }

    /**
     * Get Usuario.
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }

    public function liga($class=""){
        return '<a ' . ($class != "" ? 'class="' . $class . '"' : '') . ' href="' . url('usuarios/'.$this->id) . '">' . $this->nombre . '</a>';
    }
}