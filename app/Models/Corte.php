<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Corte extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = ['establecimiento_id', 'producto_id', 'cantidad_actual', 'created_by', 'updated_by'];
    protected $table = 'corte';


    public function establecimiento() {
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get cuarto frio
     */
    public function cuarto_frio()
    {
        return $this->belongsTo('App\Models\CuartoFrio', 'cuartofrio_id', 'id')->withTrashed();
    }

    /**
     * Get conservador
     */
    public function conservador()
    {
        return $this->belongsTo('App\Models\Conservador', 'conservador_id', 'id')->withTrashed();
    }

    /**
     * Get producto
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }
}
