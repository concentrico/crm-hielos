<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuartoFrioProductos extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $fillable = ['id','cuartofrio_id', 'producto_id', 'precio_venta', 'capacidad', 'created_by', 'updated_by'];
    protected $table = 'establecimiento_cuartofrio_productos';

	/**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    public function cuartofrio()
    {
        return $this->belongsTo('App\Models\CuartoFrio');
    }

    /**
     * Get Producto.
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();        
    }

    /**
     * Get Inventario.
     */
    public function inventario()
    {
        return $this->belongsTo('App\Models\Inventario');
    }
}
