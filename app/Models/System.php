<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;

class System extends Model
{
    protected $table ='configs';
    
    public $timestamps=false;
}