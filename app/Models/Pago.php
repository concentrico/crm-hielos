<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pagos';

    /**
     * Get pedido
     */
    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'pedido_id', 'id')->withTrashed();
    }

    /**
     * Get forma_pago.
     */
    public function forma_pago()
    {
        return $this->belongsTo('App\Models\FormaPago', 'forma_pago_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }
}
