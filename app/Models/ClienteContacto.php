<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteContacto extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente_contacto';

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get Cliente.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente')->withTrashed();
    }

    /**
     * Get funciones.
     */
    public function funciones()
    {
        return $this->belongsToMany('App\Models\ContactoFuncion', 'cliente_contacto_has_funcion', 'cliente_contacto_id', 'cliente_funcion_id');
    }

    /**
     * Get establecimientos.
     */
    public function establecimientos()
    {
        return $this->belongsToMany('App\Models\ClienteEstablecimiento', 'cliente_contacto_has_establecimiento', 'cliente_contacto_id', 'cliente_est_id')->withTrashed();
    }


    /**
     * Get telefonos.
     */
    public function telefonos()
    {
        return $this->hasMany('App\Models\ClienteContactoTelefono', 'cliente_contacto_id', 'id');
    }


    /**
     * Get emails.
     */
    public function emails()
    {
        return $this->hasMany('App\Models\ClienteContactoEmail', 'cliente_contacto_id', 'id');
    }

    /**
     * Get nombre completo.
     */
    public function nombre_completo()
    {
        return $this->nombre . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
    }

    /**
     * Get the user.
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }
}
