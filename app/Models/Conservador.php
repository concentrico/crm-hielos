<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;
use App\Models\LogConservador;

class Conservador extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'conservador';

    /**
     * Get Cliente.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'cliente_id', 'id')->withTrashed();
    }

    /**
     * Get Establecimiento.
     */
    public function establecimiento()
    {
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'cliente_establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get Modelo.
     */
    public function modelo()
    {
        return $this->belongsTo('App\Models\ModeloConservador', 'modelo_id', 'id')->withTrashed();
    }

    /**
     * Get Producto.
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();        
    }

    /**
     * Get logs.
     */
    public function logs()
    {
        return $this->hasMany('App\Models\LogConservador', 'conservador_id', 'id')->orderBy('created_at', 'DESC');
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }   

    public function inventarioDisponible() {

        if($this->capacidad_actual != null)
            $capacidad_actual = $this->capacidad_actual;
        else
            $capacidad_actual = $this->modelo->capacidad;
        $capacidad_maxima = $this->modelo->capacidad;

        return $capacidad_actual.'/'.$capacidad_maxima;
    } 


    public function porcentaje_inventario() {

        if($this->capacidad_actual == null)
            $porcentaje = 100;
        else
            $porcentaje = ($this->capacidad_actual/$this->modelo->capacidad)*100;

        return round($porcentaje,2);
    }

    public function necesita_mtto() {

        if($this->ultimo_mantenimiento != null) 
            $fecha_ultima = $this->ultimo_mantenimiento;
        else
            $fecha_ultima = $this->created_at;

        $ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_ultima)->formatLocalized('%Y-%m-%d');
        $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $diff_dias = diferencia_fechas_dias($ult_mtto, $now);
        
        $frecuencia_mtto = $this->frecuencia_mantenimiento;

        if ($diff_dias >= $frecuencia_mtto)
            return true;
        else
            return false;
        
    }

}
