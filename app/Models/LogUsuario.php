<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUsuario extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_usuarios';

    /**
     * Get Usuario.
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }
}
