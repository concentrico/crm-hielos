<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogConservador extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_conservador';

    /**
     * Get Conservador.
     */
    public function conservador()
    {
        return $this->belongsTo('App\Models\Conservador', 'conservador_id', 'id')->withTrashed();
    }

    /**
     * Get Usuario.
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }
}
