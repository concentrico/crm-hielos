<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClienteDatosBancarios extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente_datos_bancarios';

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get Cliente.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }
}
