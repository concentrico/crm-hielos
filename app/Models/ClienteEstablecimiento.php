<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteEstablecimiento extends Model
{
    protected $table = 'cliente_establecimiento';
    use SoftDeletes;

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente')->withTrashed();
    }

    /**
     * Get zona.
     */

    public function zona()
    {
        return $this->belongsTo('App\Models\ClienteZona', 'cliente_zona_id', 'id')->withTrashed();
    }

    /**
     * Get forma_pago.
     */
    public function forma_pago()
    {
        return $this->belongsTo('App\Models\FormaPago', 'forma_pago_id', 'id')->withTrashed();
    }

    /**
     * Get condicion_pago.
     */
    public function condicion_pago()
    {
        return $this->belongsTo('App\Models\CondicionPago', 'condicion_pago_id', 'id')->withTrashed();
    }

    /**
     * Get Datos Fiscales.
     */
    public function datos_fiscales()
    {
        return $this->belongsTo('App\Models\ClienteDatosFiscales', 'cliente_datos_fiscales_id', 'id');
    }

    /**
     * Get tipos.
     */

    public function tipos()
    {
        return $this->belongsToMany('App\Models\ClienteTipoEstablecimiento', 'cliente_establecimiento_has_tipo', 'establecimiento_id', 'tipo_est_id')->withTrashed();
    }

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get direccion.
     */
    public function direccion()
    {
        return $this->calle . ' #'. $this->numero_interior . ', ' . $this->colonia. ', ' . $this->municipio;
    }

    public function contactos() {

        return $this->belongsToMany('App\Models\ClienteContacto', 'cliente_contacto_has_establecimiento', 'cliente_est_id', 'cliente_contacto_id');
    }

    /**
     * Get datos pedidos.
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'cliente_establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get facturas.
     */
    public function facturas()
    {
        return $this->hasMany('App\Models\PedidoFacturas', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get datos cortes.
     */
    public function cortes()
    {
        return $this->hasMany('App\Models\Corte', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get datos conservadores.
     */
    public function conservadores()
    {
        return $this->hasMany('App\Models\Conservador', 'cliente_establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get cuarto frio.
     */
    public function cuartofrio()
    {
        return $this->belongsTo('App\Models\CuartoFrio', 'cuartofrio_id', 'id');
    }

    /**
     * Get precios establecimiento.
     */
    public function precios()
    {
        return $this->hasMany('App\Models\EstablecimientoPrecios', 'establecimiento_id', 'id');
    }

    public function capacidad_maxima() {

        $capacidad_conservadores = 0;
        if( count($this->conservadores) > 0 ) {
            foreach ($this->conservadores as $conservador) {
                $capacidad_conservadores+= $conservador->modelo->capacidad;
            }
        }

        $capacidad_cuartofrio = 0;
        if( count($this->cuartofrio) > 0 ) {
            foreach ($this->cuartofrio->productos as $producto) {
                $capacidad_cuartofrio+= $producto->capacidad;
            }
        }
    return $capacidad_conservadores + $capacidad_cuartofrio;
    }

    public function saldo(){

        $saldo=0.00;
        foreach ($this->pedidos as $pedido) {
            if ( $pedido->status == 3 && $pedido->status_cobranza < 3 && !$pedido->incobrable ) {
                $saldo += $pedido->total - $pedido->pagos->sum('monto');
            }
        }

        return number_format($saldo, 2, '.', ',');
    }

    public function saldo_porcobrar(){

        $por_cobrar_pedidos=0.00;
        foreach ($this->pedidos as $pedido) {

            $porcobrar=0.00;

            if ($pedido->status == 3 && !$pedido->incobrable && $pedido->status_facturacion == 4) {
                # Obtener el saldo vencido a partir del total del pedido y pagos.
                
                $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

                //Fecha de Vencimiento en base a la Fecha de Entregado/Recibido + Dias 
                $fecha_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at);
                $fecha_vencimiento = (clone $fecha_entrega);
                $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias);
                $fecha2 = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                $array_fi = explode('-', $fecha2);
                $array_ff = explode('-', $now);

                $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                /* Si hoy > fecha vencimiento*/

                $diff = $dt_fi->diffInDays($dt_ff, false);
                if ($diff > 0) {
                    # Ya se pasó la fecha de pago para la factura
                    $porcobrar=$pedido->total;
                }

                if ($porcobrar > 0 ) {
                    if ($pedido->status_cobranza < 3) {
                        # Saldo pendiente
                        $por_cobrar_pedidos += $porcobrar - $pedido->pagos->sum('monto');
                    }
                }


            } else if ( $pedido->status == 3 && !$pedido->incobrable ) {
                # Obtener el saldo vencido a partir de las facturas del pedido y pagos.

                foreach ($pedido->facturas as $factura) {
                    //Revisar si ya se venció la factura
                    
                    $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                    $fecha_vencimiento = Carbon::createFromFormat('Y-m-d H:i:s', $factura->fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                    $array_fi = explode('-', $fecha_vencimiento);
                    $array_ff = explode('-', $now);

                    $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                    $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                    /* Si hoy > fecha vencimiento*/

                    $diff = $dt_fi->diffInDays($dt_ff, false);
                    if ($diff > 0) {
                        # Ya se pasó la fecha de pago para la factura
                        $porcobrar+=$factura->total;
                    }

                }

                if ($porcobrar > 0 ) {
                    if ($pedido->status_cobranza < 3) {
                        # Saldo pendiente
                        $por_cobrar_pedidos += $porcobrar - $pedido->pagos->sum('monto');
                    }
                }

            }

        }

        return number_format($por_cobrar_pedidos, 2, '.', ',');
    }

    public function getCapacidadRecomendada($tipo) {

        $capacidad_total = $this->capacidad_maxima();

        switch ($tipo) {
            case 'minima':
                return round($capacidad_total*.1);
                break;
            
            case 'maxima':
                return $capacidad_total;
                break;

            case 'punto_reorden':
                return round($capacidad_total*.4);
                break;
        }
    }

    public function getColorInventario() {

        switch ($this->status_inventario) {
            case '0':
                return '<i class="fa fa-circle text-danger"></i>';
                break;
            case '1':
                return '<i class="fa fa-circle text-warning"></i>';
                break;
            case '2':
                return '<i class="fa fa-circle" style="color:#1ab368;"></i>';
                break;
            default:
                return '<i class="fa fa-circle"></i>';
                break;
        }
        
    }

    public function porcentaje_inventario(){
        if($this->capacidad_actual == null)
            $porcentaje = 100;
        else
            $porcentaje = ($this->capacidad_actual/$this->capacidad_maxima())*100;

        return round($porcentaje,2);
    }

    public function ultimo_corte() {

        $ultimo = '';
        foreach ($this->cortes->sortBy('created_at') as $corte) {
            $cantidad_actual=$corte->cantidad_actual;
            $ultimo=$corte->created_at;
        }


        if ($ultimo != '') {

            $hour_formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultimo)->formatLocalized('%H:%M');


            $ult_corte = Carbon::createFromFormat('Y-m-d H:i:s', $ultimo)->formatLocalized('%Y-%m-%d');
            $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

            $diff = diferencia_fechas($ult_corte, $now);
            

            if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false )
                $hace='Hoy';
            else
                $hace='Hace '.$diff;


            //dd($ultimo);

#DEJAR SALTO DE LINEA EN TITULO (TOOLTIP BREAKLINE)
            $title = fecha_to_human($ultimo,false).'
'.$cantidad_actual.' bolsas';

            return '<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="'.$title.'">'.$hace.'</span>';

        } else {
            return 'Sin registros.';
        }
    }

    public function fecha_ultimo_corte() {

        $ultimo = '';
        foreach ($this->cortes->sortBy('created_at') as $corte) {
            $cantidad_actual=$corte->cantidad_actual;
            $ultimo=$corte->created_at;
        }

        if ($ultimo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultimo)->formatLocalized('%d/%m/%Y');
            return $formatted;
        } else {
            return '';
        }
    }

    public function proximo_pedido() {

        $proximo = '';
        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

        $pedidos = Pedido::where('cliente_establecimiento_id',$this->id)->where('status','programado')->where('fecha_entrega','>=',$fecha_hoy)->orderBy('fecha_entrega', 'DESC')->get();

        foreach ($pedidos as $pedido) {
            $proximo=$pedido->fecha_entrega;
            $pedido_id=$pedido->id;
        }

        if ($proximo != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $proximo)->formatLocalized('%d/%m/%Y %H:%M');
            $url = url('pedidos/'.$pedido_id);
            return '<a href="'.$url.'">'.$formatted.'</a>';
        } else {
            return 'Sin próximos pedidos.';
        }
    }

    public function productos_disponibles() { 

        $productos_array = [];
        if( count($this->conservadores) > 0 ) {
            foreach ($this->conservadores as $conservador) {
                $productos_array[] = $conservador->producto_id;
            }
        }

        if( count($this->cuartofrio) > 0 ) {
            foreach ($this->cuartofrio->productos as $cf_producto) {
                $productos_array[] = $cf_producto->producto_id;
            }
        }
    return array_unique($productos_array);
    }

    public function check_cantidad_maxima_producto($producto_id) {
        $cantidad_maxima=0;
        if( count($this->conservadores) > 0 ) {
            foreach ($this->conservadores as $conservador) {
                if ($conservador->producto_id == $producto_id)
                    $cantidad_maxima+=$conservador->modelo->capacidad;
            }
        }

        if( count($this->cuartofrio) > 0 ) {
            foreach ($this->cuartofrio->productos as $cf_producto) {
                if ($cf_producto->producto_id == $producto_id)
                    $cantidad_maxima+=$cf_producto->capacidad;
            }
        }
    return $cantidad_maxima;
    }

    public function check_cantidad_sugerida_producto($producto_id){
        $cantidad_maxima=$this->check_cantidad_maxima_producto($producto_id);

        #Obtener último corte del producto
        $ultimo_corte = Corte::where('establecimiento_id', $this->id)->where('producto_id', $producto_id)->orderBy('created_at', 'desc')->first();

        if($ultimo_corte)
            $cantidad_sugerida = intval($cantidad_maxima) - intval($ultimo_corte->cantidad_actual);
        else
            $cantidad_sugerida = intval($cantidad_maxima);

    return $cantidad_sugerida;
    }

    public function proxima_entrega() {
        $dia_entrega = $this->dias_entrega;

        //dd(Carbon::create(2017, 4, 15, 1)->dayOfWeek);
        //echo new Carbon('next wednesday');

        // 0: Domingo, 6: Sabado

        if ($dia_entrega == '7') {
            if (Carbon::now()->dayOfWeek == '0') {
                # HOY
                $proxima = Carbon::now()->formatLocalized('%d/%m/%Y');
                return $proxima;
            }
        } else {
            if (Carbon::now()->dayOfWeek == $dia_entrega) {
                # HOY
                $proxima = Carbon::now()->formatLocalized('%d/%m/%Y');
                return $proxima;
            }
        }

        switch ($dia_entrega) {
            case '1':
                # Lunes
                $lunes = new Carbon('next monday'); 
                $proxima = $lunes->formatLocalized('%d/%m/%Y');
                break;
            case '2':
                # Martes
                $martes = new Carbon('next tuesday'); 
                $proxima = $martes->formatLocalized('%d/%m/%Y');
                break;
            case '3':
                # Miércoles
                $miercoles = new Carbon('next wednesday'); 
                $proxima = $miercoles->formatLocalized('%d/%m/%Y');
                break;
            case '4':
                # Jueves
                $jueves = new Carbon('next thursday'); 
                $proxima = $jueves->formatLocalized('%d/%m/%Y');
                break;
            case '5':
                # Viernes
                $viernes = new Carbon('next friday'); 
                $proxima = $viernes->formatLocalized('%d/%m/%Y');
                break;
            case '6':
                # Sábado
                $sabado = new Carbon('next saturday'); 
                $proxima = $sabado->formatLocalized('%d/%m/%Y');
                break;
            case '7':
                # Domingo
                $domingo = new Carbon('next sunday'); 
                $proxima = $domingo->formatLocalized('%d/%m/%Y');
                break;
            default:
                # Cualquier día
                $proxima = Carbon::now()->addDays(1)->formatLocalized('%d/%m/%Y');
                break;
        }

    return $proxima;
    }

}