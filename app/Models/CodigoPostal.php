<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodigoPostal extends Model
{
    protected $table = 'codigos_postales';
    protected $fillable = ['codigo_postal', 'asentamiento', 'tipo_asentamiento', 'municipio', 'estado'];
}
