<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Cliente extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente';

    /**
     * Get creado por.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get actualizado por.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get establecimientos.
     */
    public function establecimientos()
    {
        return $this->hasMany('App\Models\ClienteEstablecimiento', 'cliente_id', 'id')->withTrashed();
    }

    /**
     * Get contactos.
     */
    public function contactos()
    {
        return $this->hasMany('App\Models\ClienteContacto', 'cliente_id', 'id');
    }

    /**
     * Get datos fiscales.
     */
    public function datos_fiscales()
    {
        return $this->hasMany('App\Models\ClienteDatosFiscales', 'cliente_id', 'id');
    }

    /**
     * Get forma_pago.
     */
    public function forma_pago()
    {
        return $this->belongsTo('App\Models\FormaPago', 'forma_pago_id', 'id')->withTrashed();
    }

    /**
     * Get condicion_pago.
     */
    public function condicion_pago()
    {
        return $this->belongsTo('App\Models\CondicionPago', 'condicion_pago_id', 'id')->withTrashed();
    }

    /**
     * Get precios cliente.
     */
    public function precios()
    {
        return $this->hasMany('App\Models\EstablecimientoPrecios', 'cliente_id', 'id');
    }

    /**
     * Get datos pedidos.
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'cliente_id', 'id')->orderBy('fecha_entrega');
    }

    /**
     * Get facturas.
     */
    public function facturas()
    {
        return $this->hasMany('App\Models\PedidoFacturas', 'cliente_id', 'id')->withTrashed();
    }
    
    public function ultima_entrega() {

        $ultima = '';
        foreach ($this->pedidos as $pedido) {
            if ($pedido->status == 3) {
                $ultima=$pedido->entregado_at;
                $pedido_id=$pedido->id;
            }
        }

        if ($ultima != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultima)->formatLocalized('%d/%m/%Y %H:%M');
            $url = url('pedidos/'.$pedido_id);
            return '<a href="'.$url.'">'.$formatted.'</a>';
        } else {
            return 'Aún no hay pedidos';
        }
    }

    public function saldo(){

        $saldo=0.00;
        foreach ($this->pedidos as $pedido) {
            if ( $pedido->status == 3 && $pedido->status_cobranza < 3 && !$pedido->incobrable ) {
                $saldo += $pedido->total - $pedido->pagos->sum('monto');
            }
        }

        return number_format($saldo, 2, '.', ',');
    }

    public function saldo_porcobrar(){

        $por_cobrar_pedidos=0.00;

        //if($this->id == 17)
            //dd($this->pedidos);

        foreach ($this->pedidos as $pedido) {

            $porcobrar=0.00;

            if ($pedido->status == 3 && !$pedido->incobrable && $pedido->status_facturacion == 4) {
                # Obtener el saldo vencido a partir del total del pedido y pagos.
                
                $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');

                //Fecha de Vencimiento en base a la Fecha de Entregado/Recibido + Dias 
                $fecha_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at);
                $fecha_vencimiento = (clone $fecha_entrega);
                $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias);
                $fecha2 = Carbon::createFromFormat('Y-m-d H:i:s', $fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                $array_fi = explode('-', $fecha2);
                $array_ff = explode('-', $now);

                $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                /* Si hoy > fecha vencimiento*/

                $diff = $dt_fi->diffInDays($dt_ff, false);
                if ($diff > 0) {
                    # Ya se pasó la fecha de pago para la factura
                    $porcobrar=$pedido->total;
                }

                if ($porcobrar > 0 ) {
                    if ($pedido->status_cobranza < 3) {
                        # Saldo pendiente
                        $por_cobrar_pedidos += $porcobrar - $pedido->pagos->sum('monto');
                    }
                }


            } else if ( $pedido->status == 3 && !$pedido->incobrable ) {
                # Obtener el saldo vencido a partir de las facturas del pedido y pagos.

                foreach ($pedido->facturas as $factura) {
                    //Revisar si ya se venció la factura
                    
                    $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                    $fecha_vencimiento = Carbon::createFromFormat('Y-m-d H:i:s', $factura->fecha_vencimiento)->formatLocalized('%Y-%m-%d');

                    $array_fi = explode('-', $fecha_vencimiento);
                    $array_ff = explode('-', $now);

                    $dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
                    $dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);
                    /* Si hoy > fecha vencimiento*/

                    $diff = $dt_fi->diffInDays($dt_ff, false);
                    if ($diff > 0) {
                        # Ya se pasó la fecha de pago para la factura
                        $porcobrar+=$factura->total;
                    }

                }

                if ($porcobrar > 0 ) {
                    if ($pedido->status_cobranza < 3) {
                        # Saldo pendiente
                        $por_cobrar_pedidos += $porcobrar - $pedido->pagos->sum('monto');
                    }
                }

            }

        }

        return number_format($por_cobrar_pedidos, 2, '.', ',');
    }

    public function liga($class=""){
        return '<a ' . ($class != "" ? 'class="' . $class . '"' : '') . ' href="' . url('clientes/'.$this->id) . '">' . $this->nombre_corto . '</a>';
    } 
}
