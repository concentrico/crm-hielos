<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mantenimiento extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mantenimiento';

    /**
     * Get conservador
     */
    public function conservador()
    {
        return $this->belongsTo('App\Models\Conservador', 'conservador_id', 'id')->withTrashed();
    }

    /**
     * Get establecimiento
     */
    public function establecimiento()
    {
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }
}
