<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventario';

    /**
     * Get producto
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();
    }

    /**
     * Get entradas.
     */
    public function entradas()
    {
        return $this->hasMany('App\Models\InventarioEntradas', 'inventario_id', 'id');
    }

    /**
     * Get salidas.
     */
    public function salidas()
    {
        return $this->hasMany('App\Models\InventarioSalidas', 'inventario_id', 'id');
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }



    public function porcentaje_actual() {
        if($this->cantidad_disponible != null) {

            if($this->cantidad_producir != null)
                $porcentaje = ($this->cantidad_disponible/$this->cantidad_producir)*100;
            else
                return 0;
                //$porcentaje = ($this->cantidad_disponible/$this->cantidad_disponible)*100;

            return round($porcentaje,2);
        } else {
            return 0;
        }
        
    } 

    public function inventario_actual() {

        if($this->cantidad_disponible != null)
            return $this->cantidad_disponible.'/'.$this->cantidad_producir;
        else
            return '0/0';
    } 

    public function porcentaje_inventario() {

        $inventario = Inventario::all();
        $peso_productos = [];
        foreach ($inventario as $inventario) {
            $peso_productos[] = floatval($inventario->producto->peso)*floatval($inventario->cantidad_disponible);
        }

        #Obtener Capacidad Total Cuarto Frio
        $parametro = Parametro::find(1);
        $capacidad_maxima = $parametro->capacidad_cuartofrio;

        $porcentaje = (array_sum($peso_productos)/$capacidad_maxima)*100;
        
    return round($porcentaje,2);
    }

    
    public function getKilosTotales() {

        return floatval($this->producto->peso)*floatval($this->cantidad_disponible);
        
    }

}