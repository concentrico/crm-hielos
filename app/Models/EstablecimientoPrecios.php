<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class EstablecimientoPrecios extends Model
{
    protected $table = 'cliente_establecimiento_precios';
    protected $fillable = ['cliente_id','establecimiento_id', 'producto_id', 'precio_venta', 'created_by', 'updated_by'];

    /**
     * Get Cliente.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente')->withTrashed();
    }

    /**
     * Get Establecimiento.
     */
    public function establecimiento()
    {
        return $this->belongsTo('App\Models\ClienteEstablecimiento')->withTrashed();
    }

    /**
     * Get producto
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }
}
