<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuartoFrio extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'establecimiento_cuartofrio';

    /**
     * Get productos (precio venta y capacidad)
     */
    public function productos()
    {
        return $this->hasMany('App\Models\CuartoFrioProductos', 'cuartofrio_id', 'id');
    }

    /**
     * Get Cliente.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'cliente_id', 'id')->withTrashed();
    }

    /**
     * Get Establecimiento.
     */
    public function establecimiento()
    {
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'establecimiento_id', 'id')->withTrashed();
    }

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    public function capacidadTotal() {

        if(count($this->productos) > 0) {
            $capacidades = [0,0];
            foreach ($this->productos as $producto) {
                $capacidades[] = (int)$producto->capacidad;
            }

            $capacidad_total = array_sum($capacidades);
        } else{
            $capacidad_total = 0;
        }
        return $capacidad_total;
    } 
}
