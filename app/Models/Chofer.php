<?php

namespace App\Models;  

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\PedidoHasProductos;
use DB;
use AVG;

class Chofer extends Model
{
    protected $table ='users';
    use SoftDeletes;

    /**
     * Get pedidos
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'chofer_id', 'id')->withTrashed();
    }
    
    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }  
          public function establecimiento()
    {
        //return $this->belongsTo('App\Models\ClienteEstablecimiento', 'cliente_establecimiento_id', 'id')->withTrashed();

        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'cliente_establecimiento_id', 'id'); 
    }



 

        public function producto_nombre()
        {
        $resultado = DB::table('producto')
            ->join('pedido_productos', 'producto.id', '=', 'pedido_productos.producto_id')
            ->select('producto.nombre')
            ->where('pedido_productos.producto_id', '=', $this->id)
            ->get();
            echo $resultado;
            #print_r($resultado);

            
      }


        public function conservador_nombre()
        {
        $resultado_conservador = DB::table('producto')
            ->join('pedido_productos', 'producto.id', '=', 'pedido_productos.producto_id')
            ->select('pedido_productos.conservador_id')
            ->where('pedido_productos.producto_id', '=', $this->id)
            ->get();
            echo $resultado_conservador;

            #dd($resultado_conservador);
      }
              public function cantidad()
        {
           $nombre_cantidad = DB::table('producto')
            ->join('pedido_productos', 'producto.id', '=', 'pedido_productos.producto_id')
            ->select('pedido_productos.cantidad_requerida','producto.nombre')
            ->where('pedido_productos.producto_id', '=', $this->id)
            ->get();
            #print_r($nombre_cantidad);
            echo $nombre_cantidad;
      }

    

 
}