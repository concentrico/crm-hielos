<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoFacturas extends Model
{
    use SoftDeletes;

    protected $table = 'pedido_facturas';

    /**
     * Get the user that created the object.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the object.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    /**
     * Get pedido
     */
    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'pedido_id', 'id')->withTrashed();
    }

    /**
     * Get Cliente.
     */
    public function cliente(){
        //return $this->belongsTo('App\Models\Cliente');
        return $this->belongsTo('App\Models\Cliente', 'cliente_id', 'id')->withTrashed();
    }

    /**
     * Get Establecimiento.
     */
    public function establecimiento(){
        //return $this->belongsTo('App\Models\ClienteEstablecimiento');
        return $this->belongsTo('App\Models\ClienteEstablecimiento', 'establecimiento_id', 'id')->withTrashed();
    }
}
