<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use DB;
use Mail;
use Carbon;
use Password;

class User extends Authenticatable
{   
    use Notifiable;
    use SoftDeletes;

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $usuario = $this;
                
        if($usuario->tipo == 'sistema')
        {

            Mail::send('sistema.auth.emails.password', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Reestablecer Contrase&ntilde;a');
            });         
        }
        else
        {

            Mail::send('clientes.auth.emails.password', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Reestablecer Contrase&ntilde;a');
            });         
        }
    }

    public function sendPasswordActivateNotification($token)
    {
        $usuario = $this;
                
        if($usuario->tipo == 'sistema')
        {
            Mail::send('sistema.auth.emails.activate', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Confirma tu correo electrónico');
            });         
        }
        else
        {
            Mail::send('clientes.auth.emails.activate', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Confirma tu correo electrónico');
            });
        }
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendWelcomeNotification()
    {
        $usuario = $this;
        
        $token = Password::getRepository()->create( $this );
                
        if($usuario->tipo == 'sistema')
        {
            Mail::send('sistema.auth.emails.welcome', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Bienvenido');
            });   
        }
        else
        {
            Mail::send('clientes.auth.emails.welcome', ['usuario' => $usuario, 'token' => $token], function ($m) use ($usuario, $token) {
                $m->to($usuario->email, $usuario->nombre)->subject('Bienvenido');
            });
        }
        // Retrasar correo 10 segundos
        sleep(10);
        $this->sendPasswordActivateNotification($token);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id',  'nombre', 'username', 'email', 'password', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get contacto cliente.
     */
    public function contacto()
    {
        return $this->hasOne('App\Models\ClienteContacto', 'user_id', 'id')->withTrashed();
    }

    /**
     * Get user roles.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\UserRole', 'user_has_role', 'user_id', 'user_role_id');
    }

    /**
     * Get pedidos
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'chofer_id', 'id')->withTrashed();
    }

    /**
     * Get entregas (pedidos)
     */
    public function entregas()
    {
        return $this->hasMany('App\Models\Pedido', 'chofer_id', 'id')->where('entregado_at', '!=', null)->withTrashed();
    }

    /**
     * Get logs.
     */
    public function logs()
    {
        return $this->hasMany('App\Models\LogUsuario', 'user_id', 'id')->orderBy('created_at', 'DESC');
    }

    /**
     * Get the user that created the user.
     */
    public function creado_por()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->withTrashed();
    }

    /**
     * Get the last user that updated the user.
     */
    public function actualizado_por()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id')->withTrashed();
    }

    public function ultima_entrega()
    {
        $ultima = '';
        foreach ($this->entregas as $pedido) {
            if($pedido->entregado_at != null)
                $ultima=$pedido->entregado_at;
        }

        if ($ultima != '') {
            $formatted=Carbon::createFromFormat('Y-m-d H:i:s', $ultima)->formatLocalized('%d/%m/%Y');
            return $formatted;
        } else {
            return 'No Disponible';
        }
    }

    public static function get_usuarios_select($with_trashed = false)
    {
        if($with_trashed){
            $usuarios = User::withTrashed()->get();
        }else{
            $usuarios = User::get();
        }
        
        $usuarios_select;
        foreach($usuarios as $user){
            $usuarios_select[$user->id] = $user->nombre;            
        }
        
        return $usuarios_select;
    }

    public function liga($class=""){
        return '<a ' . ($class != "" ? 'class="' . $class . '"' : '') . ' href="' . url('usuarios/'.$this->id) . '">' . $this->nombre . '</a>';
    }

    /* Portal de Clientes */

    function getAccesoEstablecimientos(){

        $establecimientos = [];
        if (in_array(1, $this->contacto->funciones->pluck('id')->toArray())) {
            # Contacto Comercial - Acceso a todos sus Establecimientos
            $establecimientos = \App\Models\ClienteEstablecimiento::all()->where('cliente_id', $this->contacto->cliente_id)->pluck('id')->toArray();
        } else {
            #Gerente Establecimiento/Almacen- Acceso a sus Establecimientos
            $establecimientos = $this->contacto->establecimientos->pluck('id')->toArray();
        }

        return $establecimientos;
    }

    function getAccesoPedidos() {

        $establecimientos = [];

        if (in_array(1, $this->contacto->funciones->pluck('id')->toArray())) {
            # Contacto Comercial - Acceso a todos los pedidos de sus Establecimientos
            $establecimientos = \App\Models\ClienteEstablecimiento::all()->where('cliente_id', $this->contacto->cliente_id)->pluck('id')->toArray();
        } else if (in_array(2, $this->contacto->funciones->pluck('id')->toArray())) {
            # Gerente Establecimiento - Acceso a sólo los pedidos de los establecimientos a los que tiene acceso
            $establecimientos = $this->contacto->establecimientos->pluck('id')->toArray();
        }

        $pedidos = DB::table('pedido')->whereIn('cliente_establecimiento_id', $establecimientos)->get()->pluck('id')->toArray();
        return $pedidos;
    }
    
}