<?php

function getHeaderLink($string_get, $string_header, $prefix_sort_order = "", $extra_input_array = array(), $href_class = "text-primary", $page_string = "page"){
	
	$link = "";
	
	$href = "";
	$caret = "";
	
	$orderByStr = 'orderBy'.$prefix_sort_order;
	$sortByStr = 'sort'.$prefix_sort_order;
	
	$arr_input = Input::except($page_string, $sortByStr, $orderByStr);
	if( count($extra_input_array) > 0){
		foreach($extra_input_array as $key => $value){
			unset($arr_input[$key]);
		}
	}
	
	if( (Input::get($orderByStr) == $string_get && Input::get($sortByStr) == 'ASC') || Input::get($orderByStr) != $string_get ){
		$href = route(Route::currentRouteName(), array_add(array_add($arr_input, $orderByStr, $string_get), $sortByStr, 'DESC'),false);
		$caret = 'up';
	}else{
		$href = route(Route::currentRouteName(), array_add(array_add($arr_input, $orderByStr, $string_get), $sortByStr, 'ASC'), false);
		$caret = 'down';
	}

	$extra_input_string = "";	
	if( count($extra_input_array) > 0){
		foreach($extra_input_array as $key => $value){
			$extra_input_string = $extra_input_string . '&'.$key.'='.$value;
		}
		
	}
		
	$link = '<a class="'.$href_class.'" href="'.url('/').$href.''.$extra_input_string.'">'.$string_header.'&nbsp;<i class="fa fa-caret-'.$caret.'"></i></a>';	

	return $link;
	
}

function imprime_direccion($direccion){
	
	$direccionStr = "";
	
	// FIRST ROW
	if($direccion->calle){
		$direccionStr = $direccionStr . $direccion->calle;
	}

	if($direccion->numero_exterior){
		$direccionStr = $direccionStr . ' ' . $direccion->numero_exterior;
	}

	if($direccion->numero_interior){
		$direccionStr = $direccionStr . ', ' . $direccion->numero_interior;
	}
	
	if($direccion->calle != null || $direccion->numero_exterior != null || $direccion->numero_interior != null){
		$direccionStr = $direccionStr . '<br />';
	}

	// SECOND ROW
	if($direccion->colonia){
		$direccionStr = $direccionStr . $direccion->colonia;
	}

	if($direccion->codigo_postal){
		$direccionStr = $direccionStr . ' C.P.' . $direccion->codigo_postal;
	}

	if($direccion->colonia != null || $direccion->codigo_postal != null){
		$direccionStr = $direccionStr . '<br />';
	}

	// THIRD ROW
	if($direccion->municipio){
		$direccionStr = $direccionStr . $direccion->municipio;
	}

	if($direccion->municipio != null){
		$direccionStr = $direccionStr . '<br />';
	}

	// FOURTH ROW
	if($direccion->estado){
		$direccionStr = $direccionStr . $direccion->estado;
	}

	if($direccion->pais){
		$direccionStr = $direccionStr . ', ' . $direccion->pais;
	}
	
	return $direccionStr;
}

function direccion_to_human($fecha, $tipo){
	
	$str = "";
	
	$hora = Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->format('h:i A');
	
	switch($tipo){
		case 'longbr' : 
			$str = ucwords(Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%A %d %B %Y')) . '<br />' . $hora; 
			break;
		default : 
			$str = ucwords(Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%A %d %B %Y')) . '<br />' . $hora; 
			break;
	}
	
	return $str;
}


function fecha_to_human($fecha, $hora) {
	//Fecha se recibe en formato Timestamp de mysql Y-m-d H:i:s

	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    if ($hora == true) {
    	$fecha = Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%d')." - ".$meses[Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%m')-1]. " - ".Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%Y'). "  ".Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%r');
    } else {
    	$fecha = Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%d')." - ".$meses[Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%m')-1]. " - ".Carbon::createFromFormat('Y-m-d H:i:s', $fecha)->formatLocalized('%Y');
    }

    return $fecha;
}

function diferencia_fechas($fecha_inicio, $fecha_fin, $horas = false) {
	#Recibe en formato Y-m-d

	$array_fi = explode('-', $fecha_inicio);
	$array_ff = explode('-', $fecha_fin);

	$dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
	$dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);

	if ($dt_fi->diffInDays($dt_ff, false) < 7) {
		# Días
		$diff = $dt_fi->diffInDays($dt_ff, false);
		if ($diff == 1) 
			$tiempo = 'día';
		else
			$tiempo = 'días';
	} else if ( $dt_fi->diffInDays($dt_ff, false) >= 7 && $dt_fi->diffInDays($dt_ff, false) < 30) {
		# Semanas
		$diff = $dt_fi->diffInWeeks($dt_ff, false);
		if ($diff == 1) 
			$tiempo = 'semana';
		else
			$tiempo = 'semanas';
	} else if( $dt_fi->diffInDays($dt_ff, false) >= 30) {
		#Meses
		$diff = $dt_fi->diffInMonths($dt_ff, false);
		if ($diff == 1) 
			$tiempo = 'mes';
		else
			$tiempo = 'meses';
	}

	return $diff.' '.$tiempo;
}

function diferencia_fechas_dias($fecha_inicio, $fecha_fin, $horas = false) {
	#Recibe en formato Y-m-d

	$array_fi = explode('-', $fecha_inicio);
	$array_ff = explode('-', $fecha_fin);

	$dt_fi = Carbon::createFromDate($array_fi[0], $array_fi[1], $array_fi[2]);
	$dt_ff = Carbon::createFromDate($array_ff[0], $array_ff[1], $array_ff[2]);

	$diff = $dt_fi->diffInDays($dt_ff, false);
	return $diff;
}

function getRandomColor(){
    $rand = dechex(rand(0x000000, 0xFFFFFF));
	$color = '#'.$rand;
    return $color;
}

function sanitize_url($str) {
	setlocale(LC_ALL, 'en_US.UTF8');
	$charset='UTF-8'; // or 'ISO-8859-1'
	$str = iconv($charset, 'ASCII//TRANSLIT', $str);
	$new = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
	return $new;
}

function getSystemSettings() {
	$site_settings = App\Models\Parametro::all()->first();
	return $site_settings;
}