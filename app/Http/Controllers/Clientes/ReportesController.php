<?php
namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\ClienteDatosBancarios as DatosBancarios;
use App\Models\ClienteDatosFiscales as DatosFiscales;
use App\Models\ClienteContacto as Contacto;
use App\Models\ClienteContactoEmail as ContactoEmail;
use App\Models\ClienteContactoTelefono as ContactoTelefono;
use App\Models\Conservador as Conservador;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Pedido as Pedido;
use App\Models\Reportes as Reportes;
use App\Models\CuartoFrioProductos as CuartoFrioProductos;
use App\Models\Corte as Corte;

use Excel;
use Auth;
use DB;
use File; 
use Input;
use Validator; 
use Carbon;
use Gate;
 
class ReportesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(){

        if (Gate::denies('manage-compras') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        $data=array();

        $data['meses'] = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $data['mes_actual'] = Carbon::now()->month-1;
        return view('clientes.reportes.index',$data);
    }

    public function generarReporte()
    {
        if (Input::has('fecha_ini') != '' && Input::has('fecha_fin') != '') {
            $fecha_ini = (Input::has('fecha_ini') ? Input::get('fecha_ini') : Carbon::parse('first day of this month')->format('Y-m-d'));
            $fecha_fin = (Input::has('fecha_fin') ? Input::get('fecha_fin') : Carbon::parse('last day of this month')->format('Y-m-d'));
        } else {
            $mes = (int) Input::get('mes_reporte') + 1;
            $fecha_ini = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($mes, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
            $fecha_fin = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($mes, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $fecha_ini)->daysInMonth)->formatLocalized('%Y-%m-%d');
        }

        $fecha_inih = ucwords(Carbon::createFromFormat('Y-m-d', $fecha_ini)->formatLocalized('%d %B, %Y'));
        $fecha_finh = ucwords(Carbon::createFromFormat('Y-m-d', $fecha_fin)->formatLocalized('%d %B, %Y'));

        $contacto = Auth::user()->contacto;
        $cliente_id = $contacto->cliente_id;
        $establecimiento_id = Input::get('cliente_establecimiento');
            
        Excel::create('Pedidos de ' . $fecha_ini . ' a ' . $fecha_fin, function($excel) use ($fecha_ini, $fecha_fin, $fecha_inih, $fecha_finh,$cliente_id,$establecimiento_id) {

            if ($establecimiento_id != '') {
                $pedidos = \App\Models\Pedido::whereRaw('DATE(entregado_at) BETWEEN ? AND ?', ['fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,] )->where('status',3)->where('cliente_id',$cliente_id)->where('cliente_establecimiento_id',$establecimiento_id)->get();

            } else {
                $pedidos = \App\Models\Pedido::whereRaw('DATE(entregado_at) BETWEEN ? AND ?', ['fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,] )->where('status',3)->where('cliente_id',$cliente_id)->get();
            }
                        
            // Set the title
            $excel->setTitle('Pedidos de ' . $fecha_inih . ' a ' . $fecha_finh);

            if ($system->site_name != null) {
                $site_name = $system->site_name;
            } else {
                $site_name = 'CRM Hielos';
            }
        
            // Chain the setters
            $excel->setCreator($site_name)
                  ->setCompany($site_name);
        
            // Call them separately
            $excel->setDescription('Pedidos por período');
            
            $excel->sheet('Pedidos', function($sheet) use ($pedidos, $fecha_inih, $fecha_finh) {

                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');

                $sheet->cells('A1:H1', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(18);
                });
                
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('Reporte General de Pedidos');
                });
                
                $sheet->cell('A2', function($cell) use ($fecha_inih, $fecha_finh) {
                    $cell->setValue('Período del Reporte: ' . $fecha_inih . ' a ' . $fecha_finh);
                });
        
                // ENCABEZADOS
                $sheet->row(3, array(
                     '# Pedido', 'Cliente', 'Establecimiento', 'Producto', 'Precio Unitario', 'Cantidad Entregada', 'Fecha de Entrega', 'Entregado el', 'Total a Pagar', 'Saldo Vencido'
                ));
                
                // FORMATEO ENCABEZADOS
                $sheet->cells('A3:J3', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(12);
                });

                $aux = 4;
                $cont = 3;
                $cont_productos_pedidos = 3;

                // Borde Superior para separar pedidos
                $sheet->cells('A4:J4', function($cells) {
                    // Set all borders (top, right, bottom, left)
                    $cells->setBorder('medium', 'none', 'none', 'none');
                });

                foreach($pedidos as $pedido){

                    $fecha_entrega = fecha_to_human($pedido->fecha_entrega,false);
                    $fecha_entregado = fecha_to_human($pedido->entregado_at,true);

                    $aux2=0;
                    foreach ($pedido->productos as $key => $pedido_producto ) {
                        if ($aux2==0){
                            $sheet->row($aux + $key, array(
                                $pedido->id, 
                                $pedido->cliente->nombre_corto,
                                $pedido->establecimiento->sucursal,
                                $pedido_producto->producto->nombre,
                                number_format($pedido_producto->precio_unitario, 2),
                                $pedido_producto->cantidad_recibida,
                                $fecha_entrega, 
                                $fecha_entregado, 
                                $pedido->total,
                                $pedido->por_cobrar(),
                            ));
                        } else {
                            $sheet->row($aux + $key, array(
                                '',
                                $pedido->cliente->nombre_corto,
                                $pedido->establecimiento->sucursal,
                                $pedido_producto->producto->nombre,
                                number_format($pedido_producto->precio_unitario, 2),
                                $pedido_producto->cantidad_recibida,
                                $fecha_entrega, 
                                $fecha_entregado, 
                                $pedido->total,
                                $pedido->por_cobrar(),
                            ));
                        }

                        $cont_productos_pedidos++;
                        $aux2++;
                    }

                    $sheet->mergeCells('A'.($cont+1).':A'.( $cont + count($pedido->productos)));

                    //Formato al ID del Pedido
                    $sheet->cells('A'.($cont+1).':A'.( $cont + count($pedido->productos)), function($cells) {
                        $cells->setFontSize(12);
                        $cells->setFontWeight('bold');
                        $cells->setValignment('center');
                    });

                    // Borde Inferior para separar pedidos
                    $sheet->cells('A'.( $cont + count($pedido->productos)).':J'.( $cont + count($pedido->productos)), function($cells) {
                        // Set all borders (top, right, bottom, left)
                        $cells->setBorder('none', 'none', 'medium', 'none');
                    });
                    $sheet->cells('A'.($cont+1).':A'.( $cont + count($pedido->productos)), function($cells) {
                        $cells->setBorder('none', 'none', 'medium', 'none');
                    });

                    $aux = $aux + count($pedido->productos);
                    $cont = $cont + count($pedido->productos);
                    
                }

                $sheet->cells('A'. (1 + $cont_productos_pedidos).':J'.(1 + $cont_productos_pedidos), function($cells) {
                    $cells->setAlignment('center');
                    $cells->setBackground('#f3f3f4');
                    $cells->setFontFamily('Calibri');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(14);

                    // Set all borders (top, right, bottom, left)
                    $cells->setBorder('medium', 'medium', 'medium', 'medium');
                    
                });
                
                $sheet->setCellValue('A'.(1 + $cont_productos_pedidos),'TOTALES');
                $sheet->setCellValue('B'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('C'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('D'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('E'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('F'.(1 + $cont_productos_pedidos),'=SUM(F4:F'.($cont_productos_pedidos).')');
                $sheet->setCellValue('G'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('H'.(1 + $cont_productos_pedidos),''); //NADA
                $sheet->setCellValue('I'.(1 + $cont_productos_pedidos),'=SUM(I4:I'.($cont_productos_pedidos).')');
                $sheet->setCellValue('J'.(1 + $cont_productos_pedidos),'=SUM(J4:J'.($cont_productos_pedidos).')');
                

                $sheet->setColumnFormat(array(
                    'I4:I'.(4 + $cont_productos_pedidos) => '0.00'
                ));

                $sheet->setColumnFormat(array(
                    'J4:J'.(4 + $cont_productos_pedidos) => '0.00'
                ));

                $sheet->cells('A2:J'.$cont_productos_pedidos, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontSize(12);
                });

            });
            
        })->export('xls');
    }

}