<?php
namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\ClienteDatosBancarios as DatosBancarios;
use App\Models\ClienteDatosFiscales as DatosFiscales;
use App\Models\ClienteContacto as Contacto;
use App\Models\ClienteContactoEmail as ContactoEmail;
use App\Models\ClienteContactoTelefono as ContactoTelefono;
use App\Models\Conservador as Conservador;
use App\Models\Producto as Producto;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\CuartoFrioProductos as CuartoFrioProductos;
use App\Models\Corte as Corte;
use App\Models\EstablecimientoPrecios as EstablecimientoPrecios;

use Auth;
use DB;
use File; 
use Input;
use Validator;
use Gate;

class EstablecimientosClienteController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {

    	if (Gate::denies('manage-establecimiento') && Gate::denies('manage-compras')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

		$q = Establecimiento::query();

		// Solo a los establecimientos que tiene acceso


		if (in_array(1, Auth::user()->contacto->funciones->pluck('id')->toArray())) {
            # Contacto Comercial
			$q->where('cliente_id','=',Auth::user()->contacto->cliente_id)->get();

        } else if (in_array(2, Auth::user()->contacto->funciones->pluck('id')->toArray())) {
            #Gerente Establecimiento
            $q->whereIn('id',Auth::user()->getAccesoEstablecimientos())->get();
        }
		 
        if (Input::has('f_busqueda'))
        {
            $q->whereRaw("( (sucursal LIKE '%". Input::get('f_busqueda') ."%') OR (SELECT nombre_corto FROM cliente WHERE id=cliente_establecimiento.cliente_id LIKE '%". Input::get('f_busqueda') ."%') )");
        }


        // FILTRO TIPOS
		if (Input::has('f_tipo'))
		{
			$aux_tipos = implode(", ", Input::get('f_tipo'));
			$q->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id IN ('. $aux_tipos .')) > 0)');
		}
		

		// SORT ORDER
		$sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

		// SORT VARIABLE
		$orderByString = "";
		switch(Input::get('orderBy')){
			case 'ID' : $orderByString = 'id '.$sort_aux;			
				break;

			case 'NOMBRE' : $orderByString = 'sucursal '.$sort_aux;			
				break;

			default : $orderByString = 'id '.$sort_aux;				
				break;
		}
		
		if($orderByString != ""){
			$q->orderByRaw($orderByString);
		}
		
		// NUM RESULTADOS
		if (Input::has('f_num_resultados'))
		{

			$num_resultados = 0 + ((int) Input::get('f_num_resultados') );
			$data['establecimientos'] = $q->paginate($num_resultados);
			
			if( count($data['establecimientos']) == 0 ){
				$data['establecimientos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
			}
		}else{
			$data['establecimientos'] = $q->paginate(50);
		}

		return view('clientes.establecimientos.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    	if (Gate::denies('manage-establecimiento') && Gate::denies('manage-compras')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        if (in_array($id, Auth::user()->getAccesoEstablecimientos())) {
        	$establecimiento = Establecimiento::findOrFail($id);
        	return view('clientes.establecimientos.show', ['establecimiento' => $establecimiento]);
        } else {
        	$message = 'No tiene acceso a éste establecimiento';
           	return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
		
    }

    public function realizarCorte(Request $request, $id) {
    	if (!isset($request->cantidad_actual_corte) ) {
    		$corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
    	}

    	$messages = [
		    'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
		];

        $validator = Validator::make($request->all(), [
	        'cantidad_actual_corte.*' 			=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = Establecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
				if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
					$producto = Producto::findOrFail($request->producto_corte[$i]);
					$productos_failed[] = $producto->nombre;
				}
		}

		if (count($productos_failed) > 0) {
			$corteFailed = [
	            'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
	        ];
	        return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
		}

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
        	
        	$cantidad_total=0;
			//INSERT PRODUCTS TO CORTE
			for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$cantidad_total+=intval($request->cantidad_actual_corte[$i]);

				$productos_corte = [
	                'establecimiento_id'    => $id,
	                'producto_id'           => $request->producto_corte[$i],
	                'cantidad_actual'      	=> $request->cantidad_actual_corte[$i],
	                'created_by'            => Auth::user()->id,
	                'updated_by'            => Auth::user()->id,
	            ];
	            $corte = new Corte($productos_corte);
	            $corte->save();
			}

			$establecimiento->capacidad_actual = $cantidad_total;

			if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
	            $establecimiento->status_inventario = 0;
	        } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 1;
	        } else if ($cantidad_total > $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 2;
	        }

			$establecimiento->update();
        } else {
        	$corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
		
		
		DB::commit();

        return redirect()->intended('/establecimientos/'.$id)->with('corte_success', true);
    }

    public function realizarCorte2(Request $request, $id) {
    	if (!isset($request->cantidad_actual_corte) ) {
    		$corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
    	}

    	$messages = [
		    'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
		];

        $validator = Validator::make($request->all(), [
	        'cantidad_actual_corte.*' 			=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = Establecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
				if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
					$producto = Producto::findOrFail($request->producto_corte[$i]);
					$productos_failed[] = $producto->nombre;
				}
		}

		if (count($productos_failed) > 0) {
			$corteFailed = [
	            'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
	        ];
	        return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
		}

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
        	
        	$cantidad_total=0;
			//INSERT PRODUCTS TO CORTE
			for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$cantidad_total+=intval($request->cantidad_actual_corte[$i]);

				$productos_corte = [
	                'establecimiento_id'    => $id,
	                'producto_id'           => $request->producto_corte[$i],
	                'cantidad_actual'      	=> $request->cantidad_actual_corte[$i],
	                'created_by'            => Auth::user()->id,
	                'updated_by'            => Auth::user()->id,
	            ];
	            $corte = new Corte($productos_corte);
	            $corte->save();
			}

			$establecimiento->capacidad_actual = $cantidad_total;

			if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
	            $establecimiento->status_inventario = 0;
	        } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 1;
	        } else if ($cantidad_total > $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 2;
	        }

			$establecimiento->update();
        } else {
        	$corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
		
		
		DB::commit();

        return redirect()->intended('/establecimientos')->with('corte_success', true);
    }

    public function realizarCorte3(Request $request, $id) {
    	if (!isset($request->cantidad_actual_corte) ) {
    		$corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
    	}

    	$messages = [
		    'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
		];

        $validator = Validator::make($request->all(), [
	        'cantidad_actual_corte.*' 			=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = Establecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
				if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
					$producto = Producto::findOrFail($request->producto_corte[$i]);
					$productos_failed[] = $producto->nombre;
				}
		}

		if (count($productos_failed) > 0) {
			$corteFailed = [
	            'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
	        ];
	        return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
		}

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
        	
        	$cantidad_total=0;
			//INSERT PRODUCTS TO CORTE
			for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$cantidad_total+=intval($request->cantidad_actual_corte[$i]);

				$productos_corte = [
	                'establecimiento_id'    => $id,
	                'producto_id'           => $request->producto_corte[$i],
	                'cantidad_actual'      	=> $request->cantidad_actual_corte[$i],
	                'created_by'            => Auth::user()->id,
	                'updated_by'            => Auth::user()->id,
	            ];
	            $corte = new Corte($productos_corte);
	            $corte->save();
			}

			$establecimiento->capacidad_actual = $cantidad_total;

			if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
	            $establecimiento->status_inventario = 0;
	        } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 1;
	        } else if ($cantidad_total > $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 2;
	        }

			$establecimiento->update();
        } else {
        	$corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
		
		
		DB::commit();

        return redirect()->intended('/')->with('corte_success', true);
    }

    public function establecimientos($id)  { 
		    $establecimiento = Establecimiento::withTrashed()->findOrFail($id);
		        return view('clientes.perfil.show3', ['establecimiento' => $establecimiento]);
	}
}
