<?php

namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Conservador as Conservador;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\LogConservador as LogConservador;
use App\Models\Corte as Corte;
use App\Models\Pedido as Pedido;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\Producto as Producto;
use App\Models\Dashboard as Dashboard;
use DB;
use Gate;
use Input;
use Validator;
use Auth;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $contacto = Auth::user()->contacto;

        if (in_array(1, $contacto->funciones->pluck('id')->toArray())) {
            # Contacto Comercial
            $data['pedidos'] = \App\Models\Pedido::all()->where('status', '!=', 5)->where('cliente_id', $contacto->cliente_id);

            $data['pedidos_entregados'] = \App\Models\Pedido::where('status', 3)->where('cliente_id', $contacto->cliente_id)->orderBy('fecha_entrega', 'asc')->get();
        } else if (in_array(2, $contacto->funciones->pluck('id')->toArray())) {
            #Gerente Establecimiento
            $pedidos_array = Auth::user()->getAccesoPedidos();
            $data['pedidos'] = \App\Models\Pedido::all()->where('status', '!=', 5)->whereIn('id', $pedidos_array);

            $data['pedidos_entregados'] = \App\Models\Pedido::where('status', 3)->whereIn('id', $pedidos_array)->orderBy('fecha_entrega', 'asc')->get();
        }

        // YEAR FILTER CONSUMO
        if (Input::has('f_year_consumption'))
            $year_filter_consumption = Input::get('f_year_consumption');
        else
            $year_filter_consumption = date('Y');

        /*Calendario*/
        for ($i=1; $i <= 12; $i++) { 

            $auxDate1 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01');
            $auxDate2 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.$auxDate1->daysInMonth);

            $dia_actual = $auxDate1;
            for ($j=0; $j < $auxDate2->weekOfMonth; $j++) { 
                # Recorrer por cada semana del mes

                $start = $dia_actual->startOfWeek();
                $end = (clone $start)->endOfWeek()->addDay();
                $start_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $start)->formatLocalized('%Y-%m-%d');
                $end_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $end)->formatLocalized('%Y-%m-%d');
                
                $pedidos_array = Auth::user()->getAccesoPedidos();
                $data['entregados'][$i-1][$j] = $this->getTotalSemanal($pedidos_array,$start_formatted,$end_formatted);
                //$data['entregados'][$i-1][$j] = 'Inicia: '.$start_formatted.' - Termina: '.$end_formatted.' TOTAL: '.$this->getTotalSemanal($pedidos_array,$start_formatted,$end_formatted);

                $dia_actual = $end;
                $dia_actual->addDay();
            }

        }

        /* Highcharts */
        $productos_hielo = Producto::where('es_hielo', '=', '1')->get();

        foreach ($productos_hielo as $producto) {
            
            for ($i=1; $i <= 12; $i++) { 

                $auxDate1 = Carbon::createFromFormat('Y-m-d', $year_filter_consumption.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
                $auxDate2 = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $auxDate1)->daysInMonth)->formatLocalized('%Y-%m-%d');

                $establecimientos_array = Auth::user()->getAccesoEstablecimientos();

                if (in_array(1, $contacto->funciones->pluck('id')->toArray())) {
                    # Contacto Comercial
                    $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.cliente_id', '=', $contacto->cliente_id],
                                    //['pedido.cliente_establecimiento_id', '=', $id],
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');
                } else if (in_array(2, $contacto->funciones->pluck('id')->toArray())) {
                    #Gerente Establecimiento
                    $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->whereIn('pedido.cliente_establecimiento_id', $establecimientos_array)
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');
                }
                
                $data['pedidos_entregados_mes'][''.$producto->nombre.''][$i] = (int) array_sum($array_count->toArray());
                $data['establecimientos'] = \App\Models\ClienteEstablecimiento::all()->whereIn('id', $establecimientos_array)->pluck('sucursal')->toArray();
            }

        }

        $anio_first_order = (int) Carbon::createFromFormat('Y-m-d H:i:s', Pedido::first()->entregado_at)->formatLocalized('%Y');
        $anio_current = (int) date('Y');

        if ($anio_current == $anio_first_order) {
            $data['years'] = [$anio_current => $anio_current];
        } else {
            $array_years = array(); 
            for ($i=0; $i <= ($anio_current-$anio_first_order); $i++) { 
                $array_years[($anio_first_order + $i)] = ($anio_first_order + $i);
            }
            $data['years'] = $array_years;
        }

        if (Auth::user()->can('manage-compras') ||  Auth::user()->can('manage-establecimiento') ){
            return view('clientes.dashboard.index', $data);  
        } else {
            return $this->indexAlmacen();
        }

                     
    }

    public function getTotalSemanal($pedidos_array, $start, $end){ 

        $total=0.0;

        $pedidos = collect(DB::table('pedido')->where('status', 3)->whereIn('id', $pedidos_array)->whereBetween('entregado_at', array($start, $end) )->get() ->toArray() )->pluck('total');

        $total = floatval( array_sum($pedidos->toArray()) );
        return $total;
    }

    public function indexAlmacen()
    {
        
        $data = array();
        $establecimientos_array = Auth::user()->getAccesoEstablecimientos();
        $data['establecimientos'] = \App\Models\ClienteEstablecimiento::all()->whereIn('id', $establecimientos_array);

        return view('clientes.dashboard.index_almacenista', $data);
    }


}
