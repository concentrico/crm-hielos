<?php
namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pedido as Pedido;
use App\Models\Pago as Pago;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\Conservador as Conservador;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Producto as Producto;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\LogUsuario as LogUsuario;
use App\Models\LogInventario as LogInventario;
use App\Models\Corte as Corte;
use DB;
use Input;
use Validator;
use Auth;
use Gate;
use File;
use Carbon\Carbon;
use PDF;

class PedidosController extends Controller
{
    public function index() {

        if (Gate::denies('manage-compras') && Gate::denies('manage-establecimiento')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éte módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $contacto = Auth::user()->contacto;

        $q = Pedido::query();


        if (in_array(1, $contacto->funciones->pluck('id')->toArray())) {
            # Contacto Comercial - Acceso a todos los pedidos de sus Establecimientos
            $establecimientos = \App\Models\ClienteEstablecimiento::all()->where('cliente_id', $contacto->cliente_id)->pluck('id')->toArray();
            $q ->where('cliente_id','=', $contacto->cliente_id)->get();


        } else {
            #Acceso a sólo los pedidos de los establecimientos a los que tiene acceso
            $establecimientos = $contacto->establecimientos->pluck('id')->toArray();

            $q->whereIn('cliente_establecimiento_id', $establecimientos)->get()->pluck('id')->toArray();

        }



        // SORT ORDER 
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

        // FILTRO BUSQUEDA
        if (Input::has('f_busqueda'))
        {
            $q->whereRaw("( (SELECT nombre FROM producto WHERE id = inventario.producto_id LIKE '%". Input::get('f_busqueda') ."%') OR (SELECT id_codigo FROM producto WHERE id = inventario.producto_id LIKE '%". Input::get('f_busqueda') ."%') )");
        }

        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'IDCODIGO' : 
                // FILTRO PRODUCTO NOMBRE
                $q->selectRaw('*, (SELECT id_codigo FROM producto WHERE producto_id = producto.id) AS id_codigo');     
                break;

            case 'PRODUCTO' : 
                // FILTRO PRODUCTO NOMBRE
                $q->selectRaw('*, (SELECT nombre FROM producto WHERE producto_id = producto.id) AS nombre');
                break;
            
            case 'STATUS' : $orderByString = 'status '.$sort_aux;
                break;

            case 'CANTPRODUCIR' : $orderByString = 'cantidad_producir '.$sort_aux; 
                break;

            case 'ULTSALIDA' : $orderByString = 'ultima_salida '.$sort_aux;
                break;
   
            default : $orderByString = '-status DESC';   
                break;
        }

        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['pedidos'] = $q->paginate($num_resultados);
            
            if( count($data['pedidos']) == 0 )
                $data['pedidos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['pedidos'] = $q->paginate(25);
        }
        
        return view('clientes.pedidos.index', $data);               
    }

    public function realizarCorte(Request $request, $id) {
        if (!isset($request->cantidad_actual_corte) ) {
            $corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sóo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'cantidad_actual_corte.*'           => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
            
            $cantidad_total=0;
            //INSERT PRODUCTS TO CORTE
            for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $cantidad_total+=intval($request->cantidad_actual_corte[$i]);
                $productos_corte = [
                    'establecimiento_id'    => $id,
                    'producto_id'           => $request->producto_corte[$i],
                    'cantidad_actual'       => $request->cantidad_actual_corte[$i],
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
                $corte = new Corte($productos_corte);
                $corte->save();
            }

            $establecimiento->capacidad_actual = $cantidad_total;

            if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
                $establecimiento->status_inventario = 0;
            } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 1;
            } else if ($cantidad_total > $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 2;
            }

            $establecimiento->update();
        } else {
            $corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mÃ­nimos y de re-orden.',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
        
        
        DB::commit();

        return redirect()->intended('inventario/externo')->with('corte_success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('manage-establecimiento') && Gate::denies('manage-compras')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }


        if (in_array($id, Auth::user()->getAccesoPedidos())) {
            $pedido = Pedido::findOrFail($id);
            return view('clientes.pedidos.show', ['pedido' => $pedido]);
        } else {
            $message = 'No tiene acceso a éste pedido';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        
    }
    
    public function actualizarRemision($id)
    {   
        $pedido = Pedido::findOrFail($id);

        $pdf = PDF::loadView('sistema.layouts.remision', ['pedido' => $pedido]);
        $nombre = "remision".$id.".pdf";
        $archivo="uploads/remisiones/".$nombre;

        $pedido->remision_path = $archivo;
        $pedido->update();
        return $pdf->save($archivo);

    }

    public function generaRemision($id)
    {   
        $pedido = Pedido::findOrFail($id);

        $pdf = PDF::loadView('sistema.layouts.remision', ['pedido' => $pedido]);
        
        $nombre = "remision".$id.".pdf";
        /*$archivo="uploads/remisiones/".$nombre;

        $pedido->remision_path = $archivo;
        $pedido->update();
        return $pdf->save($archivo)->download($nombre);
        */
        return $pdf->download($nombre);

    }
}