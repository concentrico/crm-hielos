<?php
namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User as Usuario;
use App\Models\ClienteContacto as ClienteContacto;
use App\Models\UserRole as UserRole;
use App\Models\Permiso as Permiso;
use App\Models\LogUsuario as LogUsuario;
use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\ClienteDatosBancarios as DatosBancarios;
use App\Models\ClienteDatosFiscales as DatosFiscales;
use App\Models\ClienteContacto as Contacto;
use App\Models\ClienteContactoEmail as ContactoEmail;
use App\Models\ClienteContactoTelefono as ContactoTelefono;
use Carbon\Carbon;

use Auth;
use DB;
use Hash;
use Gate;
use Input;
use Mail;
use Validator;   

class PerfilController extends Controller {

	 public function indexInfoComercial(){
		$data['cliente'] = Cliente::findOrFail(Auth::user()->contacto->cliente_id);
		return view('clientes.comercial.index', $data);
    }

    public function showPerfil(){
		$data['usuario'] = Usuario::findOrFail(Auth::user()->id);
		return view('clientes.perfil', $data);
    }

    public function updatePerfil(Request $request) {
        $usuario = Usuario::findOrFail(Auth::user()->id);

	    $msgs = array(
			'required' => 'Campo requerido.',
			'date_format' => 'La fecha debe tener un formato dd/mm/yyyy'
	    );

	    $validator = Validator::make($request->all(), [
	        'nombre' 	=> 'required',
	        'username' 	=> 'required',
	        'email' 	=> 'required|email|unique:users,email,'.Auth::user()->id,
	        'fecha_nacimiento' => 'date_format:"d/m/Y"',
        ], $msgs);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

	    DB::beginTransaction();

        $usuario->nombre = trim($request->nombre);
        $usuario->email = trim($request->email);
        $usuario->username = trim($request->username);
	    $usuario->fecha_nacimiento = trim($request->fecha_nacimiento);
	    $usuario->telefono = trim($request->telefono);
	    $usuario->celular = trim($request->celular);
        $usuario->updated_by = Auth::user()->id;
		$usuario->update();

		DB::commit();
		
        return redirect()->intended('/perfil')->with('perfil_update_success',true);
    }

}