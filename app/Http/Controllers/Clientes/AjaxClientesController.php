<?php

namespace App\Http\Controllers\Clientes;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;

use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\Producto as Producto;
use App\Models\LogUsuario as LogUsuario;
use App\Models\CodigoPostal as CodigoPostal;
use App\Models\LogInventario as LogInventario;
use App\Models\Pedido as Pedido;
use App\Models\PedidoProductos as PedidoProductos;

use Auth;
use DB;
use Input;
use Validator;
use Carbon\Carbon;
use Mail;

class AjaxClientesController extends Controller
{
	public function ajaxInventario(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
    			# Obtener productos para corte
    			$establecimiento =  Establecimiento::findOrFail($request->establecimiento_id);

    			$productos_array = $establecimiento->productos_disponibles();

    			$response_data = '';
    			foreach($productos_array as $producto_id) {
    				$producto = Producto::findOrFail($producto_id);
					$response_data .= '<tr>
                        <td class="col-xs-2">'.$producto->id_codigo.'<input type="hidden" name="producto_corte[]" value="'.$producto->id.'"/></td>
                        <td class="col-xs-6">'.$producto->nombre.'</td>
                        <td class="col-xs-4"><div class="input-group" style="width: 100%;"><span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span><input class="form-control positive-integer capproducto" onblur="calcularCapacidadCorte(this.value)" name="cantidad_actual_corte[]" type="text"></div></td>
                    </tr>';
				}

				return $response_data;

    			break;
    		}
    	}
    }

    public function ajaxDashboard(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
    				# Modal Detalle Pedidos

    				switch ($request->status) {
    					case '1':
    						# Programados y En Camino
    						$pedido = Pedido::findOrFail($request->pedido_id);
    						switch ($pedido->status) {
		            			case '1':
		            				$status_pedido='<span class="label label-primary" style="font-size: 1em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
		            				break;
		            			case '2':
		            				$status_pedido='<span class="label label-info" style="font-size: 1em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
		            				break;
		            			case '3':
		            				$status_pedido='<span class="label label-success" style="font-size: 1em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
		            				break;
		            			case '4':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
		            				break;
		            			case '5':
		            				$status_pedido='<span class="label label-danger" style="font-size: 1em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
		            				break;
		            			default:
		            				$status_pedido='<span class="label label-default" style="font-size: 1em; display:inline-block;">N.D.</span>';
		            				break;
		            		}


    						$response='';

    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Pedido: </strong>
								    <h3 class="text-info">#'.$pedido->id.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Status:</strong> <BR/>
								    <h3 class="text-info">'.$status_pedido.'</h3>
								</div>
							</div>';


    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Establecimiento: </strong>
								    <h3 class="text-info">'.$pedido->cliente->nombre_corto.'<b>'.$pedido->establecimiento->sucursal.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Fecha Entrega:</strong> <BR/>
								    <h3 class="text-info">'.fecha_to_human($pedido->fecha_entrega,false).'</h3>
								</div>
							</div>';


							$response.='<div class="row m-t-sm">
						        <div class="col-xs-12">

							        <table class="table table-striped" id="tablaCorte">
			                            <thead>
			                            <tr>
			                            	<th class="col-xs-8"><h4 class="text-success">Producto</h4></th>
			                                <th class="col-xs-4"><h4 class="text-success">Cantidad Solicitada</h4></th>
			                            </tr>
			                            </thead>
										<tbody>';

						    foreach ($pedido->productos as $pedido_producto) {
						    	$response.='<tr>
			                              <td class="col-xs-8">
			                                  <h4>'.$pedido_producto->producto->nombre.'</h4>
			                              </td>
			                              <td class="col-xs-4">
			                                  <h4 class="text-center">'.$pedido_producto->cantidad_requerida.' bolsas</h4>
			                              </td></tr>';
						    }
				            		
						    $response.='</tbody></table></div></div>';

						    $response.='<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
								  <strong>Chofer Asignado: </strong>
								    <h3 class="text-info">'.$pedido->chofer->nombre.'</h3>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								  <strong>Zona de Entrega: </strong>
								    <h3 class="text-info">'.$pedido->establecimiento->zona->nombre.'</h3>
								</div>
					      	</div>';

    						return $response;
    						break;

    						case '3':
    						# Entregados
    						$pedido = Pedido::findOrFail($request->pedido_id);
    						switch ($pedido->status) {
		            			case '1':
		            				$status_pedido='<span class="label label-primary" style="font-size: 1em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
		            				break;
		            			case '2':
		            				$status_pedido='<span class="label label-info" style="font-size: 1em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
		            				break;
		            			case '3':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
		            				break;
		            			case '4':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
		            				break;
		            			case '5':
		            				$status_pedido='<span class="label label-danger" style="font-size: 1em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
		            				break;
		            			default:
		            				$status_pedido='<span class="label label-default" style="font-size: 1em; display:inline-block;">N.D.</span>';
		            				break;
		            		}


    						$response='';

    						$response.='<div class="row">
								<div class="col-xs-4">
									<strong>Pedido: </strong>
								    <h3 class="text-info">#'.$pedido->id.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Status:</strong> <BR/>
								    <h3 class="text-info">'.$status_pedido.'</h3>
								</div>

								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								  <strong>Chofer Asignado: </strong>
								    <h3 class="text-info">'.$pedido->chofer->nombre.'</h3>
								</div>
							</div>';


    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Establecimiento: </strong>
								    <h3 class="text-info">'.$pedido->cliente->nombre_corto.'<b>'.$pedido->establecimiento->sucursal.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Hora de Entrega:</strong> <BR/>
								    <h3 class="text-info">'.Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p').'</h3>
								</div>
							</div>';


							$response.='<div class="row m-t-sm">
						        <div class="col-xs-12">

							        <table class="table table-striped" id="tablaCorte">
			                            <thead>
			                            <tr>
			                            	<th class="col-xs-6"><h4 class="text-success">Producto</h4></th>
			                                <th class="col-xs-3"><h4 class="text-success">Solicitado</h4></th>
			                                <th class="col-xs-3"><h4 class="text-success">Entregado</h4></th>
			                            </tr>
			                            </thead>
										<tbody>';

						    foreach ($pedido->productos as $pedido_producto) {
						    	$response.='<tr>
			                              <td class="col-xs-6">
			                                  <h4>'.$pedido_producto->producto->nombre.'</h4>
			                              </td>
			                              <td class="col-xs-3">
			                                  <h4>'.$pedido_producto->cantidad_requerida.' bolsas</h4>
			                              </td>
			                              <td class="col-xs-3">
			                                  <h4 class="text-danger">'.$pedido_producto->cantidad_recibida.' bolsas</h4>
			                              </td>
			                              </tr>';
						    }
				            		
						    $response.='</tbody></table></div></div>';

						    if($pedido->firmado_otro != null)
				                $firmado_por = $pedido->firmado_otro;
				            else
                				$firmado_por = $pedido->cliente_firma->nombre.' '.$pedido->cliente_firma->apellido_paterno.' '.$pedido->cliente_firma->apellido_materno;

						    $response.='<div class="row">

								<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
								  <strong>Firmado por: </strong>
								    <h3 class="text-info">'.$firmado_por.'</h3>
								</div>';

					      	$response.='<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 text-center">
								  <strong>Firma: </strong> <BR/>';
					              if($pedido->firma_path != null)
					                $response.='<img src="'.url($pedido->firma_path).'" />';
					              else
					                $response.='<div class="product-imitation">[ N.D. ]</div>';
					        $response.='</div></div>';

    						return $response;
    						break;
    				}

    			break;

    			case '2':
    				# Enviar Pedidos a Ruta

    				$array_pedidos = $request->pedidos_ids;
    				for ($i=0; $i < count($array_pedidos); $i++) { 
    					$pedido = Pedido::findOrFail($array_pedidos[$i]);

    					$pedido->status = 2;

    					#TBD - Envío notificacion por e-mail al cliente

				        $pedido->update();
    				}

    			break;

    			case '3':
    				# Cargar Choferes Pedido
					$pedido = Pedido::findOrFail($request->pedido_id);
					$choferes = DB::table('users')
                        ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
                        ->where('user_has_role.user_role_id', '=', 4)
                        ->where('users.deleted_at', NULL)
                        ->get();

                    $choferes_array = $choferes->pluck('nombre', 'id')->toArray();

					$response='';
					$response.='<div class="row">
						<div class="col-xs-2">
							<strong>Pedido: </strong>
						    <h3 class="text-info">#'.$pedido->id.'</b></h3>
						</div>

						<div class="col-xs-5">
						    <strong>Establecimiento:</strong> <BR/>
							<h3 class="text-info">'.$pedido->cliente->nombre_corto.' <b>'.$pedido->establecimiento->sucursal.'</b></h3>
						</div>

						<div class="col-xs-5">
						    <strong>Seleccione Chofer:</strong> <BR/>';
						    $response.='
						    <input type="hidden" id="pedido_id_cambiar_chofer" name="pedido_id_cambiar_chofer" value="'.$pedido->id.'">
							<select name="chofer_cambiar" id="chofer_cambiar" class="form-control selectdp">';

							foreach ($choferes_array as $key => $value) {
								if($key == $pedido->chofer_id)
									$response.='<option selected="" value="'.$key.'">'.$value.'</option>';
								else
									$response.='<option value="'.$key.'">'.$value.'</option>';
							}

						$response.='</select></div>
					</div>';

					return $response;
    			break;

    			case '4':
    				# Cambiar Chofer Pedido
					$pedido = Pedido::findOrFail($request->pedido_id);
					
					$pedido->chofer_id = $request->chofer_id;
			        $pedido->update();
    			break;
    		}
    	}
    }
}
