<?php
namespace App\Http\Controllers\Sistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Conservador as Conservador;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\LogConservador as LogConservador;
use DB;
use Input;
use Validator;
use Auth;
use Gate;
use Carbon\Carbon;

class ConservadorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index() {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $q = Conservador::query();

        // FILTRO BUSQUEDA
        if (Input::has('f_busqueda')){
            $q->join('modelo_conservador', 'conservador.modelo_id', 'modelo_conservador.id')->select('conservador.*','modelo_conservador.modelo as nombre_modelo')->where('modelo_conservador.modelo', 'like', '%'. Input::get('f_busqueda') .'%')->orWhere('numero_serie', 'like', '%'. Input::get('f_busqueda') .'%')->get();
        }

        // FILTRO ESTATUS
        if (Input::has('f_estatus') && Input::get('f_estatus') == 'todos')
        {
            $q->withTrashed();

            $data['total_conservadores'] = Conservador::all()->count();
        }else {
            switch (Input::get('f_estatus')) {
                case 'stand_by':
                    $q->whereRaw("status ='". Input::get('f_estatus') ."'");
                    break;
                case 'en_consignacion':
                    $q->whereRaw("status ='". Input::get('f_estatus') ."'");
                    break;
                case 'en_reparacion':
                    $q->whereRaw("status ='". Input::get('f_estatus') ."'");
                    break;
                case 'en_mantenimiento':
                    $q->whereRaw("status ='". Input::get('f_estatus') ."'");
                    break;
                case 'fuera_servicio':
                    $q->whereRaw("status ='". Input::get('f_estatus') ."'");
                    break;
                case 'deleted':
                    $q->onlyTrashed();
                    break;
            }

            $data['total_conservadores'] = $q->count();

            $establecimientos_array = DB::table('conservador')->where('cliente_establecimiento_id','!=',null)->select('cliente_establecimiento_id')->distinct()->get()->pluck('cliente_establecimiento_id')->toArray();

            $est_mas_conservadores = collect();
            
            foreach ($establecimientos_array as $key => $value) {
                $q2 = (clone $q);
                $q2->where('status', Input::get('f_estatus'));
                $q2->where('cliente_establecimiento_id',$value);
                $est_mas_conservadores[] = ['est_id' => $value , 'total' => $q2->count()];
            }

            $sorted = $est_mas_conservadores->sortByDesc('total');

            $q3 = (clone $q);
            $q3->where('status', Input::get('f_estatus'));
            $conservadores = $q3->get();
            $sum_mtto=0;
            foreach ($conservadores as $conservador) {
                if($conservador->necesita_mtto())
                    $sum_mtto++;
            }

            $data['establecimiento_mas_conservadores'] = trim(ClienteEstablecimiento::find($sorted->first()['est_id'])->sucursal).' ('.$sorted->first()['total'].')';
            $data['conservadores_mantenimiento'] = $sum_mtto;
        }

        // FILTRO CLIENTE
        if (Input::has('f_cliente')){
            $q->whereRaw("cliente_id ='". Input::get('f_cliente') ."'");

            $establecimientos_array = DB::table('conservador')->where('cliente_id',Input::get('f_cliente'))->where('cliente_establecimiento_id','!=',null)->select('cliente_establecimiento_id')->distinct()->get()->pluck('cliente_establecimiento_id')->toArray();

            $est_mas_conservadores = collect();
            
            foreach ($establecimientos_array as $key => $value) {
                $q2 = Conservador::query();
                $q2->where('status','en_consignacion');
                $q2->where('cliente_establecimiento_id',$value);
                $est_mas_conservadores[] = ['est_id' => $value , 'total' => $q2->count()];
            }

            $sorted = $est_mas_conservadores->sortByDesc('total');

            $q3 = Conservador::query();
            $q3->where('status','en_consignacion');
            $q3->where('cliente_id',Input::get('f_cliente'));
            $conservadores = $q3->get();
            $sum_mtto=0;
            foreach ($conservadores as $conservador) {
                if($conservador->necesita_mtto())
                    $sum_mtto++;
            }

            $data['total_conservadores'] = Conservador::where('cliente_id',Input::get('f_cliente'))->count();

            if (count($est_mas_conservadores) > 0) {
                $data['establecimiento_mas_conservadores'] = trim(ClienteEstablecimiento::find($sorted->first()['est_id'])->sucursal).' ('.$sorted->first()['total'].')';
            } else {
                $data['establecimiento_mas_conservadores'] = 'N.D.';
            }
            


            $data['conservadores_mantenimiento'] = $sum_mtto;
            
        }


        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
        
        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'STATUS' : 
                if ($sort_aux == 'DESC') 
                    $orderByString = 'status DESC';
                else
                    $orderByString = '-status ASC';  
                break;

            case 'NUMSERIE' : $orderByString = 'numero_serie '.$sort_aux;  
                break;

            case 'FRECMTTO' : $orderByString = 'frecuencia_mantenimiento '.$sort_aux;  
                break;

            case 'ESTABLECIMIENTO' : 
                $orderByString = '(SELECT sucursal FROM cliente_establecimiento WHERE cliente_establecimiento_id = cliente_establecimiento.id) '.$sort_aux;
                break;

            case 'MODELO' : 
                $orderByString = '(SELECT modelo FROM modelo_conservador WHERE modelo_id = modelo_conservador.id) '.$sort_aux;
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux;            
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE conservador.created_by = users.id) '.$sort_aux;    
                break;

            case 'UPDATEDAT' : $orderByString = 'updated_at '.$sort_aux;            
                break;

            case 'UPDATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE conservador.updated_by = users.id) '.$sort_aux;
                break;

            default : $orderByString = '-status ASC';           
                break;
        }
        
        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['conservadores'] = $q->paginate($num_resultados);
            
            if( count($data['conservadores']) == 0 )
                $data['conservadores'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['conservadores'] = $q->paginate(50);
        }



        // NO FILTROS
        if ( !Input::has('f_cliente') && ( !Input::has('f_estatus') || Input::get('f_estatus') == 'todos' ) && (!Input::has('f_nombre') || Input::has('f_nombre') == '') )
        {
            $establecimientos_array = DB::table('conservador')->where('cliente_establecimiento_id','!=',null)->select('cliente_establecimiento_id')->distinct()->get()->pluck('cliente_establecimiento_id')->toArray();

            $est_mas_conservadores = collect();

            foreach ($establecimientos_array as $key => $value) {
                $q2 = Conservador::query();
                $q2->where('status','en_consignacion');
                $q2->where('cliente_establecimiento_id',$value);

                $est_mas_conservadores[] = ['est_id' => $value , 'total' => $q2->count()];
            }

            $sorted = $est_mas_conservadores->sortByDesc('total');

            $q3 = Conservador::query();
            $q3->where('status','en_consignacion');
            $conservadores = $q3->get();
            $sum_mtto=0;
            foreach ($conservadores as $conservador) {
                if($conservador->necesita_mtto())
                    $sum_mtto++;
            }

            $data['total_conservadores'] = $q->count();
            $data['establecimiento_mas_conservadores'] = trim(ClienteEstablecimiento::find($sorted->first()['est_id'])->sucursal).' ('.$sorted->first()['total'].')';
            $data['conservadores_mantenimiento'] = $sum_mtto;
        }
        

        return view('sistema.conservadores.index', $data);               
    }


    public function store(Request $request) {
        $messages = [
            'required' => 'Campo requerido.',
            'id_codigo.unique' => 'Éste código ya está en uso.',
            'numero_serie.unique' => 'El número de serie ya ha sido registrado.',
        ];

        $validator = Validator::make($request->all(), [
            'id_codigo'             => 'required|numeric|unique:conservador,id_codigo',
            'modelo'                => 'required',
            'numero_serie'          => 'required|unique:conservador,numero_serie',
            'nombre_producto'       => '',
            'producto_id'           => 'required|exists:producto,id',
            //'precio_venta'          => 'numeric',
            'frecuencia'            => 'required|numeric',
            'comentarios'           => '',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_conservador_create', true);
        }

        DB::beginTransaction();

        $conservador = new Conservador;
        $conservador->id_codigo = $request->input('id_codigo');
        $conservador->modelo_id = $request->input('modelo');
        $conservador->numero_serie = trim($request->input('numero_serie'));
        $conservador->producto_id = $request->input('producto_id');
        $conservador->status = 'stand_by';
        //$conservador->precio_venta = $request->input('precio_venta');
        $conservador->frecuencia_mantenimiento = $request->input('frecuencia');
        $conservador->comentarios = $request->input('comentarios');
        $conservador->created_by = Auth::user()->id;
        $conservador->updated_by = Auth::user()->id;
        
        $conservador->save();
        
        DB::commit();
        
        return redirect()->intended('/conservadores/'.$conservador->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        
        $conservador = Conservador::findOrFail($id);

        return view('sistema.conservadores.show', ['conservador' => $conservador]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'id_codigo_edit.unique' => 'Éste código ya está en uso.',
            'numero_serie_edit.unique' => 'El número de serie ya ha sido registrado.',
        ];

        $validator = Validator::make($request->all(), [
            'id_codigo_edit'            => 'required|numeric|unique:conservador,id_codigo,'.$id,
            'numero_serie_edit'    => 'required|unique:conservador,numero_serie,'.$id,
            'producto_id'               => 'required|exists:producto,id',
            'frecuencia_edit'           => 'required|numeric',
            'comentarios_edit'          => '',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_conservador_edit', true);
        }

        DB::beginTransaction();
        $conservador =  Conservador::findOrFail($id);
        $conservador->id_codigo = $request->input('id_codigo_edit');
        $conservador->modelo_id = $request->input('modelo_edit');
        $conservador->numero_serie = trim($request->input('numero_serie_edit'));
        $conservador->producto_id = $request->input('producto_id');
        $conservador->frecuencia_mantenimiento = $request->input('frecuencia_edit');
        $conservador->comentarios = $request->input('comentarios_edit');
        $conservador->created_by = Auth::user()->id;
        $conservador->updated_by = Auth::user()->id;
        $conservador->update();
        
        DB::commit();
        
        return redirect()->intended('/conservadores/'.$conservador->id);
    }

    public function consignarEstablecimiento(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), ['conservador_cliente'  => 'required', 'conservador_establecimiento'  => 'required'], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_consignar', true);
        }

        DB::beginTransaction();
        $conservador =  Conservador::findOrFail($id);
        $conservador->cliente_id = $request->conservador_cliente;
        $conservador->cliente_establecimiento_id = $request->conservador_establecimiento;
        $conservador->status = 'en_consignacion';
        $conservador->update();

        #Actualizar Puntos de inventario
        $establecimiento = ClienteEstablecimiento::findOrFail($request->conservador_establecimiento);
        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
        $establecimiento->update();
        DB::commit();

        return redirect()->intended('/conservadores/'.$id)->with('consignacion_success', true);
    }

    public function registrarMtto(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'date_format' => 'La fecha debe tener un formato dd/mm/yyyy',
        ];

        $validator = Validator::make($request->all(), ['fecha_mtto'  => 'required|date_format:"d/m/Y"'], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registrar_mtto', true);
        }

        DB::beginTransaction();
        $conservador =  Conservador::findOrFail($id);
        $ult_mtto = Carbon::createFromFormat('d/m/Y', $request->input('fecha_mtto'))->formatLocalized('%Y-%m-%d');
        $conservador->ultimo_mantenimiento = $ult_mtto;

        $log_conservador = new LogConservador;
        $log_conservador->conservador_id = $id;
        $log_conservador->user_id = Auth::user()->id;
        $log_conservador->tipo = 'mantenimiento';
        $log_conservador->comentarios = 'Se registró Mantenimiento a Conservador.';

        $conservador->update();
        $log_conservador->save();
        
        DB::commit();
        return redirect()->intended('/conservadores/'.$conservador->id);
    }

    public function cambiarStatus(Request $request, $id) {

        DB::beginTransaction();
        $conservador =  Conservador::findOrFail($id);
        
        switch ($conservador->status) {
            case 'stand_by':
            $status_actual = 'Stand By';
                break;

            case 'en_reparacion':
            $status_actual = 'En Reparación';
                break;

            case 'en_consignacion':
            $status_actual = 'En Consignación';
                break;

            case 'fuera_servicio':
            $status_actual = 'Fuera de Servicio';
                break;
        }


        $status_nuevo='';
        switch ($request->status_change) {
            case 'stand_by':
                $status_nuevo = 'STAND BY';

                if ($conservador->cliente_establecimiento_id != null && $conservador->cliente_id != null) {
                    # Aún está consignado a un establecimiento (REPARACION)
                    $conservador->status = 'en_consignacion';

                } else {
                    # No tenía ninguna relación (FUERA DE SERVICIO)
                    $conservador->status = 'stand_by';
                }
                $conservador->update();

                break;

            case 'en_reparacion':
                $status_nuevo = 'EN REPARACIÓN';
                $conservador->status = 'en_reparacion';
                $conservador->update();
                break;

            case 'fuera_servicio':
                $status_nuevo = 'FUERA DE SERVICIO';

                //Desconsignar o Retirar Conservador

                $establecimiento_id = $conservador->cliente_establecimiento_id;
                $producto_id = $conservador->producto_id;

                $conservador->status = 'fuera_servicio';
                $conservador->cliente_id = NULL;
                $conservador->cliente_establecimiento_id = NULL;
                $conservador->update();

                $establecimiento = ClienteEstablecimiento::findOrFail($establecimiento_id);
                $productos_asociados = collect($establecimiento->productos_disponibles());

                if( !$productos_asociados->contains($producto_id) ) {
                    #Borrar
                    DB::table('cliente_establecimiento_precios')->where([
                        ['establecimiento_id', $establecimiento_id],
                        ['producto_id', $producto_id]
                    ])->delete();
                }

                #Actualizar Puntos de inventario
                $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
                $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
                $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
                $establecimiento->update();

                break;
        }

        $log_conservador = new LogConservador;
        $log_conservador->conservador_id = $id;
        $log_conservador->user_id = Auth::user()->id;
        $log_conservador->tipo = 'status';
        $log_conservador->comentarios = 'Se cambió status de <i>'.$status_actual.'</i>'.' a <b>'.$status_nuevo.'</b>';
        $log_conservador->save();
        
        DB::commit();
        return redirect()->intended('/conservadores/'.$conservador->id)->with('change_status_success', true);
    }

    public function updateAcuerdosComerciales(Request $request, $id, $est_id) {
        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se aceptan numeros en el Precio de Venta.',
        ];

        $validator = Validator::make($request->all(), ['producto_id'  => 'required', 'precio_venta'  => 'required|numeric'], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        DB::beginTransaction();
        $conservador =  Conservador::findOrFail($id);
        $conservador->producto_id = $request->producto_id;
        $conservador->precio_venta = $request->precio_venta;
        $conservador->update();
        DB::commit();

        return redirect()->intended('/conservadores/'.$id);

    }

    public function destroy($id) {

        DB::beginTransaction();
        $conservador = Conservador::findOrFail($id);
        $conservador->status = 'fuera_servicio';
        $conservador->cliente_id = NULL;
        $conservador->cliente_establecimiento_id = NULL;

        $log_conservador = new LogConservador;
        $log_conservador->conservador_id = $id;
        $log_conservador->user_id = Auth::user()->id;
        $log_conservador->tipo = 'status';
        $log_conservador->comentarios = 'Se eliminó el Conservador';

        #Actualizar Puntos de inventario
        $establecimiento = ClienteEstablecimiento::find($conservador->cliente_establecimiento_id);
        if($establecimiento) {
            $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
            $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
            $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
            $establecimiento->update();
        }
        
        $conservador->update();
        $conservador->delete();
        $log_conservador->save();
        DB::commit();

        return redirect()->intended('/conservadores');
    }

    public function reactivar($id)
    {

        DB::beginTransaction();
        $conservador = Conservador::withTrashed()->findOrFail($id);
        
        $conservador->status = 'stand_by';
        $conservador->cliente_id = NULL;
        $conservador->cliente_establecimiento_id = NULL;
        $log_conservador = new LogConservador;
        $log_conservador->conservador_id = $id;
        $log_conservador->user_id = Auth::user()->id;
        $log_conservador->tipo = 'status';
        $log_conservador->comentarios = 'Se re-activó el conservador.';

        $conservador->update();
        $conservador->restore();
        $log_conservador->save();

        DB::commit();
        
        return redirect()->intended('/conservadores/'.$id);
    }

 }