<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Datatables;
//use Yajra\Datatables\Datatables;
use App\User;
use App\Models\Cliente as Cliente;

class DatatablesController extends Controller {
	
	public function getUsers() 
	{
	    return Datatables::of(User::query())->make(true);
	}

	public function getClientes() {

		/*$data_clientes = array();
        $data_clientes[] = array('id' => '1', 'name' => 'Pedro Salazar Villa', 'username' => 'psalazar', 'email' => 'psalazar@gmail.com', 'created_at' => '2017-01-23 14:30:13' , 'updated_at' => '2017-01-23 14:30:13' );

        return response()->json($data_clientes);*/
        
		$query = DB::table('clientes')
            ->join('cliente_contacto', 'users.id', '=', 'contacts.user_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.id','users.nombre_corto', 'cliente_contacto.nombre', 'orders.price')
            ->get();

        return Datatables::of($query)->make(true);
		
		foreach ($clientes as $cliente) {
			# code...
		}

	    //return Datatables::of(Cliente::query())->make(true);
	}
}
