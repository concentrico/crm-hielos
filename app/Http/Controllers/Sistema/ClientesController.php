<?php
namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Cliente as Cliente;
use App\Models\Producto as Producto;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\ClienteDatosBancarios as DatosBancarios;
use App\Models\ClienteDatosFiscales as DatosFiscales;
use App\Models\ClienteContacto as Contacto;
use App\Models\ClienteContactoEmail as ContactoEmail;
use App\Models\ClienteContactoTelefono as ContactoTelefono;
use App\Models\EstablecimientoPrecios;
use App\User as Usuario;


use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

use Auth;
use DB;
use File;
use Input;
use Validator;
use Gate;
use Hash;
use Mail;
use Carbon\Carbon;

class ClientesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

    	if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

    	$q = Cliente::query();

		// FILTRO NOMBRE
		if (Input::has('f_nombre'))
		{
			$q->whereRaw("nombre_corto LIKE '%". Input::get('f_nombre') ."%'");

			$clientes_aux = $q->get();
			$aux_num_est1 = 0;
            foreach ($clientes_aux as $cliente) {
                $aux_num_est1 += count($cliente->establecimientos);
            }

			$data['total_clientes'] = $q->count();
        	$data['total_establecimientos'] = $aux_num_est1;
		}

		// SORT ORDER
		$sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
		
		// SORT VARIABLE
		$orderByString = "";
		switch(Input::get('orderBy')){
			case 'ID' : $orderByString = 'id '.$sort_aux;			
				break;

			case 'NOMBRE' : $orderByString = 'nombre_corto '.$sort_aux;
				break;

			case 'TIPO' : $orderByString = 'tipo '.$sort_aux;			
				break;

			case 'PEDIDOS' : $orderByString = 'nombre_corto '.$sort_aux;			
				break;

			case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux;            
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE cliente.created_by = users.id) '.$sort_aux;
                break;

            case 'UPDATEDAT' : $orderByString = 'updated_at '.$sort_aux;            
                break;

            case 'UPDATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE cliente.updated_by = users.id) '.$sort_aux;
                break;

			case 'PORCOBRAR' : $orderByString = '(SELECT SUM(total) FROM pedidos_orden_compra WHERE cliente_id = cliente.id) '.$sort_aux;			
				break;

			default : $orderByString = 'nombre_corto '.$sort_aux;			
				break;
		}
		
		if($orderByString != ""){
			$q->orderByRaw($orderByString);
		}

		// FILTRO ESTATUS
		if (Input::has('f_estatus') && Input::get('f_estatus') == 'todos')
		{
			$q->withTrashed();

			$clientes_aux = $q->get();
			$aux_num_est2 = 0;
            foreach ($clientes_aux as $cliente) {
                $aux_num_est2 += count($cliente->establecimientos);
            }

			$data['total_clientes'] = $q->count();
        	$data['total_establecimientos'] = $aux_num_est2;

		}else if(Input::has('f_estatus') && Input::get('f_estatus') == 'suspendido'){
			$q->onlyTrashed();

			$clientes_aux = $q->get();
			$aux_num_est3 = 0;
            foreach ($clientes_aux as $cliente) {
                $aux_num_est3 += count($cliente->establecimientos);
            }

			$data['total_clientes'] = $q->count();
        	$data['total_establecimientos'] = $aux_num_est3;
		}

		// FILTRO COBRANZA
		if (Input::has('f_estatus_cobranza') && Input::get('f_estatus_cobranza') == 'vencido')
		{
			
			$clientes_aux = $q->get();
			$clientes_pdte_array = [];
			$aux_num_est4 = 0;
            foreach ($clientes_aux as $cliente) {
                if($cliente->saldo_porcobrar() > 0.00) {
                    $clientes_pdte_array[] = $cliente->id;
                    $aux_num_est4 += count($cliente->establecimientos);
                }
            }

            $q->whereIn('id', $clientes_pdte_array);

            $data['total_clientes'] = count($clientes_pdte_array);
        	$data['total_establecimientos'] = $aux_num_est4;
		}
		
		// NUM RESULTADOS
		if (Input::has('f_num_resultados'))
		{

			$num_resultados = 0 + ((int) Input::get('f_num_resultados') );
			$data['clientes'] = $q->paginate($num_resultados);
			
			if( count($data['clientes']) == 0 ){
				$data['clientes'] = $q->paginate($num_resultados, ['*'], 'page', 1);
			}
		}else{
			$data['clientes'] = $q->paginate(25);
		}

		// NO FILTROS
		if ( ( !Input::has('f_estatus_cobranza') || Input::get('f_estatus_cobranza') != 'vencido' || Input::get('f_estatus_cobranza') == 'todos' ) && (!Input::has('f_estatus') || Input::get('f_estatus') == 'todos' || Input::get('f_estatus') == 'activo' ) && (!Input::has('f_nombre') || Input::has('f_nombre') == '') )
		{
			$data['total_clientes'] = Cliente::all()->count();
			$data['total_establecimientos'] = ClienteEstablecimiento::all()->count();
		}

		return view('sistema.clientes.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
	    
		$messages = [
		    'required' => 'Campo requerido.',
		];
	    
        $validator = Validator::make($request->all(), [
	        'nombre_corto' 		=> 'required',
	        'tipo' 				=> 'required',
	        'comentarios' 		=> '',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_create', true);
        }
	    
	    DB::beginTransaction();
        $cliente = new Cliente;
		
        $cliente->nombre_corto = $request->nombre_corto;
		$cliente->es_preferencial = (isset($request->es_preferencial)) ? 1 : 0;
		$cliente->tipo = $request->tipo;
        $cliente->comentarios = $request->comentarios;
		$cliente->created_by = Auth::user()->id;
		$cliente->updated_by = Auth::user()->id;
		$cliente->save();

	    DB::commit();

		$return_id = $cliente->id;
		
        return redirect()->intended('/clientes/'.$return_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeContacto(Request $request, $id)
    {
	    
	    DB::beginTransaction();
        $cliente =  Cliente::findOrFail($id);

		$messages = [
		    'required' => 'Campo requerido.',
		    'email' => 'Formato inválido.',
		];

		if ($cliente->tipo == 'horeca') {
			$validator = Validator::make($request->all(), [
		        'contacto_nombre' 			=> 'required',
		        'contacto_apellido_paterno' => 'required',
		        'contacto_establecimientos'	=> 'required|exists:cliente_establecimiento,id',
		        'tipo_email.*' 				=> 'required',
		        'email.*' 					=> 'required|email',
		        'tipo_telefono.*' 			=> 'required',
		        'telefono.*' 				=> 'required',
		        'telefono_ext.*' 			=> '',
		        'contacto_comentarios' 		=> '',
	        ], $messages);
		} else {
			$validator = Validator::make($request->all(), [
		        'contacto_nombre' 			=> 'required',
		        'contacto_apellido_paterno' => 'required',
		        'tipo_email.*' 				=> 'required',
		        'email.*' 					=> 'required|email',
		        'tipo_telefono.*' 			=> 'required',
		        'telefono.*' 				=> 'required',
		        'telefono_ext.*' 			=> '',
		        'contacto_comentarios' 		=> '',
	        ], $messages);
		}
		

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_contacto_create', true);
        }

        $arr_desc_email = array();
		$arr_email = array();
		$arr_desc_telefono = array();
		$arr_telefono = array();
		$arr_telefono_ext = array();
		
		if($request->tipo_email){
			$arr_desc_email = $request->tipo_email;
		}

		if($request->email){
			$arr_email = $request->email;
		}
		if($request->tipo_telefono){
			$arr_desc_telefono = $request->tipo_telefono;
		}
		if($request->telefono){
			$arr_telefono = $request->telefono;
		}
		if($request->telefono_ext){
			$arr_telefono_ext = $request->telefono_ext;
		}

        $contacto = new Contacto;
        
        $contacto->cliente_id = $cliente->id;
        $contacto->nombre = trim($request->contacto_nombre);
		$contacto->apellido_paterno = trim($request->contacto_apellido_paterno);
		$contacto->apellido_materno = trim($request->contacto_apellido_materno);
		$contacto->comentarios = trim($request->contacto_comentarios);
		$contacto->created_by = Auth::user()->id;
		$contacto->updated_by = Auth::user()->id;

		$contacto->save();

		if(count($request->contacto_establecimientos) > 0){
			foreach($request->contacto_establecimientos as $establecimiento){
				$contacto->establecimientos()->attach($establecimiento, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
			}
		}
		
        foreach($arr_desc_email AS $key => $value){
	        $correo = new ContactoEmail;
	        $correo->valor = $arr_email[$key]; 
	        $correo->tipo = $value;
	        $correo->created_by = Auth::user()->id;
	        $correo->updated_by = Auth::user()->id;
		    $correo = $contacto->emails()->save($correo);
        }
        
        foreach($arr_desc_telefono AS $key => $value){
	        $telefono = new ContactoTelefono;
	        $telefono->valor = $arr_telefono[$key]; 
	        $telefono->tipo = $value;
	        $telefono->extension = $arr_telefono_ext[$key];
	        $telefono->created_by = Auth::user()->id;
	        $telefono->updated_by = Auth::user()->id;
	        $telefono = $contacto->telefonos()->save($telefono);
        }
        		
		DB::commit();

        return redirect()->intended('/clientes/'.$cliente->id)->with('show_tab_contactos', true);
    }

    public function createFromContact($data) {

        DB::beginTransaction();

        $usuario = new Usuario;
        $contacto = Contacto::findOrFail($data['contacto_id']);
		
		$full_name = trim($contacto->nombre).' '.trim($contacto->apellido_paterno).' '.trim($contacto->apellido_materno);
        $usuario->nombre = trim($full_name);
        $usuario->tipo = 'cliente';
        //$usuario->email = $data['email'];
        $usuario->username = $data['username'];
        $usuario->password = Hash::make($data['password']);
        //$usuario->password = Hash::make(Hash::make(rand(0,100)));
        $usuario->created_by = Auth::user()->id;
        $usuario->updated_by = Auth::user()->id;

		$usuario->save();

		$contacto->user_id = $usuario->id;
		$contacto->update();

		$usuario->roles()->attach($data['rol'], ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);

		//$usuario->sendWelcomeNotification();
		DB::commit();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDatosFiscales(Request $request, $id)
    {
		$messages = [
		    'required' 	=> 'Campo requerido.',
		    'rfc' 		=> 'Revisa el formato del RFC',
		];

        $validator = Validator::make($request->all(), [
	        'tipo_persona' 			=> 'required',
	        'rfc'		 			=> 'required|rfc',
	        'razon_social' 			=> 'required',
	        'representante_legal' 	=> 'required_if:tipo_persona,moral',
	        'df_calle' 				=> '',
	        'df_no_exterior' 		=> '',
	        'df_no_interior' 		=> '',
	        'df_colonia' 			=> '',
	        'df_codigo_postal' 		=> 'numeric',
	        'df_municipio' 			=> '',
	        'df_estado' 			=> '',
        ], $messages);
        		
        if ($validator->fails()) {
	        
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_datos_fiscales_create', true);
        }
	    
	    DB::beginTransaction();

        $cliente =  Cliente::findOrFail($id);
		$datos_fiscales = new DatosFiscales;
		
		$datos_fiscales->cliente_id = $id;
		$datos_fiscales->persona = $request->tipo_persona;
		$datos_fiscales->rfc = trim($request->rfc);
		$datos_fiscales->razon_social = strtoupper(trim($request->razon_social));
		$datos_fiscales->representante_legal = ucfirst(trim($request->representante_legal));
		$datos_fiscales->calle = ucfirst(trim($request->df_calle));
		$datos_fiscales->numero_interior = trim($request->df_no_interior);
		$datos_fiscales->numero_exterior = trim($request->df_no_exterior);
		$datos_fiscales->colonia = ucfirst(trim($request->df_colonia));
		$datos_fiscales->codigo_postal = trim($request->df_codigo_postal);
		$datos_fiscales->municipio = ucfirst(trim($request->df_municipio));
		$datos_fiscales->estado = ucfirst(trim($request->df_estado));
		$datos_fiscales->created_by = Auth::user()->id;
		$datos_fiscales->updated_by = Auth::user()->id;
		$datos_fiscales->save();		
		
		DB::commit();

		$return_id = $cliente->id;

        return redirect()->intended('/clientes/'.$return_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        
		$cliente = Cliente::withTrashed()->findOrFail($id);

		$data['cliente'] = $cliente;

		$data['pedidos'] = \App\Models\Pedido::all()->where('status', '!=', 5)->where('cliente_id', $id);

        $data['pedidos_entregados'] = \App\Models\Pedido::where('status', 3)->where('cliente_id', $id)->orderBy('fecha_entrega', 'asc')->get();

		/*Calendario*/
        for ($i=1; $i <= 12; $i++) { 

            $auxDate1 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01');
            $auxDate2 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.$auxDate1->daysInMonth);

            $dia_actual = $auxDate1;
            for ($j=0; $j < $auxDate2->weekOfMonth; $j++) { 
                # Recorrer por cada semana del mes

                $start = $dia_actual->startOfWeek();
                $end = (clone $start)->endOfWeek()->addDay();
                $start_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $start)->formatLocalized('%Y-%m-%d');
                $end_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $end)->formatLocalized('%Y-%m-%d');
                

                $establecimientos = \App\Models\ClienteEstablecimiento::all()->where('cliente_id', $id)->pluck('id')->toArray();
        		$pedidos_array = DB::table('pedido')->whereIn('cliente_establecimiento_id', $establecimientos)->get()->pluck('id')->toArray();

                $data['entregados'][$i-1][$j] = $this->getTotalSemanal($pedidos_array,$start_formatted,$end_formatted);

                $dia_actual = $end;
                $dia_actual->addDay();
            }

        }

        /* Highcharts */
        $productos_hielo = Producto::where('es_hielo', '=', '1')->get();

        foreach ($productos_hielo as $producto) {
            
            for ($i=1; $i <= 12; $i++) { 


                $auxDate1 = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
                $auxDate2 = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $auxDate1)->daysInMonth)->formatLocalized('%Y-%m-%d');

                $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.cliente_id', '=', $id],
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');

                $data['pedidos_entregados_mes'][''.$producto->nombre.''][$i] = (int) array_sum($array_count->toArray());
            }

        }

		return view('sistema.clientes.show', $data);
    }

    public function getTotalSemanal($pedidos_array, $start, $end){ 

        $total=0.0;

        $pedidos = collect(DB::table('pedido')->where('status', 3)->whereIn('id', $pedidos_array)->whereBetween('entregado_at', array($start, $end) )->get() ->toArray() )->pluck('total');

        $total = floatval( array_sum($pedidos->toArray()) );
        return $total;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    
		$messages = [
		    'required' => 'Campo requerido.',
		    'email' => 'Formato inválido.',
		];
	    
        $validator = Validator::make($request->all(), [
	        'nombre_corto' 			=> 'required',
	        'tipo' 					=> 'required',
	        'comentarios' 			=> '',
	        'forma_pago' 			=> 'exists:forma_pago,id',
	        'condicion_pago'		=> 'exists:condicion_pago,id',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_edit', true);
        }
	    
	    DB::beginTransaction();
        $cliente =  Cliente::findOrFail($id);
		
        $cliente->nombre_corto = $request->nombre_corto;
        $cliente->es_preferencial = (isset($request->es_preferencial)) ? 1 : 0;
		$cliente->tipo = $request->tipo;
        $cliente->comentarios = $request->comentarios;
		$cliente->updated_by = Auth::user()->id;
		$cliente->update();

	    DB::commit();

        return redirect()->intended('/clientes/'.$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateContacto(Request $request, $cliente_id, $contacto_id)
    {
    	DB::beginTransaction();
        $cliente =  Cliente::findOrFail($cliente_id);

		$messages = [
		    'required' => 'Campo requerido.',
		    'email' => 'Formato inválido.',
		];

		if ($cliente->tipo == 'horeca') {
			$validator = Validator::make($request->all(), [
		        'contacto_nombre_edit' 				=> 'required',
		        'contacto_apellido_paterno_edit' 	=> 'required',
		        'contacto_establecimientos_edit'	=> 'required|exists:cliente_establecimiento,id',
		        'tipo_email_edit.*' 				=> 'required',
		        'email_edit.*' 						=> 'required|email',
		        'email_edit_id.*' 					=> 'required',
		        'tipo_telefono_edit.*' 				=> 'required',
		        'telefono_edit.*' 					=> 'required',
		        'telefono_edit_id.*'	 			=> 'required',
		        'contacto_comentarios_edit' 		=> '',
	        ], $messages);
		} else {
			$validator = Validator::make($request->all(), [
		        'contacto_nombre_edit' 				=> 'required',
		        'contacto_apellido_paterno_edit' 	=> 'required',
		        'tipo_email_edit.*' 				=> 'required',
		        'email_edit.*' 						=> 'required|email',
		        'email_edit_id.*' 					=> 'required',
		        'tipo_telefono_edit.*' 				=> 'required',
		        'telefono_edit.*' 					=> 'required',
		        'telefono_edit_id.*'	 			=> 'required',
		        'contacto_comentarios_edit' 		=> '',
	        ], $messages);
		}

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_contacto_edit', true)->with('contacto_id', $contacto_id);
        }
        
        $contacto = Contacto::findOrFail($contacto_id);
        $contacto->nombre = trim($request->contacto_nombre_edit);
		$contacto->apellido_paterno = trim($request->contacto_apellido_paterno_edit);
		$contacto->apellido_materno = trim($request->contacto_apellido_materno_edit);
		$contacto->comentarios = trim($request->contacto_comentarios_edit);
		$contacto->update();
		
		if ($cliente->tipo == 'horeca') {
			foreach($request->contacto_establecimientos_edit as $establecimiento){
				if(count($request->contacto_establecimientos_edit) > 1){
					$contacto->establecimientos()->syncWithoutDetaching([$establecimiento => ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]]);
				} else {
					$contacto->establecimientos()->sync([$establecimiento => ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]]);
				}
			}
		}
		
		$arr_desc_email = array();
		$arr_email = array();
		$arr_email_id = array();
		$arr_desc_telefono = array();
		$arr_telefono = array();
		$arr_telefono_id = array();
		
		if($request->tipo_email_edit){
			$arr_desc_email = $request->tipo_email_edit;
		}
		if($request->email_edit){
			$arr_email = $request->email_edit;
		}
		if($request->email_edit_id){
			$arr_email_id = $request->email_edit_id;
		}
		if($request->tipo_telefono_edit){
			$arr_desc_telefono = $request->tipo_telefono_edit;
		}
		if($request->telefono_edit){
			$arr_telefono = $request->telefono_edit;
		}
		if($request->telefono_ext_edit){
			$arr_telefono_ext = $request->telefono_ext_edit;
		}
		if($request->telefono_edit_id){
			$arr_telefono_id = $request->telefono_edit_id;
		}

		foreach($contacto->emails as $correo){
			if(!in_array($correo->id, $arr_email_id)){
				$correo->delete();
			}
		}

		foreach($contacto->telefonos as $telefono){
			if(!in_array($telefono->id, $arr_telefono_id)){
				$telefono->delete();
			}
		}
		
        foreach($arr_desc_email AS $key => $value){
	        if(isset($arr_email_id[$key])){
		        $correo = ContactoEmail::findOrFail($arr_email_id[$key]);
		        $correo->valor = $arr_email[$key]; 
		        $correo->tipo = $value;
		        $correo->updated_by = Auth::user()->id;
				$correo->update();
	        }else{
		        $correo = new ContactoEmail;
		        $correo->valor = $arr_email[$key]; 
		        $correo->tipo = $value;
		        $correo->created_by = Auth::user()->id;
		        $correo->updated_by = Auth::user()->id;
		        $correo = $contacto->emails()->save($correo);
	        }
        }
        
        foreach($arr_desc_telefono AS $key => $value){
	        if(isset($arr_telefono_id[$key])){
		        $telefono = ContactoTelefono::findOrFail($arr_telefono_id[$key]);
		        $telefono->valor = $arr_telefono[$key]; 
		        $telefono->tipo = $value;
		        $telefono->updated_by = Auth::user()->id;
				$telefono->update();
	        }else{
		        $telefono = new ContactoTelefono;
		        $telefono->valor = $arr_telefono[$key]; 
		        $telefono->tipo = $value;
		        $telefono->extension = $arr_telefono_ext[$key];
		        $telefono->created_by = Auth::user()->id;
		        $telefono->updated_by = Auth::user()->id;
		        $telefono = $contacto->telefonos()->save($telefono);
	        }
        }
				
		DB::commit();

        return redirect()->intended('/clientes/'.$cliente_id)->with('show_tab_contactos', true);
    }

    public function updateDatosFiscales(Request $request, $id, $datosf_id)
    {
		$messages = [
		    'required' 	=> 'Campo requerido.',
		    'rfc' 		=> 'Revisa el formato del RFC',
		];

        $validator = Validator::make($request->all(), [
	        'tipo_persona_edit' 		=> 'required',
	        'rfc_edit'		 			=> 'required|rfc',
	        'razon_social_edit' 		=> 'required',
	        'representante_legal_edit' 	=> 'required_if:tipo_persona_edit,moral',
	        'df_calle_edit' 			=> '',
	        'df_no_exterior_edit' 		=> '',
	        'df_no_interior_edit' 		=> '',
	        'df_colonia_edit' 			=> '',
	        'df_codigo_postal_edit' 	=> 'numeric',
	        'df_municipio_edit' 		=> '',
	        'df_estado_edit' 			=> '',
        ], $messages);
        		
        if ($validator->fails()) {
	        
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_datos_fiscales_edit', true)->with('datosf_id', $datosf_id);
        }
	    
	    DB::beginTransaction();

        $cliente =  Cliente::findOrFail($id);
		$datos_fiscales = DatosFiscales::findOrFail($datosf_id);
		
		$datos_fiscales->persona = $request->tipo_persona_edit;
		$datos_fiscales->rfc = trim($request->rfc_edit);
		$datos_fiscales->razon_social = strtoupper(trim($request->razon_social_edit));
		$datos_fiscales->representante_legal = ucfirst(trim($request->representante_legal_edit));
		$datos_fiscales->calle = ucfirst(trim($request->df_calle_edit));
		$datos_fiscales->numero_interior = trim($request->df_no_interior_edit);
		$datos_fiscales->numero_exterior = trim($request->df_no_exterior_edit);
		$datos_fiscales->colonia = ucfirst(trim($request->df_colonia_edit));
		$datos_fiscales->codigo_postal = trim($request->df_codigo_postal_edit);
		$datos_fiscales->municipio = ucfirst(trim($request->df_municipio_edit));
		$datos_fiscales->estado = ucfirst(trim($request->df_estado_edit));
		$datos_fiscales->updated_by = Auth::user()->id;
		$datos_fiscales->update();		
		
		DB::commit();

		$return_id = $cliente->id;

        return redirect()->intended('/clientes/'.$return_id);
    }

    public function updateAcuerdosComerciales(Request $request, $id) {

    	$messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
	        'forma_pago' 		=> 'required|exists:forma_pago,id',
	        'condicion_pago'	=> 'required|exists:condicion_pago,id',
	        'producto_id.*' 		=> 'required',
	        'precio_venta.*' 	=> 'required',
        ], $messages);

        if ($validator->fails()) {
	      return redirect('/clientes/'.$id)->withErrors($validator);
	    }

        DB::beginTransaction();
        $cliente =  Cliente::findOrFail($id);
        $cliente->forma_pago_id = ( $request->forma_pago == "" ? null : $request->forma_pago );
		$cliente->condicion_pago_id = ( $request->condicion_pago == "" ? null : $request->condicion_pago );
        $cliente->update();

    	//DELETE ALL PRODUCTS
		foreach($cliente->precios as $precio_producto){
			$precio_producto->delete();
		}
		
		//INSERT PRODUCTS
		for ($i=0; $i < count($request->producto_id); $i++) { 
			$precios_producto = [
		        'establecimiento_id'    => NULL,
                'producto_id'           => $request->producto_id[$i],
                'precio_venta'      	=> $request->precio_venta[$i],
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $precio_producto_cliente = new EstablecimientoPrecios($precios_producto);
            $cliente->precios()->save($precio_producto_cliente);
		}
        
        DB::commit();
        return redirect()->intended('/clientes/'.$id)->with('update_acuerdos_success', true);
    }

    public function addProducto(Request $request, $id ){
        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        DB::beginTransaction();
        $cliente = Cliente::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'producto_agregar'   => 'required',
            'precio_unitario'   => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_add_producto', true);
        }
        
        $precios_producto = [
            'establecimiento_id'    => NULL,
            'producto_id'           => $request->producto_agregar,
            'precio_venta'      	=> $request->precio_unitario,
            'created_by'            => Auth::user()->id,
            'updated_by'            => Auth::user()->id,
        ];
        $precio_producto_cliente = new EstablecimientoPrecios($precios_producto);
        $cliente->precios()->save($precio_producto_cliente);

        DB::commit();

        return redirect()->intended('clientes/'.$cliente->id)->with('add_producto_success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    DB::beginTransaction();
	    
		$cliente = Cliente::findOrFail($id);

		foreach ($cliente->establecimientos as $establecimiento) {
			$establecimiento->delete();
		}
		
		$cliente->delete();

	    DB::commit();
	    
	    return redirect()->intended('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivar($id)
    {
	    DB::beginTransaction();
	    
	    $cliente = Cliente::withTrashed()->findOrFail($id);

	    foreach ($cliente->establecimientos as $establecimiento) {
			$establecimiento->restore();
		}
		
		$cliente->restore();

	    DB::commit();
	    
	    return redirect()->intended('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyContacto($cliente_id, $contacto_id)
    {
    	DB::beginTransaction();
        $contacto = Contacto::findOrFail($contacto_id);

        if ($contacto->user_id != null) {
        	$usuario = Usuario::withTrashed()->findOrFail($contacto->user_id);
        	$usuario->delete();
        }
                
		$contacto->delete();
		
		DB::commit();
		
        return redirect()->intended('/clientes/'.$cliente_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDatosFiscales($cliente_id, $datos_fiscales_id)
    {
        
        DB::beginTransaction();
        $datos_fiscales = DatosFiscales::findOrFail($datos_fiscales_id);
        $cliente = Cliente::findOrFail($cliente_id);

        foreach ($cliente->establecimientos as $establecimiento) {
			if($establecimiento->cliente_datos_fiscales_id == $datos_fiscales_id)
				$establecimiento->cliente_datos_fiscales_id = null;
		}
		
		$datos_fiscales->delete();
		
		DB::commit();
		
        return redirect()->intended('/clientes/'.$cliente_id);
    }

}
