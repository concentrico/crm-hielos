<?php
namespace App\Http\Controllers\Sistema;

set_time_limit(0);
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events\EventoPedido as EventoPedido;
use App\Models\Cliente as Cliente;
use App\Models\Pedido as Pedido;
use App\Models\Producto as Producto;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;

use Carbon\Carbon;
use Auth;
use DB;
use Gate;
use Input;
use Validator;

class DashboardController extends MyController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $data = array();

	    $data['pedidos_hoy'] = 0;

        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $fecha_hoy_mas_uno = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addDay())->formatLocalized('%Y-%m-%d');

        // YEAR FILTER CONSUMO
        if (Input::has('f_year_consumo'))
            $year_filter_consumo = Input::get('f_year_consumo');
        else
            $year_filter_consumo = date('Y');

        // YEAR FILTER VENTAS
        if (Input::has('f_year_ventas'))
            $year_filter_ventas = Input::get('f_year_ventas');
        else
            $year_filter_ventas = date('Y');

		/* Highcharts */
		$productos = DB::table('producto')
			->join('inventario', 'producto.id', '=', 'inventario.producto_id')
			->select('*')
			->whereNotNull('inventario.cantidad_disponible')
			->get()->toArray();
		$data['producto'] = $productos;

		$productos_hielo = Producto::where('es_hielo', '=', '1')->get();

		foreach ($productos_hielo as $producto) {
			//CONSUMO
			for ($i=1; $i <= 12; $i++) { 
				$auxDate1 = Carbon::createFromFormat('Y-m-d', $year_filter_consumo.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
				$auxDate2 = Carbon::createFromFormat('Y-m-d', $year_filter_consumo.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $auxDate1)->daysInMonth)->addDay()->formatLocalized('%Y-%m-%d');

                // FILTRO CONSUMO CLIENTE
                if (Input::has('f_consumo_cliente'))
                {
                    $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido.cliente_id', Input::get('f_consumo_cliente')],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');
                } else {
                    $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');
                }

				$data['pedidos_entregados_mensual'][$producto->id][$i] = (int) array_sum($array_count->toArray());

                for($j = 0; $j < Carbon::createFromFormat('Y-m-d', $auxDate1)->daysInMonth; $j++){

                    $auxDate5 = $year_filter_consumo.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($j + 1, 2, "0", STR_PAD_LEFT);

                    // FILTRO CONSUMO CLIENTE
                    if (Input::has('f_consumo_cliente'))
                    {

                        $array_count3 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido.cliente_id', Input::get('f_consumo_cliente')],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereRaw("DATE(pedido.entregado_at) = DATE('".$auxDate5."')" )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');

                    } else {
                        $array_count3 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereRaw("DATE(pedido.entregado_at) = DATE('".$auxDate5."')" )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');
                    }

                    $data['pedidos_entregados_dia'][$producto->id][$i][$j] = (int) array_sum($array_count3->toArray());
                }
			}

            //VENTAS
            for ($i=1; $i <= 12; $i++) { 

                $auxDate3 = Carbon::createFromFormat('Y-m-d', $year_filter_ventas.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
                $auxDate4 = Carbon::createFromFormat('Y-m-d', $year_filter_ventas.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $auxDate3)->daysInMonth)->addDay()->formatLocalized('%Y-%m-%d');

                // FILTRO VENTAS CLIENTE
                if (Input::has('f_ventas_cliente'))
                {
                    $array_count2 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido.cliente_id', Input::get('f_ventas_cliente')],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate3, $auxDate4) )
                        ->get() ->toArray() ) ->pluck('total');
                } else {
                    $array_count2 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate3, $auxDate4) )
                        ->get() ->toArray() ) ->pluck('total');
                }

                $data['pedidos_entregados_mensual_clientes'][$producto->id][$i] = (int) array_sum($array_count2->toArray());

                for($j = 0; $j < Carbon::createFromFormat('Y-m-d', $auxDate3)->daysInMonth; $j++){
                    $auxDate6 = $year_filter_ventas.'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($j + 1, 2, "0", STR_PAD_LEFT);

                    // FILTRO VENTAS CLIENTE
                    if (Input::has('f_ventas_cliente'))
                    {
                        $array_count4 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido.cliente_id', Input::get('f_ventas_cliente')],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereRaw("DATE(pedido.entregado_at) = DATE('".$auxDate6."')" )
                        ->get() ->toArray() ) ->pluck('total');
                    } else {
                        $array_count4 = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->where([
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereRaw("DATE(pedido.entregado_at) = DATE('".$auxDate6."')" )
                        ->get() ->toArray() ) ->pluck('total');
                    }

                    $data['pedidos_entregados_dia_clientes'][$producto->id][$i][$j] = (int) array_sum($array_count4->toArray());     
                }
            }

		}

        $pedidos = Pedido::all();

        $data['pedidos_programados'] = $pedidos->where('status', 1)->where('fecha_entrega', '<', $fecha_hoy_mas_uno)->sortBy('fecha_entrega');
        $data['pedidos_en_ruta'] = $pedidos->where('status', 2);
        $data['pedidos_entregados'] = $pedidos->where('status', 3)->where('entregado_at', '>=', $fecha_hoy)->sortByDesc('entregado_at');

        /* Pedidos de HOY */
        $data['pedidos_hoy'] = $data['pedidos_programados']->count() + $data['pedidos_en_ruta']->count() + $data['pedidos_entregados']->count();

        $sum_saldo_vencido=0.0;
        foreach ($pedidos as $pedido) {
            $sum_saldo_vencido+=floatval($pedido->por_cobrar());
        }

        $data['total_vencido'] = $sum_saldo_vencido;

        $first_order = $pedidos->where('status',3)->sortBy('entregado_at')->first();
        if ($first_order == null) {
            $anio_first_order = (int) date('Y');
        } else {
            $anio_first_order = (int) Carbon::createFromFormat('Y-m-d H:i:s', $first_order->entregado_at)->formatLocalized('%Y');
        }

        $anio_current = (int) date('Y');

        if ($anio_current == $anio_first_order) {
            $data['years'] = [$anio_current => $anio_current];
        } else {
            $array_years = array(); 
            for ($i=0; $i <= ($anio_current-$anio_first_order); $i++) { 
                $array_years[($anio_first_order + $i)] = ($anio_first_order + $i);
            }
            $data['years'] = $array_years;
        }

    	if (Auth::user()->can('manage-users') ){
    		return view('sistema.dashboard.index', $data);
    	}

    	if (Auth::user()->can('manage-distribucion')){
    		return redirect()->intended('distribucion');
    	}
			
    	if (Auth::user()->can('manage-produccion')){
    		return redirect()->intended('produccion');
    	}

    	if (Auth::user()->can('manage-choferes')){
    		return redirect()->intended('choferes');
    	}

    }

    public function testPusher(){
    	$pedido = Pedido::where('status', 3)->firstOrFail();
    	event(new EventoPedido($pedido));
    }

    public function indexDireccion()
    {
        
        $data = array();
        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $fecha_hoy_mas_uno = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addDay())->formatLocalized('%Y-%m-%d');

        $data['pedidos_programados'] = Pedido::where('status', 1)->where('fecha_entrega', '<', $fecha_hoy_mas_uno)->orderBy('fecha_entrega', 'asc')->get();
        $data['pedidos_en_ruta'] = Pedido::where('status', 2)->get();
        $data['pedidos_entregados'] = Pedido::where('status', 3)->where('entregado_at', '>=', $fecha_hoy)->orderBy('entregado_at', 'desc')->get();

        /*Progress Distribucion*/
        $pedidos_hoy = count($data['pedidos_programados']) + count($data['pedidos_en_ruta']) + count($data['pedidos_entregados']);

        if ($pedidos_hoy > 0)
            $data['porcentaje_distribucion'] = ( count($data['pedidos_entregados']) /  $pedidos_hoy )*100;
        else
            $data['porcentaje_distribucion']=0;

        $data['inventario'] = Inventario::all();
        $data['entradas_hoy'] = InventarioEntradas::where('motivo', 'produccion')->where('created_at', '>=', $fecha_hoy)->orderBy('created_at', 'desc')->get();

        $productos_hielo = Producto::where('es_hielo', '=', '1')->get();
        foreach ($productos_hielo as $producto) {
            $meta = (int) $producto->getNumBolsasProducir();
            $data['produccion_meta'][] = array('nombre' => $producto->nombre, 'bolsas' => $meta );
            $produccion_count = collect(InventarioEntradas::where('motivo', 'produccion')->where('created_at', '>=', $fecha_hoy)->where('producto_id', $producto->id)->get()->toArray())->pluck('cantidad_entrada');
            $producido = (int) array_sum($produccion_count->toArray());
            $data['producidas'][] = array('nombre' => $producto->nombre, 'bolsas' => $producido );
            $pendiente = $meta-$producido;
            $data['pendientes'][] = array('nombre' => $producto->nombre, 'bolsas' => $pendiente );
        }

        $cont_programados = 0;
        for ($i=0; $i < count($data['pedidos_programados']); $i++) { 
            $pedido = $data['pedidos_programados'][$i];
            foreach ($pedido->productos as $pedido_productos) {
                $cont_programados += $pedido_productos->cantidad_requerida;
            }
        }

        $cont_ruta = 0;
        for ($i=0; $i < count($data['pedidos_en_ruta']); $i++) { 
            $pedido = $data['pedidos_en_ruta'][$i];
            foreach ($pedido->productos as $pedido_productos) {
                $cont_ruta += $pedido_productos->cantidad_requerida;
            }
        }

        $cont_entregados = 0;
        for ($i=0; $i < count($data['pedidos_entregados']); $i++) { 
            $pedido = $data['pedidos_entregados'][$i];
            foreach ($pedido->productos as $pedido_productos) {
                $cont_entregados += $pedido_productos->cantidad_recibida;
            }
        }

        $data['bolsas_por_entregar'] = $cont_programados+$cont_ruta;
        $data['bolsas_entregadas'] = $cont_entregados;

        return view('sistema.dashboard.direccion.index', $data);
    }

    public function indexDistribucion()
    {
        if (!Auth::user()->can('manage-distribucion') && !Auth::user()->can('manage-users')){
            $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
	    
	    $data = array();
        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $fecha_hoy_mas_uno = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addDay())->formatLocalized('%Y-%m-%d');

	    $data['pedidos_programados'] = Pedido::where('status', 1)->where('fecha_entrega', '<', $fecha_hoy_mas_uno)->orderBy('fecha_entrega', 'asc')->get();
	    $data['pedidos_en_ruta'] = Pedido::where('status', 2)->get();
	    $data['pedidos_entregados'] = Pedido::where('status', 3)->where('entregado_at', '>=', $fecha_hoy)->orderBy('entregado_at', 'desc')->get();

        $data['bolsas_por_entregar'] = 0;
        $data['bolsas_entregadas'] = 0;

	    return view('sistema.dashboard.distribucion.index', $data);
    }

    public function indexProduccion()
    {
        if (!Auth::user()->can('manage-produccion') && !Auth::user()->can('manage-users')){
            $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

	    $data = array();
	    $data['inventario'] = Inventario::all();
	    
        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
    	$data['entradas_hoy'] = InventarioEntradas::where('motivo', 'produccion')->where('created_at', '>=', $fecha_hoy)->orderBy('created_at', 'desc')->get();

	    $productos_hielo = Producto::where('es_hielo', '=', '1')->get();
		foreach ($productos_hielo as $producto) {
			$produccion_count = collect(InventarioEntradas::where('motivo', 'produccion')->where('created_at', '>=', $fecha_hoy)->where('producto_id', $producto->id)->get()->toArray())->pluck('cantidad_entrada');
			$producido = (int) array_sum($produccion_count->toArray());
			$data['producidas'][] = array('nombre' => $producto->nombre, 'bolsas' => $producido );
		}

	    return view('sistema.dashboard.produccion.index', $data);
    }



    public function registrarProduccion(Request $request) {

        if (Gate::denies('manage-users') && Gate::denies('manage-produccion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
             'producto_produccion' => 'required',
             'cantidad_produccion' => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        $inventario = Inventario::where('producto_id', $request->producto_produccion)->firstOrFail();

        $entrada = [
            'producto_id'           => $inventario->producto_id,
            'cantidad_entrada'      => $request->cantidad_produccion,
            'motivo'                => 'produccion',
            'created_by'            => Auth::user()->id,
            'updated_by'            => Auth::user()->id,
        ];

        $inv_entrada = new \App\Models\InventarioEntradas($entrada);
        $inventario->entradas()->save($inv_entrada);

        $cant_actual = $inventario->cantidad_disponible;
        $cant_nueva = intval($cant_actual)+$request->cantidad_produccion;
        $inventario->cantidad_disponible = $cant_nueva;

        if ($request->movimiento == 'produccion') {
            // Registro de Log
            $log_usuario = new LogUsuario;
            $log_usuario->user_id = Auth::user()->id;
            $log_usuario->tipo = 'produccion';
            $log_usuario->comentarios = 'Se registraron '.$request->cantidad_produccion.' entradas de producción. Inventario Actual: <b>'.$cant_nueva.'</b>';
            $log_usuario->save();
        }


        $inventario->update();
        DB::commit();

        return redirect()->intended('produccion')->with('registro_success', true);
    }
}
