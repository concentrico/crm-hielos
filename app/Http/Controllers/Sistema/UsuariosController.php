<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

use App\Http\Requests;

use App\User as Usuario;
use App\Models\ClienteContacto as ClienteContacto;
use App\Models\UserRole as UserRole;
use App\Models\Permiso as Permiso;
use App\Models\LogUsuario as LogUsuario;

use Auth;
use DB;
use Hash;
use Gate;
use Input;
use Mail;
use Validator;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Gate::denies('manage-users')) {
	        $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
	    	return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

		$q = Usuario::query();

		$aux_user_roles_view = array();
        if (Gate::allows('view-user', 1)) {
			array_push($aux_user_roles_view, 1);
        }
        if (Gate::allows('view-user', 2)) {
			array_push($aux_user_roles_view, 2);
        }
        if (Gate::allows('view-user', 3)) {
			array_push($aux_user_roles_view, 3);
        }
        if (Gate::allows('view-user', 4)) {
			array_push($aux_user_roles_view, 4);
        }
        if (Gate::allows('view-user', 5)) {
			array_push($aux_user_roles_view, 5);
        }
        if (Gate::allows('view-user', 6)) {
			array_push($aux_user_roles_view, 6);
        }
        if (Gate::allows('view-user', 7)) {
			array_push($aux_user_roles_view, 7);
        }
                
// 		$q->whereIn('user_role_id', $aux_user_roles_view);
		
		if (Input::has('f_nombre'))
		{
			$q->whereRaw("( nombre LIKE '%". Input::get('f_nombre') ."%'
							OR email LIKE '%". Input::get('f_nombre') ."%' )");
		}

		// FILTRO ROLES
		if (Input::has('f_rol'))
		{
			$aux_roles = implode(", ", Input::get('f_rol'));
			$q->whereRaw('((SELECT COUNT(user_id) FROM user_has_role WHERE user_id = users.id AND user_role_id IN ('. $aux_roles .')) > 0)');
		}

		if (Input::has('f_estatus'))
		{
			if(Input::get('f_estatus') == 'todos'){
				$q->withTrashed();
			}else if(Input::get('f_estatus') == 'suspendido'){
				$q->onlyTrashed();
			}
		}

		if (Input::has('orderBy') && Input::has('sort')){

			$sort_aux = ( Input::get('sort') == 'ASC' ? 'ASC' : 'DESC');
			
			$orderByString = "";
			switch(Input::get('orderBy')){
				case 'ID' : 
					$orderByString = 'id '.$sort_aux;
					break;
				case 'NOMBRE' : 
					$orderByString = 'nombre '.$sort_aux;
					break;
				case 'EMAIL' : 
					$orderByString = 'email '.$sort_aux;
					break;
				case 'ROL' : 
					$orderByString = '(SELECT user_role_id FROM user_has_role WHERE users.id = user_id) '.$sort_aux;
					break;
				default : 
					$orderByString = "";
					break;
			}
			
			if($orderByString != ""){
				$q->orderByRaw($orderByString);
			}
		}else{
			$q->orderByRaw('id');
		}

		// NUM RESULTADOS
		
		if (Input::has('f_num_resultados'))
		{

			$num_resultados = 0 + ((int) Input::get('f_num_resultados') );
			$data['usuarios'] = $q->paginate($num_resultados);
			
			if( count($data['usuarios']) == 0 ){
				$data['usuarios'] = $q->paginate($num_resultados, ['*'], 'page', 1);
			}
		}else{
			$data['usuarios'] = $q->paginate(50);
		}

		$data['user_roles'] = UserRole::all();

		$data['user_roles_select'] = UserRole::all()->whereIn('id', $aux_user_roles_view)->pluck('nombre', 'id')->toArray();
		$data['user_roles_select'][''] = '';
		
		return view('sistema.usuarios.index', $data);
	    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $msgs = array(
			'confirmed' => 'Los emails no coinciden.',
			'required' => 'Campo requerido.',
			'date_format' => 'La fecha debe tener un formato dd/mm/yyyy',
			'username.unique' => 'El usuario ya está dado de alta.',
			'email.unique' => 'El email ya está dado de alta.',
	    );

	    if (isset($request->rol)) {

		    if ($request->rol == '4') {
		    	#Chofer
		    	$validator = Validator::make($request->all(), [
			        'rol' 		=> 'required|exists:user_role,id',
			        'nombre' 	=> 'required',
			        'username' 	=> 'required|unique:users,username',
			        'fecha_nacimiento' => 'required|date_format:"d/m/Y"',
			        'telefono' => 'required',
			        'celular' => 'required',
			        'email' 	=> 'email|confirmed|unique:users,email',
			        'email_confirmation' => 'email'
		        ], $msgs);

		        if ($validator->fails()) {
		            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_create', true)->with('chofer', true);
		        }

		    } else {
		    	#Todos los demás
		    	$validator = Validator::make($request->all(), [
			        'rol' 		=> 'required|exists:user_role,id',
			        'nombre' 	=> 'required',
			        'username' 	=> 'required|unique:users,username',
			        'email' 	=> 'email|confirmed|unique:users,email',
			        'email_confirmation' => 'email'
		        ], $msgs);

		        if ($validator->fails()) {
		            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_create', true)->with('chofer', false);
		        }
		    }

		} else {

			$validator = Validator::make($request->all(), [
		        'rol' 		=> 'required|exists:user_role,id',
		        'nombre' 	=> 'required',
		        'username' 	=> 'required',
	        ], $msgs);

	        if ($validator->fails()) {
	            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_create', true)->with('chofer', false);
	        }

		}

	    DB::beginTransaction();

        $usuario = new Usuario;
		
        $usuario->nombre = trim($request->nombre);
        if ($request->email != '')
        	$usuario->email = trim($request->email);
        else
        	$usuario->email = null;
        $usuario->username = trim($request->username);
	    $usuario->password = Hash::make(Hash::make(rand(0,100)));
	    $usuario->tipo = 'sistema';
	    $usuario->fecha_nacimiento = trim($request->fecha_nacimiento);
	    $usuario->telefono = trim($request->telefono);
	    $usuario->celular = trim($request->celular);
        $usuario->created_by = Auth::user()->id;
        $usuario->updated_by = Auth::user()->id;

		$usuario->save();

		$usuario->roles()->attach($request->rol, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
		
		if ($request->email != '' && $usuario->email != NULL) {
			$usuario->sendWelcomeNotification();
		}

		DB::commit();
		
        return redirect()->intended('/usuarios/'.$usuario->id);
    }

    public function createFromContact(Request $request) {

    	$msgs = array(
			'required' => 'Campo requerido.',
			//'contacto_user_email' => 'Ingrese un email válido.',
			'username.unique' => 'El usuario ya está dado de alta.',
			'email.unique' => 'El email ya está dado de alta.',
	    );

    	$validator = Validator::make($request->all(), [
	        'contacto_id' 			=> 'required',
	        'cliente_id' 			=> 'required',
	        'rols' 					=> 'required|exists:user_role,id',
	        'username' 				=> 'required|unique:users,username',
	        'password' 				=> 'required',
	        //'contacto_user_email' 	=> 'required|email|unique:users,email'
        ], $msgs);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_user_create', true)->with('contacto_id', $request->contacto_id);
        }

        DB::beginTransaction();

        $usuario = new Usuario;
        $contacto = ClienteContacto::findOrFail($request->contacto_id);
		
		$full_name = trim($contacto->nombre).' '.trim($contacto->apellido_paterno).' '.trim($contacto->apellido_materno);
        $usuario->nombre = trim($full_name);
        $usuario->tipo = 'cliente';
        //$usuario->email = trim($request->contacto_user_email);
        $usuario->username = trim($request->username);
        //$usuario->password = Hash::make(Hash::make(rand(0,100)));
        $usuario->password = Hash::make($request->password);
        $usuario->created_by = Auth::user()->id;
        $usuario->updated_by = Auth::user()->id;

		$usuario->save();

		$contacto->user_id = $usuario->id;
		$contacto->update();

		if(count($request->rols) > 0){
			foreach($request->rols as $g){
				$usuario->roles()->attach($g, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
			}			
		}

		DB::commit();
		//$usuario->sendWelcomeNotification();
		
        //return redirect()->intended('/clientes/'.$request->cliente_id)->with('email_send_success', true);
        return redirect()->intended('/clientes/'.$request->cliente_id)->with('contact_create_success', true);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$data['usuario'] = Usuario::findOrFail($id);

        if (Gate::denies('manage-users', $data['usuario']->user_role_id)) {
	        $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
	    	return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
		
		return view('sistema.usuarios.show', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPerfil()
    {
		$data['usuario'] = Usuario::findOrFail(Auth::user()->id);
		
		return view('sistema.usuarios.show_perfil', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    	if (Gate::denies('manage-users')) {
	        $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
	    	return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

	    $msgs = array(
			'confirmed' => 'Los emails no coinciden.',
			'required' => 'Campo requerido.',
			'date_format' => 'La fecha debe tener un formato dd/mm/yyyy',
			'email_edit.unique' => 'El email ya está dado de alta.',
			'username_edit.unique' => 'El usuario ya está dado de alta.',
	    );

	    if ($request->rol_edit == '4') {
	    	# Chofer
	    	$validator = Validator::make($request->all(), [
		        'rol_edit' 		=> 'required|exists:user_role,id',
		        'nombre_edit' 	=> 'required',
			    'username_edit' 	=> 'required|unique:users,username,'.$id,
		        'fecha_nacimiento_edit' => 'required|date_format:"d/m/Y"',
		        'telefono_edit' => 'required',
		        'celular_edit' => 'required'
	        ], $msgs);

	        if ($validator->fails()) {
	            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_edit', true)->with('chofer', true);
	        }

	    } else {
	    	#Todos los demás
	    	$validator = Validator::make($request->all(), [
		        'rol_edit' 		=> 'required|exists:user_role,id',
		        'nombre_edit' 	=> 'required',
			    'username_edit' => 'required|unique:users,username,'.$id,
		        'email_edit' 	=> 'email|confirmed|unique:users,email,'.$id,
		        'email_edit_confirmation' => 'email'
	        ], $msgs);

	        if ($validator->fails()) {
	            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_edit', true)->with('chofer', false);
	        }
	    }

	    DB::beginTransaction();
	    $reenviar_email=false;
	    $usuario = Usuario::findOrFail($id);
	    if ($request->email_edit != $usuario->email) {
			# Ha cambiado el email enviar notificacion via email
			$reenviar_email=true;
		}

        $usuario->nombre = trim($request->nombre_edit);
        $usuario->email = trim($request->email_edit);
        $usuario->username = trim($request->username_edit);
        $usuario->tipo = 'sistema';
        $usuario->updated_by = Auth::user()->id;
	    $usuario->fecha_nacimiento = trim($request->fecha_nacimiento_edit);
	    $usuario->telefono = trim($request->telefono_edit);
	    $usuario->celular = trim($request->celular_edit);

		$roles = array();
		$roles[$request->rol_edit] = array('created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id);
		$usuario->roles()->sync($roles);

		$usuario->update();

		if ($reenviar_email) {
			$token = Password::getRepository()->create( $usuario );
			$usuario->sendPasswordActivateNotification($token);
		}

		DB::commit();
        return redirect()->intended('/usuarios/'.$usuario->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update2(Request $request, $id)
    {

        if (Gate::denies('manage-users')) {
	        $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
	    	return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

	    $msgs = array(
			'confirmed' => 'Los emails no coinciden.',
			'required' => 'Campo requerido.',
			'date_format' => 'La fecha debe tener un formato dd/mm/yyyy',
			'email_edit.unique' => 'El email ya está dado de alta.',
			'username_edit.unique' => 'El usuario ya está dado de alta.',
	    );

	    if ( isset($request->rol_edit) && $request->rol_edit == '4' ) {
	    	$validator = Validator::make($request->all(), [
		        'rol_edit' 		=> 'required|exists:user_role,id',
		        'nombre_edit' 	=> 'required',
			    'username_edit' 	=> 'required|unique:users,username,'.$id,
		        'email_edit' 	=> 'email|confirmed|unique:users,email,'.$id,
		        'email_edit_confirmation' => 'email',
		        //'password_edit' 	=> 'required|confirmed',
		        //'password_edit_confirmation' => 'required',
		        'fecha_nacimiento_edit' => 'required|date_format:"d/m/Y"',
		        'telefono_edit' => 'required',
		        'celular_edit' => 'required'
	        ], $msgs);

	        if ($validator->fails()) {
	            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_edit', true)->with('chofer', true);
	        }

	    } else {
	    	$validator = Validator::make($request->all(), [
		        'rol_edit' 		=> 'required|exists:user_role,id',
		        'nombre_edit' 	=> 'required',
			    'username_edit' 	=> 'required|unique:users,username,'.$id,
		        //'password_edit' 	=> 'required|confirmed',
		        //'password_edit_confirmation' => 'required',
		        'email_edit' 	=> 'email|confirmed|unique:users,email,'.$id,
		        'email_edit_confirmation' => 'email'
	        ], $msgs);

	        if ($validator->fails()) {
	            return back()->withErrors($validator)->withInput()->with('show_modal_usuario_edit', true)->with('chofer', false);
	        }
	    }

	    DB::beginTransaction();
        $reenviar_email=false;
	    $usuario = Usuario::findOrFail($id);
	    if ($request->email_edit != $usuario->email) {
			# Ha cambiado el email enviar notificacion via email
			$reenviar_email=true;
		}

        $usuario->nombre = $request->nombre_edit;
        $usuario->username = $request->username_edit;
        
        if($request->email_edit != null && $request->email_edit != ""){
        	$usuario->email = $request->email_edit;
        }

        if($request->password_edit != null && $request->password_edit != "" && Gate::allows('update-user-password', Auth::user())){
	        $usuario->password = Hash::make($request->password_edit);
        }
        
	    $usuario->fecha_nacimiento = trim($request->fecha_nacimiento_edit);
	    $usuario->telefono = trim($request->telefono_edit);
	    $usuario->celular = trim($request->celular_edit);
        
        $usuario->updated_by = Auth::user()->id;

		$roles = array();
		$roles[$request->rol_edit] = array('created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id);
		$usuario->roles()->sync($roles);

		$usuario->update();

		if ($reenviar_email) {
			$token = Password::getRepository()->create( $usuario );
			$usuario->sendPasswordActivateNotification($token);
		}

		DB::commit();
		
        return redirect()->intended('/usuarios/'.$usuario->id)->with('usuario_edit_success', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePerfil(Request $request)
    {
        $usuario = Usuario::findOrFail(Auth::user()->id);

	    $msgs = array(
			'required' => 'Campo requerido.',
			'date_format' => 'La fecha debe tener un formato dd/mm/yyyy'
	    );

	    $validator = Validator::make($request->all(), [
	        'nombre' 	=> 'required',
	        'username' 	=> 'required',
	        'email' 	=> 'required|email|unique:users,email,'.Auth::user()->id,
	        'fecha_nacimiento' => 'date_format:"d/m/Y"',
        ], $msgs);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

	    DB::beginTransaction();

        $usuario->nombre = trim($request->nombre);
        $usuario->email = trim($request->email);
        $usuario->username = trim($request->username);
	    $usuario->fecha_nacimiento = trim($request->fecha_nacimiento);
	    $usuario->telefono = trim($request->telefono);
	    $usuario->celular = trim($request->celular);
        $usuario->updated_by = Auth::user()->id;
		$usuario->update();

		DB::commit();
		
        return redirect()->intended('/perfil')->with('perfil_update_success',true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::findOrFail($id);
 
 	    DB::beginTransaction();
       
        $usuario->delete();        
        $usuario->update();
        
	    DB::commit();
        
        return back();
    }

    public function postEmail2(Request $request)
    {
        $this->validate($request, ['contacto_user_email' => 'required|email']);


        $array = ['email' => $request->only('contacto_user_email')];
        $response = Password::sendResetLink($array);
		
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans($response))->with('email_send_success', true);

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }
    
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'));
		
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans($response))->with('email_send_success', true);

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivar($id)
    {
	    DB::beginTransaction();
	    
		$usuario = Usuario::withTrashed()->findOrFail($id);
		
		$usuario->restore();

	    DB::commit();
	    
	    return redirect()->intended('usuarios');
    }

}
