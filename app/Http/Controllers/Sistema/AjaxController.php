<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;

use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Producto as Producto;
use App\Models\ModeloConservador as ModeloConservador;
use App\Models\Conservador as Conservador;
use App\Models\LogConservador as LogConservador;
use App\Models\LogUsuario as LogUsuario;
use App\Models\CodigoPostal as CodigoPostal;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\LogInventario as LogInventario;
use App\Models\Pedido as Pedido;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\ClienteDatosFiscales as DatosFiscales;

use Auth;
use DB;
use Input;
use Validator;
use Carbon\Carbon;
use Mail;

class AjaxController extends Controller
{
    public function ajaxCodigosPostales(Request $request)
    {
        if($request->ajax()){

        	switch ($request->type) {
        		case '1':
        			# Municipios
        			$query = $request->busqueda;
		        	$res = CodigoPostal::where('municipio', 'LIKE', "%$query%")->get()->pluck('municipio')->toArray();

		        	$municipios = array_unique($res);
		        	$array_municipios = array_values($municipios);

					return response()->json($array_municipios);
        			break;
        		case '2':
        			# Codigos Postales
        			$query = $request->busqueda;
        			$municipio = $request->municipio;

        			if($municipio != '') {
        				$res = CodigoPostal::where('codigo_postal', 'LIKE', "%$query%")->where('municipio', $municipio)->get()->pluck('codigo_postal')->toArray();
        			} else {
        				$res = CodigoPostal::where('codigo_postal', 'LIKE', "%$query%")->get()->pluck('codigo_postal')->toArray();
        			}

		        	$codigos_postales = array_unique($res);
		        	$array_cp = array_values($codigos_postales);

					return response()->json($array_cp);
        			break;
        		case '3':
        			# Colonias
        			$query = $request->busqueda;
        			$municipio = $request->municipio;
        			$codigo_postal = $request->codigo_postal;

        			if($codigo_postal != '') {
        				$res = CodigoPostal::where('asentamiento', 'LIKE', "%$query%")->where('codigo_postal', $codigo_postal)->get()->pluck('asentamiento')->toArray();
        			} else if ($municipio != '') {
        				$res = CodigoPostal::where('asentamiento', 'LIKE', "%$query%")->where('municipio', $municipio)->get()->pluck('asentamiento')->toArray();
        			} else {
        				$res = CodigoPostal::where('asentamiento', 'LIKE', "%$query%")->get()->pluck('asentamiento')->toArray();
        			}


		        	$colonias = array_unique($res);
		        	$array_colonias = array_values($colonias);

					return response()->json($array_colonias);
        			break;

        		case '4':
        			# Cargar Datos de Colonia
        			$colonia = $request->colonia;

        			$res = CodigoPostal::where('asentamiento', $colonia)->get();

        			$response_data = array();
        			$i=0;
        			foreach ($res as $cp) {

        				$response_data[$i]['codigo_postal'] = $cp->codigo_postal;
        				$response_data[$i]['municipio'] = $cp->municipio;
					    $i++;
        			}

					return response()->json($response_data);
        			break;
        	}

        	
	    }
    }

    public function ajaxProductos(Request $request)
    {
        if($request->ajax()){
	        switch ($request->type) {
	    		case '1':
	    			# Obtener Datos Producto
	    			$producto =  Producto::withTrashed()->findOrFail($request->producto_id);

	    			$response_data = array(
			          "id" => $producto->id,
			          "id_codigo" => $producto->id_codigo,
			          "tipo" => $producto->tipo, 
			          "es_hielo" => $producto->es_hielo, 
			          "nombre" => $producto->nombre, 
			          "peso" => $producto->peso, 
			          "precio_venta" => $producto->precio_venta,
			          "iva" => $producto->iva,
				    );

	    		return response()->json($response_data);
	    		break;
	    	}
	    }
    }

    public function ajaxEstablecimientos(Request $request) {
    	if($request->ajax()){
    		switch ($request->type) {
	    		case '1':
	    			# Obtener Razones sociales establecimiento
	    			$cliente =  Cliente::findOrFail($request->cliente_id);
	    			$array = $cliente->datos_fiscales;

	    			$response_data = [];
	    			
	    			$response_data[] = array(
			          "id" => '',
			          "text" => '', 
					  'selected' => 'selected'
				    );
				    
	    			for ($i=0; $i < count($array); $i++) { 
				        $response_data[] = array(
				          "id" => $array[$i]['id'],
				          "text" => $array[$i]['razon_social']
				        );
				    }

	    		return $response_data;
	    		#return response()->json($response_data);
	    		break;

	    		case '2':
	    			# Obtener Conservadores Filtrados

	    			$num_res = $request->FormData[0]['num_res'];
	    			$modelo_id = $request->FormData[0]['modelo'];
	    			$ult_mtto = $request->FormData[0]['ult_mtto'];

	    			if($num_res == '' || $num_res == 'todos' ) $num_res = 10000;

	    			if ($modelo_id != '' && $ult_mtto != '') {
	    				$conservadores = \App\Models\Conservador::where(
		    				[
							    ['status', '=', 'stand_by'],
							    ['modelo_id', '=', $modelo_id],
							    ['ultimo_mantenimiento', '<', Carbon::now()->addDays($ult_mtto)],
							]
		    			)
		    			->limit($num_res)
		    			->get();
	    			} else if ($modelo_id != '' && $ult_mtto == '') {
	    				$conservadores = \App\Models\Conservador::where(
		    				[
							    ['status', '=', 'stand_by'],
							    ['modelo_id', '=', $modelo_id],
							]
		    			)
		    			->limit($num_res)
		    			->get();
	    			} else if ($modelo_id == '' && $ult_mtto != '') { 
	    				$conservadores = \App\Models\Conservador::where(
		    				[
							    ['status', '=', 'stand_by'],
							    ['ultimo_mantenimiento', '<', Carbon::now()->addDays($ult_mtto)],
							]
		    			)
		    			->limit($num_res)
		    			->get();
	    			} else {
	    				$conservadores = \App\Models\Conservador::where(
		    				[
							    ['status', '=', 'stand_by']
							]
		    			)
		    			->limit($num_res)
		    			->get();
	    			}

	    			$response_data = '';
	    			foreach($conservadores as $conservador) {
						$response_data .= '<tr>
                            <td class="col-xs-1">'.$conservador->id_codigo.'</td>
                            <td class="col-xs-2">'.$conservador->modelo->modelo.'</td>
                            <td class="col-xs-2"><span class="label label-success">Stand By</span></td>
                            <td class="col-xs-3">'.$conservador->producto->nombre.'</td>
                            <td class="col-xs-3">'.$conservador->modelo->capacidad.' bolsas</td>
                            <td class="col-xs-1"><input type="checkbox" value="'.$conservador->id.'" class="i-checks" name="asignar_conservador[]"></td>
                        </tr>';
					}
					return $response_data;
	    		break;

		    	case '3':
		    		# Agregar Cuarto Frio

		    		DB::beginTransaction();
			        $cuartofrio = new CuartoFrio;
					
			        $cuartofrio->codigo = NULL;
					$cuartofrio->created_by = Auth::user()->id;
					$cuartofrio->updated_by = Auth::user()->id;
					$cuartofrio->save();

		    		$establecimiento = Establecimiento::findOrFail($request->establecimiento_id);
		    		$establecimiento->cuartofrio_id = $cuartofrio->id;
		    		$establecimiento->update();

		    		DB::commit();
		    	break;

		    	case '4':
			    		# Cargar Sucursales de Cliente

			    		$cliente =  Cliente::findOrFail($request->cliente_id);
			    		//dd($cliente->establecimientos);
		    			$array = $cliente->establecimientos;

		    			$response_data = [];
		    			$response_data[] = array(
				          "id" => '',
				          "text" => '', 
						  'selected' => 'selected'
					    );
		    			for ($i=0; $i < count($array); $i++) { 
					        $response_data[] = array(
					          "id" => $array[$i]['id'],
					          "text" => $array[$i]['sucursal']
					        );
					    }

			    		return response()->json($response_data);
			    break;

			    case '5':
			    		# Cargar Datos de Sucursal

			    		$establecimiento = Establecimiento::findOrFail($request->sucursal_id);

			    		if ($establecimiento->dias_entrega != null)
			    			$dias_entrega = $establecimiento->dias_entrega;
			    		else
			    			$dias_entrega = '0';

			    		$total_pedido=0.0;
			    		$pedido_producto = [];
			    		$pedido_producto_nombre = [];
			    		$precio_producto = [];
			    		$iva_producto = [];
			    		$cantidad_producto = [];
			    		$subtotal_producto = [];

			    		if(count($establecimiento->precios) > 0) {
			    			foreach($establecimiento->precios as $est_producto) {

			    				$producto = Producto::withTrashed()->findOrFail($est_producto->producto_id);

			    				$pedido_producto[] = $est_producto->producto_id;
			    				$pedido_producto_nombre[] = $producto->nombre;
			    				$precio_producto[] = $est_producto->precio_venta;
			    				$iva_producto[] = $producto->iva;
			    				$cantidad_producto[] = $establecimiento->check_cantidad_sugerida_producto($est_producto->producto_id);

			    				$subtotal = $est_producto->precio_venta*intval($establecimiento->check_cantidad_sugerida_producto($est_producto->producto_id));


			    				$subtotal_producto[] = $subtotal+($subtotal*floatval($producto->iva));
                            	
                            	$total_pedido+=$subtotal*floatval($producto->iva);
			    			}
			    		}

		    			$response_data = array(
				          "latitud_longitud" => $establecimiento->latitud_longitud,
				          "direccion" => imprime_direccion($establecimiento),

				          "calle" => $establecimiento->calle,
				          "numero_interior" => $establecimiento->numero_interior,
				          "numero_exterior" => $establecimiento->numero_exterior,
				          "colonia" => $establecimiento->colonia,
				          "codigo_postal" => $establecimiento->codigo_postal,
				          "municipio" => $establecimiento->municipio,
				          "estado" => $establecimiento->estado,

				          "status_inventario" => $establecimiento->status_inventario,
				          "capacidad_actual" => $establecimiento->capacidad_actual,
				          "capacidad_maxima" => $establecimiento->capacidad_maxima,
				          "capacidad_minima" => $establecimiento->capacidad_minima,
				          "punto_reorden" => $establecimiento->punto_reorden,

				          
				          "forma_pago_id" => $establecimiento->forma_pago_id,
				          "condicion_pago_id" => $establecimiento->condicion_pago_id,
				          "cuartofrio_id" => $establecimiento->cuartofrio_id,

				          //Pedido Sugerido
				          "pedido_producto" => $pedido_producto,
				          "pedido_producto_nombre" => $pedido_producto_nombre,
				          "precio_producto" => $precio_producto,
				          "iva_producto" => $iva_producto,
				          "cantidad_producto" => $cantidad_producto,
				          "subtotal_producto" => $subtotal_producto,
				          "total_pedido" => $total_pedido,

				        );

			    		return response()->json($response_data);
			    break;

			    case '6':
			    		# Cargar Datos de Cliente

			    		$cliente = Cliente::findOrFail($request->cliente_id);

			    		$total_pedido=0.0;
			    		$pedido_producto = [];
			    		$pedido_producto_nombre = [];
			    		$precio_producto = [];
			    		$iva_producto = [];
			    		$cantidad_producto = [];
			    		$subtotal_producto = [];

			    		if(count($cliente->precios) > 0) {
			    			foreach($cliente->precios as $est_producto) {
			    				$producto = Producto::withTrashed()->findOrFail($est_producto->producto_id);
			    				$pedido_producto[] = $est_producto->producto_id;
			    				$pedido_producto_nombre[] = $producto->nombre;
			    				$precio_producto[] = $est_producto->precio_venta;
			    				$iva_producto[] = $producto->iva;
			    				$cantidad_producto[] = 1;
			    				$subtotal = $est_producto->precio_venta;
			    				$subtotal_producto[] = $subtotal+($subtotal*floatval($producto->iva));
                            	$total_pedido+=$subtotal*floatval($producto->iva);
			    			}
			    		}

		    			$response_data = array(
				          "forma_pago_id" => $cliente->forma_pago_id,
				          "condicion_pago_id" => $cliente->condicion_pago_id,
				          //Pedido Sugerido
				          "pedido_producto" => $pedido_producto,
				          "pedido_producto_nombre" => $pedido_producto_nombre,
				          "precio_producto" => $precio_producto,
				          "iva_producto" => $iva_producto,
				          "cantidad_producto" => $cantidad_producto,
				          "subtotal_producto" => $subtotal_producto,
				          "total_pedido" => $total_pedido,
				        );

			    		return response()->json($response_data);
			    break;
	    		case '7':
	    			# Obtener Razones sociales cliente
	    			$datos_fiscales = DatosFiscales::findOrFail($request->billing_info_id);

	    			$response_data = array(
			          "persona" => $datos_fiscales->persona,
			          "razon_social" => $datos_fiscales->razon_social,
			          "representante_legal" => $datos_fiscales->representante_legal,
			          "rfc" => $datos_fiscales->rfc,
			          "calle" => $datos_fiscales->calle,
			          "numero_interior" => $datos_fiscales->numero_interior,
			          "numero_exterior" => $datos_fiscales->numero_exterior,
			          "colonia" => $datos_fiscales->colonia,
			          "codigo_postal" => $datos_fiscales->codigo_postal,
			          "municipio" => $datos_fiscales->municipio,
			          "estado" => $datos_fiscales->estado,
			          "pais" => $datos_fiscales->pais,
			        );

		    		return response()->json($response_data);
	    	}
    	}
    }

    public function ajaxConservadores(Request $request) {

    	if($request->ajax()){
    		switch ($request->type) {
	    		case '1':
		    		#Obtener Producto, Precio y Capacidad segun modelo

		    		$modelo_conservador =  ModeloConservador::findOrFail($request->modelo_id);
	    			$response_data = array();
	    			$response_data = [
			          "id" => $modelo_conservador->producto_id,
			          "nombre" => $modelo_conservador->producto->nombre,
			          "capacidad" => $modelo_conservador->capacidad,
			          "precio_venta" => $modelo_conservador->producto->precio_venta
				    ];

		    		return response()->json($response_data);

	    		break;

	    		case '2':
	    		#Cambiar Status Conservador
	    			DB::beginTransaction();
		    		$conservador =  Conservador::findOrFail($request->conservador_id);

		    		switch ($request->status) {
		    			case 'stand_by':
		    				# Obtener ultimo status del log
		    				break;

		    			case 'en_reparacion':
		    				# Obtener ultimo status del log
		    				break;

		    			case 'fuera_servicio':
		    				# Obtener ultimo status del log
		    				break;

		    			case 'reactivar':
		    				# Obtener ultimo status del log
		    				$conservador->status = 'stand_by';
			                $conservador->cliente_id = NULL;
			                $conservador->cliente_establecimiento_id = NULL;
			                $log_conservador = new LogConservador;
					        $log_conservador->conservador_id = $request->conservador_id;
					        $log_conservador->user_id = Auth::user()->id;
					        $log_conservador->tipo = 'status';
					        $log_conservador->comentarios = 'Se re-activó el conservador.';

					        $conservador->update();
		    				$conservador->restore();
					        $log_conservador->save();
		    				break;

		    			case 'delete':
		    				# Obtener ultimo status del log

		    				$conservador->status = 'fuera_servicio';
			                $conservador->cliente_id = NULL;
			                $conservador->cliente_establecimiento_id = NULL;
			                $log_conservador = new LogConservador;
					        $log_conservador->conservador_id = $request->conservador_id;
					        $log_conservador->user_id = Auth::user()->id;
					        $log_conservador->tipo = 'status';
					        $log_conservador->comentarios = 'Se dió de baja el Conservador.';

					        $conservador->update();
					        $conservador->delete();
					        $log_conservador->save();
		    				break;
		    		}
		    		DB::commit();
	    		break;

	    		case '3':
	    			# Desconsignar o Retirar Conservador

	    			DB::beginTransaction();
	    			$conservador =  Conservador::findOrFail($request->conservador_id);

	    			$establecimiento_id = $conservador->cliente_establecimiento_id;
                	$producto_id = $conservador->producto_id;

			        $conservador->cliente_id = NULL;
			        $conservador->cliente_establecimiento_id = NULL;
			        $conservador->status = 'stand_by';
					$conservador->updated_by = Auth::user()->id;
		    		$conservador->update();

		    		$establecimiento = Establecimiento::findOrFail($establecimiento_id);
	                $productos_asociados = collect($establecimiento->productos_disponibles());

	                if( !$productos_asociados->contains($producto_id) ) {
	                    #Borrar
	                    DB::table('cliente_establecimiento_precios')->where([
	                        ['establecimiento_id', $establecimiento_id],
	                        ['producto_id', $producto_id]
	                    ])->delete();
	                }

	                #Actualizar Puntos de inventario
			        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
			        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
			        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
			        $establecimiento->update();

		    		DB::commit();

	    			break;
	    	}
    	}

    }

    public function ajaxInventario(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
    			# Obtener productos para corte
    			$establecimiento =  Establecimiento::findOrFail($request->establecimiento_id);
    			$productos_array = $establecimiento->productos_disponibles();

    			$response_data = '';
    			foreach($productos_array as $producto_id) {
    				$producto = Producto::withTrashed()->findOrFail($producto_id);
					$response_data .= '<tr>
                        <td class="col-xs-2">'.$producto->id_codigo.'<input type="hidden" name="producto_corte[]" value="'.$producto->id.'"/></td>
                        <td class="col-xs-6">'.$producto->nombre.'</td>
                        <td class="col-xs-4"><div class="input-group" style="width: 100%;"><span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span><input class="form-control positive-integer capproducto" onblur="calcularCapacidadCorte(this.value)" name="cantidad_actual_corte[]" type="text"></div></td>
                    </tr>';
				}
				return $response_data;

    			break;
    		}
    	}
    }

    public function ajaxPedidos(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
	    			# Cancelar pedido
	    			$pedido =  Pedido::findOrFail($request->pedido_id);

	    			if ($pedido->status == 1 || $pedido->status == 2 ) {

		                foreach ($pedido->productos as $producto_pedido) {
	                        # Para cada producto del pedido hacer movimiento inventario
	                        if ($pedido->status == 2) {
	                        	$pedido->registrarMovimientoInventario('entrada', 'devolucion', $producto_pedido->id);
	                        }
	                    }

	                    $pedido->status = 5;
		                $pedido->status_cobranza = 4;
		                $pedido->status_facturacion = 4;
		                // Registro de Log
		                $log_usuario = new LogUsuario;
		                $log_usuario->user_id = Auth::user()->id;
		                $log_usuario->tipo = 'pedido';
		                $log_usuario->comentarios = 'El usuario ha cancelado el Pedido #<b>'.$request->pedido_id.'</b>';

				        $pedido->update();
				        $log_usuario->save();
	    			}

    			break;
    			case '2':
    				# Eliminar producto del pedido
	    			$pedido =  Pedido::findOrFail($request->pedido_id);
	    			$producto_pedido = PedidoProductos::findOrFail($request->producto_pedido_id);
					$producto_pedido->delete();
	        		$pedido->updateTotales(); # Actualizar Total del pedido
    			break;

    			case '3':
	    			# Registrar Devolución pedido
	    			$pedido =  Pedido::findOrFail($request->pedido_id);

	    			if ($pedido->status == 3) {
	                    /*
	                        - Se da entrada como devolución
	                        - Se da salida como merma
	                        - Status Cobranza: incobrable
	                        - Status Facturacion: infacturable
	                    */
	                    #Aqui se hace movimientos de inventario

	                        # Pasa de Entregado a Devolucion

	                    foreach ($pedido->productos as $producto_pedido) {
	                        $pedido->registrarMovimientoInventario('entrada', 'devolucion', $producto_pedido->id);
	                    }

	                    foreach ($pedido->productos as $producto_pedido) {
	                        $pedido->registrarMovimientoInventario('salida', 'merma', $producto_pedido->id);
	                    }

	                    $pedido->status = 4;
		                $pedido->status_cobranza = 4;
		                $pedido->status_facturacion = 4;
		                // Registro de Log
		                $log_usuario = new LogUsuario;
		                $log_usuario->user_id = Auth::user()->id;
		                $log_usuario->tipo = 'pedido';
		                $log_usuario->comentarios = 'El usuario ha marcado devolución del Pedido #<b>'.$request->pedido_id.'</b>';

				        $pedido->update();
				        $log_usuario->save();

	    			}
    			break;
    		}
    	}
    }

    public function ajaxCobranza(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
    			# Marcar como incobrable
    			$pedido =  Pedido::findOrFail($request->pedido_id);

    			$pedido->incobrable = 1;
                // Registro de Log
                $log_usuario = new LogUsuario;
                $log_usuario->user_id = Auth::user()->id;
                $log_usuario->tipo = 'pedido';
                $log_usuario->comentarios = 'El usuario ha marcado el Pedido #'.$request->pedido_id.' como <b>INCOBRABLE</b>';

		        $pedido->update();
		        $log_usuario->save();

    			break;
    		}
    	}
    }

    public function ajaxDashboard(Request $request) {
    	if ($request->ajax()) {
    		switch ($request->type) {
    			case '1':
    				# Modal Detalle Pedidos

    				switch ($request->status) {
    					case '1':
    						# Programados y En Camino
    						$pedido = Pedido::findOrFail($request->pedido_id);
    						switch ($pedido->status) {
		            			case '1':
		            				$status_pedido='<span class="label label-primary" style="font-size: 1em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
		            				break;
		            			case '2':
		            				$status_pedido='<span class="label label-info" style="font-size: 1em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
		            				break;
		            			case '3':
		            				$status_pedido='<span class="label label-success" style="font-size: 1em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
		            				break;
		            			case '4':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
		            				break;
		            			case '5':
		            				$status_pedido='<span class="label label-danger" style="font-size: 1em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
		            				break;
		            			default:
		            				$status_pedido='<span class="label label-default" style="font-size: 1em; display:inline-block;">N.D.</span>';
		            				break;
		            		}


    						$response='';

    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Pedido: </strong>
								    <h3 class="text-info">#'.$pedido->id.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Status:</strong> <BR/>
								    <h3 class="text-info">'.$status_pedido.'</h3>
								</div>
							</div>';


    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Establecimiento: </strong>
								    <h3 class="text-info">'.$pedido->cliente->nombre_corto.'<b>'.$pedido->establecimiento->sucursal.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Fecha Entrega:</strong> <BR/>
								    <h3 class="text-info">'.fecha_to_human($pedido->fecha_entrega,false).'</h3>
								</div>
							</div>';


							$response.='<div class="row m-t-sm">
						        <div class="col-xs-12">

							        <table class="table table-striped" id="tablaCorte">
			                            <thead>
			                            <tr>
			                            	<th class="col-xs-8"><h4 class="text-success">Producto</h4></th>
			                                <th class="col-xs-4"><h4 class="text-success">Cantidad Solicitada</h4></th>
			                            </tr>
			                            </thead>
										<tbody>';

						    foreach ($pedido->productos as $pedido_producto) {
						    	$response.='<tr>
			                              <td class="col-xs-8">
			                                  <h4 class=" text-danger">'.$pedido_producto->producto->nombre.'</h4>
			                              </td>
			                              <td class="col-xs-4">
			                                  <h4 class="text-center text-danger">'.$pedido_producto->cantidad_requerida.' bolsas</h4>
			                              </td></tr>';
						    }
				            		
						    $response.='</tbody></table></div></div>';

						    $response.='<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
								  <strong>Chofer Asignado: </strong>
								    <h3 class="text-info">'.$pedido->chofer->nombre.'</h3>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								  <strong>Zona de Entrega: </strong>
								    <h3 class="text-info">'.$pedido->establecimiento->zona->nombre.'</h3>
								</div>
					      	</div>';

    						return $response;
    						break;

    						case '3':
    						# Entregados
    						$pedido = Pedido::findOrFail($request->pedido_id);
    						switch ($pedido->status) {
		            			case '1':
		            				$status_pedido='<span class="label label-primary" style="font-size: 1em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
		            				break;
		            			case '2':
		            				$status_pedido='<span class="label label-info" style="font-size: 1em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
		            				break;
		            			case '3':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
		            				break;
		            			case '4':
		            				$status_pedido='<span class="label label-warning" style="font-size: 1em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
		            				break;
		            			case '5':
		            				$status_pedido='<span class="label label-danger" style="font-size: 1em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
		            				break;
		            			default:
		            				$status_pedido='<span class="label label-default" style="font-size: 1em; display:inline-block;">N.D.</span>';
		            				break;
		            		}


    						$response='';

    						$response.='<div class="row">
								<div class="col-xs-4">
									<strong>Pedido: </strong>
								    <h3 class="text-info">#'.$pedido->id.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Status:</strong> <BR/>
								    <h3 class="text-info">'.$status_pedido.'</h3>
								</div>

								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								  <strong>Chofer Asignado: </strong>
								    <h3 class="text-info">'.$pedido->chofer->nombre.'</h3>
								</div>
							</div>';


    						$response.='<div class="row">
								<div class="col-xs-8">
									<strong>Establecimiento: </strong>
								    <h3 class="text-info">'.$pedido->cliente->nombre_corto.'<b>'.$pedido->establecimiento->sucursal.'</b></h3>
								</div>

								<div class="col-xs-4">
								    <strong>Hora de Entrega:</strong> <BR/>
								    <h3 class="text-info">'.Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p').'</h3>
								</div>
							</div>';


							$response.='<div class="row m-t-sm">
						        <div class="col-xs-12">

							        <table class="table table-striped" id="tablaCorte">
			                            <thead>
			                            <tr>
			                            	<th class="col-xs-6"><h4 class="text-success">Producto</h4></th>
			                                <th class="col-xs-3"><h4 class="text-success">Solicitado</h4></th>
			                                <th class="col-xs-3"><h4 class="text-success">Entregado</h4></th>
			                            </tr>
			                            </thead>
										<tbody>';

						    foreach ($pedido->productos as $pedido_producto) {
						    	$response.='<tr>
			                              <td class="col-xs-6">
			                                  <h4>'.$pedido_producto->producto->nombre.'</h4>
			                              </td>
			                              <td class="col-xs-3">
			                                  <h4>'.$pedido_producto->cantidad_requerida.' bolsas</h4>
			                              </td>
			                              <td class="col-xs-3">
			                                  <h4 class="text-danger">'.$pedido_producto->cantidad_recibida.' bolsas</h4>
			                              </td>
			                              </tr>';
						    }
				            		
						    $response.='</tbody></table></div></div>';

						    if($pedido->firmado_otro != null)
				                $firmado_por = $pedido->firmado_otro;
				            else
                				$firmado_por = $pedido->cliente_firma->nombre.' '.$pedido->cliente_firma->apellido_paterno.' '.$pedido->cliente_firma->apellido_materno;

						    $response.='<div class="row">

								<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
								  <strong>Firmado por: </strong>
								    <h3 class="text-info">'.$firmado_por.'</h3>
								</div>';

					      	$response.='<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8 text-center">
								  <strong>Firma: </strong> <BR/>';
					              if($pedido->firma_path != null)
					                $response.='<img src="'.url($pedido->firma_path).'" />';
					              else
					                $response.='<div class="product-imitation">[ N.D. ]</div>';
					        $response.='</div></div>';

    						return $response;
    						break;
    				}

    			break;

    			case '2':
    				# Enviar Pedidos a Ruta
    			
    				$array_pedidos = $request->pedidos_ids;
    				for ($i=0; $i < count($array_pedidos); $i++) { 
    					$pedido = Pedido::findOrFail($array_pedidos[$i]);

    					foreach ($pedido->productos as $producto_pedido) {
                            # Para cada producto del pedido hacer movimiento inventario
                            if ($producto_pedido->precio_unitario == 0.00)
                                $pedido->registrarMovimientoInventario('salida', 'cortesia', $producto_pedido->id);
                            else
                                $pedido->registrarMovimientoInventario('salida', 'pedido', $producto_pedido->id);
                        }

    					$pedido->status = 2;

    					#TBD - Envío notificacion por e-mail al cliente

				        $pedido->update();
    				}

    			break;

    			case '3':
    				# Cargar Choferes Pedido
					$pedido = Pedido::findOrFail($request->pedido_id);
					$choferes = DB::table('users')
                        ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
                        ->where('user_has_role.user_role_id', '=', 4)
                        ->where('users.deleted_at', NULL)
                        ->get();

                    $choferes_array = $choferes->pluck('nombre', 'id')->toArray();

					$response='';
					$response.='<div class="row">
						<div class="col-xs-2">
							<strong>Pedido: </strong>
						    <h3 class="text-info">#'.$pedido->id.'</b></h3>
						</div>

						<div class="col-xs-5">
						    <strong>Establecimiento:</strong> <BR/>
							<h3 class="text-info">'.$pedido->cliente->nombre_corto.' <b>'.$pedido->establecimiento->sucursal.'</b></h3>
						</div>

						<div class="col-xs-5">
						    <strong>Seleccione Chofer:</strong> <BR/>';
						    $response.='
						    <input type="hidden" id="pedido_id_cambiar_chofer" name="pedido_id_cambiar_chofer" value="'.$pedido->id.'">
							<select name="chofer_cambiar" id="chofer_cambiar" class="form-control selectdp">';

							foreach ($choferes_array as $key => $value) {
								if($key == $pedido->chofer_id)
									$response.='<option selected="" value="'.$key.'">'.$value.'</option>';
								else
									$response.='<option value="'.$key.'">'.$value.'</option>';
							}

						$response.='</select></div>
					</div>';

					return $response;
    			break;

    			case '4':
    				# Cambiar Chofer Pedido
					$pedido = Pedido::findOrFail($request->pedido_id);
					
					$pedido->chofer_id = $request->chofer_id;
			        $pedido->update();
    			break;
    		}
    	}
    }
}
