<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Pedido as Pedido;
use App\Models\Pago as Pago;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\PedidoFacturas as PedidoFacturas;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\Conservador as Conservador;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Producto as Producto;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\LogUsuario as LogUsuario;
use App\Models\LogInventario as LogInventario;
use App\Models\Corte as Corte;
use DB;
use Input;
use Validator;
use Auth;
use Gate;
use File;
use Carbon\Carbon;

class CobranzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

		$q = Pedido::query();

		//$q->where('status_facturacion','>',1)->where('status_cobranza', '<', 4);

        // FILTRO CLIENTE
		if (Input::has('f_cliente'))
		{
			$q->where('cliente_id', Input::get('f_cliente'));
		}

        // FILTRO STATUS COBRANZA
        if (Input::has('f_estatus_cob') && Input::get('f_estatus_cob') != 'todos')
        {
            $q->where('status_cobranza', Input::get('f_estatus_cob'));
        }
		
        // FILTRO STATUS FACTURACION
        if (Input::has('f_estatus_fact') && Input::get('f_estatus_fact') != 'todos')
        {
            $q->where('status_facturacion', Input::get('f_estatus_fact'));
        }
		
		// SORT ORDER
		$sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

		// SORT VARIABLE
		$orderByString = "";
		switch(Input::get('orderBy')){
			case 'PEDIDO' : $orderByString = 'id '.$sort_aux;			
				break;

			case 'ESTATUSF' : $orderByString = 'status_facturacion '.$sort_aux;
                break;

            case 'ESTATUSC' : $orderByString = 'status_cobranza '.$sort_aux;
                break;

            case 'ENTREGADO' : $orderByString = 'entregado_at '.$sort_aux; 
                break;

            case 'TOTAL' : $orderByString = 'total '.$sort_aux;
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux;            
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE pedido.created_by = users.id) '.$sort_aux;    
                break;

            case 'UPDATEDAT' : $orderByString = 'updated_at '.$sort_aux;            
                break;

            case 'UPDATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE pedido.updated_by = users.id) '.$sort_aux;
                break;

			default : $orderByString = 'status_cobranza ASC';			
				break;
		}
		
		if($orderByString != ""){
			$q->orderByRaw($orderByString);
		}

        //FILTRO FECHAS

        if (Input::has('fecha_ini') != '' && Input::has('fecha_fin') != '') {
            $fecha_ini = (Input::has('fecha_ini') ? Input::get('fecha_ini') : Carbon::parse('first day of this month')->format('Y-m-d'));
            $fecha_fin = (Input::has('fecha_fin') ? Input::get('fecha_fin') : Carbon::parse('last day of this month')->format('Y-m-d'));

            $q->whereRaw('DATE(entregado_at) BETWEEN ? AND ?', ['fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin] );
        }

        // FILTRO SALDO VENCIDO

        //
        if (Input::has('f_saldo_vencido') && Input::get('f_saldo_vencido') == '1')
        {

            $pedidos_aux = $q->get();
            $pedidos_vencidos_array = [];
            foreach ($pedidos_aux as $pedido) {
                if($pedido->por_cobrar() > 0.00)
                    $pedidos_vencidos_array[] = $pedido->id;
            }

            $q->whereIn('id', $pedidos_vencidos_array);
        }
		
		// NUM RESULTADOS
		if (Input::has('f_num_resultados'))
		{

			$num_resultados = 0 + ((int) Input::get('f_num_resultados') );
			$data['pedidos'] = $q->paginate($num_resultados);
			
			if( count($data['pedidos']) == 0 ){
				$data['pedidos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
			}
		}else{
			$data['pedidos'] = $q->paginate(25);
		}

        $q2 = Pedido::query();
        $q3 = Pedido::query();
        $q4 = Pedido::query();
        $q2->where('status',3)->where('status_facturacion', 1)->where('incobrable', 0);
        $q3->where('status',3)->where('status_cobranza', '<', 3)->where('incobrable', 0);

        // FILTRO CLIENTE
        if (Input::has('f_cliente'))
        {
            $q2->where('cliente_id', Input::get('f_cliente'));
            $q3->where('cliente_id', Input::get('f_cliente'));
            $q4->where('cliente_id', Input::get('f_cliente'));
        }


        

        $sum_no_facturados=0.0;
        foreach ($q2->get() as $pedido) {
            $pendiente_facturar = $pedido->total - $pedido->facturas->sum('total');
            $sum_no_facturados+=$pendiente_facturar;
        }

        $sum_saldo_pendiente=0.0;
        foreach ($q3->get() as $pedido) {
            $pendiente_cobrar = $pedido->total - $pedido->pagos->sum('monto');
            $sum_saldo_pendiente+=$pendiente_cobrar;
        }

        $sum_saldo_vencido=0.0;
        foreach ($q4->get() as $pedido) {
            $sum_saldo_vencido+=floatval($pedido->por_cobrar());
        }

        $data['total_no_facturado'] = $sum_no_facturados;
        $data['total_por_cobrar'] = $sum_saldo_pendiente;
        $data['total_vencido'] = $sum_saldo_vencido;
        
		
		return view('sistema.cobranza.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $pedido = Pedido::findOrFail($id);

        return view('sistema.cobranza.show', ['pedido' => $pedido]);
    }

    public function storePagoPedido(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_pago_pedido'          => 'required|date_format:d/m/Y',
            'forma_pago_pedido'          => 'required|exists:forma_pago,id',
            'monto_pago_pedido'          => 'required|numeric',
            'comprobante_pago_pedido'    => 'mimes:pdf,xls,xlsx,doc,docx,jpg,jpeg,png,ppt,pptx',
            'comentarios_pago_pedido'    => '',
        ],[
            'fecha_pago_pedido.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_pago_create', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);

        $pago = new Pago();
        $pago->pedido_id = $pedido->id;
        $pago->forma_pago_id = $request->forma_pago_pedido;
        $fecha_pago = Carbon::createFromFormat('d/m/Y', $request->fecha_pago_pedido)->formatLocalized('%Y-%m-%d');
        $pago->fecha_pago = $fecha_pago;
        $pago->monto = $request->monto_pago_pedido;
        $pago->comentarios = $request->comentarios_pago_pedido;
        $pago->created_by = Auth::user()->id;
        $pago->updated_by = Auth::user()->id;

        $pago = $pedido->pagos()->save($pago);

        $comprobante = $request->file('comprobante_pago_pedido');
        
        if($comprobante != null){
            
            $file_name = time() . $comprobante->getClientOriginalName();
                                        
            $file_path = 'uploads/pagos/'.$id.'/';
    
            $comprobante->move($file_path, $file_name);
    
            $pago->comprobante_path = $file_path . $file_name;
    
            $pago->update();
        }


        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();


        DB::commit();

        return redirect()->intended('/cobranza/'.$pedido->id)->with('pago_pedido_success', true);
    }

    public function storeFacturaPedido(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_facturacion'          => 'required|date_format:d/m/Y',
            'folio'                      => 'required',
            'subtotal_factura'          => 'required|numeric',
            'iva_factura'               => 'required|numeric',
            'total_factura'          => 'required|numeric',
            'pdf_factura'    => 'mimes:pdf',
            'xml_factura'    => 'mimes:xml',
            'comentarios_factura'    => '',
        ],[
            'fecha_facturacion.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_factura_create', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);

        $factura = new PedidoFacturas();
        $factura->pedido_id = $pedido->id;
        $factura->cliente_id = $pedido->cliente_id;
        $factura->establecimiento_id = $pedido->cliente_establecimiento_id;
        $factura->folio = $request->folio;

        $fecha_facturacion = Carbon::createFromFormat('d/m/Y', $request->fecha_facturacion);
        $fecha_vencimiento = (clone $fecha_facturacion);
        $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias); 

        $factura->fecha_facturacion = $fecha_facturacion->formatLocalized('%Y-%m-%d');
        $factura->fecha_vencimiento = $fecha_vencimiento->formatLocalized('%Y-%m-%d');

        $factura->subtotal = $request->subtotal_factura;
        $factura->iva = $request->iva_factura;
        $factura->total = $request->total_factura;
        $factura->comentarios = $request->comentarios_factura;
        $factura->created_by = Auth::user()->id;
        $factura->updated_by = Auth::user()->id;
        $factura = $pedido->facturas()->save($factura);

        $pdf = $request->file('pdf_factura');
        if($pdf != null){
            $file_name = time() . $pdf->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/pdf/';
            $pdf->move($file_path, $file_name);
            $factura->pdf_path = $file_path . $file_name;
            $factura->update();
        }

        $xml = $request->file('xml_factura');
        if($xml != null){
            $file_name = time() . $xml->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/xml/';
            $xml->move($file_path, $file_name);
            $factura->xml_path = $file_path . $file_name;
            $factura->update();
        }

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();


        DB::commit();

        return redirect()->intended('/cobranza/'.$pedido->id)->with('factura_pedido_success', true);
    }

    public function updatePagoPedido(Request $request, $id, $pago_id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_pago_pedido_edit'          => 'date_format:d/m/Y',
            'forma_pago_pedido_edit'          => 'required|exists:forma_pago,id',
            'monto_pago_pedido_edit'          => 'required|numeric',
            'archivo_orden_edit'        => 'mimes:pdf,xls,xlsx,doc,docx,jpg,jpeg,png,ppt,pptx',
            'comentarios_pago_pedido_edit'    => '',
        ],[
            'fecha_pago_pedido_edit.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);
                

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_pago_edit', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);
        $pago =  Pago::findOrFail($pago_id);

        $fecha_pago = Carbon::createFromFormat('d/m/Y', $request->fecha_pago_pedido_edit)->formatLocalized('%Y-%m-%d');
        $pago->fecha_pago = $fecha_pago;
        $pago->forma_pago_id = $request->forma_pago_pedido_edit;
        $pago->monto = $request->monto_pago_pedido_edit;
        $pago->comentarios = $request->comentarios_pago_pedido_edit;
        $pago->updated_by = Auth::user()->id;

        $comprobante = $request->file('comprobante_pago_pedido_edit');
        
        if($comprobante != null){
            
            $file_name = time() . $comprobante->getClientOriginalName();
                                        
            $file_path = 'uploads/pagos/'.$id.'/';
    
            $comprobante->move($file_path, $file_name);
    
            $pago->comprobante_path = $file_path . $file_name;
    
            $pago->update();
        }

        $pago->update();

        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();
                
        DB::commit();

        return redirect()->intended('/cobranza/' . $pedido->id)->with('pago_pedido_update_success', true);
    }

    public function updateFacturaPedido(Request $request, $id, $factura_id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_facturacion_edit'          => 'required|date_format:d/m/Y',
            'folio_edit'                      => 'required',
            'subtotal_factura_edit'          => 'required|numeric',
            'iva_factura_edit'               => 'required|numeric',
            'total_factura_edit'          => 'required|numeric',
            'pdf_factura_edit'    => 'mimes:pdf',
            'xml_factura_edit'    => 'mimes:xml',
            'comentarios_factura_edit'    => '',
        ],[
            'fecha_facturacion_edit.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_factura_edit', true);
        }
        
                
        DB::beginTransaction();

        $pedido = Pedido::findOrFail($id);
        $factura = PedidoFacturas::where('id', $factura_id)->where('pedido_id', $id)->first();

        $fecha_facturacion = Carbon::createFromFormat('d/m/Y', $request->fecha_facturacion_edit);
        $fecha_vencimiento = (clone $fecha_facturacion);
        $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias); 

        $factura->fecha_facturacion = $fecha_facturacion->formatLocalized('%Y-%m-%d');
        $factura->fecha_vencimiento = $fecha_vencimiento->formatLocalized('%Y-%m-%d');

        $factura->folio = $request->folio_edit;
        $factura->subtotal = $request->subtotal_factura_edit;
        $factura->iva = $request->iva_factura_edit;
        $factura->total = $request->total_factura_edit;
        $factura->comentarios = $request->comentarios_factura_edit;
        $factura->updated_by = Auth::user()->id;
        $factura->update();

        $pdf = $request->file('pdf_factura_edit');
        if($pdf != null){
            $file_name = time() . $pdf->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/pdf/';
            $pdf->move($file_path, $file_name);
            $factura->pdf_path = $file_path . $file_name;
            $factura->update();
        }

        $xml = $request->file('xml_factura_edit');
        if($xml != null){
            $file_name = time() . $xml->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/xml/';
            $xml->move($file_path, $file_name);
            $factura->xml_path = $file_path . $file_name;
            $factura->update();
        }
        

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();
                
        DB::commit();

        return redirect()->intended('/cobranza/' . $pedido->id)->with('factura_pedido_update_success', true);
    }
    
    public function cambiarStatusFacturacion(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
            'status_facturacion'   => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cambiar_status_facturacion', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);
        $pedido->status_facturacion = $request->status_facturacion;

        $pedido->update();

        DB::commit();

        return redirect()->intended('/cobranza/' . $id)->with('show_tab_cobranza', true);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPagoPedido($pedido_id, $pago_id)
    {
        $pago = Pago::findOrFail($pago_id);
        $pedido = Pedido::findOrFail($pago->pedido_id);
        
        DB::beginTransaction();
                
        $pago->delete();

        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();
        
        DB::commit();
        
        return redirect()->intended('/cobranza/' . $pedido_id)->with('pego_pedido_delete_success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFacturaPedido($pedido_id, $factura_id)
    {
        $factura = PedidoFacturas::findOrFail($factura_id);
        $pedido = Pedido::findOrFail($factura->pedido_id);
        
        DB::beginTransaction();
                
        $factura->delete();

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();
        
        DB::commit();
        
        return redirect()->intended('/cobranza/' . $pedido_id)->with('factura_pedido_delete_success', true);
    }
}
