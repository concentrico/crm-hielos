<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use App\Models\Parametro as System;

class SessionController extends Controller {

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    
     /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function showLoginForm() {

	    if(Auth::check()){
            return redirect()->intended('/');
	    }else{
			return view('sistema.login');
	    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request) {

		$messages = [
		    'inputUsername.required' 		=> 'El usuario es requerido.',
		    'inputPassword.required' 	=> 'El password es requerido.',
		];

		$this->validate($request, [
		    'inputUsername' => 'required',
		    'inputPassword' => 'required',
		], $messages);
        //guard('sistema')->
        if (Auth::attempt(['username' => $request->inputUsername, 'password' => $request->inputPassword, 'tipo' => 'sistema'], $request->has('remember') )) {
            return redirect()->intended('/');
        } elseif (Auth::attempt(['email'=> $request->inputUsername, 'password' => $request->inputPassword, 'tipo' => 'sistema'], $request->has('remember')) ) {
            return redirect()->intended('/');
        } else {
            $loginFailed = [
                'loginFailed' => 'Usuario o contraseña son incorrectos',
            ];
            return redirect()->back()->withInput()->withErrors($loginFailed);
        }

    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('sistema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function logout() {
		Auth::logout();
        return redirect()->intended('/login');
    }
}
