<?php
namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
Use App\Models\Chofer as Chofer;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\ClienteContacto as Contacto;
Use App\User as User;
Use App\Models\Pedido as Pedido;
Use App\Models\Producto as Producto;
Use App\Models\PedidoProductos as PedidoProductos;
use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\Corte as Corte;
use Carbon\Carbon;

use Auth;
use DB;
use Hash;
use Gate;
use Input;
use Mail;
use Validator;  

class ChoferesController extends Controller {

    public function index() {
        if (Auth::user()->can('manage-users') || Auth::user()->can('manage-distribucion') ){
            return $this->indexAdmin();
        } else if (Auth::user()->can('manage-choferes')){
            return $this->indexChofer();
        } else {
            $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
    }

    public function indexChofer(){ 

       if (Gate::denies('manage-choferes')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
                return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $messag]);
        }

        $q = Pedido::query();
        $q2 = Pedido::query();
        $q ->where('chofer_id','=',Auth::user()->id)->get();
        $q2 ->where('chofer_id','=',Auth::user()->id)->get();
        $q ->where('status',2)->get();
        $q2 ->where('status',3)->get();
        #Fecha hoy sin contar horas
        $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
        $q2 ->where('entregado_at','>=',  $fecha_hoy)->get();

        $data['pedidos'] = $q->paginate(500);
        $data['entregados'] = $q2->paginate(500);
            
        return view('sistema.choferes.index', $data);
    } 

    public function indexAdmin()
    {

        $q = User::query();

        $q->whereRaw('((SELECT COUNT(user_id) FROM user_has_role WHERE user_id = users.id AND user_role_id = 4) > 0)');
    
        if (Input::has('f_nombre'))
        {
          $q->whereRaw("( nombre LIKE '%". Input::get('f_nombre') ."%'
            OR username LIKE '%". Input::get('f_nombre') ."%' OR email LIKE '%". Input::get('f_nombre') ."%' )");
        }

        if (Input::has('f_estatus'))
        {
          if(Input::get('f_estatus') == 'todos'){
            $q->withTrashed();
          }else if(Input::get('f_estatus') == 'suspendido'){
            $q->onlyTrashed();
          }
        }

        if (Input::has('orderBy') && Input::has('sort')){

        $sort_aux = ( Input::get('sort') == 'ASC' ? 'ASC' : 'DESC');
          
        $orderByString = "";
          switch(Input::get('orderBy')){
            case 'ID' : 
              $orderByString = 'id '.$sort_aux;
              break;
            case 'NOMBRE' : 
              $orderByString = 'nombre '.$sort_aux;
              break;
            case 'USERNAME' : 
              $orderByString = 'username '.$sort_aux;
              break;
            case 'EMAIL' : 
              $orderByString = 'email '.$sort_aux;
              break;
            case 'CELPHONE' : 
              $orderByString = 'celular '.$sort_aux;
              break;
            default : 
              $orderByString = "";
              break;
          }
          
          if($orderByString != ""){
            $q->orderByRaw($orderByString); 
          }
        }else{
          $q->orderByRaw('id');
        }

        $data['usuarios'] = $q->paginate(10, ['*'], 'usuarios_page');
        return view('sistema.choferes.admin.index', $data);

    }

    public function show($id)
    {
        if (Auth::user()->can('manage-users') || Auth::user()->can('manage-distribucion') ){
            return $this->showAdmin($id);
        } else if (Auth::user()->can('manage-choferes')){
            return $this->showChofer($id);
        } else {
            $message = 'El usuario no tiene los permisos requeridos para accesar esta parte del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
    }

    public function showAdmin($id)
    {
        $data['usuario'] = User::findOrFail($id);
    
    return view('sistema.choferes.admin.show', $data);
    }


    //ESTA FUNCION MANDA A LOS PEDIDOS QUE ESTAN PROGRAMADOS
    public function showChofer($id)  { 
        $pedido = Pedido::withTrashed()->findOrFail($id);

        if($pedido->chofer_id == Auth::user()->id) {
            if($pedido->status == 1 || $pedido->status == 2 )
                return view('sistema.choferes.show.showPedidos', ['pedido' => $pedido]);
            else
                return redirect()->intended('choferes');
        } else {
            $message = 'No tiene acceso a éste pedido';
                return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
    }

    //ESTA FUNCION MANDA A LOS PEDIDOS QUE HAN SIDO ENTREGADO
    public function entregados($id)  { 
        $pedido = Pedido::withTrashed()->findOrFail($id);
            if($pedido->chofer_id == Auth::user()->id) {
                if($pedido->status == 3 ){
                    return view('sistema.choferes.show.showEntregas', ['pedido' => $pedido]);
                } else {
                    return view('sistema.choferes.index');
                }
            } else {
                $message = 'No tiene acceso a éste pedido';
                return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
            }
    }

    //ESTA FUNCION MANDA A LOS ESTABLECIMIENTOS EN EL DIA
    public function establecimientos($id)  { 
        $pedido = Pedido::withTrashed()->findOrFail($id);
            return view('sistema.choferes.show.showEstablecimientos', ['pedido' => $pedido]);
    }


    public function entregarPedido(Request $request, $pedido_id){

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        if ($request->firmado_por != 'otro') {
            $validator = Validator::make($request->all(), [
                'firmado_por'           => 'required',
                'image_data_uri'           => 'required'
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                'firmado_otro'           => 'required',
                'image_data_uri'           => 'required'
            ], $messages);
        }

        if($request->cantidad_recibida){
            $validator->each('cantidad_recibida', ['required']);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $arr_producto_id = [];
        if($request->id_pedido_producto){
            $arr_producto_id = $request->id_pedido_producto;
        }
        $arr_producto_cantidad = [];     
        if($request->cantidad_recibida){
            $arr_producto_cantidad = $request->cantidad_recibida;
        }
        
        DB::beginTransaction();
        $pedido = Pedido::findOrFail($pedido_id);

        $nombre = "pedido".$pedido_id.".jpg";

        $img_existente = "uploads/firmas/$nombre";
            if (file_exists($img_existente)) {

                mt_srand(time());
                $numero = mt_rand(0,10000);
                $aux = explode(".", $nombre);
                $tamano = sizeof($aux);
                $extension = $aux[$tamano-1];
                $pos=0;
                $nombre = "";

                while ($pos<$tamano-1) {
                    $nombre.=$aux[$pos];
                    $pos = $pos+1;
                }

                $nombre=$nombre.$numero.".".$extension;
                $archivo="uploads/firmas/".$nombre;
            }
            
            if($request->image_data_uri){
                $data_uri = $request->image_data_uri;
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);
                file_put_contents("uploads/firmas/$nombre", $decoded_image);

                $pedido->firma_path = 'uploads/firmas/'.$nombre;
            }
            //aqui termina el guardado de imagen

            if($request->firmado_otro){
                $pedido->firmado_otro = $request->firmado_otro; 
            } else {
                $pedido->firmado_por = $request->firmado_por; 
            }
            $cantidad_total=0;
            foreach($arr_producto_id AS $key => $value){
                $producto_pedido = PedidoProductos::findOrFail($arr_producto_id[$key]);
                $producto_pedido->cantidad_recibida = $arr_producto_cantidad[$key];
                $cantidad_total += $arr_producto_cantidad[$key];

                if ($producto_pedido->cantidad_requerida != $arr_producto_cantidad[$key]) {
                    
                    if ( $arr_producto_cantidad[$key] > $producto_pedido->cantidad_requerida) {
                        # Si es mayor la cantidad entregada (Salida Inventario - Pedido)
                        $cantidad_movimiento = $arr_producto_cantidad[$key] - $producto_pedido->cantidad_requerida;
                        if ($producto_pedido->precio_unitario == 0.00)
                            $this->registrarMovimientoInvInterno('cortesia', $producto_pedido->id, $cantidad_movimiento );
                        else
                            $this->registrarMovimientoInvInterno('pedido', $producto_pedido->id, $cantidad_movimiento );

                    } else if( $arr_producto_cantidad[$key] < $producto_pedido->cantidad_requerida){
                        # Si es menor la cantidad entregada (Entrada Inventario - Devolucion)
                        $cantidad_movimiento = $producto_pedido->cantidad_requerida-$arr_producto_cantidad[$key];
                        $this->registrarMovimientoInvInterno('devolucion', $producto_pedido->id, $cantidad_movimiento );
                    }
                    
                }
                $producto_pedido->update();
            }

        $pedido->status = 3;
        $pedido->entregado_at = Carbon::now();
        $pedido->update();
        $pedido->updateTotales(); //Actualizar el total del pedido en base a lo entregado

        /*ToDo: Enviar Remision para el Cliente*/
        $pedido->enviarRemision();

        DB::commit();

        return redirect()->intended('choferes')->with('entrega_pedido_success', true);
    }

    public function realizarCorte(Request $request, $pedido_id, $establecimiento_id) {
        if (!isset($request->cantidad_actual_corte) ) {
            $corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'cantidad_actual_corte.*'           => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = ClienteEstablecimiento::findOrFail($establecimiento_id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
                if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
                    $producto = Producto::findOrFail($request->producto_corte[$i]);
                    $productos_failed[] = $producto->nombre;
                }
        }

        if (count($productos_failed) > 0) {
            $corteFailed = [
                'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
            
            $cantidad_total=0;
            //INSERT PRODUCTS TO CORTE
            for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $cantidad_total+=intval($request->cantidad_actual_corte[$i]);

                $productos_corte = [
                    'establecimiento_id'    => $establecimiento_id,
                    'producto_id'           => $request->producto_corte[$i],
                    'cantidad_actual'       => $request->cantidad_actual_corte[$i],
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
                $corte = new Corte($productos_corte);
                $corte->save();
            }

            $establecimiento->capacidad_actual = $cantidad_total;

            if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
                $establecimiento->status_inventario = 0;
            } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 1;
            } else if ($cantidad_total > $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 2;
            }

            $establecimiento->update();
        } else {
            $corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
        
        DB::commit();
        
        return redirect()->intended('/choferes/entregas/'.$pedido_id)->with('corte_success', true);
    }

    public function registrarMovimientoInvInterno($tipo, $item_id, $cantidad) {

        DB::beginTransaction();
        $producto_pedido = PedidoProductos::findOrFail($item_id);
        $producto_id = $producto_pedido->producto_id;
        $inventario = Inventario::where('producto_id', $producto_id)->firstOrFail();

        switch ($tipo) {
            case 'pedido':
                $salida = [
                    'producto_id'           => $producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => 'pedido',
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

                $cant_actual = $inventario->cantidad_disponible;
                $cant_nueva = intval($cant_actual)-$cantidad;

                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $cantidad;
                $inv_salida = new \App\Models\InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);

                break;
            case 'cortesia':
                $salida = [
                    'producto_id'           => $producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => 'cortesia',
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

                $cant_actual = $inventario->cantidad_disponible;
                $cant_nueva = intval($cant_actual)-$cantidad;

                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $cantidad;
                $inv_salida = new \App\Models\InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);

                break;
            case 'devolucion':
                $entrada = [
                    'producto_id'           => $producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => 'devolucion',
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

                $inv_entrada = new \App\Models\InventarioEntradas($entrada);
                $inventario->entradas()->save($inv_entrada);

                $cant_actual = $inventario->cantidad_disponible;
                $cant_nueva = intval($cant_actual)+$cantidad;
                $inventario->cantidad_disponible = $cant_nueva;
                /*
                $log_text = 'Chofer registró movimiento <b>Devolución</b>. Cantidad devuelta Pedido #: '.$cantidad.'.';

                $log_inventario = new LogInventario;
                $log_inventario->inventario_id = $inventario->id;
                $log_inventario->entrada_id = $id;
                $log_inventario->user_id = Auth::user()->id;
                $log_inventario->log = $log_text;
                $log_inventario->comentarios = trim($request->comentarios);
                $log_inventario->save();
                */

                break;
        }

        $inventario->update();
        DB::commit();

        //return redirect()->intended('inventario')->with('registro_success', true);
    }

}