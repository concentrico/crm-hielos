<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\Conservador as Conservador;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Producto as Producto;
use App\Models\CuartoFrioProductos as CuartoFrioProductos;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\EstablecimientoPrecios as EstablecimientoPrecios;
use App\Models\LogUsuario as LogUsuario;
use App\Models\LogInventario as LogInventario;
use App\Models\Corte as Corte;
use DB;
use Input;
use Validator;
use Auth;
use Gate;
use Carbon\Carbon;

class InventarioController extends Controller
{
    public function index() {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $q = Inventario::query();

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

        // FILTRO BUSQUEDA
        if (Input::has('f_busqueda'))
        {
            $q->whereRaw("( (SELECT nombre FROM producto WHERE id = inventario.producto_id LIKE '%". Input::get('f_busqueda') ."%') OR (SELECT id_codigo FROM producto WHERE id = inventario.producto_id LIKE '%". Input::get('f_busqueda') ."%') )");
        }

        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'IDCODIGO' : 
                // FILTRO PRODUCTO NOMBRE
                $q->selectRaw('*, (SELECT id_codigo FROM producto WHERE producto_id = producto.id) AS id_codigo');     
                break;

            case 'PRODUCTO' : 
                // FILTRO PRODUCTO NOMBRE
                $q->selectRaw('*, (SELECT nombre FROM producto WHERE producto_id = producto.id) AS nombre');
                break;
            
            case 'CANTACTUAL' : $orderByString = 'cantidad_disponible '.$sort_aux;
                break;

            case 'CANTPRODUCIR' : $orderByString = 'cantidad_producir '.$sort_aux; 
                break;

            case 'ULTSALIDA' : $orderByString = 'ultima_salida '.$sort_aux;
                break;

            default : $orderByString = 'id '.$sort_aux;           
                break;
        }

        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['inventarios'] = $q->paginate($num_resultados);
            
            if( count($data['inventarios']) == 0 )
                $data['inventarios'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['inventarios'] = $q->paginate(10);
        }
        
        return view('sistema.inventario.index', $data);               
    }

    public function indexExterno() {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $q = ClienteEstablecimiento::query();

        // FILTRO BUSQUEDA
        if (Input::has('f_busqueda'))
        {
            $q->join('cliente', 'cliente_establecimiento.cliente_id', 'cliente.id')->select('cliente_establecimiento.*','cliente.nombre_corto as nombre_corto')->where('nombre_corto', 'like', '%'. Input::get('f_busqueda') .'%')->orWhere('sucursal', 'like', '%'. Input::get('f_busqueda') .'%')->get();

        }

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
        
        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'STATUS' : 
                if ($sort_aux == 'DESC') 
                    $orderByString = 'status_inventario DESC';
                else
                    $orderByString = '-status_inventario DESC';  
                break;

            case 'ESTABLECIMIENTO' : $orderByString = 'sucursal '.$sort_aux;  
                break;

            case 'CLIENTE' : $orderByString = '(SELECT nombre_corto FROM cliente WHERE cliente_id = cliente.id) '.$sort_aux;
                break;

            case 'PROXENTREGA' : 
                //
                break;

            case 'CANTACTUAL' : $orderByString = 'capacidad_actual '.$sort_aux;           
                break;

            default : $orderByString = '-status_inventario DESC';           
                break;
        }
        
        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['inventario_ext'] = $q->paginate($num_resultados);
            
            if( count($data['inventario_ext']) == 0 )
                $data['inventario_ext'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['inventario_ext'] = $q->paginate(25);
        }


        return view('sistema.inventario.index_externos', $data);

    }

    public function registrarMovimientoInvInterno(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        if($request->movimiento == 'ajuste'){
            $validator = Validator::make($request->all(), [
                'tipo_movimiento' => 'required',
                'movimiento' => 'required',
                'comentarios' => 'required',
                'cantidad' => 'required|numeric|min:1',
            ], $messages);
        } else{
            $validator = Validator::make($request->all(), [
                'tipo_movimiento' => 'required',
                'movimiento' => 'required',
                'cantidad' => 'required|numeric',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro', true);
        }

        DB::beginTransaction();
        $inventario = Inventario::findOrFail($id);

        if ($request->tipo_movimiento == 'entrada') {
            $entrada = [
                'producto_id'           => $inventario->producto_id,
                'cantidad_entrada'      => $request->cantidad,
                'motivo'                => $request->movimiento,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];

            $inv_entrada = new \App\Models\InventarioEntradas($entrada);
            $inventario->entradas()->save($inv_entrada);

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)+$request->cantidad;
            $inventario->cantidad_disponible = $cant_nueva;

            if ($request->movimiento == 'produccion') {
                // Registro de Log
                $log_usuario = new LogUsuario;
                $log_usuario->user_id = Auth::user()->id;
                $log_usuario->tipo = 'produccion';
                $log_usuario->comentarios = 'Se registraron '.$request->cantidad.' entradas de producción. Inventario Actual: <b>'.$cant_nueva.'</b>';
                $log_usuario->save();
            }

        } else if($request->tipo_movimiento == 'salida') {
            $salida = [
                'producto_id'           => $inventario->producto_id,
                'cantidad_salida'       => $request->cantidad,
                'motivo'                => $request->movimiento,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)-$request->cantidad;

            if ($cant_nueva > 0) {
                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $request->cantidad;
                $inv_salida = new \App\Models\InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);
            } else {
                $registroFailed = [
                    'registroFailed' => 'No hay cantidad disponible para la salida registrada.',
                ];
                return back()->withErrors($registroFailed)->withInput()->with('show_modal_registro', true);
            }
        }

        if($request->movimiento == 'ajuste') {
            // Registro de Log
            $log_inventario = new LogInventario;
            $log_inventario->inventario_id = $inventario->id;
            if($request->tipo_movimiento == 'entrada') 
                $log_inventario->entrada_id = $inv_entrada->id;
            else if($request->tipo_movimiento == 'salida')
                $log_inventario->salida_id = $inv_salida->id;
            $log_inventario->user_id = Auth::user()->id;
            $log_inventario->log = 'Se registró un ajuste de Inventario';
            $log_inventario->comentarios = trim($request->comentarios);
            $log_inventario->save();
        }

        $inventario->update();
        DB::commit();

        return redirect()->intended('inventario')->with('registro_success', true);
    }

    public function editarPuntosInventario(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'capacidad_minima'           => 'required|numeric',
            'punto_reorden'           => 'required|numeric',
            'capacidad_maxima'           => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_puntos_inventario', true);
        }

        DB::beginTransaction();

        $establecimiento = ClienteEstablecimiento::findOrFail($id);
        $establecimiento->capacidad_minima = $request->capacidad_minima;
        $establecimiento->punto_reorden = $request->punto_reorden;
        $establecimiento->capacidad_maxima = $request->capacidad_maxima;
        $establecimiento->update();
        DB::commit();

        return redirect()->intended('inventario/externo/'.$id)->with('puntos_inventario_success', true);
    }


    public function realizarCorte(Request $request, $id) {
        if (!isset($request->cantidad_actual_corte) ) {
            $corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'cantidad_actual_corte.*'           => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
                if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
                    $producto = Producto::findOrFail($request->producto_corte[$i]);
                    $productos_failed[] = $producto->nombre;
                }
        }

        if (count($productos_failed) > 0) {
            $corteFailed = [
                'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
            
            $cantidad_total=0;
            //INSERT PRODUCTS TO CORTE
            for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $cantidad_total+=intval($request->cantidad_actual_corte[$i]);

                $productos_corte = [
                    'establecimiento_id'    => $id,
                    'producto_id'           => $request->producto_corte[$i],
                    'cantidad_actual'       => $request->cantidad_actual_corte[$i],
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
                $corte = new Corte($productos_corte);
                $corte->save();
            }

            $establecimiento->capacidad_actual = $cantidad_total;

            if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
                $establecimiento->status_inventario = 0;
            } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 1;
            } else if ($cantidad_total > $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 2;
            }

            $establecimiento->update();
        } else {
            $corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
        
        DB::commit();

        if ($request->pantalla == 'index') {
            return redirect()->intended('inventario/externo')->with('corte_success', true);
        } else {
            return redirect()->intended('inventario/externo/'.$id)->with('corte_success', true);
        }
    }

    public function asignarConservadores(Request $request, $id)
    {
        $messages = [
            'asignar_conservador.required'  => 'Es necesario seleccionar al menos un conservador.',
        ];

        $validator = Validator::make($request->all(), [
            'asignar_conservador'           => 'required|exists:conservador,id',
        ], $messages);
                
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_conservador_asignar', true);
        }

        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        $producto_id = 0;
        if(count($request->asignar_conservador) > 0){
            foreach($request->asignar_conservador as $conservador_id){
                $conservador = Conservador::findOrFail($conservador_id);
                $conservador->cliente_id = $establecimiento->cliente->id;
                $conservador->cliente_establecimiento_id = $establecimiento->id;
                $conservador->status = 'en_consignacion';
                $conservador->updated_by = Auth::user()->id;
                $conservador->update();
                $producto_id = $conservador->producto_id;
            }           
        }

        #Store in Precios Table

        #SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO
        if( !$establecimiento->precios->pluck('producto_id')->contains($producto_id) ) {

            $producto = Producto::findOrFail($producto_id);
            $precios_producto = [
                'cliente_id'            => $establecimiento->cliente_id,
                'producto_id'           => $producto_id,
                'precio_venta'          => $producto->precio_venta,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $precio_producto_est = new EstablecimientoPrecios($precios_producto);
            $establecimiento->precios()->save($precio_producto_est);
        }

        #Actualizar Puntos de inventario
        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
        $establecimiento->update();

        return redirect()->intended('/inventario/externo/'.$id)->with('show_tab_conservadores', true);

    }

    public function updateCuartoFrio(Request $request, $id, $cf_id) {
        if (!isset($request->nombre_productos_cf) || !isset($request->capacidad_productos_cf) ) {
            $updateFailed = [
                'updateFailed' => 'Agrege al menos un producto al Cuarto Frío',
            ];
            return back()->withErrors($updateFailed)->withInput()->with('show_tab_conservadores', true);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'nombre_productos_cf.*'             => 'required',
            'capacidad_productos_cf.*'          => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            //dd($validator->fails());
            return back()->withErrors($validator)->withInput()->with('show_tab_conservadores', true);
        }

        DB::beginTransaction();
        $cuartofrio = CuartoFrio::findOrFail($cf_id);
        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        //DELETE ALL PRODUCTS
        foreach($cuartofrio->productos as $producto){
            $producto->delete();
        }
        
        //INSERT PRODUCTS
        for ($i=0; $i < count($request->cfproducto_edit_id); $i++) { 
            $productos_cuartofrio = [
                'producto_id'           => $request->cfproducto_edit_id[$i],
                'capacidad'             => $request->capacidad_productos_cf[$i],
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $producto_cf = new CuartoFrioProductos($productos_cuartofrio);
            $cuartofrio->productos()->save($producto_cf);

            #Store in Precios Table

            #SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO
            if( !$establecimiento->precios->pluck('producto_id')->contains($request->cfproducto_edit_id[$i]) ) {

                $producto = Producto::findOrFail($request->cfproducto_edit_id[$i]);
                $precios_producto = [
                    'cliente_id'            => $establecimiento->cliente_id,
                    'producto_id'           => $request->cfproducto_edit_id[$i],
                    'precio_venta'          => $producto->precio_venta,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
                $precio_producto_est = new EstablecimientoPrecios($precios_producto);
                $establecimiento->precios()->save($precio_producto_est);
            }

            #Actualizar Puntos de inventario
            $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
            $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
            $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
            $establecimiento->update();

        }

        DB::commit();

        return redirect()->intended('/inventario/externo/'.$id)->with('show_tab_cuartofrio', true);
    }

    public function updateInventario(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'capacidad_minima'  => 'required|numeric',
            'capacidad_maxima'  => 'required|numeric',
            'punto_reorden'     => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_tab_conservadores', true);
        }

        DB::beginTransaction();
        $establecimiento =  ClienteEstablecimiento::findOrFail($id);
        $establecimiento->capacidad_minima = $request->capacidad_minima;
        $establecimiento->capacidad_maxima = $request->capacidad_maxima;
        $establecimiento->punto_reorden = $request->punto_reorden;

        $establecimiento->update();
        
        DB::commit();
        return redirect()->intended('/inventario/externo/'.$id)->with('show_tab_conservadores', true);
    }

    public function updateEntrada(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'producto_id'  => 'required',
            'cantidad'  => 'required|numeric',
            'movimiento'  => 'required',
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_editar_entrada', true);
        }

        DB::beginTransaction();
        $entrada =  InventarioEntradas::findOrFail($id);
        $inventario =  Inventario::findOrFail($entrada->inventario_id);

        $cant_actual = $inventario->cantidad_disponible;
        //Registros anteriores
        $cant_entrada_anterior = $entrada->cantidad_entrada;
        $motivo_entrada_anterior = $entrada->motivo;

        $cant_nueva = (intval($cant_actual)-intval($cant_entrada_anterior))+$request->cantidad;

        if ( $cant_entrada_anterior != $request->cantidad && $motivo_entrada_anterior != $request->movimiento ) {
            #Cambio cantidad y tipo de movimiento
            $log_text = 'La entrada cambió de Cantidad: '.$cant_entrada_anterior.' => <b>'.$request->cantidad.'</b>, ';
            $log_text .= 'Movimiento: '.ucfirst($request->movimiento).'</b>.';
        } else if ( $cant_entrada_anterior != $request->cantidad && $motivo_entrada_anterior == $request->movimiento ) {
            #Cambio solo cantidad
            $log_text = 'La entrada cambió de Cantidad: '.$cant_entrada_anterior.' => <b>'.$request->cantidad.'</b>.';
        } else if ( $cant_entrada_anterior == $request->cantidad && $motivo_entrada_anterior != $request->movimiento ) {
            #Cambio solo tipo de movimiento
            $log_text = 'La entrada cambió de Movimiento: '.ucfirst($motivo_entrada_anterior).' => <b>'.ucfirst($request->movimiento).'</b>.';
        } else {
            # No hubo cambios
            $editFailed = [
                'editFailed' => 'No se detectaron cambios en el movimiento.',
            ];
            return back()->withErrors($editFailed)->withInput()->with('show_modal_editar_entrada', true);
        }

        $inventario->cantidad_disponible = $cant_nueva;
        $entrada->tipo = 'entrada';
        $entrada->producto_id = $request->producto_id;
        $entrada->cantidad_entrada = $request->cantidad;
        $entrada->motivo = $request->movimiento;
        $inventario->update();
        $entrada->update();

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $entrada->inventario_id;
        $log_inventario->entrada_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = $log_text;
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        DB::commit();
        return redirect()->intended('/inventario/entradas/'.$entrada->inventario_id)->with('registro_update_success', true);

    }

    public function updateSalida(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'producto_id'  => 'required',
            'cantidad'  => 'required|numeric',
            'movimiento'  => 'required',
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_editar_salida', true);
        }

        DB::beginTransaction();
        $salida =  InventarioSalidas::findOrFail($id);
        $inventario =  Inventario::findOrFail($salida->inventario_id);

        $cant_actual = $inventario->cantidad_disponible;
        //Registros anteriores
        $cant_salida_anterior = $salida->cantidad_salida;
        $motivo_salida_anterior = $salida->motivo;

        $cant_nueva = (intval($cant_actual)+intval($cant_salida_anterior))-$request->cantidad;

        if ( $cant_salida_anterior != $request->cantidad && $cant_salida_anterior != $request->movimiento ) {
            #Cambio cantidad y tipo de movimiento
            $log_text = 'La salida cambió de Cantidad: '.$cant_salida_anterior.' => <b>'.$request->cantidad.'</b>, ';
            $log_text .= 'Movimiento: '.ucfirst($request->movimiento).'</b>.';
        } else if ( $cant_salida_anterior != $request->cantidad && $cant_salida_anterior == $request->movimiento ) {
            #Cambio solo cantidad
            $log_text = 'La salida cambió de Cantidad: '.$cant_salida_anterior.' => <b>'.$request->cantidad.'</b>.';
        } else if ( $cant_salida_anterior == $request->cantidad && $cant_salida_anterior != $request->movimiento ) {
            #Cambio solo tipo de movimiento
            $log_text = 'La salida cambió de Movimiento: '.ucfirst($motivo_salida_anterior).' => <b>'.ucfirst($request->movimiento).'</b>.';
        } else {
            # No hubo cambios
            $editFailed = [
                'editFailed' => 'No se detectaron cambios en el movimiento.',
            ];
            return back()->withErrors($editFailed)->withInput()->with('show_modal_editar_salida', true);
        }

        $inventario->cantidad_disponible = $cant_nueva;
        $salida->tipo = 'salida';
        $salida->producto_id = $request->producto_id;
        $salida->cantidad_salida = $request->cantidad;
        $salida->motivo = $request->movimiento;
        $inventario->update();
        $salida->update();

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $salida->inventario_id;
        $log_inventario->salida_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = $log_text;
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        DB::commit();
        return redirect()->intended('/inventario/salidas/'.$salida->inventario_id)->with('registro_update_success', true);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showEntradas($id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $inventario = Inventario::findOrFail($id);
        $q = InventarioEntradas::query();

        $q->whereRaw("inventario_id = '". $id ."'");

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
        
        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'MOTIVO' : $orderByString = 'motivo '.$sort_aux;  
                break;

            case 'PRODUCTO' : $orderByString = '(SELECT nombre FROM producto WHERE producto_id = producto.id) '.$sort_aux;
                break;

            case 'CANTIDAD' : $orderByString = 'cantidad_entrada '.$sort_aux;  
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux; 
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE created_by = user.id) '.$sort_aux;
                break;

            default: $orderByString = 'created_at DESC';
                break;
        }
        
        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // FILTRO TIPO MOVIMIENTO
        if (Input::has('f_motivo') && Input::get('f_motivo') != 'todos')
        {
            $q->whereRaw("motivo = '". Input::get('f_motivo') ."'");
        }

        // FILTRO ESTATUS
        if (Input::has('f_estatus')) {

            switch ( Input::get('f_estatus') ) {
                case 'todos':
                    $q->withTrashed();
                    break;

                case 'activo':
                    $q->withoutTrashed();
                    break;
                
                case 'eliminado':
                    $q->onlyTrashed();
                    break;
            }

        } else {
            $q->withoutTrashed();
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['entradas'] = $q->paginate($num_resultados);
            
            if( count($data['entradas']) == 0 )
                $data['entradas'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['entradas'] = $q->paginate(10);
        }

        return view('sistema.inventario.show_entradas', ['inventario' => $inventario, 'entradas' => $data['entradas']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showSalidas($id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $inventario = Inventario::findOrFail($id);
        $q = InventarioSalidas::query();

        $q->whereRaw("inventario_id = '". $id ."'");

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
        
        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'MOTIVO' : $orderByString = 'motivo '.$sort_aux;  
                break;

            case 'PRODUCTO' : $orderByString = '(SELECT nombre FROM producto WHERE producto_id = producto.id) '.$sort_aux;
                break;

            case 'CANTIDAD' : $orderByString = 'cantidad_salida '.$sort_aux;  
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux; 
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE created_by = user.id) '.$sort_aux;
                break;

            default: $orderByString = 'created_at DESC';
                break;
        }
        
        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // FILTRO TIPO MOVIMIENTO
        if (Input::has('f_motivo') && Input::get('f_motivo') != 'todos')
        {
            $q->whereRaw("motivo = '". Input::get('f_motivo') ."'");
        }

        // FILTRO ESTATUS
        if (Input::has('f_estatus')) {

            switch ( Input::get('f_estatus') ) {
                case 'todos':
                    $q->withTrashed();
                    break;

                case 'activo':
                    $q->withoutTrashed();
                    break;
                
                case 'eliminado':
                    $q->onlyTrashed();
                    break;
            }

        } else {
            $q->withoutTrashed();
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['salidas'] = $q->paginate($num_resultados);
            
            if( count($data['salidas']) == 0 )
                $data['salidas'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['salidas'] = $q->paginate(10);
        }

        return view('sistema.inventario.show_salidas', ['inventario' => $inventario, 'salidas' => $data['salidas']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showExterno($id)
    {
        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        return view('sistema.inventario.show_externos', ['establecimiento' => $establecimiento]);
    }

    public function deleteEntrada(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_delete', true);
        }

        DB::beginTransaction();
        $entrada = InventarioEntradas::findOrFail($id);
        $inventario =  Inventario::findOrFail($entrada->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_entrada_anterior = $entrada->cantidad_entrada;
        $cant_nueva = (intval($cant_actual)-intval($cant_entrada_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id =$entrada->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->entrada_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se eliminó la entrada '.$cant_entrada_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $entrada->delete();
        DB::commit();

        return redirect()->intended('/inventario/entradas/'.$inventario_id)->with('delete_entrada_success', true);
    }

    public function reactivarEntrada(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_restore', true);
        }

        DB::beginTransaction();
        $entrada = InventarioEntradas::withTrashed()->findOrFail($id);

        $inventario =  Inventario::findOrFail($entrada->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_entrada_anterior = $entrada->cantidad_entrada;
        $cant_nueva = (intval($cant_actual)+intval($cant_entrada_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id =$entrada->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->entrada_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se restauró la entrada '.$cant_entrada_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $entrada->restore();
        DB::commit();

        return redirect()->intended('/inventario/entradas/'.$inventario_id)->with('restore_entrada_success', true);
    }

    public function deleteSalida(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_delete', true);
        }

        DB::beginTransaction();
        $salida = InventarioSalidas::findOrFail($id);
        $inventario = Inventario::findOrFail($salida->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_salida_anterior = $salida->cantidad_salida;
        $cant_nueva = (intval($cant_actual)+intval($cant_salida_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id=$salida->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->salida_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se eliminó la salida '.$cant_salida_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $salida->delete();
        DB::commit();

        return redirect()->intended('/inventario/salidas/'.$inventario_id)->with('delete_salida_success', true);
    }

    public function reactivarSalida(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_restore', true);
        }

        DB::beginTransaction();
        $salida = InventarioSalidas::withTrashed()->findOrFail($id);

        $inventario =  Inventario::findOrFail($salida->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_salida_anterior = $salida->cantidad_salida;
        $cant_nueva = (intval($cant_actual)-intval($cant_salida_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id =$salida->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->salida_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se restauró la salida '.$cant_salida_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $salida->restore();
        DB::commit();

        return redirect()->intended('/inventario/salidas/'.$inventario_id)->with('restore_salida_success', true);
    }

    public function destroyCuartoFrio($establecimiento_id, $cuartofrio_id)
    {
        $establecimiento = ClienteEstablecimiento::findOrFail($establecimiento_id);
        $cuartofrio = CuartoFrio::findOrFail($cuartofrio_id);
        $productos = $cuartofrio->productos();

        DB::beginTransaction();

        $borrados=[];
        //DELETE ALL PRODUCTS

        foreach($cuartofrio->productos as $cf_producto){
            #guardar id de productos borrados
            $borrados[] = $cf_producto->producto_id;
            $cf_producto->delete();
        }

        $establecimiento = ClienteEstablecimiento::findOrFail($establecimiento_id);
        $establecimiento->cuartofrio_id = null;
        $establecimiento->update();
                
        $cuartofrio->delete();

        $productos_asociados = collect($establecimiento->productos_disponibles());

        for ($i=0; $i < count($borrados); $i++) { 
            if( !$productos_asociados->contains($borrados[$i]) ) {
                #Borrar
                DB::table('cliente_establecimiento_precios')->where([
                    ['establecimiento_id', $establecimiento_id],
                    ['producto_id', $borrados[$i]]
                ])->delete();
            }
        }


        DB::commit();
        
        return redirect()->intended('/inventario/externo/'.$establecimiento_id)->with('show_tab_cuartofrio', true);
    }
}