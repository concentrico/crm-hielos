<?php
namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\ClienteEstablecimiento as Establecimiento;
use App\Models\Cliente as Cliente;
use App\Models\ClienteDatosFiscales as DatosFiscales;
use App\Models\ClienteContacto as Contacto;
use App\Models\ClienteContactoEmail as ContactoEmail;
use App\Models\ClienteContactoTelefono as ContactoTelefono;
use App\Models\ClienteTipoEstablecimiento as TipoEstablecimiento;
use App\Models\ClienteZona as ClienteZona;
use App\Models\Conservador as Conservador;
use App\Models\Producto as Producto;
use App\Models\Pedido as Pedido;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\CuartoFrioProductos as CuartoFrioProductos;
use App\Models\Corte as Corte;
use App\Models\EstablecimientoPrecios as EstablecimientoPrecios;
use App\User as Usuario;

use Auth;
use DB;
use File; 
use Input;
use Validator;
use Gate;
use Hash;
use Mail;
use Carbon\Carbon;

class EstablecimientosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {

    	if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

		$q = Establecimiento::query();
		$tipos_array = TipoEstablecimiento::all()->pluck('id')->toArray();
		$zonas_array = ClienteZona::all()->pluck('id')->toArray();

		// FILTRO BUSQUEDA
        if (Input::has('f_busqueda'))
        {
            $q->whereRaw("( (sucursal LIKE '%". Input::get('f_busqueda') ."%') OR (SELECT nombre_corto FROM cliente WHERE id=cliente_establecimiento.cliente_id LIKE '%". Input::get('f_busqueda') ."%') )");


            $tipos_mas_popular = collect();
			foreach ($tipos_array as $key => $value) {
				$q2 = (clone $q);
				$q2->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '.$value.') > 0)');

				$tipos_mas_popular[] = ['tipo_id' => $value , 'total' => $q2->count()];
			}

			$zonas_mas_popular = collect();
			foreach ($zonas_array as $key => $value) {
				$q3 = (clone $q);
				$q3->where('cliente_zona_id',$value);
				$zonas_mas_popular[] = ['zona_id' => $value , 'total' => $q3->count()];
			}

			$sorted = $tipos_mas_popular->sortByDesc('total');
			$sorted2 = $zonas_mas_popular->sortByDesc('total');

	        $data['total_establecimientos'] = $q->count();
			$data['tipo_sucursal_maspopular'] = trim(TipoEstablecimiento::find($sorted->first()['tipo_id'])->nombre);
	        $data['tipo_zona_maspopular'] = trim(ClienteZona::find($sorted2->first()['zona_id'])->nombre);


        }


        // FILTRO TIPO
		if (Input::has('f_tipo'))
		{
			//$aux_tipos = implode(", ", Input::get('f_tipo'));
			//$q->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id IN ('. $aux_tipos .')) > 0)');

			$q->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '. Input::get('f_tipo') .' ) > 0)');

			$zonas_mas_popular = collect();
			foreach ($zonas_array as $key => $value) {
				$q3 = (clone $q);
				$q3->where('cliente_zona_id',$value);
				$q3->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '. Input::get('f_tipo') .' ) > 0)');
				$zonas_mas_popular[] = ['zona_id' => $value , 'total' => $q3->count()];
			}

			$sorted2 = $zonas_mas_popular->sortByDesc('total');

	        $data['total_establecimientos'] = $q->count();
			$data['tipo_sucursal_maspopular'] = trim(TipoEstablecimiento::find(Input::get('f_tipo'))->nombre);
	        $data['tipo_zona_maspopular'] = trim(ClienteZona::find($sorted2->first()['zona_id'])->nombre);

		}

		// FILTRO ZONA DE ENTREGA
		if (Input::has('f_zona'))
		{
			$q->whereRaw('cliente_zona_id = '. Input::get('f_zona'));

			$tipos_mas_popular = collect();
			foreach ($tipos_array as $key => $value) {
				$q2 = (clone $q);
				$q2->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '.$value.') > 0)');

				$tipos_mas_popular[] = ['tipo_id' => $value , 'total' => $q2->count()];
			}

			$sorted = $tipos_mas_popular->sortByDesc('total');

	        $data['total_establecimientos'] = $q->count();
			$data['tipo_sucursal_maspopular'] = trim(TipoEstablecimiento::find($sorted->first()['tipo_id'])->nombre);
	        $data['tipo_zona_maspopular'] = trim(ClienteZona::find(Input::get('f_zona'))->nombre);
		}
		

		// SORT ORDER
		$sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

		// SORT VARIABLE
		$orderByString = "";
		switch(Input::get('orderBy')){
			case 'ID' : $orderByString = 'id '.$sort_aux;			
				break;

			case 'NOMBRE' : $orderByString = 'sucursal '.$sort_aux;			
				break;

			case 'CLIENTE' : $orderByString = '(SELECT nombre_corto FROM cliente WHERE cliente_id = cliente.id) '.$sort_aux;
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux;            
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE cliente_establecimiento.created_by = users.id) '.$sort_aux;
                break;

            case 'UPDATEDAT' : $orderByString = 'updated_at '.$sort_aux;            
                break;

            case 'UPDATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE cliente_establecimiento.updated_by = users.id) '.$sort_aux;
                break;

			default : $orderByString = 'id '.$sort_aux;				
				break;
		}
		
		if($orderByString != ""){
			$q->orderByRaw($orderByString);
		}

		// FILTRO COBRANZA
		if (Input::has('f_estatus_cobranza') && Input::get('f_estatus_cobranza') == 'vencido')
		{
			
			$establecimientos_aux = $q->get();
			$establecimientos_pdte_array = [];
            foreach ($establecimientos_aux as $establecimiento) {
                if($establecimiento->saldo_porcobrar() > 0.00)
                    $establecimientos_pdte_array[] = $establecimiento->id;
            }

            $q->whereIn('id', $establecimientos_pdte_array);

            $tipos_mas_popular = collect();
			foreach ($tipos_array as $key => $value) {
				$q2 = Establecimiento::query();
				$q2->whereIn('id', $establecimientos_pdte_array);
				$q2->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '.$value.') > 0)');

				$tipos_mas_popular[] = ['tipo_id' => $value , 'total' => $q2->count()];
			}

			$zonas_mas_popular = collect();
			foreach ($zonas_array as $key => $value) {
				$q3 = Establecimiento::where('cliente_zona_id',$value);
				$q3->whereIn('id', $establecimientos_pdte_array);
				$zonas_mas_popular[] = ['zona_id' => $value , 'total' => $q3->count()];
			}

			$sorted = $tipos_mas_popular->sortByDesc('total');
			$sorted2 = $zonas_mas_popular->sortByDesc('total');

	        $data['total_establecimientos'] = $q->count();
			$data['tipo_sucursal_maspopular'] = trim(TipoEstablecimiento::find($sorted->first()['tipo_id'])->nombre);
	        $data['tipo_zona_maspopular'] = trim(ClienteZona::find($sorted2->first()['zona_id'])->nombre);


		}
		
		// NUM RESULTADOS
		if (Input::has('f_num_resultados'))
		{

			$num_resultados = 0 + ((int) Input::get('f_num_resultados') );
			$data['establecimientos'] = $q->paginate($num_resultados);
			
			if( count($data['establecimientos']) == 0 ){
				$data['establecimientos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
			}
		}else{
			$data['establecimientos'] = $q->paginate(50);
		}

		if ( ( !Input::has('f_estatus_cobranza') || Input::get('f_estatus_cobranza') != 'vencido' || Input::get('f_estatus_cobranza') == 'todos') && (!Input::has('f_estatus') || Input::get('f_estatus') == 'todos' ) && (!Input::has('f_busqueda') || Input::has('f_busqueda') == '') && (!Input::has('f_tipo') || Input::has('f_tipo') == '') && (!Input::has('f_zona') || Input::has('f_zona') == '') )
		{

			$tipos_mas_popular = collect();
			foreach ($tipos_array as $key => $value) {
				$q2 = Establecimiento::query();
				$q2->whereRaw('((SELECT COUNT(establecimiento_id) FROM cliente_establecimiento_has_tipo WHERE establecimiento_id = cliente_establecimiento.id AND tipo_est_id = '.$value.') > 0)');

				$tipos_mas_popular[] = ['tipo_id' => $value , 'total' => $q2->count()];
			}


			$zonas_mas_popular = collect();
			foreach ($zonas_array as $key => $value) {
				$q3 = Establecimiento::where('cliente_zona_id',$value);
				$zonas_mas_popular[] = ['zona_id' => $value , 'total' => $q3->count()];
			}

			$sorted = $tipos_mas_popular->sortByDesc('total');
			$sorted2 = $zonas_mas_popular->sortByDesc('total');

	        $data['total_establecimientos'] = Establecimiento::all()->count();
			$data['tipo_sucursal_maspopular'] = trim(TipoEstablecimiento::find($sorted->first()['tipo_id'])->nombre);
	        $data['tipo_zona_maspopular'] = trim(ClienteZona::find($sorted2->first()['zona_id'])->nombre);


		}
		
		
		return view('sistema.establecimientos.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $cliente_id)
    {
    	
        $validator = Validator::make($request->all(), [
	        'cliente_establecimiento_sucursal'					=> 'required',
	        'cliente_establecimiento_tipos'						=> 'required|exists:cliente_tipo_establecimiento,id',
	        'cliente_establecimiento_zona'						=> 'required|exists:cliente_zona,id',
	        'est_calle' => 'required',
	        'est_no_exterior' => 'required',
	        'est_colonia' => 'required',
	        'est_codigo_postal' => 'required',
	        'est_municipio' => 'required',
        ],[
        	'required'	=> 'Campo requerido',
	        'cliente_establecimiento_sucursal.required'			=> 'El nombre de la sucursal es requerida',
	        'cliente_establecimiento_tipos.required'			=> 'Seleccione al menos un tipo de Establecimiento',
	        'cliente_establecimiento_zona.required'				=> 'La zona de entrega es requerida',
	        'est_calle.required'								=> 'La calle es requerida',
	        'est_no_exterior.required'							=> 'El número es requerido',
	        'est_colonia.required'								=> 'La colonia es requerida',
	        'est_municipio.required'							=> 'El municipio es requerido',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_establecimiento_create', true);
        }
	    
	    DB::beginTransaction();
        $establecimiento = new Establecimiento;
		
        $establecimiento->cliente_id = $cliente_id;
        $establecimiento->sucursal = $request->cliente_establecimiento_sucursal;
		$establecimiento->cliente_zona_id = ( $request->cliente_establecimiento_zona == "" ? null : $request->cliente_establecimiento_zona );
        $establecimiento->cliente_datos_fiscales_id = ( $request->cliente_establecimiento_razon_social == "" ? null : $request->cliente_establecimiento_razon_social );

        $establecimiento->calle = $request->est_calle;
        $establecimiento->numero_exterior = $request->est_no_exterior;
        $establecimiento->numero_interior = $request->est_no_interior;
        $establecimiento->colonia= $request->est_colonia;
        $establecimiento->codigo_postal = $request->est_codigo_postal;
        $establecimiento->municipio = $request->est_municipio;
        $establecimiento->estado = 'Nuevo León';
        $establecimiento->comentarios = $request->establecimiento_comentarios;
		$establecimiento->created_by = Auth::user()->id;
		$establecimiento->updated_by = Auth::user()->id;

		$establecimiento->save();

		if(count($request->cliente_establecimiento_tipos) > 0){
			foreach($request->cliente_establecimiento_tipos as $t){
				$establecimiento->tipos()->attach($t, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
			}			
		}

	    DB::commit();

        return redirect()->intended('/clientes/'.$cliente_id)->with('show_tab_establecimientos', true);
    }

    public function store2(Request $request)
    {
    	
        $validator = Validator::make($request->all(), [
	        'cliente_establecimiento_cliente'					=> 'required|exists:cliente,id',
	        'cliente_establecimiento_sucursal'					=> 'required',
	        'cliente_establecimiento_tipos'						=> 'required|exists:cliente_tipo_establecimiento,id',
	        'cliente_establecimiento_zona'						=> 'required|exists:cliente_zona,id',
	        'est_calle' => 'required',
	        'est_no_exterior' => 'required',
	        'est_colonia' => 'required',
	        'est_municipio' => 'required',
        ],[
	        'cliente_establecimiento_cliente.required'			=> 'Debe asignar un cliente para continuar',
	        'cliente_establecimiento_sucursal.required'			=> 'El nombre de la sucursal es requerido',
	        'cliente_establecimiento_tipos.required'			=> 'Seleccione al menos un tipo de Establecimiento',
	        'cliente_establecimiento_zona.required'				=> 'La zona de entrega es requerida',
	        'est_calle.required'								=> 'La calle es requerida',
	        'est_no_exterior.required'							=> 'El número es requerido',
	        'est_colonia.required'								=> 'La colonia es requerida',
	        'est_municipio.required'							=> 'El municipio es requerido',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_establecimiento_create', true);
        }
        
	    DB::beginTransaction();
        $establecimiento = new Establecimiento;
		
        $establecimiento->cliente_id = $request->cliente_establecimiento_cliente;
        $establecimiento->sucursal = $request->cliente_establecimiento_sucursal;
		$establecimiento->cliente_zona_id = ( $request->cliente_establecimiento_zona == "" ? null : $request->cliente_establecimiento_zona );
        $establecimiento->cliente_datos_fiscales_id = ( $request->cliente_establecimiento_razon_social == "" ? null : $request->cliente_establecimiento_razon_social );
        $establecimiento->calle = $request->est_calle;
        $establecimiento->numero_exterior = $request->est_no_exterior;
        $establecimiento->numero_interior = $request->est_no_interior;
        $establecimiento->colonia= $request->est_colonia;
        $establecimiento->codigo_postal = $request->est_codigo_postal;
        $establecimiento->municipio = $request->est_municipio;
        $establecimiento->estado = 'Nuevo León';
        $establecimiento->comentarios = $request->establecimiento_comentarios;
		$establecimiento->created_by = Auth::user()->id;
		$establecimiento->updated_by = Auth::user()->id;

		$establecimiento->save();

		if(count($request->cliente_establecimiento_tipos) > 0){
			foreach($request->cliente_establecimiento_tipos as $t){
				$establecimiento->tipos()->attach($t, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
			}			
		}

	    DB::commit();

        return redirect()->intended('/establecimientos/'.$establecimiento->id);
    }

    public function storeDatosFiscales(Request $request, $id)
    {
		$messages = [
		    'required' 	=> 'Campo requerido.',
		    'rfc' 		=> 'Revisa el formato del RFC',
		];

        $validator = Validator::make($request->all(), [
	        'tipo_persona' 			=> 'required',
	        'rfc'		 			=> 'required|rfc',
	        'razon_social' 			=> 'required',
	        'representante_legal' 	=> 'required_if:tipo_persona,moral',
	        'df_calle' 				=> '',
	        'df_no_exterior' 		=> '',
	        'df_no_interior' 		=> '',
	        'df_colonia' 			=> '',
	        'df_codigo_postal' 		=> 'numeric',
	        'df_municipio' 			=> '',
	        'df_estado' 			=> '',
        ], $messages);
        		
        if ($validator->fails()) {
	        
            return back()->withErrors($validator)->withInput()->with('show_modal_establecimiento_datos_fiscales_create', true);
        }
	    
	    DB::beginTransaction();

        $establecimiento =  Establecimiento::findOrFail($id);

        if (isset($request->datos_fiscales_id) && $request->datos_fiscales_id != 0 && $request->datos_fiscales_id != '') { # ASOCIAR
        	$datos_fiscales = DatosFiscales::findOrFail($request->datos_fiscales_id);
        } else { # CREAR
        	$datos_fiscales = new DatosFiscales;
			$datos_fiscales->cliente_id = $establecimiento->cliente_id;
			$datos_fiscales->persona = $request->tipo_persona;
			$datos_fiscales->rfc = trim($request->rfc);
			$datos_fiscales->razon_social = strtoupper(trim($request->razon_social));
			$datos_fiscales->representante_legal = ucfirst(trim($request->representante_legal));
			$datos_fiscales->calle = ucfirst(trim($request->df_calle));
			$datos_fiscales->numero_interior = trim($request->df_no_interior);
			$datos_fiscales->numero_exterior = trim($request->df_no_exterior);
			$datos_fiscales->colonia = ucfirst(trim($request->df_colonia));
			$datos_fiscales->codigo_postal = trim($request->df_codigo_postal);
			$datos_fiscales->municipio = ucfirst(trim($request->df_municipio));
			$datos_fiscales->estado = ucfirst(trim($request->df_estado));
			$datos_fiscales->created_by = Auth::user()->id;
			$datos_fiscales->updated_by = Auth::user()->id;
			$datos_fiscales->save();
        }

		$establecimiento->cliente_datos_fiscales_id = $datos_fiscales->id;
		$establecimiento->update();
		
		DB::commit();

        return redirect()->intended('/establecimientos/'.$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeContacto(Request $request, $id)
    {

		$messages = [
		    'required' => 'Campo requerido.',
		    'email' => 'Formato inválido.',
		];

        $validator = Validator::make($request->all(), [
	        'contacto_nombre'			=> 'required',
	        'contacto_apellido_paterno' => 'required',
	        'contacto_apellido_materno' => '',
	        'tipo_email.*' 				=> 'required',
	        'email.*' 					=> 'required|email',
	        'tipo_telefono.*' 			=> 'required',
	        'telefono.*' 				=> 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_establecimiento_contacto_create', true);
        }

        $arr_desc_email = array();
		$arr_email = array();
		$arr_desc_telefono = array();
		$arr_telefono = array();
		$arr_telefono_ext = array();

        if($request->tipo_email){
			$arr_desc_email = $request->tipo_email;
		}
		if($request->email){
			$arr_email = $request->email;
		}
		if($request->tipo_telefono){
			$arr_desc_telefono = $request->tipo_telefono;
		}
		if($request->telefono){
			$arr_telefono = $request->telefono;
		}
		if($request->telefono_ext){
			$arr_telefono_ext = $request->telefono_ext;
		}

        DB::beginTransaction();

        $establecimiento =  Establecimiento::findOrFail($id);
        $contacto = new Contacto;
        
        $contacto->cliente_id = $establecimiento->cliente_id;
        $contacto->nombre = trim($request->contacto_nombre);
		$contacto->apellido_paterno = trim($request->contacto_apellido_paterno);
		$contacto->apellido_materno = trim($request->contacto_apellido_materno);
		$contacto->comentarios = trim($request->contacto_comentarios);
		$contacto->created_by = Auth::user()->id;
		$contacto->updated_by = Auth::user()->id;

		$contacto->save();

		//Asignar a Establecimiento
		$contacto->establecimientos()->attach($id, ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
		
        foreach($arr_desc_email AS $key => $value){
	        $correo = new ContactoEmail;
	        $correo->valor = $arr_email[$key]; 
	        $correo->tipo = $value;
	        $correo->created_by = Auth::user()->id;
	        $correo->updated_by = Auth::user()->id;
		    $correo = $contacto->emails()->save($correo);
        }
        
        foreach($arr_desc_telefono AS $key => $value){
	        $telefono = new ContactoTelefono;
	        $telefono->valor = $arr_telefono[$key]; 
	        $telefono->tipo = $value;
	        $telefono->extension = $arr_telefono_ext[$key];
	        $telefono->created_by = Auth::user()->id;
	        $telefono->updated_by = Auth::user()->id;
	        $telefono = $contacto->telefonos()->save($telefono);
        }
        		
		DB::commit();

        return redirect()->intended('/establecimientos/'.$id)->with('show_tab_contactos', true);
    }

    public function createFromContact($data) {

        DB::beginTransaction();

        $usuario = new Usuario;
        $contacto = Contacto::findOrFail($data['contacto_id']);
		
		$full_name = trim($contacto->nombre).' '.trim($contacto->apellido_paterno).' '.trim($contacto->apellido_materno);
        $usuario->nombre = trim($full_name);
        $usuario->tipo = 'cliente';
        //$usuario->email = $data['email'];
        $usuario->username = $data['username'];
        $usuario->password = Hash::make($data['password']);
        //$usuario->password = Hash::make(Hash::make(rand(0,100)));
        $usuario->created_by = Auth::user()->id;
        $usuario->updated_by = Auth::user()->id;

		$usuario->save();

		$contacto->user_id = $usuario->id;
		$contacto->update();

		$usuario->roles()->attach($data['rol'], ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);

		//$usuario->sendWelcomeNotification();
		DB::commit();
    }
    
    public function updateCuartoFrio(Request $request, $id, $cf_id) {
    	if (!isset($request->nombre_productos_cf) || !isset($request->capacidad_productos_cf) ) {
    		$updateFailed = [
                'updateFailed' => 'Agrege al menos un producto al Cuarto Frío',
            ];
    		return back()->withErrors($updateFailed)->withInput()->with('show_tab_conservadores', true);
    	}

    	$messages = [
		    'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
		];

        $validator = Validator::make($request->all(), [
	        'nombre_productos_cf.*' 			=> 'required',
	        'capacidad_productos_cf.*' 			=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
        	//dd($validator->fails());
            return back()->withErrors($validator)->withInput()->with('show_tab_conservadores', true);
        }

        DB::beginTransaction();
        $cuartofrio = CuartoFrio::findOrFail($cf_id);
        $establecimiento = Establecimiento::findOrFail($id);

    	//DELETE ALL PRODUCTS
		foreach($cuartofrio->productos as $producto){
			$producto->delete();
		}
		
		//INSERT PRODUCTS
		for ($i=0; $i < count($request->cfproducto_edit_id); $i++) { 
			$productos_cuartofrio = [
                'producto_id'           => $request->cfproducto_edit_id[$i],
                'capacidad'      		=> $request->capacidad_productos_cf[$i],
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $producto_cf = new CuartoFrioProductos($productos_cuartofrio);
            $cuartofrio->productos()->save($producto_cf);

            #Store in Precios Table

			#SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO
			if( !$establecimiento->precios->pluck('producto_id')->contains($request->cfproducto_edit_id[$i]) ) {

				$producto = Producto::findOrFail($request->cfproducto_edit_id[$i]);
				$precios_producto = [
	                'cliente_id'           	=> $establecimiento->cliente_id,
	                'producto_id'           => $request->cfproducto_edit_id[$i],
	                'precio_venta'      	=> $producto->precio_venta,
	                'created_by'            => Auth::user()->id,
	                'updated_by'            => Auth::user()->id,
	            ];
	            $precio_producto_est = new EstablecimientoPrecios($precios_producto);
	            $establecimiento->precios()->save($precio_producto_est);
			}
		}

		#Actualizar Puntos de inventario
        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
        $establecimiento->update();

		DB::commit();

        return redirect()->intended('/establecimientos/'.$id)->with('show_tab_conservadores', true);
    }

    public function updateInventario(Request $request, $id) {

    	$messages = [
            'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
	        'capacidad_minima' 	=> 'required|numeric',
	        'capacidad_maxima' 	=> 'required|numeric',
	        'punto_reorden' 	=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_tab_conservadores', true);
        }

        DB::beginTransaction();
        $establecimiento =  Establecimiento::findOrFail($id);
        $establecimiento->capacidad_minima = $request->capacidad_minima;
        $establecimiento->capacidad_maxima = $request->capacidad_maxima;
        $establecimiento->punto_reorden = $request->punto_reorden;

        $establecimiento->update();
        
        DB::commit();
        return redirect()->intended('/establecimientos/'.$id)->with('show_tab_conservadores', true);

    }

    public function realizarCorte(Request $request, $id) {
    	if (!isset($request->cantidad_actual_corte) ) {
    		$corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
    	}

    	$messages = [
		    'required' => 'Campo requerido.',
		    'numeric' => 'Sólo se permiten números.',
		];

        $validator = Validator::make($request->all(), [
	        'cantidad_actual_corte.*' 			=> 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = Establecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
				if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
					$producto = Producto::findOrFail($request->producto_corte[$i]);
					$productos_failed[] = $producto->nombre;
				}
		}

		if (count($productos_failed) > 0) {
			$corteFailed = [
	            'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
	        ];
	        return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
		}

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
        	
        	$cantidad_total=0;
			//INSERT PRODUCTS TO CORTE
			for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
				$cantidad_total+=intval($request->cantidad_actual_corte[$i]);

				$productos_corte = [
	                'establecimiento_id'    => $id,
	                'producto_id'           => $request->producto_corte[$i],
	                'cantidad_actual'      	=> $request->cantidad_actual_corte[$i],
	                'created_by'            => Auth::user()->id,
	                'updated_by'            => Auth::user()->id,
	            ];
	            $corte = new Corte($productos_corte);
	            $corte->save();
			}

			$establecimiento->capacidad_actual = $cantidad_total;

			if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
	            $establecimiento->status_inventario = 0;
	        } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 1;
	        } else if ($cantidad_total > $establecimiento->punto_reorden) {
	            $establecimiento->status_inventario = 2;
	        }

			$establecimiento->update();
        } else {
        	$corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
    		return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
		
		
		DB::commit();

        return redirect()->intended('/establecimientos/'.$id)->with('corte_success', true);
    }

    public function establecerUbicacion(Request $request, $id) {

    	$messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
	        'latitud_longitud' 	=> 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_ubicacion', true);
        }

        DB::beginTransaction();
        $establecimiento =  Establecimiento::findOrFail($id);
        $establecimiento->latitud_longitud = str_replace(' ','',$request->latitud_longitud);

        $establecimiento->update();
        
        DB::commit();
        return redirect()->intended('/establecimientos/'.$id)->with('set_ubicacion_success', true);

    }

    public function updateAcuerdosComerciales(Request $request, $id) {

    	$messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
	        'forma_pago' 		=> 'required|exists:forma_pago,id',
	        'condicion_pago'	=> 'required|exists:condicion_pago,id',
	        'producto_id.*' 		=> 'required',
	        'precio_venta.*' 	=> 'required',
        ], $messages);

        if ($validator->fails()) {
	      return redirect('/establecimientos/'.$id)->withErrors($validator);
	    }

        DB::beginTransaction();
        $establecimiento =  Establecimiento::findOrFail($id);
        $establecimiento->forma_pago_id = ( $request->forma_pago == "" ? null : $request->forma_pago );
		$establecimiento->condicion_pago_id = ( $request->condicion_pago == "" ? null : $request->condicion_pago );
        $establecimiento->update();

    	//DELETE ALL PRODUCTS
		foreach($establecimiento->precios as $precio_producto){
			$precio_producto->delete();
		}
		
		//INSERT PRODUCTS
		for ($i=0; $i < count($request->producto_id); $i++) { 
			$precios_producto = [
                'cliente_id'           	=> $establecimiento->cliente_id,
                'producto_id'           => $request->producto_id[$i],
                'precio_venta'      	=> $request->precio_venta[$i],
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $precio_producto_est = new EstablecimientoPrecios($precios_producto);
            $establecimiento->precios()->save($precio_producto_est);
		}
        
        DB::commit();
        return redirect()->intended('/establecimientos/'.$id)->with('update_acuerdos_success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    	if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        
		$establecimiento = Establecimiento::findOrFail($id);
		
		$data['establecimiento'] = $establecimiento;

		$pedidos_array = DB::table('pedido')->where('cliente_establecimiento_id', $id)->get()->pluck('id')->toArray();
		
		$data['pedidos'] = Pedido::all()->where('status', '!=', 5)->where('cliente_establecimiento_id', $id)->whereIn('id', $pedidos_array);
        $data['pedidos_entregados'] = Pedido::where('status', 3)->where('cliente_establecimiento_id', $id)->orderBy('fecha_entrega', 'asc')->get();

		/*Calendario*/
        for ($i=1; $i <= 12; $i++) { 

            $auxDate1 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01');
            $auxDate2 = ( new Carbon() )->createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.$auxDate1->daysInMonth);

            $dia_actual = $auxDate1;
            for ($j=0; $j < $auxDate2->weekOfMonth; $j++) { 
                # Recorrer por cada semana del mes

                $start = $dia_actual->startOfWeek();
                $end = (clone $start)->endOfWeek()->addDay();
                $start_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $start)->formatLocalized('%Y-%m-%d');
                $end_formatted = Carbon::createFromFormat('Y-m-d H:i:s', $end)->formatLocalized('%Y-%m-%d');

                $data['entregados'][$i-1][$j] = $this->getTotalSemanal($pedidos_array,$start_formatted,$end_formatted);

                $dia_actual = $end;
                $dia_actual->addDay();
            }

        }

        /* Highcharts */
        $productos_hielo = Producto::where('es_hielo', '=', '1')->get();

        foreach ($productos_hielo as $producto) {
            
            for ($i=1; $i <= 12; $i++) { 


                $auxDate1 = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01')->formatLocalized('%Y-%m-%d');
                $auxDate2 = Carbon::createFromFormat('Y-m-d', date('Y').'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.Carbon::createFromFormat('Y-m-d', $auxDate1)->daysInMonth)->formatLocalized('%Y-%m-%d');

                $array_count = collect(DB::table('pedido_productos')
                        ->join('pedido', 'pedido_productos.pedido_id', '=', 'pedido.id' )
                        ->selectRaw('sum(pedido_productos.cantidad_recibida) as cantidad_recibida')
                        ->where([
                                    ['pedido.cliente_establecimiento_id', '=', $id],
                                    ['pedido.status', '=', 3],
                                    ['pedido_productos.producto_id', '=', $producto->id]
                                ])
                        ->whereBetween('pedido.entregado_at', array($auxDate1, $auxDate2) )
                        ->get() ->toArray() ) ->pluck('cantidad_recibida');

                $data['pedidos_entregados_mes'][''.$producto->nombre.''][$i] = (int) array_sum($array_count->toArray());
            }

        }

		return view('sistema.establecimientos.show', $data);
    }


    public function getTotalSemanal($pedidos_array, $start, $end){ 

        $total=0.0;

        $pedidos = collect(DB::table('pedido')->where('status', 3)->whereIn('id', $pedidos_array)->whereBetween('entregado_at', array($start, $end) )->get() ->toArray() )->pluck('total');

        $total = floatval( array_sum($pedidos->toArray()) );
        return $total;
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
	        'cliente_establecimiento_sucursal'					=> 'required',
	        'cliente_establecimiento_tipos'						=> 'required|exists:cliente_tipo_establecimiento,id',
	        'cliente_establecimiento_zona'						=> 'exists:cliente_zona,id',
	        'cliente_establecimiento_razon_social'				=> 'exists:cliente_datos_fiscales,id',
	        'forma_pago' 										=> 'exists:forma_pago,id',
	        'condicion_pago'									=> 'exists:condicion_pago,id',
	        'est_calle' => 'required',
	        'est_no_exterior' => 'required|numeric',
	        'est_no_interior' => '',
	        'est_colonia' => 'required',
	        'est_codigo_postal' => 'numeric',
	        'est_municipio' => 'required',
	        'comentarios' 	=> '',
        ],[
	        'cliente_establecimiento_sucursal.required'			=> 'El nombre de la sucursal es requerido',
	        'cliente_establecimiento_tipos.required'			=> 'Seleccione al menos un tipo de Establecimiento',
	        'cliente_establecimiento_zona.required'		=> 'La zona de entrega es requerida',
	        'est_calle.required'			=> 'La calle es requerida',
	        'est_no_exterior.required'			=> 'El número es requerido',
	        'est_colonia.required'			=> 'La colonia es requerida',
	        'est_municipio.required'		=> 'El municipio es requerido',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cliente_establecimiento_edit', true);
        }
	    
		$establecimiento = Establecimiento::findOrFail($id);
		
        $establecimiento->cliente_id = $establecimiento->cliente_id;
        $establecimiento->sucursal = $request->cliente_establecimiento_sucursal;
		$establecimiento->cliente_zona_id = ( $request->cliente_establecimiento_zona == "" ? null : $request->cliente_establecimiento_zona );
        $establecimiento->cliente_datos_fiscales_id = ( $request->cliente_establecimiento_razon_social == "" ? null : $request->cliente_establecimiento_razon_social );
        $establecimiento->forma_pago_id = ( $request->forma_pago == "" ? null : $request->forma_pago );
		$establecimiento->condicion_pago_id = ( $request->condicion_pago == "" ? null : $request->condicion_pago );
        $establecimiento->calle = $request->est_calle;
        $establecimiento->numero_exterior = $request->est_no_exterior;
        $establecimiento->numero_interior = $request->est_no_interior;
        $establecimiento->colonia= $request->est_colonia;
        $establecimiento->codigo_postal = $request->est_codigo_postal;
        $establecimiento->municipio = $request->est_municipio;
        $establecimiento->estado = 'Nuevo León';
        $establecimiento->comentarios = $request->establecimiento_comentarios;
		$establecimiento->created_by = Auth::user()->id;
		$establecimiento->updated_by = Auth::user()->id;

		$establecimiento->update();

		// Tipos Establecimiento
		//dd($request->cliente_establecimiento_tipos);
		if(count($request->cliente_establecimiento_tipos) > 0){
			foreach($request->cliente_establecimiento_tipos as $t){
				$establecimiento->tipos()->syncWithoutDetaching([$t => ['created_by' => $establecimiento->created_by, 'updated_by' => $establecimiento->created_by]]);
			}			
		}

        return redirect()->intended('/establecimientos/'.$id);
    }

    public function updateDatosFiscales(Request $request, $id, $datosf_id)
    {
		$messages = [
		    'required' 	=> 'Campo requerido.',
		    'rfc' 		=> 'Revisa el formato del RFC',
		];

        $validator = Validator::make($request->all(), [
	        'tipo_persona_edit' 		=> 'required',
	        'rfc_edit'		 			=> 'required|rfc',
	        'razon_social_edit' 		=> 'required',
	        'representante_legal_edit' 	=> 'required_if:tipo_persona_edit,moral',
	        'df_calle_edit' 			=> '',
	        'df_no_exterior_edit' 		=> '',
	        'df_no_interior_edit' 		=> '',
	        'df_colonia_edit' 			=> '',
	        'df_codigo_postal_edit' 	=> 'numeric',
	        'df_municipio_edit' 		=> '',
	        'df_estado_edit' 			=> '',
        ], $messages);
        		
        if ($validator->fails()) {
	        
            return back()->withErrors($validator)->withInput()->with('show_modal_establecimiento_datos_fiscales_edit', true)->with('datosf_id', $datosf_id);
        }
	    
	    DB::beginTransaction();

	    $establecimiento =  Establecimiento::findOrFail($id);
		$datos_fiscales = DatosFiscales::findOrFail($datosf_id);
		
		$datos_fiscales->cliente_id = $establecimiento->cliente_id;
		$datos_fiscales->persona = $request->tipo_persona_edit;
		$datos_fiscales->rfc = trim($request->rfc_edit);
		$datos_fiscales->razon_social = strtoupper(trim($request->razon_social_edit));
		$datos_fiscales->representante_legal = ucfirst(trim($request->representante_legal_edit));
		$datos_fiscales->calle = ucfirst(trim($request->df_calle_edit));
		$datos_fiscales->numero_interior = trim($request->df_no_interior_edit);
		$datos_fiscales->numero_exterior = trim($request->df_no_exterior_edit);
		$datos_fiscales->colonia = ucfirst(trim($request->df_colonia_edit));
		$datos_fiscales->codigo_postal = trim($request->df_codigo_postal_edit);
		$datos_fiscales->municipio = ucfirst(trim($request->df_municipio_edit));
		$datos_fiscales->estado = ucfirst(trim($request->df_estado_edit));
		$datos_fiscales->created_by = Auth::user()->id;
		$datos_fiscales->updated_by = Auth::user()->id;
		$datos_fiscales->update();

		$establecimiento->cliente_datos_fiscales_id = $datos_fiscales->id;
		$establecimiento->update();	
		
		DB::commit();


        return redirect()->intended('/establecimientos/'.$id);
    }


    public function updateContacto(Request $request, $id, $contacto_id)
    {

		$messages = [
		    'required' => 'Campo requerido.',
		    'email' => 'Formato inválido.',
		];

        $validator = Validator::make($request->all(), [
	        'contacto_nombre_edit' 				=> 'required',
	        'contacto_apellido_paterno_edit' 	=> '',
	        'contacto_apellido_materno_edit' 	=> '',
	        'tipo_email_edit.*' 				=> 'required',
	        'email_edit.*' 						=> 'required|email',
	        'email_edit_id.*' 					=> 'required',
	        'tipo_telefono_edit.*' 				=> 'required',
	        'telefono_edit.*' 					=> 'required',
	        'telefono_edit_id.*'	 			=> 'required',
	        'contacto_comentarios_edit' 		=> '',
        ], $messages);


        if ($validator->fails()) {
	        
            return back()->withErrors($validator)->withInput()->with('show_modal_establecimiento_contacto_edit', true)->with('contacto_id', $contacto_id);
        }
	    
	    DB::beginTransaction();
        
        $establecimiento =  Establecimiento::findOrFail($id);
        $contacto = Contacto::findOrFail($contacto_id);

        $contacto->nombre = trim($request->contacto_nombre_edit);
		$contacto->apellido_paterno = trim($request->contacto_apellido_paterno_edit);
		$contacto->apellido_materno = trim($request->contacto_apellido_materno_edit);
		$contacto->comentarios = trim($request->contacto_comentarios_edit);
		$contacto->update();

		$contacto->establecimientos()->sync([$id => ['created_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]]);
		
		$arr_desc_email = array();
		$arr_email = array();
		$arr_email_id = array();
		$arr_desc_telefono = array();
		$arr_telefono = array();
		$arr_telefono_id = array();
		
		if($request->tipo_email_edit){
			$arr_desc_email = $request->tipo_email_edit;
		}
		if($request->email_edit){
			$arr_email = $request->email_edit;
		}
		if($request->email_edit_id){
			$arr_email_id = $request->email_edit_id;
		}
		if($request->tipo_telefono_edit){
			$arr_desc_telefono = $request->tipo_telefono_edit;
		}
		if($request->telefono_edit){
			$arr_telefono = $request->telefono_edit;
		}
		if($request->telefono_ext_edit){
			$arr_telefono_ext = $request->telefono_ext_edit;
		}
		if($request->telefono_edit_id){
			$arr_telefono_id = $request->telefono_edit_id;
		}

		foreach($contacto->emails as $correo){
			if(!in_array($correo->id, $arr_email_id)){
				$correo->delete();
			}
		}

		foreach($contacto->telefonos as $telefono){
			if(!in_array($telefono->id, $arr_telefono_id)){
				$telefono->delete();
			}
		}
		
        foreach($arr_desc_email AS $key => $value){
	        if(isset($arr_email_id[$key])){
		        $correo = ContactoEmail::findOrFail($arr_email_id[$key]);
		        $correo->valor = $arr_email[$key]; 
		        $correo->tipo = $value;
		        $correo->updated_by = Auth::user()->id;
				$correo->update();
	        }else{
		        $correo = new ContactoEmail;
		        $correo->valor = $arr_email[$key]; 
		        $correo->tipo = $value;
		        $correo->created_by = Auth::user()->id;
		        $correo->updated_by = Auth::user()->id;
		        $correo = $contacto->emails()->save($correo);
	        }
        }
        
        foreach($arr_desc_telefono AS $key => $value){
	        if(isset($arr_telefono_id[$key])){
		        $telefono = ContactoTelefono::findOrFail($arr_telefono_id[$key]);
		        $telefono->valor = $arr_telefono[$key]; 
		        $telefono->tipo = $value;
		        $telefono->updated_by = Auth::user()->id;
				$telefono->update();
	        }else{
		        $telefono = new ContactoTelefono;
		        $telefono->valor = $arr_telefono[$key]; 
		        $telefono->tipo = $value;
		        $telefono->extension = $arr_telefono_ext[$key];
		        $telefono->created_by = Auth::user()->id;
		        $telefono->updated_by = Auth::user()->id;
		        $telefono = $contacto->telefonos()->save($telefono);
	        }
        }
				
		DB::commit();

        return redirect()->intended('/establecimientos/'.$id)->with('show_tab_contactos', true);
    }

    public function asignarConservadores(Request $request, $id)
    {
		$messages = [
		    'asignar_conservador.required' 	=> 'Es necesario seleccionar al menos un conservador.',
		];

        $validator = Validator::make($request->all(), [
	        'asignar_conservador'			=> 'required|exists:conservador,id',
        ], $messages);
        		
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_conservador_asignar', true);
        }

        $establecimiento = Establecimiento::findOrFail($id);

        $producto_id = 0;
        if(count($request->asignar_conservador) > 0){
			foreach($request->asignar_conservador as $conservador_id){
				$conservador = Conservador::findOrFail($conservador_id);
		        $conservador->cliente_id = $establecimiento->cliente->id;
		        $conservador->cliente_establecimiento_id = $establecimiento->id;
		        $conservador->status = 'en_consignacion';
		        $conservador->updated_by = Auth::user()->id;
				$conservador->update();
				$producto_id = $conservador->producto_id;
			}			
		}

		#Store in Precios Table

		#SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO
		if( !$establecimiento->precios->pluck('producto_id')->contains($producto_id) ) {

			$producto = Producto::findOrFail($producto_id);
			$precios_producto = [
                'cliente_id'           	=> $establecimiento->cliente_id,
                'producto_id'           => $producto_id,
                'precio_venta'      	=> $producto->precio_venta,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ];
            $precio_producto_est = new EstablecimientoPrecios($precios_producto);
            $establecimiento->precios()->save($precio_producto_est);
		}

		#Actualizar Puntos de inventario
        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
        $establecimiento->update();

        return redirect()->intended('/establecimientos/'.$id)->with('show_tab_conservadores', true);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    DB::beginTransaction();
	    
		$establecimiento = Establecimiento::findOrFail($id);
		
		$establecimiento->delete();

	    DB::commit();
	    
	    return redirect()->intended('establecimientos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivar($id)
    {
	    DB::beginTransaction();
	    
		$establecimiento = Establecimiento::findOrFail($id);
		
		$establecimiento->restore();

	    DB::commit();
	    
	    return redirect()->intended('establecimientos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyContacto($establecimiento_id, $contacto_id)
    {
        $contacto = Contacto::findOrFail($contacto_id);
        
        DB::beginTransaction();
                
		$contacto->delete();
		
		DB::commit();
		
        return redirect()->intended('/establecimientos/'.$establecimiento_id)->with('show_tab_contactos', true);
    }
    
    public function destroyCuartoFrio($establecimiento_id, $cuartofrio_id)
    {
        $establecimiento = Establecimiento::findOrFail($establecimiento_id);
        $cuartofrio = CuartoFrio::findOrFail($cuartofrio_id);
        $productos = $cuartofrio->productos();

        DB::beginTransaction();

        $borrados=[];
        //DELETE ALL PRODUCTS

		foreach($cuartofrio->productos as $cf_producto){
			#guardar id de productos borrados
			$borrados[] = $cf_producto->producto_id;
			$cf_producto->delete();
		}

		$establecimiento = Establecimiento::findOrFail($establecimiento_id);
        $establecimiento->cuartofrio_id = null;
		$establecimiento->update();
                
		$cuartofrio->delete();

		$productos_asociados = collect($establecimiento->productos_disponibles());

		for ($i=0; $i < count($borrados); $i++) { 
			if( !$productos_asociados->contains($borrados[$i]) ) {
				#Borrar
				DB::table('cliente_establecimiento_precios')->where([
				    ['establecimiento_id', $establecimiento_id],
				    ['producto_id', $borrados[$i]]
				])->delete();
			}
		}


		DB::commit();
		
        return redirect()->intended('/establecimientos/'.$establecimiento_id)->with('show_tab_conservadores', true);
    }

    public function destroyDatosFiscales(Request $request, $establecimiento_id, $datos_fiscales_id) {

    	DB::beginTransaction();
        $datos_fiscales = DatosFiscales::findOrFail($datos_fiscales_id);
        $establecimiento = Establecimiento::findOrFail($establecimiento_id);

        if($establecimiento->cliente_datos_fiscales_id == $datos_fiscales_id)
			$establecimiento->cliente_datos_fiscales_id = null;
		
		//$datos_fiscales->delete();
		$establecimiento->update();
		DB::commit();
		
        return redirect()->intended('/establecimientos/'.$establecimiento_id);
    }
}
