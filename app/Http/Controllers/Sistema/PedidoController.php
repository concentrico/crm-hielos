<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pedido as Pedido;
use App\Models\Pago as Pago;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\PedidoFacturas as PedidoFacturas;
use App\Models\Inventario as Inventario;
use App\Models\InventarioEntradas as InventarioEntradas;
use App\Models\InventarioSalidas as InventarioSalidas;
use App\Models\Conservador as Conservador;
use App\Models\CuartoFrio as CuartoFrio;
use App\Models\Producto as Producto;
use App\Models\ClienteEstablecimiento as ClienteEstablecimiento;
use App\Models\Cliente;
use App\Models\LogUsuario as LogUsuario;
use App\Models\LogInventario as LogInventario;
use App\Models\Corte as Corte;
use App\Models\Parametro as System;
use DB;
use Input;
use Validator;
use Auth;
use Gate;
use File;
use Carbon\Carbon; 
use Mail;
use PDF;

class PedidoController extends Controller
{
    public function index() {

        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $q = Pedido::query();

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');

        // FILTRO BUSQUEDA
        if (Input::has('f_busqueda'))
        {
            $q->join('cliente', 'pedido.cliente_id', 'cliente.id')->join('cliente_establecimiento', 'pedido.cliente_establecimiento_id', '=', 'cliente_establecimiento.id')->select('pedido.*','cliente.nombre_corto as nombre_corto','cliente_establecimiento.sucursal as sucursal')->where('nombre_corto', 'like', '%'. Input::get('f_busqueda') .'%')->orWhere('sucursal', 'like', '%'. Input::get('f_busqueda') .'%')->get();
        }

        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){

            case 'ID' : $orderByString = 'id '.$sort_aux;
                break;

            case 'ESTABLECIMIENTO' : 
                // FILTRO SUCURSAL
                $q->join('cliente_establecimiento', 'pedido.cliente_establecimiento_id', '=', 'cliente_establecimiento.id')->select('pedido.*','cliente_establecimiento.sucursal as sucursal')->get();
                $orderByString = 'sucursal ' . $sort_aux;
                break;

            case 'CLIENTE' : 
                // FILTRO NOMBRE CLIENTE
                $q->join('cliente', 'pedido.cliente_id', 'cliente.id')->select('pedido.*','cliente.nombre_corto as nombre_corto')->get();
                $orderByString = 'nombre_corto ' . $sort_aux;
                break;
            
            case 'STATUS' : $orderByString = 'status '.$sort_aux;
                break;

            case 'FECHAENTREGA' : $orderByString = 'fecha_entrega '.$sort_aux; 
                break;

            case 'ENTREGADO' : $orderByString = 'entregado_at '.$sort_aux; 
                break;

            case 'TOTAL' : $orderByString = 'total '.$sort_aux;
                break;

            case 'CREATEDAT' : $orderByString = 'created_at '.$sort_aux;            
                break;

            case 'CREATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE pedido.created_by = users.id) '.$sort_aux;    
                break;

            case 'UPDATEDAT' : $orderByString = 'updated_at '.$sort_aux;            
                break;

            case 'UPDATEDBY' : $orderByString = '(SELECT nombre FROM users WHERE pedido.updated_by = users.id) '.$sort_aux;  
                break;
   
            //default : $orderByString = '-status DESC';   
            default : $orderByString = 'created_at DESC';   
                break;
        }

        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        // FILTRO STATUS PEDIDO

        if (Input::has('f_status'))
        {
            $q->where('status', '=', Input::get('f_status'));
        }

        // NUM RESULTADOS
        if (Input::has('f_num_resultados')) {
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['pedidos'] = $q->paginate($num_resultados);
            
            if( count($data['pedidos']) == 0 )
                $data['pedidos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
        }else{
            $data['pedidos'] = $q->paginate(25);
        }
        
        return view('sistema.pedidos.index', $data);               
    }

    public function store(Request $request) {

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
            'hora_deseada.date_format' => 'El formato requerido es HH:MM.',
        ];

        if ($request->otra_direccion == '1') {
            # Evento
            $validator = Validator::make($request->all(), [
                'cliente_establecimiento_cliente'   => 'required|exists:cliente,id',
                'evento_calle'   => 'required',
                'evento_no_exterior'   => 'required',
                'evento_colonia'   => 'required',
                'evento_municipio'   => 'required',
                'fecha_entrega' => 'required|date_format:"d/m/Y"',
                'hora_deseada' => 'date_format:H:i',

                'producto_pedido.*'              => 'required',
                'precio_pedido.*'              => 'required',
                'cantidad_pedido.*'              => 'required',

                'chofer_asignado'        => 'required|exists:users,id',
                'forma_pago'        => 'required|exists:forma_pago,id',
                'condicion_pago'        => 'required|exists:condicion_pago,id',
            ], $messages);
        } else {
            # Establecimiento
            $validator = Validator::make($request->all(), [
                'cliente_establecimiento_cliente'   => 'required|exists:cliente,id',
                'cliente_establecimiento_sucursal'   => 'required|exists:cliente_establecimiento,id',
                'fecha_entrega' => 'required|date_format:"d/m/Y"',
                'hora_deseada' => 'date_format:H:i',

                'producto_pedido.*'              => 'required',
                'precio_pedido.*'              => 'required',
                'cantidad_pedido.*'              => 'required',

                'chofer_asignado'        => 'required|exists:users,id',
                'forma_pago'        => 'required|exists:forma_pago,id',
                'condicion_pago'        => 'required|exists:condicion_pago,id',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        $pedido = new Pedido;

        $pedido->cliente_id = $request->cliente_establecimiento_cliente;

        if ($request->cliente_establecimiento_sucursal != '')
            $pedido->cliente_establecimiento_id = $request->cliente_establecimiento_sucursal;
        else
            $pedido->cliente_establecimiento_id = null;

        $fecha_entrega = Carbon::createFromFormat('d/m/Y', $request->fecha_entrega)->formatLocalized('%Y-%m-%d');
        $pedido->fecha_entrega = $fecha_entrega;
        $pedido->hora_deseada = $request->hora_deseada;

        $pedido->es_evento = $request->es_evento;
        $pedido->forma_pago_id = $request->forma_pago;
        $pedido->condicion_pago_id = $request->condicion_pago;
        $pedido->chofer_id = $request->chofer_asignado;

        $pedido->status = 1;
        $pedido->created_by = Auth::user()->id;
        $pedido->updated_by = Auth::user()->id;

        $arr_producto_id = [];
        if($request->producto_pedido){
            $arr_producto_id = $request->producto_pedido;
        }
        $arr_producto_cantidad = [];     
        if($request->cantidad_pedido){
            $arr_producto_cantidad = $request->cantidad_pedido;
        }
        $arr_producto_precio = [];
        if($request->precio_pedido){
            $arr_producto_precio = $request->precio_pedido;
        }

        $total=0.0;
        $subtotal = 0.0;
        $iva = 0.0;
        $total_mas_iva = 0.0;
        $total_iva = 0.0;
        $totales = [];
        $imp = [];
        foreach($arr_producto_id AS $key => $value){
            $subtotal = floatval($arr_producto_cantidad[$key])*floatval($arr_producto_precio[$key]);
            $totales[] = $subtotal;
            $total += $subtotal;
        }
        foreach($arr_producto_id AS $key => $value){
            $producto = Producto::findOrFail($arr_producto_id[$key]);
            $iva_producto = $producto->iva;
            $imp[] = $iva_producto;
        }

        for ($i = 0; $i < count($totales); $i++) {
         if( $totales[$i] != '') {
            $mas_iva = $totales[$i]*($imp[$i]/100);
            $total_iva += $mas_iva;
            $total_mas_iva += $totales[$i]+$mas_iva;
          } else {
            $total_mas_iva += 0.0;
            $total_iva += 0.0;
          }
        }
            
        $pedido->subtotal = $total;
        $pedido->iva = $total_iva;
        $pedido->total = $total_mas_iva;

        if ($request->otra_direccion == '1' ){
            $pedido->evento_calle = $request->evento_calle;
            $pedido->evento_numero_exterior = $request->evento_no_exterior;
            $pedido->evento_numero_interior = null;
            $pedido->evento_colonia = $request->evento_colonia;
            $pedido->evento_municipio = $request->evento_municipio;
            $pedido->evento_estado = 'Nuevo León';
        }

        $pedido->evento_direccion_indicaciones = $request->evento_direccion_indicaciones;

        $pedido->save();

        //Atach Productos
        foreach($arr_producto_id AS $key => $value){
            $producto_pedido = new PedidoProductos;
            $producto_pedido->pedido_id = $pedido->id;
            $producto_pedido->producto_id = $arr_producto_id[$key];
            $producto_pedido->cantidad_requerida = $arr_producto_cantidad[$key];
            $producto_pedido->cantidad_recibida = null;
            $producto_pedido->precio_unitario = $arr_producto_precio[$key];
            $subtotal = floatval($arr_producto_cantidad[$key])*floatval($arr_producto_precio[$key]);
            $producto_pedido->subtotal = $subtotal;
            $producto_pedido->created_by = Auth::user()->id;
            $producto_pedido->updated_by = Auth::user()->id;

            $pedido->productos()->save($producto_pedido);
        }

        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id);

    }

    public function storePagoPedido(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_pago_pedido'          => 'required|date_format:d/m/Y',
            'forma_pago_pedido'          => 'required|exists:forma_pago,id',
            'monto_pago_pedido'          => 'required|numeric',
            'comprobante_pago_pedido'    => 'mimes:pdf,xls,xlsx,doc,docx,jpg,jpeg,png,ppt,pptx',
            'comentarios_pago_pedido'    => '',
        ],[
            'fecha_pago_pedido.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_pago_create', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);

        $pago = new Pago();
        $pago->pedido_id = $pedido->id;
        $pago->forma_pago_id = $request->forma_pago_pedido;
        $fecha_pago = Carbon::createFromFormat('d/m/Y', $request->fecha_pago_pedido)->formatLocalized('%Y-%m-%d');
        $pago->fecha_pago = $fecha_pago;
        $pago->monto = $request->monto_pago_pedido;
        $pago->comentarios = $request->comentarios_pago_pedido;
        $pago->created_by = Auth::user()->id;
        $pago->updated_by = Auth::user()->id;

        $pago = $pedido->pagos()->save($pago);

        $comprobante = $request->file('comprobante_pago_pedido');
        
        if($comprobante != null){
            
            $file_name = time() . $comprobante->getClientOriginalName();
                                        
            $file_path = 'uploads/pagos/'.$id.'/';
    
            $comprobante->move($file_path, $file_name);
    
            $pago->comprobante_path = $file_path . $file_name;
    
            $pago->update();
        }


        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();


        DB::commit();

        return redirect()->intended('/pedidos/'.$pedido->id)->with('show_tab_cobranza', true);
    }

    public function storeFacturaPedido(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_facturacion'          => 'required|date_format:d/m/Y',
            'folio'                      => 'required',
            'subtotal_factura'          => 'required|numeric',
            'iva_factura'               => 'required|numeric',
            'total_factura'          => 'required|numeric',
            'pdf_factura'    => 'mimes:pdf',
            'xml_factura'    => 'mimes:xml',
            'comentarios_factura'    => '',
        ],[
            'fecha_facturacion.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_factura_create', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);

        $factura = new PedidoFacturas();
        $factura->pedido_id = $pedido->id;
        $factura->cliente_id = $pedido->cliente_id;
        $factura->establecimiento_id = $pedido->cliente_establecimiento_id;
        $factura->folio = $request->folio;

        $fecha_facturacion = Carbon::createFromFormat('d/m/Y', $request->fecha_facturacion);
        $fecha_vencimiento = (clone $fecha_facturacion);
        $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias); 

        $factura->fecha_facturacion = $fecha_facturacion->formatLocalized('%Y-%m-%d');
        $factura->fecha_vencimiento = $fecha_vencimiento->formatLocalized('%Y-%m-%d');

        $factura->subtotal = $request->subtotal_factura;
        $factura->iva = $request->iva_factura;
        $factura->total = $request->total_factura;
        $factura->comentarios = $request->comentarios_factura;
        $factura->created_by = Auth::user()->id;
        $factura->updated_by = Auth::user()->id;
        $factura = $pedido->facturas()->save($factura);

        $pdf = $request->file('pdf_factura');
        if($pdf != null){
            $file_name = time() . $pdf->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/pdf/';
            $pdf->move($file_path, $file_name);
            $factura->pdf_path = $file_path . $file_name;
            $factura->update();
        }

        $xml = $request->file('xml_factura');
        if($xml != null){
            $file_name = time() . $xml->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/xml/';
            $xml->move($file_path, $file_name);
            $factura->xml_path = $file_path . $file_name;
            $factura->update();
        }

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();


        DB::commit();

        return redirect()->intended('/pedidos/'.$pedido->id)->with('show_tab_cobranza', true);
    }

    public function updatePagoPedido(Request $request, $id, $pago_id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_pago_pedido_edit'          => 'date_format:d/m/Y',
            'forma_pago_pedido_edit'          => 'required|exists:forma_pago,id',
            'monto_pago_pedido_edit'          => 'required|numeric',
            'archivo_orden_edit'        => 'mimes:pdf,xls,xlsx,doc,docx,jpg,jpeg,png,ppt,pptx',
            'comentarios_pago_pedido_edit'    => '',
        ],[
            'fecha_pago_pedido_edit.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);
                

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_pago_edit', true);
        }
        
        DB::beginTransaction();

        $pedido =  Pedido::findOrFail($id);
        $pago =  Pago::findOrFail($pago_id);

        $fecha_pago = Carbon::createFromFormat('d/m/Y', $request->fecha_pago_pedido_edit)->formatLocalized('%Y-%m-%d');
        $pago->fecha_pago = $fecha_pago;
        $pago->forma_pago_id = $request->forma_pago_pedido_edit;
        $pago->monto = $request->monto_pago_pedido_edit;
        $pago->comentarios = $request->comentarios_pago_pedido_edit;
        $pago->updated_by = Auth::user()->id;

        $comprobante = $request->file('comprobante_pago_pedido_edit');
        
        if($comprobante != null){
            
            $file_name = time() . $comprobante->getClientOriginalName();
                                        
            $file_path = 'uploads/pagos/'.$id.'/';
    
            $comprobante->move($file_path, $file_name);
    
            $pago->comprobante_path = $file_path . $file_name;
    
            $pago->update();
        }

        $pago->update();

        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();
                
        DB::commit();

        return redirect()->intended('/pedidos/' . $pedido->id)->with('show_tab_cobranza', true);
    }

    public function updateFacturaPedido(Request $request, $id, $factura_id)
    {

        $validator = Validator::make($request->all(), [
            'fecha_facturacion_edit'          => 'required|date_format:d/m/Y',
            'folio_edit'                      => 'required',
            'subtotal_factura_edit'          => 'required|numeric',
            'iva_factura_edit'               => 'required|numeric',
            'total_factura_edit'          => 'required|numeric',
            'pdf_factura_edit'    => 'mimes:pdf',
            'xml_factura_edit'    => 'mimes:xml',
            'comentarios_factura_edit'    => '',
        ],[
            'fecha_facturacion_edit.date_format'  => 'El formato requerido es dd/mm/YYYY.',
            'required'  => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_factura_edit', true);
        }
        
                
        DB::beginTransaction();

        $pedido = Pedido::findOrFail($id);
        $factura = PedidoFacturas::where('id', $factura_id)->where('pedido_id', $id)->first();

        $fecha_facturacion = Carbon::createFromFormat('d/m/Y', $request->fecha_facturacion_edit);
        $fecha_vencimiento = (clone $fecha_facturacion);
        $fecha_vencimiento = $fecha_vencimiento->addDays($pedido->condicion_pago->dias); 

        $factura->fecha_facturacion = $fecha_facturacion->formatLocalized('%Y-%m-%d');
        $factura->fecha_vencimiento = $fecha_vencimiento->formatLocalized('%Y-%m-%d');

        $factura->folio = $request->folio_edit;
        $factura->subtotal = $request->subtotal_factura_edit;
        $factura->iva = $request->iva_factura_edit;
        $factura->total = $request->total_factura_edit;
        $factura->comentarios = $request->comentarios_factura_edit;
        $factura->updated_by = Auth::user()->id;
        $factura->update();

        $pdf = $request->file('pdf_factura_edit');
        if($pdf != null){
            $file_name = time() . $pdf->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/pdf/';
            $pdf->move($file_path, $file_name);
            $factura->pdf_path = $file_path . $file_name;
            $factura->update();
        }

        $xml = $request->file('xml_factura_edit');
        if($xml != null){
            $file_name = time() . $xml->getClientOriginalName();
            $file_path = 'uploads/facturas/'.$id.'/xml/';
            $xml->move($file_path, $file_name);
            $factura->xml_path = $file_path . $file_name;
            $factura->update();
        }

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();
                
        DB::commit();

        return redirect()->intended('/pedidos/' . $pedido->id)->with('show_tab_cobranza', true);
    }

    public function update(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
            'hora_deseada.date_format' => 'El formato requerido es HH:MM.',
        ];

        $validator = Validator::make($request->all(), [
            'fecha_entrega' => 'required|date_format:"d/m/Y"',
            //'hora_deseada' => 'date_format:H:i',
            'chofer_asignado_edit'        => 'required|exists:users,id',
            'es_evento'        => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_edit', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        $fecha_entrega = Carbon::createFromFormat('d/m/Y', $request->fecha_entrega)->formatLocalized('%Y-%m-%d');
        $pedido->fecha_entrega = $fecha_entrega;
        $pedido->hora_deseada = $request->hora_deseada;
        $pedido->es_evento = $request->es_evento;
        $pedido->chofer_id = $request->chofer_asignado_edit;
        $pedido->comentarios = $request->comentarios_pedido;
        $pedido->updated_by = Auth::user()->id;

        $pedido->update();

        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id);

    }

    public function updateDireccion(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'evento_calle'   => 'required',
            'evento_numero_exterior'   => 'required',
            'evento_colonia'   => 'required',
            'evento_municipio'   => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_direccion', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);
        $pedido->evento_calle = $request->evento_calle;
        $pedido->evento_numero_exterior = $request->evento_numero_exterior;
        if($request->evento_numero_interior == '')
            $pedido->evento_numero_interior = null;

        $pedido->evento_colonia = $request->evento_colonia;
        $pedido->evento_codigo_postal = $request->evento_codigo_postal;
        $pedido->evento_municipio = $request->evento_municipio;
        $pedido->evento_estado = 'Nuevo León';
        $pedido->evento_direccion_indicaciones = $request->evento_direccion_indicaciones;
        $pedido->updated_by = Auth::user()->id;

        $pedido->update();

        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id);

    }

    public function updateProductoPedido(Request $request, $id, $producto_pedido_id ){
        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        if($pedido->status > 2) {
            $validator = Validator::make($request->all(), [
                'precio_unitario_edit'   => 'required|numeric',
                'cantidad_requerida_edit'   => 'required|numeric',
                'cantidad_recibida_edit'   => 'required|numeric',
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                'precio_unitario_edit'   => 'required|numeric',
                'cantidad_requerida_edit'   => 'required|numeric',
                //'cantidad_recibida_edit'   => 'required|numeric',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_producto_pedido_edit', true);
        }

        $producto_pedido = PedidoProductos::findOrFail($producto_pedido_id);
        

        $subtotal_producto=0.0;
        if ($request->cantidad_recibida_edit != '') {
            # Subtotal en base a lo recibido
            $subtotal_producto = floatval($request->cantidad_recibida_edit)*floatval($request->precio_unitario_edit);
        } else {
            # Subtotal en base a lo requerido
            $subtotal_producto = floatval($request->cantidad_requerida_edit)*floatval($request->precio_unitario_edit);
        }
        $producto_pedido->subtotal = $subtotal_producto;
        $producto_pedido->updated_by = Auth::user()->id;


        // REVISAR INVENTARIOS Y HACER MOVIMIENTO DE SER NECESARIO.
        $inventario = Inventario::where('producto_id', $producto_pedido->producto_id)->first();
        $cant_actual = $inventario->cantidad_disponible;

        if ($pedido->status == 1 || $pedido->status == 2) {
            if ($producto_pedido->cantidad_requerida != $request->cantidad_requerida_edit) {
                if ( $request->cantidad_requerida_edit > $producto_pedido->cantidad_requerida) {
                    # Si es mayor la cantidad requerida (Salida Inventario - Pedido)
                    $cantidad_movimiento = $request->cantidad_requerida_edit - $producto_pedido->cantidad_requerida;
                    $cantidad_agregar = $cantidad_movimiento;

                    if (intval($cant_actual) - $cantidad_agregar < 0) {
                        #no hay inventario
                        $editProductoFailed = [
                            'editProductoFailed' => 'No hay inventario disponible para éste producto.',
                        ];
                        return back()->withErrors($editProductoFailed)->withInput()->with('show_modal_producto_pedido_edit', true);
                    }

                } else {
                    # Si es menor la cantidad entregada (Entrada Inventario - Devolucion)
                    $cantidad_movimiento = $producto_pedido->cantidad_requerida-$request->cantidad_requerida_edit;
                }

                if ($pedido->status == 2) {
                    if ($producto_pedido->precio_unitario == 0.00)
                        $this->registrarMovimientoInventario2($id, 'salida', 'cortesia', $producto_pedido->id, $cantidad_movimiento );
                    else
                        $this->registrarMovimientoInventario2($id, 'salida', 'pedido', $producto_pedido->id, $cantidad_movimiento );
                }
                
            }
        }
        else if ($pedido->status == 3) {

            if ($producto_pedido->cantidad_recibida != $request->cantidad_recibida_edit) {

                if ( $request->cantidad_recibida_edit > $producto_pedido->cantidad_recibida) {
                    # Si es mayor la cantidad requerida (Salida Inventario - Pedido/Cortesía)
                    $cantidad_movimiento = $request->cantidad_recibida_edit - $producto_pedido->cantidad_recibida;
                    $cantidad_agregar = $cantidad_movimiento;

                    if (intval($cant_actual) - $cantidad_agregar < 0) {
                        #no hay inventario
                        $editProductoFailed = [
                            'editProductoFailed' => 'No hay inventario disponible para éste producto.',
                        ];
                        return back()->withErrors($editProductoFailed)->withInput()->with('show_modal_producto_pedido_edit', true);
                    }

                    if ($producto_pedido->precio_unitario == 0.00)
                        $this->registrarMovimientoInventario2($id, 'salida', 'cortesia', $producto_pedido->id, $cantidad_movimiento );
                    else
                        $this->registrarMovimientoInventario2($id, 'salida', 'pedido', $producto_pedido->id, $cantidad_movimiento );

                } else {
                    # Si es menor la cantidad entregada (Entrada Inventario - Devolucion)
                    $cantidad_movimiento = $producto_pedido->cantidad_recibida-$request->cantidad_recibida_edit;
                    $this->registrarMovimientoInventario2($id, 'entrada', 'devolucion', $producto_pedido->id, $cantidad_movimiento );
                }
                
            }
        } 


        $producto_pedido->cantidad_requerida = $request->cantidad_requerida_edit;
        $producto_pedido->cantidad_recibida = $request->cantidad_recibida_edit;
        $producto_pedido->precio_unitario = $request->precio_unitario_edit;
        $producto_pedido->update();

        // Actualizar Total del pedido
        $pedido->updateTotales();

        // Actualizar Remision del Pedido
        $pedido->generaRemision();
        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id)->with('update_pedido_success', true);
    }

    public function addCortesiaPedido(Request $request, $id ){
        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        if($pedido->status > 2) {
            $validator = Validator::make($request->all(), [
                'cortesia_agregar'   => 'required',
                'cantidad_requerida_cortesia'   => 'required|numeric',
                'cantidad_recibida_cortesia'   => 'required|numeric',
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                'cortesia_agregar'   => 'required',
                'cantidad_requerida_cortesia'   => 'required|numeric',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cortesia_pedido_agregar', true);
        }

        $producto_pedido = new PedidoProductos;
        $producto_pedido->pedido_id = $pedido->id;
        $producto_pedido->producto_id = $request->cortesia_agregar;
        $producto_pedido->cantidad_requerida = $request->cantidad_requerida_cortesia;
        $producto_pedido->cantidad_recibida = $request->cantidad_recibida_cortesia;
        $producto_pedido->precio_unitario = 0.00;
        $subtotal_producto=0.0;
        $producto_pedido->subtotal = $subtotal_producto;
        $producto_pedido->created_by = Auth::user()->id;
        $producto_pedido->updated_by = Auth::user()->id;

        $inventario = Inventario::where('producto_id', $request->cortesia_agregar)->first();

        if ($pedido->status == 1 || $pedido->status == 2)
            $cantidad = $request->cantidad_requerida_cortesia;
        else
            $cantidad = $request->cantidad_recibida_cortesia;

        $cant_actual = $inventario->cantidad_disponible;
        $cant_nueva = intval($cant_actual)-$cantidad;

        if ($cant_nueva < 0) {
            #no hay inventario
            $addCortesiaFailed = [
                'addCortesiaFailed' => 'No hay inventario disponible para éste producto.',
            ];
            return back()->withErrors($addCortesiaFailed)->withInput()->with('show_modal_cortesia_pedido_agregar', true);
        }

        $pedido->productos()->save($producto_pedido);

        // Dar de baja de inventario según status del pedido actual

        if ($pedido->status == 2 || $pedido->status == 3) {
            # Dar de baja como cortesía
            $this->registrarMovimientoInventario2($id, 'salida', 'cortesia', $producto_pedido->id,$cantidad);
        }

        // Actualizar Total del pedido
        $pedido->updateTotales();

        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id)->with('update_pedido_success', true);
    }

    public function addProductoPedido(Request $request, $id ){
        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
        ];

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        if($pedido->status > 2) {
            $validator = Validator::make($request->all(), [
                'producto_agregar'   => 'required',
                'precio_unitario'   => 'required|numeric',
                'cantidad_requerida'   => 'required|numeric',
                'cantidad_recibida'   => 'required|numeric',
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                'producto_agregar'   => 'required',
                'precio_unitario'   => 'required|numeric',
                'cantidad_requerida'   => 'required|numeric',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_producto_pedido_agregar', true);
        }

        $producto_pedido = new PedidoProductos;
        $producto_pedido->pedido_id = $pedido->id;
        $producto_pedido->producto_id = $request->producto_agregar;
        $producto_pedido->cantidad_requerida = $request->cantidad_requerida;
        $producto_pedido->cantidad_recibida = $request->cantidad_recibida;
        $producto_pedido->precio_unitario = $request->precio_unitario;

        $subtotal_producto=0.0;
        if ($request->cantidad_recibida != '') {
            # Subtotal en base a lo recibido
            $subtotal_producto = floatval($request->cantidad_recibida)*floatval($request->precio_unitario);
        } else {
            # Subtotal en base a lo requerido
            $subtotal_producto = floatval($request->cantidad_requerida)*floatval($request->precio_unitario);
        }

        $producto_pedido->subtotal = $subtotal_producto;
        $producto_pedido->created_by = Auth::user()->id;
        $producto_pedido->updated_by = Auth::user()->id;

        $inventario = Inventario::where('producto_id', $request->producto_agregar)->first();

        if ($pedido->status == 1 || $pedido->status == 2)
            $cantidad = $request->cantidad_requerida;
        else
            $cantidad = $request->cantidad_recibida;

        $cant_actual = $inventario->cantidad_disponible;
        $cant_nueva = intval($cant_actual)-$cantidad;

        if ($cant_nueva < 0) {
            #no hay inventario
            $addProductoFailed = [
                'addProductoFailed' => 'No hay inventario disponible para éste producto.',
            ];
            return back()->withErrors($addProductoFailed)->withInput()->with('show_modal_producto_pedido_agregar', true);
        }

        $pedido->productos()->save($producto_pedido);

        // Dar de baja de inventario según status del pedido actual

        if ($pedido->status == 2 || $pedido->status == 3) {
            # Dar de baja como cortesía
            $this->registrarMovimientoInventario2($id, 'salida', 'pedido', $producto_pedido->id, $cantidad);
        }

        // Actualizar Total del pedido
        $pedido->updateTotales();

        DB::commit();

        return redirect()->intended('pedidos/'.$pedido->id)->with('update_pedido_success', true);
    }
    
    public function updateTotales($id){

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        $total=0.0;
        $subtotal = 0.0;
        $iva = 0.0;
        $total_mas_iva = 0.0;
        $total_iva = 0.0;
        $totales = [];
        $imp = [];
        foreach($pedido->productos AS $producto){

            if ($producto->cantidad_recibida_edit != '' || $producto->cantidad_recibida_edit != null) {
                # Subtotal en base a lo recibido
                $subtotal = floatval($producto->cantidad_recibida)*floatval($producto->precio_unitario);
                $producto->subtotal = $subtotal;
                $producto->update();
            } else {
                # Subtotal en base a lo requerido
                $subtotal = floatval($producto->cantidad_requerida)*floatval($producto->precio_unitario);
                $producto->subtotal = $subtotal;
                $producto->update();
            }

            $totales[] = $subtotal;
            $total += $subtotal;
        }

        foreach($pedido->productos AS $producto){
            $producto = Producto::findOrFail($producto->producto_id);
            $iva_producto = $producto->iva;
            $imp[] = $iva_producto;
        }

        for ($i = 0; $i < count($totales); $i++) {
         if( $totales[$i] != '') {
            $mas_iva = $totales[$i]*($imp[$i]);
            $total_iva += $mas_iva;
            $total_mas_iva += $totales[$i]+$mas_iva;
          } else {
            $total_mas_iva += 0.0;
            $total_iva += 0.0;
          }
        }

        $pedido->subtotal = $total;
        $pedido->iva = $total_iva;
        $pedido->total = $total_mas_iva;
        $pedido->updated_by = Auth::user()->id;
        $pedido->update();

        DB::commit();
    }


    public function cambiarStatusCobranza(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
            'status_cobranza'   => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cambiar_status_cobranza', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);
        $pedido->status_cobranza = $request->status_cobranza;

        $pedido->update();

        DB::commit();

        return redirect()->intended('/pedidos/' . $id)->with('show_tab_cobranza', true);

    }

    public function cambiarStatusFacturacion(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
            'status_facturacion'   => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cambiar_status_facturacion', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);
        $pedido->status_facturacion = $request->status_facturacion;

        $pedido->update();

        DB::commit();

        return redirect()->intended('/pedidos/' . $id)->with('show_tab_cobranza', true);

    }

    public function cambiarStatus(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
        ];

        $validator = Validator::make($request->all(), [
                'status'   => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_cambiar_status', true);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($id);

        switch ($pedido->status) {
            case '1': // Programado
            $status_cambiar = ['1', '2', '3', '5'];
                break;
            case '2': // En camino
                $status_cambiar = ['1', '2', '3', '5'];
            break;
            case '3': // Entregado
                $status_cambiar = ['3', '4'];
            break;
            case '4': // Devolución
                $status_cambiar = ['4'];
            break;
            case '5': // Cancelado
                $status_cambiar = ['1','5'];
            break;
            default:
                $status_cambiar = [];
            break;
        }

        if (!in_array($request->status, $status_cambiar)  ) {
            # Se valida que se pueda cambiar a ese status
            $statusFailed = [
                'statusFailed' => 'No es posible cambiar el Pedido a este status.',
            ];
            return back()->withErrors($statusFailed)->withInput()->with('show_modal_cambiar_status', true);

        } else {

            switch ($request->status) {
                case '1':
                    # Cambiar a Programado
                    /* 
                        (Cuando pasa de cancelado a programado)
                        - Status Cobranza: pendiente
                        - Status Facturacion: no facturado

                        (Cuando pasa de en camino a programado)
                        - Se registra entrada devolución
                    */

                    if ($pedido->status == 5) {
                        # Pasa de Cancelado a Programado
                        $pedido->status = $request->status;
                        $pedido->status_cobranza = 1;
                        $pedido->status_facturacion = 1;
                    } else if ($pedido->status == 2) {
                        # Pasa de En Camino a Programado

                        foreach ($pedido->productos as $producto_pedido) {
                            # Para cada producto del pedido hacer movimiento inventario
                            $this->registrarMovimientoInventario($id, 'entrada', 'devolucion', $producto_pedido->id);
                        }
                        $pedido->status = $request->status;
                        
                    }
                    
                    break;
                
                case '2':
                    # Cambiar a En Camino
                    /* 
                        (Cuando pasa de programado a en camino)
                        - Se da salida como pedido
                    */

                    if ($pedido->status == 1) {
                        # Pasa  Programado a En camino

                        foreach ($pedido->productos as $producto_pedido) {
                            # Para cada producto del pedido hacer movimiento inventario
                            
                            /* REVISAR SI HAY ITEMS DE CORTESÍA */
                            if ($producto_pedido->precio_unitario == 0.00)
                                $this->registrarMovimientoInventario($id, 'salida', 'cortesia', $producto_pedido->id);
                            else
                                $this->registrarMovimientoInventario($id, 'salida', 'pedido', $producto_pedido->id);
                        }
                        $pedido->status = $request->status;
                    }
                    break;

                case '3':
                    # Cambiar a Entregado
                    /* 
                        (Cuando pasa de programado/en camino a entregado)
                        - Se da salida como pedido
                    */
                    if ($pedido->status == 1) {
                        # Pasa  Programado a entregado

                        foreach ($pedido->productos as $producto_pedido) {
                            # Para cada producto del pedido hacer movimiento inventario

                            /* REVISAR SI HAY ITEMS DE CORTESÍA */
                            if ($producto_pedido->precio_unitario == 0.00)
                                $this->registrarMovimientoInventario($id, 'salida', 'cortesia', $producto_pedido->id);
                            else
                                $this->registrarMovimientoInventario($id, 'salida', 'pedido', $producto_pedido->id);

                            // Actualizar cantidad recibida=entregada en pedido productos
                            $producto_pedido->cantidad_recibida = $producto_pedido->cantidad_requerida;
                            $producto_pedido->updated_by = Auth::user()->id;
                            $producto_pedido->update();
                        }

                        
                    } else if ($pedido->status == 2) {
                        # Pasa En camino a entregado
                        foreach ($pedido->productos as $producto_pedido) {

                            // Actualizar cantidad recibida=entregada en pedido productos
                            $producto_pedido->cantidad_recibida = $producto_pedido->cantidad_requerida;
                            $producto_pedido->updated_by = Auth::user()->id;
                            $producto_pedido->update();
                        }
                    }
                    $pedido->status = $request->status;
                    $pedido->entregado_at = Carbon::now();
                    //$pedido->generaRemision();
                    break;

                case '4':
                    # Cambiar a Devolucion
                    /*
                        - Se da entrada como devolución
                        - Se da salida como merma
                        - Status Cobranza: incobrable
                        - Status Facturacion: infacturable
                    */
                    #Aqui se hace movimientos de inventario

                        # Pasa de Entregado a Devolucion

                    foreach ($pedido->productos as $producto_pedido) {
                        # Para cada producto del pedido hacer movimiento inventario
                        $this->registrarMovimientoInventario($id, 'entrada', 'devolucion', $producto_pedido->id);
                    }

                    foreach ($pedido->productos as $producto_pedido) {
                        # Para cada producto del pedido hacer movimiento inventario
                        $this->registrarMovimientoInventario($id, 'salida', 'merma', $producto_pedido->id);
                    }


                    $pedido->status_cobranza = 4;
                    $pedido->status_facturacion = 4;
                    $pedido->status = $request->status;
                    break;

                case '5':
                    # Cambiar a Cancelado
                    /*
                        - Status Cobranza: incobrable
                        - Status Facturacion: infacturable

                        Aqui se hace movimientos de inventario
                        - Pasa a Devolucion
                    */

                    foreach ($pedido->productos as $producto_pedido) {
                        # Para cada producto del pedido hacer movimiento inventario
                        if ($pedido->status == 2 ) {
                            $this->registrarMovimientoInventario($id, 'entrada', 'devolucion', $producto_pedido->id);
                        }
                    }

                    $pedido->status = $request->status;
                    $pedido->status_cobranza = 4;
                    $pedido->status_facturacion = 4;

                    // Registro de Log
                    $log_usuario = new LogUsuario;
                    $log_usuario->user_id = Auth::user()->id;
                    $log_usuario->tipo = 'pedido';
                    $log_usuario->comentarios = 'El usuario ha cancelado el Pedido #<b>'.$id.'</b>';

                    break;
            }

            $pedido->updated_by = Auth::user()->id;
            $pedido->update();

            DB::commit();

            return redirect()->intended('pedidos/'.$pedido->id)->with('update_pedido_success', true);

        }

    }

    public function registrarMovimientoInventario($pedido_id, $tipo_movimiento, $movimiento, $item_id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($pedido_id);

        $producto_pedido = PedidoProductos::findOrFail($item_id);
        $producto_id = $producto_pedido->producto_id;
        $inventario = Inventario::where('producto_id', $producto_id)->first();
        //$producto_pedido = PedidoProductos::where('pedido_id', $pedido_id)->where('producto_id', $producto_id)->first();
        

        if ($tipo_movimiento == 'entrada') {

            if ($pedido->status == 2) { //si es de en camino a programado
                $cantidad = $producto_pedido->cantidad_requerida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            } else if ($pedido->status == 3) { //si es de entregado a devolucion
                $cantidad = $producto_pedido->cantidad_recibida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $inv_entrada = new InventarioEntradas($entrada);
            $inventario->entradas()->save($inv_entrada);

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)+$cantidad;
            $inventario->cantidad_disponible = $cant_nueva;

        } else if($tipo_movimiento == 'salida') {


            if ($pedido->status == 1) { //si es de programado a en camino
                $cantidad = $producto_pedido->cantidad_requerida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

            } else if ($pedido->status == 3) { //si es de entregado a devolucion
                $cantidad = $producto_pedido->cantidad_recibida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)-$cantidad;

            if ($cant_nueva > 0) {
                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $cantidad;
                $inv_salida = new InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);
            }

        }

        $inventario->update();
        DB::commit();

    }

    # ESTA FUNCIÓN ES PARA LOS AGREGADOS Y EDICIONES DE ITEMS (PEDIDO)
    public function registrarMovimientoInventario2($pedido_id, $tipo_movimiento, $movimiento, $item_id, $cantidad) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        DB::beginTransaction();
        $pedido = Pedido::findOrFail($pedido_id);

        $producto_pedido = PedidoProductos::findOrFail($item_id);
        $producto_id = $producto_pedido->producto_id;
        $inventario = Inventario::where('producto_id', $producto_id)->first();
        //$producto_pedido = PedidoProductos::where('pedido_id', $pedido_id)->where('producto_id', $producto_id)->first();
        
        if ($tipo_movimiento == 'entrada') {

            if ($pedido->status == 2) { // Si actualmente el pedido está EN CAMINO
                //$cantidad = $producto_pedido->cantidad_requerida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            } else if ($pedido->status == 3) { //Si actualmente el pedido está en entregado
                //$cantidad = $producto_pedido->cantidad_recibida;
                $entrada = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_entrada'      => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $inv_entrada = new InventarioEntradas($entrada);
            $inventario->entradas()->save($inv_entrada);

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)+$cantidad;
            $inventario->cantidad_disponible = $cant_nueva;

        } else if($tipo_movimiento == 'salida') {

            if ($pedido->status == 2) { //Si actualmente el pedido está EN CAMINO
                //$cantidad = $producto_pedido->cantidad_requerida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];

            } else if ($pedido->status == 3) { // Si actualmente el pedido está ENTREGADO
                //$cantidad = $producto_pedido->cantidad_recibida;
                $salida = [
                    'producto_id'           => $inventario->producto_id,
                    'cantidad_salida'       => $cantidad,
                    'motivo'                => $movimiento,
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
            }

            $cant_actual = $inventario->cantidad_disponible;
            $cant_nueva = intval($cant_actual)-$cantidad;

            if ($cant_nueva > 0) {
                $inventario->cantidad_disponible = $cant_nueva;
                $inventario->ultima_salida = $cantidad;
                $inv_salida = new InventarioSalidas($salida);
                $inventario->salidas()->save($inv_salida);
            }

        }

        $inventario->update();
        DB::commit();

    }

    public function realizarCorte(Request $request, $id, $pedido_id) {
        if (!isset($request->cantidad_actual_corte) ) {
            $corteFailed = [
                'corteFailed' => 'Es necesario hacer el corte de todos los productos',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Sólo se permiten números.',
        ];

        $validator = Validator::make($request->all(), [
            'cantidad_actual_corte.*'           => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_realizar_corte', true);
        }

        DB::beginTransaction();
        $establecimiento = ClienteEstablecimiento::findOrFail($id);

        #Validar capacidades actuales por producto
        $productos_failed = [];
        for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $capacidad_maxima = $establecimiento->check_cantidad_maxima_producto($request->producto_corte[$i]);
                if ($request->cantidad_actual_corte[$i] > $capacidad_maxima) {
                    $producto = Producto::findOrFail($request->producto_corte[$i]);
                    $productos_failed[] = $producto->nombre;
                }
        }

        if (count($productos_failed) > 0) {
            $corteFailed = [
                'corteFailed' => 'Revise las cantidades, los siguientes productos sobrepasan la capacidad máxima en éste establecimiento: <b>'.implode(', ', $productos_failed).'</b>'
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }

        if ($establecimiento->capacidad_minima!=null && $establecimiento->punto_reorden!=null && $establecimiento->capacidad_maxima!=null) {
            
            $cantidad_total=0;
            //INSERT PRODUCTS TO CORTE
            for ($i=0; $i < count($request->cantidad_actual_corte); $i++) { 
                $cantidad_total+=intval($request->cantidad_actual_corte[$i]);

                $productos_corte = [
                    'establecimiento_id'    => $id,
                    'producto_id'           => $request->producto_corte[$i],
                    'cantidad_actual'       => $request->cantidad_actual_corte[$i],
                    'created_by'            => Auth::user()->id,
                    'updated_by'            => Auth::user()->id,
                ];
                $corte = new Corte($productos_corte);
                $corte->save();
            }

            $establecimiento->capacidad_actual = $cantidad_total;

            if ($cantidad_total >=0 && $cantidad_total <= $establecimiento->capacidad_minima) {
                $establecimiento->status_inventario = 0;
            } else if ($cantidad_total > $establecimiento->capacidad_minima && $cantidad_total <= $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 1;
            } else if ($cantidad_total > $establecimiento->punto_reorden) {
                $establecimiento->status_inventario = 2;
            }

            $establecimiento->update();
        } else {
            $corteFailed = [
                'corteFailed' => 'Es necesario primero establecer puntos mínimos y de re-orden.',
            ];
            return back()->withErrors($corteFailed)->withInput()->with('show_modal_realizar_corte', true);
        }
        
        DB::commit();

        return redirect()->intended('pedidos/'.$pedido_id)->with('corte_success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $pedido = Pedido::findOrFail($id);

        return view('sistema.pedidos.show', ['pedido' => $pedido]);
    }

    public function create()
    {
        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        return view('sistema.pedidos.create');
    }

    public function createFromInventario(Request $request, $id) {

        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        
        $data = array();
        $inventario = array();
        for($i = 0; $i < count($request->producto_pedido); $i++){
            $inventario['producto'][$i] = Producto::findOrFail($request->producto_pedido[$i]);
            $inventario['cantidad_pedido'][$i] = $request->cantidad_pedido[$i];
            $inventario['precio_pedido'][$i] = $request->precio_pedido[$i];
            $inventario['subtotal_producto'][$i] = $request->subtotal_producto[$i];
        }
        
        $data['establecimiento'] = ClienteEstablecimiento::findOrFail($id);
        $data['cliente'] = null;
        $data['es_evento'] = 0;
        return redirect('/pedidos/create')->with('data', $data)->with('inventario', $inventario);
    }

    public function createFromCliente(Request $request) {
        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $cliente = Cliente::findOrFail($request->cliente_id);

        $messages = [
            'required' => 'Campo requerido.'
        ];

        if ($cliente->type == 'horeca') {
            $validator = Validator::make($request->all(), [
                'cliente_establecimiento'           => 'required',
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                'cliente_id'           => 'required',
            ], $messages);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_create', true);
        }

        $data = array();
        
        $data['cliente'] = $cliente;
        if ($request->cliente_establecimiento) {
            $data['establecimiento'] = ClienteEstablecimiento::findOrFail($request->cliente_establecimiento);
        } else {
            $data['establecimiento'] = null;
        }
        $data['es_evento'] = $request->es_evento;

        return redirect('/pedidos/create')->with('data', $data);
    }

    public function createFromClienteEst(Request $request) {
        if (Gate::denies('manage-users')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.'
        ];

        $validator = Validator::make($request->all(), [
            'cliente_establecimiento'           => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_pedido_create', true);
        }

        $data = array();
        
        $data['establecimiento'] = ClienteEstablecimiento::findOrFail($request->cliente_establecimiento);
        $data['cliente'] = null;
        $data['es_evento'] = $request->es_evento;

        return redirect('/pedidos/create')->with('data', $data);
    }

    public function actualizarRemision($id)
    {   
        $pedido = Pedido::findOrFail($id);

        $pdf = PDF::loadView('sistema.layouts.remision', ['pedido' => $pedido]);
        $nombre = "remision".$id.".pdf";
        $archivo="uploads/remisiones/".$nombre;

        $pedido->remision_path = $archivo;
        $pedido->update();
        return $pdf->save($archivo);

    }

    public function generaRemision($id)
    {   
        $pedido = Pedido::findOrFail($id);
        $pdf = PDF::loadView('sistema.layouts.remision', ['pedido' => $pedido]);
        $nombre = "remision".$id.".pdf";
        #return $pdf->save($archivo)->download($nombre);
        return $pdf->download($nombre);

    }

    public function sendEmail(Request $request, $id) {

        $messages = [
            'required' => 'Campo requerido.',
            'email' => 'Debe ser un email válido.',
        ];

        $validator = Validator::make($request->all(), [
            'email_to'     => 'required|email',
            'email_cc'     => 'email',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_send_email', true);
        }

        $pedido = Pedido::findOrFail($id);
        $pdf = PDF::loadView('sistema.layouts.remision', ['pedido' => $pedido]);
        $filename = "remision".$id.".pdf";
        $attach = $pdf->output();
        $site_settings = System::all()->first();

        try {
            Mail::send('sistema.layouts.email', ['pedido' => $pedido, 'attach' => $attach, 'filename' => $filename, 'site_settings' => $site_settings, 'request' => $request], function ($m) use ($pedido, $request, $attach,$filename,$site_settings) {
                if($site_settings->site_name != null) {
                    $site_name = $site_settings->site_name;
                } else {
                    $site_name = env('APP_NAME');
                }
                
                if (isset($request->email_cc) && $request->email_cc != '') {
                    $m->to($request->email_to, $pedido->cliente->nombre_corto)->subject($site_name.' | Remisión #'.$pedido->id)->cc($request->email_cc);
                    $m->cc($request->email_cc)->subject($site_name.' | Remisión #'.$pedido->id);
                } else {
                    $m->to($request->email_to, $pedido->cliente->nombre_corto)->subject($site_name.' | Remisión #'.$pedido->id);
                }

                $m->attachData($attach, $filename,[
                    'mime' => 'application/pdf',
                ]);
            });
        } catch (Exception $ex) {
            $sendEmailFailed = [
                'sendEmailFailed' => $ex,
            ];
            return back()->withErrors($sendEmailFailed)->withInput()->with('show_modal_send_email', true);
        }

        return redirect('pedidos/'.$id)->with('send_email_success', true);
    }

    public function delete(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_delete', true);
        }

        DB::beginTransaction();
        $salida = InventarioSalidas::findOrFail($id);
        $inventario = Inventario::findOrFail($salida->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_salida_anterior = $salida->cantidad_salida;
        $cant_nueva = (intval($cant_actual)+intval($cant_salida_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id=$salida->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->salida_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se eliminó la salida '.$cant_salida_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $salida->delete();
        DB::commit();

        return redirect()->intended('/inventario/salidas/'.$inventario_id)->with('delete_salida_success', true);
    }

    public function reactivar(Request $request, $id) {

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para realizar ésta acción en el sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $messages = [
            'required' => 'Campo requerido.',
            'min' => 'Escribe al menos 5 caracteres.',
        ];

        $validator = Validator::make($request->all(), [
            'comentarios'     => 'required|min:5',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_registro_restore', true);
        }

        DB::beginTransaction();
        $salida = InventarioSalidas::withTrashed()->findOrFail($id);

        $inventario =  Inventario::findOrFail($salida->inventario_id);

        //Registros anteriores
        $cant_actual = $inventario->cantidad_disponible;
        $cant_salida_anterior = $salida->cantidad_salida;
        $cant_nueva = (intval($cant_actual)-intval($cant_salida_anterior));

        // Update Inventario
        $inventario->cantidad_disponible = $cant_nueva;
        $inventario_id =$salida->inventario_id;

        // Registro de Log
        $log_inventario = new LogInventario;
        $log_inventario->inventario_id = $inventario_id;
        $log_inventario->salida_id = $id;
        $log_inventario->user_id = Auth::user()->id;
        $log_inventario->log = 'Se restauró la salida '.$cant_salida_anterior.'. Inventario Actual: <b>'.$cant_nueva.'</b>';
        $log_inventario->comentarios = trim($request->comentarios);
        $log_inventario->save();
        
        $inventario->update();
        $salida->restore();
        DB::commit();

        return redirect()->intended('/inventario/salidas/'.$inventario_id)->with('restore_salida_success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPagoPedido($pedido_id, $pago_id)
    {
        $pago = Pago::findOrFail($pago_id);
        $pedido = Pedido::findOrFail($pago->pedido_id);
        
        DB::beginTransaction();
                
        $pago->delete();

        if($pedido->total - $pedido->pagos->sum('monto') > 0) {
            # pago parcial
            $pedido->status_cobranza = 2;
        } else {
            #liquidado
            $pedido->status_cobranza = 3;
        }
        $pedido->update();
        
        DB::commit();
        
        return redirect()->intended('/pedidos/' . $pedido_id)->with('show_tab_cobranza', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFacturaPedido($pedido_id, $factura_id)
    {
        $factura = PedidoFacturas::findOrFail($factura_id);
        $pedido = Pedido::findOrFail($factura->pedido_id);
        
        DB::beginTransaction();
                
        $factura->delete();

        if($pedido->total - $pedido->facturas->sum('total') > 0) {
            # Faturado Parcialmente
            $pedido->status_facturacion = 2;
        } else {
            # Faturado
            $pedido->status_facturacion = 3;
        }
        $pedido->update();
        
        DB::commit();
        
        return redirect()->intended('/pedidos/' . $pedido_id)->with('show_tab_cobranza', true);
    }
}
