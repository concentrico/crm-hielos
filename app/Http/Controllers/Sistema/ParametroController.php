<?php
namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Parametro as Parametro;
use App\Models\ParametroProductos as ParametroProductos;
use App\Models\FormaPago as FormaPago;
use App\Models\ModeloConservador as ModeloConservador;
use App\Models\CondicionPago as CondicionPago;
use App\Models\ClienteZona as ClienteZona;
use App\Models\ClienteTipoEstablecimiento as ClienteTipoEstablecimiento;

use Auth;
use DB;
use File;
use Input;
use Validator;
use Gate;

class ParametroController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {

    if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
        $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
        return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
    }

    $data =[
     'parametros' => Parametro::all(),
     'forma_pago' => FormaPago::all(),
     'modelo_conservador' => ModeloConservador::all(),
     'condicion_pago' => CondicionPago::all(),
     'cliente_zona' => ClienteZona::all(),
     'cliente_tipo_establecimiento' => ClienteTipoEstablecimiento::all(),
    ];
  return view ('sistema.parametros.index')->with('data', $data);
  }

  public function store(Request $request){

    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'capacidad_cuartofrio' => 'required|numeric',
    ], $messages);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput()->with('show_modal_parametro_create', true);
    }

    DB::beginTransaction();
    $parametro = new Parametro;
    $parametro->capacidad_cuartofrio = $request->capacidad_cuartofrio;
    $parametro->save();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function storeModeloConservador(Request $request) {
    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'modelo' => 'required',
         'codigo' => 'required',
         'producto' => 'required',
         'capacidad' => 'required|numeric',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_modelo_conservador_create', true);
    }
    DB::beginTransaction();
    $modelo_conservador = new ModeloConservador;
    $modelo_conservador->codigo = $request->input('codigo');
    $modelo_conservador->modelo = $request->input('modelo');
    $modelo_conservador->producto_id = $request->input('producto');
    $modelo_conservador->capacidad = $request->input('capacidad');
    $modelo_conservador->created_by = Auth::user()->id;
    $modelo_conservador->updated_by = Auth::user()->id;
    
    $modelo_conservador->save();
    DB::commit();
  return redirect()->intended('parametros');
  }

  public function storeZonaEntrega(Request $request) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_zona' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_zona_entrega_create', true);
    }
    DB::beginTransaction();
    $zona_entrega = new ClienteZona;
    $zona_entrega->nombre = $request->input('nombre_zona');
    $zona_entrega->created_by = Auth::user()->id;
    $zona_entrega->updated_by = Auth::user()->id;
    
    $zona_entrega->save();
    DB::commit();
  return redirect()->intended('parametros');
  }

  public function storeCondicionPago(Request $request) {
    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_condicion' => 'required',
         'dias_condicion'   => 'required|numeric',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_condicion_pago_create', true);
    }
    DB::beginTransaction();
    $condicion_pago = new CondicionPago;
    $condicion_pago->nombre = $request->input('nombre_condicion');
    $condicion_pago->dias = $request->input('dias_condicion');
    $condicion_pago->created_by = Auth::user()->id;
    $condicion_pago->updated_by = Auth::user()->id;
    
    $condicion_pago->save();
    DB::commit();
  return redirect()->intended('parametros');
  }

  public function storeFormaPago(Request $request) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_formapago' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_forma_pago_create', true);
    }
    DB::beginTransaction();
    $forma_pago = new FormaPago;
    $forma_pago->nombre = $request->input('nombre_formapago');
    $forma_pago->created_by = Auth::user()->id;
    $forma_pago->updated_by = Auth::user()->id;
    
    $forma_pago->save();
    DB::commit();
  return redirect()->intended('parametros');
  }

  public function storeTipoEstablecimiento(Request $request) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_tipoest' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_cliente_tipoest_create', true);
    }
    DB::beginTransaction();
    $tipo_establecimiento = new ClienteTipoEstablecimiento;
    $tipo_establecimiento->nombre = $request->input('nombre_tipoest');
    $tipo_establecimiento->created_by = Auth::user()->id;
    $tipo_establecimiento->updated_by = Auth::user()->id;
    
    $tipo_establecimiento->save();
    DB::commit();
  return redirect()->intended('parametros');
  }

  public function update(Request $request, $id) {

    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'capacidad_cuartofrio_edit' => 'required|numeric',
    ], $messages);

    if ($validator->fails()) {
      return redirect('parametros')->withErrors($validator);
    }

    DB::beginTransaction();
    $parametro = Parametro::findOrFail($id);
    $parametro->capacidad_cuartofrio = $request->capacidad_cuartofrio_edit;

    $total=0.0;
    for ($i=0; $i < count($request->parametro_producto_id); $i++) { 
      $cantidad = $request->capacidad[$i];
      $peso = $request->peso[$i];
      $total+=floatval($cantidad)*floatval($peso);
    }

    if ($total > $parametro->capacidad_cuartofrio) {
      $cuartoFrioProductosFailed = [
        'cuartoFrioProductosFailed' => 'Las cantidades de producto sobrepasan la capacidad del Inventario Interno.',
      ];
      return redirect('parametros')->withErrors($cuartoFrioProductosFailed);
    }

    $parametro->produccion_diaria = $request->produccion_diaria_edit;
    $parametro->update();
    
    //UPDATE CAPACIDADES CUARTO FRIO (PRODUCTO)
    for ($i=0; $i < count($request->parametro_producto_id); $i++) { 
      $capacidad_producto = ParametroProductos::findOrFail($request->parametro_producto_id[$i]);
      $capacidad_producto->capacidad = $request->capacidad[$i];
      $capacidad_producto->updated_by = Auth::user()->id;
      $capacidad_producto->update();
    }

    DB::commit();

    return redirect()->intended('parametros')->with('update_success',true);
  }

  public function updateSettings(Request $request) {

    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
        'site_url' => 'required|url',
        'site_name' => 'required',
        'site_logo'    => 'mimes:gif,jpg,jpeg,png',
    ], $messages);

    if ($validator->fails()) {
      return redirect('parametros')->withErrors($validator);
    }

    DB::beginTransaction();
    $parametro = Parametro::findOrFail(1)->first();
    $parametro->site_url = $request->site_url;
    $parametro->site_name = $request->site_name;
    $parametro->site_email = $request->site_email;
    $parametro->site_phone = $request->site_phone;
    $parametro->google_maps_api_key = $request->google_maps_api_key;
    $parametro->google_recaptcha_api_key = $request->google_recaptcha_api_key;
    $parametro->update();

    $logo = $request->file('site_logo');
        
    if($logo != null){
        $file_name = 'logo_'.time().'_'.rawurlencode(sanitize_url($logo->getClientOriginalName()));    
        $file_path = 'uploads/';
        $logo->move($file_path, $file_name);
        $parametro->site_logo = $file_path . $file_name;
        $parametro->update();
    }

    DB::commit();

    return redirect()->intended('parametros')->with('update_success',true);
  }

  public function updateModeloConservador(Request $request, $id) {
    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'modelo_edit' => 'required',
         'codigo_edit' => 'required',
         'producto_edit' => 'required',
         'capacidad_edit' => 'required|numeric',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_modelo_conservador_edit', true);
    }
    DB::beginTransaction();
    $modelo_conservador = ModeloConservador::findOrFail($id);
    $modelo_conservador->codigo = $request->input('codigo_edit');
    $modelo_conservador->modelo = $request->input('modelo_edit');
    $modelo_conservador->producto_id = $request->producto_edit;
    $modelo_conservador->capacidad = $request->input('capacidad_edit');
    $modelo_conservador->updated_by = Auth::user()->id;
    
    $modelo_conservador->update();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function updateZonaEntrega(Request $request, $id) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_zona_edit' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_cliente_zona_edit', true);
    }
    DB::beginTransaction();
    $zona_entrega = ClienteZona::findOrFail($id);
    $zona_entrega->nombre = $request->input('nombre_zona_edit');
    $zona_entrega->updated_by = Auth::user()->id;
    
    $zona_entrega->update();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function updateCondicionPago(Request $request, $id) {
    $messages = [
        'required' => 'Campo requerido.',
        'numeric' => 'Solo se permiten números.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_condicion_edit' => 'required',
         'dias_condicion_edit' => 'required|numeric',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_condicion_pago_edit', true);
    }
    DB::beginTransaction();
    $condicion_pago = CondicionPago::findOrFail($id);
    $condicion_pago->nombre = $request->input('nombre_condicion_edit');
    $condicion_pago->dias = $request->input('dias_condicion_edit');
    $condicion_pago->updated_by = Auth::user()->id;
    
    $condicion_pago->update();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function updateFormaPago(Request $request, $id) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_formapago_edit' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_forma_pago_edit', true);
    }
    DB::beginTransaction();
    $forma_pago = FormaPago::findOrFail($id);
    $forma_pago->nombre = $request->input('nombre_formapago_edit');
    $forma_pago->updated_by = Auth::user()->id;
    
    $forma_pago->update();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function updateTipoEstablecimiento(Request $request, $id) {
    $messages = [
        'required' => 'Campo requerido.',
    ];

    $validator = Validator::make($request->all(), [
         'nombre_tipoest_edit' => 'required',
     ], $messages);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput()->with('show_modal_cliente_tipoest_edit', true);
    }
    DB::beginTransaction();
    $tipo_establecimiento = ClienteTipoEstablecimiento::findOrFail($id);
    $tipo_establecimiento->nombre = $request->input('nombre_tipoest_edit');
    $tipo_establecimiento->updated_by = Auth::user()->id;
    
    $tipo_establecimiento->update();
    DB::commit();

  return redirect()->intended('parametros');
  }

  public function destroy($id) {
      $parametro = Parametro::findOrFail($id);

      DB::beginTransaction();

      $parametro->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }

  public function destroyModeloConservador($id) {
      $modelo_conservador = ModeloConservador::findOrFail($id);

      DB::beginTransaction();
      
      $modelo_conservador->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }

  public function destroyZonaEntrega($id) {
      $zona_entrega = ClienteZona::findOrFail($id);

      DB::beginTransaction();
      
      $zona_entrega->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }

  public function destroyCondicionPago($id) {
      $condicion_pago = CondicionPago::findOrFail($id);

      DB::beginTransaction();
      
      $condicion_pago->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }

  public function destroyFormaPago($id) {
      $forma_pago = FormaPago::findOrFail($id);

      DB::beginTransaction();
      
      $forma_pago->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }

  public function destroyTipoEstablecimiento($id) {
      $tipo_establecimiento = ClienteTipoEstablecimiento::findOrFail($id);

      DB::beginTransaction();
      
      $tipo_establecimiento->delete();

      DB::commit();

      return redirect()->intended('parametros');
  }
}