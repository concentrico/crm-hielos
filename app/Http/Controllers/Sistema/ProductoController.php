<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PedidoProductos as PedidoProductos;
use App\Models\ParametroProductos as ParametroProductos;
use App\User as Usuario;
use App\Models\Producto as Producto;
use App\Models\Inventario as Inventario;
use App\Models\CuartoFrioProductos;
use App\Models\Conservador;
use App\Models\EstablecimientoPrecios;

use Auth;
use DB;
use Hash;
use Gate;
use Input;
use Mail;
use Validator;

class ProductoController extends Controller{
    public function index(){

        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }

        $q = Producto::query();

        // FILTRO ESTATUS
        if (Input::has('f_estatus') && Input::get('f_estatus') == 'todos')
        {
            $q->withTrashed();
        }else if(Input::has('f_estatus') && Input::get('f_estatus') == 'suspendido'){
            $q->onlyTrashed();
        }

        // FILTRO NOMBRE
        if (Input::has('f_nombre'))
        {
            $q->whereRaw("nombre LIKE '%". Input::get('f_nombre') ."%'");
        }

        // SORT ORDER
        $sort_aux = ( Input::get('sort') == 'DESC' ? 'DESC' : 'ASC');
        
        // SORT VARIABLE
        $orderByString = "";
        switch(Input::get('orderBy')){
            case 'IDCODIGO' : $orderByString = 'id_codigo '.$sort_aux;           
                break;

            case 'PRODUCTO' : $orderByString = 'nombre '.$sort_aux;         
                break;
            
            case 'PRECMIN' : 
                # pendiente       
                break;

            case 'PRECMAX' : 
                # pendiente        
                break;

            case 'PREPROM' : 
                # pendiente         
                break;

            default : $orderByString = 'nombre '.$sort_aux;           
                break;
        }

        if($orderByString != ""){
            $q->orderByRaw($orderByString);
        }

        if (Input::has('f_num_resultados')){
            $num_resultados = 0 + ((int) Input::get('f_num_resultados') );
            $data['productos'] = $q->paginate($num_resultados);
            if( count($data['productos']) == 0 ){
                $data['productos'] = $q->paginate($num_resultados, ['*'], 'page', 1);
            }
        }else{
            $data['productos'] = $q->paginate(10);
        }

    return view('sistema.productos.index', $data);
    }
    
    public function store(Request $request) {
        $messages = ['required' => 'Campo requerido.','numeric' => 'Solo se permiten números.',
            'id_codigo.unique' => 'Éste código ya está en uso.',];
        $validator = Validator::make($request->all(), [
            'id_codigo'         => 'required|unique:producto,id_codigo',
            'nombre'            => 'required',
            'precio_venta'      => 'numeric|required',
            'iva'               => 'required|numeric',
            'peso'              => 'numeric|required',
            'tipo'              => 'required',
            'es_hielo'          => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_producto_create', true);
        }
        
        DB::beginTransaction();
        $producto = new Producto;
        
        $producto->nombre = $request->nombre;
        $producto->id_codigo = $request->id_codigo;
        $producto->precio_venta = $request->precio_venta;
        $producto->peso = $request->peso;
        $producto->iva = floatval($request->iva)/100;
        $producto->tipo = $request->tipo;
        $producto->es_hielo = $request->es_hielo;
        $producto->comentarios = $request->comentarios;
        $producto->created_by = Auth::user()->id;
        $producto->updated_by = Auth::user()->id;
        $producto->save();

        if ($request->es_hielo == '1') {
            # Dar de alta en tabla Inventario
            $inventario = new Inventario;
            $inventario->producto_id = $producto->id;
            $inventario->cantidad_disponible = NULL;
            $inventario->cantidad_producir= NULL;
            $inventario->ultima_salida = NULL;
            $inventario->created_by = Auth::user()->id;
            $inventario->updated_by = Auth::user()->id;
            $inventario->save();

            #Dar de alta en Parámetro Produccion
            $capacidad_producto = new ParametroProductos;
            $capacidad_producto->parametro_id = 1;
            $capacidad_producto->producto_id = $producto->id;
            $capacidad_producto->capacidad = NULL;
            $capacidad_producto->created_by = Auth::user()->id;
            $capacidad_producto->updated_by = Auth::user()->id;
            $capacidad_producto->save();
        }

        DB::commit();
        
        return redirect()->intended('productos');

    }

    public function show($id){
        if (Gate::denies('manage-users') && Gate::denies('manage-distribucion')) {
            $message = 'El usuario no tiene los permisos requeridos para visualizar éste módulo del sistema';
            return view('errors.master', ['error_no' => 403, 'error_title' => 'Acceso denegado', 'error_message' => $message]);
        }
        $producto = Producto::withTrashed()->findOrFail($id);
        return view('sistema.productos.show', ['producto' => $producto]);
    }
    
    public function update(Request $request, $id)
    {
        $producto = Producto::findOrFail($id);

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
            'id_codigo_edit.unique' => 'Éste código ya está en uso.',
        ];

        $validator = Validator::make($request->all(), [
            'id_codigo_edit'    => 'required|unique:producto,id_codigo,'.$id,
            'nombre_edit'       => 'required',
            'precio_venta_edit' => 'numeric|required',
            'iva_edit'          => 'required|numeric',
            'peso_edit'         => 'numeric|required',
            'es_hielo_edit'     => 'required',
            'tipo_edit'         => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_producto_edit', true);
        }
        
        DB::beginTransaction();
        
        $producto->id_codigo = $request->id_codigo_edit;
        $producto->nombre = $request->nombre_edit;
        $producto->comentarios = $request->comentarios_edit;
        $producto->precio_venta = $request->precio_venta_edit;
        $producto->es_hielo = $request->es_hielo_edit;
        $producto->peso = $request->peso_edit;
        $producto->iva = floatval($request->iva_edit)/100;
        $producto->tipo = $request->tipo_edit;
        $producto->created_by = Auth::user()->id;
        $producto->updated_by = Auth::user()->id;
        $producto->update();

        DB::commit();

        return redirect()->intended('productos');
    }

    public function update2(Request $request, $id)
    {
        $producto = Producto::findOrFail($id);

        $messages = [
            'required' => 'Campo requerido.',
            'numeric' => 'Solo se permiten números.',
            'id_codigo_edit.unique' => 'Éste código ya está en uso.',
        ];

        $validator = Validator::make($request->all(), [
            'id_codigo_edit'    => 'required|unique:producto,id_codigo,'.$id,
            'nombre_edit'       => 'required',
            'precio_venta_edit' => 'numeric|required',
            'iva_edit'          => 'required|numeric',
            'peso_edit'         => 'numeric|required',
            'es_hielo_edit'     => 'required',
            'tipo_edit'         => 'required',
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with('show_modal_producto_edit', true);
        }
        
        DB::beginTransaction();
        
        $producto->id_codigo = $request->id_codigo_edit;
        $producto->nombre = $request->nombre_edit;
        $producto->comentarios = $request->comentarios_edit;
        $producto->precio_venta = $request->precio_venta_edit;
        $producto->es_hielo = $request->es_hielo_edit;
        $producto->peso = $request->peso_edit;
        $producto->iva = floatval($request->iva_edit)/100;
        $producto->tipo = $request->tipo_edit;
        $producto->created_by = Auth::user()->id;
        $producto->updated_by = Auth::user()->id;
        $producto->update();

        DB::commit();

        return redirect()->intended('/productos/'.$producto->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        
        $producto = Producto::find($id);
        $inventario = Inventario::where('producto_id', $id)->first();
        $parametro_produccion = ParametroProductos::where('producto_id', $id)->first();
        $cuartos_frios = CuartoFrioProductos::where('producto_id', $id)->get();
        $acuerdos_estab = EstablecimientoPrecios::where('producto_id', $id)->get();
    
        $producto->delete();
        if($inventario != null)
            $inventario->delete();
        if($parametro_produccion != null)
            $parametro_produccion->delete();
        foreach ($cuartos_frios as $cuarto_frio)
            $cuarto_frio->delete();
        foreach ($acuerdos_estab as $acuerdo)
            $acuerdo->delete();

        DB::commit();
        
        return redirect()->intended('productos');
    }

    public function reactivar($id)
    {

        DB::beginTransaction();
        $producto = Producto::withTrashed()->findOrFail($id);
        $inventario = Inventario::where('producto_id', $id)->withTrashed()->firstOrFail();
        $parametro_produccion = ParametroProductos::where('producto_id', $id)->withTrashed()->firstOrFail();

        $producto->restore();
        if($inventario!=null)
            $inventario->restore();
        if($parametro_produccion!=null)
            $parametro_produccion->restore();

        DB::commit();
        
        return redirect()->intended('/productos/'.$id);
    }
}
