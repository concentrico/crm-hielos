<?php

namespace App\Events;

use Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventoPedido extends Event implements ShouldBroadcast
{
    //use InteractsWithSockets, SerializesModels;
    use SerializesModels;

    public $pedido;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        //dd('hi');
        //$ch = new PrivateChannel('pedidos-entregados-channel');
        //return $ch;
        return ['pedidos-entregados-channel'];

        //$data['message'] = 'hello world';
        //$pusher->trigger('pedidos-entregados-channel', 'my-event', $data);
    }
}
