<?php

namespace App\Listeners;

use App\Events\EventoPedido;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Distribucion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventoPedido  $event
     * @return void
     */
    public function PedidoEntregado(EventoPedido $event)
    {
        echo 'El pedido #'.$event->pedido->id.' ha sido entregado satisfactoriamente';
    }
}
