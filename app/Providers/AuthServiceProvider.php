<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('manage-users', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(1));
        });

        $gate->define('update-user-password', function ($user, $user_update_password) {
            return $user->id == $user_update_password->id;
        });

        $gate->define('create-user', function ($user, $user_type_id) {
            if($user->user_type_id == 1){
                return true;
            }elseif($user->user_type_id == 2){
                return in_array($user_type_id, array(2, 3, 4));
            }elseif($user->user_type_id == 3){
                return in_array($user_type_id, array(3, 4));
            }elseif($user->user_type_id == 4){
                return false;
            }elseif($user->user_type_id == 5){
                return in_array($user_type_id, array(5, 6, 7));
            }elseif($user->user_type_id == 6){
                return in_array($user_type_id, array(6, 7));
            }elseif($user->user_type_id == 7){
                return false;
            }else{
                return false;
            }
        });

        $gate->define('edit-user', function ($user, $user_type_id) {
            if($user->user_type_id == 1){
                return true;
            }elseif($user->user_type_id == 2){
                return in_array($user_type_id, array(2, 3, 4));
            }elseif($user->user_type_id == 3){
                return in_array($user_type_id, array(3, 4));
            }elseif($user->user_type_id == 4){
                return false;
            }elseif($user->user_type_id == 5){
                return in_array($user_type_id, array(5, 6, 7));
            }elseif($user->user_type_id == 6){
                return in_array($user_type_id, array(6, 7));
            }elseif($user->user_type_id == 7){
                return false;
            }else{
                return false;
            }
        });

        $gate->define('view-user', function ($user, $user_type_id) {
            return true;
            if($user->user_type_id == 1){
                return true;
            }elseif($user->user_type_id == 2){
                return in_array($user_type_id, array(2, 3, 4));
            }elseif($user->user_type_id == 3){
                return in_array($user_type_id, array(3, 4));
            }elseif($user->user_type_id == 4){
                return false;
            }elseif($user->user_type_id == 5){
                return in_array($user_type_id, array(5, 6, 7));
            }elseif($user->user_type_id == 6){
                return in_array($user_type_id, array(6, 7));
            }elseif($user->user_type_id == 7){
                return false;
            }else{
                return false;
            }
        });

        $gate->define('manage-distribucion', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(2));
        });
        $gate->define('manage-produccion', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(3));
        });
        $gate->define('manage-choferes', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(4));
        });
        $gate->define('manage-compras', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(5));
        });
        $gate->define('manage-establecimiento', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(6));
        });
        $gate->define('manage-almacen', function ($user) {
            return array_intersect($user->roles->pluck('id')->toArray(), array(7));
        });
    }
}
