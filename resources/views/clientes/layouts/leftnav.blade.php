<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    @if(getSystemSettings()->site_logo != null) 
                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="90">
                    @else 
                        <img src="{{ asset('img/logo-large-default.png') }}" alt="Logo" height="90">
                    @endif
                </div>

                <div class="logo-element">
                    @if(getSystemSettings()->site_logo != null) 
                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" width="30">
                    @else 
                        <img src="{{ asset('img/logo-default.png') }}" alt="Logo" width="30">
                    @endif
                </div>
            </li>

            <!--Cliente Compras-->
            @can('manage-compras')
            <li @yield('attr_navlink_1')>
                <a href="{{ url('/') }}"><i class="fa fa-lg fa-home"></i> <span class="nav-label">Home</span></a>
            </li>

            <li @yield('attr_navlink_2')>
                <a href="{{ url('informacion-comercial')}}"><i class="fa fa-lg fa-handshake-o"></i><span class="nav-label">Perfil Comercial</span></a>
            </li>

            <li @yield('attr_navlink_3')>
                <a href="{{ url('establecimientos') }}"><i class="fa fa-lg fa-map"></i> <span class="nav-label">Establecimientos</span></a>
            </li>

            <li @yield('attr_navlink_4')>
                <a href="{{ url('pedidos') }}"><i class="fa fa-lg fa-clipboard"></i> <span class="nav-label">Pedidos</span></a>
            </li>

            <li @yield('attr_navlink_5')>
                <a href="{{ url('reportes') }}"><i class="fa fa-lg fa-download"></i> <span class="nav-label">Reportes</span></a>
            </li>

            @endcan
            <!--Gerente de Establecimiento-->
            @can('manage-establecimiento')

            <li @yield('attr_navlink_1')>
                <a href="{{ url('/') }}"><i class="fa fa-lg fa-home"></i> <span class="nav-label">Home</span></a>
            </li>

            <li @yield('attr_navlink_3')>
                <a href="{{ url('establecimientos') }}"><i class="fa fa-lg fa-map"></i> <span class="nav-label">Establecimientos</span></a>
            </li>

            <li @yield('attr_navlink_4')>
                <a href="{{ url('pedidos') }}"><i class="fa fa-lg fa-clipboard"></i> <span class="nav-label">Pedidos</span></a>
            </li>
            
            @endcan

            <!--Almacenista-->
            @can('manage-almacen')
              <li @yield('attr_navlink_1')>
                <a href="{{ url('/') }}"><i class="fa fa-lg fa-home"></i> <span class="nav-label">Home</span></a>
            </li>

            @endcan

        </ul>
    </div>
</nav>