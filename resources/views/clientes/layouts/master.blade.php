<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

    <title>@if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | @yield('title') </title>

	@yield('assets_top_top')

	<link href="{{ asset('select2-4.0.3/dist/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/footable/footable.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
	<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/ui-transition/transition.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/plugins/ui-dropdown/dropdown.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <style type="text/css" media="screen">
    	.select2-container {width: 100%!important;}
    	.select2-container--default.select2-container--focus .select2-selection--multiple {border: solid #e7eaec 1px; }
		.typeahead { width:90%; z-index: 1051; }
    	.table-fixed thead {
		  width: 100%;
		}
		.table-fixed tbody {
		  height: 230px;
		  overflow-y: auto;
		  width: 100%;
		}
		.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
		  display: block;
		}
		.table-fixed tbody td, .table-fixed thead > tr> th {
		  float: left;
		  border-bottom-width: 0;
		}
		.tooltip-inner {
		    white-space:pre-wrap!important;
		}
		i.icon.delete:before {
			display: inline-block;
		    font: normal normal normal 14px/1 FontAwesome;
		    font-size: inherit;
		    text-rendering: auto;
		    -webkit-font-smoothing: antialiased;
		  	content: "\f00d";
		}
		.style-custom {
    		border: solid 1px #585858;
		}

		.ui.selection.dropdown {min-width: 5em!important;}
    </style>
	
	@yield('assets_top_bottom')
	
</head>

<body class="body-small mini-navbar">
	<div id="wrapper">
	
		@include('clientes.layouts.leftnav')
	
	    <div id="page-wrapper" class="gray-bg">
		    
		    @include('clientes.layouts.topnav')
	
		    @include('clientes.layouts.breadcrumb')
	        
	        <div class="wrapper wrapper-content animated fadeIn">
				<div class="row">
	            	<div class="col-lg-12">
		            	@yield('content')
		            </div>
				</div>
	        </div>
	
			@include('clientes.layouts.footer')
			
	    </div>
	</div>

	<!-- Mainly scripts -->
	<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
	<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- Custom and plugin javascript -->
	<script src="{{ asset('js/inspinia.js') }}"></script>
	<script src="{{ asset('js/jquery.peity.js') }}"></script>
	<script src="{{ asset('js/jquery.numeric.min.js') }}"></script>
	<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
	<script src="{{ asset('select2-4.0.3/dist/js/select2.min.js') }}"></script>	
	<script src="{{ asset('js/plugins/footable/footable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/plugins/typehead/bootstrap3-typeahead.min.js') }}"></script>
	<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('js/plugins/numeral/numeral.min.js') }}"></script>
	
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
	<script src="{{ asset('js/plugins/ui-transition/transition.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ui-dropdown/dropdown.min.js') }}"></script>
	
	<script type="text/javascript">

		$('[data-toggle="tooltip"]').tooltip(); 

		//Prevent multiple requests
		$('form').submit(function(){
		    $(this).find('button[type=submit]').prop('disabled', true);
		    $(this).find('input[type=submit]').prop('disabled', true);
		});
	
		if( $('body').width() < 768 ) {
			$('body').removeClass('mini-navbar');
			$('.filtros-res').addClass('collapsed');
		}
		
		$(".select2").select2({
			allowClear : true,
			placeholder : 'Seleccione'
		});

		$('.selectdp').dropdown();
		
		$('.table-res').footable({
			"breakpoints": {
				"xs": 480, // extra small
				"sm": 768, // small
				"md": 992, // medium
				"lg": 1200, // large
				"xlg": 1600, // large
				"xxlg": 1900 // large
			}
		});

		$("span.pie").peity("pie");
		$("span.donut").peity("donut")

		$(".numeric").numeric();
		$(".integer").numeric(false, function() { alert("Integers only"); this.value = ""; this.focus(); });
		$(".positive").numeric({ negative: false }, function() { alert("No negative values"); this.value = ""; this.focus(); });
		$(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });
	    $(".decimal-2-places").numeric({ decimalPlaces: 2 });

	    $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $(".touchspin_porcentaje").TouchSpin({
	        min: 0,
	        max: 100,
	        step: 1,
	        decimals: 2,
	        boostat: 5,
	        maxboostedstep: 10,
	        postfix: '%',
	        buttondown_class: 'btn btn-white',
	        buttonup_class: 'btn btn-white'
	    });

	    $(".touchspin_cantidad").TouchSpin({
	        min: 0,
	        max: 1000000000,
	        step: 1,
	        decimals: 0,
	        boostat: 5,
	        maxboostedstep: 10,
	        buttondown_class: 'btn btn-white',
	        buttonup_class: 'btn btn-white'
	    });

	    toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "progressBar": true,
		  "preventDuplicates": false,
		  "positionClass": "toast-bottom-right",
		  "onclick": null,
		  "showDuration": "400",
		  "hideDuration": "1000",
		  "timeOut": "7000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

	</script>
	@yield('assets_bottom')
	
</body>
</html>