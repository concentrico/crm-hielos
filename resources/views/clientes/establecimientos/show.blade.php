@extends('clientes.layouts.master')

@section('title', 'Establecimientos')

@section('attr_navlink_3', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', $establecimiento->sucursal)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('establecimientos') }}">Establecimientos</a></li>
<li class="active"><strong>{{ $establecimiento->sucursal }}</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
	table.footable-details>tbody>tr>th {
	    width: 210px!important;
	}
	.pac-container{z-index: 999999;}
    .labels {
     color: red;
     background-color: white;
     font-family: "Lucida Grande", "Arial", sans-serif;
     font-size: 10px;
     font-weight: bold;
     text-align: center;
     width: 40px;
     border: 2px solid black;
     white-space: nowrap;
   }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
	            
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
                		<i style="margin:0px;" class="fa fa-address-card fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Datos Generales</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 4) active @endif">
                	<a data-toggle="tab" href="#tab-2">
                		<i style="margin:0px;" class="fa fa-file-text fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Pedidos ({{ count($establecimiento->pedidos) }})<span> 
	                </a>
	            </li>     
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-6 col-md-6" style="padding: 0 5px;">
								@include('clientes.establecimientos.iboxes.datos')
							</div>
							<div class="col-sm-6 col-md-6" style="padding: 0 5px;">
								@include('clientes.establecimientos.iboxes.inventario')				
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('clientes.establecimientos.iboxes.pedidos')
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('clientes.establecimientos.modals.corte')

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript">	

	@if(session('corte_success'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	c3.generate({
        bindto: '#nivel_conservador',
        size: {
		  height: 230
		},
        data:{
            columns: [
                ['Capacidad Actual', {{$establecimiento->porcentaje_inventario()}}]
            ],

            type: 'gauge'
        },
        color:{
            pattern: ['#1ab394', '#BABABA']

        }
    });

	var establecimiento = {
		id : {{ $establecimiento->id }},
	};

	function calcularCapacidadCorte(cap) {
		var sum = 0;
		$('.capproducto').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
	}

    var marker = '';

    function initMap() {

      @if($establecimiento->latitud_longitud != null) 

      	var input = document.getElementById('latitud_longitud').value;
	  	var latlngStr = input.split(',', 2);
	  	var myLatLng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  @else
	  	var myLatLng = {lat: 25.6510566, lng: -100.4025978};
	  @endif

      var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 18,
	    center: myLatLng
	  });

	  var geocoder = new google.maps.Geocoder();

	  var infowindow = new google.maps.InfoWindow({
	      disableAutoPan: true
	   });

	  @if($establecimiento->latitud_longitud != null) 
	  	geocodeLatLng(geocoder, map, infowindow);
	  @endif

	  $('#direccion2').keypress(function (e) {
		 var key = e.which;
		 if(key == 13) { // the enter key code
		    geocodeAddress(geocoder, map, infowindow);
		  }
	  }); 

	}

	function geocodeLatLng(geocoder, map, infowindow) {

	  var input = document.getElementById('latitud_longitud').value;
	  var latlngStr = input.split(',', 2);
	  var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};

	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[1]) {

	        map.setZoom(16);

	        var marker = new google.maps.Marker({
	          position: latlng,
	          map: map,
	          draggable: false,
    		  animation: google.maps.Animation.DROP,
	        });
	        infowindow.setContent('{{$establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$establecimiento->sucursal}}</b>');
	        infowindow.open(map, marker);

	        google.maps.event.addListener(marker, "drag", function (mEvent) { 
		      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
		      	$('#latitud_longitud').val(latlng);
		    });

	      } else {
	        //sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	      }
	    } else {
	      //sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

  	function geocodeAddress(geocoder, resultsMap, infowindow) {
	  var address = document.getElementById('direccion2').value;
	  geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {

	      resultsMap.setCenter(results[0].geometry.location);

	      var lat = results[0].geometry.location.lat();
	      var lng = results[0].geometry.location.lng();
	      $('#latitud_longitud').val(lat+', '+lng);

	      if ( !marker == '' ) {
		    //Destroy marker and create new
		  	marker.setMap(null);
		  } 

		  marker = new google.maps.Marker({
	        map: resultsMap,
	        draggable: true,
    		animation: google.maps.Animation.DROP,
	        position: results[0].geometry.location
	      });

        	infowindow.setContent('{{$establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$establecimiento->sucursal}}</b>');
        	infowindow.open(map, marker);

	      google.maps.event.addListener(marker, "drag", function (mEvent) { 
	      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
	      	$('#latitud_longitud').val(latlng);
	      });


	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

	// FIX TO RESIZE AND SHOW MAP DIV 

	$('#tab-3 a').click(function (e) {
      e.preventDefault()

      jQuery(window).trigger('resize');
        var ibox = $(this).closest('div.ibox');
		ibox.find('[id=nivel_conservador]').resize();

    })

</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{getSystemSettings()->google_maps_api_key}}&callback=initMap"
        async defer></script>

@endsection