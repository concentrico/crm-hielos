@extends('clientes.layouts.master')

@section('title', 'Lista de Establecimientos')

@section('attr_navlink_3', 'class="active"')

@section('breadcrumb_title', 'Establecimientos')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Establecimientos</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('clientes.establecimientos.iboxes.tabla')
	</div>
</div>

@include('clientes.establecimientos.modals.corte2')
@endsection

@section('assets_bottom')
<script type="text/javascript">

	@if(session('corte_success'))
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif

	establecimientos = [];
	@foreach($establecimientos as $establecimiento)
		establecimientos[{{ $establecimiento->id }}] = {
			sucursal: '{{ $establecimiento->sucursal }}',
		}
	@endforeach

</script>
@endsection
