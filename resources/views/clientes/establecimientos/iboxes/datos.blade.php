<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">

			    <h2 class="text-info">
				    {{ $establecimiento->cliente->nombre_corto }} <small>{{ $establecimiento->sucursal }} </small>
				</h2>
				
				@if(count($establecimiento->tipos) > 0)
					<span style="font-weight: bold;">
						{{ (count($establecimiento->tipos) === 1 ? 'Tipo:' : 'Tipos:' ) }}
					</span>					
					@foreach($establecimiento->tipos as $key => $tipo)
						<span class="label label-success" style="margin-top: 5px; display:inline-block;">{{ $tipo->nombre }}</span>
					@endforeach
					<br />
				@endif

				<br/>
				<div class="row">
					<div class="col-sm-6">
						<span style="font-weight: bold;">Nombre del Cliente:</span><br />
						<a href="{{url('clientes/'.$establecimiento->cliente_id)}}">{{ $establecimiento->cliente->nombre_corto }} </a>
					</div>
					<div class="col-sm-6">
						<span style="font-weight: bold;">Contacto Comercial</span><br />

						@if(count($establecimiento->cliente->contactos) > 0 )
							@foreach($establecimiento->cliente->contactos as $contacto)
								@if (in_array('1', $contacto->funciones()->pluck('id')->toArray() )) 
									<span style=""> {{ $contacto->nombre_completo() }} </span>
								@endif
							@endforeach	
						@endif
						
					</div>
				</div>

				<br/>
				@if( imprime_direccion($establecimiento) !== "")
					<h4 class="text-success">Dirección</h4>
					<div class="row">
						<div class="col-sm-12">
							<address>
								{!! imprime_direccion($establecimiento) !!}
							</address>
							</div>
					</div>
				@endif

			</div>
		</div>
		@if($establecimiento->latitud_longitud != null)
		<div class="row">
		    <div class="col-sm-12">
				<div class="row" id="mapa">
					<input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$establecimiento->latitud_longitud}}" />
					<div id="map" style="height: 400px;"></div>
				</div>
			</div>
		</div>
		@endif
    </div>
</div>
