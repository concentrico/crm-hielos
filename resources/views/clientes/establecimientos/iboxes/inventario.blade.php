<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				<h3 style="font-weight: bold;">Status Inventario:</h3>

				@if($establecimiento->status_inventario === null)
					<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
				@else
					@if($establecimiento->status_inventario == 0)
						<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Mínimo</span>
					@elseif($establecimiento->status_inventario == 1)
						<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Reorden</span>
					@elseif($establecimiento->status_inventario == 2)
						<span class="label label-success" style="margin-top: 5px; display:inline-block;">Óptimo</span>
					@else
						<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
					@endif
				@endif
				<BR/><BR/>
				<div class="row">
					<div class="col-sm-6">
						<h4 class="text-success">Inventario Actual:</h4>
						<span style=""> {{ ( $establecimiento->capacidad_actual != null ? $establecimiento->capacidad_actual : 'N.D.') }} bolsas</span>
					</div>
					<div class="col-sm-6">

					<h4 class="text-success">Último Corte:</h4>
					{!!$establecimiento->ultimo_corte()!!}
					</div>
				</div>

			</div>
			<div class="col-sm-12 m-t-lg">
				<h2 class="text-primary text-center">
				    Nivel Inventario
				</h2>
	            <div id="nivel_conservador"></div>
	        </div>

	        <div class="col-sm-12 m-t-lg text-center">

	        	@if($establecimiento->fecha_ultimo_corte() != '')
            		@if($establecimiento->fecha_ultimo_corte() != Carbon::now()->formatLocalized('%d/%m/%Y') )
            			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" style="width: 100%;" onclick="showModalRealizarCorte()"><i class="fa fa-clipboard"></i> Realizar Corte</button>
            		@else
            			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" style="color:white; width: 100%;" disabled=""><i class="fa fa-clipboard"></i> Realizar Corte</button>
                	@endif
            	@else
            		<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" style="width: 100%;" onclick="showModalRealizarCorte({{$establecimiento->id}})"><i class="fa fa-clipboard"></i>  Corte</button>
            	@endif
            	
	        </div>
		</div>
		
    </div>
</div>
