<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Pedidos</h5>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
			    @if(count($establecimiento->pedidos) == 0)
					<div class="alert alert-warning">
						El establecimiento aún no tiene pedidos registrados!
					</div>
			    @else
					<table class="table table-res" data-cascade="true">
						<thead>
							<tr>
								<th data-type="html">No. Pedido</th>
								<th data-type="html" data-breakpoints="sm">Fecha de Entrega</th>
								<th data-type="html" data-breakpoints="sm">Status</th>
								<th data-type="html" data-breakpoints="sm">Chofer Asignado</th>
								<th data-type="html" data-breakpoints="sm">Recibido el</th>
								<th data-type="html" data-breakpoints="sm">Firmado por</th>
								<th data-type="html" data-breakpoints="sm">Remisión</th>
							</tr>
						</thead>
						<tbody>
		                    @foreach ($establecimiento->pedidos as $pedido)
			                    <tr class="@if($pedido->deleted_at != NULL) warning @endif">
			                        <td>
				                        <a class="text-navy" href="{{ url('pedidos/'.$pedido->id) }}">Pedido #{{ $pedido->id }}</a>
					               	</td>

			                       <td>
									@if($pedido->fecha_entrega != null)
										{{ fecha_to_human($pedido->fecha_entrega,false) }}
										@else
											Sin definir				
										@endif
					                </td>

					                <td>
				                    	@php
				                    		switch ($pedido->status) {
				                    			case '1':
				                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">Programado</span>';
				                    				break;
				                    			case '2':
				                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">En Camino</span>';
				                    				break;
				                    			case '3':
				                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Entregado</span>';
				                    				break;
				                    			case '4':
				                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Devolución</span>';
				                    				break;
				                    			case '5':
				                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Cancelado</span>';
				                    				break;
				                    			default:
				                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>';
				                    				break;
				                    		}
				                    	@endphp
					                   
				                    </td>

				                    <td>{{$pedido->chofer->nombre}}</td>

				                    <td >
				                        @if($pedido->entregado_at != null)
											{{ fecha_to_human($pedido->entregado_at,false) }}
											-
											{{Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
										@else
											N.D.
										@endif
					               	</td>
					               	<td>
					               		@if($pedido->firmado_por != null)
											@php
											$nombre_completo=$pedido->cliente_firma->nombre.' '.$pedido->cliente_firma->apellido_paterno.' '.$pedido->cliente_firma->apellido_materno;
											@endphp
											{{$nombre_completo}}
										@else
											@if($pedido->firmado_otro != null)
												{{$pedido->firmado_otro}}
											@else
												N.D.
											@endif
										@endif
					               	</td>
			                        <td class="text-center">
			                        	@if($pedido->status == 3)
											<a href="{{url('pedidos/' . $pedido->id . '/remision') }}"><i class="fa fa-file-pdf-o"></i></a>
										@else
											-
										@endif
			                        </td>

			                    </tr>
							@endforeach
						</tbody>
					</table>            	        
			    @endif
		    </div>
	    </div>
    </div>
</div>
