<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Lista de establecimientos</h5>
    </div>
    <div class="ibox-content">
	     <div class="row">
		    <div class="col-sm-12 text-right">
			</div>
	    </div>
        <div>
			<table id="dataTableEstablecimientos" name="dataTableEstablecimientos" class="table table-res">
				<thead>
					<tr>
						<th data-type="html"></th>
						<th data-type="html">Establecimiento</th>
						<th data-type="html" data-breakpoints="xs sm">Cliente</th>
						<th data-type="html" data-breakpoints="xs sm">Dirección</th>
						<th data-type="html" data-breakpoints="xs">Último Corte</th>
						<th data-type="html" data-breakpoints="xs sm">Inventario Actual</th>
						@can('manage-compras')
							<th data-type="html" data-breakpoints="xs sm">Saldo</th>
							<th data-type="html" data-breakpoints="xs sm">Vencido</th>
						@endcan
						<th data-type="html" ></th>
						<th data-type="html" data-breakpoints="xs"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($establecimientos as $establecimiento)
	                    <tr>
	                        <td class="text-center">{!!$establecimiento->getColorInventario()!!}</td>
	                        <td><a href="{{ url('establecimientos/'.$establecimiento->id) }}">{{ $establecimiento->sucursal }}</a></td>
	                        <td><a href="{{ url('informacion-comercial') }}">{{ $establecimiento->cliente->nombre_corto }}</a></td>
	                        <td>{{ $establecimiento->direccion() }}</td>
	                        <td class="text-center">{!!$establecimiento->ultimo_corte()!!}
	                        </td>
	                        <td class="text-center">
		                    	@if($establecimiento->capacidad_actual != null)
		                    		{{$establecimiento->capacidad_actual}} bolsas
		                    	@else
		                    		Sin registros
		                    	@endif
		                    </td>
		                    @can('manage-compras')
		                    	<td class="text-center">
				                    <span>
				                    	${{ $establecimiento->saldo() }} 
					                </span>
			                    </td>
			                    <td class="text-center">
		                        	@if($establecimiento->saldo_porcobrar() > 0.00)
					                    <span class="text-danger">
					                    	${{ $establecimiento->saldo_porcobrar() }} 
						                </span>
						            @else
						            	<span>
					                    	${{ $establecimiento->saldo_porcobrar() }} 
						                </span>
						            @endif
			                    </td>
		                    @endcan
		                    <td class="text-center">
	                    		@if($establecimiento->fecha_ultimo_corte() != '')
		                    		@if($establecimiento->fecha_ultimo_corte() != Carbon::now()->formatLocalized('%d/%m/%Y') )
		                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" onclick="showModalRealizarCorte({{$establecimiento->id}})"><i class="fa fa-clipboard"></i> Realizar Corte</button>
		                    		@else
		                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" style="color:white;" disabled=""><i class="fa fa-clipboard"></i> Realizar Corte</button>
			                    	@endif
		                    	@else
		                    		<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" onclick="showModalRealizarCorte({{$establecimiento->id}})"><i class="fa fa-clipboard"></i> Realizar Corte</button>
		                    	@endif
		                    </td>
		                    <td class="text-center">
		                    	<a href="{{url('establecimientos').'/'.$establecimiento->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $establecimientos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>



