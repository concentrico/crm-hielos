<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-users"></i> Contactos</h5>
		<a class="text-success pull-right" id="btnContactoCreate" data-toggle="modal" data-target="#modalEstablecimientoContactoCreate">
			<i class="fa fa-plus"></i> Contacto 
		</a>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				@if(count($establecimiento->contactos) <= 0 )
				<div class="alert alert-warning" style="margin-top: 10px;">
					El establecimiento no tiene contactos registrados!
				</div>
				@else
					@foreach($establecimiento->contactos as $contacto)

						<a class="text-danger pull-right" onclick="showModalEstablecimientoContactoDelete({{$contacto->id}})">
							<i class="fa fa-trash"></i> Eliminar
						</a>
						@if($contacto->cliente_contacto_funcion_id == 1)
							<a class="text-success pull-right" onclick="showModalEstablecimientoContactoEdit({{$contacto->id}})">
								<i class="fa fa-edit"></i> Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</a>
						@else
							<a class="text-success pull-right" onclick="showModalEstablecimientoContactoEdit({{$contacto->id}})">
								<i class="fa fa-edit"></i> Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</a>
						@endif

						<h4 class="text-info">{{ $contacto->nombre_completo() }}</h4>

						<div class="row">
							<div class="col-sm-6">
								@if(count($contacto->funciones) > 0)
									<span style="font-weight: bold;">
										{{ (count($contacto->funciones) === 1 ? 'Función:' : 'Funciones:' ) }}
									</span>					
									@foreach($contacto->funciones->sortBy('nombre') as $key => $funcion)
										<span class="label {{ ($funcion->id == 1 ? 'label-danger' : 'label-default' ) }}" style="margin-top: 5px; display:inline-block;">
											{{ $funcion->nombre }}
										</span>
									@endforeach
								@endif
							</div>
							<div class="col-sm-6">
								<span style="font-weight: bold;">Establecimiento:</span>
								<span>

								@if($contacto->cliente_contacto_funcion_id != 1)
									@if(count($contacto->establecimientos) > 0)
										@foreach($contacto->establecimientos as $establecimiento)
											<span class="label label-primary" style="margin-top: 5px; display:inline-block;">{{ $establecimiento->sucursal }}</span>
										@endforeach
									@else
										<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
									@endif
								@else
									<span class="label label-default" style="margin-top: 5px; display:inline-block;">Todos</span>
								@endif
								</span>
							</div>
						</div>
						
						@if(count($contacto->emails) == 0 && count($contacto->telefonos) == 0)
							<small class="text-warning"> No existen medios de contacto disponibles para este contacto</small>
						
						@else
							<div class="row">
								@if(count($contacto->emails) > 0)
									<div class="col-sm-6">
										<br />
										<h5><i class="fa fa-envelope-o"></i> Email(s)</h5>
									@foreach($contacto->emails as $email)
										@if($email->tipo == 'personal')
											<i class="fa fa-home" style="width:16px"></i> {{ $email->valor }}<br />
										@elseif($email->tipo == 'trabajo')
											<i class="fa fa-building" style="width:16px"></i> {{ $email->valor }}<br />
										@else
											<i class="fa fa-circle" style="width:16px"></i> {{ $email->valor }}<br />
										@endif
									@endforeach
									</div>
								@endif
	
								@if(count($contacto->telefonos) > 0)
									<div class="col-sm-6">
										<br />
										<h5><i class="fa fa-phone"></i> Teléfono(s)</h5>
									@foreach($contacto->telefonos as $telefono)
										
										@if($telefono->tipo == 'casa')
											<i class="fa fa-home" style="width:16px"></i>
										@elseif($telefono->tipo == 'oficina')
											<i class="fa fa-building" style="width:16px"></i>
										@elseif($telefono->tipo == 'celular')
											<i class="fa fa-lg fa-mobile" style="width:16px"></i>
										@else
											<i class="fa fa-circle" style="width:16px"></i>
										@endif
										
										{{ $telefono->valor }} 
										
										@if($telefono->extension != null)
											&nbsp;&nbsp;&nbsp;Ext. {{ $telefono->extension }}
										@endif
										
										<br />
										
									@endforeach
									</div>
								@endif
							</div>
						@endif
						
						@if($contacto->comentarios != null)
							<div class="row">
								<div class="col-sm-12">
									<h5> Comentarios </h5>
									<span>{!! nl2br(e($contacto->comentarios)) !!}</span>
								</div>
							</div>
						@endif					
						<hr />

					@endforeach
				@endif
		    </div>
	    </div>
    </div>
</div>