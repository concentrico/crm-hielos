<div class="modal inmodal" id="modalRealizarCorte" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-danger"><i class="fa fa-clipboard modal-icon"></i> <BR/>Corte del día</h4>
				<h2 class="text-success" style="margin: 0px;">{{fecha_to_human(Carbon::now(),false)}}</h2>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('establecimientos/0/realizar-corte2'), 'novalidate' => ''] ) !!}
			<input type="hidden" name="pantalla" value="index">
				<div class="modal-body">
					<div class="row" id="corte">
                        <table class="table table-striped" id="tablaCorte">
                            <thead>
                            <tr>
                            	<th class="col-xs-2">ID</th>
                                <th class="col-xs-6">Producto</th>
                                <th class="col-xs-4">Inventario Actual</th>
                            </tr>
                            </thead>
                            <tbody id="body_tabla_corte">
                            </tbody>
                            <tfoot id="loading-productos" class="hidden">
                            	<tr>
                            		<td class="text-center" colspan="3"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></td>
                            	</tr>
                            </tfoot>
                        </table>
                        <div class="m-t-md pull-right">
                            <h2 class="product-main-price" id="cantidad_total_actual">0 <small class="text-muted">Bolsas Totales</small></h2>
                        </div>
                        <div id="error-productos" class="col-sm-12 hidden">
                        	<div class="alert alert-danger alert-dismissable">
                                El establecimiento no tiene productos asociados. <a class="alert-link" href="{{ url('conservadores')}}">Asigne Conservadores</a>.
                            </div>
                        </div>
					</div>

					@if ($errors->first('corteFailed') )
					    <div id="corte-failed" class="alert alert-danger text-center">
				           	{!!$errors->first('corteFailed')!!}
				        </div>
					@endif

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" id="btnCorte" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_realizar_corte'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		$('#modalRealizarCorte').modal('show');
	@endif

	function calcularCapacidadCorte(cap) {
		var sum = 0;
		$('.capproducto').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
	}

	function showModalRealizarCorte(establecimiento_id) {

		if ( $('#modalRealizarCorte').find('form').attr('action') == "{{ url('establecimientos') }}/" + establecimiento_id+"/realizar-corte2"){
			
		}else{
			$('#modalRealizarCorte').find('form').attr('action', "{{ url('establecimientos') }}/" + establecimiento_id+"/realizar-corte2")

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/inventario')}}',
	            data:{ 'type': '1', 'establecimiento_id': establecimiento_id, '_token': '{{ csrf_token()}}' },
	            beforeSend: function() {
	            	$("#body_tabla_corte").empty();
	            	$("#loading-productos").removeClass('hidden');
	            	$("#corte-failed").addClass('hidden');
		        },
	            success: function(data) {
	            	$("#loading-productos").addClass('hidden');
	            	if (data != '') {
	            		$("#body_tabla_corte").empty();
	            		$("#body_tabla_corte").html(data);
	            		$("#error-productos").addClass('hidden');
	            		$('#modalRealizarCorte').find('#btnCorte').removeAttr('disabled');
	            		$(".touchspin_cantidad").TouchSpin({
					        min: 0,max: 1000000000,step: 1,decimals: 0,boostat: 5,maxboostedstep: 10,buttondown_class: 'btn btn-white',buttonup_class: 'btn btn-white'
					    });
	            	} else {
	            		$("#body_tabla_corte").empty();
	            		$("#error-productos").removeClass('hidden');
	            		$('#modalRealizarCorte').find('#btnCorte').attr('disabled','disabled');
	            	}
	            	
	            }
	        });
		}


		$('#modalRealizarCorte').modal('show');
	}
</script>	
@endsection