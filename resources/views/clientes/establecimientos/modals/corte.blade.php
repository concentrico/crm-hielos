<div class="modal inmodal" id="modalRealizarCorte" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-danger"><i class="fa fa-clipboard modal-icon"></i> <BR/>Corte del día</h4>
				<h2 class="text-success" style="margin: 0px;">{{fecha_to_human(Carbon::now(),false)}}</h2>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('establecimientos/'.$establecimiento->id.'/realizar-corte'), 'novalidate' => ''] ) !!}
			<input type="hidden" name="pantalla" value="detalle">
				<div class="modal-body">
					<div class="row" id="corte">
                        <table class="table table-striped" id="tablaCorte">
                            <thead>
                            <tr>
                            	<th class="col-xs-2">ID</th>
                                <th class="col-xs-6">Producto</th>
                                <th class="col-xs-4">Inventario Actual</th>
                            </tr>
                            </thead>
                            <tbody id="body_tabla_conservadores">
                            @php
								$productos_array = $establecimiento->productos_disponibles();
							@endphp
							@foreach($productos_array as $producto_id)
								@php 
									$producto = \App\Models\Producto::findOrFail($producto_id);
								@endphp
								<tr>
                                    <td class="col-xs-2">{{$producto->id_codigo}}<input type="hidden" name="producto_corte[]" value="{{$producto->id}}"></td>
                                    <td class="col-xs-6">{{$producto->nombre}}</td>
                                    <td class="col-xs-4">
	                                    <div class="input-group" style="width: 100%;">
		                                    <input class="touchspin_cantidad positive-integer capproducto" type="text" name="cantidad_actual_corte[]" onblur="calcularCapacidadCorte(this.value)" onchange="calcularCapacidadCorte(this.value)"/>
		                                </div>
                                    </td>
                                </tr>
							@endforeach
                            </tbody>

                        </table>
                        <div class="m-t-md pull-right">
                            <h2 class="product-main-price" id="cantidad_total_actual">0 <small class="text-muted">Bolsas Totales</small></h2>
                        </div>
                        <div id="error-productos" class="col-sm-12 @if (count($productos_array) > 0) hidden @endif">
                        	<div class="alert alert-danger alert-dismissable">
                                El establecimiento no tiene productos asociados. <a class="alert-link" href="{{ url('conservadores') }}">Asigne Conservadores</a>.
                            </div>
                        </div>
					</div>

					@if ($errors->first('corteFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('corteFailed')!!}
				        </div>
					@endif

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					@if(count($productos_array) > 0)
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
					@else
						<button disabled="" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
					@endif
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_realizar_corte'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		$('#modalRealizarCorte').modal('show');
	@endif

	function showModalRealizarCorte() {
		$('#modalRealizarCorte').modal('show');
	}
</script>	
@endsection