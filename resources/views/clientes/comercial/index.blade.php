@extends('clientes.layouts.master')

@section('title', 'Perfil Comercial')

@section('attr_navlink_2', 'class="active"')

@section('breadcrumb_title', $cliente->nombre_corto )

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong> Perfil Comercial</strong></li>
<li></li>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 col-md-7" style="padding: 0 5px;">
		@include('clientes.comercial.iboxes.contactos')							
	</div>
	<div class="col-sm-12 col-md-5" style="padding: 0 5px;">
		@include('clientes.comercial.iboxes.datosf')
	</div>
</div>
@endsection