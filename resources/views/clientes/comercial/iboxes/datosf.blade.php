<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-gavel"></i> Datos fiscales</h5>
    </div>
    
    <div class="ibox-content" style="min-height: 500px;max-height: 500px; overflow-y: scroll;">

	    @if(count($cliente->datos_fiscales) == 0)
		<div class="alert alert-warning" style="margin-top: 10px;">
			El cliente no tiene datos fiscales registrados.
		</div>
	    @endif

		@foreach($cliente->datos_fiscales as $key => $df)
	    <div class="row">
		    <div class="col-sm-12">
				<h4 class="text-info">{{ $df->rfc }}</h4>
				<span style="font-weight: bold;">Tipo de persona: </span>
				@if($df->persona == null)
					<span class="text-muted"> N.D. </span>
				@else
					<span class="text-primary"> {{ ucfirst($df->persona) }}</span>
				@endif
				<br />
				<span style="font-weight: bold;">
				@if($df->persona == 'moral')
					Razón social: 
				@else
					Nombre completo: 
				@endif
				</span>
				@if($df->razon_social == null)
					<span class="text-muted"> N.D. </span>
				@else
					<span class="text-primary"> {{ $df->razon_social }}</span>
				@endif
				<br />
					<span style="font-weight: bold;">
						Representante Legal: 
					</span>
					<span class="text-primary"> {{ $df->representante_legal }}</span>
				@if( imprime_direccion($df) !== "")
				<h4 class="text-success">Dirección de facturación</h4>
				<address>
					{!! imprime_direccion($df) !!}
				</address>				
				@endif
		    </div>
	    </div>
	    
	    @if(count($cliente->datos_fiscales) > $key+1)
			<hr />
	    @endif

	    @endforeach
	    
	    
    </div>
    
</div>