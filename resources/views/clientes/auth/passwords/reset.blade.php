<!-- resources/views/auth/reset.blade.php -->
<html lang="es"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    
    <title>@if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | Reestablecer Contraseña</title>

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Custom styles  -->
	<link rel="stylesheet" href="{{ asset('css/login.css') }}">
	
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>

  <body>

    <div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				@if(getSystemSettings()->site_logo != null) 
                    <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" width="100">
                @else 
                    <img src="{{ asset('img/logo-default.png') }}" alt="Logo" width="100">
                @endif
			</div>
		</div>
		{!! Form::open(['method' => 'POST',  'url' => url('password/reset'), 'class' => 'form-signin'] ) !!}
            {!! Form::hidden('token', $token, null) !!}
			<h2 class="form-signin-heading text-center">Reestablecer Contraseña</h2>
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<label class="text-success text-center">
				Ingresa el correo en el que recibiste la liga y tu nueva contraseña
			</label>
			{!! Form::label('email', 'Email', ['class' => 'sr-only text-center']) !!}
            {!! Form::email('email', null, ['id' => 'inputEmail', 'class' => 'form-control', 'placeholder' => 'Email', 'autofocus' => '']) !!}
			{!! Form::label('password', 'Contraseña', ['class' => 'sr-only text-center']) !!}
            {!! Form::password('password', ['id' => 'inputPassword', 'class' => 'form-control', 'placeholder' => 'Contraseña', 'style' => 'margin-bottom:0px;']) !!}
			{!! Form::label('password_confirmation', 'Confirmar Contraseña', ['class' => 'sr-only text-center']) !!}
            {!! Form::password('password_confirmation', ['id' => 'inputPassword', 'class' => 'form-control', 'placeholder' => 'Confirmar Contraseña']) !!}

            <div class="g-recaptcha" data-sitekey="{{getSystemSettings()->google_recaptcha_api_key}}"></div>
            <BR/>
			<button class="btn btn-lg btn-primary btn-block btn-login" type="submit">Reestablecer</button>
		{!! Form::close() !!}

    </div> <!-- /container -->

	<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  

	</body>
</html>