<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CRM Hielos</title>
    <style type="text/css" media="screen">
    		/* -------------------------------------
			    GLOBAL
			    A very basic CSS reset
			------------------------------------- */
			* {
			    margin: 0;
			    padding: 0;
			    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
			    box-sizing: border-box;
			    font-size: 14px;
			}

			img {
			    max-width: 100%;
			}

			body {
			    -webkit-font-smoothing: antialiased;
			    -webkit-text-size-adjust: none;
			    width: 100% !important;
			    height: 100%;
			    line-height: 1.6;
			}

			/* Let's make sure all tables have defaults */
			table td {
			    vertical-align: top;
			}

			/* -------------------------------------
			    BODY & CONTAINER
			------------------------------------- */
			body {
			    background-color: #f6f6f6;
			}

			.body-wrap {
			    background-color: #f6f6f6;
			    width: 100%;
			}

			.container {
			    display: block !important;
			    max-width: 600px !important;
			    margin: 0 auto !important;
			    /* makes it centered */
			    clear: both !important;
			}

			.content {
			    max-width: 600px;
			    margin: 0 auto;
			    display: block;
			    padding: 20px;
			}

			/* -------------------------------------
			    HEADER, FOOTER, MAIN
			------------------------------------- */
			.main {
			    background: #fff;
			    border: 1px solid #e9e9e9;
			    border-radius: 3px;
			}

			.content-wrap {
			    padding: 20px;
			}

			.content-block {
			    padding: 0 0 20px;
			}

			.header {
			    width: 100%;
			    margin-bottom: 20px;
			}

			.footer {
			    width: 100%;
			    clear: both;
			    color: #999;
			    padding: 20px;
			}
			.footer a {
			    color: #999;
			}
			.footer p, .footer a, .footer unsubscribe, .footer td {
			    font-size: 12px;
			}

			/* -------------------------------------
			    TYPOGRAPHY
			------------------------------------- */
			h1, h2, h3 {
			    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
			    color: #000;
			    margin: 40px 0 0;
			    line-height: 1.2;
			    font-weight: 400;
			}

			h1 {
			    font-size: 32px;
			    font-weight: 500;
			}

			h2 {
			    font-size: 24px;
			}

			h3 {
			    font-size: 18px;
			}

			h4 {
			    font-size: 14px;
			    font-weight: 600;
			}

			p, ul, ol {
			    margin-bottom: 10px;
			    font-weight: normal;
			}
			p li, ul li, ol li {
			    margin-left: 5px;
			    list-style-position: inside;
			}

			/* -------------------------------------
			    LINKS & BUTTONS
			------------------------------------- */
			a {
			    color: #1c84c6;
			    text-decoration: underline;
			}

			.btn-primary {
			    text-decoration: none;
			    color: #FFF;
			    background-color: #2e368e;
			    border: solid #1c84c6;
			    border-width: 5px 10px;
			    line-height: 2;
			    font-weight: bold;
			    text-align: center;
			    cursor: pointer;
			    display: inline-block;
			    border-radius: 5px;
			    text-transform: capitalize;
			}

			/* -------------------------------------
			    OTHER STYLES THAT MIGHT BE USEFUL
			------------------------------------- */
			.last {
			    margin-bottom: 0;
			}

			.first {
			    margin-top: 0;
			}

			.aligncenter {
			    text-align: center;
			}

			.alignright {
			    text-align: right;
			}

			.alignleft {
			    text-align: left;
			}

			.clear {
			    clear: both;
			}

			/* -------------------------------------
			    ALERTS
			    Change the class depending on warning email, good email or bad email
			------------------------------------- */
			.alert {
			    font-size: 16px;
			    color: #fff;
			    font-weight: 500;
			    padding: 20px;
			    text-align: center;
			    border-radius: 3px 3px 0 0;
			}
			.alert a {
			    color: #fff;
			    text-decoration: none;
			    font-weight: 500;
			    font-size: 16px;
			}
			.alert.alert-warning {
			    background: #f8ac59;
			}
			.alert.alert-bad {
			    background: #ed5565;
			}
			.alert.alert-good {
			    background: #1ab394;
			}

			/* -------------------------------------
			    INVOICE
			    Styles for the billing table
			------------------------------------- */
			.invoice {
			    margin: 40px auto;
			    text-align: left;
			    width: 80%;
			}
			.invoice td {
			    padding: 5px 0;
			}
			.invoice .invoice-items {
			    width: 100%;
			}
			.invoice .invoice-items td {
			    border-top: #eee 1px solid;
			}
			.invoice .invoice-items .total td {
			    border-top: 2px solid #333;
			    border-bottom: 2px solid #333;
			    font-weight: 700;
			}

			/* -------------------------------------
			    RESPONSIVE AND MOBILE FRIENDLY STYLES
			------------------------------------- */
			@media only screen and (max-width: 640px) {
			    h1, h2, h3, h4 {
			        font-weight: 600 !important;
			        margin: 20px 0 5px !important;
			    }

			    h1 {
			        font-size: 22px !important;
			    }

			    h2 {
			        font-size: 18px !important;
			    }

			    h3 {
			        font-size: 16px !important;
			    }

			    .container {
			        width: 100% !important;
			    }

			    .content, .content-wrap {
			        padding: 10px !important;
			    }

			    .invoice {
			        width: 100% !important;
			    }
			}
    </style>
    @php

    $style = [
        /* Layout ------------------------------ */

        'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
        'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

        /* Masthead ----------------------- */

        'email-masthead' => 'padding: 25px 0; text-align: center;',
        'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

        'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
        'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
        'email-body_cell' => 'padding: 35px;',

        'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
        'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

        /* Body ------------------------------ */

        'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
        'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

        /* Type ------------------------------ */

        'anchor' => 'color: #3869D4;',
        'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
        'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
        'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
        'paragraph-center' => 'text-align: center;',

        /* Buttons ------------------------------ */

        'button' => 'display: block; display: inline-block; width: auto; min-height: 20px; padding: 10px;
                     background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                     text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

        'button--green' => 'background-color: #22BC66;',
        'button--red' => 'background-color: #dc4d2f;',
        'button--blue' => 'background-color: #3869D4;',
    ];
    @endphp

    @php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; @endphp
</head>

<body>

<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" class="text-center">
                                        <img class="img-responsive" src="{{env('EXTERNAL_LOGO_URL')}}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>Hola, {{$usuario->nombre}}:</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block" style="text-align: justify;">
										Hemos recibido una solicitud para reestablecer la contraseña de tu cuenta en <b>{{getSystemSettings()->site_name}}</b>, en caso de no haber solicitado este proceso, haz caso omiso al correo y continúa tranquilamente.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
										Para re-activar tu cuenta, da clic en el botón <b>Reestablecer contraseña</b>:
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block aligncenter">
                                    	{{--
                                        <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($usuario->getEmailForPasswordReset()) }}" style="{{ $style['button'] }}"
                                            target="_blank">Reestablecer contraseña</a>
                                        --}}
                                    	@php
                                        	$link = 'http://customers.'.env('APP_DOMAIN').'/password/reset/'.$token.'?email='.urlencode($usuario->getEmailForPasswordReset());
                                        @endphp
                                        <a href="{{ $url }}" style="{{ $style['button'] }}" target="_blank">Reestablecer contraseña</a>
                                    </td>
                                </tr>
                                <!-- Sub Copy -->
                                @if (isset($actionText))
                                    <table style="{{ $style['body_sub'] }}">
                                        <tr>
                                            <td style="{{ $fontFamily }}">
                                                <p style="{{ $style['paragraph-sub'] }}">
                                                    Si tiene problemas al hacer clic en el botón "Reestablecer contraseña", copie y pegue la URL a continuación en su navegador:
                                                </p>

                                                <p style="{{ $style['paragraph-sub'] }}">
                                                	{{--
                                                    <a style="{{ $style['anchor'] }}" href="{{ $link = url('password/reset', $token).'?email='.urlencode($usuario->getEmailForPasswordReset()) }}" target="_blank">
                                                        {{ $actionUrl }}
                                                    </a>
                                                    --}}

                                                    <a style="{{ $style['anchor'] }}" href="{{ $link }}" target="_blank">
                                                        {{ $actionUrl }}
                                                    </a>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                @endif
                                <tr>
                                    <td class="content-block">
                                        <strong>* Favor de no contestar este correo.</strong>
                                    </td>
                                </tr>
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block"><a href="{{getSystemSettings()->site_url}}">{{getSystemSettings()->site_url}}</a></td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>