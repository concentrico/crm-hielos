<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-clipboard"></i> Últimos Cortes de Inventario </h5>
    </div>
    
    <div class="ibox-content" style="max-height: 400px; overflow-y: scroll;">
        <div class="col-lg-12">
          <table id="dataTableCortes" name="dataTableCortes" class="table table-res" data-cascade="true" data-expand-first="false">
              <thead>
                <tr>
                  <th data-type="html" data-breakpoints="sm">Fecha de Corte</th>
                  <th data-type="html" data-breakpoints="sm">Producto</th>
                  <th data-type="html" data-breakpoints="sm">Capacidad Actual</th>
                  <th data-type="html" data-breakpoints="sm">Realizado por</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($establecimiento->cortes->sortByDesc('created_at') as $corte)
                <tr> 
                  <td>{{ fecha_to_human($corte->created_at,true) }}</td>
                  <td>{{ $corte->producto->nombre }}</td>
                  <td>{{ $corte->cantidad_actual }} bolsas</td>
                  <td>{!! $corte->creado_por->liga() !!}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
    </div>
</div>