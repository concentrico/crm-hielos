 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Establecimientos</h5>
    </div>
    <div class="ibox-content">
		<table id="dataTableInventarioExterno" name="dataTableInventarioExterno" class="table table-res" data-cascade="true" data-expand-first="false">
			<thead>
				<tr>
					<th data-type="html" data-breakpoints="sm"></th>
					<th data-type="html">Establecimiento</th>
					<th data-type="html" data-breakpoints="xs">Cliente</th>
					<th data-type="html" data-breakpoints="xs">Último Corte</th>
					<th data-type="html" >Inventario Actual</th>
					<th data-type="html" data-breakpoints="xs"></th>
					<th data-type="html" data-breakpoints="xs"></th>
				</tr>
	        </thead>
		<tbody>
                @foreach ($establecimientos as $inventario)
                    <tr>
                        <td class="text-center">{!!$inventario->getColorInventario()!!}</td>
                        <td><a class="text-success">{{ $inventario->sucursal }}</a></td>
                         <td><a class="text-success">{{ $inventario->cliente->nombre_corto }}</a></td>
                        <td>{!!$inventario->ultimo_corte()!!}
                        </td>
	                    <td>
	                    	@if($inventario->capacidad_actual != null)
	                    		{{$inventario->capacidad_actual}} bolsas
	                    	@else
	                    		Sin registros
	                    	@endif
	                    </td>
	                    <td >
	                    	@if($inventario->status_inventario == 0)
	                    		<span class="donut" data-peity='{ "fill": ["red", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
	                    	@elseif($inventario->status_inventario == 1)
	                    		<span class="donut" data-peity='{ "fill": ["orange", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
	                    	@elseif($inventario->status_inventario == 2)
	                    		<span class="donut" data-peity='{ "fill": ["green", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
	                    	@else
	                    		<span class="donut" data-peity='{ "fill": ["#eeeeee", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
	                    	@endif
	                    </td>
	                    <td>
                    		@if($inventario->fecha_ultimo_corte() != '')
	                    		@if($inventario->fecha_ultimo_corte() != Carbon::now()->formatLocalized('%d/%m/%Y') )
	                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" onclick="showModalRealizarCorte({{$inventario->id}})"><i class="fa fa-clipboard"></i> Realizar Corte</button>
	                    		@else
	                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" style="color:white;" disabled=""><i class="fa fa-clipboard"></i> Realizar Corte</button>
		                    	@endif
	                    	@else
	                    		<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" onclick="showModalRealizarCorte({{$inventario->id}})"><i class="fa fa-clipboard"></i> Realizar Corte</button>
	                    	@endif
	                    </td>
                    </tr>
				@endforeach
		</tbody>
		</table>
    </div>
</div>