@extends('clientes.layouts.master')

@section('title', 'Dashboard')

@section('attr_navlink_1', 'class="active"')

@section('breadcrumb_title', 'Dashboard')

@section('assets_top_top')
<meta name="csrf-token" content="{{ Session::token() }}">
<link href="{{ asset('fullcalendar/fullcalendar.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css">
<style type="text/css" media="screen">
 .highcharts-button {
    display:none;
}   
</style>
@endsection

<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">-->
@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Home</strong></li>
<li></li>
@endsection

@section('content')
	<div class="row white-bg">
		<div class="col-lg-12">
			{!! Form::open(['method' => 'GET',  'url' => url('/')] ) !!}
	        <div class="form-group col-sm-4 col-md-3 col-lg-2 pull-right">
	            {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
	            <button type="submit" class="btn btn-primary col-sm-12"><i class="fa fa-filter"></i> Filtrar</button>
	        </div>
	        <div class="form-group col-sm-4 col-lg-2 pull-right">
	            {!! Form::label('f_year_consumption', 'Año', ['class' => 'control-label']) !!}
	            {!! Form::select('f_year_consumption', $years, (Input::get('f_year_consumption') != null ? Input::get('f_year_consumption') : date('Y') ), ['class' => 'form-control selectdp'] ) !!}
	        </div>
	        {!! Form::close() !!}
	    </div>
	    <div class="col-lg-12">
	        @include('clientes.dashboard.iboxes.consumo')
	    </div>
	    <div class="col-lg-12">
	        @include('clientes.dashboard.iboxes.calendario')
	    </div>
	</div>

@endsection


@section('assets_bottom')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>   
<script src="{{ asset('fullcalendar/fullcalendar.js') }}"></script> 
<script src="{{ asset('fullcalendar/locale/es.js') }}"></script>    

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>
<script type="text/javascript">

</script>

@endsection
