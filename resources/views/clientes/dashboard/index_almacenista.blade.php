@extends('clientes.layouts.master')

@section('title', 'Home')

@section('attr_navlink_1', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', 'Inventario Establecimientos')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('clientes.dashboard.iboxes.tabla_establecimientos')
	</div>

	<div class="col-sm-12">
	</div>
</div>

@include('clientes.dashboard.modals.corte')

@endsection

@section('assets_bottom')

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>

<script type="text/javascript">

	establecimientos = [];
	
	@foreach($establecimientos as $establecimiento)
	
		establecimientos[{{ $establecimiento->id }}] = {
			sucursal: '{{ $establecimiento->sucursal }}',
			capacidad_minima: '{{ $establecimiento->capacidad_minima }}',
			capacidad_maxima: '{{ $establecimiento->capacidad_maxima }}',
			punto_reorden: '{{ $establecimiento->punto_reorden }}',
		}
	
	@endforeach

	@if(session('corte_success'))
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif

</script>
@endsection
