<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-truck"></i> Datos de Entrega</h5>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-xs-6 text-center">
				<label class="text-info">Chofer Asignado</label><br />
				<div class="@if ($errors->first('chofer_asignado') !== "") has-error @endif">
					{!! Form::text('chofer_asignado', $pedido->chofer->nombre, ['id'=> 'chofer_asignado','class' => 'form-control', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('chofer_asignado')}}</span>
                </div>
		    </div>
		    <div class="col-xs-6 text-center">
		    	<label class="text-info">Recibido por</label><br />
		    	@if($pedido->firmado_por != null)
					@php
					$nombre_completo=$pedido->cliente_firma->nombre.' '.$pedido->cliente_firma->apellido_paterno.' '.$pedido->cliente_firma->apellido_materno;
					@endphp

					{!! Form::text('firmado_por', $nombre_completo, ['id'=> 'firmado_por','class' => 'form-control', 'readonly' => 'readonly']) !!}

				@else
					@if($pedido->firmado_otro != null)
						{!! Form::text('firmado_otro', $pedido->firmado_otro, ['id'=> 'firmado_otro','class' => 'form-control', 'readonly' => 'readonly']) !!}
					@else
						{!! Form::text('firmado_otro', 'Sin definir', ['id'=> 'firmado_otro','class' => 'form-control', 'readonly' => 'readonly']) !!}
						
					@endif
				@endif
		    </div>
		</div>
		<div class="row m-t-md">
			<div class="col-md-4"></div>
		    <div class="col-md-4 text-center">
		    	<label class="text-info">Firma Recibido</label><br />
                @if($pedido->firma_path != null)
            		<img src="{{url($pedido->firma_path)}}" />
				@else
					<div class="product-imitation">
                        [ N.D. ]
                    </div>
				@endif
            </div>
            <div class="col-md-4"></div>
	    </div>
    </div>
</div>