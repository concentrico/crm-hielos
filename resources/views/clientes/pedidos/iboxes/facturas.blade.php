<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-file-pdf-o text-info"></i> Facturas</h5>

	   	@if($pedido->total - $pedido->facturas->sum('total') == 0)
			<div class="pull-right text-info">
				<i class="fa fa-lg fa-check-circle-o text-info " style="font-size: 1.9em;"></i> &nbsp;Facturado
			</div>
			@if($pedido->checkFacturasVencidas())
				<div class="pull-right text-danger m-r-xl">
					<i class="fa fa-lg fa-check-circle-o text-danger " style="font-size: 1.9em;"></i> &nbsp;Factura Vencida
				</div>
			@endif
		@endif
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-md-12">
				@if(count($pedido->facturas) > 0)
					<table class="table table-res" id="tablaFacturasPedido" data-cascade="true">
						<thead>
							<tr>
								<th class="col-xs-1 text-center">Folio</th>
								<th class="col-xs-3 text-center">Facturado</th>
								<th class="col-xs-2 text-center">Total</th>
								<th class="col-xs-3 text-center">Vencimiento</th>
								<th class="col-xs-3 text-center" data-type="html" data-breakpoints="xs sm">Archivos</th>
							</tr>
						</thead>
						<tbody>
							@foreach($pedido->facturas as $factura)
							<tr>
								<td class="text-center">{{$factura->folio}}</td>
								<td class="text-center">{{fecha_to_human($factura->fecha_facturacion,false)}}</td>
								<td class="text-center">${{ number_format($factura->total, 2, '.', ',') }}</td>
								<td class="text-center text-danger">{{fecha_to_human($factura->fecha_vencimiento,false)}}</td>
								<td class="text-center">
									<ul class="list-unstyled file-list" style="display: inline-flex;">

										@if($factura->pdf_path != null)
											<li><a target="_blank" href="{{ asset($factura->pdf_path) }}"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
										@else
											<li><a style="opacity: 0.5; cursor: help;"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
										@endif
										&nbsp;
										@if($factura->xml_path != null)
											<li><a target="_blank" href="{{ asset($factura->xml_path) }}"><i class="fa fa-file"></i> XML</a></li>
										@else
											<li><a style="opacity: 0.5; cursor: help;"><i class="fa fa-file"></i> XML</a></li>
										@endif
		                            </ul>
								</td>
							</tr>
							@endforeach									
						</tbody>
					</table>
				@else
					<div class="alert alert-warning text-center" style="margin-top: 10px;">
						El pedido no tiene facturas registradas.
					</div>
				@endif 
			</div>
		</div>
    </div>

</div>