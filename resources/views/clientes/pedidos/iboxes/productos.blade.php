<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-cubes"></i> Productos</h5>
    </div>
    
    <div class="ibox-content">
	    	    
	    <div class="row">
		    <div class="col-sm-12">
				<div class="m-t">
					<table class="table table-striped" id="tablaProductosPedido">
						<thead>
							<tr>
								<th class="col-xs-4 text-center">Producto</th>
		                        <th class="col-xs-2 text-center">Costo Unitario</th>
		                        <th class="col-xs-2 text-center">Solicitado</th>
		                        <th class="col-xs-2 text-center">Recibido</th>
		                        <th class="col-xs-3 text-center">Subtotal</th>
							</tr>
						</thead>
						<tbody>
							@if(count($pedido->productos) > 0)
								@foreach($pedido->productos as $producto)
									<tr>
										<td>
											<h4 class="text-navy"> {{ $producto->producto->nombre }} </h4>
										</td>
										<td class="text-center">
											<h4> ${{number_format($producto->precio_unitario,2)}} </h4>
										</td>
										<td class="text-center prod-compra">
										<h4> {{ $producto->cantidad_requerida }} </h4>
										</td>
										<td class="text-center prod-venta">
										@if($producto->cantidad_recibida != null)
											<h4 class="text-danger"> {{ $producto->cantidad_recibida }} </h4>
										@else
											<h4> N.D. </h4>
										@endif
										</td>
										<td class="text-right prod-dif">
											<h4 id="subtotal_producto{{$producto->producto_id}}">${{number_format($producto->subtotal,2)}}</h4>
										</td>
									</tr>
								@endforeach
								<tfoot>
								@if($pedido->iva > 0)
									<tr>
										<td class="text-right" colspan="4">
											<strong>SUBTOTAL:</strong>
										</td>
										<td class="text-right text-success cot-subtotal">${{ number_format($pedido->subtotal, 2, '.', ',') }}</td>
									</tr>
									<tr>
										<td class="text-right" colspan="4">
											<strong>IVA:</strong>
										</td>
										<td class="text-right text-success cot-iva">${{ number_format($pedido->iva, 2, '.', ',') }}</td>
									</tr>
								@endif
								<tr>
									<td class="text-right" colspan="4">
										<strong>TOTAL:</strong>
									</td>
									<td class="text-right text-success cot-total">${{ number_format($pedido->total, 2, '.', ',') }}</td>
								</tr>
								</tfoot>
							@else
								<tfoot>
								<tr>
									<td class="text-center" colspan="5">
										<div class="alert alert-danger">
						                    El Pedido no tiene productos.
						                </div>
									</td>
								</tr>
								</tfoot>
							@endif
						</tbody>
					</table>
				</div>
		    </div>
	    </div>
    </div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "progressBar": true,
	  "preventDuplicates": false,
	  "positionClass": "toast-bottom-right",
	  "onclick": null,
	  "showDuration": "400",
	  "hideDuration": "1000",
	  "timeOut": "4000",
	  "extendedTimeOut": "4000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
</script>	
@endsection