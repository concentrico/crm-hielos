 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Pedidos</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableInventarioExterno" name="dataTableInventarioExterno" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" >{!! getHeaderLink('ID', 'ID') !!}</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('ESTABLECIMIENTO', 'Establecimiento') !!}</th>
						<th data-type="html" data-breakpoints="xs sm">{!! getHeaderLink('CLIENTE', 'Cliente') !!}</th>
						<th data-type="html" data-breakpoints="">{!! getHeaderLink('ENTREGADO', 'Entregado el') !!}</th>
						<th data-type="html" data-breakpoints="xs sm">{!! getHeaderLink('PORCOBRAR', 'Total') !!}</th>
						<th data-type="html" data-breakpoints="xs">Vencido</th>
						<th data-type="html" >{!! getHeaderLink('STATUS', 'Status') !!}</th>
						<th data-type="html" data-breakpoints="xs sm"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($pedidos as $pedido)
	                    <tr>
	                        <td>
	                        	<a class="text-navy" href="{{ url('pedidos/'.$pedido->id) }}">#{{ $pedido->id }}</a>
	                        </td>
	                        <td><a href="{{ url('establecimientos/'.$pedido->establecimiento->id) }}">{{ $pedido->establecimiento->sucursal }}</a></td>
	                         <td><a href="{{ url('informacion-comercial') }}">{{ $pedido->cliente->nombre_corto }}</a></td>
	                        <td>
                                @if($pedido->entregado_at != null)
                                    {{fecha_to_human($pedido->entregado_at,false) }}
                                    -
                                    {{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
                                @else
                                    N.D.
                                @endif
                            </td>
		                    <td>${{ number_format($pedido->total(), 2, '.', ',') }}</td>
		                    <td class="text-center">
	                        	@if($pedido->por_cobrar() > 0.00)
				                    <span class="text-danger">
				                    	${{ number_format($pedido->por_cobrar(), 2, '.', ',') }} 
					                </span>
					            @else
					            	<span>
				                    	${{ number_format($pedido->por_cobrar(), 2, '.', ',') }} 
					                </span>
					            @endif
		                    </td>
		                    <td>
		                    	@php
		                    		switch ($pedido->status) {
		                    			case '1':
		                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">Programado</span>';
		                    				break;
		                    			case '2':
		                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">En Camino</span>';
		                    				break;
		                    			case '3':
		                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Entregado</span>';
		                    				break;
		                    			case '4':
		                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Devolución</span>';
		                    				break;
		                    			case '5':
		                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Cancelado</span>';
		                    				break;
		                    			default:
		                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>';
		                    				break;
		                    		}
		                    	@endphp
		                    </td>

		                    <td class="text-center">
		                    	<a href="{{url('pedidos').'/'.$pedido->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    	<a href="{{url('pedidos/' . $pedido->id . '/remision') }}"><i class="fa fa-lg fa-file-pdf-o"></i></a>
		                    </td>
	                    </tr>
					@endforeach
			</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>