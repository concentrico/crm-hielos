<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-info-circle"></i> Info</h5>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-center">
		    	@php
            		switch ($pedido->status) {
            			case '1':
            				echo '<span class="label label-primary" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
            				break;
            			case '2':
            				echo '<span class="label label-info" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
            				break;
            			case '3':
            				echo '<span class="label label-success" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
            				break;
            			case '4':
            				echo '<span class="label label-warning" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
            				break;
            			case '5':
            				echo '<span class="label label-danger" style="font-size: 1.3em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
            				break;
            			default:
            				echo '<span class="label label-default" style="font-size: 1.3em; display:inline-block;">N.D.</span>';
            				break;
            		}
            	@endphp
		    </div>
	    </div>
	    <hr />
	    <div class="row">
		    <div class="col-sm-12">

		    	<div class="row">
					<div class="col-xs-6">
						<label class="text-info">Cliente</label><br />
							{{$pedido->cliente->nombre_corto}}
					</div>
					<div class="col-xs-6">
						<label class="text-info">Establecimiento</label><br />
						@if($pedido->cliente_establecimiento_id != null)
		                	{{$pedido->establecimiento->sucursal}}
						@else
							No Aplica
						@endif
		            </div>
				</div>
				<br /><br />

				<div class="row">
		    		<div class="col-xs-6">
		    			<label class="text-info">Fecha de Petición</label><br />
		    			{{ fecha_to_human($pedido->created_at,false) }}
		    		</div>
		    		<div class="col-xs-6">
		    			<label class="text-info">Es Evento</label><br />
						@if($pedido->es_evento == '1')
							Sí
						@else
							No
						@endif
		    		</div>
		    	</div>
				<br /><br />
		    	<div class="row">
		    		<div class="col-xs-6" style="font-weight: bold;">
		    			<label class="text-info">Entregado:</label><br />
						@if($pedido->entregado_at != null)
							{{ fecha_to_human($pedido->entregado_at,false) }}
						@else
							N.D.
						@endif
		    		</div>
		    		<div class="col-xs-6" style="font-weight: bold;">
		    			<label class="text-info">Hora:</label><br />
		    			@if($pedido->entregado_at != null)
		    				@php
						      $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
						    @endphp
						   	{{ $hora_entrega }}
						@else
							N.D.
						@endif
		    		</div>
		    	</div>
				<hr />
		    	@if($pedido->status > 2)
			    	<div class="row">
			    		<div class="col-xs-12" style="font-weight: bold; text-align: center;">
			    			<a class="btn btn-primary btn-rounded btn-block" href="{{ url('pedidos/' . $pedido->id . '/remision') }}"><i class="fa fa-file-pdf-o"></i> Descargar Remisión</a>
						</div>
					</div>
				@endif
		    </div>
	    </div>
    </div>
</div>