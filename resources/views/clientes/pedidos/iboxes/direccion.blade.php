<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-map-marker"></i> Dirección Entrega</h5>
    </div>
    
    <div class="ibox-content">

    	@if($pedido->evento_calle != null)
		    <div class="row">
			    <div class="col-sm-12">
					<span class="text-success">Calle: </span>
					<span class="text-primary">{{ $pedido->evento_calle }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número exterior: </span>
					<span class="text-primary">{{ $pedido->evento_numero_exterior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número interior: </span>
					@if($pedido->evento_numero_interior != null)
						<span class="text-muted">{{ $pedido->evento_numero_interior }}</span>
					@else
						<span class="text-primary">N.A.</span>
					@endif
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Colonia: </span>
					<span class="text-primary">{{ $pedido->evento_colonia }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Código Postal: </span>
					<span class="text-primary">{{ $pedido->evento_codigo_postal }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Municipio: </span>
					<span class="text-primary">{{ $pedido->evento_municipio }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Estado: </span>
					<span class="text-primary">{{ $pedido->evento_estado }}</span>
			    </div>
		    </div>
		@else
			<div class="row">
			    <div class="col-sm-12">
					<span class="text-success">Calle: </span>
						<span class="text-muted">{{ $pedido->establecimiento->calle }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número exterior: </span>
						<span class="text-muted">{{ $pedido->establecimiento->numero_exterior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número interior: </span>
						<span class="text-muted">{{ $pedido->establecimiento->numero_interior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Colonia: </span>
						<span class="text-muted">{{ $pedido->establecimiento->colonia }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Código Postal: </span>
						<span class="text-muted">{{ $pedido->establecimiento->codigo_postal }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Municipio: </span>
						<span class="text-muted">{{ $pedido->establecimiento->municipio }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Estado: </span>
						<span class="text-muted">{{ $pedido->establecimiento->estado }}</span>
			    </div>
		    </div>
		@endif
    </div>
</div>