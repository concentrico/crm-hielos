@extends('clientes.layouts.master')

@section('title', 'Pedido #'.$pedido->id)

@section('attr_navlink_4', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/slick/slick.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/slick/slick-theme.css') }}" rel="stylesheet" />
<style type="text/css" media="screen">
.i-checks{
	line-height: 32px;
	display: inline-block;
}
.selectdias {
	width: 50%!important;
	margin-left: 10%;
	display: inline-block;
}
.table > tbody > tr > td{
    border: 0px!important;
}
.btn:active:focus, .btn:focus {
    outline: none!important;
}

.facturas{
    float:right;
    position:relative;
    left:-50%;
    text-align:left;
}
.facturas ul{
    list-style:none;
    position:relative;
    left:50%;
}

</style>
@endsection

@section('breadcrumb_title', 'Pedido #'.$pedido->id)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('pedidos') }}">Pedidos</a></li>
<li class="active"><strong>Pedido #{{ $pedido->id }}</strong></li>
<li></li>
@endsection

@section('content')

    <div class="row">
		<div class="col-sm-12 col-md-4 col-lg-4" style="padding: 0 5px;">
			@include('clientes.pedidos.iboxes.info')
			@include('clientes.pedidos.iboxes.direccion')
		</div>
		<div class="col-sm-12 col-md-8 col-lg-8" style="padding: 0 5px;">
			<div class="row">
				<div class="col-sm-12">
					@include('clientes.pedidos.iboxes.productos')
					@include('clientes.pedidos.iboxes.facturas')
					@include('clientes.pedidos.iboxes.entrega')
				</div>
			</div>
		</div>
	</div>
    
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/slick/slick.min.js') }}"></script>
<script src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>

@endsection