@extends('clientes.layouts.master')

@section('title', 'Perfil Comercial')

@section('breadcrumb_title', 'Mi Perfil')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong> Mi Perfil</strong></li>
<li></li>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 col-md-8 col-sm-offset-2">
		<div class="ibox float-e-margin">
		    <div class="ibox-title">
		        <h5><i class="fa fa-user-circle-o"></i> Mi Perfil </h5>
		    </div>
		    
		    <div class="ibox-content">

			    {!! Form::open(['method' => 'PUT',  'url' => url('perfil')] ) !!}

			    <div class="row">
				     <div class="form-group col-sm-6 @if ($errors->first('nombre') !== "") has-error @endif">
			            {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
			            {!! Form::text('nombre', $usuario->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
			            <span class="help-block">{{ $errors->first('nombre')}}</span>
			        </div>
			        <div class="form-group col-sm-6 @if ($errors->first('username') !== "") has-error @endif">
			            {!! Form::label('username', 'Usuario', ['class' => 'control-label']) !!}
			            {!! Form::text('username', $usuario->username , ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
			            <span class="help-block">{{ $errors->first('username')}}</span>
			        </div>
			    </div>

			    <div class="row">
				     <div class="form-group col-sm-6 @if ($errors->first('email') !== "") has-error @endif">
			            {!! Form::label('email', 'Correo Electrónico', ['class' => 'control-label']) !!}
			            {!! Form::text('email', $usuario->email, ['class' => 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
			            <span class="help-block">{{ $errors->first('email')}}</span>
			        </div>
			        <div class="form-group col-sm-6 @if ($errors->first('fecha_nacimiento') !== "") has-error @endif">
			            {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento', ['class' => 'control-label']) !!}
			            <div class="input-group date">
		                	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							{!! Form::text('fecha_nacimiento', $usuario->fecha_nacimiento, ['id' => 'fecha_nacimiento', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
		                </div>
			            <span class="help-block">{{ $errors->first('fecha_nacimiento')}}</span>
			        </div>
			    </div>
			    <div class="row">
			    	<div class="form-group col-sm-6 @if ($errors->first('telefono') !== "") has-error @endif">
			            {!! Form::label('telefono', 'Teléfono', ['class' => 'control-label']) !!}
			            <div class="input-group m-b">
			            	<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
			          		{!! Form::text('telefono', $usuario->telefono, ['id'=> 'telefono','class' => 'form-control', 'placeholder' => '(81) 1234-5678', 'data-mask' => '(99) 9999-9999']) !!}
			            </div>
			            <span class="help-block">{{ $errors->first('telefono')}}</span>
			        </div>
		            <div class="form-group col-sm-6 @if ($errors->first('celular') !== "") has-error @endif">
			            {!! Form::label('celular', 'Celular', ['class' => 'control-label']) !!}
			            <div class="input-group m-b">
			            	<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
			          		{!! Form::text('celular', $usuario->celular, ['id'=> 'celular','class' => 'form-control', 'placeholder' => '(812) 345-6789', 'data-mask' => '(999) 999-9999']) !!}
			            </div>
			            <span class="help-block">{{ $errors->first('celular')}}</span>
			        </div>
			    </div>
			    <div class="row text-right">
			    	<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar Cambios</button>
			    </div>

			    {!! Form::close() !!}
				
			    <div class="row">
			    <hr />
				    <div class="col-xs-6">
						<small class="text-muted">Creado: <br />{{ $usuario->created_at }}</small>
				    </div>
				    <div class="col-xs-6 text-right">
						<small class="text-muted">Última actualización: <br /> {{ $usuario->updated_at }} </small>
				    </div>
			    </div>
		    </div>
		</div>				
	</div>
</div>
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">	

	@if(session('perfil_update_success'))
		swal('Correcto', "Se han guardado los cambios satisfactoriamente.", "success");
	@endif

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 2,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});
</script>
@endsection