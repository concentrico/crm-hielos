<!DOCTYPE html>
<html>
    <head>
        <title>Error 404</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: white;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
                color: #333;
            }

            h2{color: #2e368e}
        </style>
    </head>
    <body>
        <div class="container" style="width: 100%!important;">
            <div class="content">
                <div class="col-sm-12 text-center">
                    @if(getSystemSettings()->site_logo != null) 
                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="100">
                    @else 
                        <img src="{{ asset('img/logo-default.png') }}" alt="Logo" height="100"> 
                    @endif
                </div>
                <h2>Página no encontrada.</h2>
                <BR/>
                <p>
                    <a href="{{ url('/') }}" class="btn btn-block btn-primary">Regresar a Home</a>
                </p>
            </div>
        </div>
    </body>
</html>