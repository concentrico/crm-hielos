@extends('sistema.layouts.master')

@section('title', 'Pedido #'.$pedido->id)

@section('attr_navlink_9', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/slick/slick.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/slick/slick-theme.css') }}" rel="stylesheet" />
<style type="text/css" media="screen">
.i-checks{
	line-height: 32px;
	display: inline-block;
}
.selectdias {
	width: 50%!important;
	margin-left: 10%;
	display: inline-block;
}
.table > tbody > tr > td{
    border: 0px!important;
}
.btn:active:focus, .btn:focus {
    outline: none!important;
}
</style>
@endsection

@section('breadcrumb_title', 'Pedido #'.$pedido->id)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('pedidos') }}">Pedidos</a></li>
<li class="active"><strong>Pedido #{{ $pedido->id }}</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
	            
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
                		<i style="margin:0px;" class="fa fa-file-text fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Detalles</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 2) active @endif">
                	<a data-toggle="tab" href="#tab-2">
                		<i style="margin:0px;" class="fa fa-money fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Cobranza<span> 
	                </a>
	            </li>  
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">

                    	<div class="row">
							<div class="col-sm-12 col-md-4 col-lg-4" style="padding: 0 5px;">
								@include('sistema.pedidos.iboxes.info')
								@include('sistema.pedidos.iboxes.direccion')
							</div>
							<div class="col-sm-12 col-md-8 col-lg-8" style="padding: 0 5px;">
								<div class="row">
									<div class="col-sm-12">
										@include('sistema.pedidos.iboxes.productos')
										@include('sistema.pedidos.iboxes.entrega')
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
                
                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.pedidos.iboxes.show.pagos')
								@include('sistema.pedidos.iboxes.show.facturas')
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('sistema.pedidos.modals.status')
@include('sistema.pedidos.modals.edit')
@include('sistema.pedidos.modals.add_producto')
@include('sistema.pedidos.modals.add_cortesia')
@include('sistema.pedidos.modals.edit_producto')
@include('sistema.pedidos.modals.direccion')
@include('sistema.pedidos.modals.email')
@include('sistema.pedidos.modals.pagos.create')
@include('sistema.pedidos.modals.pagos.edit')
@include('sistema.pedidos.modals.pagos.delete')
@include('sistema.pedidos.modals.facturas.create')
@include('sistema.pedidos.modals.facturas.edit')
@include('sistema.pedidos.modals.facturas.delete')
@include('sistema.pedidos.modals.status_cobranza')
@include('sistema.pedidos.modals.status_facturacion')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/slick/slick.min.js') }}"></script>
<script src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>	
<script type="text/javascript">	

	@if(session('show_tab_cobranza'))
		$('.nav-tabs a[href="#tab-2"]').tab('show');
	@endif

	@if(session('update_pedido_success'))
		toastr.success("Pedido actualizado satisfactoriamente.");
	@endif

	@if(session('send_email_success'))
		toastr.success("Se ha enviado la remisión satisfactoriamente.");
	@endif

	$(document).on('change', '.btn-file :file', function() {
	    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready( function() {
	    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
			
			if( label != ""){
				this.nextSibling.innerHTML = label;		
			}else{
				this.nextSibling.innerHTML = 'Selecciona un archivo';				
			}
			
	        console.log(numFiles);
	        console.log(label);
	    });
	});
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	// PRODUCTOS PEDIDO (DESDE INVENTARIO) NO SE PUEDEN AGREGAR DOBLE
	var productos_pedido_evento = [];

	@foreach($pedido->productos as $producto)
		productos_pedido_evento.push({{$producto->producto_id}});
		$("#producto_agregar option[value='{{$producto->producto_id}}']").remove();
		$('#producto_agregar').dropdown("set text", '');
        $('#producto_agregar').dropdown('refresh');

        @if($producto->precio_unitario == 0.00)
        	$("#cortesia_agregar option[value='{{$producto->producto_id}}']").remove();
			$('#cortesia_agregar').dropdown("set text", '');
        	$('#cortesia_agregar').dropdown('refresh');
        @endif
	@endforeach

	function addProductoPedido() {
		var producto_id = $("#producto_agregar").val();
		if( producto_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/productos')}}',
	            data:{ 'type': '1', 'producto_id': producto_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {

	            		var precio = data['precio_venta'];

						$response='<tr>';
                        $response+='<td><a class="text-navy">'+data['nombre']+'</a><input type="hidden" name="producto_pedido[]" value="'+producto_id+'"/></td>';

                        $response+='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('+producto_id+')" name="precio_pedido[]" id="precio_producto'+producto_id+'" type="text" placeholder="0.00" value="'+precio+'"></div></td>';

                        $response+='<td><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto'+producto_id+'" onblur="calcularSubtotalProducto('+producto_id+')" onchange="calcularSubtotalProducto('+producto_id+')" value="1"/></div></td>';

                        $response+='<td><h4 id="subtotal_producto'+producto_id+'">'+numeral(precio.toString().replace(',','.')).format('$0,0.00')+'</h4></td>';

                        $response+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

                        $response+='</tr>';

                        productos_pedido_evento.push(producto_id);

                        $("#producto_agregar option[value='"+producto_id+"']").remove();
						$('#producto_agregar').dropdown("set text", '');
                        $('#producto_agregar').dropdown('refresh');

                        $('#tablaProductosPedido > tbody').append($response);

                        $("#cantidad_producto"+producto_id).TouchSpin({
					        min: 0,
					        max: 1000000000,
					        step: 1,
					        decimals: 0,
					        boostat: 5,
					        maxboostedstep: 10,
					        buttondown_class: 'btn btn-white',
					        buttonup_class: 'btn btn-white'
					    });

					    toastr.success("Producto agregado al Pedido.");

					calcularTotalPedido();
	            }
	        });

	        $('#modalAgregarProducto').modal('hide');
		} else {
			toastr.error("Seleccione un producto.");
		}
	}

	function cancelarPedido() {
		swal({
		  title: "¿Desea cancelar el pedido?",
		  text: "Ésto registrará una devolución de inventario en caso de ser necesario.",
		  type: "warning",
		  showCancelButton: true,
		  html: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/pedidos')}}',
	            data:{ 'type': '1', 'pedido_id': '{{$pedido->id}}', '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	swal("Correcto!", "Se ha cancelado el pedido satisfactoriamente.", "success");

	            	setTimeout(function(){
					    location.reload();
					}, 2000);
	            }
	        });
		  
		});
	}

	function realizarDevolucion() {
		swal({
		  title: "¿Desea marcar la Devolución el pedido?",
		  text: "Ésto marcará una entrada tipo devolución y dará después baja como merma.",
		  type: "warning",
		  showCancelButton: true,
		  html: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/pedidos')}}',
	            data:{ 'type': '3', 'pedido_id': '{{$pedido->id}}', '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	swal("Correcto!", "Se ha marcado la devolución del pedido satisfactoriamente.", "success");

	            	setTimeout(function(){
					    location.reload();
					}, 2000);
	            }
	        });
		  
		});
	}

	

	function eliminarProductoPedido(producto_pedido_id) {
		swal({
		  title: "¿Desea eliminar el producto del pedido?",
		  text: "Se recalcularán los totales del pedido.",
		  type: "warning",
		  showCancelButton: true,
		  html: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/pedidos')}}',
	            data:{ 'type': '2', 'pedido_id': '{{$pedido->id}}', 'producto_pedido_id': producto_pedido_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	swal("Correcto!", "Se ha eliminado el producto del pedido satisfactoriamente.", "success");

	            	setTimeout(function(){
					    location.reload();
					}, 2000);
	            }
	        });
		  
		});
	}

	

	$('#evento_municipio').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '1', busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

	$('#evento_codigo_postal').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '2', municipio: $('#evento_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });
		
	$('#evento_colonia').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '3', codigo_postal: $('#evento_codigo_postal').val(), municipio: $('#evento_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

	function setColonia(colonia) {

		if (colonia != '') {
			$.ajax({
	            type: 'GET',
	            url: '{{url('ajax/autocomplete-codigospostales')}}',
	            data:{ 'type': '4', 'colonia': colonia, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	if (data.length > 1) {

	            		html='';
	            		html2='';
					    $.each(data, function(key, value) {
				            html += "<option value=" + value['municipio']  + ">" +value['municipio'] + "</option>"
				            html2 += "<option value=" + value['codigo_postal']  + ">" +value['codigo_postal'] + "</option>"
				        });

	            		$("#evento_municipio")
						    .replaceWith('<select id="evento_municipio" name="evento_municipio" class="form-control">'+html+'</select>');
						$("#evento_municipio").dropdown();

	            		$("#evento_codigo_postal")
						    .replaceWith('<select id="evento_codigo_postal" name="evento_codigo_postal" class="form-control">'+html2+'</select>');
						$("#evento_codigo_postal").dropdown();

	            	} else {

	            		if(!$("#evento_municipio").is("select")) {
						    // no es un select
						    $('#evento_municipio').val(data[0]['municipio']);
	            			$('#evento_codigo_postal').val(data[0]['codigo_postal']);
						} else {
							// Es un select
							$('#evento_codigo_postal').destroyDropdown();
							$('#evento_municipio').destroyDropdown();

							$("#evento_municipio")
						    .replaceWith('<input type="text" id="evento_municipio" name="evento_municipio" class="form-control" value="'+data[0]['municipio']+'"/>');

							$("#evento_codigo_postal")
						    .replaceWith('<input type="text" id="evento_codigo_postal" name="evento_codigo_postal" class="form-control positive-integer" value="'+data[0]['codigo_postal']+'" data-mask="99999" maxlength="5" />');

						}
	            		
	            	}
	            	
	            }
	        });
		} else {

			if($("#evento_municipio").is("select")) {

				// Es un select
				$('#evento_codigo_postal').destroyDropdown();
				$('#evento_municipio').destroyDropdown();

				$("#evento_municipio")
			    .replaceWith('<input type="text" id="evento_municipio" name="evento_municipio" class="form-control" value=""/>');

				$("#evento_codigo_postal")
			    .replaceWith('<input type="text" id="evento_codigo_postal" name="evento_codigo_postal" class="form-control positive-integer" value="" data-mask="99999" maxlength="5" />');

			}

		}
	
	}

	$.fn.destroyDropdown = function() {
		return $(this).each(function() {
			$(this).parent().dropdown( 'destroy' ).replaceWith( $(this) );
		});
	};

	var productos_pedido = [];

	@foreach($pedido->productos as $producto)
		
		productos_pedido[{{ $producto->id }}] = {
			producto_id : '{{ $producto->producto_id }}',
			producto_nombre : '{{ $producto->producto->nombre }}',
			precio : '{{ $producto->precio_unitario }}',
			cantidad_requerida : '{{ $producto->cantidad_requerida }}',
			cantidad_recibida : '{{ $producto->cantidad_recibida }}',
			iva : '{{ $producto->producto->iva }}',
			subtotal : '{{ $producto->subtotal }}',
		}

	@endforeach

	var pagos_pedido = [];

	@foreach($pedido->pagos as $pago)
		
		pagos_pedido[{{ $pago->id }}] = {
			forma_pago : '{{ $pago->forma_pago_id }}',
			fecha_pago : "{{ Carbon::parse($pago->fecha_pago)->format('d/m/Y') }}",
			monto : '{{ $pago->monto }}',
			comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $pago->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
		}

	@endforeach

	var facturas = [];

	@foreach($pedido->facturas as $factura)
		
		facturas[{{ $factura->id }}] = {
			folio : '{{ $factura->folio }}',
			fecha_facturacion : "{{ Carbon::parse($factura->fecha_facturacion)->format('d/m/Y') }}",
			subtotal : '{{ $factura->subtotal }}',
			iva : '{{ $factura->iva }}',
			total : '{{ $factura->total }}',
			comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $factura->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
		}

	@endforeach

	Dropzone.options.myDropzone = {
		acceptedFiles: '.jpeg, .jpg, .png, .gif, .svg, .bmp, .xls, .xlsx, .doc, .docx, .pdf, .ppt, .pptx',
		complete: function(file) {
			
 			this.removeFile(file);

		},
		success: function(file, response){

			var pathArray = location.href.split( '/' );
			var protocol = pathArray[0];
			var host = pathArray[2];
			var base_url = protocol + '//' + host;

			var aux_tr = '<tr><td><input type="checkbox" name="archivo[]" value="' + response.id + '"/></td><td><a target="_blank" href="' + base_url + '/' + encodeURIComponent(response.path) + '"><i class="fa fa-file"></i> ' + response.original_filename + '</a><br /><div class="row"><div class="col-sm-12"><small class="text-muted"><a href="' + base_url + '/' + encodeURIComponent(response.path) + '">' + response.creado_por.nombre + '</a>&nbsp;&nbsp;|&nbsp;&nbsp;' + response.created_at + '</small></div></div></td></tr>';			
														
			$('#contenedor_archivos').show();
			$('#contenedor_archivos').append(aux_tr);

			console.log(response);
			archivos[response.id] = { 
				original_filename: response.original_filename,
				descripcion:'',
				path: encodeURIComponent(response.path) 
			};
		}
	};

</script>

@endsection