<div class="modal inmodal" id="modalProductoPedidoAgregar" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-refresh modal-icon"></i><BR/>Agregar Producto</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('pedidos/' . $pedido->id . '/producto' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-3 @if ($errors->first('producto_agregar') !== "") has-error @endif">
				            {!! Form::label('producto_agregar', 'Producto', ['class' => 'control-label']) !!}
					        {!! Form::select('producto_agregar', \App\Models\Producto::all()->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['id' => 'producto_agregar','class' => 'col-xs-12 selectdp'] ) !!}
				            <span class="help-block">{{ $errors->first('producto_agregar')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('precio_unitario') !== "") has-error @endif">
				            {!! Form::label('precio_unitario', 'Precio Unitario', ['class' => 'control-label']) !!}
			            	<div class="input-group" style="width: 100%;">
			            		<span class="input-group-addon">$</span>
			            		{!! Form::text('precio_unitario', null, ['class' => 'form-control decimal-2-places']) !!}	
			            	</div>
				            <span class="help-block">{{ $errors->first('precio_unitario')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('cantidad_requerida') !== "") has-error @endif">
				            {!! Form::label('cantidad_requerida', 'Cantidad Requerida', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
					           	<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_requerida" />
							</div>
				            <span class="help-block">{{ $errors->first('cantidad_requerida')}}</span>
				        </div>
				        <div class="form-group col-sm-3 @if ($errors->first('cantidad_recibida') !== "") has-error @endif">
				            {!! Form::label('cantidad_recibida', 'Cantidad Recibida', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
				            	@if($pedido->status > 2)
					           		<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_recibida" />
					           	@else
					           		<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_recibida" disabled="" />
					           	@endif
							</div>
				            <span class="help-block">{{ $errors->first('cantidad_recibida')}}</span>
				        </div>
		            </div>
		            @if ($errors->first('addProductoFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('addProductoFailed')!!}
				        </div>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_producto_pedido_agregar'))
		$('#modalProductoPedidoAgregar').modal('show');
	@endif

</script>
@endsection