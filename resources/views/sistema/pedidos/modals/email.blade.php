<div class="modal inmodal" id="modalEnviarEmail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-send modal-icon"></i><BR/>Enviar Remisión por Email</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('pedidos/' . $pedido->id . '/sendEmail' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('email_to') !== "") has-error @endif">
				            {!! Form::label('email_to', 'Correo electrónico', ['class' => 'control-label']) !!}
			            	<div class="input-group" style="width: 100%;">
			            		<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
			            		{!! Form::text('email_to', null, ['class' => 'form-control']) !!}	
			            	</div>
				            <span class="help-block">{{ $errors->first('email_to')}}</span>
				        </div>
			            <div class="form-group col-sm-12 @if ($errors->first('email_cc') !== "") has-error @endif">
				            {!! Form::label('email_cc', 'CC', ['class' => 'control-label']) !!}
			            	<div class="input-group" style="width: 100%;">
			            		<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
			            		{!! Form::text('email_cc', null, ['class' => 'form-control']) !!}	
			            	</div>
				            <span class="help-block">{{ $errors->first('email_cc')}}</span>
				        </div>
		            </div>
		            @if ($errors->first('sendEmailFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('sendEmailFailed')!!}
				        </div>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Enviar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_send_email'))
		$('#modalEnviarEmail').modal('show');
	@endif

</script>
@endsection