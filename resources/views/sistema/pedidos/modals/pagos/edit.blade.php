<div class="modal inmodal" id="modalPedidoPagoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-money modal-icon"></i><BR/>Editar Pago</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id . '/pago' ), 'enctype' => 'multipart/form-data'] ) !!}
				<div class="modal-body">
					<h4 class="text-navy">Pedido #{{ $pedido->id }}</h4>
					<hr />
		            <div class="row">
			            <div class="form-group col-sm-3 @if ($errors->first('fecha_pago_pedido_edit') !== "") has-error @endif">
				            {!! Form::label('fecha_pago_pedido_edit', 'Fecha', ['class' => 'control-label']) !!}
	                        <div class="input-group date">
	                        	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								{!! Form::text('fecha_pago_pedido_edit', null, ['id' => 'fecha_entrega', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
	                        </div>
				            <span class="help-block">{{ $errors->first('fecha_pago_pedido_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('forma_pago_pedido_edit') !== "") has-error @endif">
				            {!! Form::label('forma_pago_pedido_edit', 'Forma de Pago', ['class' => 'control-label']) !!}
			            	{!! Form::select('forma_pago_pedido_edit', App\Models\FormaPago::all()->pluck('nombre', 'id')->toArray(), null, 
			            	['class' => 'form-control', 'placeholder' => 'Forma de Pago'] ) !!}
				            <span class="help-block">{{ $errors->first('forma_pago_pedido_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('monto_pago_pedido_edit') !== "") has-error @endif">
				            {!! Form::label('monto_pago_pedido_edit', 'Monto', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
					           	<span class="input-group-addon">$</span>
					           	{!! Form::text('monto_pago_pedido_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
							</div>
				            <span class="help-block">{{ $errors->first('monto_pago_pedido_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('comprobante_pago_pedido_edit') !== "") has-error @endif">
				            {!! Form::label('comprobante_pago_pedido_edit', 'Comprobante', ['class' => 'control-label']) !!}<br class="visible-xs-block"/>
							<span class="btn btn-primary btn-file col-xs-12" >
								<input type="file" accept=".pdf, .xls, .xlsx, .doc, .docx, .png, .ppt" name="comprobante_pago_pedido_edit" style="z-index: 100;"><label class="col-xs-12" style="margin-bottom: 0px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">Comprobante</label>
							</span>
				            <span class="help-block">{{ $errors->first('comprobante_pago_pedido_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-12 @if ($errors->first('comentarios_pago_pedido_edit') !== "") has-error @endif">
				            {!! Form::label('comentarios_pago_pedido_edit', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('comentarios_pago_pedido_edit', null, ['class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            <span class="help-block">{{ $errors->first('comentarios_pago_pedido_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_evento_pago_orden_compra_cliente_edit'))
		$('#modalPedidoPagoEdit').find('form').attr('action', "{{ url('pedidos/' . $pedido->id .'/pago' . session('id_pago_pedido_edit')) }}")
		$('#modalPedidoPagoEdit').modal('show');
	@endif

	function showModalPedidoPagoEdit(pago_id){
		
		var aux_form_action = "{{ url('pedidos/' . $pedido->id . '/pago' ) }}/" + pago_id;
		
		if ( $('#modalPedidoPagoEdit').find('form').attr('action') == aux_form_action){
			
		}else{
			$('#modalPedidoPagoEdit').find('form').attr('action', aux_form_action)


			$('#modalPedidoPagoEdit').find('input[name=fecha_pago_pedido_edit]').val(pagos_pedido[pago_id]['fecha_pago']);
			$('#modalPedidoPagoEdit').find('select[name=forma_pago_pedido_edit]').dropdown("set selected", pagos_pedido[pago_id]['forma_pago']);
			$('#modalPedidoPagoEdit').find('input[name=monto_pago_pedido_edit]').val(pagos_pedido[pago_id]['monto']);
			$('#modalPedidoPagoEdit').find('textarea[name=comentarios_pago_pedido_edit]').html(pagos_pedido[pago_id]['comentarios']).text();


		}

		$('#modalPedidoPagoEdit').modal('show');
	}

</script>
@endsection