<div class="modal inmodal" id="modalPedidoFacturaEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-legal modal-icon"></i> Editar Factura</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id . '/factura') , 'enctype' => 'multipart/form-data'] ) !!}
				<div class="modal-body">
					<h4 class="text-navy">Pedido #{{ $pedido->id }}</h4>
					<hr />
		            <div class="row">

		            	<div class="form-group col-sm-4">
				            {!! Form::label('cliente', 'Cliente', ['class' => 'control-label']) !!}
							{!! Form::text('cliente', $pedido->cliente->nombre_corto, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
				            <span class="help-block">{{ $errors->first('cliente')}}</span>
				        </div>

			            <div class="form-group col-sm-4 @if ($errors->first('fecha_facturacion_edit') !== "") has-error @endif">
				            {!! Form::label('fecha_facturacion_edit', 'Fecha Facturación', ['class' => 'control-label']) !!}
	                        <div class="input-group date">
	                        	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								{!! Form::text('fecha_facturacion_edit', null, ['id' => 'fecha_pago_pedido', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
	                        </div>
				            <span class="help-block">{{ $errors->first('fecha_facturacion_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('folio_edit') !== "") has-error @endif">
				            {!! Form::label('folio_edit', 'Folio', ['class' => 'control-label']) !!}
							{!! Form::text('folio_edit', null, ['class' => 'form-control', 'placeholder' => 'Remisión']) !!}
				            <span class="help-block">{{ $errors->first('folio_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('subtotal_factura_edit') !== "") has-error @endif">
				            {!! Form::label('subtotal_factura_edit', 'Subtotal', ['class' => 'control-label']) !!}
				            {!! Form::text('subtotal_factura_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
				            <span class="help-block">{{ $errors->first('subtotal_factura_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('iva_factura_edit') !== "") has-error @endif">
				            {!! Form::label('iva_factura_edit', 'IVA', ['class' => 'control-label']) !!}
				            {!! Form::text('iva_factura_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
				            <span class="help-block">{{ $errors->first('iva_factura_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('total_factura_edit') !== "") has-error @endif">
				            {!! Form::label('total_factura_edit', 'Total', ['class' => 'control-label']) !!}
				            {!! Form::text('total_factura_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
				            <span class="help-block">{{ $errors->first('total_factura_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('pdf_factura_edit') !== "") has-error @endif">
				            {!! Form::label('pdf_factura_edit', 'PDF', ['class' => 'control-label']) !!}<br class="visible-xs-block"/>
							<span class="btn btn-primary btn-file col-xs-12" >
								<input type="file" accept=".pdf" name="pdf_factura_edit" style="z-index: 100;"><label class="col-xs-12" style="margin-bottom: 0px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">PDF</label>
							</span>
				            <span class="help-block">{{ $errors->first('pdf_factura_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('xml_factura_edit') !== "") has-error @endif">
				            {!! Form::label('xml_factura_edit', 'XML', ['class' => 'control-label']) !!}<br class="visible-xs-block"/>
							<span class="btn btn-primary btn-file col-xs-12" >
								<input type="file" accept=".xml" name="xml_factura_edit" style="z-index: 100;"><label class="col-xs-12" style="margin-bottom: 0px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">XML</label>
							</span>
				            <span class="help-block">{{ $errors->first('xml_factura_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-12 @if ($errors->first('comentarios_factura_edit') !== "") has-error @endif">
				            {!! Form::label('comentarios_factura_edit', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('comentarios_factura_edit', null, ['class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            <span class="help-block">{{ $errors->first('comentarios_factura_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_pedido_factura_edit'))
		$('#modalPedidoFacturaEdit').find('form').attr('action', "{{ url('pedidos/' . $pedido->id . '/factura/' . session('factura_id') ) }}")
		$('#modalPedidoFacturaEdit').modal('show');
	@endif

	function showModalPedidoFacturaEdit(id){
		
		if ( $('#modalPedidoFacturaEdit').find('form').attr('action') == "{{ url('pedidos/' . $pedido->id . '/factura' ) }}/" + id){
			
		}else{
			$('#modalPedidoFacturaEdit').find('form').attr('action', "{{ url('pedidos/' . $pedido->id . '/factura' ) }}/" + id)

			$('#modalPedidoFacturaEdit').find('input[name=fecha_facturacion_edit]').val(facturas[id]['fecha_facturacion']);
			$('#modalPedidoFacturaEdit').find('input[name=folio_edit]').val(facturas[id]['folio']);
			$('#modalPedidoFacturaEdit').find('input[name=subtotal_factura_edit]').val(facturas[id]['subtotal']);
			$('#modalPedidoFacturaEdit').find('input[name=iva_factura_edit]').val(facturas[id]['iva']);
			$('#modalPedidoFacturaEdit').find('input[name=total_factura_edit]').val(facturas[id]['total']);
			$('#modalPedidoFacturaEdit').find('textarea[name=comentarios_factura_edit]').html(facturas[id]['comentarios']).text();
		}

		$('#modalPedidoFacturaEdit').modal('show');
	}

</script>
@endsection