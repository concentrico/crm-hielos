<div class="modal inmodal" id="modalCambiarStatus" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-refresh modal-icon"></i> <BR/>Cambiar Status</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id . '/status' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3">
						{!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
				           @php
				           	switch ($pedido->status) {
				           	case '1': // Programado
				           	$status_array = array('1' => 'Programado', '2' => 'En Camino', '3' => 'Entregado', '5' => 'Cancelado');
				           	break;
				           	case '2': // En camino
				           	$status_array = array('1' => 'Programado', '2' => 'En Camino', '3' => 'Entregado', '5' => 'Cancelado');
				           	break;
				           	case '3': // Entregado
				           	$status_array = array('3' => 'Entregado', '4' => 'Devolución');
				           	break;
				           	case '4': // Devolución
				           	$status_array = array('4' => 'Devolución');
				           	break;
				           	case '5': // Cancelado
				           	$status_array = array('1' => 'Programado','5' => 'Cancelado');
				           	break;
				           	default:
				           	$status_array = array('' => '');
				           	break;
				           	}
				           @endphp
							{!! Form::select('status', $status_array, $pedido->status, ['class' => 'form-control selectdp'] ) !!}
				           <span class="help-block">{{ $errors->first('status')}}</span>
				        </div>
		            </div>
		            @if ($errors->first('statusFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('statusFailed')!!}
				        </div>
					@endif
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="button" class="btn btn-success" type="submit"><i class="fa fa-save"></i> Cambiar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cambiar_status'))
		$('#modalCambiarStatus').modal('show');
	@endif
	
</script>	
@endsection