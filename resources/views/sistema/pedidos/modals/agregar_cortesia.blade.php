<div class="modal inmodal" id="modalAgregarCortesia" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-gift modal-icon"></i> <BR/>Agregar Cortesía</h4>
			</div>
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3">
				            {!! Form::label('producto_cortesia', 'Seleccione Producto', ['class' => 'control-label text-center']) !!}
			            	{!! Form::select('producto_cortesia', \App\Models\Producto::all()->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['id' => 'producto_cortesia','class' => 'col-xs-12 selectdp'] ) !!}
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="button" class="btn btn-success" onclick="addCortesiaPedido()"><i class="fa fa-save"></i> Agregar</button>
				</div>
		</div>
	</div>
</div>