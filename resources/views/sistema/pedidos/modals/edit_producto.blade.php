<div class="modal inmodal" id="modalProductoPedidoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-edit modal-icon"></i><BR/>Editar Producto</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id . '/producto' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-3">
				            {!! Form::label('producto', 'Producto', ['class' => 'control-label']) !!}
					        {!! Form::text('producto', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
				            <span class="help-block">{{ $errors->first('producto')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('precio_unitario_edit') !== "") has-error @endif">
				            {!! Form::label('precio_unitario_edit', 'Precio Unitario', ['class' => 'control-label']) !!}
			            	<div class="input-group" style="width: 100%;">
			            		<span class="input-group-addon">$</span>
			            		{!! Form::text('precio_unitario_edit', null, ['class' => 'form-control decimal-2-places']) !!}	
			            	</div>
				            <span class="help-block">{{ $errors->first('precio_unitario_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('cantidad_requerida_edit') !== "") has-error @endif">
				            {!! Form::label('cantidad_requerida_edit', 'Cantidad Requerida', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
					           	<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_requerida_edit" />
							</div>
				            <span class="help-block">{{ $errors->first('cantidad_requerida_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-3 @if ($errors->first('cantidad_recibida_edit') !== "") has-error @endif">
				            {!! Form::label('cantidad_recibida_edit', 'Cantidad Recibida', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
				            	@if($pedido->status > 2)
					           		<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_recibida_edit" />
					           	@else
					           		<input class="touchspin_cantidad positive-integer" type="text" name="cantidad_recibida_edit" disabled="" />
					           	@endif
							</div>
				            <span class="help-block">{{ $errors->first('cantidad_recibida_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_producto_pedido_edit'))
		$('#modalProductoPedidoEdit').find('form').attr('action', "{{ url('pedidos/' . $pedido->id .'/producto' . session('producto_pedido_id')) }}")
		$('#modalProductoPedidoEdit').modal('show');
	@endif

	function showModalProductoPedidoEdit(producto_id){
		
		var aux_form_action = "{{ url('pedidos/' . $pedido->id . '/producto' ) }}/" + producto_id;
		
		if ( $('#modalProductoPedidoEdit').find('form').attr('action') == aux_form_action){
			
		}else{
			$('#modalProductoPedidoEdit').find('form').attr('action', aux_form_action)

			$('#modalProductoPedidoEdit').find('input[name=producto]').val(productos_pedido[producto_id]['producto_nombre']);
			$('#modalProductoPedidoEdit').find('input[name=precio_unitario_edit]').val(productos_pedido[producto_id]['precio']);
			$('#modalProductoPedidoEdit').find('input[name=cantidad_requerida_edit]').val(productos_pedido[producto_id]['cantidad_requerida']);
			$('#modalProductoPedidoEdit').find('input[name=cantidad_recibida_edit]').val(productos_pedido[producto_id]['cantidad_recibida']);


		}

		$('#modalProductoPedidoEdit').modal('show');
	}

</script>
@endsection