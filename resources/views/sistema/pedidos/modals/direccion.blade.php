<div class="modal inmodal" id="modalPedidoDireccion" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<i class="fa fa-map-marker modal-icon"></i>
				<h4 class="modal-title">Editar Dirección</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id . '/direccion' )] ) !!}
				<div class="modal-body">

		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('evento_calle') !== "") has-error @endif">
				            {!! Form::label('evento_calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_calle', $pedido->evento_calle, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('evento_calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('evento_numero_exterior') !== "") has-error @endif">
				            {!! Form::label('evento_numero_exterior', 'No. Exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_numero_exterior', $pedido->evento_numero_exterior, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('evento_numero_exterior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('evento_numero_interior') !== "") has-error @endif">
				            {!! Form::label('evento_numero_interior', 'No. Interior', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_numero_interior', $pedido->evento_numero_interior, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('evento_numero_interior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('evento_colonia') !== "") has-error @endif">
				            {!! Form::label('evento_colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_colonia', $pedido->evento_colonia, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('evento_colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('evento_codigo_postal') !== "") has-error @endif">
				            {!! Form::label('evento_codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_codigo_postal', $pedido->evento_codigo_postal, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('evento_codigo_postal')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('evento_municipio') !== "") has-error @endif">
				            {!! Form::label('evento_municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('evento_municipio', $pedido->evento_municipio, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('evento_municipio')}}</span>
				        </div>
		            </div>
		            <div class="row">
		                <div class="form-group col-sm-12 @if ($errors->first('evento_direccion_indicaciones') !== "") has-error @endif">
		                    {!! Form::label('evento_direccion_indicaciones', 'Indicaciones para el Chofer', ['class' => 'control-label']) !!}
	                        <div class="input-group" style="width: 100%;">
	                            <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
	                            {!! Form::textarea('evento_direccion_indicaciones', $pedido->evento_direccion_indicaciones, ['id' => 'evento_direccion_indicaciones',  'class' => 'form-control', 'placeholder' => 'Indicaciones de Ubicación', 'rows' => 3, 'style' => 'resize:none; max-width:100%;']) !!}
	                        </div>
	                        <span class="help-block">{{ $errors->first('evento_direccion_indicaciones')}}</span>
		                </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar cambios</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_pedido_direccion'))
		$('#modalPedidoDireccion').modal('show');
	@endif
</script>
@endsection