<div class="modal inmodal" id="modalPedidoInfoEdit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title "><i class="fa fa-clipboard modal-icon"></i> Editar Pedido</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('pedidos/' . $pedido->id)] ) !!}
				<div class="modal-body">
					@if(session('errores_pedido_edit') != null)
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								{{ session('errores_pedido_edit') }}
							</div>
						</div>
					</div>
					@endif
		            <div class="row">
			            <div class="form-group col-sm-12 col-md-6">
			            	{!! Form::label('cliente', 'Cliente', ['class' => 'control-label']) !!}
					        @if($pedido->cliente != null)
								<span class="form-control" disabled>{{ $pedido->cliente->nombre_corto }}</span>
							@else
								<span class="form-control" disabled>N.A.</span>
							@endif
				        </div>
			            <div class="form-group col-sm-12 col-md-6">
			            	{!! Form::label('establecimiento', 'Establecimiento', ['class' => 'control-label']) !!}
					        @if($pedido->establecimiento != null)
								<span class="form-control" disabled>{{ $pedido->establecimiento->sucursal }}</span>
							@else
								<span class="form-control" disabled>N.A.</span>
							@endif
				        </div>
		            </div>

		            <div class="row">
			            <div class="form-group col-sm-8 col-md-3 @if ($errors->first('fecha_entrega') !== "" || $errors->first('fecha_entrega')) has-error @endif">
				            {!! Form::label('fecha_entrega', 'Fecha de Entrega', ['class' => 'control-label']) !!}
	                        <div class="input-group date">
	                        	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                        	{!! Form::text('fecha_entrega', Carbon::parse($pedido->fecha_entrega)->format('d/m/Y'), ['id' => 'fecha_entrega', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
	                        </div>
				            <span class="help-block">{{ $errors->first('fecha_entrega')}}</span>
				        </div>
			            <div class="form-group col-sm-4 col-md-3 @if ($errors->first('es_evento') !== "" || $errors->first('es_evento')) has-error @endif">
				            {!! Form::label('hora_deseada', 'Hora Deseada', ['class' => 'control-label']) !!}
                            <div class="input-group clockpicker">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                {!! Form::text('hora_deseada', $pedido->hora_deseada, ['class' => 'form-control', 'placeholder' => 'Hora Deseada']) !!}
                            </div>
                            <span class="help-block">{{ $errors->first('hora_deseada')}}</span>
				        </div>
			            <div class="form-group col-sm-8 col-md-3 @if ($errors->first('es_evento') !== "" || $errors->first('es_evento')) has-error @endif">
				            {!! Form::label('es_evento', '¿Es Evento?', ['class' => 'control-label hidden-xs']) !!}
								{!! Form::select('es_evento', ['No','Si'], $pedido->es_evento, ['class' => 'form-control selectdp', 'onchange' => 'esEventoChange(this.value)'] ) !!}
				            <span class="help-block">{{ $errors->first('es_evento')}}</span>
				        </div>
			            <div class="form-group col-sm-4 col-md-3 @if ($errors->first('chofer_asignado_edit') !== "" || $errors->first('chofer_asignado_edit')) has-error @endif">
				            {!! Form::label('chofer_asignado_edit', 'Chofer Asignado', ['class' => 'control-label hidden-xs']) !!}
	                        @php
	                            $choferes = DB::table('users')
	                            ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
	                            ->where('user_has_role.user_role_id', '=', 4)
	                            ->where('users.deleted_at', NULL)
	                            ->get();
	                        @endphp
	                        {!! Form::select('chofer_asignado_edit', $choferes->pluck('nombre', 'id')->prepend('','')->toArray(), $pedido->chofer_id, ['class' => 'form-control selectdp'] ) !!}
	                        <span class="help-block">{{ $errors->first('chofer_asignado_edit')}}</span>
				        </div>
		            </div>

		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('comentarios_pedido') !== "") has-error @endif">
				            {!! Form::label('comentarios_pedido', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('comentarios_pedido', $pedido->comentarios, ['id' => 'comentarios_pedido', 
				            'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            <span class="help-block">{{ $errors->first('comentarios_pedido')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_pedido_edit'))
		$('#modalPedidoInfoEdit').modal('show');
	@endif
	
</script>	
@endsection