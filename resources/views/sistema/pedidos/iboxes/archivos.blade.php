<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-files-o"></i> Archivos</h5>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-xs-12 text-center">
				<a href="{{ url('pedidos/' . $pedido->id . '/remision') }}" class="text-primary"><i class="fa fa-file-pdf-o"></i> Remisión Cliente </a>
		    </div>
	    </div>
    </div>
</div>