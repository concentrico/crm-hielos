<div class="ibox">
    <div class="ibox-title">
        <h5>Dirección de Entrega</h5>
        <div class="checkbox checkbox-success pull-right" style="margin:0px;">
            <input type="checkbox" name="otra_direccion" id="otra_direccion" value="0" onclick="otraDireccion(this.checked);" disabled="">
            <label for="otra_direccion"> Otra Dirección </label>
        </div>
    </div>
    <div class="ibox-content">

        <div id="direccion_evento">
            <div class="row">
                <div class="form-group col-sm-9 @if ($errors->first('evento_calle') !== "") has-error @endif">
                    {!! Form::label('evento_calle', 'Calle', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_calle', null, ['class' => 'form-control', 'placeholder' => 'Calle', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_calle')}}</span>
                </div>
                <div class="form-group col-sm-3 @if ($errors->first('evento_no_exterior') !== "") has-error @endif">
                    {!! Form::label('evento_no_exterior', 'Núm.', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_no_exterior', null, ['class' => 'form-control', 'placeholder' => '#', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_no_exterior')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-9 @if ($errors->first('evento_colonia') !== "") has-error @endif">
                    {!! Form::label('evento_colonia', 'Colonia', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_colonia', null, ['class' => 'form-control', 'placeholder' => 'Colonia', 'onchange' => 'setColonia(this.value)', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_colonia')}}</span>
                </div>
                <div class="form-group col-sm-3 @if ($errors->first('evento_codigo_postal') !== "") has-error @endif">
                    {!! Form::label('evento_codigo_postal', 'C.P.', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_codigo_postal', null, ['class' => 'form-control positive-integer', 'placeholder' => 'C.P.', 'data-mask' => '99999','maxlength' => '5', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_codigo_postal')}}</span>
                </div>
                
            </div>
            <div class="row">
                <div class="form-group col-sm-6 @if ($errors->first('evento_municipio') !== "") has-error @endif">
                    {!! Form::label('evento_municipio', 'Municipio', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_municipio', null, ['id'=> 'evento_municipio','class' => 'form-control', 'placeholder' => 'Municipio', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_municipio')}}</span>
                </div>
                <div class="form-group col-sm-6 @if ($errors->first('evento_estado') !== "") has-error @endif">
                    {!! Form::label('evento_estado', 'Estado', ['class' => 'control-label']) !!}
                    {!! Form::text('evento_estado', null, ['id'=> 'evento_estado','class' => 'form-control', 'placeholder' => 'Estado', 'readonly' => 'readonly']) !!}
                    <span class="help-block">{{ $errors->first('evento_estado')}}</span>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="form-group col-sm-12 @if ($errors->first('evento_direccion_indicaciones') !== "") has-error @endif">
                    {!! Form::label('evento_direccion_indicaciones', 'Indicaciones para el Chofer', ['class' => 'control-label']) !!}
                        <div class="input-group" style="width: 100%;">
                            <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
                            {!! Form::textarea('evento_direccion_indicaciones', null, ['id' => 'evento_direccion_indicaciones',  'class' => 'form-control', 'placeholder' => 'Indicaciones de Ubicación', 'rows' => 3, 'style' => 'resize:none; max-width:100%;']) !!}
                        </div>
                        <span class="help-block">{{ $errors->first('evento_direccion_indicaciones')}}</span>
                </div>
            </div>
        </div>

        
    </div>
</div>