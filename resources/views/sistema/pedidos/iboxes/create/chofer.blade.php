<div class="ibox">
    <div class="ibox-title">
        <h5>Chofer Asignado</h5>
    </div>
    <div class="ibox-content text-center">
        <div class="@if ($errors->first('chofer_asignado') !== "") has-error @endif">
            @php
                $choferes = DB::table('users')
                ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
                ->where('user_has_role.user_role_id', '=', 4)
                ->where('users.deleted_at', null)
                ->get();
            @endphp
            {!! Form::select('chofer_asignado', $choferes->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp'] ) !!}
            <span class="help-block">{{ $errors->first('chofer_asignado')}}</span>
        </div>
    </div>
</div>