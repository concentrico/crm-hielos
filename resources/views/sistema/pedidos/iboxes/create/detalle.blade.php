<div class="ibox">
<div class="ibox-title">
    <h5>Detalle del Pedido</h5>
    <button class="btn btn-danger btn-xs pull-right" type="button" id="btnAgregarCortesia" data-toggle="modal" data-target="#modalAgregarCortesia"><i class="fa fa-gift"></i>&nbsp;&nbsp;<span class="bold">Agregar Cortesía</span></button>
</div>
<div class="ibox-content">

    <div class="table-responsive">
        <table id="tablaProductosPedido" class="table shoping-cart-table">
            <thead>
                <tr>
                    <th class="col-xs-4">Producto</th>
                    <th class="col-xs-2">Precio Unitario</th>
                    <th class="col-xs-2">Cantidad</th>
                    <th class="col-xs-1">IVA</th>
                    <th class="col-xs-2">Total</th>
                    <th style="border-top:0px; vertical-align: middle;" class="center text-center col-xs-1">
                    <a class="btn btn-info btn-sm" id="btnAgregarProductos" disabled=""><i class="fa fa-plus"></i></a>
                </th>
                </tr>
            </thead>
            <tbody>

            @if(session('inventario'))
                @php
                    $total=0.00;
                    for ($i=0; $i < count(session('inventario')['producto']); $i++) {

                        $total += floatval(str_replace(',', '', session('inventario')['subtotal_producto'][$i]));

                        $response='<tr>';
                        $response.='<td><a class="text-navy">'.session('inventario')['producto'][$i]->nombre.'</a>
                            <input type="hidden" name="producto_pedido[]" value="'.session('inventario')['producto'][$i]->id.'"/></td>';

                        $response.='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('.session('inventario')['producto'][$i]->id.')" name="precio_pedido[]" id="precio_producto'.session('inventario')['producto'][$i]->id.'" type="text" placeholder="0.00" value="'.session('inventario')['precio_pedido'][$i].'"></div></td>';

                        $response.='<td class="text-center" ><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto'.session('inventario')['producto'][$i]->id.'" onblur="calcularSubtotalProducto('.session('inventario')['producto'][$i]->id.')" onchange="calcularSubtotalProducto('.session('inventario')['producto'][$i]->id.')" value="'.session('inventario')['cantidad_pedido'][$i].'"/></div></td>';

                        $iva = floatval(session('inventario')['producto'][$i]->iva*100);
                        $response.='<td class="text-center"><h4 class="ivas-pedido" id="iva_producto'.session('inventario')['producto'][$i]->id.'">'.$iva.'%</h4></td>';

                        $response.='<td class="text-center"><h4 class="subtotales-pedido" id="subtotal_producto'.session('inventario')['producto'][$i]->id.'">$'.session('inventario')['subtotal_producto'][$i].'</h4></td>';

                        $response.='<td class="center text-center">
                                <button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

                        $response.='</tr>';

                        echo $response;
                    }
                @endphp
            @endif
            </tbody>
        </table>
    </div>
    <table class="table invoice-total m-b-none">
        <tbody>
        <tr>
            <td><strong>Subtotal :</strong></td>
            <td id="subtotal_pedido">$0.00</td>
        </tr>
        <tr>
            <td><strong>IVA :</strong></td>
            <td id="iva_pedido">$0.00</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="ibox-content text-right">
    <span>
        Total
    </span>
    <h2 class="font-bold" id="total_pedido">
        @if(session('inventario'))
            ${{ number_format($total,2) }}
        @else
            $0.00
        @endif
    </h2>
</div>
</div>