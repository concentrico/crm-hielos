<div class="ibox">
                <div class="ibox-title">
                    <h5>Datos Generales</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="form-group col-sm-6 @if ($errors->first('cliente_establecimiento_cliente') !== "") has-error @endif">
                            {!! Form::label('cliente_establecimiento_cliente', 'Cliente', ['class' => 'control-label']) !!}
                            @if(session('data')['establecimiento'] != null)
                                {!! Form::select('cliente_establecimiento_cliente', \App\Models\Cliente::findOrFail($establecimiento->cliente_id)->pluck('nombre_corto', 'id')->prepend('','')->toArray(), $establecimiento->cliente_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Cliente'] ) !!}
                            @elseif(session('data')['cliente'] != null)
                                {!! Form::select('cliente_establecimiento_cliente', \App\Models\Cliente::findOrFail($cliente->id)->pluck('nombre_corto', 'id')->prepend('','')->toArray(), $cliente->id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Cliente'] ) !!}
                            @else
                                {!! Form::select('cliente_establecimiento_cliente', \App\Models\Cliente::all()->sortBy('nombre_corto')->pluck('nombre_corto', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Cliente', 'onchange' => 'cargarEstablecimientos(this.value)'] ) !!}
                            @endif
                            <span class="help-block">{{ $errors->first('cliente_establecimiento_cliente')}}</span>
                        </div>
                        <div class="form-group col-sm-6 @if ($errors->first('es_evento') !== "" || $errors->first('es_evento')) has-error @endif">
                            {!! Form::label('es_evento', '¿Es un Evento?', ['class' => 'control-label']) !!}
                            @if(session('data'))
                                @if($es_evento == '1')
                                    {!! Form::select('es_evento', ['No','Si'], 1, ['class' => 'form-control selectdp', 'onchange' => 'esEventoChange(this.value)'] ) !!}
                                @else
                                     {!! Form::select('es_evento', ['No','Si'], 0, ['class' => 'form-control selectdp', 'onchange' => 'esEventoChange(this.value)'] ) !!}
                                @endif
                            @else
                                {!! Form::select('es_evento', ['No','Si'], null, ['class' => 'form-control selectdp', 'onchange' => 'esEventoChange(this.value)'] ) !!}
                            @endif
                            <span class="help-block">{{ $errors->first('es_evento')}}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6  @if ($errors->first('cliente_establecimiento_sucursal') !== "") has-error @endif">
                            {!! Form::label('cliente_establecimiento_sucursal', 'Sucursal', ['class' => 'control-label']) !!}
                            @if(session('data')['establecimiento'] != null)
                                {!! Form::select('cliente_establecimiento_sucursal', \App\Models\ClienteEstablecimiento::findOrFail($establecimiento->id)->pluck('sucursal', 'id')->prepend('','')->toArray(), $establecimiento->id, ['class' => 'form-control selectdp', 'placeholder' => 'Sucursal','onchange' => 'cargarDatosSucursal(this.value,true)']) !!}
                            @else
                                {!! Form::select('cliente_establecimiento_sucursal', [''], null, ['class' => 'form-control selectdp', 'placeholder' => 'Sucursal','onchange' => 'cargarDatosSucursal(this.value,true)']) !!}
                            @endif
                            <span class="help-block">{{ $errors->first('cliente_establecimiento_sucursal')}}</span>
                        </div>
                        <div class="form-group col-sm-6  @if ($errors->first('chofer_asignado') !== "") has-error @endif">

                            {!! Form::label('chofer_asignado', 'Chofer Asignado', ['class' => 'control-label']) !!}
                            @php
                                $choferes = DB::table('users')
                                ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
                                ->where('user_has_role.user_role_id', '=', 4)
                                ->where('users.deleted_at', null)
                                ->get();

                            @endphp
                            {!! Form::select('chofer_asignado', $choferes->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp'] ) !!}
                            <span class="help-block">{{ $errors->first('chofer_asignado')}}</span>
                        </div>
                    </div>

                    <hr/>

                    <div class="row">
                        <div class="form-group col-sm-4 @if ($errors->first('fecha_pedido') !== "") has-error @endif">
                            {!! Form::label('fecha_pedido', 'Fecha de Petición', ['class' => 'control-label']) !!}
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('fecha_pedido', Carbon::now()->formatLocalized('%d/%m/%Y'), ['id' => 'fecha_pedido', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
                            </div>
                            <span class="help-block">{{ $errors->first('fecha_pedido')}}</span>
                        </div>
                        <div class="form-group col-sm-4 @if ($errors->first('fecha_entrega') !== "") has-error @endif">
                            {!! Form::label('fecha_entrega', 'Fecha de Entrega', ['class' => 'control-label']) !!}
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('fecha_entrega', Carbon::now()->formatLocalized('%d/%m/%Y'), ['id' => 'fecha_entrega', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
                            </div>
                            <span class="help-block">{{ $errors->first('fecha_entrega')}}</span>
                        </div>
                        <div class="form-group col-sm-4 @if ($errors->first('hora_deseada') !== "") has-error @endif">
                            {!! Form::label('hora_deseada', 'Hora Deseada', ['class' => 'control-label']) !!}
                            <div class="input-group clockpicker">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                {!! Form::text('hora_deseada', null, ['class' => 'form-control', 'placeholder' => 'Hora Deseada']) !!}
                            </div>
                            <span class="help-block">{{ $errors->first('hora_deseada')}}</span>
                        </div>
                    </div>

                    <hr/>

                    <div class="row" style="margin-bottom: 7px;">
                        <div class="col-sm-6">
                            <h4>Forma de Pago</h4>
                            {!! Form::select('forma_pago', App\Models\FormaPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Forma de Pago'] ) !!}
                        </div>
                        <div class="col-sm-6">
                            <h4>Condición de Pago</h4>
                            {!! Form::select('condicion_pago', App\Models\CondicionPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Condiciones de Pago'] ) !!}
                        </div>
                    </div>
                </div>
</div>