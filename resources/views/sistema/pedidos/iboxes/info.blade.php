<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-info-circle"></i> Info</h5>

        <div class="btn-group pull-right" style="padding: 0;">
			<button type="button" class="btn btn-outline btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0;margin:0px;">
				&nbsp;&nbsp;<i class="fa fa-edit text-success"></i>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li class="text-success">
					<a data-toggle="modal" data-target="#modalPedidoInfoEdit">
						<i class="fa fa-edit"></i> Editar
					</a>
				</li>
				<li class="text-warning">
					<a data-toggle="modal" data-target="#modalCambiarStatus"">
						<i class="fa fa-refresh"></i> Cambiar Status
					</a>
				</li>
				@if($pedido->status < 3) {{--Sólo cuando esté en Programado o En Camino--}}
					<li class="text-danger">
						<a onclick="cancelarPedido();">
							<i class="fa fa-times"></i> Cancelar
						</a>
					</li>
				@endif
				@if($pedido->status == 3) {{--Sólo cuando esté en Entregado--}}
					<li class="text-danger">
						<a onclick="realizarDevolucion();">
							<i class="fa fa-mail-reply"></i> Devolución
						</a>
					</li>
				@endif
			</ul>
		</div>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-center">
		    	@php
            		switch ($pedido->status) {
            			case '1':
            				echo '<span class="label label-primary" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-clock-o"></i> Programado</span>';
            				break;
            			case '2':
            				echo '<span class="label label-info" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-truck"></i> En Camino</span>';
            				break;
            			case '3':
            				echo '<span class="label label-success" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-calendar-check-o"></i> Entregado</span>';
            				break;
            			case '4':
            				echo '<span class="label label-warning" style="font-size: 1.3em; display:inline-block;"><i class="fa fa-check-circle"></i> Devolución</span>';
            				break;
            			case '5':
            				echo '<span class="label label-danger" style="font-size: 1.3em;  display:inline-block;"><i class="fa fa-times-circle"></i> Cancelado</span>';
            				break;
            			default:
            				echo '<span class="label label-default" style="font-size: 1.3em; display:inline-block;">N.D.</span>';
            				break;
            		}
            	@endphp
		    </div>
	    </div>
	    <hr />
	    <div class="row">
		    <div class="col-sm-12">

		    	<div class="row">
					<div class="col-xs-6">
						<label class="text-info">Cliente</label><br />
							<a href="{{ url('clientes/'.$pedido->cliente->id) }}">{{ $pedido->cliente->nombre_corto }}</a>
					</div>
					<div class="col-xs-6">
						<label class="text-info">Establecimiento</label><br />
						@if($pedido->cliente_establecimiento_id != null)
		                	<a href="{{ url('establecimientos/'.$pedido->establecimiento->id) }}">{{ $pedido->establecimiento->sucursal }}</a>
						@else
							No Aplica
						@endif
		            </div>
				</div>
		    	<br />
				<div class="row">
		    		<div class="col-xs-6">
		    			<label class="text-info">Fecha de Petición</label><br />
		    			{{ fecha_to_human($pedido->created_at,false) }}
		    		</div>
		    		<div class="col-xs-6">
		    			<label class="text-info">Es Evento</label><br />
						@if($pedido->es_evento == '1')
							Sí
						@else
							No
						@endif
		    		</div>
		    	</div>
		    	<br />
		    	<div class="row">
		    		<div class="col-xs-6">
		    			<label class="text-info">Fecha de Entrega</label><br />
						@if($pedido->fecha_entrega != null)
							{{ fecha_to_human($pedido->fecha_entrega,false) }}
						@else
							Sin definir
						@endif
		    		</div>
		    		<div class="col-xs-6">
		    			<label class="text-info">Hora Deseada</label><br />
		    			@if($pedido->hora_deseada != null)
							{{ $pedido->hora_deseada }}
						@else
							Sin definir
						@endif
		    		</div>
		    	</div>
		    	<br />
		    	<div class="row">
		    		<div class="col-xs-6" style="font-weight: bold;">
		    			<label class="text-info">Entregado:</label><br />
						@if($pedido->entregado_at != null)
							{{ fecha_to_human($pedido->entregado_at,false) }}
						@else
							N.D.
						@endif
		    		</div>
		    		<div class="col-xs-6" style="font-weight: bold;">
		    			<label class="text-info">Hora:</label><br />
		    			@if($pedido->entregado_at != null)
		    				@php
						      $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
						    @endphp
						   	{{ $hora_entrega }}
						@else
							N.D.
						@endif
		    		</div>
		    	</div>
		    	<br />
		    	@if($pedido->status > 2)
			    	<div class="row">
			    		<div class="col-xs-6" style="font-weight: bold; text-align: center;">
			    			<a class="btn btn-primary btn-rounded btn-block" href="{{ url('pedidos/' . $pedido->id . '/remision') }}"><i class="fa fa-file-pdf-o"></i> Descargar Remisión</a>
						</div>
						<div class="col-xs-6" style="font-weight: bold; text-align: center;">
			    			<a class="btn btn-primary btn-rounded btn-block" data-toggle="modal" data-target="#modalEnviarEmail"><i class="fa fa-envelope"></i> Enviar por Email</a>
						</div>
					</div>
				@endif
				<hr />
				<label class="text-info">Comentarios</label>
				<br />
				@if($pedido->comentarios == null)
					Sin comentarios
				@else
					{!! nl2br(e($pedido->comentarios)) !!}
				@endif
		    </div>
	    </div>
    </div>
</div>