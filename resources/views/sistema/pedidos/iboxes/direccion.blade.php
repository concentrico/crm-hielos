<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-map-marker"></i> Dirección Entrega</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalPedidoDireccion">
			<i class="fa fa-edit"></i> Editar
		</a>
    </div>
    
    <div class="ibox-content">

    	@if($pedido->evento_calle != null)
		    <div class="row">
			    <div class="col-sm-12">
					<span class="text-success">Calle: </span>
					<span class="text-primary">{{ $pedido->evento_calle }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número exterior: </span>
					<span class="text-primary">{{ $pedido->evento_numero_exterior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número interior: </span>
					@if($pedido->evento_numero_interior != null)
						<span class="text-primary">{{ $pedido->evento_numero_interior }}</span>
					@else
						<span class="text-primary">N.A.</span>
					@endif
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Colonia: </span>
					<span class="text-primary">{{ $pedido->evento_colonia }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Código Postal: </span>
					<span class="text-primary">{{ $pedido->evento_codigo_postal }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Municipio: </span>
					<span class="text-primary">{{ $pedido->evento_municipio }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Estado: </span>
					<span class="text-primary">{{ $pedido->evento_estado }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Indicaciones Chofer: </span>
					@if($pedido->evento_direccion_indicaciones != null)
						<span class="text-primary">{!! nl2br(e($pedido->evento_direccion_indicaciones)) !!}</span>
					@else
						<span class="text-primary">N.A.</span>
					@endif
			    </div>
		    </div>
		@else
			<div class="row">
			    <div class="col-sm-12">
					<span class="text-success">Calle: </span>
					<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->calle : $pedido->evento_calle }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número exterior: </span>
						<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->numero_exterior : $pedido->evento_numero_exterior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Número interior: </span>
					<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->numero_interior : $pedido->evento_numero_interior }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Colonia: </span>
						<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->colonia : $pedido->evento_colonia }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Código Postal: </span>
					<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->codigo_postal : $pedido->evento_codigo_postal }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Municipio: </span>
					<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->municipio : $pedido->evento_municipio }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Estado: </span>
					<span class="text-muted">{{ ($pedido->cliente_establecimiento_id != null) ? $pedido->establecimiento->estado : $pedido->evento_estado }}</span>
			    </div>
			    <div class="col-sm-12">
					<span class="text-success">Indicaciones Chofer: </span>
					@if($pedido->evento_direccion_indicaciones != null)
						<span class="text-muted">{!! nl2br(e($pedido->evento_direccion_indicaciones)) !!}</span>
					@else
						<span class="text-muted">N.A.</span>
					@endif
			    </div>
		    </div>
		@endif
    </div>
</div>