 <div class="ibox float-e-margins">
    <div class="ibox-title">
    	<a class="text-success pull-right" href="{{url('pedidos/create')}}">
			<i class="fa fa-plus"></i> Pedido
		</a>
        <h5><i class="fa fa-list"></i> Pedidos</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableInventarioExterno" name="dataTableInventarioExterno" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" data-breakpoints="sm" >{!! getHeaderLink('ID', 'ID') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('CLIENTE', 'Cliente') !!}</th>
						<th data-type="html">{!! getHeaderLink('ESTABLECIMIENTO', 'Establecimiento') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('FECHAENTREGA', 'Fecha Entrega') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('ENTREGADO', 'Entregado') !!}</th>
						<th data-type="html" data-breakpoints="sm">Bolsas</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('TOTAL', 'Total Pagar') !!}</th>
						<th data-type="html">{!! getHeaderLink('STATUS', 'Status') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($pedidos as $pedido)
	                    <tr>
	                        <td>
	                        	<a class="text-navy" href="{{ url('pedidos/'.$pedido->id) }}">{{ $pedido->id }}</a>
	                        </td>
	                        <td><a href="{{ url('clientes/'.$pedido->cliente_id) }}">{{ $pedido->cliente->nombre_corto }}</a></td>

	                        <td>
	                        	@if($pedido->cliente_establecimiento_id != null)
	                        		<a href="{{ url('establecimientos/'.$pedido->establecimiento->id) }}">{{ $pedido->establecimiento->sucursal }}</a>
	                        	@endif
	                        </td>
	                        <td>
	                        	@if($pedido->fecha_entrega != null)
									{{fecha_to_human($pedido->fecha_entrega,false) }}
									-
									{{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->fecha_entrega)->format('h:i A') }}
								@else
									Sin definir
								@endif
	                        </td>
	                        <td>
                                @if($pedido->entregado_at != null)
                                    {{fecha_to_human($pedido->entregado_at,false) }}
                                    -
                                    {{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
                                @else
                                    N.D.
                                @endif
                            </td>
                            <td class="text-center">
                            	@if($pedido->status == 3)
                            		<b>{{ $pedido->getTotalBolsas() }}</b>
                            	@else
                            		{{ $pedido->getTotalBolsas() }}
                            	@endif
                            </td>
		                    <td>${{ number_format($pedido->total(), 2, '.', ',') }}</td>
		                    <td>
		                    	@php
		                    		switch ($pedido->status) {
		                    			case '1':
		                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">Programado</span>';
		                    				break;
		                    			case '2':
		                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">En Camino</span>';
		                    				break;
		                    			case '3':
		                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Entregado</span>';
		                    				break;
		                    			case '4':
		                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Devolución</span>';
		                    				break;
		                    			case '5':
		                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Cancelado</span>';
		                    				break;
		                    			default:
		                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>';
		                    				break;
		                    		}
		                    	@endphp
		                    </td>
		                    <td>{{ $pedido->created_at }}</td>
	                        <td>{!! $pedido->creado_por->liga() !!}</td>
	                        <td>{{ $pedido->updated_at }}</td>
	                        <td>{!! $pedido->actualizado_por->liga() !!}</td>
		                    <td class="text-center">
		                    	<a href="{{url('pedidos').'/'.$pedido->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    </td>
	                    </tr>
					@endforeach
			</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>