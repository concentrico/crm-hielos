<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-cubes"></i> Productos</h5>
        @if($pedido->status < 3)
        <button class="btn btn-danger btn-xs pull-right m-l-md" type="button" id="btnAgregarCortesia" data-toggle="modal" data-target="#modalAgregarCortesia"><i class="fa fa-gift"></i>&nbsp;&nbsp;<span class="bold">Agregar Cortesía</span></button>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalProductoPedidoAgregar">
			<i class="fa fa-plus"></i> Agregar Producto
		</a>
		@endif
    </div>
    
    <div class="ibox-content">
	    	    
	    <div class="row">
		    <div class="col-sm-12">
				<div class="m-t">
					<table class="table table-striped" id="tablaProductosPedido">
						<thead>
							<tr>
								<th class="col-xs-1">&nbsp;</th>
								<th class="col-xs-3 text-center">Producto</th>
		                        <th class="col-xs-1 text-center">Precio Unitario</th>
		                        <th class="col-xs-2 text-center">Requerido</th>
		                        <th class="col-xs-2 text-center">Recibido</th>
                    			<th class="col-xs-1 text-center">IVA</th>
		                        <th class="col-xs-2 text-center">Total</th>
							</tr>
						</thead>
						<tbody>
							@if(count($pedido->productos) > 0)
								@foreach($pedido->productos as $producto)
									<tr>
										<td>
											@if($pedido->status < 3)
												<a href="javascript:void(0)" class="text-danger" onclick="eliminarProductoPedido({{ $producto->id }})">
												<i class="fa fa-minus-circle"></i>
												</a>
											@else
												<a href="javascript:void(0)" class="text-danger" style="opacity: 0.5;">
												<i class="fa fa-minus-circle"></i>
												</a>
											@endif
											@if($pedido->status < 4 && $pedido->status_cobranza < 3)
												<a href="javascript:void(0)" class="m-l-sm text-success" onclick="showModalProductoPedidoEdit({{ $producto->id }})">
													<i class="fa fa-pencil"></i>
												</a>
								           	@else
												<a href="javascript:void(0)" class="m-l-sm text-success" style="opacity: 0.5;">
													<i class="fa fa-pencil"></i>
												</a>
								           	@endif
										</td>
										<td>
											<a class="text-navy" href="{{url('productos/'.$producto->producto_id)}}">{{ $producto->producto->nombre }}</a>
										</td>
										<td class="text-center">
											@if($producto->precio_unitario == 0.00)
												<h4 class="text-info"> Cortesía </h4>
											@else
												<h4> ${{number_format($producto->precio_unitario,2)}} </h4>
											@endif
										</td>
										<td class="text-center prod-compra">
											<h4> {{ $producto->cantidad_requerida }} </h4>
										</td>
										<td class="text-center prod-venta">
										@if($producto->cantidad_recibida != null)
											<h4> {{ $producto->cantidad_recibida }} </h4>
										@else
											<h4> N.D. </h4>
										@endif
										</td>
										<td class="text-center prod-compra">
											<h4> {{ ($producto->producto->iva*100) }} %</h4>
										</td>
										<td class="text-right prod-dif">
											@if($producto->subtotal == 0.00)
												<h4 style="text-decoration: line-through;" id="subtotal_producto{{$producto->producto_id}}">${{number_format($producto->subtotal,2)}}</h4>
											@else
												<h4 id="subtotal_producto{{$producto->producto_id}}">${{number_format($producto->subtotal,2)}}</h4>
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tfoot>
								<tr>
									<td class="text-center" colspan="7">
										<div class="alert alert-danger">
						                    El Pedido no tiene productos.
						                </div>
									</td>
								</tr>
								</tfoot>
							@endif
						</tbody>
					</table>
					<table class="table invoice-total m-b-none">
        				<tbody>
						<tr>
							<td class="text-right" colspan="6">
								<strong>SUBTOTAL:</strong>
							</td>
							<td class="text-right cot-subtotal">${{ number_format($pedido->subtotal, 2, '.', ',') }}</td>
						</tr>
						<tr>
							<td class="text-right" colspan="6">
								<strong>IVA:</strong>
							</td>
							<td class="text-right cot-iva">${{ number_format($pedido->iva, 2, '.', ',') }}</td>
						</tr>
						<tr>
							<td class="text-right" colspan="6">
								<strong>TOTAL:</strong>
							</td>
							<td class="text-right text-success cot-total">${{ number_format($pedido->total, 2, '.', ',') }}</td>
						</tr>
						</tbody>
					</table>
				</div>
		    </div>
	    </div>
    </div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "progressBar": true,
	  "preventDuplicates": false,
	  "positionClass": "toast-bottom-right",
	  "onclick": null,
	  "showDuration": "400",
	  "hideDuration": "1000",
	  "timeOut": "4000",
	  "extendedTimeOut": "4000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
</script>	
@endsection