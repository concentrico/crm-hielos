<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-credit-card text-info"></i> Pagos</h5>
    </div>

    <div class="ibox-content">
	   	@if($pedido->total - $pedido->pagos->sum('monto') > 0) 
	   		@if($pedido->status == 3) {{-- Solo cuando esta en status ENTREGADO ya que lo que se cobrara es en base a lo que recibió el cliente y no en base a lo que pidió--}}
				<a class="text-success pull-right" data-toggle="modal" data-target="#modalPedidoPagoCreate">
					<i class="fa fa-plus"></i> Agregar pago
				</a>
			@endif
		@else
			<div class="pull-right text-info">
				<i class="fa fa-lg fa-check-circle-o text-info " style="font-size: 1.9em;"></i> &nbsp;Liquidado
			</div>
			 
		@endif
	    <div class="row">
		    <div class="col-sm-12 col-lg-4">
		    	<h4 class="text-success">Status Cobranza:
				@php
		           	switch ($pedido->status_cobranza) {
		           	case '1': // Pendiente
		           		echo '<span class="label label-danger" style="display:inline-block;">Pendiente</span>';
		           	break;
		           	case '2': // Pago Parcial
		           		echo '<span class="label label-warning" style="display:inline-block;">Pago Parcial</span>';
		           	break;
		           	case '3': // Liquidado
		           		echo '<span class="label label-success" style="display:inline-block;">Liquidado</span>';
		           	break;
		           	case '4': // Incobrable
		           		echo '<span class="label label-default" style="display:inline-block;">Incobrable</span>';
		           	break;
		           	}
	           	@endphp
	           	<a href="javascript:void(0)" class="m-l-sm text-success" data-toggle="modal" data-target="#modalStatusCobranza">
					<i class="fa fa-pencil"></i>
				</a>
	           	</h4>
		   		<div class="row">
					<div class="col-sm-4">
						<span style="font-weight: bold;">SUBTOTAL</span><br />
						<span style="">${{ number_format($pedido->subtotal, 2, '.', ',') }} </span>
					</div>
					<div class="col-sm-4">
						<span style="font-weight: bold;">IVA</span><br />
						<span style="">${{ number_format($pedido->iva, 2, '.', ',') }} </span>
					</div>
					<div class="col-sm-4">
						<span style="font-weight: bold;">TOTAL</span><br />
						<span style="">${{ number_format($pedido->total, 2, '.', ',') }} </span>
					</div>
				</div>
				<BR/>
			    <div class="row">
					<div class="col-sm-4 col-lg-12">
						<small class="stats-label">Pendiente</small>
						<h4 class="text-danger">${{ number_format($pedido->total - $pedido->pagos->sum('monto'), 2, '.', ',') }}</h4>
						<div class="progress">
							@if($pedido->total > 0)
								<div style="width: {{ 100 - (($pedido->pagos->sum('monto')/$pedido->total)*100)}}%;" class="progress-bar progress-bar-danger"></div>
							@else
								<div style="width: 0%;" class="progress-bar progress-bar-danger"></div>
							@endif
						</div>
					</div>
					<div class="col-sm-4 col-lg-12">
						<small class="stats-label">Cobrado</small>
						<h4 class="text-info">${{ number_format($pedido->pagos->sum('monto'), 2, '.', ',') }}</h4>
						<div class="progress">
							@if($pedido->total > 0)
								<div style="width: {{ (($pedido->pagos->sum('monto')/$pedido->total)*100)}}%; background-color: #1ab394;" class="progress-bar"></div>
							@else
								<div style="width: 0%;" class="progress-bar progress-bar-danger"></div>
							@endif
						</div>
					</div>
				</div>
		    </div>
		    <div class="col-sm-12 col-lg-8">
			    <h4 class="text-info"> Pagos registrados</h4>
			    <div class="row">
				    <div class="col-sm-12">
					    @if(count($pedido->pagos) == 0)
							<div class="alert alert-info">
								No hay pagos registrados!
							</div>
						@else
							<div class=" m-t">
								<table class="table table-res" data-cascade="true">
									<thead>
										<tr>
											<th>Fecha</th>
											<th>Forma de pago</th>
											<th class="text-right">Monto</th>
											<th class="text-center">Comprobante</th>
											<th class="text-center" data-type="html" data-breakpoints="xxlg">Comentarios</th>
											<th class="text-center" data-type="html" >&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										@foreach($pedido->pagos as $pago)
											<tr>
												<td>
													<div>
														{{ fecha_to_human($pago->fecha_pago,false)}}
													</div>
												</td>
												<td>
													{{ ucfirst($pago->forma_pago->nombre) }}
												</td>
												<td class="text-right">${{ number_format($pago->monto, 2, '.', ',') }}</td>
												<td class="text-center">
													@if($pago->comprobante_path != null)
														<a class="" 
													target="_blank" href="{{ asset($pago->comprobante_path) }}"><i class="fa fa-file-pdf-o"></i></a>
													@else
														-
													@endif
												</td>
												<td>
													@if($pago->comentarios!=null)
														{!! $pago->comentarios !!}
													@else
														N.D.
													@endif
												</td>
												<td>
													@if($pedido->status == 3) {{-- Solo cuando esta en status ENTREGADO ya que lo que se cobrara es en base a lo que recibió el cliente y no en base a lo que pidió--}}
													<div class="dropdown">
														<button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<i class="fa fa-pencil"></i>
														<span class="caret"></span>
														</button>
														<ul class="dropdown-menu" style="right: 5px; left: auto;">
															<li class="text-success"><a href="#" onclick="showModalPedidoPagoEdit({{$pago->id}});">
																<i class="fa fa-pencil"></i> Editar</a>
															</li>
															<li role="separator" class="divider"></li>
															<li class="text-danger"><a href="#" onclick="showModalPedidoPagoDelete({{$pago->id}});">
																<i class="fa fa-trash"></i> Eliminar</a>
															</li>
														</ul>
													</div>
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div><!-- /table-responsive -->
						@endif
				    </div>
			    </div>
			</div>
	    </div>
    </div>

</div>