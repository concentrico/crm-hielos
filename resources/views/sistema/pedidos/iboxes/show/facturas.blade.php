<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-file-pdf-o text-info"></i> Facturas</h5>
    </div>
    <div class="ibox-content">
	   	@if($pedido->total - $pedido->facturas->sum('total') > 0)
	   		@if($pedido->status == 3 && $pedido->status_facturacion != 4) {{-- Solo cuando esta en status ENTREGADO ya que lo que se cobrara es en base a lo que recibió el cliente y no en base a lo que pidió--}}
				<a class="text-success pull-right" data-toggle="modal" data-target="#modalPedidoFacturaCreate">
					<i class="fa fa-plus"></i> Agregar Factura
				</a>
			@endif
		@else
			<div class="pull-right text-info">
				<i class="fa fa-lg fa-check-circle-o text-info " style="font-size: 1.9em;"></i> &nbsp;Facturado
			</div>

			@if($pedido->checkFacturasVencidas())
				<div class="pull-right text-danger m-r-xl">
					<i class="fa fa-lg fa-check-circle-o text-danger " style="font-size: 1.9em;"></i> &nbsp;Factura Vencida
				</div>
			@endif
		@endif
	    <div class="row">
		    <div class="col-md-12">
				<h4 class="text-success">Status Facturación:
				@php
		           	switch ($pedido->status_facturacion) {
		           	case '1': // No Facturado
		           		echo '<span class="label label-danger" style="display:inline-block;">No Facturado</span>';
		           	break;
		           	case '2': // Facturado Parcialmente
		           		echo '<span class="label label-warning" style="display:inline-block;">Facturado Parcialmente</span>';
		           	break;
		           	case '3': // Facturado
		           		echo '<span class="label label-success" style="display:inline-block;">Facturado</span>';
		           	break;
		           	case '4': // Infacturable
		           		echo '<span class="label label-default" style="display:inline-block;">Infacturable</span>';
		           	break;
		           	}
	           @endphp
				<a href="javascript:void(0)" class="m-l-sm text-success" data-toggle="modal" data-target="#modalStatusFacturacion">
					<i class="fa fa-pencil"></i>
				</a>
	           </h4>
				@if(count($pedido->facturas) > 0)
					<table class="table table-res" id="tablaFacturasPedido" data-cascade="true">
						<thead>
							<tr>
								<th class="col-xs-2 text-center">Fecha</th>
								<th class="col-xs-2 text-center">Folio</th>
								<th class="col-xs-1 text-center" data-breakpoints="xs sm">Subtotal</th>
								<th class="col-xs-1 text-center" data-breakpoints="xs sm">IVA</th>
								<th class="col-xs-1 text-center">Total</th>
								<th class="col-xs-2 text-center">Fecha Vencimiento</th>
								<th class="col-xs-2 text-center" data-type="html" data-breakpoints="xs sm">Adjuntos</th>
								<th class="col-xs-12 text-center" data-type="html" data-breakpoints="xxlg">Comentarios</th>
								<th class="col-xs-1 text-center" data-type="html" style="width: 50px;"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($pedido->facturas as $factura)
							<tr>
								<td class="text-center">{{fecha_to_human($factura->fecha_facturacion,false)}}</td>
								<td class="text-center">{{$factura->folio}}</td>
								<td class="text-center">${{ number_format($factura->subtotal, 2, '.', ',') }}</td>
								<td class="text-center">${{ number_format($factura->iva, 2, '.', ',') }}</td>
								<td class="text-center">${{ number_format($factura->total, 2, '.', ',') }}</td>
								<td class="text-center text-bold">{{fecha_to_human($factura->fecha_vencimiento,false)}}</td>
								<td class="text-center">

									<ul class="list-unstyled file-list" style="display: inline-flex;">

										@if($factura->pdf_path != null)
											<li><a target="_blank" href="{{ asset($factura->pdf_path) }}"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
										@else
											<li><a style="opacity: 0.5; cursor: help;"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
										@endif
										@if($factura->xml_path != null)
											<li><a target="_blank" href="{{ asset($factura->xml_path) }}"><i class="fa fa-file"></i> XML</a></li>
										@else
											<li><a style="opacity: 0.5; cursor: help;"><i class="fa fa-file"></i> XML</a></li>
										@endif
		                            </ul>

								</td>
								<td>
									@if($factura->comentarios!=null)
										{!! $factura->comentarios !!}
									@else
										N.D.
									@endif
								</td>
								<td>
									@if($pedido->status == 3) {{-- Solo cuando esta en status ENTREGADO ya que lo que se cobrara es en base a lo que recibió el cliente y no en base a lo que pidió--}}
									<div class="dropdown">
										<button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										<i class="fa fa-pencil"></i>
										<span class="caret"></span>
										</button>
										<ul class="dropdown-menu" style="right: 5px; left: auto;">
											<li class="text-success"><a href="#" onclick="showModalPedidoFacturaEdit({{$factura->id}})">
												<i class="fa fa-pencil"></i> Editar</a>
											</li>
											<li role="separator" class="divider"></li>
											<li class="text-danger"><a href="#" onclick="showModalPedidoFacturaDelete({{$factura->id}})">
												<i class="fa fa-trash"></i> Eliminar</a>
											</li>
										</ul>
									</div>
									@endif
								</td>
							</tr>
							@endforeach									
						</tbody>
					</table>
				@else
					<div class="alert alert-warning text-center" style="margin-top: 10px;">
						El pedido no tiene facturas registradas.
					</div>
				@endif 
			</div>
		</div>
    </div>

</div>