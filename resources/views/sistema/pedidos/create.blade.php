@extends('sistema.layouts.master')

@section('title', 'Alta de Pedido')

@section('attr_navlink_9', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<style type="text/css" media="screen">
.i-checks{
	line-height: 32px;
	display: inline-block;
}
.selectdias {
	width: 50%!important;
	margin-left: 10%;
	display: inline-block;
}
.table > tbody > tr > td{
    border: 0px!important;
}
.btn:active:focus, .btn:focus {
    outline: none!important;
}
.form-group{
	margin-bottom: 5px!important;
}
</style>
@endsection

@section('breadcrumb_title', 'Alta de Pedido')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li><a href="{{ url('/pedidos') }}">Pedidos</a></li>
<li class="active"><strong>Alta de Pedido</strong></li>
<li></li>
@endsection

@if(session('data'))
    @php
    $cliente = session('data')['cliente'];
    $establecimiento = session('data')['establecimiento'];
    $es_evento = session('data')['es_evento'];
    @endphp
@endif

@section('content')
{!! Form::open(['method' => 'POST', 'onSubmit' => 'return false', 'id' => 'formPedido', 'url' => url('pedidos')] ) !!}
<div class="wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;">
	<div class="row">
		<div class="col-sm-7">
			@include('sistema.pedidos.iboxes.create.generales')
		</div>
		<div class="col-sm-5">
			@include('sistema.pedidos.iboxes.create.direccion')
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			@include('sistema.pedidos.iboxes.create.detalle')
		</div>
		<div class="col-sm-12">
			@include('sistema.pedidos.iboxes.create.submit')
		</div>
	</div>
</div>
{!! Form::close() !!}

@include('sistema.pedidos.modals.agregar_producto')
@include('sistema.pedidos.modals.agregar_cortesia')
@endsection

@section('assets_bottom')

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>

<script type="text/javascript">

	@if(session('data'))
	    $('select[name="cliente_establecimiento_cliente"]').dropdown().addClass("disabled");
	    $('select[name="cliente_establecimiento_sucursal"]').dropdown().addClass("disabled");
	@endif

	@if(session('data'))
		$('select[name="es_evento"]').dropdown().addClass("disabled");
	@endif

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	var direccion_entrega='';

	// PRODUCTOS PEDIDO (DESDE INVENTARIO) NO SE PUEDEN AGREGAR DOBLE
	var productos_pedido_evento = [];

	function esEventoChange(es_evento) {
		if (es_evento == '0') {
			$("#otra_direccion").prop('checked', false); 
			$("#otra_direccion").attr('disabled','disabled');
			otraDireccion(false);
			$('#btnAgregarProductos').attr('disabled','disabled');
			$('#btnAgregarProductos').removeAttr('data-toggle');
			$('#btnAgregarProductos').removeAttr('data-target');
		} else if (es_evento == '1') {
			$("#otra_direccion").removeAttr('disabled');
			$('#btnAgregarProductos').removeAttr('disabled');

			$('#btnAgregarProductos').attr('data-toggle','modal');
			$('#btnAgregarProductos').attr('data-target','#modalAgregarProducto');

			$('select[name="forma_pago"]').dropdown().removeClass("disabled");
			$('select[name="condicion_pago"]').dropdown().removeClass("disabled");
		}
		$('#tablaProductosPedido > tbody').empty();
		productos_pedido_evento = [];
		calcularTotalPedido();
	}

	function diaEntrega(obj) {
		if (obj.checked) {
			$("#select_dia_entrega").attr('readonly','readonly');
		} else {
			$("#select_dia_entrega").removeAttr('readonly');
		}
	}

	function otraDireccion(checked) {
		if (checked) {
			$("#otra_direccion").val('1');
			
			direccion_entrega = $("#direccion_entrega").html();

	        $("#direccion_entrega").html('<BR/><BR/><BR/><BR/>');
	        $("#evento_calle").removeAttr('readonly');
	        $("#evento_no_exterior").removeAttr('readonly');
	        $("#evento_colonia").removeAttr('readonly');
	        $("#evento_codigo_postal").removeAttr('readonly');
	        $("#evento_municipio").removeAttr('readonly');
	        $("#evento_estado").removeAttr('readonly');

	        $("#evento_calle").attr('required','required');
	        $("#evento_no_exterior").attr('required','required');
	        $("#evento_colonia").attr('required','required');
	        $("#evento_municipio").attr('required','required');
	        $("#evento_estado").attr('required','required');
		} else {
			$("#otra_direccion").val('0');
			$("#direccion_entrega").html(direccion_entrega);
			$("#evento_calle").attr('readonly','readonly');
	        $("#evento_no_exterior").attr('readonly','readonly');
	        $("#evento_colonia").attr('readonly','readonly');
	        $("#evento_codigo_postal").attr('readonly','readonly');
	        $("#evento_municipio").attr('readonly','readonly');
	        $("#evento_estado").attr('readonly','readonly');

	        $("#evento_calle").removeAttr('required');
	        $("#evento_no_exterior").removeAttr('required');
	        $("#evento_colonia").removeAttr('required');
	        $("#evento_municipio").removeAttr('required');
	        $("#evento_estado").removeAttr('required');
		}

	}

	@if(session('data')['establecimiento'] != null && session('inventario'))
		cargarDatosSucursal({{$establecimiento->id}},false);
	@elseif(session('data')['establecimiento'] != null && !session('inventario'))
		cargarDatosSucursal({{$establecimiento->id}},true);
	@elseif(session('data')['cliente'] != null && !session('inventario'))
		cargarDatosCliente({{$cliente->id}});
	@endif

	@if(session('inventario'))
		@foreach(session('inventario')['producto'] as $producto)
			productos_pedido_evento.push({{$producto->id}});
			$("#producto_agregar option[value='{{$producto->id}}']").remove();
			$('#producto_agregar').dropdown("set text", '');
            $('#producto_agregar').dropdown('refresh');
		@endforeach
	@endif

	function addCortesiaPedido() {
		var producto_id = $("#producto_cortesia").val();
		if( producto_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/productos')}}',
	            data:{ 'type': '1', 'producto_id': producto_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {

						response='<tr class="bg-info">';
                        response+='<td><a class="text-navy">'+data['nombre']+'</a><input type="hidden" name="producto_pedido[]" value="'+producto_id+'"/></td>';

                        response+='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('+producto_id+')" name="precio_pedido[]" id="precio_cortesia'+producto_id+'" type="text" placeholder="0.00" value="0.00" readonly=""></div></td>';

                        response+='<td><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_cortesia'+producto_id+'" onblur="calcularSubtotalProducto('+producto_id+')" onchange="calcularSubtotalProducto('+producto_id+')" value="1"/></div></td>';

                        var iva = parseFloat(data['iva']*100);

                        response+='<td class="text-center"><h4 class="ivas-pedido" id="iva_cortesia'+producto_id+'">'+iva+'%</h4></td>';

                        response+='<td><h4 class="subtotales-pedido" id="subtotal_cortesia'+producto_id+'">$0.00</h4></td>';

                        response+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

                        response+='</tr>';

                        productos_pedido_evento.push(producto_id);

                        $("#producto_cortesia option[value='"+producto_id+"']").remove();
						$('#producto_cortesia').dropdown("set text", '');
                        $('#producto_cortesia').dropdown('refresh');

                        $('#tablaProductosPedido > tbody').append(response);

                        $("#cantidad_cortesia"+producto_id).TouchSpin({
					        min: 0,
					        max: 1000000000,
					        step: 1,
					        decimals: 0,
					        boostat: 5,
					        maxboostedstep: 10,
					        buttondown_class: 'btn btn-white',
					        buttonup_class: 'btn btn-white'
					    });

					    toastr.success("Producto de cortesía agregado al Pedido.");

					calcularSubtotalProducto(producto_id);
	            }
	        });

	        $('#modalAgregarCortesia').modal('hide');
		} else {
			toastr.error("Seleccione un producto.");
		}
	}

	function addProductoPedido() {
		var producto_id = $("#producto_agregar").val();
		if( producto_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/productos')}}',
	            data:{ 'type': '1', 'producto_id': producto_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {

	            		var precio = data['precio_venta'];

						response='<tr>';
                        response+='<td><a class="text-navy">'+data['nombre']+'</a><input type="hidden" name="producto_pedido[]" value="'+producto_id+'"/></td>';

                        response+='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('+producto_id+')" name="precio_pedido[]" id="precio_producto'+producto_id+'" type="text" placeholder="0.00" value="'+precio+'"></div></td>';

                        response+='<td><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto'+producto_id+'" onblur="calcularSubtotalProducto('+producto_id+')" onchange="calcularSubtotalProducto('+producto_id+')" value="1"/></div></td>';

                        var iva = parseFloat(data['iva']*100);

                        response+='<td class="text-center"><h4 class="ivas-pedido" id="iva_producto'+producto_id+'">'+iva+'%</h4></td>';

                        var subtotal = parseFloat(precio)+(parseFloat(precio)*(parseFloat(data['iva'])));
                        response+='<td><h4 class="subtotales-pedido" id="subtotal_producto'+producto_id+'">'+numeral(subtotal.toString().replace(',','.')).format('$0,0.00')+'</h4></td>';

                        response+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

                        response+='</tr>';

                        productos_pedido_evento.push(producto_id);

                        $("#producto_agregar option[value='"+producto_id+"']").remove();
						$('#producto_agregar').dropdown("set text", '');
                        $('#producto_agregar').dropdown('refresh');

                        $('#tablaProductosPedido > tbody').append(response);

                        $("#cantidad_producto"+producto_id).TouchSpin({
					        min: 0,
					        max: 1000000000,
					        step: 1,
					        decimals: 0,
					        boostat: 5,
					        maxboostedstep: 10,
					        buttondown_class: 'btn btn-white',
					        buttonup_class: 'btn btn-white'
					    });

					    toastr.success("Producto agregado al Pedido.");

					calcularSubtotalProducto(producto_id);
	            }
	        });

	        $('#modalAgregarProducto').modal('hide');
		} else {
			toastr.error("Seleccione un producto.");
		}
	}

	function cargarDatosSucursal(sucursal_id,con_productos) {
		if(sucursal_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/establecimientos')}}',
	            data:{ 'type': '5', 'sucursal_id': sucursal_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	$("#evento_calle").val(data['calle']);
			        $("#evento_no_exterior").val(data['numero_exterior']);
			        $("#evento_colonia").val(data['colonia']);
			        $("#evento_codigo_postal").val(data['codigo_postal']);
			        $("#evento_municipio").val(data['municipio']);
			        $("#evento_estado").val(data['estado']);
					$('select[name="forma_pago"]').dropdown("set selected", data['forma_pago_id']);
					$('select[name="condicion_pago"]').dropdown("set selected", data['condicion_pago_id']);
					$('select[name="forma_pago"]').dropdown().addClass("disabled");
					$('select[name="condicion_pago"]').dropdown().addClass("disabled");


					if(con_productos) {
						$('#tablaProductosPedido > tbody').empty();
						if(data['pedido_producto'].length > 0) {
							for(var i=0; i < data['pedido_producto'].length; i++) {
								var response='<tr>';
		                        response+='<td><a class="text-navy">'+data['pedido_producto_nombre'][i]+'</a><input type="hidden" name="producto_pedido[]" value="'+data['pedido_producto'][i]+'"/></td>';

		                        response+='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('+data['pedido_producto'][i]+')" name="precio_pedido[]" id="precio_producto'+data['pedido_producto'][i]+'" type="text" placeholder="0.00" value="'+data['precio_producto'][i]+'"></div></td>';

		                        response+='<td><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto'+data['pedido_producto'][i]+'" onblur="calcularSubtotalProducto('+data['pedido_producto'][i]+')" onchange="calcularSubtotalProducto('+data['pedido_producto'][i]+')" value="'+data['cantidad_producto'][i]+'"/></div></td>';

		                        var iva = parseFloat(data['iva_producto'][i]*100);

		                        response+='<td class="text-center"><h4 class="ivas-pedido" id="iva_producto'+data['pedido_producto'][i]+'">'+iva+'%</h4></td>';

		                        response+='<td><h4 class="subtotales-pedido" id="subtotal_producto'+data['pedido_producto'][i]+'">'+numeral(data['subtotal_producto'][i].toFixed(2).toString().replace(',','.')).format('$0,0.00')+'</h4></td>';

		                        response+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

		                        response+='</tr>';

		                        $('#tablaProductosPedido > tbody').append(response);

		                        $("#cantidad_producto"+data['pedido_producto'][i]).TouchSpin({
							        min: 0,
							        max: 1000000000,
							        step: 1,
							        decimals: 0,
							        boostat: 5,
							        maxboostedstep: 10,
							        buttondown_class: 'btn btn-white',
							        buttonup_class: 'btn btn-white'
							    });
							    
							    productos_pedido_evento.push(data['pedido_producto'][i]);
							}
						} else {

							$('#tablaProductosPedido > tbody').empty();
							var response='<tr>';
		                    response+='<td class="text-center" colspan="6"><div class="alert alert-danger">El Establecimiento no tiene productos asociados. Por favor <a href="{{url('establecimientos/')}}'+'/'+sucursal_id+'">consigne conservadores</a> primero.</div></td>';
		                    response+='</tr>';
							$('#tablaProductosPedido > tbody').append(response);

						}
						
						calcularTotalPedido();
					}
					
	            }
	        });
		} else {
	        
		}
	}

	function cargarDatosCliente(cliente_id) {
		if(cliente_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/establecimientos')}}',
	            data:{ 'type': '6', 'cliente_id': cliente_id, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
					$('select[name="forma_pago"]').dropdown("set selected", data['forma_pago_id']);
					$('select[name="condicion_pago"]').dropdown("set selected", data['condicion_pago_id']);
					$('select[name="forma_pago"]').dropdown().addClass("disabled");
					$('select[name="condicion_pago"]').dropdown().addClass("disabled");

					$('#tablaProductosPedido > tbody').empty();
					if(data['pedido_producto'].length > 0) {
						for(var i=0; i < data['pedido_producto'].length; i++) {
							var response='<tr>';
	                        response+='<td><a class="text-navy">'+data['pedido_producto_nombre'][i]+'</a><input type="hidden" name="producto_pedido[]" value="'+data['pedido_producto'][i]+'"/></td>';

	                        response+='<td><div class="input-group" style="width: 100%;"><span class="input-group-addon">$</span><input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto('+data['pedido_producto'][i]+')" name="precio_pedido[]" id="precio_producto'+data['pedido_producto'][i]+'" type="text" placeholder="0.00" value="'+data['precio_producto'][i]+'"></div></td>';

	                        response+='<td><div class="input-group" style="width: 100%;"><input class="touchspin_cantidad positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto'+data['pedido_producto'][i]+'" onblur="calcularSubtotalProducto('+data['pedido_producto'][i]+')" onchange="calcularSubtotalProducto('+data['pedido_producto'][i]+')" value="'+data['cantidad_producto'][i]+'"/></div></td>';

	                        var iva = parseFloat(data['iva_producto'][i]*100);

	                        response+='<td class="text-center"><h4 class="ivas-pedido" id="iva_producto'+data['pedido_producto'][i]+'">'+iva+'%</h4></td>';

	                        response+='<td><h4 class="subtotales-pedido" id="subtotal_producto'+data['pedido_producto'][i]+'">'+numeral(data['subtotal_producto'][i].toFixed(2).toString().replace(',','.')).format('$0,0.00')+'</h4></td>';

	                        response+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';

	                        response+='</tr>';

	                        $('#tablaProductosPedido > tbody').append(response);

	                        $("#cantidad_producto"+data['pedido_producto'][i]).TouchSpin({
						        min: 0,
						        max: 1000000000,
						        step: 1,
						        decimals: 0,
						        boostat: 5,
						        maxboostedstep: 10,
						        buttondown_class: 'btn btn-white',
						        buttonup_class: 'btn btn-white'
						    });
						    
						    productos_pedido_evento.push(data['pedido_producto'][i]);
						}
					}
					calcularTotalPedido();
					
					$("#otra_direccion").removeAttr('disabled');
					$("#otra_direccion").prop('checked', true);
					otraDireccion(true);
					$('#btnAgregarProductos').removeAttr('disabled');

					$('#btnAgregarProductos').attr('data-toggle','modal');
					$('#btnAgregarProductos').attr('data-target','#modalAgregarProducto');

					$('select[name="forma_pago"]').dropdown().removeClass("disabled");
					$('select[name="condicion_pago"]').dropdown().removeClass("disabled");
	            }
	        });
		} 
	}

	function cargarEstablecimientos(cliente_id){
		if(cliente_id != '' ) {

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/establecimientos')}}',
	            data:{ 'type': '4', 'cliente_id': cliente_id, '_token': '{{ csrf_token()}}' },
	            beforeSend: function() {
	            	$("#cliente_establecimiento_sucursal").empty();
	            	$("#cliente_establecimiento_sucursal").dropdown('clear');

	            	$("#cliente_establecimiento_sucursal").dropdown().addClass('loading');
	            	$("#cliente_establecimiento_sucursal").dropdown('refresh');
		        },
	            success: function(data) {
	            	
	            	html='';
				    $.each(data, function(key, value) {
			            html += "<option value=" + value['id']  + ">" +value['text'] + "</option>"
			        });
			        $("#cliente_establecimiento_sucursal").append(html);
			        $("#cliente_establecimiento_sucursal").dropdown().removeClass('loading');
	            	$("#cliente_establecimiento_sucursal").dropdown('refresh');
	            	$("#cliente_establecimiento_sucursal").dropdown();


	            }
	        });
		} else {
	        $("#cliente_establecimiento_sucursal").empty();
	        $("#cliente_establecimiento_sucursal").dropdown('clear');
		}
	}

	function calcularSubtotalProducto(producto_id) {

		var subtotal=0.0;
		var cantidad = $('#cantidad_producto'+producto_id).val();
		var precio = $('#precio_producto'+producto_id).val();

		subtotal= parseFloat(cantidad) * parseFloat(precio);

        $('#subtotal_producto'+producto_id).html('$'+numeral(subtotal.toFixed(2).toString().replace(',','.')).format('0,0.00'));
        calcularTotalPedido();
	}

	function calcularTotalPedido() {

        var total = 0.0;
        var subtotal = 0.0;
        var iva = 0.0;
        var total_mas_iva = 0.0;
        var total_iva = 0.0;
        var totales = [];
        var imp = [];
        $(".subtotales-pedido").each(
            function(index, value) {
            	subtotal = parseFloat( $(this).html().replace("$", "").replace(',','') );
            	totales.push(subtotal);
            	total += subtotal;
            }
        );

        //Revisar productos con IVA
        $(".ivas-pedido").each(
            function(index, value) {
            	iva = parseFloat( $(this).html().replace("%", "").replace(',','') )
                imp.push( iva );
            }
        );
 
        for (var i = 0; i < totales.length; i++) {
         if( totales[i] != '') {
         	var mas_iva = totales[i]*(imp[i]/100);
            total_iva += mas_iva;
            total_mas_iva += totales[i]+( mas_iva );
          } else {
            total_mas_iva += 0.0;
            total_iva += 0.0;
          }
       	};

        $("#iva_pedido").html(numeral(total_iva.toFixed(2).toString().replace(',','.')).format('$0,0.00'));
        $("#subtotal_pedido").html(numeral(total.toFixed(2).toString().replace(',','.')).format('$0,0.00'));
        $("#total_pedido").html(numeral(total_mas_iva.toFixed(2).toString().replace(',','.')).format('$0,0.00'));
    }

	function confirmarPedido(){
		swal({
		  title: "Está seguro de generar el Pedido?",
		  text: "Su status pasará a Programado.",
		  type: "info",
		  showCancelButton: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){
			$("#formPedido").removeAttr('onsubmit');
			$("#formPedido").submit();
		});
	}

</script>
@endsection
