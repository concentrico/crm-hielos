@extends('sistema.layouts.master')

@section('title', 'Pedidos')

@section('attr_navlink_9', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', 'Pedidos')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Pedidos</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.pedidos.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.pedidos.iboxes.tabla')
	</div>
</div>
{{--
@include('sistema.pedidos.modals.corte')
@include('sistema.pedidos.modals.pedido')
--}}
@endsection

@section('assets_bottom')

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>

<script type="text/javascript">

	pedidos = [];
	
	@foreach($pedidos as $pedido)
	
		pedidos[{{ $pedido->id }}] = {
			cliente_id: '{{ $pedido->cliente_id }}',
			establecimiento_id: '{{ $pedido->cliente_establecimiento_id }}',
			fecha_entrega: '{{ $pedido->fecha_entrega }}',
			total: '{{ $pedido->total }}',
			chofer_id: '{{ $pedido->chofer_id }}',
			status: '{{ $pedido->status }}',
			es_evento: '{{ $pedido->es_evento }}',
		}
	
	@endforeach

</script>
@endsection
