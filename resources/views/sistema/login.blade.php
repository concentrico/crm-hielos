<!-- resources/views/auth/login.blade.php -->
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="Concéntrico">
        <link rel="icon" href="{{ asset('favicon.png') }}">

        <title>@if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | Login</title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <!-- Custom styles  -->
        <link rel="stylesheet" href="{{ asset('css/components.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/login.min.css') }}">

        <style type="text/css" media="screen">
            .bg { 
                /* The image used */
                background-image: url("{{ asset('img/bg.jpg') }}");

                /* Full height */
                height: 100%; 

                /* Center and scale the image nicely */
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }
            .login {
                background-color: #5f7083!important;
            }
        </style>

    </head>

  <body class="login bg">

    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{url('/')}}">
            @if(getSystemSettings()->site_logo != null) 
                <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="100">
            @else 
                <img src="{{ asset('img/logo-default.png') }}" alt="Logo" height="100"> 
            @endif
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        {!! Form::open(['id' => 'form_login', 'name' => 'login', 'class' => 'login-form' ,'method' => 'POST', 'url' => url('login')] ) !!}
            {!! Form::hidden('_token', csrf_token() ) !!}

            <h3 class="form-title text-center">Iniciar Sesión</h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Ingresa usuario y contraseña. </span>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger text-center">
                    <ul style="padding-left: 0px;">
                        @foreach ($errors->all() as $error)
                            <li style="list-style: none;">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuario/Email" name="inputUsername" id="inputEmail" /> </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="inputPassword" /> </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="1" /> Recordarme </label>
                <button type="submit" class="btn blue pull-right"> Ingresar </button>
            </div>
            <div class="forget-password">
                <h4>¿Olvidaste tu contraseña?</h4>
                <p>Haz clic <a href="{{ url('password/reset') }}"> aquí </a> para restaurar tu contraseña.</p>
            </div>

        {!! Form::close() !!}
    </div>

    </div> <!-- /container -->

    <!-- BEGIN CORE PLUGINS -->
    {{--<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    --}}
    <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.uniform.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.backstretch.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('js/login.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  

</body></html>