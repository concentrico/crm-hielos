<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-clipboard"></i> Pedido Sugerido </h5>
    </div>
    
    <div class="ibox-content">
        <div class="col-lg-12">
        	@if(count($establecimiento->precios) > 0)
        		<div class="alert alert-warning">
                    Pedido sugerido en base al último corte marcado por el cliente.
                </div>
                {!! Form::open(['method' => 'POST', 'onSubmit' => 'return false', 'id' => 'formPedido', 'url' => url('pedidos/inventario-externo/'.$establecimiento->id )] ) !!}
                <div class="col-lg-9 col-xs-12">
                	<table class="table table-res table-striped" id="tablaPedido" data-cascade="true" data-expand-first="true">
                    <thead>
                    <tr>
                        <th data-type="html" class="col-lg-3 col-xs-12">Producto</th>
                        <th data-type="html" class="col-lg-3 col-xs-12">Cantidad</th>
                        <th data-type="html" data-breakpoints="xs" class="col-lg-3 col-xs-12">Precio</th>
                        <th data-type="html" data-breakpoints="xs" class="col-lg-3 col-xs-12">Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $total=0.0;
                    @endphp
					@foreach($establecimiento->precios as $est_producto)
                        @php 
                            $subtotal_producto = $est_producto->precio_venta*intval($establecimiento->check_cantidad_sugerida_producto($est_producto->producto_id));
                            $total+=$subtotal_producto;
                        @endphp
						<tr>
                            <td>{{$est_producto->producto->nombre}}<input type="hidden" name="producto_pedido[]" value="{{$est_producto->producto_id}}"/></td>
                            <td>
                                <div class="input-group" style="width: 100%;">
								<input class="touchspin_responsive positive-integer cantidades-pedido" type="text" name="cantidad_pedido[]" id="cantidad_producto{{$est_producto->producto_id}}" onblur="calcularSubtotalProducto({{$est_producto->producto_id}})" onchange="calcularSubtotalProducto({{$est_producto->producto_id}})" value="{{$establecimiento->check_cantidad_sugerida_producto($est_producto->producto_id)}}"/>
                                </div>
                            </td>
                            <td>
                                <div class="input-group" style="width: 100%;">
								<span class="input-group-addon">$</span>
							   		<input class="form-control decimal-2-places precios-pedido" onblur="calcularSubtotalProducto({{$est_producto->producto_id}})" name="precio_pedido[]" id="precio_producto{{$est_producto->producto_id}}" type="text" placeholder="0.00" value="{{$est_producto->precio_venta}}">
							   	</div>
                            </td>
                            <td>
                                <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon">$</span>
							   		<input class="form-control" name="subtotal_producto[]" id="subtotal_producto{{$est_producto->producto_id}}" type="text" value="{{number_format($subtotal_producto,2)}}" readonly="">
							   	</div>
                            </td>
                        </tr>
					@endforeach
                    </tbody>
                    <tfoot>
                    	<tr>
                    		<td id="tfootTotalLabel" colspan="3" class="text-right"><h3 class="font-bold"> Total</h3></td>
                    		<td id="tfootTotal" class="text-center">
                    		<h3 class="font-bold" id="total_pedido">
                                ${{number_format($total,2)}}
                            </h3></td>
                    	</tr>
                    </tfoot>
					</table>
                </div>
                <div class="col-lg-3 col-xs-12 text-center p-xl h-300">
               		<button class="btn btn-outline btn-success dim btn-large" type="submit" onclick="generarPedido()"><i class="fa fa-shopping-cart"></i> Generar Pedido</button>
                </div>
                {!! Form::close() !!}    
        	@else
        		<div class="alert alert-warning">
                    El Establecimiento aún no tiene productos asociados.
                </div>
        	@endif
        </div>
    </div>
</div>