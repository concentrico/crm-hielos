 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list-all"></i> Registro de Movimientos </h5>
    </div>
    <div class="ibox-content" style="max-height: 700px; overflow-y: scroll;">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $entradas->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
        	<table id="dataTableInventarioEntradas" name="dataTableInventarioEntradas" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html">Tipo Movimiento</th>
						<th data-type="html">Producto</th>
						<th data-type="html" data-breakpoints="sm">Movimiento</th>
						<th data-type="html" data-breakpoints="sm">Cantidad</th>
						<th data-type="html" data-breakpoints="sm">Creado</th>
						<th data-type="html" data-breakpoints="sm">Creado por</th>
						<th data-type="html" data-breakpoints="xlg">Actualizado</th>
						<th data-type="html" data-breakpoints="xlg">Actualizado por</th>
					<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			<tbody>
				@if(count($entradas) > 0)
                    @foreach ($entradas as $entrada)
	                    <tr class="@if($entrada->deleted_at != NULL) warning @endif">
	                        <td>
	                        	<span class="label label-success">Entrada</span>
	                        </td>
	                        <td><a href="{{ url('productos/'.$entrada->producto_id) }}">{{ $entrada->producto->nombre }}</a></td>
	                        <td class="text-bold">
	                        	@php
	                        		switch ($entrada->motivo) {
	                        			case 'produccion':
	                        				echo 'Producción';
	                        				break;
	                        			case 'devolucion':
	                        				echo 'Devolución';
	                        				break;
	                        			case 'ajuste':
	                        				echo 'Ajuste';
	                        				break;
	                        			default:
	                        				echo 'N.D.';
	                        				break;
	                        		}
	                        	@endphp
	                        </td>
	                        <td class="text-center">{{ $entrada->cantidad_entrada }}</td>
	                        <td>{{ $entrada->created_at }}</td>
	                        <td>{!! $entrada->creado_por->liga() !!}</td>
	                        <td>{{ $entrada->updated_at }}</td>
	                        <td>{!! $entrada->actualizado_por->liga() !!}</td>
	                        <td>
		                        <div class="btn-group pull-right">
		                        @if($entrada->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalRegistroReactivar({{ $entrada->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
		                        @else
			                        <button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalEntradaEdit({{ $entrada->id }});">
			                        	<i class="fa fa-edit"></i>
			                        </button>
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalRegistroDelete({{ $entrada->id }});">
			                        	<i class="fa fa-trash"></i>
			                        </button>
		                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				@endif
				</tbody>
			</table>
        </div>
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $entradas->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>