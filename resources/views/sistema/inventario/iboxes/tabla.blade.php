 <div class="ibox float-e-margins sk">
    <div class="ibox-title">
        <h5><i class="fa fa-list-all"></i> Inventario Interno</h5>
    </div>
    @if(count($inventarios) > 2)
    	<div class="ibox-content" style="min-height: 450px;max-height: 450px; overflow-y: scroll;">
    @else
    	<div class="ibox-content" style="min-height: 450px;">
    @endif
    	@if(count($inventarios) > 2)
		    <div class="row">
			    <div class="col-sm-12 text-right">
					{!! $inventarios->appends(Input::except('page'))->render() !!}
				</div>
		    </div>
		@endif
        <div>

	        @if(count($inventarios) == 2)

	        	<div class="row" style="padding: 50px;">
	        		@foreach ($inventarios as $inventario)
		                <div class="col-lg-6">
		                    <div class="ibox float-e-margins" style="border: 3px solid #e7eaec; border-radius: 5px;">
		                        <div class="ibox-title" style="border: 0px;">
		                            <h5><a href="{{ url('productos/'.$inventario->producto_id) }}">{{ $inventario->producto->nombre }}</a></h5>
		                            <div class="pull-right">
		                            	@if($inventario->porcentaje_actual() >= 70)
			                    			<span class="pie" data-peity='{ "fill": ["green", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
			                    		@elseif($inventario->porcentaje_actual() >= 40 && $inventario->porcentaje_actual() < 70)
			                    			<span class="pie" data-peity='{ "fill": ["orange", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
			                    		@elseif($inventario->porcentaje_actual() < 40)
			                    			<span class="pie" data-peity='{ "fill": ["red", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
			                    		@else
			                    			<span class="pie" data-peity='{ "fill": ["gray", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
			                    		@endif
		                            </div>
		                            
		                        </div>
		                        <div class="ibox-content">
		                            <h1 class="no-margins text-center">
		                            	@if($inventario->cantidad_disponible != null) 
			                        		{{ $inventario->cantidad_disponible }}
			                        	@else
			                        		Sin registros
			                        	@endif
		                            </h1>

		                            <div class="row" style="margin-top: 1em; margin-left: 0px; margin-right: 0px;">
		                            	<h3 class="text-info">A Producir <i class="fa fa-level-up"></i>
			                            	<div class="stat-percent font-bold text-navy">
					                            @if($inventario->cantidad_producir != null) 
					                        		{{ $inventario->cantidad_producir }}
					                        	@else
					                        		Sin registros
					                        	@endif
					                            
				                            </div>
		                            	</h3>
		                            </div>
		                            
		                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
		                            	<h3 class="text-danger">Última Salida <i class="fa fa-level-down"></i>
				                            <div class="stat-percent font-bold text-navy">
					                            @if($inventario->ultima_salida != null) 
					                        		{{ $inventario->ultima_salida }}
					                        	@else
					                        		Sin registros
					                        	@endif
					                            
				                            </div>
				                        </h3>
		                            </div>

		                            <div class="row center text-center" style="margin-top: 1em; margin-left: 0px; margin-right: 0px;">
		                            	<button class="btn btn-success btn-sm" type="button" onclick="showModalRegistrar({{$inventario->id}})"><i class="fa fa-exchange"></i> Registrar
				               			</button>

				               			<div class="btn-group">
				                            <button data-toggle="dropdown" class="btn btn-danger btn-sm dropdown-toggle" aria-expanded="false"><i class="fa fa-eye"></i> Consultar <span class="caret"></span></button>
				                            <ul class="dropdown-menu">
				                                <li><a href="{{ url('inventario/entradas/'.$inventario->id) }}"> Entradas</a></li>
				                                <li><a href="{{ url('inventario/salidas/'.$inventario->id) }}"> Salidas</a></li>
				                            </ul>
				                        </div>
				               			
		                            </div>
		                            
		                        </div>
		                    </div>
		                </div>
		            @endforeach
	            </div>

	        @else

	        	<table id="dataTableConservadores" name="dataTableInventario" class="table table-res" data-cascade="true" data-expand-first="false">
					<thead>
						<tr>
							<th data-type="html">{!! getHeaderLink('PRODUCTO', 'Producto') !!}</th>
							<th data-type="html">{!! getHeaderLink('CANTACTUAL', 'Disponible') !!}</th>
							<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('CANTPRODUCIR', 'A Producir') !!}</th>
							<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('ULTSALIDA', 'Última Salida') !!}</th>
							<th data-type="html" data-breakpoints="sm"></th>
							<th data-type="html" data-breakpoints="sm"></th>
						</tr>
			        </thead>
				<tbody>
					@if(count($inventarios) > 0)
	                    @foreach ($inventarios as $inventario)
		                    <tr class="@if($inventario->deleted_at != NULL) warning @endif">
		                        <td><a href="{{ url('productos/'.$inventario->producto_id) }}">{{ $inventario->producto->nombre }}</a></td>
		                        <td class="text-center">
		                        	@if($inventario->cantidad_disponible != null) 
		                        		{{ $inventario->cantidad_disponible }}
		                        	@else
		                        		Sin registros
		                        	@endif
		                        </td>
		                        <td class="text-center">
		                        	@if($inventario->cantidad_producir != null) 
		                        		{{ $inventario->cantidad_producir }}
		                        	@else
		                        		Sin registros
		                        	@endif
		                        </td>
		                        <td class="text-center">
		                        	@if($inventario->ultima_salida != null) 
		                        		{{ $inventario->ultima_salida }}
		                        	@else
		                        		Sin registros
		                        	@endif
		                        </td>
			                    <td class="text-center">
			                    	@if($inventario->porcentaje_actual() >= 70)
		                    			<span class="pie" data-peity='{ "fill": ["green", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
		                    		@elseif($inventario->porcentaje_actual() >= 40 && $inventario->porcentaje_actual() < 70)
		                    			<span class="pie" data-peity='{ "fill": ["orange", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
		                    		@elseif($inventario->porcentaje_actual() < 40)
		                    			<span class="pie" data-peity='{ "fill": ["red", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
		                    		@else
		                    			<span class="pie" data-peity='{ "fill": ["gray", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->inventario_actual()}}</span>
		                    		@endif
			                    </td>
		                        <td class="center text-center">
			               			<button class="btn btn-primary btn-sm" type="button" onclick="showModalRegistrar({{$inventario->id}})"><i class="fa fa-exchange"></i>
			               			</button>
			               			<div class="btn-group">
			                            <button data-toggle="dropdown" class="btn btn-danger btn-sm dropdown-toggle" aria-expanded="false"><i class="fa fa-eye"></i> Consultar <span class="caret"></span></button>
			                            <ul class="dropdown-menu">
			                                <li><a href="{{ url('inventario/entradas/'.$inventario->id) }}"> Entradas</a></li>
			                                <li><a href="{{ url('inventario/salidas/'.$inventario->id) }}"> Salidas</a></li>
			                            </ul>
			                        </div>
			               		</td>
		                    </tr>
						@endforeach
					@endif
					</tbody>
				</table>

	        @endif
			
        </div>
        @if(count($inventarios) > 2)
		    <div class="row">
			    <div class="col-sm-12 text-right">
					{!! $inventarios->appends(Input::except('page'))->render() !!}
				</div>
		    </div>
		@endif
    </div>
</div>