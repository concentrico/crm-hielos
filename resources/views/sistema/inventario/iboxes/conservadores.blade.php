<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Conservadores</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalConservadorAsignar">
			<i class="fa fa-plus"></i> Asignar Conservador
		</a>
    </div>

    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
			    @if(count($establecimiento->conservadores) == 0)
					<div class="alert alert-warning">
						El establecimiento no tiene conservadores registrados!
					</div>
				@else
					<table class="table table-res" data-cascade="true">
						<thead>
							<tr>
								<th data-type="html">ID Sistema</th>
								<th data-type="html" data-breakpoints="sm">Modelo</th>
								<th data-type="html" data-breakpoints="sm">Número de Serie</th>
								<th data-type="html" data-breakpoints="sm">Status</th>
								<th data-type="html" data-breakpoints="sm">Frecuencia Mantenimiento</th>
								<th data-type="html" data-breakpoints="sm">Último Mantenimiento</th>
								<th data-type="html" data-breakpoints="xlg">Producto</th>
								<th data-type="html" data-breakpoints="xlg">Capacidad</th>
							</tr>
						</thead>
						<tbody>
		                    @foreach ($establecimiento->conservadores as $conservador)
			                    <tr class="@if($conservador->deleted_at != NULL) warning @endif">

			                        <td><a href="{{ url('conservadores/'.$conservador->id) }}">{{ $conservador->id_codigo }}</a></td>
			                        <td>{{ $conservador->modelo->modelo }}</td>
			                        <td class="text-center">{{ $conservador->numero_serie }}</td>
			                        <td class="text-center">
				                    	@php
				                    		switch ($conservador->status) {
				                    			case 'stand_by':
				                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Stand By</span>';
				                    				break;
				                    			case 'en_consignacion':
				                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">En Consignación</span>';
				                    				break;
				                    			case 'en_reparacion':
				                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Reparación</span>';
				                    				break;
				                    			case 'en_mantenimiento':
				                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Mantenimiento</span>';
				                    				break;
				                    			case 'fuera_servicio':
				                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">Fuera de Servicio</span>';
				                    				break;
				                    			default:
				                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">N.D.</span>';
				                    				break;
				                    		}
				                    	@endphp
				                    </td>

				                    <td class="text-center">{{ $conservador->frecuencia_mantenimiento }} días</td>

			                        <td class="text-center">

			                        @if($conservador->ultimo_mantenimiento != null)
				                    	@php 
				                    	$ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%Y-%m-%d');
				                    	$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
				                    	$diff = diferencia_fechas($ult_mtto, $now);

				                    	$formatted = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%A %d %B %Y');
				                    	@endphp

				                    	<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="{{$formatted}}">
				                    	
				                    	@if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false )    Hoy
				                    	@else
				                    		Hace {{$diff}}
				                    	@endif

				                    	</span>

				                    @else
				                    @endif
				                    </td>
			                        <td>{{ $conservador->producto->nombre }}</td>
			                        <td>{{ $conservador->modelo->capacidad }}</td>
			                    </tr>
							@endforeach
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	function checkCapacidadMaxima(obj){
		@if ($establecimiento->capacidad_maxima != null)
    		var max_capacidad = {{$establecimiento->capacidad_maxima}};
    	@else
    		var max_capacidad = {{$establecimiento->getCapacidadRecomendada('maxima')}};
    	@endif

    	if (obj.value > max_capacidad) {
    		obj.value=max_capacidad;
    	}
	}

	function calcularCapacidad(cap) {
		var sum = 0;
		$('.capbolsas').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#total_bolsas').html(sum);
	}

	function calcularCapacidadCorte(cap) {
		var sum = 0;
		$('.capproducto').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
	}

</script>