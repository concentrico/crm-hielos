 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Inventario Externo</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $inventario_ext->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableInventarioExterno" name="dataTableInventarioExterno" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" >{!! getHeaderLink('STATUS', '') !!}</th>
						<th data-type="html">{!! getHeaderLink('ESTABLECIMIENTO', 'Establecimiento') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('CLIENTE', 'Cliente') !!}</th>
						<th data-type="html" data-breakpoints="sm">Último Corte</th>
						<th data-type="html" data-breakpoints="sm">Próxima Entrega</th>
						<th data-type="html">{!! getHeaderLink('CANTACTUAL', 'Inventario Disponible') !!}</th>
						<th data-type="html"></th>
						<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($inventario_ext as $inventario)
	                    <tr>
	                        <td class="text-center">{!!$inventario->getColorInventario()!!}</td>
	                        <td><a href="{{ url('establecimientos/'.$inventario->id) }}">{{ $inventario->sucursal }}</a></td>
	                         <td><a href="{{ url('clientes/'.$inventario->clienteid) }}">{{ $inventario->cliente->nombre_corto }}</a></td>
	                        <td class="text-center">{!!$inventario->ultimo_corte()!!}
	                        </td>
		                    <td class="text-center">{!!$inventario->proximo_pedido()!!}</td>
		                    <td class="text-center">
		                    	@if($inventario->capacidad_actual != null)
		                    		{{$inventario->capacidad_actual}} bolsas
		                    	@else
		                    		Sin registros
		                    	@endif
		                    </td>
		             
		                    <td class="text-center">

		                    	@if($inventario->status_inventario == 0)
		                    		<span class="donut" data-peity='{ "fill": ["red", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
		                    	@elseif($inventario->status_inventario == 1)
		                    		<span class="donut" data-peity='{ "fill": ["orange", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
		                    	@elseif($inventario->status_inventario == 2)
		                    		<span class="donut" data-peity='{ "fill": ["green", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
		                    	@else
		                    		<span class="donut" data-peity='{ "fill": ["#eeeeee", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{$inventario->capacidad_actual}}/{{$inventario->capacidad_maxima}}</span>
		                    	@endif
	                    		
		                    </td>
		                    <td class="text-center">
		                    	<a href="{{url('inventario/externo').'/'.$inventario->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    </td>
	                    </tr>
					@endforeach
			</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $inventario_ext->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>