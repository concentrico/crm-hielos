<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-6">
			    <h2 class="text-info">
				    {{ $establecimiento->cliente->nombre_corto }} 
				</h2>
				<h3 class="text-primary"><a href="{{ url('establecimientos/'.$establecimiento->id) }}">{{ $establecimiento->sucursal }}</a></h3>
				<BR/>
				<h3 style="font-weight: bold;">Status Inventario:</h3>

				@if($establecimiento->status_inventario === null)
					<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
				@else
					@if($establecimiento->status_inventario == 0)
						<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Mínimo</span>
					@elseif($establecimiento->status_inventario == 1)
						<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Reorden</span>
					@elseif($establecimiento->status_inventario == 2)
						<span class="label label-success" style="margin-top: 5px; display:inline-block;">Óptimo</span>
					@else
						<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
					@endif
				@endif
				<BR/>
				<BR/>
				<div class="row">
					<div class="col-sm-6">
						<h4 class="text-success">Inventario Actual:</h4>
						<span style=""> {{ ( $establecimiento->capacidad_actual != null ? $establecimiento->capacidad_actual : 'No Establecida') }} bolsas</span>
					</div>
					<div class="col-sm-6">

					<h4 class="text-success">Último Corte:</h4>
					{!!$establecimiento->ultimo_corte()!!}
					</div>
				</div>
				@php 
                	if ($establecimiento->capacidad_maxima != null) {
                		$max_capacidad = $establecimiento->capacidad_maxima;
                	} else {
                		$max_capacidad = $establecimiento->getCapacidadRecomendada('maxima');
                	}
                @endphp
                <BR/>
                {!! Form::open(['method' => 'POST',  'url' => url('inventario/externo/'.$establecimiento->id.'/editar-puntos')] ) !!}
				<div class="row">
					<div class="form-group col-sm-4">
	                   {!! Form::label('capacidad_minima', 'Capacidad Mínima', ['class' => 'control-label']) !!}
	                   <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-battery-empty"></i></span>
	                   {!! Form::number('capacidad_minima', $establecimiento->capacidad_minima, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('minima'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
	                   </div>
	                   <span class="help-block">{{ $errors->first('capacidad_minima')}}</span>
	                </div>

			    	<div class="form-group col-sm-4">
	                    {!! Form::label('punto_reorden', 'Punto de Reorden', ['class' => 'control-label']) !!}
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></span>
	                        {!! Form::number('punto_reorden', $establecimiento->punto_reorden, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('punto_reorden'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
	                    </div>
	                    <span class="help-block">{{ $errors->first('punto_reorden')}}</span>
	                </div>

	                <div class="form-group col-sm-4">
	                   {!! Form::label('capacidad_maxima', 'Capacidad Máxima', ['class' => 'control-label']) !!}
	                    <div class="input-group">
	                        <span class="input-group-addon"><i class="fa fa-battery-full"></i></span>
	                   {!! Form::number('capacidad_maxima', $establecimiento->capacidad_maxima, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('maxima'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
	                   </div>
	                   <span class="help-block">{{ $errors->first('capacidad_maxima')}}</span>
	                </div>
	                <div class="form-group col-sm-12">

	                	@if($establecimiento->fecha_ultimo_corte() != '')
                    		@if($establecimiento->fecha_ultimo_corte() != Carbon::now()->formatLocalized('%d/%m/%Y') )
                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" onclick="showModalRealizarCorte()"><i class="fa fa-clipboard"></i> Realizar Corte</button>
                    		@else
                    			<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-xs pull-left" style="color:white;" disabled=""><i class="fa fa-clipboard"></i> Realizar Corte</button>
	                    	@endif
                    	@else
                    		<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" onclick="showModalRealizarCorte({{$establecimiento->id}})"><i class="fa fa-clipboard"></i>  Corte</button>
                    	@endif

			            <button type="submit" id="btnSaveCF" class="btn btn-outline btn-primary btn-sm pull-right">
				        	<i class="fa fa-save"></i> Guardar
				        </button>
				    </div>
		            {!! Form::close() !!}
				</div>

			</div>

			<div class="col-sm-6">
				<h2 class="text-primary text-center">
				    Nivel Inventario
				</h2>
	            <div id="nivel_conservador"></div>
	        </div>
		</div>
		
		
    </div>
</div>
