 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list-all"></i> Cuarto Frío </h5>
    </div>
    <div class="ibox-content text-center" style="min-height: 450px;max-height: 450px;">
        <div>
            <h1>Inventario Actual</h1>
            <h2 class="text-success">(bolsas)</h2>
            <div id="cuartofrio-chart" ></div>
        </div>
    </div>
</div>