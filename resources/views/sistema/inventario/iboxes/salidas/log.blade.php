<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-history"></i> Historial Cambios </h5>
    </div>
    
    <div class="ibox-content" style="max-height: 500px; overflow-y: scroll;">

    @php 
        $logs = \App\Models\LogInventario::where('salida_id','!=',null)->where('inventario_id',$inventario->id)->orderBy('created_at', 'DESC')->get();
    @endphp
        @if(count($logs) > 0)

            <div class="table-responsive">
                <table class="table table-hover issue-tracker">
                    <thead>
                        <th>Fecha y Hora</th>
                        <th>Usuario</th>
                        <th>Mensaje</th>
                        <th>Comentarios</th>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                        <tr>
                            
                            <td><span class="label label-default">{{ $log->created_at }}</span></td>
                            <td>{!!$log->usuario->liga()!!}</td>
                            <td class="issue-info">
                                {!!$log->log!!}
                            </td>
                            <td>
                            <span>{!! nl2br(e($log->comentarios))!!}</span>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="alert alert-warning text-center">
                No existen logs registrados!
            </div>
        @endif
    </div>
</div>
