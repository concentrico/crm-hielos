 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list-all"></i> Registro de Movimientos </h5>
    </div>
    <div class="ibox-content" style="max-height: 700px; overflow-y: scroll;">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $salidas->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
        	<table id="dataTableInventarioSalidas" name="dataTableInventarioSalidas" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html">Tipo Movimiento</th>
						<th data-type="html">Producto</th>
						<th data-type="html" data-breakpoints="sm">Movimiento</th>
						<th data-type="html" data-breakpoints="sm">Cantidad</th>
						<th data-type="html" data-breakpoints="sm">Creado</th>
						<th data-type="html" data-breakpoints="sm">Creado por</th>
						<th data-type="html" data-breakpoints="xlg">Actualizado</th>
						<th data-type="html" data-breakpoints="xlg">Actualizado por</th>
					<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			<tbody>
				@if(count($salidas) > 0)
                    @foreach ($salidas as $salida)
	                    <tr class="@if($salida->deleted_at != NULL) warning @endif">
	                        <td>
	                        	<span class="label label-danger">Salida</span>
	                        </td>
	                        <td><a href="{{ url('productos/'.$salida->producto_id) }}">{{ $salida->producto->nombre }}</a></td>
	                        <td class="text-bold">
	                        	@php
	                        		switch ($salida->motivo) {
	                        			case 'pedido':
	                        				echo 'Pedido';
	                        				break;
	                        			case 'merma':
	                        				echo 'Merma';
	                        				break;
	                        			case 'cortesia':
	                        				echo 'Cortesía';
	                        				break;
	                        			case 'ajuste':
	                        				echo 'Ajuste';
	                        				break;
	                        			default:
	                        				echo 'N.D.';
	                        				break;
	                        		}
	                        	@endphp
	                        </td>
	                        <td class="text-center">{{ $salida->cantidad_salida }}</td>
	                        <td>{{ $salida->created_at }}</td>
	                        <td>{!! $salida->creado_por->liga() !!}</td>
	                        <td>{{ $salida->updated_at }}</td>
	                        <td>{!! $salida->actualizado_por->liga() !!}</td>
	                        <td>
		                        <div class="btn-group pull-right">
		                        @if($salida->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalRegistroReactivar({{ $salida->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
		                        @else
			                        <button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalSalidaEdit({{ $salida->id }});">
			                        	<i class="fa fa-edit"></i>
			                        </button>
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalRegistroDelete({{ $salida->id }});">
			                        	<i class="fa fa-trash"></i>
			                        </button>
		                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				@endif
				</tbody>
			</table>
        </div>
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $salidas->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>