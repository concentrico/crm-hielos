<div class="modal inmodal" id="modalSalidaEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-edit modal-icon"></i> <BR/>Editar Registro</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('inventario/salidas/')] ) !!}
			{!! Form::hidden('salida_id', null) !!}
				<div class="modal-body">

		            <div class="row">
		            	<div class="form-group col-sm-5 @if ($errors->first('producto') !== "") has-error @endif">
							{!! Form::label('producto_nombre', 'Producto', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
						           	<span class="input-group-addon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
									{!! Form::text('producto_nombre', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
									<input type="hidden" name="producto_id" value="">
								</div>
									<span class="help-block">{{ $errors->first('producto')}}</span>
						</div>
						<div class="form-group col-sm-3 @if ($errors->first('movimiento') !== "") has-error @endif">
				            {!! Form::label('', 'Movimiento', ['class' => 'control-label text-center']) !!}
			            	{!! Form::select('movimiento', ['' => '', 'pedido' => 'Pedido', 'merma' => 'Merma', 'cortesia' => 'Cortesía'], null, ['class' => 'form-control selectdp', 'placeholder' => 'Movimiento'] ) !!}
				            <span class="help-block">{{ $errors->first('fecha_mtto')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('cantidad') !== "") has-error @endif">
				            {!! Form::label('cantidad', 'Cantidad', ['class' => 'control-label text-center']) !!}
			            	<input class="touchspin_cantidad positive-integer" type="text" id="cantidad" name="cantidad">
				        </div>
				        <div class="form-group col-sm-12 @if ($errors->first('comentarios') !== "") has-error @endif">
						    {!! Form::label('comentarios', 'Comentarios *', ['class' => 'control-label']) !!}
						        <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
								    {!! Form::textarea('comentarios', null, ['id' => 'comentarios',  'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						        </div>
						        <span class="help-block">{{ $errors->first('comentarios')}}</span>
						</div>
		            </div>

		            @if ($errors->first('editFailed') )
					    <div class="alert alert-warning text-center">
				           	{{$errors->first('editFailed')}}
				        </div>
					@endif

				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_editar_salida'))
		showModalSalidaEdit({{ Input::old('salida_id') }} );
	@endif
	@if(session('registro_update_success'))
		swal('Correcto', "Se ha actualizado el registro satisfactoriamente.", "success");
	@endif
	
	function showModalSalidaEdit(id){
		
		if ( $('#modalSalidaEdit').find('form').attr('action') == "{{url('inventario/salidas')}}/"+id){
			// Dejar en blanco, es cuando se recarga la pantalla.
		
		}else{
			$('#modalSalidaEdit').find('form').attr('action', "{{url('inventario/salidas')}}/"+id )
			$('#modalSalidaEdit input[name="salida_id"]').val( id );
			$('#modalSalidaEdit input[name="producto_id"]').val( salidas[id]['producto_id'] );
			$('#modalSalidaEdit input[name="producto_nombre"]').val( salidas[id]['producto'] );

			$('#modalSalidaEdit').find('select[name=movimiento]').dropdown('set selected', salidas[id]['motivo']);
			$('#modalSalidaEdit').find('input[name=cantidad]').val(salidas[id]['cantidad']);

		}

		$('#modalSalidaEdit').modal('show');
	}

</script>
@endsection