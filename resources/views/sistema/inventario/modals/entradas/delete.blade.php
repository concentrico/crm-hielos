<div class="modal inmodal" id="modalRegistroDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			{!! Form::open(['method' => 'POST',  'url' => url('inventario/entradas' )] ) !!}
			<input type="hidden" name="entrada_id" value="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar Registro</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            <h3>¿Estas seguro de eliminar éste registro?</h3>
				            <h4 class="text-info">Se <b>AJUSTARÁ</b> la cantidad actual de Inventario según la cantidad a eliminar.</h4>
				        </div>
				        <div class="col-sm-12 @if ($errors->first('comentarios') !== "") has-error @endif">
						    {!! Form::label('comentarios', 'Comentarios *', ['class' => 'control-label']) !!}
						        <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
								    {!! Form::textarea('comentarios', null, ['id' => 'comentarios',  'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						        </div>
						        <span class="help-block">{{ $errors->first('comentarios')}}</span>
						</div>
		            </div>
			</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
				</div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_registro_delete'))
		showModalRegistroDelete({{ Input::old('entrada_id')}});
	@endif

	function showModalRegistroDelete(id){

		if ( $('#modalRegistroDelete').find('form').attr('action') == "{{ url('inventario/entradas') }}/" + id +"/delete"){

		}else{
			$('#modalRegistroDelete').find('form').attr('action', "{{ url('inventario/entradas') }}/" + id+"/delete");
			$('#modalRegistroDelete input[name="entrada_id"]').val( id );
		}

		$('#modalRegistroDelete').modal('show');
	}

</script>
@endsection