<div class="modal inmodal" id="modalRegistrar" tabindex="-1" role="dialog" aria-hidden="true">
	
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="sk-spinner sk-spinner-wave">
		        <div class="sk-rect1"></div>
		        <div class="sk-rect2"></div>
		        <div class="sk-rect3"></div>
		        <div class="sk-rect4"></div>
		        <div class="sk-rect5"></div>
		    </div>
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-exchange modal-icon"></i> <BR/>Registrar Movimiento Inventario</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('inventario/interno'), 'id' => 'formRegistroInv'] ) !!}
				{!! Form::hidden('inventario_id') !!}
				<div class="modal-body">

		            <div class="row">
			            <div class="form-group col-sm-5 @if ($errors->first('producto_nombre') !== "") has-error @endif">
				            {!! Form::label('producto_nombre', 'Producto', ['class' => 'control-label text-center']) !!}
			            	{!! Form::text('producto_nombre', null, ['id'=>'producto_nombre','class' => 'form-control','readonly' => 'readonly'] ) !!}
				            <span class="help-block">{{ $errors->first('producto_nombre')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('tipo_movimiento') !== "") has-error @endif">
				            {!! Form::label('tipo_movimiento', 'Tipo de Movimiento', ['class' => 'control-label text-center']) !!}
				            {!! Form::select('tipo_movimiento', ['' => 'Seleccione', 'entrada' => 'Entrada', 'salida' => 'Salida'], null, ['class' => 'form-control selectdp', 'onchange' => 'cargarMovimientos(this.value)'] ) !!}
				            <span class="help-block">{{ $errors->first('tipo_movimiento')}}</span>
				        </div>
				        <div class="form-group col-sm-3 @if ($errors->first('movimiento') !== "") has-error @endif">
				            {!! Form::label('movimiento', 'Movimiento', ['class' => 'control-label text-center']) !!}
				            {!! Form::select('movimiento', ['' => ''], null, ['id'=> 'movimiento', 'class' => 'form-control selectdp', 'onchange' => 'selectMovimiento(this.value)'] ) !!}
				            <span class="help-block">{{ $errors->first('movimiento')}}</span>
				        </div>
		            </div>

		            <div class="row">

		            	<div class="form-group col-sm-4 col-sm-offset-4 @if ($errors->first('cantidad') !== "") has-error @endif">
							{!! Form::label('cantidad', 'Cantidad a registrar', ['class' => 'control-label']) !!}
							<input class="touchspin_cantidad positive-integer" type="text" name="cantidad">
							<span class="help-block">{{ $errors->first('cantidad')}}</span>
						</div>

						<div class="form-group col-sm-12 @if ($errors->first('comentarios') !== "") has-error @else hidden @endif" id="motivo">
						    {!! Form::label('comentarios', 'Motivo *', ['class' => 'control-label']) !!}
						        <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
								    {!! Form::textarea('comentarios', null, ['id' => 'comentarios',  'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						        </div>
						        <span class="help-block">{{ $errors->first('comentarios')}}</span>
						</div>
				    </div>

				    @if ($errors->first('registroFailed') )
					    <div class="alert alert-danger text-center">
				           	{{$errors->first('registroFailed')}}
				        </div>
					@endif

				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="submit" class="btn btn-primary" id="btnSubmit"><i class="fa fa-save"></i> Registrar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_registro'))
		$('#modalRegistrar').modal('show');
		showModalRegistrar({{ Input::old('inventario_id') }} );
	@endif
	@if(session('registro_success'))
		swal('Correcto', "Se ha registrado el Movimiento satisfactoriamente.", "success");
	@endif

	$('#formRegistroInv').submit(function(){
		$('#btnSubmit').prop('disabled', true);
		$('#modalRegistrar').find('.modal-content').toggleClass('sk-loading');
	});

	function cargarMovimientos(tipo_movimiento) {
		$("#movimiento").empty();
	    $("#movimiento").dropdown('clear');

	    var html='';
		if (tipo_movimiento == 'entrada') {
			html = "<option value=''></option>";
			html += "<option value='produccion'>Producción</option>";
			html += "<option value='devolucion'>Devolución</option>";
			html += "<option value='ajuste'>Ajuste</option>";
 		} else {
 			html = "<option value=''></option>";
			html += "<option value='pedido'>Pedido</option>";
			html += "<option value='merma'>Merma</option>";
			html += "<option value='cortesia'>Cortesía</option>";
			html += "<option value='ajuste'>Ajuste</option>";
 		}

 		$("#movimiento").append(html);
        $("#movimiento").dropdown('refresh');
        $("#movimiento").dropdown();
	}

	function selectMovimiento(movimiento) {
		if (movimiento == 'ajuste') {
	    	$("#motivo").removeClass('hidden');
		} else {
	    	$("#motivo").addClass('hidden');
		}
	}
	
	function showModalRegistrar(inventario_id){
		
		if ( $('#modalRegistrar').find('form').attr('action') == "{{url('inventario/interno')}}/"+inventario_id ){
			// Dejar en blanco, es cuando se recarga la pantalla.
		
		}else{
			$('#modalRegistrar').find('form').attr('action', "{{url('inventario/interno')}}/"+inventario_id )
			$('#modalRegistrar input[name="inventario_id"]').val( inventario_id );
			$('#modalRegistrar input[name="producto_nombre"]').val(inventario_externo[inventario_id]['producto'] );

		}

		$('#modalRegistrar').modal('show');
	}

</script>
@endsection