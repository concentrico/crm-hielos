<div class="modal inmodal" id="modalAgregarProductoCF" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-snowflake-o modal-icon"></i> <BR/>Agregar Producto</h4>
			</div>
				<div class="modal-body">
					@php $productos_cf_array = []; @endphp
					@if($establecimiento->cuartofrio_id != null )
						@if(count($establecimiento->cuartofrio->productos) > 0)
			            	@foreach ($establecimiento->cuartofrio->productos as $producto)
			            		@php $productos_cf_array[] = $producto->producto->id; @endphp
			            	@endforeach
			            @endif
			        @endif
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('fecha_mtto') !== "") has-error @endif">
				            {!! Form::label('producto_agregar', 'Seleccione Producto', ['class' => 'control-label text-center']) !!}
			            	{!! Form::select('producto_agregar', \App\Models\Producto::where('es_hielo', '=', 1)->get()->pluck('nombre', 'id')->except($productos_cf_array)->prepend('','')->toArray(), null, ['id' => 'producto_agregar','class' => 'col-xs-12 selectdp'] ) !!}
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="button" class="btn btn-success" onclick="addProductoCuartoFrio()"><i class="fa fa-save"></i> Agregar</button>
				</div>
		</div>
	</div>
</div>