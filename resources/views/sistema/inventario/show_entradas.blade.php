@extends('sistema.layouts.master')

@section('title', 'Inventario Interno | Entradas')

@section('attr_navlink_8', 'class="active"')
@section('attr_navlink_8_1', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', $inventario->producto->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('inventario') }}">Inventario Interno</a></li>
<li>Entradas</li>
<li class="active"><strong>{{ $inventario->producto->nombre }}</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
	table.footable-details>tbody>tr>th {
	    width: 210px!important;
	}
</style>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.inventario.iboxes.entradas.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.inventario.iboxes.entradas.movimientos')
		@include('sistema.inventario.iboxes.entradas.log')
	</div>
</div>

@include('sistema.inventario.modals.entradas.edit')
@include('sistema.inventario.modals.entradas.delete')
@include('sistema.inventario.modals.entradas.reactivar')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>	
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>	
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript">	

	@if(session('delete_entrada_success'))
		swal('Correcto', "Se ha eliminado la entrada satisfactoriamente.", "success");
	@endif
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	// ENTRADAS
	var entradas = [];
	@foreach($entradas as $entrada)
		entradas[{{ $entrada->id }}] = {
			tipo : '{{ $entrada->tipo }}',
			producto_id : '{{ $entrada->producto_id }}',
			producto : '{{ $entrada->producto->nombre }}',
			cantidad : '{{ $entrada->cantidad_entrada }}',
			motivo : '{{ $entrada->motivo }}',
		}
    @endforeach
	
</script>
@endsection