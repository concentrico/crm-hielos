@extends('sistema.layouts.master')

@section('title', 'Inventario Interno')

@section('attr_navlink_8', 'class="active"')
@section('attr_navlink_8_1', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', 'Inventario Interno')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Inventario Interno</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-8" style="padding: 5px;">
        @include('sistema.inventario.iboxes.tabla')
    </div>
    <div class="col-sm-4" style="padding: 5px;">
        @include('sistema.inventario.iboxes.inventario')
    </div>
</div>

@include('sistema.inventario.modals.registrar')

@endsection

@section('assets_bottom')

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>

<script type="text/javascript">
    inventario_externo = [];
    
    @foreach($inventarios as $inventario_ext)
    
        inventario_externo[{{ $inventario_ext->id }}] = {
            producto_id: '{{ $inventario_ext->producto_id }}',
            producto: '{{ $inventario_ext->producto->nombre }}',
            cantidad_disponible: '{{ $inventario_ext->cantidad_disponible }}',
            cantidad_producir: '{{ $inventario_ext->cantidad_producir }}',
            ultima_salida: '{{ $inventario_ext->ultima_salida }}',
        }
    
    @endforeach
    
    c3.generate({
        bindto: '#cuartofrio',
        data:{
            columns: [
                ['Cantidad Actual', {{ \App\Models\Inventario::where('deleted_at',null)->firstOrFail()->porcentaje_inventario() }}]
            ],
            type: 'gauge'
        },
        color:{
            pattern: ['#1ab394', '#BABABA']

        }
    });

    
    Morris.Donut({
        element: 'cuartofrio-chart',
        data: [
        @php 
            $inventarios = \App\Models\Inventario::all();
            $contador = count($inventarios);
            $c1=0;
            $c2=0;
            $array_colors = ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565',];
        @endphp
        @foreach($inventarios as $inventario)
            @if($inventario->cantidad_disponible != '0')
                { label: "{{$inventario->producto->nombre}}", value: @if($inventario->cantidad_disponible != null) {{$inventario->cantidad_disponible}} @else 0 @endif},
            @endif
        @endforeach
        ],
        resize: true,
        colors: ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565'],
        {{--
        colors: [
        @foreach($inventarios as $inventario2)
            @if( $c2 < 5)
                '{{$array_colors[$c2]}}',
            @else
                '{{getRandomColor()}}',
            @endif
        @php $c2++; @endphp
        @endforeach
        ],
        --}}
        
    });
</script>
@endsection