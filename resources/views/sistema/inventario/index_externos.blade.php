@extends('sistema.layouts.master')

@section('title', 'Inventario Externo')

@section('attr_navlink_8', 'class="active"')
@section('attr_navlink_8_2', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', 'Inventario Externo')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Inventario Externo</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.inventario.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.inventario.iboxes.tabla_externos')
	</div>
</div>

@include('sistema.inventario.modals.corte')
@include('sistema.inventario.modals.pedido')

@endsection

@section('assets_bottom')

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>

<script type="text/javascript">

	establecimientos = [];
	
	@foreach($inventario_ext as $establecimiento)
	
		establecimientos[{{ $establecimiento->id }}] = {
			sucursal: '{{ $establecimiento->sucursal }}',
			capacidad_minima: '{{ $establecimiento->capacidad_minima }}',
			capacidad_maxima: '{{ $establecimiento->capacidad_maxima }}',
			punto_reorden: '{{ $establecimiento->punto_reorden }}',
		}
	
	@endforeach

	@if(session('corte_success'))
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif

</script>
@endsection
