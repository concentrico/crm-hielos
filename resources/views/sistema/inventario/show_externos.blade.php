@extends('sistema.layouts.master')

@section('title', 'Inventario Externo')

@section('attr_navlink_8', 'class="active"')
@section('attr_navlink_8_2', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', $establecimiento->sucursal)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('inventario') }}">Inventario Externo</a></li>
<li class="active"><strong>{{ $establecimiento->sucursal }}</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
	table.footable-details>tbody>tr>th {
	    width: 210px!important;
	}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
	            
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
                		<i style="margin:0px;" class="fa fa-eye fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Detalle</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 2) active @endif">
                	<a data-toggle="tab" href="#tab-2">
                		<i style="margin:0px;" class="fa fa-snowflake-o fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Conservadores Asignados<span> 
	                </a>
	            </li>
	            <li class="@if(Input::get('active_tab') == 3) active @endif">
                	<a data-toggle="tab" href="#tab-3">
                		<i style="margin:0px;" class="fa fa-snowflake-o fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Cuarto Frío<span> 
	                </a>
	            </li>         
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12 col-md-12" style="padding: 0 5px;">
								@include('sistema.inventario.iboxes.detalle')
							</div>
							<div class="col-sm-12 col-md-12" style="padding: 0 5px;">
								@include('sistema.inventario.iboxes.pedido')
							</div>
							<div class="col-sm-12 col-md-12" style="padding: 0 5px;">
								@include('sistema.inventario.iboxes.historico_cortes')
							</div>
						</div>
                    </div>
                </div>
                
                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.inventario.iboxes.conservadores')
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.inventario.iboxes.cuartofrio')
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('sistema.inventario.modals.corte2')
@include('sistema.inventario.modals.asignar')
@include('sistema.inventario.modals.agregar_producto')
@include('sistema.inventario.modals.delete_cf')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>	
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>	
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript">	

	if( $('body').width() < 768 ) {
		$('#tfootTotalLabel').removeAttr('colspan');
		$('#tfootTotal').removeAttr('colspan');
	}

	$(".touchspin_responsive").TouchSpin({
        min: 0,
        max: 1000000000,
        verticalbuttons: true,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });

	@if(session('show_tab_conservadores'))
		$('.nav-tabs a[href="#tab-2"]').tab('show');
	@endif

	@if(session('show_tab_cuartofrio'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
	@endif

	@if(session('corte_success'))
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});


	c3.generate({
        bindto: '#nivel_conservador',
        size: {
		  height: 230
		},
        data:{
            columns: [
                ['Capacidad Actual', {{$establecimiento->porcentaje_inventario()}}]
            ],

            type: 'gauge'
        },
        color:{
            pattern: ['#1ab394', '#BABABA']

        }
    });
    
	function calcularCapacidadCorte(cap) {
		var sum = 0;
		$('.capproducto').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
	}

	function calcularSubtotalProducto(producto_id) {

		var subtotal=0.0;
		var cantidad = $('#cantidad_producto'+producto_id).val();
		var precio = $('#precio_producto'+producto_id).val();
		if( precio != '' && cantidad != '') {
            subtotal= parseFloat(cantidad) * parseFloat(precio);
        } else {
            subtotal = 0.0;
        }

        $('#subtotal_producto'+producto_id).val(numeral(subtotal.toFixed(2).toString().replace(',','.')).format('0,0.00'));
        calcularTotalPedido();
	}

	function calcularTotalPedido() {

        var total = 0.0;
        var precio;
        var cant = [];
        $(".cantidades-pedido").each(
            function(index, value) {
                cant.push( $(this).val() );
            }
        );
        var imp = [];
        $(".precios-pedido").each(
            function(index, value) {
                precio = $(this).val(); 
                imp.push( precio );
            }
        );

        for (var i = 0; i < cant.length; i++) {
          if( imp[i] != '') {
            total += parseFloat(cant[i]) * parseFloat(imp[i]);

          } else {
            total += 0.0;
          }
          
        };

        $("#total_pedido").html(numeral(total.toFixed(2).toString().replace(',','.')).format('$0,0.00'));
    }

	function generarPedido() {
		swal({
		  title: "¿Desea generar el Pedido?",
		  text: "Ésta acción te redirigirá a la pantalla de Alta de Pedidos.",
		  type: "info",
		  showCancelButton: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){
			$("#formPedido").removeAttr('onsubmit');
			$("#formPedido").submit();
		});
	}
	
	var establecimiento = {
		id : '{{ $establecimiento->id }}',
		sucursal : '{{ $establecimiento->sucursal }}',
		cuartofrio_id : '{{ $establecimiento->cuartofrio_id }}',
		capacidad_minima: '{{ $establecimiento->capacidad_minima }}',
		capacidad_maxima: '{{ $establecimiento->capacidad_maxima }}',
		punto_reorden: '{{ $establecimiento->punto_reorden }}',
	};
</script>
@endsection