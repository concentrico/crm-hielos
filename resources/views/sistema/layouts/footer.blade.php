<div class="footer fixed">
    <div class="text-center text-bold">
        &copy; {{date('Y')}} @if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | Todos los derechos reservados.
    </div>
</div>
