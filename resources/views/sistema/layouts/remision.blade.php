<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
	</head>

	<body style="padding: 0px; margin: 0px; background: transparent;">
		<div class="ibox-content" style="padding: 0px; border: transparent solid 0px;">

			<div class="table-responsive" style="border: transparent solid 0px;">
				<table class="table invoice-table" style="width:100%; border: transparent solid 0px;">
					<tbody style="border: transparent solid 0px;">
						<tr style="border: transparent solid 0px;">
							<td style="width: 20%; border: transparent solid 0px;">
								@if(getSystemSettings()->site_logo != null) 
			                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="100">
			                    @else 
			                        <img src="{{ asset('img/logo-default.png') }}" alt="Logo" height="100">
			                    @endif
							</td>

							<td style="width: 50%; border: transparent solid 0px; text-align: center;">
							</td>

							<td style="width: 30%; border: transparent solid 0px;">
								<div>
									<small style="font-size: 11px;">{{ fecha_to_human($pedido->updated_at,false) }}</small>
									<h4 style="padding-bottom: 0px; margin-bottom: 0px;">PEDIDO NO.</h4>
									<h4 class="text-success">{{ str_pad($pedido->id, 5, 0, STR_PAD_LEFT) }}</h4>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="table-responsive" style="border: transparent solid 0px;">
				<table class="table invoice-table" style="width:100%; border: transparent solid 0px;">
					<tbody style="border: transparent solid 0px;">
						<tr>
							<td style="width: 50%; border: transparent solid 0px; text-align: left;">
								<span><strong>Cliente:</strong> <strong class="text-success">{{ $pedido->cliente->nombre_corto }}</strong></span><br />
								<span><strong>Establecimiento:</strong> <strong class="text-success"> @if($pedido->cliente_establecimiento_id != null){{ $pedido->establecimiento->sucursal }} @else N.A. @endif</strong></span><br />
								<strong>Estatus:</strong> <br/>
								@php
		                    		switch ($pedido->status) {
		                    			case '1':
		                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">Programado</span>';
		                    				break;
		                    			case '2':
		                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">En Camino</span>';
		                    				break;
		                    			case '3':
		                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Entregado</span>';
		                    				break;
		                    			case '4':
		                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Devolución</span>';
		                    				break;
		                    			case '5':
		                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Cancelado</span>';
		                    				break;
		                    			default:
		                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>';
		                    				break;
		                    		}
		                    	@endphp
							</td>
							<td style="width: 50%; border: transparent solid 0px; text-align: right;">
									@if($pedido->entregado_at != null)
										<span><strong>Entregado:</strong> {{ fecha_to_human($pedido->entregado_at,true) }}</span><br>
									@endif

									@if($pedido->evento_calle != null || $pedido->cliente_establecimiento_id == null)
										<address>
										@if($pedido->evento_calle)
										{{ $pedido->evento_calle }}
										@endif
										@if($pedido->evento_numero_exterior)
										No. {{ $pedido->evento_numero_exterior }} @if($pedido->evento_numero_interior) - {{ $pedido->evento_numero_interior }} @endif
										@endif
										<br/>
										@if($pedido->evento_colonia)
										{{ $pedido->evento_colonia }}
										@endif
										@if($pedido->evento_codigo_postal)
										, C.P. {{ $pedido->evento_codigo_postal }}
										@endif
										<br />
										@if($pedido->evento_municipio)
										{{ $pedido->evento_municipio }},
										@endif
										{{ $pedido->evento_estado }}, México<br />
										</address><br />
									@else
										<address>
										@if($pedido->establecimiento->calle)
										{{ $pedido->establecimiento->calle }} &nbsp;
										@endif
										@if($pedido->establecimiento->numero_exterior)
										No. {{ $pedido->establecimiento->numero_exterior }} @if($pedido->establecimiento->numero_interior) - {{ $pedido->establecimiento->numero_interior }} @endif 
										<br/>
										@endif
										@if($pedido->establecimiento->colonia)
										{{ $pedido->establecimiento->colonia }}
										@endif
										@if($pedido->establecimiento->codigo_postal)
										, C.P. {{ $pedido->establecimiento->codigo_postal }}
										@endif
										<br />
										@if($pedido->establecimiento->municipio)
										{{ $pedido->establecimiento->municipio }},
										@endif
										{{ $pedido->establecimiento->estado }}, México<br />
										</address><br />
									@endif
							</td>

						</tr>
					</tbody>
				</table>
			</div>
			@if($pedido->status == 4)
				<div class="row">
					<div class="col-sm-12 text-center">
						<h2 align="center" class="text-center" style="color: #333;">DEVOLUCIÓN</h2>
					</div>
				</div>
			@endif
			<div class="table-responsive m-t">
				<table class="table invoice-table" style="width:100%">
				<thead>
				<tr style="background: #2e368e; color: white">
					<th>Producto</th>
					<th>Cantidad Requerida</th>
					<th>Cantidad Entregada</th>
					<th style="text-align: center;">Precio</th>
					<th style="text-align: center;">IVA</th>
					<th>Total</th>
				</tr>
				</thead>
				<tbody>

				@foreach($pedido->productos as $pedido_productos)
					
					<tr style="border: transparent solid 0px;">
					<td style="text-align:left; border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">{{ $pedido_productos->producto->nombre }}</td>

					<td style="text-align:center; border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">{{ $pedido_productos->cantidad_requerida}}</td>

					<td style="text-align:center; border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">
						@if($pedido_productos->cantidad_recibida != null)
							<b>{{ $pedido_productos->cantidad_recibida}}</b>
						@else
							N.D.
						@endif
					</td>

					<td @if($pedido_productos->subtotal == 0.00) class="text-info" @endif style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">
						@if($pedido_productos->precio_unitario == 0.00)
							Cortesía 
						@else
							${{ number_format($pedido_productos->precio_unitario, 2, '.', ',') }}
						@endif
					</td>

					<td style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">{{ ($pedido_productos->producto->iva*100) }}%</td>

					<td style="text-align:right; border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px; @if($pedido_productos->subtotal == 0.00) text-decoration: line-through; @endif">
							${{ number_format($pedido_productos->subtotal, 2, '.', ',') }}
					</td>
				</tr>

				@endforeach

				@if($pedido->iva != null)
					<tr style="border: transparent solid 0px;">
					<td colspan="5" style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;text-align: right;"><div><strong>SUBTOTAL</strong></div></td>
					<td style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;"><b>${{ number_format($pedido->subtotal, 2, '.', ',') }}</b></td>
					</tr>
					<tr style="border: transparent solid 0px;">
					<td colspan="5" style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;text-align: right;"><div><strong>IVA</strong></div></td>
					<td style="border-bottom: #ccfdff solid 0px; border-top: #d89f00 solid 0px; padding-top: 2px; padding-bottom: 2px;">${{ number_format($pedido->iva, 2, '.', ',') }}</td>
					</tr>
				@endif

				@if( count($pedido->pagos) > 0 )
					<tr style="background: #2e368e; color: white">
						<td colspan="5" style="text-align: right; border-top: #d89f00 solid 0px;"><div><strong>ABONO/ANTICIPO:</strong></div></td>
						<td style="border-top: #2e368e solid 0px;">${{ number_format($pedido->pagos->sum('monto'), 2, '.', ',') }}</td>
					</tr>
				@endif
					<tr style="background: #2e368e; color: white">
					<td colspan="5" style="text-align: right; border-top: #d89f00 solid 0px;"><div><strong>TOTAL</strong></div></td>
					@if($pedido->status == 4)
						<td style="border-top: #2e368e solid 0px; text-decoration: line-through;"><b>${{ number_format($pedido->total - $pedido->pagos->sum('monto'), 2, '.', ',') }}</b></td>
					@else
						<td style="border-top: #2e368e solid 0px;"><b>${{ number_format($pedido->total - $pedido->pagos->sum('monto'), 2, '.', ',') }}</b></td>
					@endif
					</tr>
				</tbody>
				</table>
			</div>

			<BR/><BR/>
			<div class="table-responsive m-t" style="border: transparent solid 0px;">
				<table class="table invoice-table" style="width:100%; border: transparent solid 0px;">
					<tbody style="border: transparent solid 0px;">
						<tr>
							<td style="width: 20%; border: transparent solid 0px; text-align: left;">

								&nbsp;
								
							</td>

							<td style="width: 80%; border: transparent solid 0px; text-align: right;">

								<span><strong>Chofer Asignado:</strong> <strong class="text-success">{{$pedido->chofer->nombre}}</strong></span><br />

								<span><strong>Recibido por:</strong> <strong class="text-success">
								@if($pedido->firmado_por != null)
									{{$pedido->cliente_firma->nombre.' '.$pedido->cliente_firma->apellido_paterno.' '.$pedido->cliente_firma->apellido_materno}}
								@else
									{{ $pedido->firmado_otro }}
								@endif
								</strong></span><br />

								<span><strong>Firma de Recibido:</strong><br/>
								@if($pedido->firma_path != null)
				            		<img width="150" src="{{url($pedido->firma_path)}}" />
								@else
									Sin firma
								@endif
							</td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>