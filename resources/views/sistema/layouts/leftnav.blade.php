<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    @if(getSystemSettings()->site_logo != null) 
                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="90">
                    @else 
                        <img src="{{ asset('img/logo-large-default.png') }}" alt="Logo" height="90">
                    @endif
                </div>

                <div class="logo-element">
                    @if(getSystemSettings()->site_logo != null) 
                        <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" width="30">
                    @else 
                        <img src="{{ asset('img/logo-default.png') }}" alt="Logo" width="30">
                    @endif
                </div>
            </li>

            @can('manage-users')

            <li @yield('attr_navlink_1')>
                <a href="{{ url('home') }}"><i class="fa fa-lg fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            
            <li @yield('attr_navlink_3')>
                <a href="{{ url('clientes') }}"><i class="fa fa-lg fa-address-book-o"></i> <span class="nav-label">Clientes</span></a>
            </li>

            <li @yield('attr_navlink_4')>
                <a href="{{ url('establecimientos') }}"><i class="fa fa-lg fa-map"></i> <span class="nav-label">Establecimientos</span></a>
            </li>

            <li @yield('attr_navlink_5')>
                <a href="{{ url('conservadores') }}"><i class="fa fa-lg fa-snowflake-o"></i> <span class="nav-label">Conservadores</span></a>
            </li>
            
            <li @yield('attr_navlink_6')>
                <a href="{{url('choferes')}}"><i class="fa fa-lg fa-truck"></i> <span class="nav-label">Choferes</span></a>
            </li>

            <li @yield('attr_navlink_7')>
                <a href="{{ url('productos') }}"><i class="fa fa-lg fa-cubes"></i> <span class="nav-label">Productos</span></a>
            </li>

            <li @yield('attr_navlink_8')>
                <a href="{{ url('inventario') }}"><i class="fa fa-lg fa-list-alt"></i> <span class="nav-label">Inventario</span></a>
            </li>
            
            <li @yield('attr_navlink_9')>
                <a href="{{ url('pedidos') }}"><i class="fa fa-lg fa-clipboard"></i> <span class="nav-label">Pedidos</span></a>
            </li>

            <li @yield('attr_navlink_10')>
                <a href="{{ url('cobranza') }}"><i class="fa fa-lg fa-money"></i> <span class="nav-label">Cobranza</span></a>
            </li>

             <li @yield('attr_navlink_11')>
                <a href="{{ url('reportes') }}"><i class="fa fa-lg fa-download"></i> <span class="nav-label">Reportes</span></a>
            </li>

            <li @yield('attr_navlink_2')>
                <a href="{{ url('usuarios') }}"><i class="fa fa-lg fa-user"></i> <span class="nav-label">Usuarios</span></a>
            </li>

            <li @yield('attr_navlink_12')>
                <a href="{{ url('parametros') }}"><i class="fa fa-lg fa-cogs"></i> <span class="nav-label">Configuraciones</span></a>
            </li>
            @endcan

             <!--Gerente de Distribucion-->
            @can('manage-distribucion')
            <li @yield('attr_navlink_1')>
                <a href="{{ url('home') }}"><i class="fa fa-lg fa-home"></i> <span class="nav-label">Home</span></a>
            </li>

            <li @yield('attr_navlink_3')>
                <a href="{{ url('clientes') }}"><i class="fa fa-lg fa-address-book-o"></i> <span class="nav-label">Clientes</span></a>
            </li>

            <li @yield('attr_navlink_4')>
                <a href="{{ url('establecimientos') }}"><i class="fa fa-lg fa-map"></i> <span class="nav-label">Establecimientos</span></a>
            </li>

            <li @yield('attr_navlink_5')>
                <a href="{{ url('conservadores') }}"><i class="fa fa-lg fa-snowflake-o"></i> <span class="nav-label">Conservadores</span></a>
            </li>

            <li @yield('attr_navlink_6')>
                <a href="{{url('choferes')}}"><i class="fa fa-lg fa-truck"></i> <span class="nav-label">Choferes</span></a>
            </li>

            <li @yield('attr_navlink_7')>
                <a href="{{ url('productos') }}"><i class="fa fa-lg fa-cubes"></i> <span class="nav-label">Productos</span></a>
            </li>

            <li @yield('attr_navlink_8')>
                <a href="{{ url('inventario') }}"><i class="fa fa-lg fa-list-alt"></i> <span class="nav-label">Inventarios</span></a>
                <ul class="nav nav-second-level collapse">
                    <li @yield('attr_navlink_8_1')><a href="{{ url('inventario') }}"><i class="fa fa-external-link-square rotate180"></i>Interno</a></li>
                    <li @yield('attr_navlink_8_2')><a href="{{ url('inventario/externo') }}"><i class="fa fa-external-link-square"></i> Externo</a></li>
                </ul>
            </li>

            <li @yield('attr_navlink_9')>
                <a href="{{ url('pedidos') }}"><i class="fa fa-lg fa-clipboard"></i> <span class="nav-label">Pedidos</span></a>
            </li>

            <li @yield('attr_navlink_10')>
                <a href="{{ url('cobranza') }}"><i class="fa fa-lg fa-money"></i> <span class="nav-label">Cobranza</span></a>
            </li>

            <li @yield('attr_navlink_11')>
                <a href="{{ url('reportes') }}"><i class="fa fa-lg fa-download"></i> <span class="nav-label">Reportes</span></a>
            </li>
            
            @endcan
             <!--fin del gerente Distribucion-->

            <!--Gerente de Produccion-->
            @can('manage-produccion')

            <li @yield('attr_navlink_1')>
                <a href="{{ url('home') }}"><i class="fa fa-lg fa-dashboard"></i> <span class="nav-label">Home</span></a>
            </li>

            {{--
            <li @yield('attr_navlink_8')>
                <a href="{{ url('inventario') }}"><i class="fa fa-lg fa-list-alt"></i> <span class="nav-label">Inventarios</span></a>
                <ul class="nav nav-second-level collapse">
                    <li @yield('attr_navlink_2_1')><a href="{{ url('inventario') }}"><i class="fa fa-external-link-square rotate180"></i>Interno</a></li>
                    <li @yield('attr_navlink_2_2')><a href="{{ url('inventario/externo') }}"><i class="fa fa-external-link-square"></i> Externo</a></li>
                </ul>
            </li>
            
            --}}

            @endcan
             <!--fin de Produccion-->
            @can ('manage-choferes')
            
            <li @yield('attr_navlink_2')>
                <a href="{{ url('choferes') }}"><i class="fa fa-lg fa-truck"></i> <span class="nav-label">Entregas</span></a>
            </li>
            @endcan
        </ul>

    </div>
</nav>