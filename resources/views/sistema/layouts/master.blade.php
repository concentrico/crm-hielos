<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

    <title>@if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | @yield('title') </title>

	@yield('assets_top_top')

	<link href="{{ asset('select2-4.0.3/dist/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/footable/footable.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
	<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/ui-transition/transition.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/plugins/ui-dropdown/dropdown.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/chosen/chosen.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
	
	@yield('assets_top_bottom')
	
</head>

<body class="body-small mini-navbar">
	<div id="wrapper">
	
		@include('sistema.layouts.leftnav')
	
	    <div id="page-wrapper" class="gray-bg">
		    
		    @include('sistema.layouts.topnav')
	
		    @include('sistema.layouts.breadcrumb')
	        
	        <div class="wrapper wrapper-content animated fadeIn">
				<div class="row">
	            	<div class="col-lg-12">
		            	@yield('content')
		            </div>
				</div>
	        </div>
	
			@include('sistema.layouts.footer')
			
	    </div>
	</div>

	<!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
	<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	
	<!-- Custom and plugin javascript -->
	<script src="{{ asset('js/inspinia.js') }}"></script>
	<script src="{{ asset('js/jquery.peity.js') }}"></script>
	<script src="{{ asset('js/jquery.numeric.min.js') }}"></script>
	<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
	<script src="{{ asset('select2-4.0.3/dist/js/select2.min.js') }}"></script>	
	<script src="{{ asset('js/plugins/footable/footable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/plugins/typehead/bootstrap3-typeahead.min.js') }}"></script>
	<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/plugins/touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('js/plugins/numeral/numeral.min.js') }}"></script>
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
	<script src="{{ asset('js/plugins/ui-transition/transition.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ui-dropdown/dropdown.min.js') }}"></script>
    <script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

	
	<script type="text/javascript">

		$('[data-toggle="tooltip"]').tooltip(); 

		$('form').submit(function(){
		    $(this).find('button[type=submit]').prop('disabled', true);
		    $(this).find('input[type=submit]').prop('disabled', true);
		});
	
		if( $('body').width() < 768 ) {
			//$('body').removeClass('mini-navbar');
			$('.filtros-res').addClass('collapsed');
		}
		
		$(".select2").select2({
			allowClear : true,
			placeholder : 'Seleccione'
		});

		$('.selectdp').dropdown();
		
		$('.table-res').footable({
			"breakpoints": {
				"xs": 480, // extra small
				"sm": 768, // small
				"md": 992, // medium
				"lg": 1200, // large
				"xlg": 1600, // large
				"xxlg": 1900 // large
			}
		});

		$("span.pie").peity("pie");
		$("span.donut").peity("donut")

		$(".numeric").numeric();
		$(".integer").numeric(false, function() { alert("Integers only"); this.value = ""; this.focus(); });
		$(".positive").numeric({ negative: false }, function() { alert("No negative values"); this.value = ""; this.focus(); });
		$(".positive-integer").numeric({ decimal: false, negative: false }, function() { return false; this.value = ""; this.focus(); });
	    $(".decimal-2-places").numeric({ decimalPlaces: 2, negative: false });

	    $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $(".touchspin_porcentaje").TouchSpin({
	        min: 0,
	        max: 100,
	        step: 1,
	        decimals: 2,
	        boostat: 5,
	        maxboostedstep: 10,
	        postfix: '%',
	        buttondown_class: 'btn btn-white',
	        buttonup_class: 'btn btn-white'
	    });

	    $(".touchspin_cantidad").TouchSpin({
	        min: 1,
	        max: 1000000000,
	        step: 1,
	        decimals: 0,
	        boostat: 5,
	        maxboostedstep: 10,
	        buttondown_class: 'btn btn-white',
	        buttonup_class: 'btn btn-white'
	    });

	    toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "progressBar": true,
		  "preventDuplicates": false,
		  "positionClass": "toast-bottom-right",
		  "onclick": null,
		  "showDuration": "400",
		  "hideDuration": "1000",
		  "timeOut": "7000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		$(".chosen-select").chosen({
          width: '100%',
          disable_search_threshold: 10,
          no_results_text:'No se encontraron resultados',
          allow_single_deselect: true
        });

	</script>
	@yield('assets_bottom')
	
</body>
</html>