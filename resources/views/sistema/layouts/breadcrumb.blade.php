<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>@yield('breadcrumb_title')</h2>
		<ol class="breadcrumb">
			@yield('breadcrumb_content')
		</ol>
	</div>
	<div class="col-lg-2">
	
	</div>
</div>
