<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-default" href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>

                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="m-t-xs"> <strong class="font-bold">{{ ucfirst(Auth::user()->nombre) }}</strong>
                         </span> <span class="text-muted text-xs">{{ ucfirst(Auth::user()->rol) }}<b class="caret"></b></span> </span> 
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="{{ url('perfil') }}">Perfil</a></li>
                    <li><a href="{{ url('logout') }}">Cerrar Sesión</a></li>
                </ul>

            </li>
        </ul>
    </nav>
</div>