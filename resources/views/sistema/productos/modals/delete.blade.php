<div class="modal inmodal" id="modalProductoDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar Producto</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            <h3>¿Estas seguro de eliminar éste producto?</h3>
				            <h4 class="text-info">El productó también se dará de baja de <b>INVENTARIO</b>, pero no se verán afectados conservadores y/o pedidos.</h4>
				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'DELETE',  'url' => url('productos' )] ) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalProductoDelete(id){

		if ( $('#modalProductoDelete').find('form').attr('action') == "{{ url('productos') }}/" + id){

		}else{
			$('#modalProductoDelete').find('form').attr('action', "{{ url('productos') }}/" + id)

			$('#modalProductoDelete').find('h5').html(productos[id]['nombre']);
		}

		$('#modalProductoDelete').modal('show');
	}

</script>
