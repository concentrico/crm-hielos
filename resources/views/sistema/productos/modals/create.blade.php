<div class="modal inmodal" id="modalProductosCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-cubes modal-icon"></i>Agregar Nuevo Producto</h4>
			</div>
	{!! Form::open(['method' => 'POST',  'url' => url('productos')] ) !!}
		<div class="modal-body">
			<div class="row">
				<div class="col-sm-12"> 
				    <div class="row">
				    	<div class="form-group col-sm-3 @if ($errors->first('id_codigo') !== "") has-error @endif">
				            {!! Form::label('id_codigo', 'ID Sistema *', ['class' => 'control-label']) !!}
							   <div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-link"></i></span>
				            {!! Form::text('id_codigo', null, ['class' => 'form-control', 'placeholder' => 'ID Sistema']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('id_codigo')}}</span>
				        </div>
						<div class="form-group col-sm-6 @if ($errors->first('nombre') !== "") has-error @endif">
							{!! Form::label('nombre', 'Nombre del Producto *', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
						           	<span class="input-group-addon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
									{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre Producto', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
								</div>
									<span class="help-block">{{ $errors->first('nombre')}}</span>
						</div>
						<div class="form-group col-sm-3 @if ($errors->first('peso') !== "") has-error @endif">
							{!! Form::label('peso', 'Peso (Kilos) *', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
						           	<span class="input-group-addon"><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
									{!! Form::text('peso', null, ['class' => 'form-control decimal-2-places','placeholder' => '0.00', 'style' => 'max-width:100%;']) !!}
								</div>
								<span class="help-block">{{ $errors->first('peso')}}</span>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
				    <div class="row">
				    	<div class="form-group col-sm-3 @if ($errors->first('tipo') !== "") has-error @endif">
 							{!! Form::label('tipo', 'Tipo *', ['class' => 'control-label']) !!}
 							{!!Form::select('tipo', ['producto' => 'Producto','servicio' => 'Servicio'], null, ['class' => 'form-control selectdp'])!!}
							<span class="help-block">{{ $errors->first('tipo')}}</span>
						</div>
						<div class="form-group col-sm-3 @if ($errors->first('es_hielo') !== "") has-error @endif">
							{!! Form::label('es_hielo', 'Es Hielo', ['class' => 'control-label']) !!}
							<div class="col-sm-12 form-control" style="background: none; border:0px;">
								<div class="radio radio-info radio-inline">
	                                <input type="radio" id="hielo_si" value="1" name="es_hielo" checked="">
	                                <label for="hielo_si"> Sí </label>
	                            </div>
	                            <div class="radio radio-inline">
	                                <input type="radio" id="hielo_no" value="0" name="es_hielo">
	                                <label for="hielo_no"> No </label>
	                            </div>
	                        </div>
							<span class="help-block">{{ $errors->first('es_hielo')}}</span>
						</div>
						<div class="form-group col-sm-3 @if ($errors->first('precio_venta') !== "") has-error @endif">
							{!! Form::label('precio_venta', 'Precio Venta Default *', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
						           	<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
						           	{!! Form::text('precio_venta', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
								</div>
								<span class="help-block">{{ $errors->first('precio_venta')}}</span>
						</div>
						<div class="form-group col-sm-3 @if ($errors->first('iva') !== "") has-error @endif">
							{!! Form::label('iva', 'IVA *', ['class' => 'control-label']) !!}
							<input class="touchspin_porcentaje decimal-2-places" type="text"  name="iva" placeholder="16.00">
							<span class="help-block">{{ $errors->first('iva')}}</span>
						</div>
					</div>
				</div>
				<div class="form-group col-sm-12 @if ($errors->first('comentarios') !== "") has-error @endif">
				    {!! Form::label('comentarios', 'Comentarios', ['class' => 'control-label']) !!}
				        <div class="input-group" style="width: 100%;">
						    <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
						    {!! Form::textarea('comentarios', null, ['id' => 'comentarios',  'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				        </div>
				        <span class="help-block">{{ $errors->first('comentarios')}}</span>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
		</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent

	<script type="text/javascript">
		@if(session('show_modal_producto_create'))
			$('#modalProductosCreate').modal('show');
		@endif
	</script>	
@endsection 