<div class="modal inmodal" id="modalProductoEdit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title "><i class="fa fa-lg fa-cubes"></i> Editar Producto</h4>
			</div>
				{!! Form::open(['method' => 'PUT',  'url' => url('productos')] ) !!}

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
				        <div class="row">
				        	<div class="form-group col-sm-3 @if ($errors->first('id_codigo_edit') !== "") has-error @endif">
					            {!! Form::label('id_codigo_edit', 'ID Sistema *', ['class' => 'control-label']) !!}
								   <div class="input-group" style="width: 100%;">
									<span class="input-group-addon"><i class="fa fa-link"></i></span>
					            {!! Form::text('id_codigo_edit', null, ['class' => 'form-control', 'placeholder' => 'ID Sistema']) !!}
					            </div>
					            <span class="help-block">{{ $errors->first('id_codigo_edit')}}</span>
					        </div>
				            <div class="form-group col-sm-6 @if ($errors->first('nombre_edit') !== "") has-error @endif">
								{!! Form::label('nombre_edit', 'Nombre del Producto *', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
					           		<span class="input-group-addon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
									{!! Form::text('nombre_edit', null, ['class' => 'form-control', 'placeholder' => 'producto', 'style' => 'max-width:100%;']) !!}
								</div>
								<span class="help-block">{{ $errors->first('nombre_edit')}}</span>
							</div>
							<div class="form-group col-sm-3 @if ($errors->first('peso_edit') !== "") has-error @endif">
								{!! Form::label('peso_edit', 'Peso (Kilos) *', ['class' => 'control-label']) !!}
								<div class="input-group" style="width: 100%;">
					           	<span class="input-group-addon"><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
								{!! Form::text('peso_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => 'kilos']) !!}
								</div>
								<span class="help-block">{{ $errors->first('peso_edit')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					    <div class="row">
					    	<div class="form-group col-sm-3 @if ($errors->first('tipo_edit') !== "") has-error @endif">
	 							{!! Form::label('tipo_edit', 'Tipo *', ['class' => 'control-label']) !!}
	 							{!!Form::select('tipo_edit', ['producto' => 'Producto','servicio' => 'Servicio'],null, ['id' => 'tipo_edit','class' => 'form-control selectdp'])!!}
								<span class="help-block">{{ $errors->first('tipo_edit')}}</span>
							</div>
							<div class="form-group col-sm-3 @if ($errors->first('es_hielo_edit') !== "") has-error @endif">
								{!! Form::label('es_hielo_edit', 'Es Hielo', ['class' => 'control-label']) !!}
								<div class="col-sm-12 form-control" style="background: none; border:0px;">
									<div class="radio radio-info radio-inline">
		                                <input type="radio" id="hielo_si" value="1" name="es_hielo_edit" checked="">
		                                <label for="hielo_si"> Sí </label>
		                            </div>
		                            <div class="radio radio-inline">
		                                <input type="radio" id="hielo_no" value="0" name="es_hielo_edit">
		                                <label for="hielo_no"> No </label>
		                            </div>
		                        </div>
								<span class="help-block">{{ $errors->first('es_hielo_edit')}}</span>
							</div>
							<div class="form-group col-sm-3 @if ($errors->first('precio_venta_edit') !== "") has-error @endif">
								{!! Form::label('precio_venta_edit', 'Precio Venta Default *', ['class' => 'control-label']) !!}
									<div class="input-group" style="width: 100%;">
							           	<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
							           	{!! Form::text('precio_venta_edit', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
									</div>
									<span class="help-block">{{ $errors->first('precio_venta_edit')}}</span>
							</div>
							<div class="form-group col-sm-3 @if ($errors->first('iva_edit') !== "") has-error @endif">
								{!! Form::label('iva_edit', 'IVA *', ['class' => 'control-label']) !!}
								<input class="touchspin_porcentaje decimal-2-places" type="text" name="iva_edit" placeholder="16.00">
								<span class="help-block">{{ $errors->first('iva_edit')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
			         		<div class="form-group col-sm-12 @if ($errors->first('comentarios_edit') !== "") has-error @endif">
				            	{!! Form::label('comentarios_edit', 'Comentarios', ['class' => 'control-label']) !!}
				            	<div class="input-group" style="width: 100%;">
						           	<span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
				            	{!! Form::textarea('comentarios_edit', null, ['id' => 'comentarios_edit', 'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            	</div>
				            	<span class="help-block">{{ $errors->first('comentarios_edit')}}</span>
				       		</div>
		           		 </div>
	           		</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_producto_edit'))
		showModalProductoEdit({{ Input::old('producto_id') }} );
	@endif

	function showModalProductoEdit(id){

		if ( $('#modalProductoEdit').find('form').attr('action') == "{{ url('productos') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.
		}else{
			$('#modalProductoEdit').find('form').attr('action', "{{ url('productos') }}/" + id )
			$('#modalProductoEdit input[name="id_codigo_edit"]').val(productos[id]['id_codigo']);
			$('#modalProductoEdit input[name="nombre_edit"]').val(productos[id]['nombre']);
			$('#modalProductoEdit select[name="tipo_edit"]').dropdown("set selected", productos[id]['tipo']);

			$('#modalProductoEdit input[name="precio_venta_edit"]').val(productos[id]['precio_venta']);
			$('#modalProductoEdit input[name="peso_edit"]').val(productos[id]['peso']);
			$('#modalProductoEdit input[name="iva_edit"]').val((productos[id]['iva']*100).toFixed(2));
			$('#modalProductoEdit input[name="comentarios_edit"]').val(productos[id]['comentarios']);

			if (productos[id]['es_hielo'] == 1) {
				$('#modalProductoEdit input[name="es_hielo_edit"]').val('1');
				$('#hielo_no').removeAttr('checked');
				$('#hielo_si').attr('checked','checked');
			} else {
				$('#modalProductoEdit input[name="es_hielo_edit"]').val('0');
				$('#hielo_si').removeAttr('checked');
				$('#hielo_no').attr('checked','checked');
			}
		}

		$('#modalProductoEdit').modal('show');
	}

</script>
@endsection