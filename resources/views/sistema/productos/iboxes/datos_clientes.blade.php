<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-briefcase"></i> Clientes Asociados</h5>
    </div>
    <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">
            <div class="row" >
                <div class="col-sm-12">
                    @php
                    $clientes_array = array_unique(\App\Models\EstablecimientoPrecios::where('producto_id',$producto->id)->pluck('cliente_id')->toArray()); 
                    @endphp 
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th data-type="html">Clientes</th>
                                <th data-type="html">Precio Venta Promedio</th>
                            </tr>
                        </thead>
                        @foreach ($clientes_array as $cliente_id)
                        @php 
                            $cliente = App\Models\Cliente::find($cliente_id);
                        @endphp
                        <tr>
                           <td> {{ $cliente->nombre_corto}} </td>
                           <td>$ {{number_format( collect( $cliente->precios()->pluck('precio_venta')->toArray())->median() ,2)}} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
    </div>
</div>