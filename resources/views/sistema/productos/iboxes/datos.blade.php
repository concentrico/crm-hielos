<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-eye"></i> Datos del Producto </h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalProductoEdit">
			<i class="fa fa-edit"></i> Editar
		</a>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">
	    <div class="row">
		    <div class="col-sm-12">
			    <h3 class="text-info">{{ $producto->nombre }}</h3>
		    </div>
	    </div>

		<div class="row">
			<div class="col-sm-4">
				<h4 class="text-success">ID Sistema</h4>
			    {{ $producto->id_codigo }}
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Tipo</h4>
				@if($producto->tipo == 'producto')
					<span class="label label-warning">Producto</span>
				@elseif($producto->tipo == 'servicio')
					<span class="label label-danger">Servicio</span>
				@else
					<span class="label label-default">N.D.</span>
				@endif
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">¿Es Hielo?</h4>
				@if($producto->es_hielo == '1')
					Sí
				@else
			    	No
			    @endif
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
		    <div class="col-sm-4">
				<h4 class="text-success">Precio Venta Default</h4>
			    $ {{ number_format($producto->precio_venta,2) }}
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">IVA</h4>
			    {{ $producto->iva*100 }} %
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Peso</h4>
			    {{ $producto->peso }} Kg
		    </div>
	    </div>
	    <BR/>
	    <h4 class="text-success">Comentarios</h4>
	    <div class="row">
			@if($producto->comentarios)
				<div class="col-sm-12">
					<span>{!! nl2br(e($producto->comentarios))!!}</span>
				</div>
			@else
				<div class="col-sm-12">
                	No hay comentarios.
                </div>
			@endif
		</div>
		<hr />
	    <div class="row">
		    <div class="col-sm-6">
				<small class="text-muted">Creado: {{ $producto->created_at }} <br /> 
				<a href="{{ url('productos/' . $producto->created_by) }}">{{ $producto->creado_por->nombre }}</a></small><br />
		    </div>
		    	    <div class="col-sm-6">
				<small class="text-muted">Actualizado: {{ $producto->updated_at }} <br /> 
				<a href="{{ url('usuarios/' . $producto->updated_by) }}">{{ $producto->actualizado_por->nombre }}</a></small><br />
		    </div>
	    </div>
    </div>
</div>
