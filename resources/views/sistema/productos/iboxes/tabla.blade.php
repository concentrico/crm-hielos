<div class="ibox float-e-margins"> 
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalProductosCreate">
			<i class="fa fa-plus"></i> Producto
		</a>
        <h5><i class="fa fa-list"></i> Lista de Productos</h5>
    </div>
<div class="ibox-content">

	@if(count($productos) > 2)
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $productos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
	@endif

	<div>
	@if(count($productos) == 2)

	        	<div class="row" style="padding: 50px;">
	        		@foreach ($productos as $producto)
		                <div class="col-lg-6">
		                    <div class="ibox float-e-margins" style="border: 3px solid #e7eaec; border-radius: 5px;">
		                        <div class="ibox-title" style="border: 0px;">

		                            <h5>Tipo:</h5>

		                            @if($producto->tipo == 'producto')
										<span class="label label-success">Producto</span>
									@elseif($producto->tipo == 'servicio')
										<span class="label label-info">Servicio</span>
									@else
										<span class="label label-default">N.D.</span>
									@endif
		                        </div>
		                        <div class="ibox-content">
		                        	
		                            <h1 class="no-margins text-center">
		                            	<a href="{{ url('productos/'.$producto->id) }}">{{ $producto->nombre }}</a>
		                            </h1>

		                            <div class="row" style="margin-top: 1em; margin-left: 0px; margin-right: 0px;">
		                            	<h3 class="text-primary">Precio de Venta Default:
			                            	<div class="stat-percent font-bold text-info">
					                        	${{ number_format( $producto->precio_venta ,2) }}
				                            </div>
		                            	</h3>
		                            </div>
		                            
		                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
		                            	<h3 class="text-primary">IVA: 
				                            <div class="stat-percent font-bold text-info">
					                            {{ $producto->iva*100 }} %
				                            </div>
				                        </h3>
		                            </div>

		                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
		                            	<h3 class="text-primary">Es Hielo:
				                            <div class="stat-percent font-bold text-info">
					                            @if($producto->es_hielo == '1')
													Sí
												@else
											    	No
											    @endif
				                            </div>
				                        </h3>
		                            </div>


		                            <div class="row center text-center" style="margin-top: 1em;">
		                            	@if($producto->deleted_at != NULL)
								        	<button class="btn btn-primary" onclick="showModalProductoReactivar({{ $producto->id }});">
								        	<i class="fa fa-refresh"></i> Re-Activar
								        	 </button>
							            @else
							                <button class="btn btn-primary" onclick="showModalProductoEdit({{ $producto->id }});">
								            <i class="fa fa-edit"></i> Editar
								            </button>
								            <button class="btn btn-danger" onclick="showModalProductoDelete({{ $producto->id }});">
								            <i class="fa fa-trash"></i> Eliminar
								            </button>
							            @endif
		                            </div>
		                            
		                        </div>
		                    </div>
		                </div>
		            @endforeach
	            </div>

	@else
		<table id="dataTableConservadores" name="dataTableConservadores" class="table table-res" data-cascade="true" data-expand-first="false">
			<thead>
				<tr>
					<th data-type="html" style="min-width: 75px;">{!! getHeaderLink('IDCODIGO', 'ID') !!}</th>
					<th data-type="html">{!! getHeaderLink('PRODUCTO', 'Producto') !!}</th>
					<th data-type="html">{!! getHeaderLink('PREMIN', 'Precio de Venta Mínimo') !!}</th>
					<th data-type="html">{!! getHeaderLink('PREMAX', 'Precio de Venta Máximo') !!}</th>
					<th data-type="html">{!! getHeaderLink('PREPROM', 'Precio de Venta Promedio') !!}</th>
					<th data-type="html" data-breakpoints="sm"></th>
				</tr>
		    </thead>
		    <tbody>
		        @foreach ($productos as $producto) 
			    <tr class="@if($producto->deleted_at != NULL) warning @endif">
			        <td>{{ $producto->id_codigo}}</td> 
					<td><a href="{{ url('productos/'.$producto->id) }}">{{ $producto->nombre}}</a></td>
		            <td class="text-center"><span class="text-info" style="font-size: 16px;">
				            ${{ number_format(  collect(\App\Models\EstablecimientoPrecios::where('producto_id',$producto->id)->pluck('precio_venta')->toArray() )->min() ,2) }}</span>
		            </td>
		            <td class="text-center">
			            <span class="text-info" style="font-size: 16px;">${{ number_format(  collect(\App\Models\EstablecimientoPrecios::where('producto_id',$producto->id)->pluck('precio_venta')->toArray())->max() ,2) }} 
				             </span>
		            </td>

		            <td class="text-center"><span class="text-info" style="font-size: 16px;">${{ number_format( collect( \App\Models\EstablecimientoPrecios::where('producto_id',$producto->id)->pluck('precio_venta')->toArray())->median() ,2) }} </span>
		            </td>
					<td>
				        <div class="btn-group pull-right">
				            @if($producto->deleted_at != NULL)
					        	<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalProductoReactivar({{ $producto->id }});">
					        	<i class="fa fa-refresh"></i>
					        	 </button>
				            @else
				                <button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalProductoEdit({{ $producto->id }});">
					            <i class="fa fa-edit"></i>
					            </button>
					            <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalProductoDelete({{ $producto->id }});">
					            <i class="fa fa-trash"></i>
					            </button>
				            @endif
				        </div>
			        </td>
			    </tr>
				@endforeach
			</tbody>
		</table>
	@endif
	</div>
    @if(count($productos) > 2)
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $productos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
	@endif
</div>
</div>