@extends('sistema.layouts.master')

@section('title', 'Productos')

@section('attr_navlink_7', 'class="active"')

@section('breadcrumb_title', 'Productos')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Productos</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">

	<div class="col-sm-12">
		@include('sistema.productos.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.productos.iboxes.tabla')
	</div>
</div>

@include('sistema.productos.modals.create')
@include('sistema.productos.modals.edit')
@include('sistema.productos.modals.delete')
@include('sistema.productos.modals.reactivar')
@endsection

@section('assets_bottom')
<script type="text/javascript">

	$('.ver_cotizaciones').on('click', function () {
		$(this).next('ul').toggle(function () {
		    $(this).next('ul').css({display: "block"});
		}, function () {
		    $(this).next('ul').css({display: "none"});
		});
	});

	//PRODUCTOS
	var productos = [];
	
	@foreach($productos as $producto)
		productos[{{ $producto->id }}] = {
			id_codigo : '{{ $producto->id_codigo }}',
			nombre : '{{ $producto->nombre }}',
			peso : '{{ $producto->peso }}',
			precio_venta : '{{ $producto->precio_venta }}',
			comentarios : '{{ $producto->comentarios }}',
			iva : '{{ $producto->iva }}',
			tipo : '{{ $producto->tipo }}',
			peso : '{{ $producto->peso }}',
			es_hielo : '{{ $producto->es_hielo }}',
		}
	@endforeach
	</script>
@endsection
