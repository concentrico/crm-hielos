@extends('sistema.layouts.master')

@section('title', 'Producto')

@section('attr_navlink_7', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $producto->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('productos') }}">Productos</a></li>
<li class="active"><strong>{{ $producto->nombre}}</strong></li>
<li></li>
@endsection
@section('content')


<div class="row">
    <div class="col-sm-12">
        
        <div class="row">
            <div class="col-sm-6">
                @include('sistema.productos.iboxes.datos')
            </div>
             <div class="col-sm-6">
                @include('sistema.productos.iboxes.datos_clientes')
            </div>
        </div>

    </div>
</div>

@include('sistema.productos.modals.edit2')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

@endsection