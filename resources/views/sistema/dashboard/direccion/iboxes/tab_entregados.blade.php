<div class="row">
    <div class="col-lg-12 p-w-xs">
        <div class="ibox float-e-margins" style="margin-bottom: 0px;">
            <div class="ibox-title">
                <h5>PEDIDOS ENTREGADOS</h5>
            </div>
            <div class="ibox-content" style="min-height: 500px;max-height: 500px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Establecimiento</th>
                        <th>Hora</th>
                        <th>Chofer</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_entregados) > 0)
                        @foreach($pedidos_entregados as $pedido)
                            @php
                                $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
                            @endphp
                            <tr>
                                <td>{{--{{$pedido->cliente->nombre_corto}}--}} {{$pedido->establecimiento->sucursal}}</td>
                                <td><i class="fa fa-clock-o"></i> {{$hora_entrega}}</td>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->productos->sum('cantidad_recibida')}}</td>
                                <td><a onclick="verDetalleEntrega({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="4" class="text-center">No hay pedidos entregados todavía.</td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>