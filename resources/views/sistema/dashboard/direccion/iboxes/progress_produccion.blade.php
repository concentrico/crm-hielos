<div class="col-lg-12 p-w-xs">
    <div class="ibox float-e-margins">

    	<div class="ibox-content">
            <div class="row">
                <div class="col-xs-12">
                	<h3>Producción del Día</h3>
                </div>
                @php 
			            $porcentaje = ( ( (int) array_sum(  collect($producidas)->pluck('bolsas')->toArray() ) / (int) array_sum(  collect($produccion_meta)->pluck('bolsas')->toArray()  ))*100);
			            if ($porcentaje <= 30) {
			                $progress_type = 'progress-bar-danger';
			            } else if ($porcentaje > 30 && $porcentaje <= 70) {
			                $progress_type = 'progress-bar-warning';
			            } else if ($porcentaje > 70 && $porcentaje <= 100) {
			                $progress_type = 'progress-bar-success';
			            } else {
			                $progress_type = 'progress-bar-default';
			            }
			        @endphp
                <div class="col-xs-12 text-left">
                    <h2 class="text-left m-r-xs">{{round($porcentaje)}}%</h2>
                </div>
                <div class="col-xs-12">
			        <div class="progress m-b-sm"+>
			        
			            <div style="width:{{$porcentaje}}%;" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar {{$progress_type}}" id="progreso_produccion"></div>
			            
			        </div>
			    </div>
            </div>
        </div>
    </div>
</div>