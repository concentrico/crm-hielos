<div class="col-lg-12 p-w-xs">
    <div class="ibox float-e-margins">

    	<div class="ibox-content">
            <div class="row">
                <div class="col-xs-12">
                	<h3>Entregas del Día</h3>
                </div>
                	@php 
			            if ($porcentaje_distribucion <= 30) {
			                $progress_type = 'progress-bar-danger';
			            } else if ($porcentaje_distribucion > 30 && $porcentaje_distribucion <= 70) {
			                $progress_type = 'progress-bar-warning';
			            } else if ($porcentaje_distribucion > 70 && $porcentaje_distribucion <= 100) {
			                $progress_type = 'progress-bar-success';
			            } else {
			                $progress_type = 'progress-bar-default';
			            }
			        @endphp
                <div class="col-xs-12 text-left">
                    <h2 class="text-left m-r-xs">{{round($porcentaje_distribucion)}}%</h2>
                </div>
                <div class="col-xs-12">
			        <div class="progress m-b-sm"+>
			        
			            <div style="width:{{$porcentaje_distribucion}}%;" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar {{$progress_type}}" id="progreso_distribucion"></div>
			            
			        </div>
			    </div>
            </div>
        </div>
    </div>
</div>