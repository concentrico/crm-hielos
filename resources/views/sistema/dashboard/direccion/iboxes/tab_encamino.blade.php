<div class="row">
    <div class="col-lg-12 p-w-xs">
        <div class="ibox float-e-margins" style="margin-bottom: 0px;">
            <div class="ibox-title">
                <h5>PEDIDOS EN RUTA</h5>
            </div>
            <div class="ibox-content" style="min-height: 500px;max-height: 500px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Chofer</th>
                        <th>Establecimiento</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_en_ruta) > 0)
                        @foreach($pedidos_en_ruta as $pedido)
                            <tr>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->cliente->nombre_corto}} <b>{{$pedido->establecimiento->sucursal}}</b></td>
                                <td>{{$pedido->productos->sum('cantidad_requerida')}}</td>
                                <td><a onclick="verDetallePedido({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="4" class="text-center">No hay pedidos en ruta todavía.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>