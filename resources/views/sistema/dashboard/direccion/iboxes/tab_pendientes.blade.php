<div class="row">
    <div class="col-lg-12 p-w-xs">
        <div class="ibox float-e-margins" style="margin-bottom: 0px;">
            <div class="ibox-title">
                <h5>PEDIDOS PROGRAMADOS</h5>
            </div>
            <div class="ibox-content" style="min-height: 500px;max-height: 500px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Establecimiento</th>
                        <th>Chofer</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_programados) > 0)
                        @foreach($pedidos_programados as $pedido)
                            @php
                                $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                            @endphp
                            <tr class="@if($pedido->fecha_entrega < $fecha_hoy) danger @endif">
                                <td>{{$pedido->cliente->nombre_corto}} <b>{{$pedido->establecimiento->sucursal}}</b></td>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->productos->sum('cantidad_requerida')}}</td>
                                <td>
                                    <a data-toggle="tooltip" data-placement="top" data-original-title="Ver Detalle" onclick="verDetallePedido({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="4" class="text-center">No hay pedidos programados para hoy.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>