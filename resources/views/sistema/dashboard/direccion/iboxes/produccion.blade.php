<div class="col-lg-12 p-w-xs">
    <div class="ibox float-e-margins">

        <div class="ibox-content">
            <div class="row">

                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6 p-w-xs">
                            <div class="widget style1 lazur-bg">
                                <div class="row">
                                    <div class="col-xs-12 m-b-xs no-padding">
                                        <h3 class="font-bold no-margins no-padding">A Producir</h3>
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        @php
                                            $bolsas_producir=0;
                                        @endphp
                                        @if(count($produccion_meta) > 0)
                                            @foreach($produccion_meta as $producto_meta)
                                                @php
                                                    $bolsas_producir+=$producto_meta['bolsas'];
                                                @endphp
                                            @endforeach
                                        @endif
                                        <h1 class="font-bold">{{$bolsas_producir}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 p-w-xs">
                            <div class="widget style1 navy-bg">
                                <div class="row">
                                    <div class="col-xs-12 m-b-xs no-padding">
                                        <h3 class="font-bold no-margins no-padding">Producidas</h3>
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        @php
                                            $bolsas_producidas=0;
                                        @endphp
                                        @if(count($producidas) > 0)
                                            @foreach($producidas as $producto_produccion)
                                                @php
                                                    $bolsas_producidas+=$producto_produccion['bolsas'];
                                                @endphp
                                            @endforeach
                                        @endif
                                        <h1 class="font-bold">{{$bolsas_producidas}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 p-w-xs">
                            <div class="widget style1 red-bg">
                                <div class="row">
                                    <div class="col-xs-12 m-b-xs no-padding">
                                        <h3 class="font-bold no-margins no-padding">Pendientes</h3>
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        @php
                                            $bolsas_pendientes=0;
                                        @endphp
                                        @if(count($pendientes) > 0)
                                            @foreach($pendientes as $producto_pendiente)
                                                @php
                                                    $bolsas_pendientes+=$producto_pendiente['bolsas'];
                                                @endphp
                                            @endforeach
                                        @endif
                                        <h1 class="font-bold">{{$bolsas_pendientes}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 p-w-xs">
                            <div class="widget style1 yellow-bg">
                                <div class="row">
                                    <div class="col-xs-12 text-center m-b-xs no-padding">
                                       <i class="fa fa-cubes fa-5x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12 p-w-xs">
                            <div class="widget style1">
                                <div class="row">
                                    <div class="col-xs-12 text-center" style="padding:0px;">
                                        <div id="chartInventario" style="min-width: 200px; height: 200px; margin: 0 auto"></div>
                                        <!--<canvas id="inventarioChart" class="m-t-xl"></canvas>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ibox-title">
                <h5>LOTES DE PRODUCCIÓN</h5>
            </div>
            <div class="ibox-content" style="min-height: 500px;max-height: 500px;padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Hora</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Registrado por</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($entradas_hoy) > 0)
                        @foreach($entradas_hoy as $entrada)
                            @php
                                $hora_produccion = Carbon::createFromFormat('Y-m-d H:i:s', $entrada->created_at)->formatLocalized('%I:%M %p');
                            @endphp
                            <tr>
                                <td><i class="fa fa-clock-o"></i> {{$hora_produccion}}</td>
                                <td>{{$entrada->producto->nombre}}</td>
                                <td>{{$entrada->cantidad_entrada}}</td>
                                <td class="text-navy"> {!! $entrada->creado_por->nombre !!} </td>
                            </tr>

                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="4" class="text-center">No hay entradas producidas el día de hoy.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
