<div class="col-lg-12 p-w-xs">
    <div class="ibox float-e-margins">

        <div class="ibox-content">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a data-toggle="tab" href="#tab-1" onclick="setCurrentTab('1')">
                        <div class="widget style1 navy-bg">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-clock-o fa-4x m-t-sm"></i>
                                </div>
                                <div class="col-xs-9 ">
                                    <h3 class="font-bold no-margins">Programados</h3>
                                    <h1 class="font-bold text-center">{{count($pedidos_programados)}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a data-toggle="tab" href="#tab-2" onclick="setCurrentTab('2')">
                        <div class="widget style1 lazur-bg">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-truck fa-4x m-t-sm"></i>
                                </div>
                                <div class="col-xs-9 ">
                                    <h3 class="font-bold no-margins">En Camino</h3>
                                    <h1 class="font-bold text-center">{{count($pedidos_en_ruta)}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a data-toggle="tab" href="#tab-3" onclick="setCurrentTab('3')">
                        <div class="widget style1 yellow-bg">
                            <div class="row m-b-xs">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar-check-o fa-4x m-t-sm"></i>
                                </div>
                                <div class="col-xs-9 m-b-xs">
                                    <h3 class="font-bold no-margins">Entregados</h3>
                                    <h1 class="font-bold text-center m-t-lg">{{count($pedidos_entregados)}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="widget style1 red-bg" style="padding: 15px 10px;">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4 class="font-bold no-margins">Por Entregar</h4>
                                <h2 class="font-bold text-center">{{$bolsas_por_entregar}}</h2>
                                bolsas
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
                    <div class="widget style1 blue-bg" style="padding: 15px 10px;">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4 class="font-bold no-margins">Entregadas</h4>
                                <h2 class="font-bold text-center">{{$bolsas_entregadas}}</h2>
                                bolsas
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                            <h2 class="font-bold no-margins text-center visible-xs visible-sm">Pendientes</h2>
                            @include('sistema.dashboard.direccion.iboxes.tab_pendientes')
                        </div>
                        <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                            <h2 class="font-bold no-margins text-center visible-xs visible-sm">Establecimientos</h2>
                            @include('sistema.dashboard.direccion.iboxes.tab_encamino')
                        </div>
                        <div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
                            <h2 class="font-bold no-margins text-center visible-xs visible-sm">Entregas del Día</h2>
                            @include('sistema.dashboard.direccion.iboxes.tab_entregados')
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
