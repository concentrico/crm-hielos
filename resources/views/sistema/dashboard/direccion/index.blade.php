@extends('sistema.layouts.master')

@section('title', 'Dirección')

@section('attr_navlink_1', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection


@section('content')

<style type="text/css" media="screen">
 .progress-bar-success{
    background-color: #1ab394!important;
 }
 .page-heading {
 	display: none;
 }
 #inventarioChart {
   /* width: auto!important;
    height: auto!important;
    max-width: 100%!important;
    */
 }
 .highcharts-button{display: none;}

@media (min-width: 1700px) {

    body{
        font-size: 1.5em;
    }
    h3{ font-size: 1.5em;}
     .ibox-title h5 {
        font-size: 20px;
    }
    h4{ font-size: 1.2em;}
     table {
        font-size: 20px;
    }
    .modal strong{
        font-size: 22px;
    }
    .inmodal .modal-title {
        font-size: 30px;
    }
}

</style>
<div class="row">
    <div class="col-lg-6">
    	<div class="row">
    		@include('sistema.dashboard.direccion.iboxes.progress_produccion')
        </div>
        <div class="row">
        	@include('sistema.dashboard.direccion.iboxes.produccion')
    	</div>
    </div>
    <div class="col-lg-6">
    	<div class="row">
    		@include('sistema.dashboard.direccion.iboxes.progress_distribucion')
        </div>
    	<div class="row">
        	@include('sistema.dashboard.direccion.iboxes.distribucion')
    	</div>
    </div>
</div>

@include('sistema.dashboard.modals.detalle_pedido')
@include('sistema.dashboard.modals.detalle_entrega')

@endsection
@section('assets_bottom')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
<script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>

	<script type="text/javascript">
		$('.body-small').addClass('mini-navbar')
        setTimeout('document.location.reload()',180000);

        $(document).ready( function() {
            switch (localStorage.getItem("lastTab")){
                case '1':
                    $('a[href="#tab-1"]').tab('show');
                break;
                case '2':
                    $('a[href="#tab-2"]').tab('show');
                break;
                case '3':
                    $('a[href="#tab-3"]').tab('show');
                break;
            }
        });

        function setCurrentTab(tab) {
            localStorage.setItem("lastTab", tab);
        }
        
        @php 
            $inventarios = \App\Models\Inventario::all();
            $contador = count($inventarios);
            $c1=0;
            $c2=0;
            $array_colors = ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565',];
        @endphp

        {{--

        var doughnutData = {
            labels: [ 
            @foreach($inventarios as $inventario)
                @if($inventario->cantidad_disponible != '0')
                    '{{$inventario->producto->nombre}}',
                @endif
            @endforeach
            ],

            datasets: [{
                data: [
                    @foreach($inventarios as $inventario)
                        @if($inventario->cantidad_disponible != '0')
                            @if($inventario->cantidad_disponible != null) '{{$inventario->cantidad_disponible}}' @else '0' @endif,
                        @endif
                    @endforeach
                ],
                backgroundColor: ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565']
            }]
        } ;


        var doughnutOptions = {
            responsive: false,
            legend: {
                fullWidth: true,
                display: true,
                position: 'right',

            }
        };

        var ctx4 = document.getElementById("inventarioChart").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
        --}}

        Highcharts.chart('chartInventario', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            legend: {
                enabled: true,
                labelFormatter: function () {
                    return (this.name) + ': ' + (this.y) + ' bolsas';
                },
                symbolHeight: 10
            },
            credits: {
                enabled: false
            },
            title: {
                text: '',
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y} {series.name}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.y} ',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [
                {
                name: 'bolsas',
                colorByPoint: true,
                data: [
                @foreach($inventarios as $inventario)
                    @if($inventario->cantidad_disponible != '0' && $inventario->cantidad_disponible != null)
                        {
                            name: '{{$inventario->producto->nombre}}',
                            y: {{$inventario->cantidad_disponible}}
                        },
                    {{--
                    @else 
                        {
                            name: '{{$inventario->producto->nombre}}',
                            y: 0
                        },
                    --}}
                    @endif
                @endforeach
                ]
                }]
        });
	</script>
@endsection