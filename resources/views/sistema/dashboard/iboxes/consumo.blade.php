<div id="grafico_consumo" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<div class="btn-group" style="position: absolute; top: 90px; right: 25px;">
    <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" aria-expanded="false"><i class="fa fa-navicon"></i></button>
    <ul class="dropdown-menu" style="right: 0;left: auto;">
        <li><a id="btnJpeg">JPEG</a></li>
        <li><a id="btnPng">PNG</a></li>
        <li><a id="btnPdf">PDF</a></li>
    </ul>
</div>

@section('assets_bottom')
	@parent
	<script type="text/javascript">

	$(document).ready(function() {

		Highcharts.setOptions({
		    lang: {
		        drillUpText: '◁ Regresar a Vista Anual',
		        resetZoom: 'Reestablecer Zoom'
		    }
		});
		var chart1 = Highcharts.chart({
	        chart: {
	            type: 'column',
	            renderTo: 'grafico_consumo',
	            zoomType: 'x'
	        },
	        title: {
	            text: 'Consumo de Hielo en Bolsas'
	        },
	        subtitle: {
	        	@if(Input::get('f_consumo_cliente') != '')
	            	text: '{{ \App\Models\Cliente::find(Input::get('f_consumo_cliente'))->nombre_corto }}'
	            @else
	            	text: '{{getSystemSettings()->site_name}}'
	            @endif
	        },
	        legend: {
	            enabled: true
	        },
	        xAxis: {
            	type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Cantidad en Bolsas'
	            }
	        },
	        plotOptions: {
	        	series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y:f}'
	                }
	            },
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Bolsas<br/>'
	        },
	        credits: {
	            enabled: false
	        },
	        series: [
	        	@foreach( $pedidos_entregados_mensual as $key => $value )
	            {
	                name: '{{ \App\Models\Producto::find($key)->nombre }}',
	                data  : [
			        	@for($i = 1; $i <= 12; $i++)
			        		{
				                name: '{{ ucfirst( Carbon::createFromFormat('Y-m-d', (Input::has('f_year_consumo') ? Input::get('f_year_consumo') : date('Y') ).'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT) ) ->formatLocalized('%b %Y') ) }}',
				                y: {{ $pedidos_entregados_mensual[$key][$i] }},
				                drilldown: '{{ ucfirst( Carbon::createFromFormat('Y-m-d', (Input::has('f_year_consumo') ? Input::get('f_year_consumo') : date('Y') ).'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT) ) ->formatLocalized('%b %Y') ) }}{{ \App\Models\Producto::find($key)->nombre }}'
			                },
			        	@endfor
		            ]
	            },
	            @endforeach
	        ],
	        drilldown: {
	        	drillUpButton: {
		            relativeTo: 'spacingBox',
		            position: {
		                y: 0,
		                x: -50
		            }
		        },
		        activeAxisLabelStyle: {
			        cursor: 'pointer',
			        color: '#039',
			        fontWeight: 'bold',
			        textDecoration: 'underline'            
			    },
			    activeDataLabelStyle: {
			        cursor: 'pointer',
			        color: '#039',
			        fontWeight: 'bold',
			        textDecoration: 'underline'            
			    },
			    animation: {
			        duration: 500
			    },

			    series: [
			    	@foreach( $pedidos_entregados_mensual_clientes as $key => $value )
			        	@for($i = 1; $i <= 12; $i++)
			        		{
				                name: '{{ \App\Models\Producto::find($key)->nombre }}',
				                id: '{{ ucfirst( Carbon::createFromFormat('Y-m-d', (Input::has('f_year_consumo') ? Input::get('f_year_consumo') : date('Y') ).'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT) ) ->formatLocalized('%b %Y') ) }}{{ \App\Models\Producto::find($key)->nombre }}',
				                data: [
									@for($j = 0; $j < count($pedidos_entregados_dia[$key][$i]); $j++)
									{
										name: '{{ ucfirst( Carbon::createFromFormat('Y-m-d', (Input::has('f_year_consumo') ? Input::get('f_year_consumo') : date('Y') ).'-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-'.str_pad($j+1, 2, "0", STR_PAD_LEFT) )->formatLocalized("%b ".($j+1)) )}}',
										y: {{ $pedidos_entregados_dia[$key][$i][$j] }}
									},
									@endfor
								]
			                },
			        	@endfor
		            @endforeach
	            ]
	        }
	    });

		
	    $("#btnPrint").on('click', function (event) {
	        chart1.print();
	    });

	    $("#btnJpeg").on('click', function (event) {
	        chart1.exportChart({
	            type: "image/jpeg",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });

	    $("#btnPng").on('click', function (event) {
	        chart1.exportChart({
	            type: "image/png",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });
	    $("#btnPdf").on('click', function (event) {
	        chart1.exportChart({
	            type: "application/pdf",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });
	    
	});

	</script>
@endsection