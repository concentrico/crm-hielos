 <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list-all"></i> Cuarto Frío </h5>
    </div>
    <div class="ibox-content" style="min-height: 450px;max-height: 450px;">

        <div class="row">

            <div class="col-lg-6 text-center">
                <h1>Inventario Actual</h1>
                <h2 class="text-success">(bolsas)</h2>
                <div id="cuartofrio-chart" ></div>
            </div>

            <div class="col-lg-6">
                <table class="table table-hover margin bottom">
                    <thead>
                        <tr>
                            <th class="text-center">Producto</th>
                            <th class="text-center">Inventario Actual</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($producto as $producto)
                           <tr>
                                <td>{{$producto->nombre}}</td>
                                <td class="text-center">{{$producto->cantidad_disponible}} unidades</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@section('assets_bottom')
    @parent
    <script type="text/javascript">
    $(document).ready(function() {

        Morris.Donut({
            element: 'cuartofrio-chart',
            data: [
            @php
                $inventarios = \App\Models\Inventario::all();
                $contador = count($inventarios);
                $c1=0;
                $c2=0;
                $array_colors = ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565',];
            @endphp
            @foreach($inventarios as $inventario)
                @if($inventario->cantidad_disponible != '0')
                    { label: "{{$inventario->producto->nombre}}", value: @if($inventario->cantidad_disponible != null) {{$inventario->cantidad_disponible}} @else 0 @endif},
                @endif
            @endforeach
            ],
            resize: true,
            colors: ['#1ab394','#1c84c6','#23c6c8','#f8ac59','#ed5565'],

        });

    });
    </script>
@endsection