@extends('sistema.layouts.master')

@section('title', 'Dashboard')

@section('attr_navlink_1', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
@endsection



@section('breadcrumb_title', 'Dashboard')

@section('breadcrumb_content')
<li class="active"><strong>Home</strong></li>
<li></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-4 col-xs-12 col-lg-3" style="padding: 0;">
        <div class="widget style-custom">
            <div class="row">
                <div class="col-xs-2 text-center">
                    <i class="fa fa-file-text-o fa-5x"></i>
                </div>
                <div class="col-xs-10 text-right">
                    <span> Pedidos de Hoy</span>
                    <h4 class="font-bold">{{ $pedidos_hoy }}</h4>
                    <a class="text-primary" href="{{ url('pedidos')}}?search_fecha_hoy={{ Carbon\Carbon::now()->format('Y-m-d') }}"><i class="fa fa-th"></i> Ver todos</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-lg-3 col-md-offset-4 col-lg-offset-6" style="padding: 0;">
        <div class="widget style-custom">
            <div class="row">
                <div class="col-xs-2 text-center">
                    <i class="fa fa-exclamation-circle fa-5x"></i>
                </div>
                <div class="col-xs-10 text-right">
                    <span> Saldo Vencido </span>
                    <h4 class="font-bold">${{ number_format($total_vencido, 2, '.', ',') }}</h4>
                    <a class="text-primary" href="{{ url('cobranza')}}"><i class="fa fa-th"></i> Ver todo</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false ) active @endif">
                        <a data-toggle="tab" href="#tab-1">
                            <i style="margin:0px;" class="fa fa-bar-chart fa-2x"></i>&nbsp;
                            <span class="hidden-xs">Consumo</span> 
                        </a> 
                    </li>
                    <li class="@if(Input::get('active_tab') == 2) active @endif">
                        <a data-toggle="tab" href="#tab-2">
                            <i style="margin:0px;" class="fa fa-line-chart fa-2x"></i>&nbsp;
                            <span class="hidden-xs">Ventas<span> 
                        </a>
                    </li>  
                </ul>
                {!! Form::open(['method' => 'GET',  'url' => url('home')] ) !!}
                <div class="tab-content">
                    
                    <div id="tab-1" class="tab-pane @if( Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                        <div class="panel-body" style="padding: 10px 20px 0px 20px;">
                            <div class="row">
                                <div class="form-group col-sm-4 col-md-3 col-lg-2 pull-right">
                                    {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
                                    <button type="submit" class="btn btn-success col-sm-12"><i class="fa fa-filter"></i> Filtrar</button>
                                </div>
                                <div class="form-group col-sm-4 col-lg-4 pull-right">
                                    {!! Form::label('f_consumo_cliente', 'Cliente', ['class' => 'control-label']) !!}
                                    {!! Form::select('f_consumo_cliente', \App\Models\Cliente::orderBy('nombre_corto')->get()->pluck('nombre_corto', 'id')->toArray(), Input::get('f_consumo_cliente'), ['class' => 'form-control selectdp', 'placeholder' => 'Ver Todos'] ) !!}
                                </div>
                                <div class="form-group col-sm-4 col-lg-2 pull-right">
                                    {!! Form::label('f_year_consumo', 'Año', ['class' => 'control-label']) !!}
                                    {!! Form::select('f_year_consumo', $years, (Input::get('f_year_consumo') != null ? Input::get('f_year_consumo') : date('Y') ), ['class' => 'form-control selectdp'] ) !!}
                                </div>
                            </div>
                            @include('sistema.dashboard.iboxes.consumo')
                        </div>
                    </div>
                    
                    <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                        <div class="panel-body" style="padding: 10px 20px 0px 20px;">
                            <div class="row">
                                <div class="form-group col-sm-3 col-md-3 col-lg-2 pull-right">
                                    {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
                                    <button type="submit" class="btn btn-primary col-sm-12"><i class="fa fa-filter"></i> Filtrar</button>
                                </div>
                                <div class="form-group col-sm-6 col-lg-4 pull-right">
                                    {!! Form::label('f_ventas_cliente', 'Cliente', ['class' => 'control-label']) !!}
                                    {!! Form::select('f_ventas_cliente', \App\Models\Cliente::orderBy('nombre_corto')->get()->pluck('nombre_corto', 'id')->toArray(), Input::get('f_ventas_cliente'), ['class' => 'form-control selectdp', 'placeholder' => 'Ver Todos'] ) !!}
                                </div>
                                <div class="form-group col-sm-4 col-lg-2 pull-right">
                                    {!! Form::label('f_year', 'Año', ['class' => 'control-label']) !!}
                                    {!! Form::select('f_year_ventas', $years, (Input::get('f_year_ventas') != null ? Input::get('f_year_ventas') : date('Y') ), ['class' => 'form-control selectdp'] ) !!}
                                </div>
                            </div>
                            @include('sistema.dashboard.iboxes.venta')
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
        <div class="col-lg-12">
            @include('sistema.dashboard.iboxes.inventario')
        </div>
    </div>

@endsection

@section('assets_bottom')
<!-- HIGH CHARTS -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
@endsection