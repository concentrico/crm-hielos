<div class="modal inmodal" id="modalCambioChofer" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-danger"><i class="fa fa-exchange modal-icon"></i> <BR/>Asignar Otro Chofer</h4>
			</div>
			<div class="modal-body" id="bodyCambioChofer">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="cambiarChoferPedido();" data-dismiss="modal"><i class="fa fa-exchange"></i> Cambiar Chofer</button>
			</div>
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function cambiarChofer(id) {

		$('#modalCambioChofer').modal('show');

		$.ajax({
            type: 'POST',
            url: '{{url('ajax/dashboard')}}',
            data:{ 'type': '3', 'pedido_id': id, '_token': '{{ csrf_token()}}' },
            beforeSend: function() {
            	$("#bodyCambioChofer").empty();
            	$("#bodyCambioChofer").html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>');

            	setTimeout(function(){
                    $('#chofer_cambiar').dropdown();
                }, 1500);
            	
	        },
            success: function(data) {
            	$("#bodyCambioChofer").empty();
            	$("#bodyCambioChofer").html(data);
            }
        });
		
	}
</script>	
@endsection