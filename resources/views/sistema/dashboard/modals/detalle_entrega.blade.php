<div class="modal inmodal" id="modalDetalleEntrega" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-danger"><i class="fa fa-calendar-check-o modal-icon"></i> <BR/>Detalle de la Entrega</h4>
			</div>
			<div class="modal-body" id="bodyPedidosEntregados">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
			</div>
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function verDetalleEntrega(id) {

		$('#modalDetalleEntrega').modal('show');

		$.ajax({
            type: 'POST',
            url: '{{url('ajax/dashboard')}}',
            data:{ 'type': '1', 'status': '3', 'pedido_id': id, '_token': '{{ csrf_token()}}' },
            beforeSend: function() {
            	$("#bodyPedidosEntregados").empty();
            	$("#bodyPedidosEntregados").html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>');
	        },
            success: function(data) {
            	$("#bodyPedidosEntregados").empty();
            	$("#bodyPedidosEntregados").html(data);
            }
        });
		
	}
</script>	
@endsection