<div class="modal inmodal" id="modalDetallePedido" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-danger"><i class="fa fa-clipboard modal-icon"></i> <BR/>Detalle del Pedido</h4>
			</div>
			<div class="modal-body" id="bodyPedidosProgramados">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
			</div>
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function verDetallePedido(id) {

		$('#modalDetallePedido').modal('show');

		$.ajax({
            type: 'POST',
            url: '{{url('ajax/dashboard')}}',
            data:{ 'type': '1', 'status': '1', 'pedido_id': id, '_token': '{{ csrf_token()}}' },
            beforeSend: function() {
            	$("#bodyPedidosProgramados").empty();
            	$("#bodyPedidosProgramados").html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>');
	        },
            success: function(data) {
            	$("#bodyPedidosProgramados").empty();
            	$("#bodyPedidosProgramados").html(data);
            }
        });
		
	}
</script>	
@endsection