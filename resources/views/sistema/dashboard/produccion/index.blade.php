@extends('sistema.layouts.master')

@section('title', 'Producción')

@section('attr_navlink_1', 'class="active"')

@section('breadcrumb_title', 'Producción de Hielo')

@section('breadcrumb_content')
<li class="active"><strong>Home</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
 .progress-bar-success{
    background-color: #1ab394!important;
 }
 .page-heading {
    display: none;
 }
</style>

<div class="row">

    <div class="col-lg-12">
        <div class="widget style1 navy-bg">
            <div class="row">
                <div class="col-xs-4" style="display: inline-flex;">
                    <i class="fa fa-cubes fa-5x"></i> &nbsp;
                    <h1 class="font-bold">Bolsas Producidas</h1>
                </div>
                <div class="col-xs-8 text-right" style="padding: 10px;">
                    @if(count($producidas) > 0)
                        @foreach($producidas as $producto_produccion)
                            <h3 class="font-bold"> {{$producto_produccion['nombre']}} : {{$producto_produccion['bolsas']}}</h3> 
                        @endforeach
                    @else
                        <h4 class="font-bold">N.D.</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-8 p-w-xs">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>LOTES DE PRODUCCIÓN</h5>
            </div>
            <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Hora</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Registrado por</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($entradas_hoy) > 0)
                        @foreach($entradas_hoy as $entrada)
                            @php
                                $hora_produccion = Carbon::createFromFormat('Y-m-d H:i:s', $entrada->created_at)->formatLocalized('%I:%M %p');
                            @endphp
                            <tr>
                                <td><i class="fa fa-clock-o"></i> {{$hora_produccion}}</td>
                                <td>{{$entrada->producto->nombre}}</td>
                                <td>{{$entrada->cantidad_entrada}}</td>
                                <td class="text-navy"> {!! $entrada->creado_por->nombre !!} </td>
                            </tr>

                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="4" class="text-center">No hay entradas producidas el día de hoy.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @can('manage-users')
                    <hr/>
                    <a class="btn btn-default btn-rounded btn-block" href="{{url('inventario')}}"><i class="fa fa-th"></i> Ver todo</a>
                @endcan
            </div>
        </div>
    </div>
    <div class="col-lg-4 p-w-xs">
        <div class="widget m-xl text-center">
            <BR/><BR/><BR/>
            <div class="m-b-md m-t-l">
            {!! Form::open(['method' => 'POST', 'url' => url('produccion/registrar'), 'novalidate' => ''] ) !!}
                <div class="form-group col-sm-12 @if ($errors->first('producto_produccion') !== "") has-error @endif">
                    {!! Form::label('producto_produccion', 'Producto', ['class' => 'control-label text-center']) !!}
                    {!! Form::select('producto_produccion', \App\Models\Producto::where('es_hielo', '=', 1)->get()->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['id' => 'producto_produccion','class' => 'col-xs-12 selectdp'] ) !!}
                </div>

                <div class="form-group col-sm-12 @if ($errors->first('cantidad_produccion') !== "") has-error @endif">
                    {!! Form::label('cantidad_produccion', 'Cantidad Bolsas', ['class' => 'control-label text-center']) !!}
                    <div class="input-group" style="width: 100%;">
                        <input class="touchspin_cantidad positive-integer capproducto" type="text" name="cantidad_produccion"/>
                    </div>
                </div>

                <div class="form-group col-sm-12 m-t-xs">
                    <button class="btn btn-success dim" type="submit" ><i class="fa fa-check"></i>&nbsp; Registrar Producción</button>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('assets_bottom')

<script type="text/javascript">
    @if(session('registro_success'))
        swal('Correcto', "Se ha registrado el lote satisfactoriamente.", "success");
    @endif
    setTimeout('document.location.reload()',180000);
</script>

@endsection