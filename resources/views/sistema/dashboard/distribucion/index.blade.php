@extends('sistema.layouts.master')

@section('title', 'Distribución')

@section('attr_navlink_1', 'class="active"')

@section('assets_top_top')
    <script src="https://js.pusher.com/4.0/pusher.min.js"></script>
    <script type="text/javascript">
        (function(){
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('16daf10813a3770b9014', {
              encrypted: true
            });

            var channel = pusher.subscribe('pedidos-entregados-channel');

            channel.bind('App\Events\EventoPedido', function(data) {
              //alert(data.message);
              console.log(data);
            });
        })();
    </script>
@endsection

@section('breadcrumb_title', 'Distribución de Pedidos')

@section('breadcrumb_content')
<li class="active"><strong>Home</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
 .page-heading {
    display: none;
 }
</style>

<div class="row">
    
    <div class="col-lg-4 p-w-xs">
        <div class="widget style1 navy-bg">
            <div class="row">
                <div class="col-xs-2">
                    <i class="fa fa-clock-o fa-5x"></i>
                </div>
                <div class="col-xs-10 text-center">
                    <span> &nbsp; </span>
                    <h1 class="font-bold no-margins">Programados</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 p-w-xs">
        <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-xs-2">
                    <i class="fa fa-truck fa-5x"></i>
                </div>
                <div class="col-xs-10 text-center">
                    <span> &nbsp; </span>
                    <h1 class="font-bold no-margins">En Camino</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 p-w-xs">
        <div class="widget style1 yellow-bg">
            <div class="row">
                <div class="col-xs-2">
                    <i class="fa fa-calendar-check-o fa-5x"></i>
                </div>
                <div class="col-xs-10 text-center">
                    <span> &nbsp; </span>
                    <h1 class="font-bold no-margins">Entregados</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 p-w-xs">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>PEDIDOS PROGRAMADOS</h5>
            </div>
            <div class="ibox-content" style="min-height: 400px;max-height: 400px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Chofer</th>
                        <th>Establecimiento</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_programados) > 0)
                        @foreach($pedidos_programados as $pedido)
                            @php
                                $fecha_hoy = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
                            @endphp
                            <tr class="@if($pedido->fecha_entrega < $fecha_hoy) danger @endif">
                                <td ><input type="checkbox" value="{{$pedido->id}}" name="en_ruta[]" class="i-checks"/></td>
                                <td>{{$pedido->cliente->nombre_corto}} <b>{{$pedido->establecimiento->sucursal}}</b></td>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->productos->sum('cantidad_requerida')}}</td>
                                <td>
                                    <a data-toggle="tooltip" data-placement="top" data-original-title="Ver Detalle" onclick="verDetallePedido({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a>
                                    <a data-toggle="tooltip" data-placement="top" data-original-title="Cambiar Chofer" class="text-danger" onclick="cambiarChofer({{$pedido->id}});"><i class="fa fa-lg fa-exchange"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="5" class="text-center">No hay pedidos programados para hoy.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="ibox-footer">
                <a class="btn btn-success btn-rounded btn-block" onclick="ponerEnRuta();"><i class="fa fa-truck"></i> Poner en Ruta</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 p-w-xs">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>PEDIDOS EN RUTA</h5>
            </div>
            <div class="ibox-content" style="min-height: 400px;max-height: 400px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Chofer</th>
                        <th>Establecimiento</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_en_ruta) > 0)
                        @foreach($pedidos_en_ruta as $pedido)
                            <tr>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->cliente->nombre_corto}} <b>{{$pedido->establecimiento->sucursal}}</b></td>
                                <td>{{$pedido->productos->sum('cantidad_requerida')}}</td>
                                <td><a onclick="verDetallePedido({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="5" class="text-center">No hay pedidos en ruta todavía.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="ibox-footer">
                <a class="btn btn-success btn-rounded btn-block" href="{{url('pedidos')}}"><i class="fa fa-th"></i> Ver todos</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 p-w-xs">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>PEDIDOS ENTREGADOS</h5>
            </div>
            <div class="ibox-content" style="min-height: 400px;max-height: 400px; padding:0px; overflow-y: scroll;">
                <table class="table table-hover no-margins">
                    <thead>
                    <tr>
                        <th>Establecimiento</th>
                        <th>Hora</th>
                        <th>Chofer</th>
                        <th>Bolsas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pedidos_entregados) > 0)
                        @foreach($pedidos_entregados as $pedido)
                            @php
                                $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
                            @endphp
                            <tr>
                                <td>{{$pedido->cliente->nombre_corto}} <b>{{$pedido->establecimiento->sucursal}}</b></td>
                                <td><i class="fa fa-clock-o"></i> {{$hora_entrega}}</td>
                                <td>{{$pedido->chofer->nombre}}</td>
                                <td>{{$pedido->productos->sum('cantidad_recibida')}}</td>
                                <td><a onclick="verDetalleEntrega({{$pedido->id}});"><i class="fa fa-lg fa-eye"></i> </a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="warning">
                            <td colspan="5" class="text-center">No hay pedidos entregados todavía.</td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="ibox-footer">
                <a class="btn btn-success btn-rounded btn-block" href="{{url('pedidos')}}"><i class="fa fa-th"></i> Ver todos</a>
            </div>
        </div>
    </div>
</div>


@include('sistema.dashboard.modals.detalle_pedido')
@include('sistema.dashboard.modals.detalle_entrega')
@include('sistema.dashboard.modals.cambiar_chofer')
@endsection

@section('assets_bottom')
<!-- Data Tables -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>

<script type="text/javascript">

    $('.body-small').addClass('mini-navbar')
    setTimeout('document.location.reload()',180000);

    if ( localStorage.getItem("poner_ruta_success") == '1' ) {
        swal("Correcto!", "Se han marcado todos los pedidos en camino.", "success");
        localStorage.setItem("poner_ruta_success", '0');
    }

    var choferes = [];
    @php
    $choferes = DB::table('users')
        ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
        ->where('user_has_role.user_role_id', '=', 4)
        ->where('users.deleted_at', NULL)
        ->get();

    $choferes_array = $choferes->pluck('nombre', 'id')->toArray();
    @endphp
    
    @foreach($choferes_array as $key => $value)
        choferes[{{ $key }}] = {
            nombre : '{{$value}}',
        }
    @endforeach

    function ponerEnRuta() {

        var array_pedidos = [];
        $(':checkbox:checked').each(function(i){
          array_pedidos[i] = $(this).val();
        });

        if (array_pedidos.length> 0) {

            var swal1 = swal({
              title: "¿Desea poner en ruta los pedidos marcados?",
              text: "El status de los pedidos: <b>"+array_pedidos.join()+"</b> cambiará a <b>EN CAMINO</b>.",
              type: "warning",
              showCancelButton: true,
              html: true,
              confirmButtonColor: "#1c84c6",
              confirmButtonText: "Sí",
              cancelButtonText: "Cancelar",
              customClass: 'modalConfirmarAlert',
              showLoaderOnConfirm: true,
              closeOnConfirm: false
            },
            function(){

                $('.modalConfirmarAlert').toggleClass('sk-loading');
                $('.modalConfirmarAlert').append('<div class="sk-spinner sk-spinner-double-bounce"><div class="sk-double-bounce1"></div><div class="sk-double-bounce2"></div></div>');
                
                $.ajax({
                    type: 'POST',
                    url: '{{url('ajax/dashboard')}}',
                    data:{ 'type': '2', 'pedidos_ids': array_pedidos, '_token': '{{ csrf_token()}}' },
                    success: function(data) {

                        localStorage.setItem("poner_ruta_success", '1');
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    }
                });
              
            });

        } else {
            swal("Error!", "Es necesario marcar por lo menos un pedido.", "error");
        }

    }

    function cambiarChoferPedido() {

        pedido_id = $('#pedido_id_cambiar_chofer').val();
        chofer_id = $('#chofer_cambiar').val();

        swal({
          title: "¿Desea cambiar el Chofer?",
          text: "Ahora el Pedido #<b>"+pedido_id+"</b> tendrá asignado al Chofer: <b>"+choferes[chofer_id]['nombre']+"</b>.",
          type: "warning",
          showCancelButton: true,
          html: true,
          showLoaderOnConfirm: true,
          confirmButtonColor: "#1c84c6",
          confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
          closeOnConfirm: false
        },
        function(){

            $.ajax({
                type: 'POST',
                url: '{{url('ajax/dashboard')}}',
                data:{ 'type': '4', 'chofer_id': chofer_id, 'pedido_id': pedido_id, '_token': '{{ csrf_token()}}' },
                success: function(data) {
                    swal("Correcto!", "Se ha cambiado el chofer satisfactoriamente.", "success");

                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                }
            });
          
        });

    }
</script>
@endsection