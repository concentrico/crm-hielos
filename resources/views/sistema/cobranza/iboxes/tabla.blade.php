<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Cuentas por Cobrar</h5>
    </div>
    <div class="ibox-content">

    	<div class="row m-b-lg">
			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-gavel fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> No Facturado </h4>
			                <h1 class="font-bold">${{ number_format($total_no_facturado, 2, '.', ',') }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-money fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Total por cobrar </h4>
			                <h1 class="font-bold">${{ number_format($total_por_cobrar, 2, '.', ',') }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-exclamation-circle fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Total Vencido </h4>
			                <h1 class="font-bold">${{ number_format($total_vencido, 2, '.', ',') }}</h1>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table class="table table-res" data-cascade="true">
				<thead>
					<tr>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('ENTREGADO', 'Fecha Entregado') !!}</th>
						<th data-type="html">{!! getHeaderLink('PEDIDO', 'Pedido') !!}</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('ESTATUSF', 'Status Facturación') !!}</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('FACTURADO', 'Facturado') !!}</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('ESTATUSC', 'Status Cobranza') !!}</th>
						<th data-type="html" data-breakpoints="xs">Pagado</th>
						<th data-type="html" data-breakpoints="xs">Bolsas</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('TOTAL', 'Total') !!}</th>
						<th data-type="html" data-breakpoints="xs">Vencido</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html" data-breakpoints="xs"></th>
					</tr>
				</thead>
				<tbody>
                    @foreach ($pedidos as $pedido)
	                    <tr class="@if($pedido->deleted_at != NULL) warning @endif">
	                        <td>
	                        	@if($pedido->entregado_at != NULL)
			                        {{fecha_to_human($pedido->entregado_at,false) }}
	                                    -
	                                {{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
	                            @else
	                            	- 
	                            @endif
	                        </td>
	                        <td>
	                        	@if( $pedido->tiene_pendientes() )
		                        	<i class="fa fa-minus-circle text-danger"></i>
		                        @else
		                        	<i class="fa fa-check text-info"></i>
		                        @endif
		                        <a href="{{ url('pedidos/' . $pedido->id . '?active_tab=2') }}">Pedido #{{ $pedido->id}}</a>
								<br />
								{!! $pedido->cliente->liga('text-info') !!}
	                        </td>
	                        <td>{!! $pedido->estatus_facturacion() !!}</td>
	                        <td>${{ number_format($pedido->facturas->sum('total'), 2, '.', ',') }}</td>
	                        <td>{!! $pedido->estatus_cobranza() !!}</td>
	                        <td>${{ number_format($pedido->pagos->sum('monto'), 2, '.', ',') }}</td>
	                        <td class="text-center">
                            	@if($pedido->status == 3)
                            		<b>{{ $pedido->getTotalBolsas() }}</b>
                            	@else
                            		{{ $pedido->getTotalBolsas() }}
                            	@endif
                            </td>
	                        <td>${{ number_format($pedido->total, 2, '.', ',') }}</td>
	                        <td class="text-center">
	                        	@if($pedido->por_cobrar() > 0.00)
				                    <span class="text-danger">
				                    	${{ number_format($pedido->por_cobrar(), 2, '.', ',') }} 
					                </span>
					            @else
					            	<span>
				                    	${{ number_format($pedido->por_cobrar(), 2, '.', ',') }} 
					                </span>
					            @endif
		                    </td>
	                        <td>{{ $pedido->created_at }}</td>
	                        <td>{!! $pedido->creado_por->liga() !!}</td>
	                        <td>{{ $pedido->updated_at }}</td>
	                        <td>{!! $pedido->actualizado_por->liga() !!}</td>
	                        <td class="text-center">
		                    	<a href="{{url('cobranza').'/'.$pedido->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    </td>
		                    {{--
							<td>
								<div class="dropdown">
									<button class="btn btn-white btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<i class="fa fa-pencil text-success"></i>
									<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" style="right: 5px; left: auto;">
										<li class="text-success"><a href="#" onclick="showModalEventoOrdenFacturaDelete({{$pedido->id}})">
											<i class="fa fa-plus"></i> Agregar Pago</a>
										</li>
										<li role="separator" class="divider"></li>
										<li class="text-success"><a href="#" onclick="showModalEventoOrdenFacturaDelete({{$pedido->id}})">
											<i class="fa fa-plus"></i> Agregar Factura</a>
										</li>
									</ul>
								</div>
							</td>
							--}}
	                    </tr>
					@endforeach
				</tbody>
			</table>            	        
        </div>
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $pedidos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>
