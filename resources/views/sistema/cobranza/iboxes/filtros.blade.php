<div class="ibox float-e-margins filtros-res">
    <div class="ibox-title">
        <h5><i class="fa fa-filter"></i> Filtrar resultados</h5>
        <div class="ibox-tools pull-right">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
		{!! Form::open(['method' => 'GET',  'url' => url('cobranza')] ) !!}
			<input type="hidden" name="orderBy" value="{{ Input::get('orderBy') }}"/>
			<input type="hidden" name="sort" value="{{ Input::get('sort') }}"/>
            <div class="row">
	            <div class="form-group col-sm-2">
		            {!! Form::label('f_num_resultados', 'Mostrar', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_num_resultados', [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100, 200 => 200], 
	            	Input::get('f_num_resultados', 25), ['class' => 'form-control selectdp', 'placeholder' => 'No. Resultados'] ) !!}
		        </div>
	            <div class="form-group col-sm-4">
		            {!! Form::label('f_cliente', 'Cliente', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_cliente', \App\Models\Cliente::orderBy('nombre_corto')->get()->pluck('nombre_corto', 'id')->toArray(), Input::get('f_cliente'), ['class' => 'form-control chosen-select', 'placeholder' => 'Ver Todos'] ) !!}
		        </div>
		        <div class="form-group col-sm-4 col-md-3 col-lg-3">
		            {!! Form::label('f_estatus_cob', 'Status Cobranza', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_estatus_cob', ['todos' => 'Ver todos', '1' => 'Pendiente', '2' => 'Pago Parcial', '3' => 'Liquidado','4' => 'Incobrable'], Input::get('f_estatus_cob', 'todos'), ['class' => 'form-control selectdp'] ) !!}
		        </div>
		        <div class="form-group col-sm-4 col-md-3 col-lg-3">
		            {!! Form::label('f_estatus_fact', 'Status Facturación', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_estatus_fact', ['todos' => 'Ver todos', '1' => 'No Facturado', '2' => 'Facturado Parcialmente', '3' => 'Facturado','4' => 'Infacturable'], Input::get('f_estatus_fact', 'todos'), ['class' => 'form-control selectdp'] ) !!}
		        </div>
		        <div class="col-sm-6">
                    {!! Form::label('fecha', 'Fecha de Entrega', ['class' => 'control-label']) !!}
                    <div class="input-daterange input-group" style="width: 100%;">
                        {!! Form::text('fecha_ini', Input::get('fecha_ini'), ['class' => 'form-control', 'placeholder' => 'Fecha Inicio']) !!}
                          <span class="input-group-addon">-</span>
                        {!! Form::text('fecha_fin', Input::get('fecha_fin'), ['class' => 'form-control', 'placeholder' => 'Fecha Fin']) !!}
                    </div>
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('f_saldo_vencido', ' ', ['class' => 'control-label']) !!}
	            	<div class="checkbox checkbox-primary">
                        <input id="f_saldo_vencido" name="f_saldo_vencido" type="checkbox" value="1" @if(Input::has('f_saldo_vencido') && Input::get('f_saldo_vencido') == '1') checked="" @endif)>
                        <label for="f_saldo_vencido" style="font-weight: bold;">
                            Saldo Vencido
                        </label>
                    </div>
		        </div>
	            <div class="form-group col-sm-3 col-md-3">
		            {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
					<button type="submit" class="btn btn-primary col-sm-12"><i class="fa fa-filter"></i> Filtrar</button>
		        </div>
            </div>			
		{!! Form::close() !!}
	</div>
</div>
