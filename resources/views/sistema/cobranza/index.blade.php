@extends('sistema.layouts.master')

@section('title', 'Cobranza')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('attr_navlink_10', 'class="active"')

@section('breadcrumb_title', 'Cobranza')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong>Cobranza</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.cobranza.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.cobranza.iboxes.tabla')
	</div>
</div>

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>	
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>	
<script>	
	
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	pedidos = [];
	
	@foreach($pedidos as $pedido)
	
		pedidos[{{ $pedido->id }}] = {
		}
	
	@endforeach

</script>
@endsection