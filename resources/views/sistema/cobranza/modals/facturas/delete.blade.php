<div class="modal inmodal" id="modalPedidoFacturaDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					
					<h4 class="text-danger modal-title" style="color:#ed5565;"><i class="fa fa-file-text modal-icon"></i> Eliminar Factura</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-navy">¿Estas seguro de que deseas eliminar la factura?</h2>
				            <h3 style="font-weight: bold;"></h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('cobranza/' . $pedido->id . '/factura/' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar </button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalPedidoFacturaDelete(id){
		
		if ( $('#modalPedidoFacturaDelete').find('form').attr('action') == "{{ url('cobranza/' . $pedido->id . '/factura' ) }}/" + id){
			
		}else{
			$('#modalPedidoFacturaDelete').find('form').attr('action', "{{ url('cobranza/' . $pedido->id . '/factura' ) }}/" + id)
			
			$('#modalPedidoFacturaDelete').find('h3').html("Folio: " + facturas[id]['folio'] + "");
						
		}

		$('#modalPedidoFacturaDelete').modal('show');
	}

</script>
@endsection