<div class="modal inmodal" id="modalStatusCobranza" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-refresh modal-icon"></i> <BR/>Cambiar Status Cobranza</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('cobranza/' . $pedido->id . '/status-cobranza' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3">
						{!! Form::label('status_cobranza', 'Status', ['class' => 'control-label']) !!}
				           @php
				           		$status_cobranza_array = array('1' => 'Pendiente', '4' => 'Incobrable');
				           @endphp
							{!! Form::select('status_cobranza', $status_cobranza_array, $pedido->status_cobranza, ['class' => 'form-control selectdp'] ) !!}
				           <span class="help-block">{{ $errors->first('status_cobranza')}}</span>
				        </div>
		            </div>
		            @if ($errors->first('statusCobranzaFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('statusCobranzaFailed')!!}
				        </div>
					@endif
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="button" class="btn btn-success" type="submit"><i class="fa fa-save"></i> Cambiar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cambiar_status_cobranza'))
		$('#modalStatusCobranza').modal('show');
	@endif
	
</script>	
@endsection