<div class="modal inmodal" id="modalPedidoPagoDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					
					<h4 class="text-danger modal-title" style="color:#ed5565;"><i class="fa fa-file-text modal-icon"></i><BR/>Eliminar Pago</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2>¿Estas seguro de que deseas eliminar el pago?</h2>
				            <h3 style="font-weight: 300;"></h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('cobranza/' . $pedido->id . '/pago/' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar </button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalPedidoPagoDelete(id){
		
		if ( $('#modalPedidoPagoDelete').find('form').attr('action') == "{{ url('cobranza/' . $pedido->id . '/pago' ) }}/" + id){
			
		}else{
			$('#modalPedidoPagoDelete').find('form').attr('action', "{{ url('cobranza/' . $pedido->id . '/pago' ) }}/" + id)
						
		}

		$('#modalPedidoPagoDelete').modal('show');
	}

</script>
@endsection