@extends('sistema.layouts.master')

@section('title', 'Cobranza')

@section('attr_navlink_10', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', 'Cobranza Pedido #'.$pedido->id)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('cobranza') }}">Cobranza</a></li>
<li class="active"><strong>Pedido #{{ $pedido->id}}</strong></li>
<li></li>
@endsection
@section('content')


<div class="row">
    <div class="col-sm-12">
        @include('sistema.cobranza.iboxes.facturas')
        @include('sistema.cobranza.iboxes.pagos')
    </div>
</div>

@include('sistema.cobranza.modals.pagos.create')
@include('sistema.cobranza.modals.pagos.edit')
@include('sistema.cobranza.modals.pagos.delete')
@include('sistema.cobranza.modals.facturas.create')
@include('sistema.cobranza.modals.facturas.edit')
@include('sistema.cobranza.modals.facturas.delete')
@include('sistema.cobranza.modals.status_cobranza')
@include('sistema.cobranza.modals.status_facturacion')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>    
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/slick/slick.min.js') }}"></script>
<script src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>  

<script type="text/javascript"> 

    @if(session('pago_pedido_success'))
        toastr.success("Pago Agregado satisfactoriamente.");
    @endif

    @if(session('factura_pedido_success'))
        toastr.success("Factura Agregada satisfactoriamente.");
    @endif

    @if(session('pago_pedido_update_success'))
        toastr.info("Pago Actualizado satisfactoriamente.");
    @endif

    @if(session('factura_pedido_update_success'))
        toastr.info("Factura Actualizada satisfactoriamente.");
    @endif

    @if(session('pego_pedido_delete_success'))
        toastr.error("Pago Eliminado satisfactoriamente.");
    @endif

    @if(session('factura_pedido_delete_success'))
        toastr.error("Factura Eliminada satisfactoriamente.");
    @endif

    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            
            if( label != ""){
                this.nextSibling.innerHTML = label;     
            }else{
                this.nextSibling.innerHTML = 'Selecciona un archivo';               
            }
            
            console.log(numFiles);
            console.log(label);
        });
    });

    $.fn.datepicker.dates['es-MX'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
        today: "Hoy",
        clear: "Borrar",
        format: "dd/mm/yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };
        
    $('.input-group.date').datepicker({
        todayHighlight: true,
        language: 'es-MX',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy"
    });

    var productos_pedido = [];

    @foreach($pedido->productos as $producto)
        
        productos_pedido[{{ $producto->id }}] = {
            producto_id : '{{ $producto->producto_id }}',
            producto_nombre : '{{ $producto->producto->nombre }}',
            precio : '{{ $producto->precio_unitario }}',
            cantidad_requerida : '{{ $producto->cantidad_requerida }}',
            cantidad_recibida : '{{ $producto->cantidad_recibida }}',
            iva : '{{ $producto->producto->iva }}',
            subtotal : '{{ $producto->subtotal }}',
        }

    @endforeach

    var pagos_pedido = [];

    @foreach($pedido->pagos as $pago)
        
        pagos_pedido[{{ $pago->id }}] = {
            forma_pago : '{{ $pago->forma_pago_id }}',
            fecha_pago : "{{ Carbon::parse($pago->fecha_pago)->format('d/m/Y') }}",
            monto : '{{ $pago->monto }}',
            comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $pago->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
        }

    @endforeach

    var facturas = [];

    @foreach($pedido->facturas as $factura)
        
        facturas[{{ $factura->id }}] = {
            folio : '{{ $factura->folio }}',
            fecha_facturacion : "{{ Carbon::parse($factura->fecha_facturacion)->format('d/m/Y') }}",
            subtotal : '{{ $factura->subtotal }}',
            iva : '{{ $factura->iva }}',
            total : '{{ $factura->total }}',
            comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $factura->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
        }

    @endforeach

    Dropzone.options.myDropzone = {
        acceptedFiles: '.jpeg, .jpg, .png, .gif, .svg, .bmp, .xls, .xlsx, .doc, .docx, .pdf, .ppt, .pptx',
        complete: function(file) {
            
            this.removeFile(file);

        },
        success: function(file, response){

            var pathArray = location.href.split( '/' );
            var protocol = pathArray[0];
            var host = pathArray[2];
            var base_url = protocol + '//' + host;

            var aux_tr = '<tr><td><input type="checkbox" name="archivo[]" value="' + response.id + '"/></td><td><a target="_blank" href="' + base_url + '/' + encodeURIComponent(response.path) + '"><i class="fa fa-file"></i> ' + response.original_filename + '</a><br /><div class="row"><div class="col-sm-12"><small class="text-muted"><a href="' + base_url + '/' + encodeURIComponent(response.path) + '">' + response.creado_por.nombre + '</a>&nbsp;&nbsp;|&nbsp;&nbsp;' + response.created_at + '</small></div></div></td></tr>';           
                                                        
            $('#contenedor_archivos').show();
            $('#contenedor_archivos').append(aux_tr);

            console.log(response);
            archivos[response.id] = { 
                original_filename: response.original_filename,
                descripcion:'',
                path: encodeURIComponent(response.path) 
            };
        }
    };

</script>

@endsection