@extends('sistema.layouts.master')

@section('title', 'Conservadores')

@section('attr_navlink_5', 'class="active"')

@section('breadcrumb_title', 'Conservadores')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Conservadores</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.conservadores.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.conservadores.iboxes.tabla')
	</div>
</div>

@include('sistema.conservadores.modals.create')
@include('sistema.conservadores.modals.mtto')
@include('sistema.conservadores.modals.suspender')
@include('sistema.conservadores.modals.reactivar')
@endsection

@section('assets_bottom')
<script type="text/javascript">
	conservadores = [];
	@foreach($conservadores as $conservador)
		conservadores[{{ $conservador->id }}] = {
			modelo: '{{ $conservador->modelo->modelo }}',
		}
	@endforeach

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
	
	$('#fecha_mtto').datepicker({
	    startView: 3,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    endDate: '0d',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});
</script>
@endsection
