<div class="modal inmodal" id="modalConservadorEdit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"> Editar Conservador</h4>
			</div>
			{!! Form::open(['method' => 'PUT', 'url' => url('conservadores/' . $conservador->id), 'enctype' => 'multipart/form-data',  'novalidate' => ''] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
				            <div class="row">

				            	<div class="form-group col-sm-4 @if ($errors->first('id_codigo_edit') !== "") has-error @endif">
						            {!! Form::label('id_codigo_edit', 'ID Sistema', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-link"></i></span>
						            {!! Form::text('id_codigo_edit', $conservador->id_codigo, ['class' => 'form-control positive-integer', 'placeholder' => 'ID Sistema']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('id_codigo_edit')}}</span>
						        </div>

					            <div class="form-group col-sm-4 @if ($errors->first('modelo_edit') !== "") has-error @endif">
						            {!! Form::label('modelo_edit', 'Modelo del Conservador', ['class' => 'control-label']) !!}
						            {!! Form::select('modelo_edit', \App\Models\ModeloConservador::all()->pluck('modelo', 'id')->prepend('','')->toArray(), $conservador->modelo_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Modelo', 'onchange' => 'cargarProducto(this.value)'] ) !!}
						            <span class="help-block">{{ $errors->first('modelo_edit')}}</span>
						        </div>

						         <div class="form-group col-sm-4 @if ($errors->first('numero_serie_edit') !== "") has-error @endif">
						            {!! Form::label('numero_serie_edit', 'Numero de Serie', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						            {!! Form::text('numero_serie_edit', $conservador->numero_serie, ['class' => 'form-control', 'placeholder' => 'Numero de Serie']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('numero_serie_edit')}}</span>
						        </div>

 								<div class="form-group col-sm-4 @if ($errors->first('producto_id') !== "") has-error @endif">
 								   {!! Form::label('producto', 'Producto', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-cubes"></i></span>
 								   		{!! Form::text('nombre_producto', $conservador->producto->nombre, ['class' => 'form-control','readonly' => 'readonly']) !!}
 								   		<input type="hidden" name="producto_id" value="{{$conservador->producto_id}}"/>
						       		</div>
						       		<span class="help-block">{{ $errors->first('producto_id')}}</span>
						        </div>
						        <div class="form-group col-sm-4">
 								   {!! Form::label('capacidad', 'Capacidad', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span>
 								   		{!! Form::text('capacidad', $conservador->modelo->capacidad, ['class' => 'form-control','readonly' => 'readonly']) !!}
 								   	</div>
						        </div>

						        <div class="form-group col-sm-4 @if ($errors->first('frecuencia_edit') !== "") has-error @endif">
						            {!! Form::label('frecuencia_edit', 'Frecuencia Mantenimiento', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-wrench"></i></span>
						            {!! Form::text('frecuencia_edit', $conservador->frecuencia_mantenimiento, ['class' => 'form-control positive-integer', 'placeholder' => 'días']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('frecuencia_edit')}}</span>
						        </div>

						        <div class="form-group col-sm-12 @if ($errors->first('comentarios_edit') !== "") has-error @endif">
						            {!! Form::label('comentarios_edit', 'Comentarios', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-commenting-o"></i></span>
						            {!! Form::textarea('comentarios_edit', $conservador->comentarios, ['id' => 'comentarios_edit', 
						            'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('comentarios_edit')}}</span>
						        </div>

				            </div>
						</div>				
					</div>

					<div class="row">					
				
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_conservador_edit'))
		$('#modalConservadorEdit').modal('show');
	@endif

	function cargarProducto(modelo_id) {

		if(modelo_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/conservadores')}}',
	            data:{ 'type': '1', 'modelo_id': modelo_id, '_token': '{{ csrf_token()}}' },
	            success: function(d) {
	            	$('#modalConservadorEdit').find('input[name=producto_id]').val(d['id']);
	            	$('#modalConservadorEdit').find('input[name=nombre_producto]').val(d['nombre']);
					$('#modalConservadorEdit').find('input[name=capacidad]').val(d['capacidad']);
	            }
	        });
		} else {
			$('#modalConservadorEdit').find('input[name=producto_id]').val('');
			$('#modalConservadorEdit').find('input[name=nombre_producto]').val('');
			$('#modalConservadorEdit').find('input[name=capacidad]').val('');
		}

	}
	
</script>	
@endsection