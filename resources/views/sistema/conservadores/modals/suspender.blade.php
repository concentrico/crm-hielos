<div class="modal inmodal" id="modalConservadorSuspender" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					
					<h4 class="text-danger modal-title" style="color:#ed5565;"><i class="fa fa-trash modal-icon"></i> <BR/>Desactivar Conservador</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-success">¿Estas seguro de que deseas desactivar el conservador?</h2>
				            <h3 style="font-weight: 300;">Se cambiará Status a <strong>Fuera de Servicio</strong> y se desconsignará del establecimiento al que esté asignado.</h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('conservadores' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-arrow-down"></i> Suspender</button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalConservadorSuspender(id){
		
		if ( $('#modalConservadorSuspender').find('form').attr('action') == "{{ url('conservadores') }}/" + id){
			
		}else{
			$('#modalConservadorSuspender').find('form').attr('action', "{{ url('conservadores') }}/" + id)
		}

		$('#modalConservadorSuspender').modal('show');
	}

</script>
@endsection
