<div class="modal inmodal" id="modalConservadorCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"> Registrar Conservadores </h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('conservadores'), 'enctype' => 'multipart/form-data',  'novalidate' => ''] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
				            <div class="row">
				            	<div class="form-group col-sm-4 @if ($errors->first('id_codigo') !== "") has-error @endif">
						            {!! Form::label('id_codigo', 'ID Sistema *', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-link"></i></span>
						            {!! Form::text('id_codigo', null, ['class' => 'form-control positive-integer', 'placeholder' => 'ID Sistema']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('id_codigo')}}</span>
						        </div>

					            <div class="form-group col-sm-4 @if ($errors->first('modelo') !== "") has-error @endif">
						            {!! Form::label('modelo', 'Modelo del Conservador *', ['class' => 'control-label']) !!}
						            {!! Form::select('modelo', \App\Models\ModeloConservador::all()->pluck('modelo', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Modelo', 'onchange' => 'cargarProducto(this.value)'] ) !!}
						            <span class="help-block">{{ $errors->first('modelo')}}</span>
						        </div>

						        <div class="form-group col-sm-4 @if ($errors->first('numero_serie') !== "") has-error @endif">
						            {!! Form::label('numero_serie', 'Numero de Serie *', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						            {!! Form::text('numero_serie', null, ['class' => 'form-control', 'placeholder' => 'Numero de Serie']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('numero_serie')}}</span>
						        </div>

 								<div class="form-group col-sm-4 @if ($errors->first('producto_id') !== "") has-error @endif">
 								   {!! Form::label('producto', 'Producto', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-cubes"></i></span>
 								   		{!! Form::text('nombre_producto', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
 								   		<input type="hidden" name="producto_id" value=""/>
						       		</div>
						       		<span class="help-block">{{ $errors->first('producto_id')}}</span>
						        </div>
						        <div class="form-group col-sm-4">
 								   {!! Form::label('capacidad', 'Capacidad', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span>
 								   		{!! Form::text('capacidad', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
 								   	</div>
						        </div>
						        {{--
						        <div class="form-group col-sm-4 @if ($errors->first('precio_venta') !== "") has-error @endif">
 								   {!! Form::label('precio_venta', 'Precio de Venta Default', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon">$</span>
 								   {!! Form::text('precio_venta', null, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
 								   </div>
						           <span class="help-block">{{ $errors->first('precio_venta')}}</span>
						        </div>
								--}}

						        <div class="form-group col-sm-4 @if ($errors->first('frecuencia') !== "") has-error @endif">
						            {!! Form::label('frecuencia', 'Frecuencia Mantenimiento *', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-wrench"></i></span>
						            {!! Form::text('frecuencia', null, ['class' => 'form-control positive-integer', 'placeholder' => 'días']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('frecuencia')}}</span>
						        </div>

						        <div class="form-group col-sm-12 @if ($errors->first('comentarios') !== "") has-error @endif">
						            {!! Form::label('comentarios', 'Comentarios', ['class' => 'control-label']) !!}
 								   <div class="input-group" style="width: 100%;">
										<span class="input-group-addon"><i class="fa fa-commenting-o"></i></span>
						            {!! Form::textarea('comentarios', null, ['id' => 'comentarios', 
						            'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('comentarios')}}</span>
						        </div>

				            </div>
						</div>				
					</div>

					<div class="row">					
				
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_conservador_create'))
		$('#modalConservadorCreate').modal('show');
	@endif

	function cargarProducto(modelo_id) {

		if(modelo_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/conservadores')}}',
	            data:{ 'type': '1', 'modelo_id': modelo_id, '_token': '{{ csrf_token()}}' },
	            success: function(d) {
	            	$('#modalConservadorCreate').find('input[name=producto_id]').val(d['id']);
	            	$('#modalConservadorCreate').find('input[name=nombre_producto]').val(d['nombre']);
					$('#modalConservadorCreate').find('input[name=capacidad]').val(d['capacidad']);
	            }
	        });
		} else {
			$('#modalConservadorCreate').find('input[name=producto_id]').val('');
			$('#modalConservadorCreate').find('input[name=nombre_producto]').val('');
			$('#modalConservadorCreate').find('input[name=capacidad]').val('');
		}

	}
	
</script>	
@endsection