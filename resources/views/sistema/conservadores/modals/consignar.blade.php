<div class="modal inmodal" id="modalConsignarConservador" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-map modal-icon"></i> <BR/>Consignar Conservador</h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('conservadores/'.$conservador->id.'/consignar'), 'novalidate' => ''] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-7 @if ($errors->first('cliente') !== "") has-error @endif">
				            {!! Form::label('conservador_cliente', 'Buscar Cliente', ['class' => 'control-label text-center']) !!}
				            {!! Form::select('conservador_cliente', \App\Models\Cliente::all()->sortBy('nombre_corto')->pluck('nombre_corto', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Cliente', 'onchange' => 'cargarEstablecimientos(this.value)'] ) !!}
				        </div>

				        <div class="form-group col-sm-5 @if ($errors->first('conservador_establecimiento') !== "") has-error @endif">
				            {!! Form::label('conservador_establecimiento', 'Establecimiento', ['class' => 'control-label text-center']) !!}
			            	{!! Form::select('conservador_establecimiento',['' => ''], null, ['id' => 'conservador_establecimiento','class' => 'col-xs-12 selectdp'] ) !!}
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Aceptar</button>
				</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>

<script type="text/javascript">

	function cargarEstablecimientos(cliente_id){
		if(cliente_id != '' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/establecimientos')}}',
	            data:{ 'type': '4', 'cliente_id': cliente_id, '_token': '{{ csrf_token()}}' },
	            beforeSend: function() {
			        /*$('#conservador_establecimiento').select2({
					  placeholder: 'Cargando sucursales'
					}).trigger('change');*/

					$("#conservador_establecimiento").empty();
	            	$("#conservador_establecimiento").dropdown('clear');

	            	$("#conservador_establecimiento").dropdown().addClass('loading');
	            	$("#conservador_establecimiento").dropdown('refresh');
		        },
	            success: function(d) {
	            	//$("#conservador_establecimiento").select2('destroy').empty().select2({placeholder: 'Seleccione',data:d});
	            	//$("#conservador_establecimiento").removeAttr('disabled');
	            	html='';

				    $.each(d, function(key, value) {
			            html += "<option value=" + value['id']  + ">" +value['text'] + "</option>"
			        });
			        $("#conservador_establecimiento").append(html);
			        $("#conservador_establecimiento").dropdown().removeClass('loading');
	            	$("#conservador_establecimiento").dropdown('refresh');
	            	$("#conservador_establecimiento").dropdown();
	            }
	        });
		} else {
			//$("#conservador_establecimiento").select2('destroy').empty();
	        //$("#conservador_establecimiento").attr('disabled','disabled');
	        $("#conservador_establecimiento").empty();
	        $("#conservador_establecimiento").dropdown('clear');
		}
	}
</script>