<div class="modal inmodal" id="modalRegistrarMtto" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-flag modal-icon"></i> <BR/>Registrar Mantenimiento</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('conservadores')] ) !!}
				{!! Form::hidden('conservador_id') !!}
				<div class="modal-body">

		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('fecha_mtto') !== "") has-error @endif">
				            {!! Form::label('fecha_mtto', 'Fecha Mantenimiento', ['class' => 'control-label text-center']) !!}
				            <div class="input-group date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				            	{!! Form::text('fecha_mtto', null, ['id'=>'fecha_mtto','class' => 'form-control','placeholder' => 'dd/mm/yyyy', 'data-mask' => '99/99/9999'] ) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('fecha_mtto')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_registrar_mtto'))
		showModalRegistrarMtto({{ Input::old('conservador_id') }} );
	@endif
	@if(session('mtto_success'))
		swal('Correcto', "Se ha registrado el Mantenimiento del Conservador.", "success");
	@endif
	
	function showModalRegistrarMtto(conservador_id){
		
		if ( $('#modalRegistrarMtto').find('form').attr('action') == "{{url('conservadores')}}/"+conservador_id+"/mtto" ){
			// Dejar en blanco, es cuando se recarga la pantalla.
		
		}else{
			$('#modalRegistrarMtto').find('form').attr('action', "{{url('conservadores')}}/"+conservador_id+"/mtto" )
			$('#modalRegistrarMtto input[name="conservador_id"]').val( conservador_id );
		}

		$('#modalRegistrarMtto').modal('show');
	}

</script>
@endsection