<div class="modal inmodal" id="modalStatusConservador" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title text-success"><i class="fa fa-refresh modal-icon"></i> <BR/>Cambiar Status</h4>
			</div>
			{!! Form::open(['method' => 'PUT', 'url' => url('conservadores/'.$conservador->id.'/status'), 'novalidate' => ''] ) !!}

				<input type="hidden" name="status_change" id="status_change" value="">
				<div class="modal-body">

				<h3 class="text-center" id="text_body_status"></h3>

				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="submit" class="btn btn-primary"><i class="fa fa-save"></i> Cambiar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_status'))
		showModalRegistrarMtto({{ Input::old('conservador_id') }} );
	@endif
	@if(session('status_success'))
		swal('Correcto', "Se ha cambiado el status satisfactoriamente.", "success");
	@endif

</script>
@endsection