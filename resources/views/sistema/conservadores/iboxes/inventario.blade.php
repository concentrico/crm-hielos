<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-snowflake-o"></i> Status Inventario </h5>
        <a class="text-success pull-right" onclick="guardarDatosConservador();">
            <i class="fa fa-floppy-o"></i> Guardar
        </a>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">

        <div class="col-lg-12">
            <div id="nivel_conservador"></div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">

                <div class="form-group col-sm-12">
                   {!! Form::label('producto', 'Producto', ['class' => 'control-label']) !!}
                   <div class="input-group" style="width: 100%;">
                        <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                        {!! Form::text('nombre_producto', $conservador->producto->nombre , ['class' => 'form-control','readonly' => 'readonly']) !!}
                        <input type="hidden" name="producto_id" value=""/>
                    </div>
                </div>
                <div class="form-group col-sm-4">
                   {!! Form::label('capacidad_actual', 'Capacidad Actual', ['class' => 'control-label']) !!}
                   <div class="input-group" style="width: 100%;">
                        <span class="input-group-addon"><i class="fa fa-battery-half"></i></span>
                        {!! Form::text('capacidad_actual', $conservador->capacidad_actual, ['class' => 'form-control','readonly' => 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group col-sm-4">
                   {!! Form::label('capacidad_maxima', 'Capacidad Máxima', ['class' => 'control-label']) !!}
                   <div class="input-group" style="width: 100%;">
                        <span class="input-group-addon"><i class="fa fa-battery-full"></i></span>
                        {!! Form::text('capacidad_maxima', $conservador->modelo->capacidad, ['class' => 'form-control','readonly' => 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group col-sm-4">
                   {!! Form::label('corte', '&nbsp;', ['class' => 'control-label']) !!}
                  <div class="input-group" style="width: 100%;">
                    <button type="button" class="btn btn-block btn-sm btn-success" onclick="realizarCorte();">Realizar Corte</button>
                  </div>
                </div>
            </div>
        </div>

	    
    </div>
</div>