<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-history"></i> Historial Movimientos </h5>
    </div>
    
    <div class="ibox-content" style="min-height: 512px;max-height: 500px; overflow-y: scroll;">

        @if(count($conservador->logs) > 0)

            <div class="table-responsive">
                <table class="table table-hover issue-tracker">
                    <thead>
                        <th>Tipo</th>
                        <th>Mensaje</th>
                        <th>Usuario</th>
                        <th>Fecha y Hora</th>
                    </thead>
                    <tbody>
                        @foreach($conservador->logs as $log)
                        <tr>
                            <td>
                                <span class="label {{ ($log->tipo == 'status' ? 'label-info' : 'label-warning' ) }}">{{ ($log->tipo == 'status' ? 'Cambio Status' : 'Mantenimiento' ) }}</span>
                            </td>
                            <td class="issue-info">
                                {!!$log->comentarios!!}
                            </td>
                            <td>{{$log->usuario->nombre}}</td>
                            <td>{{ $log->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="alert alert-warning text-center">
                No existen logs registrados!
            </div>
        @endif
    </div>
</div>
