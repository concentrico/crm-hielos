 <div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalConservadorCreate">
			<i class="fa fa-plus"></i> Conservador
		</a>
        <h5><i class="fa fa-list"></i> Lista de conservadores</h5>
    </div>
    <div class="ibox-content">

    	<div class="row m-b-lg">
			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-snowflake-o fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Total de <BR/>Conservadores </h4>
			                <h1 class="font-bold">{{ $total_conservadores }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-map fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Establecimiento con más Conservadores </h4><BR/>
			                <h3 class="font-bold" style="margin-top:8px;">{{ $establecimiento_mas_conservadores }}</h3>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-flag fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Requieren Mantenimiento </h4>
			                <h1 class="font-bold">{{ $conservadores_mantenimiento }}</h1>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $conservadores->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableConservadores" name="dataTableConservadores" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" style="min-width: 75px;">{!! getHeaderLink('IDCODIGO', 'ID') !!}</th>
						<th data-type="html">{!! getHeaderLink('NUMSERIE', 'Número de Serie') !!}</th>
						<th data-type="html">{!! getHeaderLink('MODELO', 'Modelo') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('STATUS', 'Status') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('ESTABLECIMIENTO', 'Establecimiento Asignado') !!}</th>
						<th data-type="html" data-breakpoints="sm">Mantenimiento</th>
						<!--
						<th data-type="html" data-breakpoints="sm">{{-- getHeaderLink('INVENTARIO', 'Inventario Disponible') --}}</th>-->
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('FRECMTTO', 'Frecuencia Mantenimiento') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html" data-breakpoints="sm"></th>
						<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($conservadores as $conservador)
	                    <tr class="@if($conservador->deleted_at != NULL) warning @endif">
	                        <td class="text-center">{{ $conservador->id_codigo }}</td>
	                        <td><a href="{{ url('conservadores/'.$conservador->id) }}">{{ $conservador->numero_serie }}</a></td>
	                        <td class="text-center">{{ $conservador->modelo->modelo }}</td>
		                    <td class="text-center">
		                    	@php
		                    		switch ($conservador->status) {
		                    			case 'stand_by':
		                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Stand By</span>';
		                    				break;
		                    			case 'en_consignacion':
		                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">En Consignación</span>';
		                    				break;
		                    			case 'en_reparacion':
		                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Reparación</span>';
		                    				break;
		                    			case 'fuera_servicio':
		                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">Fuera de Servicio</span>';
		                    				break;
		                    			default:
		                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">N.D.</span>';
		                    				break;
		                    		}
		                    	@endphp
		                    </td>
	                        <td>
	                        	@if ($conservador->cliente_establecimiento_id != null)
	                        		<a href="{{ url('establecimientos/'.$conservador->establecimiento->id) }}">{{ $conservador->establecimiento->sucursal }} ({{ $conservador->cliente->nombre_corto }})</a>
	                       		@else
	                       			No Asignado
	                       		@endif
		                    </td>
		                    <td class="text-center">

		                    @if($conservador->ultimo_mantenimiento != null) 
		                    	@php 
		                    	$ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%Y-%m-%d');
		                    	$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
		                    	$diff = diferencia_fechas($ult_mtto, $now);
		                    	@endphp

		                    	<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="{{fecha_to_human($conservador->ultimo_mantenimiento,false)}}">
		                    	
		                    	@if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false )    Hoy
		                    	@else
		                    		Hace {{$diff}}
		                    	@endif

		                    	</span>
		                    @endif
		                    </td>
		                    <!--
		                    <td class="text-center">
	                    		<span class="donut" data-peity='{ "fill": ["green", "#eeeeee"], "innerRadius": 4, "radius": 10 }'>{{--$conservador->inventarioDisponible() --}}</span>
	                    		<p>{{-- $conservador->inventarioDisponible() --}}</p>
		                    </td>
		                    -->
	                        <td>{{ $conservador->frecuencia_mantenimiento }} días</td>
	                        <td>{{ $conservador->created_at }}</td>
	                        <td>{!! $conservador->creado_por->liga() !!}</td>
	                        <td>{{ $conservador->updated_at }}</td>
	                        <td>{!! $conservador->actualizado_por->liga() !!}</td>
	                        <td class="text-center">
		                    	@if( $conservador->necesita_mtto() )
			                    	<div class="col-sm-6 text-center">
			                    		<a onclick="showModalRegistrarMtto({{$conservador->id}})" style="color:inherit; line-height: 40px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Registrar Mantenimiento">
			                    		<i class="fa fa-lg fa-flag text-danger" aria-hidden="true"></i>
			                    		</a>
			                    	</div>
		                    	@endif
		                    </td>
	                        <td class="center">
		                        <div class="btn-group pull-right">
			                        @if($conservador->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalConservadorReactivar({{ $conservador->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
			                        @else
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalConservadorSuspender({{ $conservador->id }});">
			                        	<i class="fa fa-trash"></i>
			                        </button>
			                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $conservadores->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>