<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-map"></i> Establecimiento Asignado </h5>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;">
	    <div class="row">
		    <div class="col-sm-6">
				<h4 class="text-success">Cliente</h4>
				@if($conservador->cliente_id != null)
					
					<a href="{{ url('clientes/'.$conservador->cliente->id) }}">{{ $conservador->cliente->nombre_corto }}</a>
			    @else
			    	Sin Cliente Asignado
			    @endif
		    </div>
		    <div class="col-sm-6">
				<h4 class="text-success">Sucursal</h4>
				@if($conservador->cliente_establecimiento_id != null)
					<a href="{{ url('establecimientos/'.$conservador->establecimiento->id) }}">{{ $conservador->establecimiento->sucursal }}</a>
			    @else
			    	Sin Establecimiento Asignado
			    @endif
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
		    <div class="col-sm-12">

		    	@if($conservador->cliente_establecimiento_id != NULL)
                    <a class="btn btn-primary btn-rounded btn-block" onclick="retirarConservador();"><i class="fa fa-undo"></i> Retirar de Establecimiento</a>
                @else
                	<a class="btn btn-primary btn-rounded btn-block" data-toggle="modal" data-target="#modalConsignarConservador"><i class="fa fa-plus"></i> Consignar a Establecimiento</a>
                @endif
		    	
		    </div>
	    </div>
    </div>
</div>