<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins filtros-res">
		    <div class="ibox-title">
		        <h5><i class="fa fa-filter"></i> Filtrar resultados</h5>
	            <div class="ibox-tools pull-right">
	                <a class="collapse-link">
	                    <i class="fa fa-chevron-up"></i>
	                </a>
	            </div>
		    </div>
		    <div class="ibox-content">
				{!! Form::open(['method' => 'GET',  'url' => url('conservadores')] ) !!}
					<input type="hidden" name="orderBy" value="{{ Input::get('orderBy') }}"/>
					<input type="hidden" name="sort" value="{{ Input::get('sort') }}"/>
		            <div class="row">
		            	<div class="form-group col-xs-3 col-sm-2 col-md-2 col-lg-1">
				            {!! Form::label('f_num_resultados', 'Mostrar', ['class' => 'control-label']) !!}
			            	{!! Form::select('f_num_resultados', ['' => '', 5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100, 200 => 200], Input::get('f_num_resultados', 50), 
			            	['class' => 'form-control selectdp', 'data-placeholder' => 'No. Resultados'] ) !!}
				        </div>
			            <div class="form-group col-sm-8 col-md-9 col-lg-4">
				            {!! Form::label('f_busqueda', 'Búsqueda', ['class' => 'control-label']) !!}
				            {!! Form::text('f_busqueda', Input::get('f_busqueda'), ['class' => 'form-control', 'placeholder' => 'Modelo, Número Serie']) !!}
				        </div>
			            <div class="form-group col-sm-4 col-md-3 col-lg-2">
				            {!! Form::label('f_estatus', 'Status', ['class' => 'control-label']) !!}
			            	{!! Form::select('f_estatus', ['todos' => 'Ver todos', 'stand_by' => 'Stand By', 'en_consignacion' => 'En Consignación','en_reparacion' => 'Reparación','en_mantenimiento' => 'Mantenimiento','fuera_servicio' => 'Fuera de Servicio', 'deleted' => 'Eliminado'], 
			            	Input::get('f_estatus', 'todos'), ['class' => 'form-control selectdp', 'placeholder' => 'Status'] ) !!}
				        </div>

				        <div class="form-group col-xs-9 col-sm-6 col-md-4 col-lg-3">
				            {!! Form::label('f_cliente', 'Cliente', ['class' => 'control-label']) !!}
	            			{!! Form::select('f_cliente', \App\Models\Cliente::orderBy('nombre_corto')->get()->pluck('nombre_corto', 'id')->toArray(), Input::get('f_cliente'), ['class' => 'form-control chosen-select', 'placeholder' => 'Ver Todos'] ) !!}
				        </div>


			            <div class="form-group col-sm-4 col-md-3 col-lg-2">
				            {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
							<button type="submit" class="btn btn-primary col-xs-12"><i class="fa fa-filter"></i> Filtrar</button>
				        </div>
		            </div>			
				{!! Form::close() !!}
			</div>
	    </div>
	</div>
</div>
