<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-eye"></i> Información General </h5>
		<a class="text-success pull-right" onclick="showModalConservadorEdit();">
			<i class="fa fa-edit"></i> Editar
		</a>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-4">
				<h4 class="text-success">ID Sistema</h4>
			    {{ $conservador->id_codigo }}
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Modelo</h4>
			    {{ $conservador->modelo->modelo }}
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Número de Serie</h4>
			    {{ $conservador->numero_serie }}
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
	    	<div class="col-sm-4">
				<h4 class="text-success">Producto</h4>
			    {{ $conservador->modelo->producto->nombre }}
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Capacidad</h4>
			    {{ $conservador->modelo->capacidad }} bolsas
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Status</h4>
				@php
            		switch ($conservador->status) {
            			case 'stand_by':
            				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Stand By</span>';
            				break;
            			case 'en_consignacion':
            				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">En Consignación</span>';
            				break;
            			case 'en_reparacion':
            				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Reparación</span>';
            				break;
            			case 'en_mantenimiento':
            				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Mantenimiento</span>';
            				break;
            			case 'fuera_servicio':
            				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">Fuera de Servicio</span>';
            				break;
            			default:
            				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">N.D.</span>';
            				break;
            		}
            	@endphp
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
	    	<div class="col-sm-4">
				<h4 class="text-success">Frecuencia Mtto</h4>
				{{ $conservador->frecuencia_mantenimiento }} días
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Último Mtto</h4>
			    
			    @if($conservador->ultimo_mantenimiento != null) 

			    	@if( $conservador->necesita_mtto() )
				    	<div class="col-sm-6 col-xs-6" style="padding: 0px;">
		            			
		            		@php 
		                	$ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%Y-%m-%d');
		                	$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
		                	$diff = diferencia_fechas($ult_mtto, $now);
		                	$formatted = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%A %d %B %Y');
		                	@endphp
		                	<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="{{$formatted}}">
			                	@if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false ) Hoy
			                	@else Hace {{$diff}} @endif
		                	</span>

		            	</div>
		            	<div class="col-sm-6 col-xs-6 text-center">
		            		<a onclick="showModalRegistrarMtto({{$conservador->id}})" style="color:inherit;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Registrar Mantenimiento">
		            		<i class="fa fa-lg fa-flag text-danger" aria-hidden="true"></i>
		            		</a>
		            	</div>
		            @else
		            	<div class="col-sm-12">
		            		@php 
		                	$ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%Y-%m-%d');
		                	$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
		                	$diff = diferencia_fechas($ult_mtto, $now);
		                	$formatted = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%A %d %B %Y');
		                	@endphp
		                	<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="{{$formatted}}">
			                	@if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false ) Hoy
			                	@else Hace {{$diff}} @endif
		                	</span>
		            	</div>
		        	@endif
                	
                @else
                	No hay registro
                @endif
		    </div>
		    <div class="col-sm-4">
				<h4 class="text-success">Comentarios</h4>
				@if($conservador->comentarios)
					<span>{!! nl2br(e($conservador->comentarios))!!}</span>
				@else
	               	No hay comentarios.
				@endif
		    </div>
	    </div>
	    
	    <hr style="margin-top: 15px; margin-bottom: 15px;"/>

	    <div class="row">
		    <div class="col-sm-6">
				<h4 class="text-success">Cliente</h4>
				@if($conservador->cliente_id != null)
					
					<a href="{{ url('clientes/'.$conservador->cliente->id) }}">{{ $conservador->cliente->nombre_corto }}</a>
			    @else
			    	Sin Cliente Asignado
			    @endif
		    </div>
		    <div class="col-sm-6">
				<h4 class="text-success">Sucursal</h4>
				@if($conservador->cliente_establecimiento_id != null)
					<a href="{{ url('establecimientos/'.$conservador->establecimiento->id) }}">{{ $conservador->establecimiento->sucursal }}</a>
			    @else
			    	Sin Establecimiento Asignado
			    @endif
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
		    <div class="col-sm-12">

		    	@if($conservador->cliente_establecimiento_id != NULL)
                    <a class="btn btn-primary btn-rounded btn-block" onclick="retirarConservador();"><i class="fa fa-undo"></i> Retirar de Establecimiento</a>
                @else
                	<a class="btn btn-primary btn-rounded btn-block" data-toggle="modal" data-target="#modalConsignarConservador"><i class="fa fa-plus"></i> Consignar a Establecimiento</a>
                @endif
		    	
		    </div>
	    </div>

	    <hr style="margin-top: 15px; margin-bottom: 15px;"/>

	    <div class="row">
	    	<div class="col-sm-12">
			   	<h4 class="text-success">Cambiar Status</h4>
	                
			    @if($conservador->status == 'stand_by' || $conservador->status == 'en_consignacion')
			    	<button type="button" class="btn btn-w-m btn-danger" onclick="cambiarStatus('en_reparacion')">
	                <i class="fa fa-wrench"></i>&nbsp;&nbsp;En Reparación</button>

	                <button type="button" class="btn btn-w-m btn-default" onclick="cambiarStatus('fuera_servicio')">
	                <i class="fa fa-minus-circle"></i>&nbsp;&nbsp;Fuera de Servicio</button>
			    @elseif($conservador->status == 'en_reparacion')
			    	<button type="button" class="btn btn-w-m btn-success" onclick="cambiarStatus('stand_by')">
	                <i class="fa fa-mail-reply"></i>&nbsp;&nbsp;Re-Activar</button>

	                <button type="button" class="btn btn-w-m btn-default" onclick="cambiarStatus('fuera_servicio')">
	                <i class="fa fa-minus-circle"></i>&nbsp;&nbsp;Fuera de Servicio</button>
	            @elseif($conservador->status == 'fuera_servicio')
	            	<button type="button" class="btn btn-w-m btn-success" onclick="cambiarStatus('stand_by')">
	                <i class="fa fa-mail-reply"></i>&nbsp;&nbsp;Re-Activar</button>
			    @endif
		    </div>
		</div>
		<hr style="margin-top: 15px; margin-bottom: 15px;" />
	    <div class="row">
		    <div class="col-sm-6">
				<small class="text-muted">Creado: {{ $conservador->created_at }} <br /> 
				<a href="{{ url('usuarios/' . $conservador->created_by) }}">{{ $conservador->creado_por->nombre }}</a></small><br />
		    </div>
		    <div class="col-sm-6">
				<small class="text-muted">Actualizado: {{ $conservador->updated_at }} <br /> 
				<a href="{{ url('usuarios/' . $conservador->updated_by) }}">{{ $conservador->actualizado_por->nombre }}</a></small><br />
		    </div>
	    </div>
    </div>
</div>


<script type="text/javascript">

	function showModalConservadorEdit() {

		if ( $('#modalConservadorEdit').find('form').attr('action') == "{{ url('conservadores') }}/{{ $conservador->id }}"){
		}else{
			$('#modalConservadorEdit').find('form').attr('action', "{{ url('usuarios') }}/{{ $conservador->id }}")
			$('#modalConservadorEdit').find('h3').html("<b>{{ $conservador->modelo->modelo }}</b>.");	
		}
		$('#modalConservadorEdit').modal('show');
	}

	function showModalConservadorDelete(){
		
		if ( $('#modalConservadorDelete').find('form').attr('action') == "{{ url('conservadores') }}/{{ $conservador->id }}"){
		}else{
			$('#modalConservadorDelete').find('form').attr('action', "{{ url('usuarios') }}/{{ $conservador->id }}")
			$('#modalConservadorDelete').find('h3').html("<b>{{ $conservador->modelo->modelo }}</b>.");	
		}
		$('#modalConservadorDelete').modal('show');
	}

	function showModalConservadorReactivar(){
		
		if ( $('#modalConservadorReactivar').find('form').attr('action') == "{{ url('conservadores') }}/{{ $conservador->id}}/reactivar"){
		}else{
			$('#modalConservadorReactivar').find('form').attr('action', "{{ url('conservadores') }}/{{ $conservador->id }}/reactivar")
			$('#modalConservadorReactivar').find('h3').html("<b>{{ $conservador->modelo->modelo }}</b>.");	
		}
		$('#modalConservadorReactivar').modal('show');
	}

</script>