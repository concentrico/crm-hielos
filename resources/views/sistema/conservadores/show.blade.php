@extends('sistema.layouts.master')

@section('title', 'Conservador '.$conservador->id_codigo)

@section('attr_navlink_5', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb_title', $conservador->numero_serie.' ('.$conservador->id_codigo.')' )

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('conservadores') }}">Conservadores</a></li>
<li class="active"><strong>{{ $conservador->numero_serie }}</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12 col-md-7" style="padding: 0 5px;">
		@include('sistema.conservadores.iboxes.datos')
	</div>
	{{--
	<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
		@include('sistema.conservadores.iboxes.establecimiento')
	</div>
	--}}
	<div class="col-sm-12 col-md-5" style="padding: 0 5px;">
		@include('sistema.conservadores.iboxes.acciones')
	</div>
</div>

@include('sistema.conservadores.modals.edit') 
@include('sistema.conservadores.modals.status')
@include('sistema.conservadores.modals.mtto')
@include('sistema.conservadores.modals.consignar')

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>

<script type="text/javascript">	

	@if(session('show_modal_consignar'))
		$('#modalConsignarConservador').modal('show');
	@endif
	@if(session('consignacion_success'))
		swal('Correcto', "Se ha consignado correctamente el Conservador.", "success");
	@endif

	@if(session('change_status_success'))
		swal('Correcto', "Se ha cambiado el status satisfactoriamente.", "success");
	@endif

	function retirarConservador() {
		swal({
		  title: "¿Desea retirar el Conservador?",
		  text: "Se desconsignará del Establecimiento y su status cambiará a <b>STAND BY</b>.",
		  type: "warning",
		  showCancelButton: true,
		  html: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/conservadores')}}',
	            data:{ 'type': '3', 'conservador_id': '{{$conservador->id}}', '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	swal("Correcto!", "Se ha retirado el conservador del establecimiento.", "success");

	            	setTimeout(function(){
					    location.reload();
					}, 2000);
	            }
	        });
		  
		});
	}

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('input[type=date]').datepicker({
	    startView: 2,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});

	function cambiarStatus(status) {
		var text_body = '';
		var text_input = '';
		var status_actual = '{{$conservador->status}}';

		if (status == 'en_reparacion') {
			text_body = 'Al cambiar Status a <b>Reparación</b> se desconsignará del establecimiento al que esté asignado el conservador.';
			text_input = 'Tipo de Reparación';

		} else if (status == 'fuera_servicio') {
			text_body = 'Al cambiar Status a <b>Fuera de Servicio</b> se desconsignará del establecimiento al que esté asignado el conservador y será considerada como <strong style="color:#ed5565;">BAJA</strong>.';
			text_input = 'Motivo de baja';

		} else if(status == 'stand_by'){
			text_body = 'Al <b>Re-Activar</b> el conservador se devolverá status a <strong>CONSIGNACIÓN</strong> si es que está asociado a un Establecimiento, de lo contrario se cambiará a <b>STAND BY</b>.';
		}

		$('#text_body_status').html(text_body);
		$('#status_change').val(status);

		$('#modalStatusConservador').modal('show');
	}

	function realizarCorte(){
		swal({
		  title: "Realizar Corte de Inventario",
		  text: "Capacidad Actual (bolsas):",
		  type: "input",
          confirmButtonColor: "#1c84c6",
          confirmButtonText: "Registrar",
          cancelButtonText: "Cancelar",
		  showCancelButton: true,
		  closeOnConfirm: false,
		  animation: "slide-from-top",
		  inputPlaceholder: ""
		},
		function(inputValue){
		  if (inputValue === false) return false;
		  
		  if (inputValue === "") {
		    swal.showInputError("Campo requerido");
		    return false
		  }
		  
		  swal("Correcto!", "Se ha realizado el corte satisfactoriamente.", "success");
		});
	}
	
</script>
@endsection