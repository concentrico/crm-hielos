<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Pedidos</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalEstablecimientoPedidoCreate">
			<i class="fa fa-plus"></i> Agregar Pedido
		</a>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
			    @if(count($establecimiento->pedidos) == 0)
					<div class="alert alert-warning">
						El establecimiento no tiene pedidos registrados!
					</div>
			    @else
					<table class="table table-res table-stripped toggle-arrow-tiny" data-cascade="true" data-page-size="10" data-paging="true">
						<thead>
							<tr>
								<th data-type="html">No. Pedido</th>
								<th data-type="html" data-breakpoints="sm">Fecha de Entrega</th>
								<th data-type="html">Status</th>
								<th data-type="html" data-breakpoints="sm">Chofer Asignado</th>
								<th data-type="html">Total Pedido</th>
								<th data-type="html" data-breakpoints="xlg">Evento</th>
								<th data-type="html" data-breakpoints="xlg">Creado</th>
								<th data-type="html" data-breakpoints="xlg">Creado por</th>
								<th data-type="html" data-breakpoints="xlg">Actualizado</th>
								<th data-type="html" data-breakpoints="xlg">Actualizado por</th>
							</tr>
						</thead>
						<tbody>
		                    @foreach ($establecimiento->pedidos->sortByDesc('created_at') as $pedido)
			                    <tr class="@if($pedido->deleted_at != NULL) warning @endif">
			                        <td>
				                        <a class="text-navy" href="{{ url('pedidos/'.$pedido->id) }}">Pedido #{{ $pedido->id }}</a>
					               	</td>
			                        <td>
										@if($pedido->entregado_at != null)
											{{fecha_to_human($pedido->entregado_at,false) }}
												-
											{{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
										@else
											{{fecha_to_human($pedido->fecha_entrega,false) }}
										@endif
					                </td>

					                <td>
				                    	@php
				                    		switch ($pedido->status) {
				                    			case '1':
				                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">Programado</span>';
				                    				break;
				                    			case '2':
				                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">En Camino</span>';
				                    				break;
				                    			case '3':
				                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Entregado</span>';
				                    				break;
				                    			case '4':
				                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Devolución</span>';
				                    				break;
				                    			case '5':
				                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Cancelado</span>';
				                    				break;
				                    			default:
				                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>';
				                    				break;
				                    		}
				                    	@endphp
					                   
				                    </td>

				                    <td >
				                        {{ $pedido->chofer->nombre }}
					               	</td>

			                        <td>
										${{ number_format($pedido->total(), 2, '.', ',') }}
			                        </td>

			                        <td>
				                        @if($pedido->es_evento == 1)
				                        	Sí
				                        @else
				                        	No
				                        @endif
			                        </td>

			                        <td>{{ $pedido->created_at }}</td>
			                        <td>
				                        <a href="{{ url('usuarios/'.$pedido->creado_por->id) }}">{{ $pedido->creado_por->nombre }}</a>
					               	</td>
			                        <td>{{ $pedido->updated_at }}</td>
			                        <td>
				                        <a href="{{ url('usuarios/'.$establecimiento->actualizado_por->id) }}">{{ $pedido->actualizado_por->nombre }}</a>
					               	</td>
			                    </tr>
							@endforeach
						</tbody>
						<tfoot>
		                  <tr>
		                    <td colspan="6">
		                        <ul class="pagination pull-right"></ul>
		                    </td>
		                  </tr>
		                </tfoot>
					</table>            	        
			    @endif
		    </div>
	    </div>
    </div>
</div>
