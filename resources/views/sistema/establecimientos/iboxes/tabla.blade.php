<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteEstablecimientoCreate">
			<i class="fa fa-plus"></i> Establecimiento
		</a>
        <h5><i class="fa fa-list"></i> Lista de establecimientos</h5>
    </div>
    <div class="ibox-content">

    	<div class="row m-b-lg">
			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-map fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Establecimientos </h4>
			                <h1 class="font-bold">{{ $total_establecimientos }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-star fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Tipo más Popular </h4>
			                <h1 class="font-bold">{{ $tipo_sucursal_maspopular }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-truck fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Zona más Popular </h4>
			                <h1 class="font-bold">{{ $tipo_zona_maspopular }}</h1>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $establecimientos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableEstablecimientos" name="dataTableEstablecimientos" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" style="min-width: 75px;">{!! getHeaderLink('ID', 'ID') !!}</th>
						<th data-type="html">{!! getHeaderLink('NOMBRE', 'Nombre Sucursal') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('CLIENTE', 'Cliente') !!}</th>
						<th data-type="html" data-breakpoints="sm">Tipo(s)</th>
						<th data-type="html" data-breakpoints="sm">Saldo</th>
						<th data-type="html" data-breakpoints="sm">Total Vencido</th>
						<th data-type="html" data-breakpoints="xxlg">Razón Social</th>
						<th data-type="html" data-breakpoints="xxlg">Dirección</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			
			<tbody>
                    @foreach ($establecimientos as $establecimiento)
	                    <tr class="@if($establecimiento->deleted_at != NULL) warning @endif">
	                        <td class="text-center">{{ $establecimiento->id }}</td>
	                        <td><a href="{{ url('establecimientos/'.$establecimiento->id) }}">{{ $establecimiento->sucursal }}</a></td>
	                        <td><a href="{{ url('clientes/'.$establecimiento->cliente->id) }}">{{ $establecimiento->cliente->nombre_corto }}</a></td>
		                    <td>
		                        <span>
									@if(count($establecimiento->tipos) > 0)
										@foreach($establecimiento->tipos as $key => $tipo)
											<span class="label label-primary" style="margin-top: 5px; display:inline-block;">{{ $tipo->nombre }}</span>
										@endforeach
										<br />
									@endif
		                        </span>
		                    </td>
		                    <td class="text-center">
			                    <span>
			                    	${{ $establecimiento->saldo() }} 
				                </span>
		                    </td>
	                        <td class="text-center">
	                        	@if($establecimiento->saldo_porcobrar() > 0.00)
				                    <span class="text-danger">
				                    	${{ $establecimiento->saldo_porcobrar() }} 
					                </span>
					            @else
					            	<span>
				                    	${{ $establecimiento->saldo_porcobrar() }} 
					                </span>
					            @endif
		                    </td>
		                    <td>
		                    @if($establecimiento->datos_fiscales != NULL)
		                    	{{ $establecimiento->datos_fiscales->razon_social }}
		                    @else
		                    	N.D.
		                    @endif
		                    </td>
		                    <td>{{ $establecimiento->direccion() }}</td>
	                        <td>{{ $establecimiento->created_at }}</td>
	                        <td>{!! $establecimiento->creado_por->liga() !!}</td>
	                        <td>{{ $establecimiento->updated_at }}</td>
	                        <td>{!! $establecimiento->actualizado_por->liga() !!}</td>
	                        <td class="center">
		                        <div class="btn-group pull-right">
			                        @if($establecimiento->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalEstablecimientoReactivar({{ $establecimiento->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
			                        @else
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalEstablecimientoSuspender({{ $establecimiento->id }});">
			                        	<i class="fa fa-trash"></i>
			                        </button>
			                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $establecimientos->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>



