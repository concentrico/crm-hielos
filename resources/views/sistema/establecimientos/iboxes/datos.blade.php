<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteEstablecimientoEdit">
					<i class="fa fa-edit"></i> Editar
				</a>

			    <h2 class="text-info">
				    {{ $establecimiento->cliente->nombre_corto }} <small>{{ $establecimiento->sucursal }} </small>
				</h2>
				
				@if(count($establecimiento->tipos) > 0)
					<span style="font-weight: bold;">
						{{ (count($establecimiento->tipos) === 1 ? 'Tipo:' : 'Tipos:' ) }}
					</span>					
					@foreach($establecimiento->tipos as $key => $tipo)
						<span class="label label-success" style="margin-top: 5px; display:inline-block;">{{ $tipo->nombre }}</span>
					@endforeach
					<br />
				@endif

				<br/>

				<span style="font-weight: bold;">Zona: </span>
				@if($establecimiento->zona == null)
					<span class="text-muted"> N.D. </span>
				@else
					<span class="text-danger">{{ $establecimiento->zona->nombre }}</span>
				@endif

				<br/><br/>

				<div class="row">
					<div class="col-sm-12">
						<span style="font-weight: bold;">Nombre del Cliente:</span><br />
						<a href="{{url('clientes/'.$establecimiento->cliente_id)}}">{{ $establecimiento->cliente->nombre_corto }} </a>
					</div>
				</div>

				<br/>
				@if( imprime_direccion($establecimiento) !== "")
					<h4 class="text-success">Dirección</h4>
					<div class="row">
						<div class="col-sm-12">
							<address>
								{!! imprime_direccion($establecimiento) !!}
							</address>
							</div>
					</div>
				@endif

				<h4 class="text-success">Comentarios</h4>

				@if($establecimiento->comentarios)

					<p>{!! $establecimiento->comentarios !!}</p>
				@else
					<p>No hay comentarios</p>
				@endif

			</div>
		</div>
		
		<hr />
	    
	    <div class="row">
		    <div class="col-sm-6">
				<small class="text-muted">Creado:  {{ $establecimiento->created_at }} <br /> 
				<a href="{{ url('usuarios/' . $establecimiento->created_by) }}">{{ $establecimiento->creado_por->nombre }}</a></small><br />
		    </div>
		    <div class="col-sm-6">
				<small class="text-muted">Actualizado: {{ $establecimiento->updated_at }} <br /> 
				<a href="{{ url('usuarios/' . $establecimiento->updated_by) }}">{{ $establecimiento->actualizado_por->nombre }}</a></small><br />
		    </div>
	    </div>
    </div>
</div>
