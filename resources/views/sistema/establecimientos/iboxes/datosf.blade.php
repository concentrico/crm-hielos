<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-gavel"></i> Datos fiscales Establecimiento</h5>
		@if($establecimiento->datos_fiscales == null)
			<a class="text-success pull-right" data-toggle="modal" data-target="#modalEstablecimientoDatosFiscalesCreate">
				<i class="fa fa-plus"></i> Datos fiscales
			</a>
		@endif
    </div>
    <div class="ibox-content">
	    @if($establecimiento->datos_fiscales == null)
			<div class="alert alert-warning" style="margin-top: 10px;">
				El establecimiento no tiene datos fiscales registrados.
			</div>
		@else
			@php $df = $establecimiento->datos_fiscales; @endphp
			<div class="row">
			    <div class="col-sm-12">
					<a class="text-danger pull-right" onclick="showModalEstablecimientoDatosFiscalesDelete({{$df->id}})">
						<i class="fa fa-trash"></i> Eliminar
					</a>
					<a class="text-success pull-right" onclick="showModalEstablecimientoDatosFiscalesEdit({{$df->id}})">
						<i class="fa fa-edit"></i> Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</a>
					<h4 class="text-info">{{ $df->rfc }}</h4>
					<span style="font-weight: bold;">Tipo de persona: </span>
					@if($df->persona == null)
						<span class="text-muted"> N.D. </span>
					@else
						<span class="text-primary"> {{ ucfirst($df->persona) }}</span>
					@endif
					<br />
					<span style="font-weight: bold;">
					@if($df->persona == 'moral')
						Razón social: 
					@else
						Nombre completo: 
					@endif
					</span>
					@if($df->razon_social == null)
						<span class="text-muted"> N.D. </span>
					@else
						<span class="text-primary"> {{ $df->razon_social }}</span>
					@endif
					<br />
					<span style="font-weight: bold;">
						Representante Legal: 
					</span>
					<span class="text-primary"> {{ $df->representante_legal }}</span>
				
					@if( imprime_direccion($df) !== "")
					<h4 class="text-success">Dirección de facturación</h4>
					<address>
						{!! imprime_direccion($df) !!}
					</address>				
					@endif
			    </div>
		    </div>
		@endif
	    
    </div>
</div>