<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-handshake-o"></i> Acuerdos Comerciales</h5>
    </div>
    <div class="ibox-content">
		{!! Form::open(['method' => 'PUT',  'url' => url('establecimientos/' . $establecimiento->id.'/update-acuerdos')] ) !!}
		<div class="row">
			<div class="col-sm-6">
				<h4>Forma de Pago</h4>
				{!! Form::select('forma_pago', App\Models\FormaPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), $establecimiento->forma_pago_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Forma de Pago'] ) !!}
				<span class="help-block text-danger">{{ $errors->first('forma_pago')}}</span>
			</div>
			<div class="col-sm-6">
				<h4>Condiciones de Pago</h4>
				{!! Form::select('condicion_pago', App\Models\CondicionPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), $establecimiento->condicion_pago_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Condiciones de Pago'] ) !!}
				<span class="help-block text-danger">{{ $errors->first('condicion_pago')}}</span>
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-12">
				<div class="table-responsive">
	                <table class="table table-striped table-fixed" id="tablaProductosEstablecimiento">
	                    <thead>
	                    <tr>
	                        <th class="col-xs-8">Producto</th>
	                        <th class="col-xs-4">Precio de Venta</th>
	                    </tr>
	                    </thead>
	                    <tbody id="body_tabla_productos_establecimiento" style="height:auto;">
	                    @if(count($establecimiento->precios) > 0)
							@foreach($establecimiento->precios as $precios)
								<tr>
		                            <td class="col-xs-8">
			                            <div class="input-group" style="width: 100%;">
								           	<span class="input-group-addon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
								           		{!! Form::text('producto[]', $precios->producto->nombre, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
											<input type="hidden" name="producto_id[]" value="{{$precios->producto_id}}"/>
										</div>
										<span class="help-block">{{ $errors->first('producto')}}</span>
		                            </td>
		                            <td class="col-xs-4">
		                            	<div class="input-group" style="width: 100%;">
								           	<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
												{!! Form::text('precio_venta[]', $precios->precio_venta, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
										</div>
										<span class="help-block">{{ $errors->first('precio_venta')}}</span>
									</td>
		                        </tr>
							@endforeach
						@else
							<tr>
								<td class="col-xs-12 text-center"><span class="text-danger">Es necesario primero asignar conservador.</span></td>
							</tr>
						@endif
	                    </tbody>
	                </table>
	            </div>
	        </div>
        	<div class="col-sm-9">
        		<span class="text-info">&nbsp;*Productos en base a Conservadores Asignados.</span>
        	</div>
        	<div class="col-sm-3">
	            <button type="submit" id="btnSaveAcuerdos" class="btn btn-outline btn-primary btn-sm pull-right">
		        	<i class="fa fa-save"></i> Guardar
		        </button>
		    </div>
		</div>
		{!! Form::close() !!}
    </div>
</div>