<div class="ibox float-e-margins">
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				{!! Form::open(['method' => 'POST', 'url' => url('establecimientos/'.$establecimiento->id.'/establecer-ubicacion'), 'novalidate' => '', 'class' => 'form-inline', 'onkeypress' => 'return event.keyCode != 13;'] ) !!}
						<div class="row" id="mapa">
						@php 
							//Example:  Miguel Nieto 612, Centro, Monterrey, México

							$direccion = trim($establecimiento->calle).' '.trim($establecimiento->numero_exterior).', '.trim($establecimiento->colonia).', '.trim($establecimiento->municipio).', '.trim($establecimiento->estado).', México';
						@endphp
						
							<div class="form-group" style="width: 100%; margin-bottom: 15px;">
								<div class="col-sm-10" style="padding-left: 0px;">
									<div class="input-group" style="width: 100%">
										<input type="text" name="direccion2" id="direccion2" class="form-control" placeholder="Buscar por dirección" value="{{$direccion}}" style="width: 100%" />
										<input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$establecimiento->latitud_longitud}}" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="btnBuscarDireccion"><i class="fa fa-search"></i></button>
										</span>
                                    </div>

								</div>
								<div class="col-sm-2" style="padding-left: 0px;">
									<button disabled="" type="submit" id="btnGuardarUbicacion" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
								</div>
	                        </div>
							<div id="map" style="height: 400px;"></div>
						</div>
					
				{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
