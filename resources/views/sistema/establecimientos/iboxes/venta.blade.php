<div class="ibox">
	<div class="ibox-content">

		<div class="row">
			<div class="col-lg-10" style="margin: 0px; padding: 0px;" id="calendar"></div>
			<div class="col-lg-2" style="margin: 0px; padding: 0px;">
				<div class="fc-toolbar fc-header-toolbar">
					<div class="fc-left"><h2>&nbsp;</h2></div>
					<div class="fc-clear"></div>
				</div>
				<div class="fc fc-view fc-month-view fc-basic-view">
						<table>
							<thead class="fc-head">
								<tr>
									<td class="fc-head-container fc-widget-header">
										<div class="fc-row fc-widget-header">
											<table>
												<thead>
													<tr>
														<th class="fc-day-header fc-widget-header fc-mon">
															<span class="text-danger">Total</span>
														</th>
													</tr>
												</thead>
											</table>
										</div>
									</td>
								</tr>
							</thead>
							<tbody class="fc-body">
								<tr>
									<td class="fc-widget-content">
										<div class="fc-scroller fc-day-grid-container" style="">
											<div class="fc-day-grid fc-unselectable">
												<div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 109px;">
													<div class="fc-content-skeleton text-center" style="height: 100%; padding-top: 2.5em;font-size: 1.3em;">
														<span class="text-navy" id="total_semana1">$0.00</span>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fc-widget-content">
										<div class="fc-scroller fc-day-grid-container" style="">
											<div class="fc-day-grid fc-unselectable">
												<div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 109px;">
													<div class="fc-content-skeleton text-center" style="height: 100%; padding-top: 2.5em;font-size: 1.3em;">
														<span class="text-navy" id="total_semana2">$0.00</span>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fc-widget-content">
										<div class="fc-scroller fc-day-grid-container" style="">
											<div class="fc-day-grid fc-unselectable">
												<div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 109px;">
													<div class="fc-content-skeleton text-center" style="height: 100%; padding-top: 2.5em;font-size: 1.3em;">
														<span class="text-navy" id="total_semana3">$0.00</span>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fc-widget-content">
										<div class="fc-scroller fc-day-grid-container" style="">
											<div class="fc-day-grid fc-unselectable">
												<div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 109px;">
													<div class="fc-content-skeleton text-center" style="height: 100%; padding-top: 2.5em;font-size: 1.3em;">
														<span class="text-navy" id="total_semana4">$0.00</span>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fc-widget-content">
										<div class="fc-scroller fc-day-grid-container" style="">
											<div class="fc-day-grid fc-unselectable">
												<div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 109px;">
													<div class="fc-content-skeleton text-center" style="height: 100%; padding-top: 2.5em;font-size: 1.3em;">
														<span class="text-navy" id="total_semana5">$0.00</span>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			
		</div>
		
	</div>
</div>

@section('assets_bottom')
	@parent

<script type="text/javascript">
$(document).ready(function() {
+
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#calendar').fullCalendar('render');
    });

	// ESTABLECIMIENTOS
	var totales = [];
	@php
	$i=0;
	@endphp
	@foreach($entregados as $mes)
		@php
		$j=0;
		@endphp
		totales[{{$i}}] = [];
		@foreach($mes as $semana)
			totales[{{$i}}][{{$j}}] = '{{$semana}}';
			@php $j++;
			@endphp
		@endforeach
		@php $i++;
			@endphp
	@endforeach

	var calendar = $('#calendar').fullCalendar({
	    locale: 'es',
	    height: 720,
	    theme: false,
	    timezone: 'local',
	    eventLimit: true,
	    //showNonCurrentDates : false,
	    displayEventTime:false,
	    header: {
		    left:   'title',
		    center:   '',
		    right:  'today prev,next'
		},
	    views: {
		    agenda: {
			    slotLabelFormat: 'ha',
				timeFormat: 'h(:mm)a'
		    },
	        agendaDay: {
	            type: 'agendaDay',
	            buttonText: 'Día',
	        },
	    },
	    events: [
		    @foreach( $pedidos_entregados as $pedido )
	        {
	            description: 'Pedido #{{ $pedido->id }}',
	            title: '{{ $pedido->establecimiento->sucursal }}',
                start  : '{{ $pedido->entregado_at }}',
                end    : '{{ $pedido->entregado_at }}',
                backgroundColor : '{{ $pedido->calendar_color() }}',
                borderColor : '#ddd',
                textColor : '#333',
                url    : '{{ url("pedidos/" . $pedido->id ) }}',
	        },
		    @endforeach
	    ],

	    eventRender: function(event, element) {
	        element.qtip({
	            content: event.description
	        });
	    },

	    viewRender: function (view, element) {
	    	setMonthTotal();
        }
    });

	function setMonthTotal(){
		setTimeout(function(){
			var moment = $("#calendar").fullCalendar('getDate');
			var view = $("#calendar").fullCalendar('getView').intervalStart;

			var date = new Date(view);

			//FIX TO MOMENT DATE IN fullCalendar getDate method 
			var somedate = date.setDate(date.getDate() + 1);
			somedate = new Date(somedate);

			var month_int = somedate.getMonth();

			$('#total_semana1').html('$'+numeral(parseFloat(totales[month_int][0]).toFixed(2).toString().replace(',','.')).format('0,0.00'));
        	$('#total_semana2').html('$'+numeral(parseFloat(totales[month_int][1]).toFixed(2).toString().replace(',','.')).format('0,0.00'));
        	$('#total_semana3').html('$'+numeral(parseFloat(totales[month_int][2]).toFixed(2).toString().replace(',','.')).format('0,0.00'));
        	$('#total_semana4').html('$'+numeral(parseFloat(totales[month_int][3]).toFixed(2).toString().replace(',','.')).format('0,0.00'));

	    	if (typeof totales[month_int][4] !== 'undefined') {
        		$('#total_semana5').html('$'+numeral(parseFloat(totales[month_int][4]).toFixed(2).toString().replace(',','.')).format('0,0.00'));
	    	}
	    	
    	}, 1000);
	}

});
</script>
@endsection