<div class="ibox">
	<div class="ibox-content">
	
		<div class="row">
			<div id="grafico_consumo"></div>
			<div class="btn-group" style="position: absolute;top: 5px; right: 25px;">
			    <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" aria-expanded="false"><i class="fa fa-navicon"></i></button>
			    <ul class="dropdown-menu" style="right: 0;left: auto;">
			        <li><a id="btnPrint">Imprimir</a></li>
			        <li><a id="btnJpeg">JPEG</a></li>
			        <li><a id="btnPng">PNG</a></li>
			        <li><a id="btnPdf">PDF</a></li>
			    </ul>
			</div>
		</div>
		
	</div>
</div>

@section('assets_bottom')
	@parent

<script type="text/javascript">
$(document).ready(function() {

	var chart1 = Highcharts.chart({
	        chart: {
	            type: 'line',
	            renderTo: 'grafico_consumo'
	        },
	        title: {
	            text: 'Consumo Mensual de Hielo'
	        },
	        subtitle: {
	            text: '{{ $establecimiento->sucursal }}'
	        },
	        xAxis: {
	            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	        },
	        yAxis: {
	            title: {
	                text: 'Cantidad en Bolsas'
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {
	                    enabled: true
	                },
	                enableMouseTracking: false
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [
	            @foreach( $pedidos_entregados_mes as $key => $value )
	            {
	                name: '{{ $key }}',
	                data  : [{{implode(',',$value)}} ],  

	            },
	            @endforeach
	        ]
	    });

	    $("#btnPrint").on('click', function (event) {
	        chart1.print();
	    });

	    $("#btnJpeg").on('click', function (event) {
	        chart1.exportChart({
	            type: "image/jpeg",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });

	    $("#btnPng").on('click', function (event) {
	        chart1.exportChart({
	            type: "image/png",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });
	    $("#btnPdf").on('click', function (event) {
	        chart1.exportChart({
	            type: "application/pdf",
	            filename: 'Consumo_Mensual_Hielo_{{Carbon::now()}}'
	        });
	    });

});
</script>
@endsection