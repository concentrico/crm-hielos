<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				
				<h2 class="text-info"> </h2>

				<h4 class="text-success">Información del Cliente</h4>
				<div class="row">
					<div class="col-sm-6">
						<span style="font-weight: bold;">Nombre del Cliente:</span><br />
						<a href="{{url('clientes/'.$establecimiento->cliente_id)}}">{{ $establecimiento->cliente->nombre_corto }} </a>
					</div>
					<div class="col-sm-6">
						<span style="font-weight: bold;">Contacto Comercial</span><br />

						@if(count($establecimiento->cliente->contactos) > 0 )
							@foreach($establecimiento->cliente->contactos as $contacto)
								@if (in_array('1', $contacto->funciones()->pluck('id')->toArray() )) 
									<span style=""> {{ $contacto->nombre_completo() }} </span>
								@endif
							@endforeach	
						@endif
						
					</div>
				</div>

			</div>
		</div>
    </div>
</div>
