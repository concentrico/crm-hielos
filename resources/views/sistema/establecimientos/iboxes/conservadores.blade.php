<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Conservadores</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalConservadorAsignar">
			<i class="fa fa-plus"></i> Asignar Conservador
		</a>
    </div>

    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
			    @if(count($establecimiento->conservadores) == 0)
					<div class="alert alert-warning">
						El establecimiento no tiene conservadores registrados!
					</div>
				@else
					<table class="table table-res" data-cascade="true">
						<thead>
							<tr>
								<th data-type="html">ID Sistema</th>
								<th data-type="html" data-breakpoints="sm">Modelo</th>
								<th data-type="html" data-breakpoints="sm">Número de Serie</th>
								<th data-type="html" data-breakpoints="sm">Status</th>
								<th data-type="html" data-breakpoints="sm">Frecuencia Mantenimiento</th>
								<th data-type="html" data-breakpoints="sm">Último Mantenimiento</th>
								<th data-type="html" data-breakpoints="xlg">Producto</th>
								<th data-type="html" data-breakpoints="xlg">Capacidad</th>
							</tr>
						</thead>
						<tbody>
		                    @foreach ($establecimiento->conservadores as $conservador)
			                    <tr class="@if($conservador->deleted_at != NULL) warning @endif">

			                        <td><a href="{{ url('conservadores/'.$conservador->id) }}">{{ $conservador->id_codigo }}</a></td>
			                        <td>{{ $conservador->modelo->modelo }}</td>
			                        <td class="text-center">{{ $conservador->numero_serie }}</td>
			                        <td class="text-center">
				                    	@php
				                    		switch ($conservador->status) {
				                    			case 'stand_by':
				                    				echo '<span class="label label-success" style="margin-top: 5px; display:inline-block;">Stand By</span>';
				                    				break;
				                    			case 'en_consignacion':
				                    				echo '<span class="label label-primary" style="margin-top: 5px; display:inline-block;">En Consignación</span>';
				                    				break;
				                    			case 'en_reparacion':
				                    				echo '<span class="label label-danger" style="margin-top: 5px; display:inline-block;">Reparación</span>';
				                    				break;
				                    			case 'en_mantenimiento':
				                    				echo '<span class="label label-warning" style="margin-top: 5px; display:inline-block;">Mantenimiento</span>';
				                    				break;
				                    			case 'fuera_servicio':
				                    				echo '<span class="label label-default" style="margin-top: 5px; display:inline-block;">Fuera de Servicio</span>';
				                    				break;
				                    			default:
				                    				echo '<span class="label label-info" style="margin-top: 5px; display:inline-block;">N.D.</span>';
				                    				break;
				                    		}
				                    	@endphp
				                    </td>

				                    <td class="text-center">{{ $conservador->frecuencia_mantenimiento }} días</td>

			                        <td class="text-center">

			                        @if($conservador->ultimo_mantenimiento != null)
				                    	@php 
				                    	$ult_mtto = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%Y-%m-%d');
				                    	$now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->formatLocalized('%Y-%m-%d');
				                    	$diff = diferencia_fechas($ult_mtto, $now);

				                    	$formatted = Carbon::createFromFormat('Y-m-d H:i:s', $conservador->ultimo_mantenimiento)->formatLocalized('%A %d %B %Y');
				                    	@endphp

				                    	<span class="badge badge-info" data-toggle="tooltip" data-placement="top" data-original-title="{{$formatted}}">
				                    	
				                    	@if( strpos($diff, '0') !== false  &&  strpos($diff, 'días') !== false )    Hoy
				                    	@else
				                    		Hace {{$diff}}
				                    	@endif

				                    	</span>

				                    @else
				                    @endif
				                    </td>
			                        <td>{{ $conservador->producto->nombre }}</td>
			                        <td>{{ $conservador->modelo->capacidad }}</td>
			                    </tr>
							@endforeach
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
{{--
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Cuarto Frío</h5>
        @if($establecimiento->cuartofrio_id == null)
	       	<a class="text-success pull-right" onclick="agregarCuartoFrio()">
				<i class="fa fa-plus"></i> Agregar Cuarto Frío
			</a>
		@else
			<button class="btn btn-outline btn-danger btn-sm pull-right" onclick="showModalCuartoFrioEliminar({{ $establecimiento->cuartofrio->id }});">
	        	<i class="fa fa-trash"></i> Eliminar Cuarto Frío
	        </button>
		</a>
		@endif
    </div>

    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
			    @if($establecimiento->cuartofrio_id == null )
					<div class="alert alert-warning">
						El establecimiento no tiene un cuarto frío registrado!
					</div>
				@else
					{!! Form::open(['method' => 'PUT',  'url' => url('establecimientos/' . $establecimiento->id . '/cuartofrio/'.$establecimiento->cuartofrio->id )] ) !!}
					<table id="tablaProductosCF" class="table" style="margin-bottom: 0px;">
						<input type="hidden" name="product_agregate" id="product_agregate" value="0">
							<thead>
								<th class="col-xs-2" style="border-top:0px; vertical-align: middle;">ID Producto</th>
								<th style="border-top:0px; vertical-align: middle;" class="center text-center col-xs-5">Producto</th>
								<th style="border-top:0px; vertical-align: middle;" class="center text-center col-xs-3">Capacidad</th>
								<th style="border-top:0px; vertical-align: middle;" class="center text-center col-xs-2">
									<a class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalAgregarProductoCF"><i class="fa fa-plus"></i></a>
								</th>
							</thead>
						<tbody>
						@php $cap_total=0; @endphp
						@if(count($establecimiento->cuartofrio->productos) > 0)

		                    @foreach ($establecimiento->cuartofrio->productos as $producto)
			                    <tr>
			                        <td class="text-center">
			                        	<div class="input-group" style="width: 100%;">
											<span class="input-group-addon"><i class="fa fa-link"></i></span>
							            {!! Form::text('id_productos_cf[]', $producto->producto->id_codigo, ['class' => 'form-control positive-integer','readonly' => 'readonly', 'placeholder' => 'ID Sistema']) !!}
							            <input type="hidden" name="cfproducto_edit_id[]" value="{{$producto->producto_id}}">
							            </div>
							            <span class="help-block">{{ $errors->first('id_productos_cf')}}</span>
			                        </td>
	                                <td class="text-center">
	                                <div class="input-group" style="width: 100%;">
	                                	<span class="input-group-addon"><i class="fa fa-cubes"></i></span>
 								   		{!! Form::text('nombre_productos_cf[]', $producto->producto->nombre, ['class' => 'form-control','readonly' => 'readonly']) !!}
 								   	</div>
 								   	<span class="help-block">{{ $errors->first('nombre_productos_cf')}}</span>
	                                </td>
	                                <td class="text-center">
	                                	<div class="input-group" style="width: 100%;">
											<span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span>
	 								   		{!! Form::text('capacidad_productos_cf[]', $producto->capacidad, ['class' => 'form-control positive-integer capbolsas', 'onblur' => 'calcularCapacidad(this.value)']) !!}
	 								   	</div>
	 								   	<span class="help-block">{{ $errors->first('capacidad_productos_cf')}}</span>
	                                </td>
				               		<td class="center text-center">
				               			<button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button>
									</a>
				               		</td>
				                </tr>
				            @php $cap_total+=$producto->capacidad; @endphp
			                @endforeach

			            
                        @else
                        	<tr><td colspan="4" class="text-center"><h4>No tiene productos asignados</h4></td></tr>
                        @endif
						</tbody>
						<tfoot>
							<tr>
								<th style="text-align: right;" colspan="2"><h3>Capacidad Total (bolsas)</h3></th>
								<th class="text-center"><h3 id="total_bolsas" class="text-info">{{$cap_total}}</h3></th>
								<th>
									<button type="submit" id="btnSaveCF" class="btn btn-outline btn-primary btn-sm pull-right">
							        	<i class="fa fa-save"></i> Guardar
							        </button>
								</th>
							</tr>
						</tfoot>
					</table>
					@if ($errors->first('updateFailed') )
					    <div class="alert alert-danger text-center">
				           	{{$errors->first('updateFailed')}}
				        </div>
					@endif
					{!! Form::close() !!}
				@endif

			</div>
		</div>
	</div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Gestión Inventario</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
		    	{!! Form::open(['method' => 'PUT',  'url' => url('establecimientos/' . $establecimiento->id . '/inventario')] ) !!}

		    	<div class="form-group col-sm-3">
                   {!! Form::label('capacidad_actual', 'Inventario Actual', ['class' => 'control-label']) !!}
                   <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-battery-half"></i></span>
                   {!! Form::text('capacidad_actual', $establecimiento->capacidad_actual, ['class' => 'form-control positive-integer', 'readonly' => 'readonly']) !!}
                   </div>
                </div>

                @php 
                	if ($establecimiento->capacidad_maxima != null) {
                		$max_capacidad = $establecimiento->capacidad_maxima;
                	} else {
                		$max_capacidad = $establecimiento->getCapacidadRecomendada('maxima');
                	}
                @endphp

                <div class="form-group col-sm-3">
                   {!! Form::label('capacidad_minima', 'Capacidad Mínima', ['class' => 'control-label']) !!}
                   <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-battery-empty"></i></span>
                   {!! Form::number('capacidad_minima', $establecimiento->capacidad_minima, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('minima'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
                   </div>
                   <span class="help-block">{{ $errors->first('capacidad_minima')}}</span>
                </div>

		    	<div class="form-group col-sm-3">
                    {!! Form::label('punto_reorden', 'Punto de Reorden', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></span>
                        {!! Form::number('punto_reorden', $establecimiento->punto_reorden, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('punto_reorden'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
                    </div>
                    <span class="help-block">{{ $errors->first('punto_reorden')}}</span>
                </div>

                <div class="form-group col-sm-3">
                   {!! Form::label('capacidad_maxima', 'Capacidad Máxima', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-battery-full"></i></span>
                   {!! Form::number('capacidad_maxima', $establecimiento->capacidad_maxima, ['class' => 'form-control positive-integer', 'placeholder' => $establecimiento->getCapacidadRecomendada('maxima'), 'min' =>'0', 'max' => $max_capacidad,'onblur' => 'checkCapacidadMaxima(this)']) !!}
                   </div>
                   <span class="help-block">{{ $errors->first('capacidad_maxima')}}</span>
                </div>

                <div class="form-group col-sm-12">
                	<button type="button" id="btnCorte" class="btn btn-outline btn-danger btn-sm pull-left" onclick="showModalRealizarCorte()"><i class="fa fa-clipboard"></i> Realizar Corte</button>
                	<button type="submit" id="btnSaveInventario" class="btn btn-outline btn-primary btn-sm pull-right"><i class="fa fa-save"></i> Guardar</button>
                </div>

                {!! Form::close() !!}
		    </div>
    	</div>
    	<div class="row">
    		<div class="col-sm-12">
			    <div class="alert alert-warning">
					Si el establecimiento tiene asignado más de un producto diferente, las capacidades serán en base al <b>total</b> de las capacidades máximas de cada producto.
				</div>
			</div>
    	</div>
    </div>
</div>

<script type="text/javascript">

	function checkCapacidadMaxima(obj){
		@if ($establecimiento->capacidad_maxima != null)
    		var max_capacidad = {{$establecimiento->capacidad_maxima}};
    	@else
    		var max_capacidad = {{$establecimiento->getCapacidadRecomendada('maxima')}};
    	@endif

    	if (obj.value > max_capacidad) {
    		obj.value=max_capacidad;
    	}
	}  

	function agregarCuartoFrio() {
		swal({
		  title: "¿Desea agregar un Cuarto Frío?",
		  text: "Deberá especificar capacidad y precio de venta por producto.",
		  type: "info",
		  showCancelButton: true,
  		  showLoaderOnConfirm: true,
		  confirmButtonColor: "#1c84c6",
		  confirmButtonText: "Sí",
          cancelButtonText: "Cancelar",
		  closeOnConfirm: false
		},
		function(){

			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/establecimientos')}}',
	            data:{ 'type': '3', 'establecimiento_id': '{{$establecimiento->id}}', '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	swal("Correcto!", "Se ha agregado un cuarto frío al establecimiento.", "success");

	            	setTimeout(function(){
					    location.reload();
					}, 2000);
	            }
	        });
		  
		});
	}

	function addProductoCuartoFrio() {

		var id_producto_agregar = $('#producto_agregar').val();

		if (id_producto_agregar && id_producto_agregar != '0' ) {
			$.ajax({
	            type: 'POST',
	            url: '{{url('ajax/productos')}}',
	            data:{ 'type': '1', 'producto_id': id_producto_agregar, '_token': '{{ csrf_token()}}' },
	            success: function(data) {

	            	var append = '<tr><td class="text-center"><div class="input-group" style="width: 100%;"><span class="input-group-addon"><i class="fa fa-link"></i></span><input type="text" name="id_productos_cf[]" value="'+data['id_codigo']+'" class="form-control positive-integer" readonly="" placeholder="ID Sistema"/><input type="hidden" name="cfproducto_edit_id[]" value="'+data['id']+'"></div><span class="help-block"></span></td>';
					append+='<td class="text-center"><div class="input-group" style="width: 100%;"><span class="input-group-addon"><i class="fa fa-cubes"></i></span><input type="text" name="nombre_productos_cf[]" value="'+data['nombre']+'" class="form-control" readonly=""/></div><span class="help-block"></span></td>';
				    append+='<td class="text-center"><div class="input-group" style="width: 100%;"><span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span><input type="text" name="capacidad_productos_cf[]" class="form-control positive-integer capbolsas" onblur="calcularCapacidad(this.value)"/></div><span class="help-block"></span></td>';
					append+='<td class="center text-center"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></a></td>';
					
					append+='</tr>';

					var cont_prod_init = 0;
					@if($establecimiento->cuartofrio_id != null )
						@if(count($establecimiento->cuartofrio->productos) > 0)
							cont_prod_init = {{count($establecimiento->cuartofrio->productos)}};
						@endif
					@endif
					var cont_prod_act = $('#product_agregate').val();

					if( cont_prod_init == 0 && cont_prod_act == '0') {
						$('#tablaProductosCF tbody tr:last').remove();
						$('#product_agregate').val('1');
					}

					$('#tablaProductosCF').append(append);
					$("#producto_agregar option[value='"+id_producto_agregar+"']").remove();
					$('#modalAgregarProductoCF').modal('hide');
	            }
	        });

		} else {
			return false;
		}

	}

	function calcularCapacidad(cap) {
		var sum = 0;
		$('.capbolsas').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#total_bolsas').html(sum);
	}

	function calcularCapacidadCorte(cap) {
		var sum = 0;
		$('.capproducto').each(function(){
			if ($(this).val() != '') {
		    	sum += parseInt($(this).val());
			} else {
				sum +=0;
			}
		});
		$('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
	}

</script>
--}}