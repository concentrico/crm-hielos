<div class="modal inmodal" id="modalEstablecimientoContactoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-user-circle-o modal-icon" style="font-size: 2em;"></i> <BR/>Editar contacto</h4>
				<h5 class="modal-title text-success">{{ $establecimiento->sucursal }}</h5>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('establecimientos/' . $establecimiento->id . '/contactos/' )] ) !!}
				<div class="modal-body">

		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_nombre_edit') !== "") has-error @endif">
				            {!! Form::label('contacto_nombre_edit', 'Nombre(s)', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_nombre_edit', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            <span class="help-block">{{ $errors->first('contacto_nombre_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_apellido_paterno_edit') !== "") has-error @endif">
				            {!! Form::label('contacto_apellido_paterno_edit', 'Apellido Paterno', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_apellido_paterno_edit', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno']) !!}
				            <span class="help-block">{{ $errors->first('contacto_apellido_paterno_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_apellido_materno_edit') !== "") has-error @endif">
				            {!! Form::label('contacto_apellido_materno_edit', 'Apellido Materno', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_apellido_materno_edit', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno']) !!}
				            <span class="help-block">{{ $errors->first('contacto_apellido_materno_edit')}}</span>
				        </div>
		            </div>

		            <div class="row">
			            <div class="col-sm-12 col-md-6" id="lista_emails_edit">
				            <button type="button" class="btn btn-info btn-sm pull-right" onclick="addEmailContactoEdit()">
				            	<i class="fa fa-plus"></i>
				            </button>
				            <h4><i class="fa fa-envelope-o"></i> Email(s)</h4><br />
				            
				            @if( Input::old('tipo_email_edit')  != null )
				            				            	
					            @foreach(Input::old('tipo_email_edit') as $key => $value)

									<div class="row">
										<div class="form-group col-sm-4 @if ($errors->first('tipo_email_edit.' . $key) !== "") has-error @endif">
											<div class="input-group">
												<span class="input-group-btn">
													<button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()">
														<i class="fa fa-times"></i>
													</button>
												</span>
												<select class="form-control selectdp" name="tipo_email_edit[]" data-placeholder="Tipo">
													<option value="" @if($value == "") selected="selected" @endif></option>
													<option value="personal" @if($value == "personal") selected="selected" @endif >Personal</option>
													<option value="trabajo" @if($value == "trabajo") selected="selected" @endif >Trabajo</option>
													<option value="otro" @if($value == "otro") selected="selected" @endif >Otro</option>
												</select>
											</div>
											<span class="help-block">{{ $errors->first('tipo_email_edit.' . $key) }}</span>
										</div>
										<div class="form-group col-sm-8 @if ($errors->first('email_edit.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="email_edit[]" value="{{ Input::old('email_edit')[$key] }}" placeholder="Email">
											<span class="help-block">{{ $errors->first('email_edit.' . $key) }}</span>
										</div>
										@if( isset(Input::old('email_edit_id')[$key]) )
										<input type="hidden" name="email_edit_id[]" value="{{ Input::old('email_edit_id')[$key] }}" />
										@endif
									</div>									
									
					            @endforeach
					            
					        @endif
			            </div>
			            <div class="col-sm-12 col-md-6" id="lista_telefonos_edit">
				            <button type="button" class="btn btn-info btn-sm pull-right" onclick="addTelefonoContactoEdit()"><i class="fa fa-plus"></i></button>
				            <h4><i class="fa fa-phone"></i> Teléfono(s)</h4><br />

				            @if( Input::old('tipo_telefono_edit')  != null )
				            				            	
					            @foreach(Input::old('tipo_telefono_edit') as $key => $value)

									<div class="row">
										<div class="form-group col-sm-4 @if ($errors->first('tipo_telefono_edit.' . $key) !== "") has-error @endif">
											<div class="input-group">
												<span class="input-group-btn">
													<button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()">
														<i class="fa fa-times"></i>
													</button>
												</span>
												<select class="form-control selectdp" name="tipo_telefono_edit[]" data-placeholder="Tipo">
													<option value="" @if($value == "") selected="selected" @endif></option>
													<option value="casa" @if($value == "casa") selected="selected" @endif >Casa</option>
													<option value="oficina" @if($value == "oficina") selected="selected" @endif >Oficina</option>
													<option value="celular" @if($value == "celular") selected="selected" @endif >Celular</option>
													<option value="otro" @if($value == "otro") selected="selected" @endif >Otro</option>
												</select>
											</div>
											<span class="help-block">{{ $errors->first('tipo_telefono_edit.' . $key) }}</span>
										</div>
										<div class="form-group col-xs-8 col-sm-5 @if ($errors->first('telefono_edit.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="telefono_edit[]" value="{{ Input::old('telefono_edit')[$key] }}" placeholder="Teléfono">
											<span class="help-block">{{ $errors->first('telefono_edit.' . $key) }}</span>
										</div>
										<div class="form-group col-xs-4 col-sm-3 @if ($errors->first('telefono_edit_ext.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="telefono_edit_ext[]" value="{{ Input::old('telefono_edit_ext')[$key] }}" placeholder="Ext.">
											<span class="help-block">{{ $errors->first('telefono_edit_ext.' . $key) }}</span>
										</div>
										@if( isset(Input::old('telefono_edit_id')[$key]) )
											<input type="hidden" name="telefono_edit_id[]" value="{{ Input::old('telefono_edit_id')[$key] }}" />
										@endif
									</div>									
									
					            @endforeach
					            
					        @endif

			            </div>
		            </div>
		            
		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('contacto_comentarios_edit') !== "") has-error @endif">
				            {!! Form::label('contacto_comentarios_edit', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('contacto_comentarios_edit', null, ['class' => 'form-control', 'placeholder' => 'Comentarios (Opcional)', 'style' => 'max-width:100%;', 'rows' => '3' ]) !!}
				            <span class="help-block">{{ $errors->first('contacto_comentarios_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar cambios</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_establecimiento_contacto_edit'))
		$('#modalEstablecimientoContactoEdit').find('form').attr('action', "{{ url('establecimientos/' . $establecimiento->id . '/contactos/' . session('contacto_id') ) }}")
		$('.nav-tabs a[href="#tab-2"]').tab('show');
		$('#modalEstablecimientoContactoEdit').modal('show');
	@endif

	function showModalEstablecimientoContactoEdit(id){
		
		if ( $('#modalEstablecimientoContactoEdit').find('form').attr('action') == "{{ url('establecimientos/' . $establecimiento->id . '/contactos' ) }}/" + id){
			
		}else{
			$('#modalEstablecimientoContactoEdit').find('form').attr('action', "{{ url('establecimientos/' . $establecimiento->id . '/contactos' ) }}/" + id)
			
			$('#modalEstablecimientoContactoEdit').find('input[name=contacto_nombre_edit]').val(contactos[id]['nombre']);
			$('#modalEstablecimientoContactoEdit').find('input[name=contacto_apellido_paterno_edit]').val(contactos[id]['apellido_paterno']);
			$('#modalEstablecimientoContactoEdit').find('input[name=contacto_apellido_materno_edit]').val(contactos[id]['apellido_materno']);

			$('#lista_emails_edit').html('<button type="button" class="btn btn-info btn-sm pull-right" onclick="addEmailContactoEdit()"><i class="fa fa-plus"></i></button><h4><i class="fa fa-envelope-o"></i> Email(s)</h4><br />');
						
			$.each( contactos[id]['emails'], function( key, value ) {
				
				var none_selected = "";
				var personal_selected = "";
				var trabajo_selected = "";
				var otro_selected = "";

				if(value.tipo == ""){
					none_selected = 'selected="selected"'
				}else if(value.tipo == "personal"){
					personal_selected = 'selected="selected"'
				}else if(value.tipo == "trabajo"){
					trabajo_selected = 'selected="selected"'
				}else if(value.tipo == "otro"){
					otro_selected = 'selected="selected"'
				}
				
				$('#lista_emails_edit').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control selectdp" name="tipo_email_edit[]" data-placeholder="Tipo"><option value="" ' + none_selected + '></option><option value="personal" ' + personal_selected + '>Personal</option><option value="trabajo" ' + trabajo_selected + '>Trabajo</option><option value="otro" ' + otro_selected + '>Otro</option></select></div></div><div class="form-group col-sm-8"><input class="form-control" type="text" name="email_edit[]" value="' + value.correo + '" placeholder="Email"><input type="hidden" name="email_edit_id[]" value="' + value.id + '" /></div></div>');
			});

			$('#lista_telefonos_edit').html('<button type="button" class="btn btn-info btn-sm pull-right" onclick="addTelefonoContactoEdit()"><i class="fa fa-plus"></i></button><h4><i class="fa fa-phone"></i> Teléfono(s)</h4><br />');
						
			$.each( contactos[id]['telefonos'], function( key, value ) {
				
				var none_selected = "";
				var casa_selected = "";
				var oficina_selected = "";
				var celular_selected = "";
				var otro_selected = "";
				
				if(value.tipo == ""){
					none_selected = 'selected="selected"'
				}else if(value.tipo == "casa"){
					casa_selected = 'selected="selected"'
				}else if(value.tipo == "oficina"){
					oficina_selected = 'selected="selected"'
				}else if(value.tipo == "celular"){
					celular_selected = 'selected="selected"'
				}else if(value.tipo == "otro"){
					otro_selected = 'selected="selected"'
				}
				
				$('#lista_telefonos_edit').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control selectdp" name="tipo_telefono_edit[]" data-placeholder="Tipo"><option value="" ' + none_selected + '></option><option value="casa" ' + casa_selected + '>Casa</option><option value="oficina" ' + oficina_selected + '>Oficina</option><option value="celular" ' + celular_selected + '>Celular</option><option value="otro" ' + otro_selected + '>Otro</option></select></div></div><div class="form-group col-xs-8 col-sm-5"><input class="form-control" type="text" name="telefono_edit[]" value="' + value.telefono + '" placeholder="Teléfono"></div><div class="form-group col-xs-4 col-sm-3"><input class="form-control" type="text" name="telefono_ext_edit[]" value="' + value.telefono_ext + '" placeholder="Ext."><input type="hidden" name="telefono_edit_id[]" value="' + value.id + '" /></div></div>');
			});
		}


		$('.selectdp').dropdown();

		$('#modalEstablecimientoContactoEdit').modal('show');
	}

	function addEmailContactoEdit(){
		
		$('#lista_emails_edit').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control chosen-select" name="tipo_email_edit[]" data-placeholder="Tipo"><option value=""></option><option value="personal">Personal</option><option value="trabajo">Trabajo</option><option value="otro">Otro</option></select></div></div><div class="form-group col-sm-8"><input class="form-control" type="text" name="email_edit[]" value="" placeholder="Email"></div></div>');

		$('.selectdp').dropdown();
	}

	function addTelefonoContactoEdit(){
		$('#lista_telefonos_edit').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control chosen-select" name="tipo_telefono_edit[]" data-placeholder="Tipo"><option value=""></option><option value="casa">Casa</option><option value="oficina">Oficina</option><option value="celular">Celular</option><option value="otro">Otro</option></select></div></div><div class="form-group col-xs-8 col-sm-5"><input class="form-control" type="text" name="telefono_edit[]" value="" placeholder="Teléfono"></div><div class="form-group col-xs-4 col-sm-3"><input class="form-control" type="text" name="telefono_ext_edit[]" value="" placeholder="Ext."></div></div>');
		$('.selectdp').dropdown();
	}
</script>
@endsection