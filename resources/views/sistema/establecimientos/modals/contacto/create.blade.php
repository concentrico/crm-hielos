<div class="modal inmodal" id="modalEstablecimientoContactoCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-user-circle-o modal-icon" style="font-size: 2em;"></i> <BR/>Registrar contacto</h4>
				<h5 class="modal-title text-success">{{ $establecimiento->sucursal }}</h5>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('establecimientos/' . $establecimiento->id . '/contactos' )] ) !!}
				<div class="modal-body">
				
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_nombre') !== "") has-error @endif">
				            {!! Form::label('contacto_nombre', 'Nombre(s)', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            <span class="help-block">{{ $errors->first('contacto_nombre')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_apellido_paterno') !== "") has-error @endif">
				            {!! Form::label('contacto_apellido_paterno', 'Apellido Paterno', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno']) !!}
				            <span class="help-block">{{ $errors->first('contacto_apellido_paterno')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('contacto_apellido_materno') !== "") has-error @endif">
				            {!! Form::label('contacto_apellido_materno', 'Apellido Materno', ['class' => 'control-label']) !!}
				            {!! Form::text('contacto_apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno']) !!}
				            <span class="help-block">{{ $errors->first('contacto_apellido_materno')}}</span>
				        </div>
		            </div>

		            <div class="row">
			            <div class="col-sm-12 col-md-6" id="lista_emails">
				            <button type="button" class="btn btn-info btn-sm pull-right" onclick="addEmailContactoCreate()">
				            	<i class="fa fa-plus"></i>
				            </button>
				            <h4>Email(s)</h4><br />
				            
				            @if( Input::old('tipo_email')  != null )
				            				            	
					            @foreach(Input::old('tipo_email') as $key => $value)

									<div class="row">
										<div class="form-group col-sm-4 @if ($errors->first('tipo_email.' . $key) !== "") has-error @endif">
											<div class="input-group">
												<span class="input-group-btn">
													<button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()">
														<i class="fa fa-times"></i>
													</button>
												</span>
												<select class="form-control selectdp" name="tipo_email[]" data-placeholder="Tipo">
													<option value="" @if($value == "") selected="selected" @endif></option>
													<option value="personal" @if($value == "personal") selected="selected" @endif >Personal</option>
													<option value="trabajo" @if($value == "trabajo") selected="selected" @endif >Trabajo</option>
													<option value="otro" @if($value == "otro") selected="selected" @endif >Otro</option>
												</select>
											</div>
											<span class="help-block">{{ $errors->first('tipo_email.' . $key) }}</span>
										</div>
										<div class="form-group col-sm-8 @if ($errors->first('email.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="email[]" value="{{ Input::old('email')[$key] }}" placeholder="Email">
											<span class="help-block">{{ $errors->first('email.' . $key) }}</span>
										</div>
									</div>									
									
					            @endforeach
					            
					        @endif
			            </div>
			            <div class="col-sm-12 col-md-6" id="lista_telefonos">
				            <button type="button" class="btn btn-info btn-sm pull-right" onclick="addTelefonoContactoCreate()"><i class="fa fa-plus"></i></button>
				            <h4><i class="fa fa-phone"></i> Teléfono(s)</h4><br />

				            @if( Input::old('tipo_telefono')  != null )
				            				            	
					            @foreach(Input::old('tipo_telefono') as $key => $value)

									<div class="row">
										<div class="form-group col-sm-4 @if ($errors->first('tipo_telefono.' . $key) !== "") has-error @endif">
											<div class="input-group">
												<span class="input-group-btn">
													<button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()">
														<i class="fa fa-times"></i>
													</button>
												</span>
												<select class="form-control selectdp" name="tipo_telefono[]" data-placeholder="Tipo">
													<option value="" @if($value == "") selected="selected" @endif></option>
													<option value="casa" @if($value == "casa") selected="selected" @endif >Casa</option>
													<option value="oficina" @if($value == "oficina") selected="selected" @endif >Oficina</option>
													<option value="celular" @if($value == "celular") selected="selected" @endif >Celular</option>
													<option value="otro" @if($value == "otro") selected="selected" @endif >Otro</option>
												</select>
											</div>
											<span class="help-block">{{ $errors->first('tipo_telefono.' . $key) }}</span>
										</div>
										<div class="form-group col-xs-8 col-sm-5 @if ($errors->first('telefono.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="telefono[]" value="{{ Input::old('telefono')[$key] }}" placeholder="Teléfono">
											<span class="help-block">{{ $errors->first('telefono.' . $key) }}</span>
										</div>
										<div class="form-group col-xs-4 col-sm-3 @if ($errors->first('telefono_ext.' . $key) !== "") has-error @endif">
											<input class="form-control" type="text" name="telefono_ext[]" value="{{ Input::old('telefono_ext')[$key] }}" placeholder="Ext.">
											<span class="help-block">{{ $errors->first('telefono_ext.' . $key) }}</span>
										</div>
									</div>									

					            @endforeach
					            
					        @endif

			            </div>
		            </div>
		            
		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('contacto_comentarios') !== "") has-error @endif">
				            {!! Form::label('contacto_comentarios', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('contacto_comentarios', null, ['class' => 'form-control', 'placeholder' => 'Comentarios (Opcional)', 'style' => 'max-width:100%;', 'rows' => '3' ]) !!}
				            <span class="help-block">{{ $errors->first('contacto_comentarios')}}</span>
				        </div>
		            </div>

		            @if ($errors->first('contactoFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('contactoFailed')!!}
				        </div>
					@endif

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_establecimiento_contacto_create'))
		$('#modalEstablecimientoContactoCreate').modal('show');
	@endif
	
	function addEmailContactoCreate(){
		
		$('#lista_emails').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control selectdp" name="tipo_email[]" data-placeholder="Tipo"><option value=""></option><option value="personal">Personal</option><option value="trabajo">Trabajo</option><option value="otro">Otro</option></select></div></div><div class="form-group col-sm-8"><input class="form-control" type="text" name="email[]" value="" placeholder="Email"></div></div>');

		$('.selectdp').dropdown();
	}

	function addTelefonoContactoCreate(){
		$('#lista_telefonos').append('<div class="row"><div class="form-group col-sm-4"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.parentNode.parentNode.remove()"><i class="fa fa-times"></i></button></span><select class="form-control selectdp" name="tipo_telefono[]" data-placeholder="Tipo"><option value=""></option><option value="casa">Casa</option><option value="oficina">Oficina</option><option value="celular">Celular</option><option value="otro">Otro</option></select></div></div><div class="form-group col-xs-8 col-sm-5"><input class="form-control" type="text" name="telefono[]" value="" placeholder="Teléfono"></div><div class="form-group col-xs-4 col-sm-3"><input class="form-control" type="text" name="telefono_ext[]" value="" placeholder="Ext."></div></div>');

		$('.selectdp').dropdown();
	}
</script>
@endsection