<div class="modal inmodal" id="modalClienteEstablecimientoEdit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title "><i class="fa fa-map-pin modal-icon"></i> Editar Establecimiento</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('establecimientos/' . $establecimiento->id)] ) !!}
				<div class="modal-body">

		            <div class="row">
		            	<div class="form-group col-sm-7  @if ($errors->first('cliente_establecimiento_sucursal') !== "") has-error @endif">
				            {!! Form::label('cliente_establecimiento_sucursal', 'Nombre Sucursal', ['class' => 'control-label']) !!}
				            {!! Form::text('cliente_establecimiento_sucursal', $establecimiento->sucursal, ['class' => 'form-control', 'placeholder' => 'Sucursal']) !!}
				            <span class="help-block">{{ $errors->first('cliente_establecimiento_sucursal')}}</span>
				        </div>
			            <div class="form-group col-sm-5  @if ($errors->first('cliente_establecimiento_tipos') !== "") has-error @endif">
				            {!! Form::label('cliente_establecimiento_tipos', 'Tipo(s)', ['class' => 'control-label']) !!}
			            	{!! Form::select('cliente_establecimiento_tipos[]', \App\Models\ClienteTipoEstablecimiento::all()->pluck('nombre', 'id')->toArray(), $establecimiento->tipos->pluck('id', 'nombre')->toArray(), 
				            	['class' => 'form-control selectdp', 'data-placeholder' => 'Tipo(s)', 'multiple' => 'multiple'] ) !!}
				            <span class="help-block">{{ $errors->first('cliente_establecimiento_tipos')}}</span>
				        </div>
		            </div>

		            <div class="row">
		            	<div class="form-group col-sm-7  @if ($errors->first('cliente_establecimiento_razon_social') !== "") has-error @endif">
				            {!! Form::label('cliente_establecimiento_razon_social', 'Razón Social', ['class' => 'control-label']) !!}
				            {!! Form::select('cliente_establecimiento_razon_social', \App\Models\ClienteDatosFiscales::where('cliente_id', '=', $establecimiento->cliente->id)->pluck('razon_social', 'id')->prepend('','')->toArray(), $establecimiento->cliente_datos_fiscales_id, 
				           	['class' => 'form-control selectdp', 'data-placeholder' => 'Razón Social Establecimiento'] ) !!}
				            <span class="help-block">{{ $errors->first('cliente_establecimiento_razon_social')}}</span>
				        </div>

			            <div class="form-group col-sm-5  @if ($errors->first('cliente_establecimiento_zona') !== "") has-error @endif">
				            {!! Form::label('cliente_establecimiento_zona', 'Zona', ['class' => 'control-label']) !!}
				            {!! Form::select('cliente_establecimiento_zona', \App\Models\ClienteZona::all()->pluck('nombre', 'id')->prepend('','')->toArray(), $establecimiento->cliente_zona_id, 
				           	['class' => 'form-control selectdp', 'data-placeholder' => 'Zona'] ) !!}
				            <span class="help-block">{{ $errors->first('cliente_establecimiento_zona')}}</span>
				        </div>
				    </div>

		            <h3>Dirección de Establecimiento</h3>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('est_calle') !== "") has-error @endif">
				            {!! Form::label('est_calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('est_calle', $establecimiento->calle, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('est_calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('est_no_exterior') !== "") has-error @endif">
				            {!! Form::label('est_no_exterior', 'No. Exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('est_no_exterior', $establecimiento->numero_exterior, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('est_no_exterior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('est_no_interior') !== "") has-error @endif">
				            {!! Form::label('est_no_interior', 'No. Interior', ['class' => 'control-label']) !!}
				            {!! Form::text('est_no_interior', $establecimiento->numero_interior, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('est_no_interior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('est_colonia') !== "") has-error @endif">
				            {!! Form::label('est_colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('est_colonia', $establecimiento->colonia, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('est_colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('est_codigo_postal') !== "") has-error @endif">
				            {!! Form::label('est_codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('est_codigo_postal', $establecimiento->codigo_postal, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('est_codigo_postal')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('est_municipio') !== "") has-error @endif">
				            {!! Form::label('est_municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('est_municipio', $establecimiento->municipio, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('est_municipio')}}</span>
				        </div>
		            </div>

		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('establecimiento_comentarios') !== "") has-error @endif">
				            {!! Form::label('establecimiento_comentarios', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('establecimiento_comentarios', $establecimiento->comentarios, ['id' => 'establecimiento_comentarios', 
				            'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            <span class="help-block">{{ $errors->first('establecimiento_comentarios')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">
	
	@if(session('show_modal_cliente_establecimiento_edit'))
		$('#modalClienteEstablecimientoEdit').modal('show');
	@endif
	
</script>	
@endsection