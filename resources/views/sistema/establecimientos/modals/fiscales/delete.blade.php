<div class="modal inmodal" id="modalEstablecimientoDatosFiscalesDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					<h4 class="text-danger modal-title" style="color:#ed5565;"><i class="fa fa-legal modal-icon"></i> <BR/> Desasociar Datos Fiscales</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-danger">¿Estas seguro de que deseas desasociar los datos fiscales?</h2>
				            <h3 style="font-weight: 300;"></h3>
				            <h4 style="font-weight: 300;">No se eliminarán de los datos fiscales del cliente.</h4>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('establecimientos/' . $establecimiento->id . '/datosf/' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-arrow-down"></i> Desasociar</button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalEstablecimientoDatosFiscalesDelete(id){
		
		if ( $('#modalEstablecimientoDatosFiscalesDelete').find('form').attr('action') == "{{ url('establecimientos/' . $establecimiento->id . '/datosf' ) }}/" + id){
			
		}else{
			$('#modalEstablecimientoDatosFiscalesDelete').find('form').attr('action', "{{ url('establecimientos/' . $establecimiento->id . '/datosf' ) }}/" + id)
			
			$('#modalEstablecimientoDatosFiscalesDelete').find('h3').html("<b>" + datosf[id]['rfc'] + "</b>");
						
		}

		$('#modalEstablecimientoDatosFiscalesDelete').modal('show');
	}

</script>
@endsection