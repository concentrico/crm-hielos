<div class="modal inmodal" id="modalEstablecimientoDatosFiscalesCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title">Registrar Datos Fiscales</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('establecimientos/' . $establecimiento->id . '/datosf' )] ) !!}
				<div class="modal-body">
					@if($establecimiento->cliente->datos_fiscales != null)
		                <div class="form-group form-md-line-input @if ($errors->first('datos_fiscales_id') !== "") has-error @endif">
		                  <div class="col-md-6" style="padding: 0px;">
		                    {!! Form::select('datos_fiscales_id', $establecimiento->cliente->datos_fiscales->pluck('razon_social','id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'title' => 'Seleccione' , 'data-none-selected-text' => 'Seleccione', 'onchange' => 'loadBillingInfo(this.value);'] ) !!}
		                    <span class="help-block">{{ $errors->first('datos_fiscales_id')}}</span>
		                  </div>
		                </div>
		                <BR/>
		            @endif
		            <br/>
					<h3>Datos Principales</h3>
		            <div class="row">
			            <div class="form-group col-sm-5 col-md-4 @if ($errors->first('tipo_persona') !== "") has-error @endif">
				            {!! Form::label('tipo_persona', 'Tipo de Persona', ['class' => 'control-label']) !!}
			            	{!! Form::select('tipo_persona', ['fisica' => 'Física', 'moral' => 'Moral', '' => ''], null, 
			            	['class' => 'form-control selectdp tipo_persona', 'data-placeholder' => 'Tipo de Persona'] ) !!}
				            <span class="help-block">{{ $errors->first('tipo_persona')}}</span>
				        </div>
			            <div class="form-group col-sm-7 col-md-3 @if ($errors->first('rfc') !== "") has-error @endif">
				            {!! Form::label('rfc', 'RFC', ['class' => 'control-label']) !!}
				            {!! Form::text('rfc', null, ['class' => 'form-control', 'placeholder' => 'RFC']) !!}
				            <span class="help-block">{{ $errors->first('rfc')}}</span>
				        </div>
			            <div class="form-group col-sm-12 col-md-5 @if ($errors->first('razon_social') !== "") has-error @endif">
				            {!! Form::label('razon_social', 'Denominación o Razón Social', ['class' => 'control-label']) !!}
				            {!! Form::text('razon_social', null, ['class' => 'form-control', 'placeholder' => 'Denominación o Razón Social']) !!}
				            <span class="help-block">{{ $errors->first('razon_social')}}</span>
				        </div>
				        <div class="form-group col-sm-12 col-md-12 @if ($errors->first('representante_legal') !== "") has-error @endif">
				            {!! Form::label('representante_legal', 'Representante Legal', ['class' => 'control-label']) !!}
				            {!! Form::text('representante_legal', null, ['class' => 'form-control', 'placeholder' => 'Representante Legal']) !!}
				            <span class="help-block">{{ $errors->first('representante_legal')}}</span>
				        </div>
		            </div>
					<h3>Dirección de Facturación</h3>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_calle') !== "") has-error @endif">
				            {!! Form::label('df_calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('df_calle', null, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('df_calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_exterior') !== "") has-error @endif">
				            {!! Form::label('df_no_exterior', 'No. Exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_exterior', null, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_exterior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_interior') !== "") has-error @endif">
				            {!! Form::label('df_no_interior', 'No. Interior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_interior', null, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_interior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-9 @if ($errors->first('df_colonia') !== "") has-error @endif">
				            {!! Form::label('df_colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('df_colonia', null, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('df_colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_codigo_postal') !== "") has-error @endif">
				            {!! Form::label('df_codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('df_codigo_postal', null, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('df_codigo_postal')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_municipio') !== "") has-error @endif">
				            {!! Form::label('df_municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('df_municipio', null, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('df_municipio')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('df_estado') !== "") has-error @endif">
				            {!! Form::label('df_estado', 'Estado', ['class' => 'control-label']) !!}
				            {!! Form::text('df_estado', 'Nuevo León', ['class' => 'form-control', 'placeholder' => 'Estado']) !!}
				            <span class="help-block">{{ $errors->first('df_estado')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
	<script type="text/javascript">
		@if(session('show_modal_establecimiento_datos_fiscales_create'))
			$('#modalEstablecimientoDatosFiscalesCreate').modal('show');
		@endif

		function loadBillingInfo(billing_info_id){
			if(billing_info_id != '' ) {
		      $.ajax({
		          type: 'POST',
	            	url: '{{url('ajax/establecimientos')}}',
	            	data:{ 'type': '7', 'billing_info_id': billing_info_id, '_token': '{{ csrf_token()}}' },
		          beforeSend: function() {
					$('.tipo_persona').addClass('disabled');
		            $('#modalEstablecimientoDatosFiscalesCreate select[name="tipo_persona"]').dropdown('refresh');
		          },
		          success: function(data) {
		          	$('#modalEstablecimientoDatosFiscalesCreate').find('select[name=tipo_persona]').dropdown('set selected', data['persona']);
					$('.tipo_persona').addClass('disabled');
		            $('#modalEstablecimientoDatosFiscalesCreate select[name="tipo_persona"]').dropdown('refresh');
		          	$('#modalEstablecimientoDatosFiscalesCreate').find('input[name=rfc]').val(data['rfc']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=razon_social]').val(data['razon_social']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=representante_legal]').val(data['representante_legal']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_calle]').val(data['calle']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_no_exterior]').val(data['numero_exterior']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_no_interior]').val(data['numero_interior']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_colonia]').val(data['colonia']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_codigo_postal]').val(data['codigo_postal']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_municipio]').val(data['municipio']).attr('readonly','readonly');
		            $('#modalEstablecimientoDatosFiscalesCreate').find('input[name=df_estado]').val(data['estado']).attr('readonly','readonly');
		          }
		      });
		    } else {
		      $('#formEditShipping')[0].reset();
    		}
		}

	</script>
@endsection