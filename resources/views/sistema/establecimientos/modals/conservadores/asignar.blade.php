<div class="modal inmodal" id="modalConservadorAsignar" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"> Asignar Conservadores </h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('establecimientos/'.$establecimiento->id.'/asignar-conservadores'), 'novalidate' => ''] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
				            <div class="row" id="filtros">
						            <div class="row">
						            	<div class="form-group col-xs-3 col-sm-2 col-md-2 col-lg-3">
								            {!! Form::label('f_num_res_asignar', 'Mostrar', ['class' => 'control-label']) !!}
							            	{!! Form::select('f_num_res_asignar', ['' => '', 5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100, 200 => 200], Input::get('f_num_res_asignar', 10), 
							            	['class' => 'form-control chosen-select', 'data-placeholder' => 'No. Resultados'] ) !!}
								        </div>
							            <div class="form-group col-sm-8 col-md-9 col-lg-4">
								            {!! Form::label('f_modelo_asignar', 'Modelo', ['class' => 'control-label']) !!}
								            {!! Form::select('f_modelo_asignar', \App\Models\ModeloConservador::all()->pluck('modelo', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Modelo'] ) !!}
								        </div>
							            <div class="form-group col-sm-4 col-md-3 col-lg-3">
								            {!! Form::label('f_mtto_asignar', 'Último Mantenimiento', ['class' => 'control-label']) !!}
							            	{!! Form::select('f_mtto_asignar', ['' => 'Ver todos', '5' => '5 días', '10' => '10 días','15' => '15 días','20' => '20 días','30' => '30 días'], 
							            	Input::get('f_mtto_asignar', 'todos'), ['class' => 'form-control selectdp', 'placeholder' => ''] ) !!}
								        </div>

							            <div class="form-group col-sm-4 col-md-3 col-lg-2">
								            {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
											<button type="button" class="btn btn-primary col-xs-12" onclick="filtrarConservadores()"><i class="fa fa-filter"></i> Filtrar</button>
								        </div>
						            </div>
				            </div>
						</div>				
					</div>

					<div class="row" id="conservadores" style="overflow-y: scroll; max-height: 300px;">	
						<div class="table-responsive">
                            <table class="table table-striped table-fixed" id="tablaConservadores">
                                <thead>
                                <tr>
                                	<th class="col-xs-1">ID</th>
                                    <th class="col-xs-2">Modelo</th>
                                    <th class="col-xs-2">Status</th>
                                    <th class="col-xs-3">Producto</th>
                                    <th class="col-xs-3">Capacidad</th>
                                    <th class="col-xs-1">Asignar</th>
                                </tr>
                                </thead>
                                <tbody id="body_tabla_conservadores">
                                @php
									$conservadores = \App\Models\Conservador::where('status', '=', 'stand_by')->get();
								@endphp
								@foreach($conservadores as $conservador)
									<tr>
                                        <td class="col-xs-1">{{$conservador->id_codigo}}</td>
                                        <td class="col-xs-2">{{$conservador->modelo->modelo}}</td>
                                        <td class="col-xs-2"><span class="label label-success">Stand By</span></td>
                                        <td class="col-xs-3">{{$conservador->producto->nombre}}</td>
                                        <td class="col-xs-3">{{$conservador->modelo->capacidad}} bolsas</td>
                                        <td class="col-xs-1"><input type="checkbox" value="{{$conservador->id}}" class="i-checks" name="asignar_conservador[]"></td>
                                    </tr>
								@endforeach
                                </tbody>
                            </table>
                        </div>
					</div>

					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Asignar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_conservador_asignar'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		$('#modalConservadorAsignar').modal('show');
	@endif

	function filtrarConservadores() {

		var FormData = [];
		FormData.push({"num_res":$("#f_num_res_asignar").val(),"modelo":$("#f_modelo_asignar").val(),"ult_mtto":$("#f_mtto_asignar").val()});

		$.ajax({
            type: 'POST',
            url: '{{url('ajax/establecimientos')}}',
            data:{ 'type': '2', 'FormData': FormData, '_token': '{{ csrf_token()}}' },
            success: function(data) {
            	$("#tablaConservadores tbody").empty();
            	$("#body_tabla_conservadores").html(data);

				$("span.pie").peity("pie");
			    $('.i-checks').iCheck({
		            checkboxClass: 'icheckbox_square-green',
		            radioClass: 'iradio_square-green',
		        });
            }
        });
	}
	
</script>	
@endsection