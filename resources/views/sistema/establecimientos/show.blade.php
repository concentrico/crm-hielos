@extends('sistema.layouts.master')

@section('title', 'Establecimientos')

@section('attr_navlink_4', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('fullcalendar/fullcalendar.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css">
<style type="text/css" media="screen">
 .highcharts-button {
    display:none;
}   
</style>
@endsection

@section('breadcrumb_title', $establecimiento->sucursal)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('establecimientos') }}">Establecimientos</a></li>
<li class="active"><strong>{{ $establecimiento->sucursal }}</strong></li>
<li></li>
@endsection

@section('content')

<style type="text/css" media="screen">
	table.footable-details>tbody>tr>th {
	    width: 210px!important;
	}
	.pac-container{z-index: 999999;}
    .labels {
     color: red;
     background-color: white;
     font-family: "Lucida Grande", "Arial", sans-serif;
     font-size: 10px;
     font-weight: bold;
     text-align: center;
     width: 40px;
     border: 2px solid black;
     white-space: nowrap;
   }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
	            
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
                		<i style="margin:0px;" class="fa fa-address-card fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Datos de la Sucursal</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 5) active @endif">
                	<a data-toggle="tab" href="#tab-5">
                		<i style="margin:0px;" class="fa fa-area-chart fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Consumo</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 6) active @endif">
                	<a data-toggle="tab" href="#tab-6">
                		<i style="margin:0px;" class="fa fa-calendar fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Venta Semanal</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 2) active @endif">
                	<a data-toggle="tab" href="#tab-2">
                		<i style="margin:0px;" class="fa fa-map fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Contactos ({{ count($establecimiento->contactos) }})<span> 
	                </a>
	            </li>
	            <li class="@if(Input::get('active_tab') == 3) active @endif">
                	<a data-toggle="tab" href="#tab-3">
                		<i style="margin:0px;" class="fa fa-snowflake-o fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Conservadores ({{(count($establecimiento->conservadores)) }})<span> 
	                </a>
	            </li>
                <li class="@if(Input::get('active_tab') == 4) active @endif">
                	<a data-toggle="tab" href="#tab-4">
                		<i style="margin:0px;" class="fa fa-file-text fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Pedidos ({{ count($establecimiento->pedidos) }})<span> 
	                </a>
	            </li>     
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.datos')
							</div>
							<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.ubicacion')				
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.acuerdos_comerciales')
							</div>
							<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.datosf')			
							</div>	
						</div>
                    </div>
                </div>

                <div id="tab-5" class="tab-pane @if(Input::get('active_tab') == 5) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.consumo')
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-6" class="tab-pane @if(Input::get('active_tab') == 6) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.venta')
							</div>
						</div>
                    </div>
                </div>
                
                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.contactos')
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.conservadores')
							</div>
						</div>
                    </div>
                </div>
                <div id="tab-4" class="tab-pane @if(Input::get('active_tab') == 4) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.establecimientos.iboxes.pedidos')
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('sistema.establecimientos.modals.edit')
@include('sistema.establecimientos.modals.fiscales.create')
@include('sistema.establecimientos.modals.fiscales.edit')
@include('sistema.establecimientos.modals.fiscales.delete')
@include('sistema.establecimientos.modals.contacto.create')
@include('sistema.establecimientos.modals.contacto.edit')
@include('sistema.establecimientos.modals.contacto.delete')
@include('sistema.establecimientos.modals.conservadores.asignar')
@include('sistema.establecimientos.modals.conservadores.corte')
@include('sistema.establecimientos.modals.conservadores.agregar_producto')
@include('sistema.establecimientos.modals.conservadores.delete_cf')
@include('sistema.establecimientos.modals.pedidos.create')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>   
<script src="{{ asset('fullcalendar/fullcalendar.js') }}"></script> 
<script src="{{ asset('fullcalendar/locale/es.js') }}"></script>    

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>
<script type="text/javascript">	

	@if(session('show_tab_contactos'))
		$('.nav-tabs a[href="#tab-2"]').tab('show');
	@endif

	@if(session('show_tab_conservadores'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
	@endif

	@if(session('corte_success'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif

	@if(session('set_ubicacion_success'))
		swal('Correcto', "Se ha establecido la ubicación del Establecimiento satisfactoriamente.", "success");
	@endif

	@if(session('update_acuerdos_success'))
		swal('Correcto', "Se han guardado los acuerdos comerciales satisfactoriamente.", "success");
	@endif
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	$('#est_municipio').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '1', busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

	$('#est_codigo_postal').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '2', municipio: $('#est_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });
		
	$('#est_colonia').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '3', codigo_postal: $('#est_codigo_postal').val(), municipio: $('#est_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

	function setColonia(colonia) {

		if (colonia != '') {
			$.ajax({
	            type: 'GET',
	            url: '{{url('ajax/autocomplete-codigospostales')}}',
	            data:{ 'type': '4', 'colonia': colonia, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	if (data.length > 1) {

	            		html='';
	            		html2='';
					    $.each(data, function(key, value) {
				            html += "<option value=" + value['municipio']  + ">" +value['municipio'] + "</option>"
				            html2 += "<option value=" + value['codigo_postal']  + ">" +value['codigo_postal'] + "</option>"
				        });

	            		$("#est_municipio")
						    .replaceWith('<select id="est_municipio" name="est_municipio" class="form-control">'+html+'</select>');
						$("#est_municipio").dropdown();

	            		$("#est_codigo_postal")
						    .replaceWith('<select id="est_codigo_postal" name="est_codigo_postal" class="form-control">'+html2+'</select>');
						$("#est_codigo_postal").dropdown();

	            	} else {

	            		if(!$("#est_municipio").is("select")) {
						    // no es un select
						    $('#est_municipio').val(data[0]['municipio']);
	            			$('#est_codigo_postal').val(data[0]['codigo_postal']);
						} else {
							// Es un select
							$('#est_codigo_postal').destroyDropdown();
							$('#est_municipio').destroyDropdown();

							$("#est_municipio")
						    .replaceWith('<input type="text" id="est_municipio" name="est_municipio" class="form-control" value="'+data[0]['municipio']+'"/>');

							$("#est_codigo_postal")
						    .replaceWith('<input type="text" id="est_codigo_postal" name="est_codigo_postal" class="form-control positive-integer" value="'+data[0]['codigo_postal']+'" data-mask="99999" maxlength="5" />');

						}
	            		
	            	}
	            	
	            }
	        });
		} else {

			if($("#est_municipio").is("select")) {

				// Es un select
				$('#est_codigo_postal').destroyDropdown();
				$('#est_municipio').destroyDropdown();

				$("#est_municipio")
			    .replaceWith('<input type="text" id="est_municipio" name="est_municipio" class="form-control" value=""/>');

				$("#est_codigo_postal")
			    .replaceWith('<input type="text" id="est_codigo_postal" name="est_codigo_postal" class="form-control positive-integer" value="" data-mask="99999" maxlength="5" />');

			}

		}
	
	}

	$.fn.destroyDropdown = function() {
		return $(this).each(function() {
			$(this).parent().dropdown( 'destroy' ).replaceWith( $(this) );
		});
	};

	var establecimiento = {
		id : {{ $establecimiento->id }},
	};

	// DATOS CONTACTOS
	var contactos = [];
	
	@foreach($establecimiento->cliente->contactos as $contacto)
	
	contactos[{{ $contacto->id }}] = {
		nombre_completo : '{{ $contacto->nombre_completo() }}',
		nombre : '{{ $contacto->nombre }}',
		apellido_paterno : '{{ $contacto->apellido_paterno }}',
		apellido_materno : '{{ $contacto->apellido_materno }}',
		emails : [
			@foreach($contacto->emails as $email)
				{
				id : '{{ $email->id }}',
				tipo : '{{ $email->tipo }}',
				correo : '{{ $email->valor }}'
				},
			@endforeach
		],
		telefonos : [
			@foreach($contacto->telefonos as $telefono)
				{
				id : '{{ $telefono->id }}',
				tipo : '{{ $telefono->tipo }}',
				telefono : '{{ $telefono->valor }}',
				telefono_ext : '{{ $telefono->extension }}'
				},
			@endforeach
		],
		comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $contacto->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
	}
	
	@endforeach

	// DATOS FISCALES
	var datosf = [];
	@if($establecimiento->datos_fiscales != null)
		datosf[{{ $establecimiento->datos_fiscales->id }}] = {
			tipo_persona : '{{ $establecimiento->datos_fiscales->persona }}',
			rfc : '{{ $establecimiento->datos_fiscales->rfc }}',
			razon_social : '{{ $establecimiento->datos_fiscales->razon_social }}',
			representante_legal : '{{ $establecimiento->datos_fiscales->representante_legal }}',
			calle : '{{ $establecimiento->datos_fiscales->calle }}',
			numero_exterior : '{{ $establecimiento->datos_fiscales->numero_exterior }}',
			numero_interior : '{{ $establecimiento->datos_fiscales->numero_interior }}',
			colonia : '{{ $establecimiento->datos_fiscales->colonia }}',
			codigo_postal : '{{ $establecimiento->datos_fiscales->codigo_postal }}',
			municipio : '{{ $establecimiento->datos_fiscales->municipio }}',
			estado : '{{ $establecimiento->datos_fiscales->estado }}',
		}
    @endif

    var marker = '';

    function initMap() {

      @if($establecimiento->latitud_longitud != null) 

      	var input = document.getElementById('latitud_longitud').value;
	  	var latlngStr = input.split(',', 2);
	  	var myLatLng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  @else
	  	var myLatLng = {lat: 25.6510566, lng: -100.4025978};
	  @endif

      var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 18,
	    center: myLatLng
	  });

	  var geocoder = new google.maps.Geocoder();

	  var infowindow = new google.maps.InfoWindow({
	      disableAutoPan: true
	   });

	  @if($establecimiento->latitud_longitud != null) 
	  	geocodeLatLng(geocoder, map, infowindow);
	  @endif

	  document.getElementById('btnBuscarDireccion').addEventListener('click', function() {
	    geocodeAddress(geocoder, map, infowindow);
	  });
	  
	  $('#direccion2').keypress(function (e) {
		 var key = e.which;
		 if(key == 13) { // the enter key code
		    geocodeAddress(geocoder, map, infowindow);
		  }
	  }); 

	}

	function geocodeLatLng(geocoder, map, infowindow) {

	  var input = document.getElementById('latitud_longitud').value;
	  var latlngStr = input.split(',', 2);
	  var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  $('#btnGuardarUbicacion').removeAttr('disabled');

	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[1]) {

	        map.setZoom(16);

	        var marker = new google.maps.Marker({
	          position: latlng,
	          //position: results[1].geometry.location,
	          map: map,
	          draggable: true,
    		  animation: google.maps.Animation.DROP,
	        });
	        infowindow.setContent('{{$establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$establecimiento->sucursal}}</b>');
	        infowindow.open(map, marker);

	        google.maps.event.addListener(marker, "drag", function (mEvent) { 
		      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
		      	$('#latitud_longitud').val(latlng);
		    });

	      } else {
	        sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	      }
	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

  	function geocodeAddress(geocoder, resultsMap, infowindow) {
	  var address = document.getElementById('direccion2').value;
	  geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {

	      resultsMap.setCenter(results[0].geometry.location);

	      var lat = results[0].geometry.location.lat();
	      var lng = results[0].geometry.location.lng();
	      $('#latitud_longitud').val(lat+', '+lng);
	      $('#btnGuardarUbicacion').removeAttr('disabled');

	      if ( !marker == '' ) {
		    //Destroy marker and create new
		  	marker.setMap(null);
		  } 

		  marker = new google.maps.Marker({
	        map: resultsMap,
	        draggable: true,
    		animation: google.maps.Animation.DROP,
	        position: results[0].geometry.location
	      });

        	infowindow.setContent('{{$establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$establecimiento->sucursal}}</b>');
        	infowindow.open(map, marker);

	      google.maps.event.addListener(marker, "drag", function (mEvent) { 
	      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
	      	$('#latitud_longitud').val(latlng);
	      });


	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

	// FIX TO RESIZE AND SHOW MAP DIV 
	$("#modalUbicacion").on("shown.bs.modal", function () {
	    google.maps.event.trigger(map, "resize");
	});

</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{getSystemSettings()->google_maps_api_key}}&callback=initMap"
        async defer></script>

@endsection