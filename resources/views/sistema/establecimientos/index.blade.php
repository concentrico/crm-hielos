@extends('sistema.layouts.master')

@section('title', 'Lista de Establecimientos')

@section('attr_navlink_4', 'class="active"')

@section('breadcrumb_title', 'Establecimientos')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Establecimientos</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.establecimientos.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.establecimientos.iboxes.tabla')
	</div>
</div>



@include('sistema.establecimientos.modals.create')
@include('sistema.establecimientos.modals.delete')
@include('sistema.establecimientos.modals.reactivar')


@endsection

@section('assets_bottom')
<script type="text/javascript">

	establecimientos = [];
	
	@foreach($establecimientos as $establecimiento)
	
		establecimientos[{{ $establecimiento->id }}] = {
			sucursal: '{{ $establecimiento->sucursal }}',
		}
	
	@endforeach

</script>
@endsection
