<!-- resources/views/auth/reset.blade.php -->
<html lang="es"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
	<meta name="author" content="Concéntrico">
	<link rel="icon" href="{{ asset('favicon.png') }}">
    
    <title> @if(getSystemSettings()->site_name != null) {{getSystemSettings()->site_name}} @else {{env('APP_NAME')}} @endif | Recuperar Contraseña</title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Custom styles  -->
	<link rel="stylesheet" href="{{ asset('css/components.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/login.min.css') }}">
  	
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <style type="text/css" media="screen">
        .bg { 
            /* The image used */
            background-image: url("{{ asset('img/bg.jpg') }}");

            /* Full height */
            height: 100%; 

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        .login {
            background-color: #5f7083!important;
        }
    </style>
    </head>

  <body class="login bg">

  	<!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{url('/')}}">
        	@if(getSystemSettings()->site_logo != null) 
                <img src="{{ asset('/') }}/{{getSystemSettings()->site_logo}}" alt="Logo" height="100">
            @else 
                <img src="{{ asset('img/logo-default.png') }}" alt="Logo" height="100"> 
            @endif
        </a>
    </div>
    <!-- END LOGO -->


    <div class="content">
    	
		{!! Form::open(['method' => 'POST',  'url' => url('password/email'), 'class' => 'login-form'] ) !!}
			<h3 class="form-title text-center">Recuperar Contraseña</h3>

			@if (count($errors) > 0)
			    <div class="alert alert-danger text-center">
			        <ul style="padding: 0px;">
			            @foreach ($errors->all() as $error)
			                <li style="list-style: none;">{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			@if (session('status'))
			    <div class="alert alert-success text-center">
			        {{ session('status') }}
			    </div>
			@endif
			
			<label class="text-success text-center"></label>
			{!! Form::label('email', 'Email', ['class' => 'sr-only text-center']) !!}

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Ingresa el email con el que tienes registrado el usuario</label>
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuario/Email" name="email" id="inputEmail" /> </div>
            </div>
            <div class="g-recaptcha" data-sitekey="{{getSystemSettings()->google_recaptcha_api_key}}"></div>
            <BR/>
			<button class="btn btn-lg btn-primary btn-block btn-login" type="submit">Enviar liga</button>
		{!! Form::close() !!}

    </div> <!-- /container -->

	<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  

	</body>
</html>