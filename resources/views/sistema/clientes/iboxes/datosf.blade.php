<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-gavel"></i> Datos fiscales</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteDatosFiscalesCreate">
			<i class="fa fa-plus"></i> Datos fiscales
		</a>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">

	    @if(count($cliente->datos_fiscales) == 0)
		<div class="alert alert-warning" style="margin-top: 10px;">
			El cliente no tiene datos fiscales registrados.
		</div>
	    @endif

		@foreach($cliente->datos_fiscales as $key => $df)
	    <div class="row">
		    <div class="col-sm-12">
				<a class="text-danger pull-right" onclick="showModalClienteDatosFiscalesDelete({{$df->id}})">
					<i class="fa fa-trash"></i> Eliminar
				</a>
				<a class="text-success pull-right" onclick="showModalClienteDatosFiscalesEdit({{$df->id}})">
					<i class="fa fa-edit"></i> Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</a>
				<h4 class="text-info">{{ $df->rfc }}</h4>
				<span style="font-weight: bold;">Tipo de persona: </span>
				@if($df->persona == null)
					<span class="text-muted"> N.D. </span>
				@else
					<span class="text-primary"> {{ ucfirst($df->persona) }}</span>
				@endif
				<br />
				<span style="font-weight: bold;">
				@if($df->persona == 'moral')
					Razón social: 
				@else
					Nombre completo: 
				@endif
				</span>
				@if($df->razon_social == null)
					<span class="text-muted"> N.D. </span>
				@else
					<span class="text-primary"> {{ $df->razon_social }}</span>
				@endif
				<br />
					<span style="font-weight: bold;">
						Representante Legal: 
					</span>
					<span class="text-primary"> {{ $df->representante_legal }}</span>
				@if( imprime_direccion($df) !== "")
				<h4 class="text-success">Dirección de facturación</h4>
				<address>
					{!! imprime_direccion($df) !!}
				</address>				
				@endif
		    </div>
	    </div>
	    
	    @if(count($cliente->datos_fiscales) > $key+1)
			<hr />
	    @endif

	    @endforeach
	    
	    
    </div>
    
</div>