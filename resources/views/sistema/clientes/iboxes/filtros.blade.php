<div class="ibox float-e-margins filtros-res">
    <div class="ibox-title">
        <h5><i class="fa fa-filter"></i> Filtrar resultados</h5>
        <div class="ibox-tools pull-right">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
		{!! Form::open(['method' => 'GET',  'url' => url('clientes')] ) !!}
			<input type="hidden" name="orderBy" value="{{ Input::get('orderBy') }}"/>
			<input type="hidden" name="sort" value="{{ Input::get('sort') }}"/>
            <div class="row">
	            <div class="form-group col-xs-3 col-sm-2 col-md-2 col-lg-1">
		            {!! Form::label('f_num_resultados', 'Mostrar', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_num_resultados', ['' => '', 5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100, 200 => 200], Input::get('f_num_resultados', 25), 
	            	['class' => 'form-control selectdp', 'data-placeholder' => 'No. Resultados'] ) !!}
		        </div>
	            <div class="form-group col-xs-9 col-sm-6 col-md-4 col-lg-4">
		            {!! Form::label('f_nombre', 'Búsqueda', ['class' => 'control-label']) !!}
		            {!! Form::text('f_nombre', Input::get('f_nombre'), ['class' => 'form-control', 'placeholder' => 'Nombre Cliente']) !!}
		        </div>
	            <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-2">
		            {!! Form::label('f_estatus', 'Status del Cliente', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_estatus', ['todos' => 'Ver Todos', 'activo' => 'Activo', 'suspendido' => 'Suspendido'], Input::get('f_estatus', 'activo'), 
	            	['class' => 'form-control selectdp', 'data-placeholder' => 'Status'] ) !!}
		        </div>
	            <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-3">
		            {!! Form::label('f_estatus_cobranza', 'Cobranza', ['class' => 'control-label']) !!}
	            	{!! Form::select('f_estatus_cobranza', ['todos' => 'Ver Todos', 'vencido' => 'Saldo Vencido'], Input::get('f_estatus_cobranza', 'todos'), 
	            	['class' => 'form-control selectdp', 'data-placeholder' => 'Status Cobranza'] ) !!}
		        </div>
	            <div class="form-group col-xs-12 col-sm-3 col-md-2 col-lg-2">
		            {!! Form::label('', '&nbsp;', ['class' => 'control-label']) !!}
					<button type="submit" class="btn btn-primary col-xs-12"><i class="fa fa-filter"></i> Filtrar</button>
		        </div>
            </div>			
		{!! Form::close() !!}
	</div>
</div>
