 <div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteCreate">
			<i class="fa fa-plus"></i> Cliente
		</a>
        <h5><i class="fa fa-list"></i> Lista de clientes</h5>
    </div>
    <div class="ibox-content">

    	<div class="row m-b-lg">
			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-address-book-o fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Clientes </h4>
			                <h1 class="font-bold">{{ $total_clientes }}</h1>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-lg-4">
			    &nbsp;
			</div>

			<div class="col-md-4 col-lg-4">
			    <div class="widget style-custom">
			        <div class="row">
			            <div class="col-xs-2 text-center">
			                <i class="fa fa-map fa-5x"></i>
			            </div>
			            <div class="col-xs-10 text-right">
			                <h4 style="font-size: 1.8em;"> Establecimientos </h4>
			                <h1 class="font-bold">{{ $total_establecimientos }}</h1>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $clientes->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
        <div>
			<table id="dataTableClientes" name="dataTableClientes" class="table table-res" data-cascade="true" data-expand-first="false">
				<thead>
					<tr>
						<th data-type="html" style="min-width: 75px;">{!! getHeaderLink('ID', 'ID') !!}</th>
						<th data-type="html">{!! getHeaderLink('NOMBRE', 'Nombre Corto') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('TIPO', 'Tipo') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('ESTABLECIMIENTOS', 'Establecimientos') !!}</th>
						<th data-type="html" data-breakpoints="sm">Saldo</th>
						<th data-type="html" data-breakpoints="sm">Vencido</th>
						<th data-type="html" data-breakpoints="sm">Última Entrega</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html" data-breakpoints="sm"></th>
					</tr>
		        </thead>
			<tbody>
                    @foreach ($clientes as $cliente)
	                    <tr class="@if($cliente->deleted_at != NULL) warning @endif">
	                        <td class="text-center">{{ $cliente->id }}</td>
	                        <td><a href="{{ url('clientes/'.$cliente->id) }}">{{ $cliente->nombre_corto }}</a></td>
	                        <td>
	                        	@if($cliente->tipo == 'horeca')
	                        		<span class="label label-success" style="margin-top: 5px; display:inline-block;">Horeca</span>
		                       	@else
		                       		<span class="label label-default" style="margin-top: 5px; display:inline-block;">Estándar</span>
		                        @endif
		                    </td>
		                    <td>
		                        <span>
									@if(count($cliente->establecimientos) > 0)
										@php $cont=0; @endphp
										@foreach($cliente->establecimientos as $key => $establecimiento)

											@if($cont < 2)
												<span class="label label-primary" style="margin-top: 5px; display:inline-block;cursor:pointer;" onclick="location.href='{{ url('establecimientos/'.$establecimiento->id) }}'">{{ $establecimiento->sucursal }}</span>

												@php $cont++; @endphp
											@endif
										@endforeach
										@if(count($cliente->establecimientos) > 2)
											@php $rest =count($cliente->establecimientos)-2; @endphp
											<span class="label label-default" style="margin-top: 5px; display:inline-block;">+{{$rest}}</span>
										@endif
										<br />
									@endif
		                        </span>
		                    </td>
		                    <td class="text-center">
			                    <span>
			                    	${{ $cliente->saldo() }} 
				                </span>
		                    </td>
	                        <td class="text-center">
	                        	@if($cliente->saldo_porcobrar() > 0.00)
				                    <span class="text-danger">
				                    	${{ $cliente->saldo_porcobrar() }} 
					                </span>
					            @else
					            	<span>
				                    	${{ $cliente->saldo_porcobrar() }} 
					                </span>
					            @endif
		                    </td>
	                        <td class="text-center">
	                       		{!! $cliente->ultima_entrega() !!}
		                    </td>
	                        <td>{{ $cliente->created_at }}</td>
	                        <td>{!! $cliente->creado_por->liga() !!}</td>
	                        <td>{{ $cliente->updated_at }}</td>
	                        <td>{!! $cliente->actualizado_por->liga() !!}</td>
	                        <td class="center">
		                        <div class="btn-group pull-right">
			                        @if($cliente->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalClienteReactivar({{ $cliente->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
			                        @else
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalClienteSuspender({{ $cliente->id }});">
			                        	<i class="fa fa-trash"></i>
			                        </button>
			                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>
        </div>
        <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $clientes->appends(Input::except('page'))->render() !!}
			</div>
	    </div>
    </div>
</div>