<div class="ibox float-e-margins">
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteEdit">
					<i class="fa fa-edit"></i> Editar
				</a>
			    <h2 class="text-info">
				    {{ $cliente->nombre_corto }}
				</h2>
			</div>
			<div class="col-sm-6 pull-left" style="text-align: left;">
				<strong>Tipo:</strong>
				@if($cliente->tipo == 'horeca')
					<span class="label label-success" style="margin-top: 5px; display:inline-block;">Horeca</span>
				@else
					<span class="label label-default" style="margin-top: 5px; display:inline-block;">Estándar</span>
				@endif
			</div>
			@if($cliente->es_preferencial != null)
				<div class="col-sm-6 pull-right" style="text-align: right;">
	                <span class="fa fa-star m-r-xs fa" style="color:#ffea3e;"></span>
	                <label>Cliente Preferencial</label>
				</div>
			@endif
		</div>
		<br />
		<h4 class="text-success">Comentarios</h4>

		<div class="row">
			@if($cliente->comentarios)
				<div class="col-sm-12">
					<span>{!! nl2br(e($cliente->comentarios))!!}</span>
				</div>
			@else
				<div class="col-sm-12"><p>No hay comentarios</p></div>
			@endif
		</div>
		<hr />

	    
	    <div class="row">
		    <div class="col-sm-6">
				<small class="text-muted">Creado:  {{ $cliente->created_at }} <br /> 
				<a href="{{ url('usuarios/' . $cliente->created_by) }}">{{ $cliente->creado_por->nombre }}</a></small><br />
		    </div>
		    <div class="col-sm-6">
				<small class="text-muted">Actualizado: {{ $cliente->updated_at }} <br /> 
				<a href="{{ url('usuarios/' . $cliente->updated_by) }}">{{ $cliente->actualizado_por->nombre }}</a></small><br />
		    </div>
	    </div>
    </div>
</div>
@if($cliente->tipo == 'standard')
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-handshake-o"></i> Acuerdos Comerciales</h5>
    </div>
    <div class="ibox-content">
		{!! Form::open(['method' => 'PUT',  'url' => url('clientes/' . $cliente->id.'/update-acuerdos')] ) !!}
		<div class="row">
			<div class="col-sm-6">
				<h4>Forma de Pago</h4>
				{!! Form::select('forma_pago', App\Models\FormaPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), $cliente->forma_pago_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Forma de Pago'] ) !!}
				<span class="help-block text-danger">{{ $errors->first('forma_pago')}}</span>
			</div>
			<div class="col-sm-6">
				<h4>Condiciones de Pago</h4>
				{!! Form::select('condicion_pago', App\Models\CondicionPago::all()->pluck('nombre', 'id')->prepend('', '')->toArray(), $cliente->condicion_pago_id, ['class' => 'form-control selectdp', 'data-placeholder' => 'Condiciones de Pago'] ) !!}
				<span class="help-block text-danger">{{ $errors->first('condicion_pago')}}</span>
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-12">
				<div class="table-responsive">
	                <table class="table table-striped table-fixed" id="tablaProductosCliente">
	                    <thead>
	                    <tr>
	                        <th class="col-xs-8">Producto</th>
	                        <th class="col-xs-4">Precio de Venta</th>
	                    </tr>
	                    </thead>
	                    <tbody id="body_tabla_productos_cliente" style="height:auto;">
	                    @if(count($cliente->precios) > 0)
							@foreach($cliente->precios as $precios)
								<tr>
		                            <td class="col-xs-8">
			                            <div class="input-group" style="width: 100%;">
								           	<span class="input-group-addon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
								           		{!! Form::text('producto[]', $precios->producto->nombre, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
											<input type="hidden" name="producto_id[]" value="{{$precios->producto_id}}"/>
										</div>
										<span class="help-block">{{ $errors->first('producto')}}</span>
		                            </td>
		                            <td class="col-xs-4">
		                            	<div class="input-group" style="width: 100%;">
								           	<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
												{!! Form::text('precio_venta[]', $precios->precio_venta, ['class' => 'form-control decimal-2-places', 'placeholder' => '0.00']) !!}
										</div>
										<span class="help-block">{{ $errors->first('precio_venta')}}</span>
									</td>
		                        </tr>
							@endforeach
						@endif
						<tr>
							<td class="col-xs-12 text-center"><a data-toggle="modal" data-target="#modalAgregarProducto">Agregar Producto</a></td>
						</tr>
	                    </tbody>
	                </table>
	            </div>
	        </div>
        	<div class="col-sm-12">
	            <button type="submit" id="btnSaveAcuerdos" class="btn btn-outline btn-primary btn-sm pull-right">
		        	<i class="fa fa-save"></i> Guardar
		        </button>
		    </div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@endif