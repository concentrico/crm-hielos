<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-users"></i> Contactos</h5>
		<a class="text-success pull-right" id="btnContactoCreate" onclick="showModalClienteContactoCreate(2)">
			<i class="fa fa-plus"></i> Contacto
		</a>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12">
				@if(count($cliente->contactos) <= 0 )
				<div class="alert alert-warning" style="margin-top: 10px;">
					El cliente no tiene contactos registrados!
				</div>
				@else
					@foreach($cliente->contactos as $contacto)

						<a class="text-danger pull-right" onclick="showModalClienteContactoDelete({{$contacto->id}})">
							<i class="fa fa-trash"></i> Eliminar
						</a>
						<a class="text-success pull-right" onclick="showModalClienteContactoEdit({{$contacto->id}})">
							<i class="fa fa-edit"></i> Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</a>

						<h4 class="text-info">{{ $contacto->nombre_completo() }}</h4>
						@if($cliente->tipo == 'horeca')
						<div class="row">
							<div class="col-sm-12">
								<span style="font-weight: bold;">Establecimiento(s):</span>
								<span>
								@if(count($contacto->establecimientos) > 0)
									@foreach($contacto->establecimientos as $establecimiento)
										<span class="label label-primary" style="margin-top: 5px; display:inline-block;">{{ $establecimiento->sucursal }}</span>
									@endforeach
								@else
									<span class="label label-default" style="margin-top: 5px; display:inline-block;">N.D.</span>
								@endif
								</span>
							</div>
						</div>
						@endif
						@if(count($contacto->emails) == 0 && count($contacto->telefonos) == 0)
							<small class="text-warning"> No existen medios de contacto disponibles para este contacto</small>
						
						@else
							<div class="row">
								@if(count($contacto->emails) > 0)
									<div class="col-sm-6">
										<br />
										<h5><i class="fa fa-envelope-o"></i> Email(s)</h5>
									@foreach($contacto->emails as $email)
										@if($email->tipo == 'personal')
											<i class="fa fa-home" style="width:16px"></i> {{ $email->valor }}<br />
										@elseif($email->tipo == 'trabajo')
											<i class="fa fa-building" style="width:16px"></i> {{ $email->valor }}<br />
										@else
											<i class="fa fa-circle" style="width:16px"></i> {{ $email->valor }}<br />
										@endif
									@endforeach
									</div>
								@endif
	
								@if(count($contacto->telefonos) > 0)
									<div class="col-sm-6">
										<br />
										<h5><i class="fa fa-phone"></i> Teléfono(s)</h5>
									@foreach($contacto->telefonos as $telefono)
										
										@if($telefono->tipo == 'casa')
											<i class="fa fa-home" style="width:16px"></i>
										@elseif($telefono->tipo == 'oficina')
											<i class="fa fa-building" style="width:16px"></i>
										@elseif($telefono->tipo == 'celular')
											<i class="fa fa-lg fa-mobile" style="width:16px"></i>
										@else
											<i class="fa fa-circle" style="width:16px"></i>
										@endif
										
										{{ $telefono->valor }} 
										
										@if($telefono->extension != null)
											&nbsp;&nbsp;&nbsp;Ext. {{ $telefono->extension }}
										@endif
										
										<br />
										
									@endforeach
									</div>
								@endif
							</div>
						@endif
						
						@if($contacto->comentarios != null)
							<div class="row">
								<div class="col-sm-12">
									<h5> Comentarios </h5>
									<span>{!! nl2br(e($contacto->comentarios)) !!}</span>
								</div>
							</div>
						@endif					
						<hr />

					@endforeach
				@endif
		    </div>
	    </div>
    </div>
</div>