<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="flaticon-clipboard" style="font-size: 0.5em;"></i> Establecimientos</h5>
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteEstablecimientoCreate">
			<i class="fa fa-plus"></i> Agregar Establecimiento
		</a>
    </div>
    
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-6">
			    @if(count($cliente->establecimientos) == 0)
					<div class="alert alert-warning">
						El cliente no tiene establecimientos registrados!
					</div>
					<hr />
			    @else
					<table class="table table-res" data-cascade="true">
						<thead>
							<tr>
								<th data-type="html">{!! getHeaderLink('NOMBRE', 'Sucursal') !!}</th>
								<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('DIRECCION', 'Dirección') !!}</th>

								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('TIPO', 'Tipo') !!}</th>

								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('RAZONSOCIAL', 'Razón Social') !!}</th>
								
								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
								<th data-type="html" data-breakpoints="xxlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
								<th data-type="html" data-breakpoints="xxlg"></th>
							</tr>
						</thead>
						<tbody>
		                    @foreach ($cliente->establecimientos as $establecimiento)
			                    <tr class="@if($establecimiento->deleted_at != NULL) warning @endif">
			                        <td><a href="{{ url('establecimientos/'.$establecimiento->id) }}">{{ $establecimiento->sucursal }}</a></td>
			                        <td>{{ $establecimiento->direccion() }}</td>

			                        <td>			
										@foreach($establecimiento->tipos as $key => $tipo)
											<span class="label label-default" style="margin-top: 5px; display:inline-block;">{{ $tipo->nombre }}</span>
										@endforeach
										<br />

			                        </td>
			                        <td>
			                        	@if($establecimiento->datos_fiscales != NULL)
			                        		{{ $establecimiento->datos_fiscales->razon_social }}
			                        	@else
			                        		N.D.
			                        	@endif
			                        </td>
			                        <td>{{ $establecimiento->created_at }}</td>
			                        <td><a href="{{ url('usuarios/'.$establecimiento->creado_por->id) }}">{{ $establecimiento->creado_por->nombre }}</a></td>
			                        <td>{{ $establecimiento->updated_at }}</td>
			                        <td><a href="{{ url('usuarios/'.$establecimiento->actualizado_por->id) }}">{{ $establecimiento->actualizado_por->nombre }}</a></td>
			                        <td>
			                        	<div class="btn-group pull-right">
			                       			<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalEstablecimientoSuspender({{ $establecimiento->id }});">
			                        			<i class="fa fa-arrow-down"></i>
			                        		</button>
			                        	</div>
			                        </td>
			                    </tr>
							@endforeach
						</tbody>
					</table>            	        
			    @endif
		    </div>
		    <div class="col-sm-6">
		    	<div id="map_sucursales" style="height:400px;"></div>
		    </div>
	    </div>
    </div>
</div>
