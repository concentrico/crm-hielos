<div class="modal inmodal" id="modalClienteCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-address-book modal-icon"></i> <br/>Registrar Cliente</h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('clientes'), 'enctype' => 'multipart/form-data',  'novalidate' => ''] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<h3>Información General</h3>
				            <div class="row">
					            <div class="form-group col-sm-8 @if ($errors->first('nombre_corto') !== "") has-error @endif">
						            {!! Form::label('nombre_corto', 'Nombre *', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
							           	<span class="input-group-addon"><i class="fa fa-vcard-o" aria-hidden="true"></i></span>
										{!! Form::text('nombre_corto', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
									</div>
						            <span class="help-block">{{ $errors->first('nombre_corto')}}</span>
						        </div>
						        <div class="form-group col-sm-4 @if ($errors->first('es_preferencial') !== "") has-error @endif">
						        	<label class="control-label">&nbsp;</label>
						        	<BR/>
						            <label>
									<input type="checkbox" id="es_preferencial" name="es_preferencial" class="i-checks" value="1" />
					            	Cliente Preferencial
					            	</label>
						            <span class="help-block">{{ $errors->first('es_preferencial')}}</span>
						        </div>
						    </div>
						    <div class="row">
						        <div class="form-group col-sm-4 @if ($errors->first('tipo') !== "" ) has-error @endif">
						            {!! Form::label('tipo', 'Tipo', ['class' => 'control-label']) !!}
					            	{!! Form::select('tipo', [ 'standard' => 'Estándar', 'horeca' => 'Horeca'], null, 
					            	['class' => 'form-control selectdp', 'data-placeholder' => 'Tipo'] ) !!}
						            <span class="help-block">{{ $errors->first('tipo')}}</span>
						        </div>
					            <div class="form-group col-sm-8 @if ($errors->first('comentarios') !== "") has-error @endif">
						            {!! Form::label('comentarios', 'Comentarios', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
									    <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
									    {!! Form::textarea('comentarios', null, ['id' => 'comentarios',  'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
							        </div>
						            <span class="help-block">{{ $errors->first('comentarios')}}</span>
						        </div>
				            </div>
						</div>				
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cliente_create'))
		$('#modalClienteCreate').modal('show');
	@endif
	
</script>	
@endsection