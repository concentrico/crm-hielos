<style type="text/css" media="screen">
.i-checks{ display: inline-block;}	
.alert{margin-bottom: 0px;}
</style>
<div class="modal inmodal" id="modalClientePedidoCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<i class="flaticon-2-contract"></i>
				<h4 class="modal-title text-success">Registrar Pedido</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('pedidos/cliente' )] ) !!}
			<input type="hidden" name="cliente_id" value="{{$cliente->id}}">
				<div class="modal-body">
					@if(session('errores_pedido_create') != null)
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								{{ session('errores_pedido_create') }}
							</div>
						</div>
					</div>
					@endif
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('cliente_establecimiento') !== "" || $errors->first('cliente_establecimiento')) has-error @endif">
				            {!! Form::label('cliente_establecimiento', 'Establecimiento', ['class' => 'control-label']) !!}
							<div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-map"></i></span>
		                        {!! Form::select('cliente_establecimiento',\App\Models\ClienteEstablecimiento::where('cliente_id', '=', $cliente->id)->pluck('sucursal', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Establecimiento'] ) !!}
							</div>
				            <span class="help-block">{{ $errors->first('cliente_establecimiento')}}</span>
				        </div>
				        <div class="form-group col-sm-6 @if ($errors->first('es_evento') !== "" || $errors->first('es_evento')) has-error @endif">
				            {!! Form::label('es_evento', '¿Es un Evento?', ['class' => 'control-label']) !!}
							<div class="input-group" style="width: 100%;">
		                        	<div class="i-checks"><label> <input type="radio" value="1" name="es_evento"> <i></i> Sí </label></div>
                                    <div class="i-checks" style="margin-left: 20px;"><label> <input type="radio" checked="" value="0" name="es_evento"> <i></i> No </label></div>
							</div>
				            <span class="help-block">{{ $errors->first('es_evento')}}</span>
				        </div>
		            </div>
		            <div class="alert alert-warning">
                        Ésta acción te redirigirá a la pantalla de Alta de Pedidos.
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar </button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">
	
	$('.select-modal-cot-create').select2({
	  dropdownParent: $('#modalClientePedidoCreate')
	});

	@if(session('show_modal_pedido_create'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
		$('#modalClientePedidoCreate').modal('show');
	@endif
	
</script>	
@endsection