<div class="modal inmodal" id="modalClienteDatosFiscalesDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					
					<h4 class="text-danger modal-title" style="color:#ed5565;"><i class="fa fa-legal modal-icon"></i> Eliminar Datos Fiscales</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-danger">¿Estas seguro de que deseas desactivar los datos fiscales?</h2>
				            <h3 style="font-weight: 300;"></h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('clientes/' . $cliente->id . '/datosf/' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-arrow-down"></i> Suspender</button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalClienteDatosFiscalesDelete(id){
		
		if ( $('#modalClienteDatosFiscalesDelete').find('form').attr('action') == "{{ url('clientes/' . $cliente->id . '/datosf' ) }}/" + id){
			
		}else{
			$('#modalClienteDatosFiscalesDelete').find('form').attr('action', "{{ url('clientes/' . $cliente->id . '/datosf' ) }}/" + id)
			
			$('#modalClienteDatosFiscalesDelete').find('h3').html("<b>" + datosf[id]['rfc'] + "</b>");
						
		}

		$('#modalClienteDatosFiscalesDelete').modal('show');
	}

</script>
@endsection