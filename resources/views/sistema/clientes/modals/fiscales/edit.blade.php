<div class="modal inmodal" id="modalClienteDatosFiscalesEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title">Editar Datos Fiscales</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('clientes/' . $cliente->id . '/datosf' )] ) !!}
				<div class="modal-body">
					<h3>Datos Principales</h3>
		            <div class="row">
			            <div class="form-group col-sm-5 col-md-4 @if ($errors->first('tipo_persona_edit') !== "") has-error @endif">
				            {!! Form::label('tipo_persona_edit', 'Tipo de Persona', ['class' => 'control-label']) !!}
			            	{!! Form::select('tipo_persona_edit', ['fisica' => 'Física', 'moral' => 'Moral', '' => ''], null, 
			            	['class' => 'form-control selectdp', 'data-placeholder' => 'Tipo de Persona'] ) !!}
				            <span class="help-block">{{ $errors->first('tipo_persona_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-7 col-md-3 @if ($errors->first('rfc_edit') !== "") has-error @endif">
				            {!! Form::label('rfc_edit', 'RFC', ['class' => 'control-label']) !!}
				            {!! Form::text('rfc_edit', null, ['class' => 'form-control', 'placeholder' => 'RFC']) !!}
				            <span class="help-block">{{ $errors->first('rfc_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-12 col-md-5 @if ($errors->first('razon_social_edit') !== "") has-error @endif">
				            {!! Form::label('razon_social_edit', 'Denominación o Razón Social', ['class' => 'control-label']) !!}
				            {!! Form::text('razon_social_edit', null, ['class' => 'form-control', 'placeholder' => 'Denominación o Razón Social']) !!}
				            <span class="help-block">{{ $errors->first('razon_social_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-12 col-md-12 @if ($errors->first('representante_legal_edit') !== "") has-error @endif">
				            {!! Form::label('representante_legal_edit', 'Representante Legal', ['class' => 'control-label']) !!}
				            {!! Form::text('representante_legal_edit', null, ['class' => 'form-control', 'placeholder' => 'En caso de personas Físicas']) !!}
				            <span class="help-block">{{ $errors->first('representante_legal_edit')}}</span>
				        </div>
		            </div>
					<h3>Dirección de Facturación</h3>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_calle_edit') !== "") has-error @endif">
				            {!! Form::label('df_calle_edit', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('df_calle_edit', null, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('df_calle_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_exterior_edit') !== "") has-error @endif">
				            {!! Form::label('df_no_exterior_edit', 'No. exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_exterior_edit', null, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_exterior_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_interior_edit') !== "") has-error @endif">
				            {!! Form::label('df_no_interior_edit', 'No. interior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_interior_edit', null, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_interior_edit')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-9 @if ($errors->first('df_colonia_edit') !== "") has-error @endif">
				            {!! Form::label('df_colonia_edit', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('df_colonia_edit', null, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('df_colonia_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_codigo_postal_edit') !== "") has-error @endif">
				            {!! Form::label('df_codigo_postal_edit', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('df_codigo_postal_edit', null, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('df_codigo_postal_edit')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_municipio_edit') !== "") has-error @endif">
				            {!! Form::label('df_municipio_edit', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('df_municipio_edit', null, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('df_municipio_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('df_estado_edit') !== "") has-error @endif">
				            {!! Form::label('df_estado_edit', 'Estado', ['class' => 'control-label']) !!}
				            {!! Form::text('df_estado_edit', 'Nuevo León', ['class' => 'form-control', 'placeholder' => 'Estado']) !!}
				            <span class="help-block">{{ $errors->first('df_estado_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cliente_datos_fiscales_edit'))
		$('#modalClienteDatosFiscalesEdit').find('form').attr('action', "{{ url('clientes/' . $cliente->id . '/datosf/' . session('datosf_id') ) }}")
		$('#modalClienteDatosFiscalesEdit').modal('show');
	@endif

	function showModalClienteDatosFiscalesEdit(id){
		
		if ( $('#modalClienteDatosFiscalesEdit').find('form').attr('action') == "{{ url('clientes/' . $cliente->id . '/datosf' ) }}/" + id){
			
		}else{
			$('#modalClienteDatosFiscalesEdit').find('form').attr('action', "{{ url('clientes/' . $cliente->id . '/datosf' ) }}/" + id)

			//$('#modalClienteDatosFiscalesEdit').find('select[name=tipo_persona_edit]').select2().val(datosf[id]['tipo_persona']).trigger("change");

			$('#modalClienteDatosFiscalesEdit').find('select[name=tipo_persona_edit]').dropdown('set selected', datosf[id]['tipo_persona']);

			$('#modalClienteDatosFiscalesEdit').find('input[name=rfc_edit]').val(datosf[id]['rfc']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=razon_social_edit]').val(datosf[id]['razon_social']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=representante_legal_edit]').val(datosf[id]['representante_legal']);

			$('#modalClienteDatosFiscalesEdit').find('input[name=df_calle_edit]').val(datosf[id]['calle']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_no_exterior_edit]').val(datosf[id]['numero_exterior']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_no_interior_edit]').val(datosf[id]['numero_interior']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_colonia_edit]').val(datosf[id]['colonia']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_codigo_postal_edit]').val(datosf[id]['codigo_postal']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_municipio_edit]').val(datosf[id]['municipio']);
			$('#modalClienteDatosFiscalesEdit').find('input[name=df_estado_edit]').val(datosf[id]['estado']);

		}

		$('#modalClienteDatosFiscalesEdit').modal('show');
	}

</script>
@endsection