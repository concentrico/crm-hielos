<div class="modal inmodal" id="modalClienteDatosFiscalesCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title">Registrar Datos Fiscales</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('clientes/' . $cliente->id . '/datosf' )] ) !!}
				<div class="modal-body">
					<h3>Datos Principales</h3>
		            <div class="row">
			            <div class="form-group col-sm-5 col-md-4 @if ($errors->first('tipo_persona') !== "") has-error @endif">
				            {!! Form::label('tipo_persona', 'Tipo de Persona', ['class' => 'control-label']) !!}
			            	{!! Form::select('tipo_persona', ['fisica' => 'Física', 'moral' => 'Moral', '' => ''], null, 
			            	['class' => 'form-control selectdp', 'data-placeholder' => 'Tipo de Persona'] ) !!}
				            <span class="help-block">{{ $errors->first('tipo_persona')}}</span>
				        </div>
			            <div class="form-group col-sm-7 col-md-3 @if ($errors->first('rfc') !== "") has-error @endif">
				            {!! Form::label('rfc', 'RFC', ['class' => 'control-label']) !!}
				            {!! Form::text('rfc', null, ['class' => 'form-control', 'placeholder' => 'RFC']) !!}
				            <span class="help-block">{{ $errors->first('rfc')}}</span>
				        </div>
			            <div class="form-group col-sm-12 col-md-5 @if ($errors->first('razon_social') !== "") has-error @endif">
				            {!! Form::label('razon_social', 'Denominación o Razón Social', ['class' => 'control-label']) !!}
				            {!! Form::text('razon_social', null, ['class' => 'form-control', 'placeholder' => 'Denominación o Razón Social']) !!}
				            <span class="help-block">{{ $errors->first('razon_social')}}</span>
				        </div>
				        <div class="form-group col-sm-12 col-md-12 @if ($errors->first('representante_legal') !== "") has-error @endif">
				            {!! Form::label('representante_legal', 'Representante Legal', ['class' => 'control-label']) !!}
				            {!! Form::text('representante_legal', null, ['class' => 'form-control', 'placeholder' => 'En caso de personas Físicas']) !!}
				            <span class="help-block">{{ $errors->first('representante_legal')}}</span>
				        </div>
		            </div>
					<h3>Dirección de Facturación</h3>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_calle') !== "") has-error @endif">
				            {!! Form::label('df_calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('df_calle', null, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('df_calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_exterior') !== "") has-error @endif">
				            {!! Form::label('df_no_exterior', 'No. Exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_exterior', null, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_exterior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_no_interior') !== "") has-error @endif">
				            {!! Form::label('df_no_interior', 'No. Interior', ['class' => 'control-label']) !!}
				            {!! Form::text('df_no_interior', null, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('df_no_interior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-9 @if ($errors->first('df_colonia') !== "") has-error @endif">
				            {!! Form::label('df_colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('df_colonia', null, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('df_colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('df_codigo_postal') !== "") has-error @endif">
				            {!! Form::label('df_codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('df_codigo_postal', null, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('df_codigo_postal')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('df_municipio') !== "") has-error @endif">
				            {!! Form::label('df_municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('df_municipio', null, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('df_municipio')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('df_estado') !== "") has-error @endif">
				            {!! Form::label('df_estado', 'Estado', ['class' => 'control-label']) !!}
				            {!! Form::text('df_estado', null, ['class' => 'form-control', 'placeholder' => 'Estado']) !!}
				            <span class="help-block">{{ $errors->first('df_estado')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
	
	@if(session('show_modal_cliente_datos_fiscales_create'))
	<script type="text/javascript">
		$('#modalClienteDatosFiscalesCreate').modal('show');
	</script>	
	@endif

@endsection