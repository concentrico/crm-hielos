<div class="modal inmodal" id="modalAgregarProducto" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-cubes modal-icon" style="font-size: 2em;"></i><BR/>Agregar Producto</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('clientes/' . $cliente->id . '/addProducto' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('producto_agregar') !== "") has-error @endif">
				            {!! Form::label('producto_agregar', 'Producto', ['class' => 'control-label']) !!}
					        {!! Form::select('producto_agregar', \App\Models\Producto::whereNotIn('id',$cliente->precios()->get()->pluck('producto_id')->toArray())->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['id' => 'producto_agregar','class' => 'col-xs-12 selectdp'] ) !!}
				            <span class="help-block">{{ $errors->first('producto_agregar')}}</span>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('precio_unitario') !== "") has-error @endif">
				            {!! Form::label('precio_unitario', 'Precio Unitario', ['class' => 'control-label']) !!}
			            	<div class="input-group" style="width: 100%;">
			            		<span class="input-group-addon">$</span>
			            		{!! Form::text('precio_unitario', null, ['class' => 'form-control decimal-2-places']) !!}	
			            	</div>
				            <span class="help-block">{{ $errors->first('precio_unitario')}}</span>
				        </div>
		            </div>
		            @if ($errors->first('addProductoFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('addProductoFailed')!!}
				        </div>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_add_producto'))
		$('#modalAgregarProducto').modal('show');
	@endif

</script>
@endsection