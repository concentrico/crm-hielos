<div class="modal inmodal" id="modalClienteEstablecimientoCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title "> Registrar Establecimiento</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('establecimientos/' . $cliente->id)] ) !!}
				<div class="modal-body">

					<div class="row">
						<div class="col-sm-12">
							<h3>Información General</h3>
				            <div class="row">
					            <div class="form-group col-sm-7  @if ($errors->first('cliente_establecimiento_sucursal') !== "") has-error @endif">
						            {!! Form::label('cliente_establecimiento_sucursal', 'Nombre Sucursal', ['class' => 'control-label']) !!}
						            {!! Form::text('cliente_establecimiento_sucursal', null, ['class' => 'form-control', 'placeholder' => 'Sucursal']) !!}
						            <span class="help-block">{{ $errors->first('cliente_establecimiento_sucursal')}}</span>
						        </div>
					            <div class="form-group col-sm-5  @if ($errors->first('cliente_establecimiento_tipos') !== "") has-error @endif">
						            {!! Form::label('cliente_establecimiento_tipos', 'Tipo(s)', ['class' => 'control-label']) !!}
					            	{!! Form::select('cliente_establecimiento_tipos[]', \App\Models\ClienteTipoEstablecimiento::all()->pluck('nombre', 'id')->toArray(), null, 
						            	['class' => 'form-control selectdp', 'data-placeholder' => 'Tipo(s)', 'multiple' => 'multiple'] ) !!}
						            <span class="help-block">{{ $errors->first('cliente_establecimiento_tipos')}}</span>
						        </div>
				            </div>
				            <div class="row">
				            	<div class="form-group col-sm-7  @if ($errors->first('cliente_establecimiento_razon_social') !== "") has-error @endif">
						            {!! Form::label('cliente_establecimiento_razon_social', 'Razón Social', ['class' => 'control-label']) !!}
						            {!! Form::select('cliente_establecimiento_razon_social', \App\Models\ClienteDatosFiscales::where('cliente_id', '=', $cliente->id)->pluck('razon_social', 'id')->prepend('','')->toArray(), null, 
						           	['class' => 'form-control selectdp', 'data-placeholder' => 'Razón Social Establecimiento'] ) !!}
						            <span class="help-block">{{ $errors->first('cliente_establecimiento_razon_social')}}</span>
						        </div>

					            <div class="form-group col-sm-5  @if ($errors->first('cliente_establecimiento_zona') !== "") has-error @endif">
						            {!! Form::label('cliente_establecimiento_zona', 'Zona', ['class' => 'control-label']) !!}
						            {!! Form::select('cliente_establecimiento_zona', \App\Models\ClienteZona::all()->pluck('nombre', 'id')->prepend('','')->toArray(), null, 
						           	['class' => 'form-control selectdp', 'data-placeholder' => 'Zona'] ) !!}
						            <span class="help-block">{{ $errors->first('cliente_establecimiento_zona')}}</span>
						        </div>
						    </div>
						</div>				
					</div>

		            <h3>Dirección de Establecimiento</h3>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('est_calle') !== "") has-error @endif">
				            {!! Form::label('est_calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('est_calle', null, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('est_calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('est_no_exterior') !== "") has-error @endif">
				            {!! Form::label('est_no_exterior', 'No. Exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('est_no_exterior', null, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('est_no_exterior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('est_no_interior') !== "") has-error @endif">
				            {!! Form::label('est_no_interior', 'No. Interior', ['class' => 'control-label']) !!}
				            {!! Form::text('est_no_interior', null, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('est_no_interior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-5 @if ($errors->first('est_colonia') !== "") has-error @endif">
				            {!! Form::label('est_colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('est_colonia', null, ['id' => 'est_colonia','class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('est_colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('est_codigo_postal') !== "") has-error @endif">
				            {!! Form::label('est_codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('est_codigo_postal', null, ['class' => 'form-control positive-integer', 'placeholder' => 'Código postal', 'data-mask' => '99999','maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('est_codigo_postal')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('est_municipio') !== "") has-error @endif">
				            {!! Form::label('est_municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('est_municipio', null, ['id'=> 'est_municipio','class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('est_municipio')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-12 @if ($errors->first('establecimiento_comentarios') !== "") has-error @endif">
				            {!! Form::label('establecimiento_comentarios', 'Comentarios', ['class' => 'control-label']) !!}
				            {!! Form::textarea('establecimiento_comentarios', null, ['id' => 'establecimiento_comentarios', 
				            'class' => 'form-control', 'placeholder' => 'Comentarios', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
				            <span class="help-block">{{ $errors->first('establecimiento_comentarios')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	$('#est_municipio').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '1', busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

    $('#est_codigo_postal').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '2', municipio: $('#est_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });
		
	$('#est_colonia').typeahead({
        source:  function (query, process) {
        return $.get('{{ url('ajax/autocomplete-codigospostales') }}', { type: '3', codigo_postal: $('#est_codigo_postal').val(), municipio: $('#est_municipio').val(), busqueda: query, _token: '{{ csrf_token() }}' }, function (data) {
            return process(data);
          });
        }
    });

    function setColonia(colonia) {

		if (colonia != '') {
			$.ajax({
	            type: 'GET',
	            url: '{{url('ajax/autocomplete-codigospostales')}}',
	            data:{ 'type': '4', 'colonia': colonia, '_token': '{{ csrf_token()}}' },
	            success: function(data) {
	            	if (data.length > 1) {

	            		html='';
	            		html2='';
					    $.each(data, function(key, value) {
				            html += "<option value=" + value['municipio']  + ">" +value['municipio'] + "</option>"
				            html2 += "<option value=" + value['codigo_postal']  + ">" +value['codigo_postal'] + "</option>"
				        });

	            		$("#est_municipio")
						    .replaceWith('<select id="est_municipio" name="est_municipio" class="form-control">'+html+'</select>');
						$("#est_municipio").dropdown();

	            		$("#est_codigo_postal")
						    .replaceWith('<select id="est_codigo_postal" name="est_codigo_postal" class="form-control">'+html2+'</select>');
						$("#est_codigo_postal").dropdown();

	            	} else {

	            		if(!$("#est_municipio").is("select")) {
						    // no es un select
						    $('#est_municipio').val(data[0]['municipio']);
	            			$('#est_codigo_postal').val(data[0]['codigo_postal']);
						} else {
							// Es un select
							$('#est_codigo_postal').destroyDropdown();
							$('#est_municipio').destroyDropdown();

							$("#est_municipio")
						    .replaceWith('<input type="text" id="est_municipio" name="est_municipio" class="form-control" value="'+data[0]['municipio']+'"/>');

							$("#est_codigo_postal")
						    .replaceWith('<input type="text" id="est_codigo_postal" name="est_codigo_postal" class="form-control positive-integer" value="'+data[0]['codigo_postal']+'" data-mask="99999" maxlength="5" />');

						}
	            		
	            	}
	            	
	            }
	        });
		} else {

			if($("#est_municipio").is("select")) {

				// Es un select
				$('#est_codigo_postal').destroyDropdown();
				$('#est_municipio').destroyDropdown();

				$("#est_municipio")
			    .replaceWith('<input type="text" id="est_municipio" name="est_municipio" class="form-control" value=""/>');

				$("#est_codigo_postal")
			    .replaceWith('<input type="text" id="est_codigo_postal" name="est_codigo_postal" class="form-control positive-integer" value="" data-mask="99999" maxlength="5" />');

			}

		}
	
	}

	$.fn.destroyDropdown = function() {
		return $(this).each(function() {
			$(this).parent().dropdown( 'destroy' ).replaceWith( $(this) );
		});
	};
	
	@if(session('show_modal_cliente_establecimiento_create'))
		$('.nav-tabs a[href="#tab-2"]').tab('show');
		$('#modalClienteEstablecimientoCreate').modal('show');
	@endif
	
</script>	
@endsection