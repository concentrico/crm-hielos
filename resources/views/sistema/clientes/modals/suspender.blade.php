<div class="modal inmodal" id="modalClienteSuspender" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					<h4 class="text-danger modal-title"><i class="fa fa-trash modal-icon"></i><BR/> Eliminar cliente</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-danger"></h2>
				            <h3 style="font-weight: 300;"></h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'DELETE',  'url' => url('clientes' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalClienteSuspender(id){
		
		if ( $('#modalClienteSuspender').find('form').attr('action') == "{{ url('clientes') }}/" + id){
			
		}else{
			$('#modalClienteSuspender').find('form').attr('action', "{{ url('clientes') }}/" + id)
			

			$('#modalClienteSuspender').find('h2').html("¿Estas seguro de que deseas desactivar a "+clientes[id]['nombre_corto']+"?");
			
			$('#modalClienteSuspender').find('h3').html("<b>Al desactivar el cliente, también se darán de baja todos sus establecimientos.</b>");
						
		}

		$('#modalClienteSuspender').modal('show');
	}

</script>
@endsection
