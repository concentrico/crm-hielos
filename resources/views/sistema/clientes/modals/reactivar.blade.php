<div class="modal inmodal" id="modalClienteReactivar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
					</button>
					
					<h4 class="text-danger modal-title"><i class="fa fa-refresh modal-icon"></i> Reactivar cliente</h4>
				</div>
				<div class="modal-body text-center">
		            <div class="row">
			            <div class="col-sm-12">
				            <h2 class="text-info">¿Estas seguro de que deseas reactivar al cliente?</h2>
				            <h3 style="font-weight: 300;"></h3>
				        </div>
		            </div>
				</div>
				{!! Form::open(['method' => 'POST',  'url' => url('clientes' )] ) !!}
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
						<button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Reactivar</button>
					</div>
	
					{!! Form::close() !!}
			</div>
		</div>
    </div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalClienteReactivar(id){
		
		if ( $('#modalClienteReactivar').find('form').attr('action') == "{{ url('clientes') }}/" + id + "/reactivar"){
			
		}else{
			$('#modalClienteReactivar').find('form').attr('action', "{{ url('clientes') }}/" + id + "/reactivar")
			
			$('#modalClienteReactivar').find('h3').html("<b>" + clientes[id]['nombre_corto'] + "</b>");

						
		}

		$('#modalClienteReactivar').modal('show');
	}

</script>
@endsection
