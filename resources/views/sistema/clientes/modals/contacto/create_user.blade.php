<style type="text/css" media="screen">
.i-checks{ display: inline-block;}	
.alert{margin-bottom: 0px;}
</style>
<div class="modal inmodal" id="modalUsarioCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<i style="font-size:30px;" class="fa fa-id-card-o"></i>
				<h4 class="modal-title text-success">Generar Usuario Portal Clientes</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('usuarios/contacto' )] ) !!}
				{!! Form::hidden('cliente_id', $cliente->id) !!}
				{!! Form::hidden('contacto_id', null,['id' => 'contacto_id']) !!}
				<div class="modal-body">
					@if(session('errores_user_create') != null)
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								{{ session('errores_user_create') }}
							</div>
						</div>
					</div>
					@endif
		            <div class="row">
		            	<div class="form-group col-sm-12" style="margin-bottom: 0px;">
				            {!! Form::label('contacto_nombre', 'Nombre:', ['class' => 'control-label', 'style' => 'display:inline-block']) !!}
		                    <p style="display: inline-block;" id="contacto_fullname"></p>
				        </div>
			            <div class="form-group col-sm-6 @if ($errors->first('username') !== "" || $errors->first('username')) has-error @endif">
				            {!! Form::label('username', 'Usuario', ['class' => 'control-label']) !!}
							<div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
		                        {!! Form::text('username',null, ['class' => 'form-control', 'data-placeholder' => 'Username'] ) !!}
							</div>
				            <span class="help-block">{{ $errors->first('username')}}</span>
				        </div>
				        <div class="form-group col-sm-6 @if ($errors->first('password') !== "" || $errors->first('password')) has-error @endif">
				            {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
							<div class="input-group" style="width: 100%;">
		                        	<span class="input-group-addon"><i class="fa fa-key"></i></span>
		                        {!! Form::text('password',null, ['class' => 'form-control', 'data-placeholder' => 'Password','autofocus' => 'autofocus'] ) !!}
							</div>
				            <span class="help-block">{{ $errors->first('password')}}</span>
				        </div>
				        <div class="form-group col-sm-12">
					        <table class="table table-hover">
			                    <thead>
				                    <tr>
										<th>&nbsp;</th>
										<th>Rol Sistema</th>
				                        <th>Descripción </th>
				                    </tr>
			                    </thead>
			                    <tbody>
									<tr>
										<td>
						            	{!! Form::checkbox('rols[]','5', null,['class' => 'i-checks'] ) !!}
						            	</td>
				                        <td>Cliente Compras</td>
				                        <td>Portal de Clientes, Pedidos y Reportes.</td>
				                    </tr>
				                    <tr>
										<td>
						            	{!! Form::checkbox('rols[]','6', null,['class' => 'i-checks'] ) !!}
						            	</td>
				                        <td>Cliente Establecimiento</td>
				                        <td>Firma y Recibe Remisión, Portal de Clientes y Pedidos.</td>
				                    </tr>
				                    <tr>
										<td>
						            	{!! Form::checkbox('rols[]','7', null,['class' => 'i-checks'] ) !!}
						            	</td>
				                        <td>Cliente Almacén</td>
				                        <td>Firma Remisión.</td>
				                    </tr>

								</tbody>
			                </table>
				        </div>
		            </div>
		            <!--
		            <div class="alert alert-warning">
                        El usuario recibirá un email para que confirme su cuenta y genere su contraseña.
                    </div>
                    -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar </button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">
	
	function crearUsuarioComercial(id){
		var first_name = contactos[id]['nombre'];
		var last_name = contactos[id]['apellido_paterno'];
		var last_name2 = contactos[id]['apellido_materno'];
		var username = first_name.toLowerCase().slice(0, 1) + last_name.toLowerCase();
		var full_name = first_name+' '+last_name+' '+last_name2;
		$('#modalUsarioCreate').find('input[name=username]').val(username);
		$('#contacto_fullname').html(full_name);
		$('#contacto_id').val(id);
		$('#modalUsarioCreate').modal('show');

	}

	@if(session('show_modal_user_create'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');

		@if(session('contacto_id'))
			var id = {{session('contacto_id')}};
			var first_name = contactos[id]['nombre'];
			var last_name = contactos[id]['apellido_paterno'];
			var last_name2 = contactos[id]['apellido_materno'];
			var username = first_name.toLowerCase().slice(0, 1) + last_name.toLowerCase();
			var full_name = first_name+' '+last_name+' '+last_name2;
			$('#modalUsarioCreate').find('input[name=username]').val(username);
			$('#contacto_fullname').html(full_name);
			$('#contacto_id').val(id);
		@endif

		$('#modalUsarioCreate').modal('show');
	@endif
	
</script>	
@endsection