@extends('sistema.layouts.master')

@section('title', 'Lista de Clientes')

@section('attr_navlink_3', 'class="active"')

@section('breadcrumb_title', 'Clientes')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Clientes</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.clientes.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.clientes.iboxes.tabla')
	</div>
</div>

@include('sistema.clientes.modals.create')
@include('sistema.clientes.modals.suspender')
@include('sistema.clientes.modals.reactivar')

@endsection

@section('assets_bottom')
<script type="text/javascript">

	$('.ver_cotizaciones').on('click', function () {
		$(this).next('ul').toggle(function () {
		    $(this).next('ul').css({display: "block"});
		}, function () {
		    $(this).next('ul').css({display: "none"});
		});
	});

	clientes = [];
	
	@foreach($clientes as $cliente)
	
		clientes[{{ $cliente->id }}] = {
			nombre_corto: '{{ $cliente->nombre_corto }}',
		}
	
	@endforeach

</script>
@endsection
