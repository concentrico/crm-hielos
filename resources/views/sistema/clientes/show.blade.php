@extends('sistema.layouts.master')

@section('title', 'Cliente')

@section('attr_navlink_3', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('fullcalendar/fullcalendar.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/c3/c3.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css">
<style type="text/css" media="screen">
 .highcharts-button {
    display:none;
}   
</style>
@endsection

@section('breadcrumb_title', $cliente->nombre_corto)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('clientes') }}">Clientes</a></li>
<li class="active"><strong>{{ $cliente->nombre_corto }}</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
                		<i style="margin:0px;" class="fa fa-address-card fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Datos del Cliente</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 5) active @endif">
                	<a data-toggle="tab" href="#tab-5">
                		<i style="margin:0px;" class="fa fa-area-chart fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Consumo</span> 
	                </a> 
                </li>
                <li class="@if(Input::get('active_tab') == 6) active @endif">
                	<a data-toggle="tab" href="#tab-6">
                		<i style="margin:0px;" class="fa fa-calendar fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Venta Semanal</span> 
	                </a> 
                </li>
                @if($cliente->tipo == 'horeca')
                <li class="@if(Input::get('active_tab') == 2) active @endif">
                	<a data-toggle="tab" href="#tab-2">
                		<i style="margin:0px;" class="fa fa-map fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Establecimientos ({{ count($cliente->establecimientos) }})<span> 
	                </a>
	            </li>
	            @endif
                <li class="@if(Input::get('active_tab') == 3) active @endif">
                	<a data-toggle="tab" href="#tab-3">
                		<i style="margin:0px;" class="fa fa-users fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Contactos ({{ count($cliente->contactos) }})<span> 
	                </a>
	            </li>
	            <li class="@if(Input::get('active_tab') == 4) active @endif">
                	<a data-toggle="tab" href="#tab-4">
                		<i style="margin:0px;" class="fa fa-file-text fa-2x"></i>&nbsp;
	                	<span class="hidden-xs">Pedidos ({{ count($cliente->pedidos) }})<span> 
	                </a>
	            </li>     
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12 col-md-7" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.datos')							
							</div>
							<div class="col-sm-12 col-md-5" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.datosf')
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-5" class="tab-pane @if(Input::get('active_tab') == 5) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.consumo')
							</div>
						</div>
                    </div>
                </div>

                <div id="tab-6" class="tab-pane @if(Input::get('active_tab') == 6) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.venta')
							</div>
						</div>
                    </div>
                </div>
                @if($cliente->tipo == 'horeca')
                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.establecimientos')
							</div>
						</div>
                    </div>
                </div>
                @endif
                <div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.contactos')
							</div>
						</div>
                    </div>
                </div>
                <div id="tab-4" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
                    <div class="panel-body" style="padding: 10px 20px 0px 20px;">
						<div class="row">
							<div class="col-sm-12" style="padding: 0 5px;">
								@include('sistema.clientes.iboxes.pedidos')
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('sistema.clientes.modals.edit')
@include('sistema.clientes.modals.fiscales.create')
@include('sistema.clientes.modals.fiscales.edit')
@include('sistema.clientes.modals.fiscales.delete')
@include('sistema.clientes.modals.contacto.create')
@include('sistema.clientes.modals.contacto.edit')
@include('sistema.clientes.modals.contacto.delete')
@if($cliente->tipo == 'horeca')
@include('sistema.clientes.modals.establecimientos.create')
@include('sistema.clientes.modals.establecimientos.delete')
@endif
@if($cliente->tipo == 'standard')
@include('sistema.clientes.modals.add_producto')
@endif
@include('sistema.clientes.modals.pedidos.create')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>	
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>	
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>   
<script src="{{ asset('fullcalendar/fullcalendar.js') }}"></script> 
<script src="{{ asset('fullcalendar/locale/es.js') }}"></script>    

<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ asset('js/plugins/c3/c3.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ asset('js/plugins/morris/morris.js') }}"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>
@if($cliente->tipo == 'horeca')
<script src="https://maps.googleapis.com/maps/api/js?key={{getSystemSettings()->google_maps_api_key}}&libraries=places&callback=initMap" async defer></script>
@endif
<script type="text/javascript">	

	@if(session('show_tab_contactos'))
		$('.nav-tabs a[href="#tab-3"]').tab('show');
	@endif

	@if(session('show_tab_establecimientos'))
		$('.nav-tabs a[href="#tab-2"]').tab('show');
	@endif
		
	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "mm/dd/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 0,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    language: 'es-MX',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "yyyy-mm-dd"
	});

	$('.clockpicker').clockpicker({
		autoclose: true,
	});

	var cliente = {
		id : {{ $cliente->id }},
	};

	// DATOS CONTACTOS
	var contactos = [];
	
	@foreach($cliente->contactos as $contacto)
	
	contactos[{{ $contacto->id }}] = {
		nombre_completo : '{{ $contacto->nombre_completo() }}',
		nombre : '{{ $contacto->nombre }}',
		apellido_paterno : '{{ $contacto->apellido_paterno }}',
		apellido_materno : '{{ $contacto->apellido_materno }}',
		
		establecimientos : [
			@foreach($contacto->establecimientos as $establecimiento)
				'{{ $establecimiento->id }}',
			@endforeach
		],

		emails : [
			@foreach($contacto->emails as $email)
				{
				id : '{{ $email->id }}',
				tipo : '{{ $email->tipo }}',
				correo : '{{ $email->valor }}'
				},
			@endforeach
		],
		telefonos : [
			@foreach($contacto->telefonos as $telefono)
				{
				id : '{{ $telefono->id }}',
				tipo : '{{ $telefono->tipo }}',
				telefono : '{{ $telefono->valor }}',
				telefono_ext : '{{ $telefono->extension }}'
				},
			@endforeach
		],
		comentarios : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $contacto->comentarios)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}'
	}

	@endforeach

	// DATOS FISCALES
	var datosf = [];
	
	@foreach($cliente->datos_fiscales as $datosf)
	
	datosf[{{ $datosf->id }}] = {
		tipo_persona : '{{ $datosf->persona }}',
		rfc : '{{ $datosf->rfc }}',
		razon_social : '{{ $datosf->razon_social }}',
		representante_legal : '{{ $datosf->representante_legal }}',
		calle : '{{ $datosf->calle }}',
		numero_exterior : '{{ $datosf->numero_exterior }}',
		numero_interior : '{{ $datosf->numero_interior }}',
		colonia : '{{ $datosf->colonia }}',
		codigo_postal : '{{ $datosf->codigo_postal }}',
		municipio : '{{ $datosf->municipio }}',
		estado : '{{ $datosf->estado }}',
	}
	
	@endforeach

	@if($cliente->tipo == 'horeca')
	// ESTABLECIMIENTOS
	var establecimientos = [];
	
	@foreach($cliente->establecimientos as $establecimiento)
	
	establecimientos[{{ $establecimiento->id }}] = {
		cliente_id : '{{ $establecimiento->cliente_id }}',
		cliente_zona_id : '{{ $establecimiento->cliente_zona_id }}',
		cliente_datos_fiscales_id : '{{ $establecimiento->cliente_datos_fiscales_id }}',
		sucursal : '{{ $establecimiento->sucursal }}',
		calle : '{{ $establecimiento->calle }}',
		numero_exterior : '{{ $establecimiento->numero_exterior }}',
		numero_interior : '{{ $establecimiento->numero_interior }}',
		colonia : '{{ $establecimiento->colonia }}',
		codigo_postal : '{{ $establecimiento->codigo_postal }}',
		municipio : '{{ $establecimiento->municipio }}',
		estado : '{{ $establecimiento->estado }}',
		latitud_longitud : '{{ $establecimiento->latitud_longitud }}',
	}
	
	@endforeach

	// UBICACION ESTABLECIMIENTOS
	var ubicaciones = [];
	var cont=0;
	@foreach($cliente->establecimientos as $establecimiento)
	
	ubicaciones[cont] = {
		establecimiento_id : '{{ $establecimiento->id }}',
		sucursal : '{{$establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$establecimiento->sucursal}}</b>',
		latitud_longitud : '{{ $establecimiento->latitud_longitud }}',
	}
	cont++;
	@endforeach

	var map;
	var infowindow;

	function initMap() {

		var pyrmont = {lat: 25.7133324, lng: -100.314264};
		var polygonCoords = [];

		map = new google.maps.Map(document.getElementById('map_sucursales'), {
		//center: pyrmont,
		zoom: 12
		});

		var geocoder = new google.maps.Geocoder();
		for (var i = 0; i < ubicaciones.length; i++) {
			if (ubicaciones[i]['latitud_longitud']) {
				var latlngStr = ubicaciones[i]['latitud_longitud'].split(',', 2);
	  			var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  			polygonCoords.push(new google.maps.LatLng(latlngStr[0], latlngStr[1]));
	  			setMarker(geocoder, map, latlng, ubicaciones[i]['sucursal']);
			}
		}

		var bounds = new google.maps.LatLngBounds();

		for (var i = 0; i < polygonCoords.length; i++) {
		  bounds.extend(polygonCoords[i]);
		}

	    var latlng_center = bounds.getCenter().toString().replace("(", "").replace(")", "");
	    var latlngStr_center = latlng_center.split(',', 2);
		map.setCenter(new google.maps.LatLng(latlngStr_center[0], latlngStr_center[1]));

		/*
		google.maps.Polygon.prototype.my_getBounds=function(){
		    var bounds = new google.maps.LatLngBounds()
		    this.getPath().forEach(function(element,index){bounds.extend(element)})
		    return bounds
		}

		// Construct the polygon.
		  var polygon = new google.maps.Polygon({
		    paths: polygonCoords,
		    strokeColor: '#FF0000',
		    strokeOpacity: 0.8,
		    strokeWeight: 2,
		    fillColor: '#FF0000',
		    fillOpacity: 0.35
		  });
		  polygon.setMap(map);
	

		var lat = polygon.my_getBounds().getCenter().lat();
		var lng = polygon.my_getBounds().getCenter().lng();

		map.setCenter(new google.maps.LatLng(lat, lng));
		*/
	}



	function setMarker(geocoder, map, latlng, sucursal) {
		
		geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[1]) {
	      	var infowindow = new google.maps.InfoWindow();
	        var marker = new google.maps.Marker({
	          position: latlng,
	          map: map,
    		  animation: google.maps.Animation.DROP,
	        });
	        infowindow.setContent(sucursal);
	        infowindow.open(map, marker);
	      }
	    }

	  });

	}

    // FIX TO RESIZE AND SHOW MAP DIV
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		if (typeof google != "undefined") {
			google.maps.event.trigger(map, "resize");
   		}
	});
	@endif
</script>
@endsection