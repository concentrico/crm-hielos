@extends('sistema.layouts.master')

@section('title', 'Par&aacutemetros')

@section('attr_navlink_12', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<style type="text/css" media="screen">
 .text-bold{
    font-weight: bold;
 }
 .btn:active:focus, .btn:focus {
    outline: none!important;
}
</style>
@endsection

@section('breadcrumb_title', 'Par&aacutemetros de Operaci&oacuten')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Inicio</a></li>
<li class="active"><strong>Parámetros</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		@include('sistema.parametros.iboxes.settings')
		@include('sistema.parametros.iboxes.modelo_conservador')
		@include('sistema.parametros.iboxes.zona')
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		@include('sistema.parametros.iboxes.parametro')
		@include('sistema.parametros.iboxes.condicionpago')
		@include('sistema.parametros.iboxes.formapago')
		@include('sistema.parametros.iboxes.tipo_establecimiento')
	</div>
</div>
@include('sistema.parametros.modals.parametro.create')

@include('sistema.parametros.modals.modelo_conservador.create')
@include('sistema.parametros.modals.modelo_conservador.edit')
@include('sistema.parametros.modals.modelo_conservador.delete')

@include('sistema.parametros.modals.forma_pago.create')
@include('sistema.parametros.modals.forma_pago.edit')
@include('sistema.parametros.modals.forma_pago.delete')

@include('sistema.parametros.modals.condicion_pago.create')
@include('sistema.parametros.modals.condicion_pago.edit')
@include('sistema.parametros.modals.condicion_pago.delete')

@include('sistema.parametros.modals.zonas.create')
@include('sistema.parametros.modals.zonas.edit')
@include('sistema.parametros.modals.zonas.delete')

@include('sistema.parametros.modals.cliente_tipo_establecimiento.create')
@include('sistema.parametros.modals.cliente_tipo_establecimiento.edit')
@include('sistema.parametros.modals.cliente_tipo_establecimiento.delete')

@endsection

@section('assets_bottom')
<script type="text/javascript">

	@if(session('update_success'))
		toastr.success("Cambios guardados satisfactoriamente.");
	@endif

	// FORMA DE PAGO
	var forma_pago = [];
	@foreach ($data['forma_pago'] as $fp)
		forma_pago[{{ $fp->id }}] = {
			nombre : '{{ $fp->nombre }}'
		}
	@endforeach
	//modelo conservador
	var modelo_conservador = [];
	@foreach ($data['modelo_conservador'] as $mc)
		modelo_conservador[{{ $mc->id }}] = {
			modelo : '{{ $mc->modelo }}',
			codigo : '{{ $mc->codigo }}',
			producto_id : '{{ $mc->producto_id }}',
			capacidad : '{{ $mc->capacidad }}'
		}
	@endforeach
		//CONDICION DE PAGO
	var condicion_pago = [];
	@foreach ($data['condicion_pago'] as $conP)
		condicion_pago[{{ $conP->id }}] = {
			nombre : '{{ $conP->nombre }}',
			dias : '{{ $conP->dias }}'
		}
	@endforeach
	//CLIENTE ZONA
	var cliente_zona = [];
	@foreach ($data['cliente_zona'] as $zon)
		cliente_zona[{{ $zon->id }}] = {
			nombre : '{{ $zon->nombre }}'
		}
	@endforeach

	//CLIENTE ESTABLECIMIENTO
	var cliente_tipo_establecimiento = [];
	@foreach ($data['cliente_tipo_establecimiento'] as $clien)
		cliente_tipo_establecimiento[{{ $clien->id }}] = {
			nombre : '{{ $clien->nombre }}'
		}
	@endforeach
	//PARAMETRTO
	var parametros = [];
	@foreach ($data['parametros'] as $par)
		parametros[{{ $par->id }}] = {
			capacidad_cuartofrio : '{{ $par->capacidad_cuartofrio }}',
			produccion_diaria : '{{ $par->produccion_diaria }}'
		}
	@endforeach

	function calcularCapacidadMaxima() {
		var total = 0.0;
        var peso;
        var cant = [];
        $(".cantidades-cf").each(
            function(index, value) {
                cant.push( $(this).val() );
            }
        );
        var pesos = [];
        $(".pesos-cf").each(
            function(index, value) {
                peso = $(this).val(); 
                pesos.push( peso );
            }
        );

        for (var i = 0; i < cant.length; i++) {
          if( cant[i] != '') {
            total += parseFloat(cant[i]) * parseFloat(pesos[i]);
          } else {
            total += 0.0;
          }
        };

        $("#total_capacidad").html('<h3>'+total+' Kg</h3>');
	}

	$(document).on('change', '.btn-file :file', function() {
	    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready( function() {
	    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
			
			if( label != ""){
				this.nextSibling.innerHTML = label;		
			}else{
				this.nextSibling.innerHTML = 'Selecciona un archivo';				
			}
			
	        console.log(numFiles);
	        console.log(label);
	    });
	});
</script>
@endsection