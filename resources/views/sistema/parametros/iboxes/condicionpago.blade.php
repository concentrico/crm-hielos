<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalCondicionCreate">
			<i class="fa fa-plus"></i> Agregar
		</a>
        <center><strong><h4>CONDICIONES DE PAGO</h4></strong></center>  
    </div>
    <div class="ibox-content">
	    <table id="" name="dataTableCondicionpagos" class="table">
			<thead>
				<tr>
					<th data-type="html" style="min-width: 5px;">Condicion de Pago</th>
					<th data-type="html" style="min-width: 5px;">Dias</th>
				</tr>
	        </thead>
			<tbody>
			@foreach($data['condicion_pago'] as $condicion_pago)
				<tr>
					<td>{{ $condicion_pago->nombre}}</td>
					<td>{{ $condicion_pago->dias}}</td>
					  <td>
					<td>
						<div class="btn-group pull-right">
						@if($condicion_pago->deleted_at != NULL)
							<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalCondicionPagoReactivar({{ $condicion_pago->id }});">
							<i class="fa fa-refresh"></i>
							</button>
						     @else
							<button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalCondicionPagoEdit({{ $condicion_pago->id }});">
							<i class="fa fa-edit"></i>
							</button>
							<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalCondicionPagoDelete({{ $condicion_pago->id }});">
							 <i class="fa fa-trash" aria-hidden="true"></i>
							</button>
						@endif
						</div>
					</td>
			 	</tr>
			@endforeach
			</tbody>
		</table>
    </div>
</div>
