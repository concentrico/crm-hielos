<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalClienteTipoEstablecimientoCreate">
			<i class="fa fa-plus"></i> Agregar
		</a>
		<center><strong><h4>TIPOS DE ESTABLECIMIENTO</h4></strong></center>
    </div>
    <div class="ibox-content">
		<table id="" name="dataTableFormapagos" class="table">
			<thead>
				<tr>
					<th data-type="html" style="min-width: 5px;">ID</th>
					<th data-type="html" style="min-width: 4px;">Establecimiento</th>
				</tr>
	        </thead>
			<tbody>
			@foreach($data['cliente_tipo_establecimiento'] as $cliente_tipo_establecimiento)
			  	<tr>
					<td>{{ $cliente_tipo_establecimiento-> id}}</td>
					<td>{{ $cliente_tipo_establecimiento-> nombre}}</td>
		   			<td>
					 	<div class="btn-group pull-right">
							@if($cliente_tipo_establecimiento->deleted_at != NULL)
							<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalClienteTipoEstablecimientoReactivar({{ $cliente_tipo_establecimiento->id }});">
							<i class="fa fa-refresh"></i>
							</button>
						     @else
							<button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalClienteTipoEstablecimientoEdit({{ $cliente_tipo_establecimiento->id }});">
							<i class="fa fa-edit"></i>
							</button>
							<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalClienteTipoEstablecimientoDelete({{ $cliente_tipo_establecimiento->id }});">
							 <i class="fa fa-trash" aria-hidden="true"></i>
							</button>
						    @endif
					    </div>
			        </td>
				</tr>
			@endforeach
			</tbody>
		</table>
    </div>
</div>