<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalZonasCreate">
			<i class="fa fa-plus"></i> Agregar
		</a>
		<center><strong><h4>Zonas de Entregas</h4></strong></center>
    </div>
    <div class="ibox-content">
			<table name="dataTableFormapagos" class="table">
				<thead>
					<tr>
						<th data-type="html" style="min-width: 5px;">ID</th>
						<th data-type="html" style="min-width: 4px;">Nombre de la Zona</th>
          			</tr>
		        </thead>
				<tbody>
				@foreach($data['cliente_zona'] as $cliente_zona)
				  	 <tr>
						<td>{{ $cliente_zona-> id}}</td>
						<td>{{ $cliente_zona-> nombre}}</td>
			   			<td>
					 		<div class="btn-group pull-right">
							@if($cliente_zona->deleted_at != NULL)
								<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalClienteZonaReactivar({{ $cliente_zona->id }});">
								<i class="fa fa-refresh"></i>
								</button>
							     @else
								<button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalClienteZonaEdit({{ $cliente_zona->id }});">
								<i class="fa fa-edit"></i>
								</button>
								<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalClienteZonaDelete({{ $cliente_zona->id }});">
								 <i class="fa fa-trash" aria-hidden="true"></i>
								</button>
							@endif
						    </div>

					    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
    </div>
</div>
