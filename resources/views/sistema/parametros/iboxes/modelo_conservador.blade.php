<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalModeloConservadorCreate">
			<i class="fa fa-plus"></i> Agregar
		</a>
		<center><strong><h4>MODELOS DE CONSERVADORES </h4></strong></center>
    </div>
    <div class="ibox-content">
   		<table name="dataTableParametros" class="table">
			<thead>
				<tr>
					<th data-type="html" style="min-width: 5px;">ID</th>
					<th data-type="html" style="min-width: 5px;">Codigo</th>
					<th data-type="html" style="min-width: 5px;">Modelo</th>
					<th data-type="html" style="min-width: 5px;">Producto</th>
					<th data-type="html" style="min-width: 5px;">Capacidad</th>
				</tr>
	        </thead>
			<tbody>
			@foreach($data['modelo_conservador'] as $modelo_conservador)
			<tr>
				<td>{{ $modelo_conservador->id}}</td>
				<td>{{ $modelo_conservador->codigo}}</td>
				<td>{{ $modelo_conservador->modelo}}</td>
				<td>{{ $modelo_conservador->producto['nombre']}}</td>
				<td>{{ $modelo_conservador->capacidad}}</td>
				<td>
					 <div class="btn-group pull-right">
						@if($modelo_conservador->deleted_at != NULL)
							<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalModeloConservadorReactivar({{ $modelo_conservador->id }});">
							<i class="fa fa-refresh"></i>
							</button>
						     @else
							<button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalModeloConservadorEdit({{ $modelo_conservador->id }});">
							<i class="fa fa-edit"></i>
							</button>
							<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalModeloConservadorDelete({{ $modelo_conservador->id }});">
							 <i class="fa fa-trash" aria-hidden="true"></i>
							</button>
						@endif
					</div>
				</td>
	        </tr>
		    @endforeach
    		</tbody>
		</table>
    </div>
</div>