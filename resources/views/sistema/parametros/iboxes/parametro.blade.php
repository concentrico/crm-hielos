<div class="ibox float-e-margins">
	<div class="ibox-title"><center><strong><h4>INVENTARIO</h4></strong></center></div>
    <div class="ibox-content">
    	<div class="row">
    	@if(count($data['parametros']) > 0)

		    {!! Form::open(['method' => 'PUT',  'url' => url('parametros/1')] ) !!}
			{!! Form::hidden('parametro_id',1) !!}

		    	<div class="form-group col-sm-12 @if ($errors->first('capacidad_cuartofrio_edit') !== "") has-error @endif">
		    		{!! Form::label('capacidad_cuartofrio_edit', 'Capacidad Inventario Interno (Kilogramos)', ['class' => 'control-label']) !!}
		    		<div class="input-group" style="width: 100%;">
			           	<span class="input-group-addon"><i class="fa fa-battery-full" aria-hidden="true"></i></span>
						{!! Form::text('capacidad_cuartofrio_edit', $data['parametros'][0]['capacidad_cuartofrio'], ['class' => 'form-control decimal-2-places', 'placeholder' => 'kilos']) !!}
					</div>
					<span class="help-block">{{ $errors->first('capacidad_cuartofrio_edit')}}</span>
				 </div>

			 	<div class="form-group col-sm-12">

				 	<div class="table-responsive">
		                <table class="table table-striped table-fixed" id="tablaProductosProduccion">
		                    <thead>
		                    <tr>
		                        <th class="col-xs-5">Producto</th>
		                        <th class="col-xs-3">Peso</th>
		                        <th class="col-xs-4">Capacidad (Bolsas)</th>
		                    </tr>
		                    </thead>
		                    <tbody id="body_tabla_productos_produccion" style="height:auto;">

						 	@if(count($data['parametros'][0]->productos) > 0)
						 		@php
						 			$total_peso=0.0;
						 		@endphp
						 		@foreach($data['parametros'][0]->productos as $capacidad_producto)
									<tr>
			                            <td class="col-xs-5">
				                            {{$capacidad_producto->producto->nombre}}
											<input type="hidden" name="parametro_producto_id[]" value="{{$capacidad_producto->id}}"/>
			                            </td>

			                            <td class="col-xs-3" >
			                            	{{$capacidad_producto->producto->peso}} Kg
			                            	<input type="hidden" class="pesos-cf" name="peso[]" value="{{$capacidad_producto->producto->peso}}"/>
										</td>

			                            <td class="col-xs-4">
			                            	<div class="input-group" style="width: 100%;">
									           	<span class="input-group-addon"><i class="fa fa-battery-full" aria-hidden="true"></i></span>
													{!! Form::text('capacidad[]', $capacidad_producto->capacidad, ['class' => 'form-control decimal-2-places cantidades-cf', 'onblur' => 'calcularCapacidadMaxima();']) !!}
											</div>
											<span class="help-block">{{ $errors->first('capacidad')}}</span>
										</td>
			                        </tr>
			                      	@php
							 			$total_peso+=floatval($capacidad_producto->producto->peso)*floatval($capacidad_producto->capacidad);
							 		@endphp
								@endforeach
								<tr>
									<td class="col-xs-8 text-right text-bold">
										<h3>Total</h3>
									</td>

									<td class="col-xs-4 text-center text-bold text-success" id="total_capacidad">
										<h3> {{$total_peso}} Kg</h3>
									</td>
								</tr>
						 	@else
								<tr>
									<td class="col-xs-12 text-center"><span class="text-danger">Es necesario primero agregar productos (tipo hielo) en el sistema.</span></td>
								</tr>
							@endif
		                    </tbody>
		                </table>
		            </div>

		            @if ($errors->first('cuartoFrioProductosFailed') )
					    <div class="alert alert-danger text-center">
				           	{!!$errors->first('cuartoFrioProductosFailed')!!}
				        </div>
					@endif

		        </div>

	            <div class="form-group col-sm-12 pull-right">
		            <button type="submit" id="btnSaveCF" class="btn btn-outline btn-primary btn-sm pull-right">
			        	<i class="fa fa-save"></i> Guardar
			        </button>
			    </div>
            {!! Form::close() !!}
        @else
        	<div class="col-sm-12">
				<a class="btn btn-primary btn-rounded btn-block" data-toggle="modal" data-target="#modalParametroCreate"><i class="fa fa-plus"></i> Registrar Parámetros</a>
		    </div>
        @endif
	    </div>
    </div>
</div>
