<div class="ibox float-e-margins">

	<div class="ibox-title"> 
		<center><strong><h4>CONFIGURACIONES DEL SITIO</h4></strong></center> 
    </div>

    <div class="ibox-content">
    	<div class="row">
		    {!! Form::open(['method' => 'POST',  'url' => url('parametros/settings'), 'enctype' => 'multipart/form-data'] ) !!}

		    	<div class="form-group col-sm-6 @if ($errors->first('site_name') !== "") has-error @endif">
					 {!! Form::label('site_name', 'Nombre del Sistema', ['class' => 'control-label']) !!}
					 {!! Form::text('site_name', $data['parametros'][0]['site_name'], ['class' => 'form-control', 'placeholder' => 'CRM Hielos']) !!}
					 <span class="help-block">{{ $errors->first('site_name')}}</span>
				 </div>

				<div class="form-group col-sm-6 @if ($errors->first('site_url') !== "") has-error @endif">
					 {!! Form::label('site_url', 'URL del Sistema', ['class' => 'control-label']) !!}
					 {!! Form::text('site_url', $data['parametros'][0]['site_url'], ['class' => 'form-control', 'placeholder' => 'http://crm-hielos.com']) !!}
					 <span class="help-block">{{ $errors->first('site_url')}}</span>
			 	</div>

			 	<div class="form-group col-sm-6 @if ($errors->first('site_email') !== "") has-error @endif">
					 {!! Form::label('site_email', 'Email del Sistema', ['class' => 'control-label']) !!}
					 {!! Form::text('site_email', $data['parametros'][0]['site_email'], ['class' => 'form-control', 'placeholder' => 'contacto@crm-hielos.com']) !!}
					 <span class="help-block">{{ $errors->first('site_email')}}</span>
				 </div>

				<div class="form-group col-sm-6 @if ($errors->first('site_phone') !== "") has-error @endif">
					 {!! Form::label('site_phone', 'Teléfono de Contacto', ['class' => 'control-label']) !!}
					 {!! Form::text('site_phone', $data['parametros'][0]['site_phone'], ['class' => 'form-control', 'placeholder' => '(81) 81 23 45 67']) !!}
					 <span class="help-block">{{ $errors->first('site_phone')}}</span>
			 	</div>

			 	<div class="form-group col-sm-6 ">
		            {!! Form::label('site_logo', 'Logo del Sistema', ['class' => 'control-label']) !!}<br class="visible-xs-block"/>
			 		@if($data['parametros'][0]['site_logo'] != null)
			 			<img src="{{url('/')}}/{{$data['parametros'][0]['site_logo']}}" height="50" />
					@else
						<br/><br/>
						<p>Aún no se configura logotipo</p>
				@endif
		        </div>

				<div class="form-group col-sm-6 @if ($errors->first('site_logo') !== "") has-error @endif">
					<br/><br/>
					<span class="btn btn-primary btn-file col-xs-12" >
						<input type="file" accept=".jpg, .jpeg, .gif, .png" name="site_logo" style="z-index: 100;"><label class="col-xs-12" style="margin-bottom: 0px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">Subir Imagen</label>
					</span>
		            <span class="help-block">{{ $errors->first('site_logo')}}</span>
		        </div>

		        <div class="form-group col-sm-6 @if ($errors->first('google_maps_api_key') !== "") has-error @endif">
					 {!! Form::label('google_maps_api_key', 'Google Maps API Key', ['class' => 'control-label']) !!}
					 {!! Form::text('google_maps_api_key', $data['parametros'][0]['google_maps_api_key'], ['class' => 'form-control', 'placeholder' => '']) !!}
					 <span class="help-block">{{ $errors->first('google_maps_api_key')}}</span>
				 </div>

				<div class="form-group col-sm-6 @if ($errors->first('google_recaptcha_api_key') !== "") has-error @endif">
					 {!! Form::label('google_recaptcha_api_key', 'Google Recaptcha API Key', ['class' => 'control-label']) !!}
					 {!! Form::text('google_recaptcha_api_key', $data['parametros'][0]['google_recaptcha_api_key'], ['class' => 'form-control', 'placeholder' => '']) !!}
					 <span class="help-block">{{ $errors->first('google_recaptcha_api_key')}}</span>
			 	</div>

	            <div class="form-group col-sm-12 pull-right">
		            <button type="submit" class="btn btn-outline btn-primary btn-sm pull-right">
			        	<i class="fa fa-save"></i> Guardar
			        </button>
			    </div>
            {!! Form::close() !!}
	    </div>
    </div>
</div>
