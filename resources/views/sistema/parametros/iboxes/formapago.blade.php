<div class="ibox float-e-margins">
    <div class="ibox-title"> 
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalFormaCreate">
			<i class="fa fa-plus"></i> Agregar
		</a>
		<center><strong><h4>FORMAS DE PAGO</h4></strong></center> 
    </div>
    <div class="ibox-content">
    	<table name="dataTableFormapagos" class="table">
			<thead>
				<tr>
					<th data-type="html" style="min-width: 5px;">ID</th>
					<th data-type="html" style="min-width: 4px;">Descripcion</th>
				</tr>
	        </thead>
			<tbody>
			@foreach($data['forma_pago'] as $forma_pago)
			  	<tr>
					<td>{{ $forma_pago-> id}}</td>
					<td>{{ $forma_pago-> nombre}}</td>
		   			<td>
						<div class="btn-group pull-right">
							@if($forma_pago->deleted_at != NULL)
								<button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalFormaPagoReactivar({{ $forma_pago->id }});">
								<i class="fa fa-refresh"></i>
								</button>
							     @else
								<button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalFormaPagoEdit({{ $forma_pago->id }});">
								<i class="fa fa-edit"></i>
								</button>
								<button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalFormaPagoDelete({{ $forma_pago->id }});">
								 <i class="fa fa-trash" aria-hidden="true"></i>
								</button>
							@endif
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
    </div>
</div>