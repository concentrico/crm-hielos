<div class="modal inmodal" id="modalParametroCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-snowflake-o modal-icon"></i><BR/> Producción e Inventario </h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('parametros'), 'enctype' => 'multipart/form-data',  'novalidate' => ''] ) !!}
				<div class="modal-body">
				    <div class="row">
				    	<div class="form-group col-sm-6 @if ($errors->first('capacidad_cuartofrio') !== "") has-error @endif">
							 {!! Form::label('capacidad_cuartofrio', 'Capacidad del Cuarto Frio', ['class' => 'control-label']) !!}
							 {!! Form::text('capacidad_cuartofrio', null, ['class' => 'form-control decimal-2-places', 'placeholder' => 'kilos']) !!}
							 <span class="help-block">{{ $errors->first('capacidad_cuartofrio')}}</span>
						 </div>

						<div class="form-group col-sm-6 @if ($errors->first('produccion_diaria') !== "") has-error @endif">
							 {!! Form::label('produccion_diaria', 'Produccion Diaria', ['class' => 'control-label']) !!}
							 {!! Form::text('produccion_diaria', null, ['class' => 'form-control positive-integer', 'placeholder' => 'kg/hora']) !!}
							 <span class="help-block">{{ $errors->first('produccion_diaria')}}</span>
					 	</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_parametro_create'))
		$('#modalParametroCreate').modal('show');
	@endif
	
</script>	
@endsection