<div class="modal inmodal" id="modalClienteZonaEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-map modal-icon"></i><BR/>Editar Zona </h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('parametros/zona')] ) !!}
			{!! Form::hidden('cliente_zona_id') !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('nombre_zona_edit') !== "") has-error @endif">
				            {!! Form::label('nombre_zona_edit', 'Nombre', ['class' => 'control-label']) !!}
				            	 <div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-map-pin" aria-hidden="true"></i></span>
				            {!! Form::text('nombre_zona_edit', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('nombre_zona_edit')}}</span>
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cliente_zona_edit'))
		$('#modalClienteZonaEdit').modal('show');
		showModalClienteZonaEdit({{ Input::old('cliente_zona_id') }} );
	@endif

	function showModalClienteZonaEdit(id){


		if ( $('#modalClienteZonaEdit').find('form').attr('action') == "{{ url('parametros/zona') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.

		}else{
			$('#modalClienteZonaEdit').find('form').attr('action', "{{ url('parametros/zona') }}/" + id )
			$('#modalClienteZonaEdit input[name="nombre_zona_edit"]').val(cliente_zona[id]['nombre']);


		}

		$('#modalClienteZonaEdit').modal('show');
	}

</script>
@endsection
