<div class="modal inmodal" id="modalModeloConservadorCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-snowflake-o modal-icon"></i><BR/>Registrar Modelo Conservador</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('parametros/modelo')] ) !!}
			<div class="modal-body">
	            <div class="row">
						<div class="form-group col-sm-6 @if ($errors->first('codigo') !== "") has-error @endif">
							{!! Form::label('codigo', 'Codigo del Conservador', ['class' => 'control-label']) !!}
							 <div class="input-group" style="width: 100%;">
							<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
							 {!! Form::text('codigo', null, ['class' => 'form-control', 'placeholder' => 'Codigo del Conservador', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
							 </div>
							 <span class="help-block">{{ $errors->first('codigo')}}</span>
					    </div>
						<div class="form-group col-sm-6 @if ($errors->first('modelo') !== "") has-error @endif">
	                        {!! Form::label('modelo', 'Modelo del Conservador', ['class' => 'control-label']) !!}
	                         <div class="input-group" style="width: 100%;">
							<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
	                         {!! Form::text('modelo', null, ['class' => 'form-control', 'placeholder' => 'Modelo del Conservador', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
	                         </div>
	                         <span class="help-block">{{ $errors->first('modelo')}}</span>
	                    </div>
	                    <div class="form-group col-sm-6 @if ($errors->first('codigo') !== "") has-error @endif">
				            {!! Form::label('producto', 'Producto', ['class' => 'control-label']) !!}
				            <div class="input-group" style="width: 100%;">
							<span class="input-group-addon"><i class="fa fa-cubes"></i></span>
				 	              {!! Form::select('producto', \App\Models\Producto::where('es_hielo', '=', '1')->pluck('nombre', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp'] ) !!}
				 	              </div>
					        <span class="help-block">{{ $errors->first('codigo')}}</span>
	       				</div>
					    <div class="form-group col-sm-6 @if ($errors->first('modelo') !== "") has-error @endif">
						     {!! Form::label('capacidad', 'Capacidad en Bolsas', ['class' => 'control-label']) !!}
						      <div class="input-group" style="width: 100%;">
							<span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span>
						     {!! Form::text('capacidad', null, ['class' => 'form-control positive-integer', 'placeholder' => 'Ejemplo 30 bolsas', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						     </div>
						     <span class="help-block">{{ $errors->first('capacidad')}}</span>
				        </div>
					</div>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_modelo_conservador_create'))
		$('#modalModeloConservadorCreate').modal('show');
	@endif

</script>

@endsection
