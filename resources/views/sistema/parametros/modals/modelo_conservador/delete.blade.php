<div class="modal inmodal" id="modalModeloConservadorDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar el Modelo del Conservador</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            ¿Estas seguro de eliminar el Modelo del Conservador<br />

				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'DELETE',  'url' => url('parametros/modelo' )] ) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalModeloConservadorDelete(id){

		if ( $('#modalModeloConservadorDelete').find('form').attr('action') == "{{ url('parametros/modelo') }}/" + id){

		}else{
			$('#modalModeloConservadorDelete').find('form').attr('action', "{{ url('parametros/modelo') }}/" + id)

			$('#modalModeloConservadorDelete').find('h5').html(modelo_conservador[id]['codigo']);

		}

		$('#modalModeloConservadorDelete').modal('show');
	}

</script>
