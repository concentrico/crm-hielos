<div class="modal inmodal" id="modalModeloConservadorEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-snowflake-o modal-icon"></i><BR/>Editar Modelo Conservador</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('parametros/modelo')] ) !!}
			{!! Form::hidden('parametro_id') !!}
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-sm-6 @if ($errors->first('codigo_edit') !== "") has-error @endif">
						 {!! Form::label('codigo_edit', 'Codigo del Conservador', ['class' => 'control-label']) !!}
						 <div class="input-group" style="width: 100%;">
						<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						 {!! Form::text('codigo_edit', null, ['class' => 'form-control', 'placeholder' => 'Codigo del Conservador', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						 </div>
						 <span class="help-block">{{ $errors->first('codigo_edit')}}</span>
				    </div>
					<div class="form-group col-sm-6 @if ($errors->first('modelo_edit') !== "") has-error @endif">
                         {!! Form::label('modelo_edit', 'Modelo del Conservador', ['class' => 'control-label']) !!}
                         <div class="input-group" style="width: 100%;">
						<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                         {!! Form::text('modelo_edit', null, ['class' => 'form-control', 'placeholder' => 'Modelo del Conservador', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
                         </div>
                         <span class="help-block">{{ $errors->first('modelo_edit')}}</span>
                     </div>
                    <div class="form-group col-sm-6 @if ($errors->first('producto_edit') !== "") has-error @endif">
			            {!! Form::label('producto_edit', 'Producto', ['class' => 'control-label']) !!}
			            <div class="input-group" style="width: 100%;">
						<span class="input-group-addon"><i class="fa fa-cubes"></i></span>
			 	              {!! Form::select('producto_edit', \App\Models\Producto::where('es_hielo', '=', '1')->pluck('nombre', 'id')->toArray(), null, ['class' => 'form-control selectdp'] ) !!}
			 	              </div>
				        <span class="help-block">{{ $errors->first('producto_edit')}}</span>
       				</div>
				    <div class="form-group col-sm-6 @if ($errors->first('capacidad_edit') !== "") has-error @endif">
					     {!! Form::label('capacidad_edit', 'Capacidad en Bolsas', ['class' => 'control-label']) !!}
					      <div class="input-group" style="width: 100%;">
						<span class="input-group-addon"><i class="fa fa-battery-three-quarters"></i></span>
					     {!! Form::text('capacidad_edit', null, ['class' => 'form-control positive-integer', 'placeholder' => 'Ejemplo 30 bolsas', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
					     </div>
					     <span class="help-block">{{ $errors->first('capacidad_edit')}}</span>
			        </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
				<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_modelo_conservador_edit'))
		$('#modalModeloConservadorEdit').modal('show');
		showModalModeloConservadorEdit({{ Input::old('modelo_conservador_id') }} );
	@endif

	function showModalModeloConservadorEdit(id){ 
		if ( $('#modalModeloConservadorEdit').find('form').attr('action') == "{{ url('parametros/modelo') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.

		}else{
			$('#modalModeloConservadorEdit').find('form').attr('action', "{{ url('parametros/modelo') }}/" + id )

			$('#modalModeloConservadorEdit input[name="codigo_edit"]').val(modelo_conservador[id]['codigo']);
			$('#modalModeloConservadorEdit input[name="modelo_edit"]').val(modelo_conservador[id]['modelo']);

			$('#modalModeloConservadorEdit select[name="producto_edit"]').dropdown("set selected", modelo_conservador[id]['producto_id']);
			$('#modalModeloConservadorEdit input[name="capacidad_edit"]').val(modelo_conservador[id]['capacidad']);
		}

		$('#modalModeloConservadorEdit').modal('show');
	}

</script>
@endsection
