<div class="modal inmodal" id="modalCondicionPagoDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar Condicion de Pago</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            ¿Estas seguro de eliminar la condicion de Pago?<br />
				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'DELETE',  'url' => url('parametros/condicion' )] ) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalCondicionPagoDelete(id){
		
		if ( $('#modalCondicionPagoDelete').find('form').attr('action') == "{{ url('parametros/condicion') }}/" + id){
			
		}else{
			$('#modalCondicionPagoDelete').find('form').attr('action', "{{ url('parametros/condicion') }}/" + id)
			
			$('#modalCondicionPagoDelete').find('h5').html(condicion_pago[id]['nombre']);
		}

		$('#modalCondicionPagoDelete').modal('show');
	}

</script>
