<div class="modal inmodal" id="modalCondicionPagoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-handshake-o modal-icon"></i><BR/> Editar Condición de Pago</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('parametros/condicion')] ) !!}
			{!! Form::hidden('condicion_pago_id') !!}
				<div class="modal-body">
				    <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('nombre_condicion_edit') !== "") has-error @endif">
				            {!! Form::label('nombre_condicion_edit', 'Condicion', ['class' => 'control-label']) !!}
				             <div class="input-group" style="width: 100%;">
						    <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
				            {!! Form::text('nombre_condicion_edit', null, ['class' => 'form-control', 'placeholder' => 'Condicion de Pago', 'style' => 'max-width:100%;']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('nombre_condicion_edit')}}</span>
				        </div>

				         <div class="form-group col-sm-6 @if ($errors->first('dias_condicion_edit') !== "") has-error @endif">
				            {!! Form::label('dias_condicion_edit', 'Dias', ['class' => 'control-label']) !!}
				             <div class="input-group" style="width: 100%;">
						    <span class="input-group-addon"><i class="fa fa-sun-o" aria-hidden="true"></i></span>
				            {!! Form::text('dias_condicion_edit', null, ['class' => 'form-control positive-integer', 'placeholder' => 'dias', 'style' => 'max-width:100%;']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('dias_condicion_edit')}}</span>
				        </div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom') 
	@parent
<script type="text/javascript">	
        
	@if(session('show_modal_condicion_pago_edit'))
		$('#modalCondicionPagoEdit').modal('show');
		showModalCondicionPagoEdit({{ Input::old('condicion_pago_id') }} );
	@endif

	function showModalCondicionPagoEdit(id){

		
		if ( $('#modalCondicionPagoEdit').find('form').attr('action') == "{{ url('parametros/condicion') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.
		
		}else{
			$('#modalCondicionPagoEdit').find('form').attr('action', "{{ url('parametros/condicion') }}/" + id )

			$('#modalCondicionPagoEdit input[name="nombre_condicion_edit"]').val(condicion_pago[id]['nombre']);

			$('#modalCondicionPagoEdit input[name="dias_condicion_edit"]').val(condicion_pago[id]['dias']);


		}

		$('#modalCondicionPagoEdit').modal('show');
	}

</script>
@endsection

