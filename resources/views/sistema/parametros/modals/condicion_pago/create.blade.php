<div class="modal inmodal" id="modalCondicionCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-handshake-o modal-icon"></i><BR/> Registrar Condición de Pago </h4>
			</div>
			{!! Form::open(['method' => 'POST', 'url' => url('parametros/condicion'), 'enctype' => 'multipart/form-data',  'novalidate' => ''] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
				            <div class="row">
					            <div class="form-group col-sm-6 @if ($errors->first('nombre_condicion') !== "") has-error @endif">
						            {!! Form::label('nombre_condicion', 'Condición *', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
						            {!! Form::text('nombre_condicion', null, ['class' => 'form-control', 'placeholder' => 'Condición de Pago', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('nombre_condicion')}}</span>
						        </div>

						         <div class="form-group col-sm-6 @if ($errors->first('dias_condicion') !== "") has-error @endif">
						            {!! Form::label('dias_condicion', 'Dias *', ['class' => 'control-label']) !!}
						             <div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-sun-o" aria-hidden="true"></i></span>
						            {!! Form::text('dias_condicion', null, ['class' => 'form-control', 'placeholder' => 'dias', 'rows' => 2, 'style' => 'max-width:100%;']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('dias_condicion')}}</span>
						        </div>
				            </div>
						</div>				
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_condicion_pago_create'))
		$('#modalCondicionCreate').modal('show');
	@endif
	
</script>	
@endsection