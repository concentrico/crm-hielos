<div class="modal inmodal" id="modalFormaPagoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-money modal-icon"></i><BR/> Editar Forma Pago  </h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('parametros/formapago')] ) !!}
			{!! Form::hidden('forma_pago_id') !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('nombre_formapago_edit') !== "") has-error @endif">
				            {!! Form::label('nombre_formapago_edit', 'Nombre', ['class' => 'control-label']) !!}
				            	<div class="input-group" style="width: 100%;">
								    <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
				            {!! Form::text('nombre_formapago_edit', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('nombre_formapago_edit')}}</span>
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_forma_pago_edit'))
		$('#modalFormaPagoEdit').modal('show');
		showModalFormaPagoEdit({{ Input::old('forma_pago_id') }} );
	@endif

	function showModalFormaPagoEdit(id){
		if ( $('#modalFormaPagoEdit').find('form').attr('action') == "{{ url('parametros/formapago') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.
		}else{
			$('#modalFormaPagoEdit').find('form').attr('action', "{{ url('parametros/formapago') }}/" + id )
			$('#modalFormaPagoEdit input[name="nombre_formapago_edit"]').val(forma_pago[id]['nombre']);
		}

		$('#modalFormaPagoEdit').modal('show');
	}
</script>
@endsection
