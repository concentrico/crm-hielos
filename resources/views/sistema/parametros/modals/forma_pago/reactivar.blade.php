<div class="modal inmodal" id="modalCondicionReactivar" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title">Reactivar Condición de Pago</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            ¿Estas seguro de que deseas reactivar esta condición?<br />
				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('condicion' )] ) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">
						<i class="fa fa-times"></i> Cancelar
					</button>
					<button type="submit" class="btn btn-success">
						<i class="fa fa-refresh"></i> Reactivar
					</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalCondicionReactivar(id){
		
		if ( $('#modalCondicionReactivar').find('form').attr('action') == "{{ url('condiciones') }}/" + id + '/reactivar'){
			
		}else{
			$('#modalCondicionReactivar').find('form').attr('action', "{{ url('condiciones') }}/" + id + '/reactivar')
			
			$('#modalCondicionReactivar').find('h5').html(condiciones[id]['nombre']);
		}

		$('#modalCondicionReactivar').modal('show');
	}

</script>
