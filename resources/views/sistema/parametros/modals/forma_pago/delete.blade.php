<div class="modal inmodal" id="modalFormaPagoDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar esta Forma de Pago</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            ¿Estas seguro de eliminar la forma de pago?<br />
				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'DELETE',  'url' => url('parametros/formapago' )] ) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
				<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalFormaPagoDelete(id){
		if ( $('#modalFormaPagoDelete').find('form').attr('action') == "{{ url('parametros/formapago') }}/" + id){

		}else{
			$('#modalFormaPagoDelete').find('form').attr('action', "{{ url('parametros/formapago') }}/" + id)

			$('#modalFormaPagoDelete').find('h5').html(forma_pago[id]['nombre']);
		}
		$('#modalFormaPagoDelete').modal('show');
	}
</script>
