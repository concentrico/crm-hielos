<div class="modal inmodal" id="modalClienteTipoEstablecimientoDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-danger modal-title">Eliminar Tipo de Establecimiento</h4>
				<h5 class="modal-title"></h5>
		            <div class="row">
			            <div class="col-sm-12">
				            ¿Estas seguro de eliminar el Tipo de Establecimiento<br />
				        </div>
		            </div>
			</div>
			{!! Form::open(['method' => 'DELETE',  'url' => url('parametros/tipoestab' )] ) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">

	function showModalClienteTipoEstablecimientoDelete(id){

		if ( $('#modalClienteTipoEstablecimientoDelete').find('form').attr('action') == "{{ url('parametros/tipoestab') }}/" + id){

		}else{
			$('#modalClienteTipoEstablecimientoDelete').find('form').attr('action', "{{ url('parametros/tipoestab') }}/" + id)

			$('#modalClienteTipoEstablecimientoDelete').find('h5').html(cliente_tipo_establecimiento[id]['nombre']);
		}

		$('#modalClienteTipoEstablecimientoDelete').modal('show');
	}

</script>
