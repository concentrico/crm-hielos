<div class="modal inmodal" id="modalClienteTipoEstablecimientoCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-map-pin modal-icon"></i><BR/> Nuevo Tipo de Establecimiento</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('parametros/tipoestab')] ) !!}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
				            <div class="row">
					            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('nombre_tipoest') !== "") has-error @endif">
						            {!! Form::label('nombre_tipoest', 'Nombre *', ['class' => 'control-label']) !!}
						            <div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
						            {!! Form::text('nombre_tipoest', null, ['class' => 'form-control', 'placeholder' => 'Establecimiento', 'style' => 'max-width:100%;']) !!}
						            </div>
						            <span class="help-block">{{ $errors->first('nombre_tipoest')}}</span>
						        </div>
						     </div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cliente_tipoest_create'))
		$('#modalClienteTipoEstablecimientoCreate').modal('show');
	@endif

</script>
@endsection
