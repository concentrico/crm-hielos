<div class="modal inmodal" id="modalClienteTipoEstablecimientoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-map-pin modal-icon"></i> <BR/>Editar Tipo de Establecimiento</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('parametros/tipoestab')] ) !!}
			{!! Form::hidden('cliente_tipo_establecimiento_id') !!}
				<div class="modal-body">


		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3 @if ($errors->first('nombre_tipoest_edit') !== "") has-error @endif">
				            {!! Form::label('nombre_tipoest_edit', 'Nombre', ['class' => 'control-label']) !!}
				            	<div class="input-group" style="width: 100%;">
								<span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
				            {!! Form::text('nombre_tipoest_edit', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('nombre_tipoest_edit')}}</span>
				        </div>
		            </div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	@if(session('show_modal_cliente_tipoest_edit'))
	$('#modalClienteTipoEstablecimientoEdit').modal('show');
		showModalClienteTipoEstablecimientoEdit({{ Input::old('cliente_tipo_establecimiento_id') }} );
	@endif

	function showModalClienteTipoEstablecimientoEdit(id){


		if ( $('#modalClienteTipoEstablecimientoEdit').find('form').attr('action') == "{{ url('parametros/tipoestab') }}/" + id ){
			// Dejar en blanco, es cuando se recarga la pantalla.

		}else{
			$('#modalClienteTipoEstablecimientoEdit').find('form').attr('action', "{{ url('parametros/tipoestab') }}/" + id )
			$('#modalClienteTipoEstablecimientoEdit input[name="nombre_tipoest_edit"]').val(cliente_tipo_establecimiento[id]['nombre']);
		}

		$('#modalClienteTipoEstablecimientoEdit').modal('show');
	}

</script>
@endsection
