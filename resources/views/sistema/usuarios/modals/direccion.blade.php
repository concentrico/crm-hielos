<div class="modal inmodal" id="modalClienteDireccion" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<i class="fa fa-map-marker modal-icon"></i>
				<h4 class="modal-title">Editar Dirección</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('clientes/' . $cliente->id . '/direccion' )] ) !!}
				<div class="modal-body">


		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('calle') !== "") has-error @endif">
				            {!! Form::label('calle', 'Calle', ['class' => 'control-label']) !!}
				            {!! Form::text('calle', $cliente->calle, ['class' => 'form-control', 'placeholder' => 'Calle']) !!}
				            <span class="help-block">{{ $errors->first('calle')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('no_interior') !== "") has-error @endif">
				            {!! Form::label('no_interior', 'No. interior', ['class' => 'control-label']) !!}
				            {!! Form::text('no_interior', $cliente->no_interior, ['class' => 'form-control', 'placeholder' => 'Número interior']) !!}
				            <span class="help-block">{{ $errors->first('no_interior')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('no_exterior') !== "") has-error @endif">
				            {!! Form::label('no_exterior', 'No. exterior', ['class' => 'control-label']) !!}
				            {!! Form::text('no_exterior', $cliente->no_exterior, ['class' => 'form-control', 'placeholder' => 'Número exterior']) !!}
				            <span class="help-block">{{ $errors->first('no_exterior')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-9 @if ($errors->first('colonia') !== "") has-error @endif">
				            {!! Form::label('colonia', 'Colonia', ['class' => 'control-label']) !!}
				            {!! Form::text('colonia', $cliente->colonia, ['class' => 'form-control', 'placeholder' => 'Colonia']) !!}
				            <span class="help-block">{{ $errors->first('colonia')}}</span>
				        </div>
			            <div class="form-group col-sm-3 @if ($errors->first('codigo_postal') !== "") has-error @endif">
				            {!! Form::label('codigo_postal', 'Código postal', ['class' => 'control-label']) !!}
				            {!! Form::text('codigo_postal', $cliente->codigo_postal, ['class' => 'form-control', 'placeholder' => 'Código postal', 'maxlength' => '5']) !!}
				            <span class="help-block">{{ $errors->first('codigo_postal')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('municipio') !== "") has-error @endif">
				            {!! Form::label('municipio', 'Municipio', ['class' => 'control-label']) !!}
				            {!! Form::text('municipio', $cliente->municipio, ['class' => 'form-control', 'placeholder' => 'Municipio']) !!}
				            <span class="help-block">{{ $errors->first('municipio')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('estado') !== "") has-error @endif">
				            {!! Form::label('estado', 'Estado', ['class' => 'control-label']) !!}
				            {!! Form::text('estado', $cliente->estado, ['class' => 'form-control', 'placeholder' => 'Estado']) !!}
				            <span class="help-block">{{ $errors->first('estado')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('pais') !== "") has-error @endif">
				            {!! Form::label('pais', 'País', ['class' => 'control-label']) !!}
				            {!! Form::text('pais', $cliente->pais, ['class' => 'form-control', 'placeholder' => 'País']) !!}
				            <span class="help-block">{{ $errors->first('pais')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar cambios</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>