<div class="modal inmodal" id="modalClienteFiscales" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<i class="fa fa-gavel modal-icon"></i>
				<h4 class="modal-title">Editar Datos Fiscales</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('clientes/' . $cliente->id . '/fiscales' )] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('calle') !== "") has-error @endif">
				            {!! Form::label('rfc', 'R.F.C.', ['class' => 'control-label']) !!}
				            {!! Form::text('rfc', $cliente->rfc, ['class' => 'form-control', 'placeholder' => 'R.F.C.']) !!}
				            <span class="help-block">{{ $errors->first('rfc')}}</span>
				        </div>
			            <div class="form-group col-sm-8 @if ($errors->first('razon_social') !== "") has-error @endif">
							@if($cliente->tipo == 'moral')
					            {!! Form::label('razon_social', 'Razón social', ['class' => 'control-label']) !!}
					            {!! Form::text('razon_social', $cliente->razon_social, ['class' => 'form-control', 'placeholder' => 'Razón social']) !!}
							@else
					            {!! Form::label('razon_social', 'Nombre completo', ['class' => 'control-label']) !!}
					            {!! Form::text('razon_social', $cliente->razon_social, ['class' => 'form-control', 'placeholder' => 'Nombre completo']) !!}
							@endif
				            <span class="help-block">{{ $errors->first('razon_social')}}</span>
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar cambios</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>