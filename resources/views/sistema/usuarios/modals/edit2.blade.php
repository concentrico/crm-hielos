<div class="modal inmodal" id="modalUsuarioEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-edit modal-icon"></i> Editar Usuario</h4>
			</div>
			{!! Form::open(['method' => 'PUT',  'url' => url('usuarios/' . $usuario->id . '/update2')] ) !!}
				{!! Form::hidden('usuario_edit_id') !!}
				<div class="modal-body">

		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('rol_edit') !== "") has-error @endif">
				            {!! Form::label('rol_edit', 'Rol', ['class' => 'control-label']) !!}
				            {!! Form::select('rol_edit', \App\Models\UserRole::all()->pluck('nombre', 'id')->toArray(), $usuario->roles->pluck('id', 'nombre')->toArray(), ['class' => 'col-xs-12 selectdp', 'placeholder' => 'Rol'] ) !!}
				            <span class="help-block">{{ $errors->first('rol_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('nombre_edit') !== "") has-error @endif">
				            {!! Form::label('nombre_edit', 'Nombre', ['class' => 'control-label']) !!}
				            {!! Form::text('nombre_edit', $usuario->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            <span class="help-block">{{ $errors->first('nombre_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('username_edit') !== "") has-error @endif">
				            {!! Form::label('username_edit', 'Usuario', ['class' => 'control-label']) !!}
				            {!! Form::text('username_edit', $usuario->username, ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
				            <span class="help-block">{{ $errors->first('username_edit')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('email_edit') !== "") has-error @endif">
				            {!! Form::label('email_edit', 'Correo Electrónico', ['class' => 'control-label']) !!}
				            {!! Form::text('email_edit', $usuario->email, ['class' => 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
				            <span class="help-block">{{ $errors->first('email_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-6 @if ($errors->first('email_edit_confirmation') !== "") has-error @endif">
				            {!! Form::label('email_edit_confirmation', 'Confirmar Correo', ['class' => 'control-label']) !!}
				            {!! Form::text('email_edit_confirmation', $usuario->email, ['class' => 'form-control', 'placeholder' => 'Confirmar Correo Electrónico']) !!}
				            <span class="help-block">{{ $errors->first('email_edit_confirmation')}}</span>
				        </div>
		            </div>

		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('password_edit') !== "") has-error @endif">
				            {!! Form::label('password_edit', 'Contraseña', ['class' => 'control-label']) !!}
				            {!! Form::text('password_edit', null, ['class' => 'form-control', 'placeholder' => 'Contraseña']) !!}
				            <span class="help-block">{{ $errors->first('password_edit')}}</span>
				        </div>

				        <div class="form-group col-sm-6 @if ($errors->first('password_edit_confirmation') !== "") has-error @endif">
				            {!! Form::label('password_edit_confirmation', 'Confirmar Contraseña', ['class' => 'control-label']) !!}
				            {!! Form::text('password_edit_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Confirmar Contraseña']) !!}
				            <span class="help-block">{{ $errors->first('password_edit_confirmation')}}</span>
				        </div>
			        </div>

		            <div class="row">

		            	<div class="form-group col-sm-4 @if ($errors->first('fecha_nacimiento_edit') !== "") has-error @endif">
				            {!! Form::label('fecha_nacimiento_edit', 'Fecha Nacimiento', ['class' => 'control-label']) !!}
				            <div class="input-group date">
	                        	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								{!! Form::text('fecha_nacimiento_edit', $usuario->fecha_nacimiento, ['id' => 'fecha_nacimiento_edit', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
	                        </div>
				            <span class="help-block">{{ $errors->first('fecha_nacimiento_edit')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('telefono_edit') !== "") has-error @endif">
				            {!! Form::label('telefono_edit', 'Teléfono', ['class' => 'control-label']) !!}
				            <div class="input-group m-b">
				            	<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
				          		{!! Form::text('telefono_edit', $usuario->telefono, ['id'=> 'telefono_edit','class' => 'form-control', 'placeholder' => '(81) 1234-5678', 'data-mask' => '(99) 9999-9999']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('telefono_edit')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('celular_edit') !== "") has-error @endif">
				            {!! Form::label('celular_edit', 'Celular', ['class' => 'control-label']) !!}
				            <div class="input-group m-b">
				            	<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
				          		{!! Form::text('celular_edit', $usuario->celular, ['id'=> 'celular_edit','class' => 'form-control', 'placeholder' => '(812) 345-6789', 'data-mask' => '(999) 999-9999']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('celular_edit')}}</span>
				        </div>
		            </div>

				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script role="text/javascript">	

	@if(session('show_modal_usuario_edit') && !session('chofer') )
		$('#modalUsuarioEdit').find('#rol_edit').select2().val([{!! "'" . implode("','", $usuario->roles->pluck('id')->toArray()) . "'" !!}]).trigger("change");
		$('#modalUsuarioEdit').modal('show');
	@elseif(session('show_modal_usuario_edit') && session('chofer') )
		$('#modalUsuarioEdit').find('#rol_edit').select2().val([{!! "'" . implode("','", $usuario->roles->pluck('id')->toArray()) . "'" !!}]).trigger("change");
		$('#fecha_nacimiento_edit').removeAttr('disabled');
		$('#telefono_edit').removeAttr('disabled');
		$('#celular_edit').removeAttr('disabled');
		$('#modalUsuarioEdit').modal('show');
	@endif

</script>
@endsection