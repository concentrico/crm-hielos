<div class="modal inmodal" id="modalUsuarioCreate" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="text-success modal-title"><i class="fa fa-user-plus modal-icon"></i> Registrar Usuario</h4>
			</div>
			{!! Form::open(['method' => 'POST',  'url' => url('usuarios')] ) !!}
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-4 @if ($errors->first('rol') !== "") has-error @endif">
				            {!! Form::label('rol', 'Rol', ['class' => 'control-label']) !!}
			            	{!! Form::select('rol', \App\Models\UserRole::where('id', '<', 5)->get()->pluck('nombre', 'id')->toArray(), null, ['class' => 'form-control selectdp'] ) !!}
				            <span class="help-block">{{ $errors->first('rol')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('nombre') !== "") has-error @endif">
				            {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
				            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
				            <span class="help-block">{{ $errors->first('nombre')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('username') !== "") has-error @endif">
				            {!! Form::label('username', 'Usuario', ['class' => 'control-label']) !!}
				            {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
				            <span class="help-block">{{ $errors->first('username')}}</span>
				        </div>
		            </div>
		            <div class="row">
			            <div class="form-group col-sm-6 @if ($errors->first('email') !== "") has-error @endif">
				            {!! Form::label('email', 'Correo Electrónico', ['class' => 'control-label']) !!}
				            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
				            <span class="help-block">{{ $errors->first('email')}}</span>
				        </div>
				        <div class="form-group col-sm-6 @if ($errors->first('email_confirmation') !== "") has-error @endif">
				            {!! Form::label('email_confirmation', 'Confirmar Correo', ['class' => 'control-label']) !!}
				            {!! Form::text('email_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Confirmar Correo Electrónico']) !!}
				            <span class="help-block">{{ $errors->first('email_confirmation')}}</span>
				        </div>
		            </div>
		            <div class="row">

		            	<div class="form-group col-sm-4 @if ($errors->first('fecha_nacimiento') !== "") has-error @endif">
				            {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento', ['class' => 'control-label']) !!}
				            <div class="input-group date">
	                        	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								{!! Form::text('fecha_nacimiento', null, ['id' => 'fecha_nacimiento', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
	                        </div>
				            <span class="help-block">{{ $errors->first('fecha_nacimiento')}}</span>
				        </div>
				        <div class="form-group col-sm-4 @if ($errors->first('telefono') !== "") has-error @endif">
				            {!! Form::label('telefono', 'Teléfono', ['class' => 'control-label']) !!}
				            <div class="input-group m-b">
				            	<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
				          		{!! Form::text('telefono', null, ['id'=> 'telefono','class' => 'form-control', 'placeholder' => '(81) 1234-5678', 'data-mask' => '(99) 9999-9999']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('telefono')}}</span>
				        </div>
			            <div class="form-group col-sm-4 @if ($errors->first('celular') !== "") has-error @endif">
				            {!! Form::label('celular', 'Celular', ['class' => 'control-label']) !!}
				            <div class="input-group m-b">
				            	<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
				          		{!! Form::text('celular', null, ['id'=> 'celular','class' => 'form-control', 'placeholder' => '(812) 345-6789', 'data-mask' => '(999) 999-9999']) !!}
				            </div>
				            <span class="help-block">{{ $errors->first('celular')}}</span>
				        </div>
		            </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>

				{!! Form::close() !!}
		</div>
	</div>
</div>
@section('assets_bottom')
	@parent
<script>
	        
	@if(session('show_modal_usuario_create') && !session('chofer') )
		$('#modalUsuarioCreate').modal('show');
	@elseif(session('show_modal_usuario_create') && session('chofer') )
		$('#fecha_nacimiento').removeAttr('disabled');
		$('#telefono').removeAttr('disabled');
		$('#celular').removeAttr('disabled');
		$('#modalUsuarioCreate').modal('show');
	@endif

</script>
@endsection