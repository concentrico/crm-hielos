<style type="text/css" media="screen">
pre{margin:3px;}
</style>
<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-history"></i> Últimas Acciones </h5>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;max-height: 400px; overflow-y: scroll;">

        <ul class="sortable-list connectList agile-list ui-sortable" id="latest-actions">

        @if(count($usuario->logs) > 0)

                @foreach($usuario->logs as $log)
                @php 
                    switch ($log->tipo) {
                        case 'pedido':
                            $type_log='info-element';
                            break;

                        case 'produccion':
                            $type_log='danger-element';
                            break;
                        
                        case 'entregas':
                            $type_log='success-element';
                            break;
                    }
                @endphp

                <li class="{{$type_log}} ui-sortable-handle" id="task{{$log->id}}">
                {!!$log->comentarios!!}
                    <div class="agile-detail">
                        <i class="fa fa-clock-o"></i> {{$log->created_at}} 
                    </div>
                </li>

                @endforeach
            @else
                <div class="alert alert-warning text-center">
                    No existen logs registrados!
                </div>
            @endif

        </ul>
	    
    </div>
</div>
