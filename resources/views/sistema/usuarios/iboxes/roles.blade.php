<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
		    <div class="ibox-title">
		        <h5><i class="fa fa-list"></i> Roles de Usuario </h5>
		    </div>
		    <div class="ibox-content">
                <table class="table table-hover" >
                    <thead>
	                    <tr>
							<th data-role="html" data-breakpoints="xlg">ID</th>
							<th data-role="html">Nombre</th>
	                        <th> Descripción </th>
	                    </tr>
                    </thead>
                    <tbody>
	                    @foreach ($user_roles as $user_role)
		                    <tr>
		                        <td>{{ $user_role->id }}</td>
		                        <td>{{ $user_role->nombre }}</td>
		                        <td>{{ $user_role->descripcion }}</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
		    </div>
		</div>
	</div>
</div>
