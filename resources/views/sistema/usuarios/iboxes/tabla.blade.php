<div class="ibox float-e-margins">
    <div class="ibox-title">
		<a class="text-success pull-right" data-toggle="modal" data-target="#modalUsuarioCreate">
			<i class="fa fa-plus"></i> Usuario
		</a>
        <h5><i class="fa fa-list"></i> Lista de usuarios</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $usuarios->appends(Input::all())->appends(['active_tab' => '1'])->render() !!}
			</div>
	    </div>
        <div>
			<table class="table table-hover table-res" data-cascade="true">
				<thead>
					<tr>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('ID', 'ID') !!}</th>
						<th data-type="html">{!! getHeaderLink('NOMBRE', 'Nombre') !!}</th>
						<th data-type="html">{!! getHeaderLink('USERNAME', 'Usuario') !!}</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('EMAIL',  'Correo Electrónico') !!}</th>
						<th data-type="html" data-breakpoints="xs">{!! getHeaderLink('ROL',  'Rol') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('CREATEDAT', 'Creado') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('CREATEDBY', 'Creado por') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('UPDATEDAT', 'Actualizado') !!}</th>
						<th data-type="html" data-breakpoints="xlg">{!! getHeaderLink('UPDATEDBY', 'Actualizado por') !!}</th>
						<th data-type="html" data-breakpoints="xs"></th>
					</tr>
				</thead>
				<tbody>
                    @foreach ($usuarios as $usuario)
	                    <tr class="@if($usuario->deleted_at != NULL) warning @endif">
	                        <td>{{ $usuario->id }}</td>
	                        <td>{!! $usuario->liga() !!}</td>
	                        <td>{!! $usuario->username !!}</td>
	                        <td>{{ $usuario->email }}</td>
	                        <td>{{ implode(', ', $usuario->roles->pluck('nombre')->toArray()) }}</td>
	                        <td>{{ $usuario->created_at }}</td>
	                        <td>{!! $usuario->creado_por->liga() !!}</td>
	                        <td>{{ $usuario->updated_at }}</td>
	                        <td>{!! $usuario->actualizado_por->liga() !!}</td>
	                        <td>
		                        <div class="btn-group pull-right">
		                        @if($usuario->deleted_at != NULL)
			                        <button class="btn btn-sm btn-primary" style="width: 45px;"  onclick="showModalUsuarioReactivar({{ $usuario->id }});">
			                        	<i class="fa fa-refresh"></i>
			                        </button>
		                        @else
			                        <button class="btn btn-sm btn-primary" style="width: 45px;" onclick="showModalUsuarioEdit({{ $usuario->id }});">
			                        	<i class="fa fa-edit"></i>
			                        </button>
			                        <button class="btn btn-sm btn-danger" style="width: 45px;" onclick="showModalUsuarioDelete({{ $usuario->id }});">
			                        	<i class="fa fa-user-times"></i>
			                        </button>
		                        @endif
		                        </div>
	                        </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>            	        
        </div>
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $usuarios->appends(Input::all())->appends(['active_tab' => '1'])->render() !!}
			</div>
	    </div>
    </div>
</div>