<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-user-circle-o"></i> Mi Perfil </h5>
    </div>
    
    <div class="ibox-content">

    	<div class="row">
		    <div class="col-sm-12">
				<h4 class="text-success">Rol Sistema</h4>
			    {{ implode(', ', $usuario->roles->pluck('nombre')->toArray()) }}
		    </div>
	    </div>
	    <BR/>

	    {!! Form::open(['method' => 'PUT',  'url' => url('usuarios/perfil')] ) !!}

	    <div class="row">
		     <div class="form-group col-sm-6 @if ($errors->first('nombre') !== "") has-error @endif">
	            {!! Form::label('nombre', 'Nombre', ['class' => 'control-label']) !!}
	            {!! Form::text('nombre', $usuario->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
	            <span class="help-block">{{ $errors->first('nombre')}}</span>
	        </div>
	        <div class="form-group col-sm-6 @if ($errors->first('username') !== "") has-error @endif">
	            {!! Form::label('username', 'Usuario', ['class' => 'control-label']) !!}
	            {!! Form::text('username', $usuario->username , ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
	            <span class="help-block">{{ $errors->first('username')}}</span>
	        </div>
	    </div>

	    <div class="row">
		     <div class="form-group col-sm-6 @if ($errors->first('email') !== "") has-error @endif">
	            {!! Form::label('email', 'Correo Electrónico', ['class' => 'control-label']) !!}
	            {!! Form::text('email', $usuario->email, ['class' => 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
	            <span class="help-block">{{ $errors->first('email')}}</span>
	        </div>
	        <div class="form-group col-sm-6 @if ($errors->first('fecha_nacimiento') !== "") has-error @endif">
	            {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento', ['class' => 'control-label']) !!}
	            <div class="input-group date">
                	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					{!! Form::text('fecha_nacimiento', $usuario->fecha_nacimiento, ['id' => 'fecha_nacimiento', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy','data-mask' => '99/99/9999']) !!}
                </div>
	            <span class="help-block">{{ $errors->first('fecha_nacimiento')}}</span>
	        </div>
	    </div>
	    <div class="row">
	    	<div class="form-group col-sm-6 @if ($errors->first('telefono') !== "") has-error @endif">
	            {!! Form::label('telefono', 'Teléfono', ['class' => 'control-label']) !!}
	            <div class="input-group m-b">
	            	<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
	          		{!! Form::text('telefono', $usuario->telefono, ['id'=> 'telefono','class' => 'form-control', 'placeholder' => '(81) 1234-5678', 'data-mask' => '(99) 9999-9999']) !!}
	            </div>
	            <span class="help-block">{{ $errors->first('telefono')}}</span>
	        </div>
            <div class="form-group col-sm-6 @if ($errors->first('celular') !== "") has-error @endif">
	            {!! Form::label('celular', 'Celular', ['class' => 'control-label']) !!}
	            <div class="input-group m-b">
	            	<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
	          		{!! Form::text('celular', $usuario->celular, ['id'=> 'celular','class' => 'form-control', 'placeholder' => '(812) 345-6789', 'data-mask' => '(999) 999-9999']) !!}
	            </div>
	            <span class="help-block">{{ $errors->first('celular')}}</span>
	        </div>
	    </div>
	    <div class="row text-right">
	    	<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar Cambios</button>
	    </div>

	    {!! Form::close() !!}
		
	    <div class="row">
	    <hr />
		    <div class="col-xs-6">
				<small class="text-muted">Creado: <br />{{ $usuario->created_at }}</small>
		    </div>
		    <div class="col-xs-6 text-right">
				<small class="text-muted">Última actualización: <br /> {{ $usuario->updated_at }} </small>
		    </div>
	    </div>
    </div>
</div>