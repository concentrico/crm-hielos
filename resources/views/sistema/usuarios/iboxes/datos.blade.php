<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-briefcase"></i> Datos Usuario </h5>
		<a class="text-success pull-right" onclick="showModalUsuarioEdit();">
			<i class="fa fa-edit"></i> Editar
		</a>
    </div>
    
    <div class="ibox-content" style="min-height: 400px;">
	    <div class="row">
		    <div class="col-sm-12">
			    <h3 class="text-info">{{ $usuario->nombre }}</h3>
		    </div>
	    </div>
	    <div class="row">
		    <div class="col-sm-6">
				<h4 class="text-success">Rol</h4>
			    {{ implode(', ', $usuario->roles->pluck('nombre')->toArray()) }}
		    </div>
		    <div class="col-sm-6">
				<h4 class="text-success">Email</h4>
				@if(isset($usuario->email)) 
			    	{{ $usuario->email }}
			    @else
			    	No Definido
			    @endif
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
		    <div class="col-sm-6">
				<h4 class="text-success">Usuario</h4>
			    {{ $usuario->username }}
		    </div>
		    <div class="col-sm-6">
				<h4 class="text-success">Fecha Nacimiento</h4>
			    @if(isset($usuario->fecha_nacimiento)) 
			    	{{ $usuario->fecha_nacimiento }}
			    @else
			    	No Aplica
			    @endif
		    </div>
	    </div>
	    <BR/>
	    <div class="row">
		    <div class="col-sm-6">
				<h4 class="text-success">Teléfono</h4>
				@if(isset($usuario->telefono)) 
			    	{{ $usuario->telefono }}
			    @else
			    	No Aplica
			    @endif
		    </div>
		    <div class="col-sm-6">
				<h4 class="text-success">Celular</h4>
				@if(isset($usuario->celular)) 
			    	{{ $usuario->celular }}
			    @else
			    	No Aplica
			    @endif
		    </div>
	    </div>
		<hr />
	    <div class="row">
		    <div class="col-sm-6">
				<small class="text-muted">Creado:  {{ $usuario->created_at }} <br /> 
				<a href="{{ url('usuarios/' . $usuario->created_by) }}">{{ $usuario->creado_por->nombre }}</a></small><br />
		    </div>
		    <div class="col-sm-6">
				<small class="text-muted">Actualizado: {{ $usuario->updated_at }} <br /> 
				<a href="{{ url('usuarios/' . $usuario->updated_by) }}">{{ $usuario->actualizado_por->nombre }}</a></small><br />
		    </div>

		    <div class="col-sm-12">
		    <BR/><BR/>

		    	@if($usuario->deleted_at != NULL)
                    <a class="btn btn-danger btn-rounded btn-block" onclick="showModalUsuarioReactivar2();">
                    	<i class="fa fa-refresh"></i>
                    </a>
                @else
                	<a class="btn btn-danger btn-rounded btn-block" onclick="showModalUsuarioDelete2();"><i class="fa fa-user-times"></i> Desactivar Usuario</a>
                @endif


		    	
		    </div>
	    </div>
    </div>
</div>

@include('sistema.usuarios.modals.delete')
@include('sistema.usuarios.modals.reactivar')

<script type="text/javascript">

	function showModalUsuarioEdit() {
		@if( !in_array("3", $usuario->roles()->pluck('id')->toArray()) )
			$('#modalUsuarioEdit').modal('show');
		@elseif( in_array("3", $usuario->roles()->pluck('id')->toArray()) )
			$('#fecha_nacimiento_edit').removeAttr('disabled');
			$('#telefono_edit').removeAttr('disabled');
			$('#celular_edit').removeAttr('disabled');
			$('#modalUsuarioEdit').modal('show');
		@endif
	}

	function showModalUsuarioDelete2(){
		
		if ( $('#modalUsuarioDelete').find('form').attr('action') == "{{ url('usuarios') }}/{{ $usuario->id }}"){
		}else{
			$('#modalUsuarioDelete').find('form').attr('action', "{{ url('usuarios') }}/{{ $usuario->id }}")
			$('#modalUsuarioDelete').find('h3').html("<b>{{ $usuario->nombre }}</b>.");	
		}
		$('#modalUsuarioDelete').modal('show');
	}

	function showModalUsuarioReactivar2(){
		
		if ( $('#modalUsuarioReactivar').find('form').attr('action') == "{{ url('usuarios') }}/{{ $usuario->id}}/reactivar"){
		}else{
			$('#modalUsuarioReactivar').find('form').attr('action', "{{ url('usuarios') }}/{{ $usuario->id }}/reactivar")
			$('#modalUsuarioReactivar').find('h3').html("<b>{{ $usuario->nombre }}</b>.");	
		}
		$('#modalUsuarioReactivar').modal('show');
	}

</script>