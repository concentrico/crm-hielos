@extends('sistema.layouts.master')

@section('title', $usuario->nombre)

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', 'Datos Personales')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong> Mi Perfil</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12 col-md-8 col-sm-offset-2">
		@include('sistema.usuarios.iboxes.perfil')						
	</div>
</div>


@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">	

	@if(session('perfil_update_success'))
		swal('Correcto', "Se han guardado los cambios satisfactoriamente.", "success");
	@endif

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 2,
	    todayHighlight: true,
	    language: 'es-MX',
	    endDate: '-16y',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});
</script>
@endsection