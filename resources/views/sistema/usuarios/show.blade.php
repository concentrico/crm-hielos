@extends('sistema.layouts.master')

@section('title', $usuario->nombre)

@section('attr_navlink_2', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $usuario->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('usuarios') }}">Usuarios</a></li>
<li class="active"><strong>{{ $usuario->nombre }}</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
		@include('sistema.usuarios.iboxes.datos')						
	</div>
	<div class="col-sm-12 col-md-6" style="padding: 0 5px;">
		@include('sistema.usuarios.iboxes.acciones')
	</div>
</div>

@include('sistema.usuarios.modals.edit2')

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">	

	@if(session('usuario_edit_success'))
		swal('Correcto', "Se ha actualizado al usuario satisfactoriamente.", "success");
	@endif

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 2,
	    todayHighlight: true,
	    endDate: '-16y',
	    language: 'es-MX',
	    todayBtn: "linked",
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});

	function checkIfChofer(obj) {
		var opts = [],opt;
		for (var i = 0; i < obj.options.length; i++) {
			opt = obj.options[i];
			if (opt.selected) {
			  opts.push(opt.value);
			}
		}

		if(jQuery.inArray("3",opts) != -1){
			// Hay un chofer
			$('#fecha_nacimiento').removeAttr('disabled');
			$('#telefono').removeAttr('disabled');
			$('#celular').removeAttr('disabled');
		} else {
			// No hay un chofer
			$('#fecha_nacimiento').attr('disabled','disabled');
			$('#telefono').attr('disabled','disabled');
			$('#celular').attr('disabled','disabled');
		}
	}
    function checkIfChoferEdit(obj) {
		var opts = [],opt;
		for (var i = 0; i < obj.options.length; i++) {
			opt = obj.options[i];
			if (opt.selected) {
			  opts.push(opt.value);
			}
		}

		if(jQuery.inArray("3",opts) != -1){
			// Hay un chofer
			$('#fecha_nacimiento_edit').removeAttr('disabled');
			$('#telefono_edit').removeAttr('disabled');
			$('#celular_edit').removeAttr('disabled');
		} else {
			// No hay un chofer
			$('#fecha_nacimiento_edit').attr('disabled','disabled');
			$('#telefono_edit').attr('disabled','disabled');
			$('#celular_edit').attr('disabled','disabled');
		}
	} 
</script>
@endsection