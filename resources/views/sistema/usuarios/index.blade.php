@extends('sistema.layouts.master')

@section('title', 'Lista usuarios')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('attr_navlink_2', 'class="active"')

@section('breadcrumb_title', 'Usuarios')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong>Usuarios</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
	            
                <li class="@if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                	<a data-toggle="tab" href="#tab-1">
	                	<i style="margin:0px;" class="fa fa-list"></i>&nbsp;
	                	<span class="hidden-xs">Usuarios </span> 
	                </a> 
                </li>
                
                <li class="@if(Input::get('active_tab') == 2) active @endif">
                	<a data-toggle="tab" href="#tab-2">
	                	<i style="margin:0px;" class="fa fa-gears"></i>&nbsp;
	                	<span class="hidden-xs">Roles de Usuario<span> 
	                </a>
	            </li>
	            
           </ul>
           
            <div class="tab-content">
	            
                <div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                    <div class="panel-body">
                    <div class="row" >
							<div class="col-sm-12">
								@include('sistema.usuarios.iboxes.filtros')
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								@include('sistema.usuarios.iboxes.tabla')
							</div>
						</div>
                    </div>
                </div>
                
                
                <div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                    <div class="panel-body">
						@include('sistema.usuarios.iboxes.roles')
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@include('sistema.usuarios.modals.create')
@include('sistema.usuarios.modals.edit')
@include('sistema.usuarios.modals.delete')
@include('sistema.usuarios.modals.reactivar')

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

<script type="text/javascript">	

	$.fn.datepicker.dates['es-MX'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
	    today: "Hoy",
	    clear: "Borrar",
	    format: "dd/mm/yyyy",
	    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
	    weekStart: 0
	};
		
	$('.input-group.date').datepicker({
	    startView: 2,
	    todayHighlight: true,
	    language: 'es-MX',
	    todayBtn: "linked",
	    endDate: '-16y',
	    keyboardNavigation: false,
	    forceParse: false,
	    autoclose: true,
	    format: "dd/mm/yyyy"
	});
	
	var usuarios = [];
	
	@foreach($usuarios as $usuario)
	
		usuarios[{{ $usuario->id }}] = {
			roles : [{!! "'" . implode("','", $usuario->roles->pluck('id')->toArray()) . "'" !!}],
			rol : {{$usuario->roles->pluck('id')->toArray()[0]}},
			nombre : '{!!  htmlentities(str_replace("\n","'\\n", str_replace("\r","'+ ", $usuario->nombre)), ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}',
			username : '{!! $usuario->username !!}',
			email : '{!! $usuario->email  !!}',
			fecha_nacimiento : '{!! $usuario->fecha_nacimiento  !!}',
			telefono : '{!! $usuario->telefono  !!}',
			celular : '{!! $usuario->celular  !!}',
		}
	
	@endforeach
</script>
@endsection