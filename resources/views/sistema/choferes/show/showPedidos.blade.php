@extends('sistema.layouts.master')

@section('title', 'Choferes')

@section('attr_navlink_2', 'class="active"')

@section('assets_top_top') 
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $pedido->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('choferes') }}">Entregas</a></li>
<li class="active"><strong> Pedido #{{ $pedido->id}}</strong></li>
<li></li>
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
	   <div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
	        <div class="panel-body" style="padding: 10px 20px 0px 20px;">
				<div class="row">
					<div class="col-sm-12" style="padding: 0 5px;">
						@include('sistema.choferes.iboxes.pedidos.datos') 
					</div>
				</div>
	        </div>
	    </div>
    </div>
</div>
{{--
@include('sistema.choferes.modals.corte')--}}
@include('sistema.choferes.modals.edit_entrega')
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

<script type="text/javascript">	
	@if(session('corte_success'))
		swal('Correcto', "Se ha realizado el corte satisfactoriamente.", "success");
	@endif

	@if (count($errors) > 0)
	    swal("Error!", "Completa todos los campos para terminar.", "error");
	    setTimeout(function(){
	    	$('#link-tab1').click();
	    	$('#link-tab2').click();
	    }, 500);
	    
	@endif

	function initMap() {

      @if( ($pedido->cliente_establecimiento_id != null && $pedido->establecimiento->latitud_longitud != null) || $pedido->evento_latitud_longitud != null) 
      	var input = document.getElementById('latitud_longitud').value;
	  	var latlngStr = input.split(',', 2);
	  	var myLatLng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  @endif

      var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 18,
	    center: myLatLng
	  });

	  var geocoder = new google.maps.Geocoder();

	  var infowindow = new google.maps.InfoWindow({
	      disableAutoPan: true
	   });

	  @if( ($pedido->cliente_establecimiento_id != null && $pedido->establecimiento->latitud_longitud != null) || $pedido->evento_latitud_longitud != null) 
	  	geocodeLatLng(geocoder, map, infowindow);
	  @endif

	  
	  $('#direccion2').keypress(function (e) {
		 var key = e.which;
		 if(key == 13) { // the enter key code
		    geocodeAddress(geocoder, map, infowindow);
		  }
	  }); 

	}

	function geocodeLatLng(geocoder, map, infowindow) {

	  var input = document.getElementById('latitud_longitud').value;
	  var latlngStr = input.split(',', 2);
	  var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  $('#btnGuardarUbicacion').removeAttr('disabled');

	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[1]) {

	        map.setZoom(16);

	        var marker = new google.maps.Marker({
	          position: latlng,
	          map: map,
	          draggable: false,
    		  animation: google.maps.Animation.DROP,
	        });
	        @if($pedido->cliente_establecimiento_id != null)
	        	infowindow.setContent('{{$pedido->establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$pedido->establecimiento->sucursal}}</b>');
	        @else
	        	infowindow.setContent('{{$pedido->cliente->nombre_corto}}');
	        @endif
	        infowindow.open(map, marker);

	      } else {
	        sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	      }
	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

  	function geocodeAddress(geocoder, resultsMap, infowindow) {
	  var address = document.getElementById('direccion2').value;
	  geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {

	      resultsMap.setCenter(results[0].geometry.location);

	      var lat = results[0].geometry.location.lat();
	      var lng = results[0].geometry.location.lng();
	      $('#latitud_longitud').val(lat+', '+lng);
	      $('#btnGuardarUbicacion').removeAttr('disabled');

	      if ( !marker == '' ) {
		    //Destroy marker and create new
		  	marker.setMap(null);
		  } 

		  marker = new google.maps.Marker({
	        map: resultsMap,
	        draggable: true,
    		animation: google.maps.Animation.DROP,
	        position: results[0].geometry.location
	      });
	      
        	@if($pedido->cliente_establecimiento_id != null)
	        	infowindow.setContent('{{$pedido->establecimiento->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$pedido->establecimiento->sucursal}}</b>');
	        @else
	        	infowindow.setContent('{{$pedido->cliente->nombre_corto}}');
	        @endif

        	infowindow.open(map, marker);

	      google.maps.event.addListener(marker, "drag", function (mEvent) { 
	      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
	      	$('#latitud_longitud').val(latlng);
	      });


	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

	// FIX TO RESIZE AND SHOW MAP DIV 
	$("#modalUbicacion").on("shown.bs.modal", function () {
	    google.maps.event.trigger(map, "resize");
	});

	var productos_pedido = [];

	@foreach($pedido->productos as $producto)
		
		productos_pedido[{{ $producto->id }}] = {
			producto_id : '{{ $producto->producto_id }}',
			producto_nombre : '{{ $producto->producto->nombre }}',
			precio : '{{ $producto->precio_unitario }}',
			cantidad_requerida : '{{ $producto->cantidad_requerida }}',
			cantidad_recibida : '{{ $producto->cantidad_recibida }}',
			iva : '{{ $producto->producto->iva }}',
			subtotal : '{{ $producto->subtotal }}',
		}

	@endforeach

	function editarProductoPedidoEntrega(){
		var producto_id = $('#producto_id_editar').val();
		$('#cantidad_recibida_html_p'+producto_id).html($('#cantidad_recibida_edit').val());
		$('#cantidad_recibida_p'+producto_id).val($('#cantidad_recibida_edit').val());
		$('#modalProductoPedidoEntregadoEdit').modal('hide');
	}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnpcSlQtYhijBFQlvqxPF2UfYcogTcljs&callback=initMap"
        async defer></script>
@endsection