@extends('sistema.layouts.master')

@section('title', 'Choferes')

@section('attr_navlink_2', 'class="active"')

@section('assets_top_top') 
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $pedido->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('choferes') }}">Entregados</a></li>
<li class="active"><strong>Pedido #{{ $pedido->id}}</strong></li>
<li></li>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-9 col-sm-offset-1">
	 	@include('sistema.choferes.iboxes.entregados.datos')   
	</div>
</div>
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
@endsection
