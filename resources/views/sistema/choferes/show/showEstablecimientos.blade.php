@extends('sistema.layouts.master')

@section('title', 'Choferes')

@section('attr_navlink_2', 'class="active"')

@section('assets_top_top') 
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $pedido->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('choferes') }}">Establecimientos</a></li>
<li class="active"><strong>{{$pedido->cliente->nombre_corto}} @if($pedido->cliente_establecimiento_id != null) - <strong>{{$pedido->establecimiento->sucursal}}</strong> @endif</li>
<li></li>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12 col-md-12" style="padding: 0 5px;">
		 @include('sistema.choferes.iboxes.establecimientos.datos') 
	</div>
</div>
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

<script type="text/javascript">

   var marker = '';

    function initMap() {

      @if($pedido->latitud_longitud != null) 

      	var input = document.getElementById('latitud_longitud').value;
	  	var latlngStr = input.split(',', 2);
	  	var myLatLng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  @else
	  	var myLatLng = {lat: 25.6510566, lng: -100.4025978};
	  @endif

      var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 18,
	    center: myLatLng
	  });

	  var geocoder = new google.maps.Geocoder();

	  var infowindow = new google.maps.InfoWindow({
	      disableAutoPan: true
	   });

	  @if($pedido->latitud_longitud != null) 
	  	geocodeLatLng(geocoder, map, infowindow);
	  @endif

	  document.getElementById('btnBuscarDireccion').addEventListener('click', function() {
	    geocodeAddress(geocoder, map, infowindow);
	  });
	  
	  $('#direccion2').keypress(function (e) {
		 var key = e.which;
		 if(key == 13) { // the enter key code
		    geocodeAddress(geocoder, map, infowindow);
		  }
	  }); 

	}

	function geocodeLatLng(geocoder, map, infowindow) {

	  var input = document.getElementById('latitud_longitud').value;
	  var latlngStr = input.split(',', 2);
	  var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
	  $('#btnGuardarUbicacion').removeAttr('disabled');

	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[1]) {

	        map.setZoom(16);

	        var marker = new google.maps.Marker({
	          position: latlng,
	          map: map,
	          draggable: false,
    		  animation: google.maps.Animation.DROP,
	        });
	        infowindow.setContent('{{$pedido->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$pedido->sucursal}}</b>');
	        infowindow.open(map, marker);

	        google.maps.event.addListener(marker, "drag", function (mEvent) { 
		      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
		      	$('#latitud_longitud').val(latlng);
		    });

	      } else {
	        sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	      }
	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

  	function geocodeAddress(geocoder, resultsMap, infowindow) {
	  var address = document.getElementById('direccion2').value;
	  geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {

	      resultsMap.setCenter(results[0].geometry.location);

	      var lat = results[0].geometry.location.lat();
	      var lng = results[0].geometry.location.lng();
	      $('#latitud_longitud').val(lat+', '+lng);
	      $('#btnGuardarUbicacion').removeAttr('disabled');

	      if ( !marker == '' ) {
		    //Destroy marker and create new
		  	marker.setMap(null);
		  } 

		  marker = new google.maps.Marker({
	        map: resultsMap,
	        draggable: false,
    		animation: google.maps.Animation.DROP,
	        position: results[0].geometry.location
	      });

        	infowindow.setContent('{{$pedido->cliente->nombre_corto}}'+'<BR/>'+'<b>{{$pedido->sucursal}}</b>');
        	infowindow.open(map, marker);

	      google.maps.event.addListener(marker, "drag", function (mEvent) { 
	      	var latlng = mEvent.latLng.toString().replace("(", "").replace(")", "");
	      	$('#latitud_longitud').val(latlng);
	      });


	    } else {
	      sweetAlert("¡Ups!", "No se ha encontrado la ubicación. Intente nuevamente.", "error");
	    }
	  });
	}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{getSystemSettings()->google_maps_api_key}}&callback=initMap"
        async defer></script>
@endsection