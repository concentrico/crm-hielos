<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>
<style type="text/css" media="screen">
 div.container-productos > div:nth-of-type(odd) {
    background: #e0e0e0;
} 
  .sweet-alert.sk-loading {
    position: fixed!important;
  }

  .sweet-alert h2 {
    margin: 0!important;
  }

</style>
{!! Form::open(['method' => 'PUT', 'id' => 'formEntrega', 'onSubmit' => 'return false', 'url' => url('choferes/entregas/'. $pedido->id )] ) !!}
{!! Form::hidden('pedido_id', $pedido->id) !!}

<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margin" id="panel_generales">
            <div class="ibox-title">
                <h5>Datos de Entrega</h5>
                <div class="ibox-tools">
                    <a class="collapse-link" id="link-tab1">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

              <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left">
                  <p class="text-success">Cliente</p>
                    <h3>
                        {{$pedido->cliente->nombre_corto}}
                    </h3>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                    <p class="text-success">Establecimiento</p>
                    <h3>
                      @if($pedido->cliente_establecimiento_id != null)
                        {{$pedido->establecimiento->sucursal}}
                      @else
                        N.D.
                      @endif
                    </h3>
                  </div>

                </div>
                <div class="row">

                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left">
                    <p class="text-success">Dirección</p>
                    @if($pedido->evento_calle != null || $pedido->cliente_establecimiento_id == null)
                      @php
                          $evento_obj = (object) [
                              'calle' => $pedido->evento_calle,
                              'numero_exterior' => $pedido->evento_numero_exterior,
                              'numero_interior' => $pedido->evento_numero_interior,
                              'colonia' => $pedido->evento_colonia,
                              'codigo_postal' => $pedido->evento_codigo_postal,
                              'municipio' => $pedido->evento_municipio,
                              'estado' => $pedido->evento_estado,
                              'pais' => 'México',
                          ];
                      @endphp
                      {!! imprime_direccion($evento_obj) !!}
                    @else
                      {!! imprime_direccion($pedido->establecimiento) !!}
                    @endif
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                    <p class="text-success">Indicaciones</p>
                    @if($pedido->evento_direccion_indicaciones != null)
                        <span class="text-primary">{!! nl2br(e($pedido->evento_direccion_indicaciones)) !!}</span>
                    @else
                      <span class="text-primary">N.D.</span>
                    @endif
                  </div>

                </div>
                <BR/>

                <div class="row">

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">

                      @if($pedido->evento_latitud_longitud != null || $pedido->cliente_establecimiento_id == null)
                        <input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$pedido->evento_latitud_longitud}}" />

                      @else
                        <input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$pedido->establecimiento->latitud_longitud}}" />
                      @endif

                      @if( ($pedido->cliente_establecimiento_id != null && $pedido->establecimiento->latitud_longitud != null) || $pedido->evento_latitud_longitud != null) 
                        <div id="map" style="height: 300px;"></div>
                      @endif
                  </div>


                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" style="padding-right: 2px;">
                      <a style="width:100%;" class="btn btn-danger pull-right" target="_blank" onclick="irDestino();"> 
                        <i class="fa fa-map-pin"></i> Ir al Destino
                      </a>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" style="padding-left: 2px;">
                      <a style="width:100%;" class="btn btn-primary pull-right" target="_blank" onclick="collapsePanel();"> 
                        <i class="fa fa-truck"></i> Entregar
                      </a>
                  </div>
              </div>

            </div>
        </div>
    </div>
</div>

<div class="ibox float-e-margin collapsed" id="panel_detalle">
  <div class="ibox-title">
    <h5>Entrega del Pedido</h5>
    <div class="ibox-tools">
        <a class="collapse-link" id="link-tab2">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
  </div>
  <div class="ibox-content" style="min-height: 400px;">

    <div class="ibox-content" style="padding: 0px;">
        <div class="row">
            <div class="col-xs-4">
                <h3 class="text-success">Producto</h3>
            </div>

            <div class="col-xs-4">
                <h3 class="text-success">Solicitado</h3>
            </div>
            <div class="col-xs-4">
                <h3 class="text-success">Entregado</h3>
            </div>
        </div>
    </div>

    @foreach($pedido->productos as $pedido_producto) 

            <div class="ibox-content" style="padding: 0px;">
                  <div class="row">
                      <div class="col-xs-4">
                          <input type="hidden" name="id_pedido_producto[]" value="{{$pedido_producto->id}}">
                          @if($pedido_producto->precio_unitario == 0.00)
                            <h4>{{ $pedido_producto->producto->nombre }} <BR/> <span class="text-info">Cortesía </span></h4>
                          @else
                            <h3>{{ $pedido_producto->producto->nombre }}</h3>
                          @endif
                      </div>

                      <div class="col-xs-4">
                          <h3>{{ $pedido_producto->cantidad_requerida }}</h3>
                      </div>
                      <div class="col-xs-4 @if ($errors->first('cantidad_recibida') !== "") has-error @endif">
                          <h3 style="display: inline-block;" id="cantidad_recibida_html_p{{ $pedido_producto->id }}">{{ $pedido_producto->cantidad_requerida }} </h3>

                          <input type="hidden" id="cantidad_recibida_p{{ $pedido_producto->id }}" name="cantidad_recibida[]" value="{{$pedido_producto->cantidad_requerida}}"/>
                          <a href="javascript:void(0)" class="m-l-sm text-danger" onclick="showModalProductoPedidoEntregadoEdit({{ $pedido_producto->id }})">
                            <i class="fa fa-pencil"></i>
                          </a>
                          <span class="help-block">{{ $errors->first('cantidad_recibida')}}</span>
                      </div>
                  </div>
              </div>
      @endforeach

    <br><br>

      <div class="row">

        <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6  @if ($errors->first('firmado_por') !== "") has-error @endif">
            {!! Form::label('firmado_por', 'Recibido por:', ['class' => 'control-label']) !!}
            {!! Form::select('firmado_por', \App\Models\ClienteContacto::where('cliente_id', '=', $pedido->cliente_id)->pluck('nombre', 'id')->prepend('','')->put('otro','Otro')->toArray(), null,['class' => 'form-control selectdp', 'onchange' => 'mostrarOtro(this.value)'] ) !!}
            <span class="help-block">{{ $errors->first('firmado_por')}}</span>
        </div>
           
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 @if ($errors->first('firmado_otro') !== "") has-error @else hidden @endif" id="otro_firma">

            {!! Form::label('firmado_otro', 'Nombre Quien Recibe:', ['class' => 'control-label']) !!}
              <div class="input-group" style="width: 100%;">
                {!! Form::text('firmado_otro', null, ['class' => 'form-control', 'placeholder' => 'Otro', 'style'=> 'background: white;']) !!}
              </div>
            <span class="help-block">{{ $errors->first('firmado_otro')}}</span>
         </div>
      
      </div>
      <div class="row">
            <div class="col-sm-12">
                <center>
                <h3 class="text-center text-bold">Firma
                <button class="btn btn-default btn-sm pull-right" type="button" onclick="signaturePad.clear();"><i class="fa fa-trash"></i></button>
                </h3>
                <BR/>
                <canvas id="signature-pad" class="signature-pad" width=300 height=150 
                 style="border: 1px solid #DED5D5; " ></canvas>
                </center>
                <input type="hidden" name="image_data_uri" id="image_data_uri" value=""><br>
                <span class="help-block">{{ $errors->first('image_data_uri')}}</span>
            </div>
      </div>

      <div class="row">

         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <button class="btn btn-success dim btn-large pull-center" type="submit" id="btnSubmit" onclick="confirmarEntrega()"><i class="fa fa-check-circle"></i> Confirmar Entrega</button>
          </div>

      </div>
  </div>
</div>

{!! Form::close() !!} 

<script type="text/javascript">

  var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    penColor: 'rgb(0, 0, 0)'
  });

  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  {{--
  function checkCapacidadMaxima(obj){
    @if ($pedido->establecimiento->capacidad_maxima != null)
        var max_capacidad = {{$pedido->establecimiento->capacidad_maxima}};
      @else
        var max_capacidad = {{$pedido->establecimiento->getCapacidadRecomendada('maxima')}};
      @endif

      if (obj.value > max_capacidad) {
        obj.value=max_capacidad;
      }
  }  


  function calcularCapacidad(cap) {
    var sum = 0;
    $('.capbolsas').each(function(){
      if ($(this).val() != '') {
          sum += parseInt($(this).val());
      } else {
        sum +=0;
      }
    });
    $('#total_bolsas').html(sum);
  }

  function calcularCapacidadCorte(cap) {
    var sum = 0;
    $('.capproducto').each(function(){
      if ($(this).val() != '') {
          sum += parseInt($(this).val());
      } else {
        sum +=0;
      }
    });
    $('#cantidad_total_actual').html(sum+' <small class="text-muted">Bolsas Totales</small>');
  }
  --}}

  function mostrarOtro(option) {
    if(option =='otro') {
      $('#otro_firma').removeClass('hidden');
    } else {
      $('#otro_firma').addClass('hidden');
    }
  }

  function irDestino(){
    $('#panel_generales').addClass('collapsed');
    $('#panel_detalle').removeClass('collapsed');

    @if($pedido->evento_latitud_longitud != null || $pedido->cliente_establecimiento_id == null)
      window.open('http://www.google.com/maps/place/{{$pedido->evento_latitud_longitud}}', '_blank');
    @else
      window.open('http://www.google.com/maps/place/{{$pedido->establecimiento->latitud_longitud}}', '_blank');
    @endif
  }

  function collapsePanel() {

    //$('#panel_generales').removeClass('collapsed');
    //$('#panel_generales').addClass('collapsed');
    //$('#panel_generales').collapse('hide');

    $('#link-tab1').click();
    $('#link-tab2').click();
    
  }

  function confirmarEntrega() {
    {{--
    @if($pedido->establecimiento->fecha_ultimo_corte() != '')
        @if($pedido->establecimiento->fecha_ultimo_corte() != Carbon::now()->formatLocalized('%d/%m/%Y') )
          swal("Error", "Por favor realice el corte al establecimiento antes de hacer la entrega.", "error");
          setTimeout(function(){
            $("#btnSubmit").removeAttr('disabled');
            $("#btnSubmit").prop('disabled', false);
          }, 500);
          return false;
        @endif
    @else
      swal("Error", "Por favor realice el corte al establecimiento antes de hacer la entrega.", "error");
      setTimeout(function(){
          $("#btnSubmit").removeAttr('disabled');
          $("#btnSubmit").prop('disabled', false);
      }, 500);
      return false;
    @endif
    --}}

    if (signaturePad.isEmpty()) {
      swal("Error", "Por favor asegúrese que el cliente firme de recibido.", "error");

      setTimeout(function(){
          $("#btnSubmit").removeAttr('disabled');
          $("#btnSubmit").prop('disabled', false);
      }, 500);
      
      return false;
    } else {
      
      var salert = swal({
        title: "¿Confirma la entrega?",
        @if($pedido->cliente_establecimiento_id != null)
        text: "<h3>{{$pedido->establecimiento->sucursal}}</h3>Revise los productos que se entregaron y la firma del cliente",
        @else
        text: "<h3>{{$pedido->cliente->nombre_corto}}</h3>Revise los productos que se entregaron y la firma del cliente",
        @endif
        type: "warning",
        showCancelButton: true,
        html: true,
        confirmButtonColor: "#1c84c6",
        confirmButtonText: "Sí, seguro!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        cancelButtonText: "Cancelar",
        customClass: 'modalConfirmarAlert',
        showLoaderOnConfirm: true,
      },
      function(){

        var data = signaturePad.toDataURL('image/png');
        $('#image_data_uri').val(data);

        $('.modalConfirmarAlert').toggleClass('sk-loading');
        $( ".modalConfirmarAlert" ).append('<div class="sk-spinner sk-spinner-double-bounce"><div class="sk-double-bounce1"></div><div class="sk-double-bounce2"></div></div>');

        setTimeout(function(){
            $("#formEntrega").removeAttr('onsubmit');
            $('#formEntrega').submit();
            $("#btnSubmit2").attr('disabled','disabled');
        }, 1000);
        
      });
      $('.modalConfirmarAlert').find('button.confirm').attr('id', "btnSubmit2");
    }
    
  }
</script>
