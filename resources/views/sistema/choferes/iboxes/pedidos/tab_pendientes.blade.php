<div class="row">
    @if(count($pedidos) > 0)
        @foreach ($pedidos as $pedido)
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <a href="{{ url('choferes/entregas/'.$pedido->id) }}">
                <div class="widget lazur-bg p-md" style="background-color: #3296b4;">
                    <div class="row">
                        <div class="col-xs-10">
                            <h2>
                                {{$pedido->cliente->nombre_corto}} <BR/> @if($pedido->cliente_establecimiento_id != null){{$pedido->establecimiento->sucursal}} @endif
                            </h2>
                            <ul class="list-unstyled m-t-md">
                                <li>
                                    @foreach($pedido->productos as $pedido_producto)
                                        <span class="fa fa-snowflake-o m-r-xs"></span>
                                        <label style="font-size: 1.3em;">Bolsas {{(int)$pedido_producto->producto->peso}}kg - {{$pedido_producto->cantidad_requerida}}</label>
                                    @endforeach
                                </li>
                                @if($pedido->cliente->es_preferencial != null)
                                    <li>
                                        <BR/>
                                        <span class="fa fa-star m-r-xs fa-2x" style="color:#ffea3e;"></span>
                                        <label>Cliente Preferencial</label>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-xs-2 text-right">
                            <br><br>
                            <i class="fa fa-angle-right fa-4x"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
      @endforeach 
    @else
        <div class="col-xs-12 m-t-md">
              <div class="alert alert-warning text-center">
                <h4>No tiene entregas asignadas para hoy.</h4>
              </div>
        </div>
    @endif
</div>