<div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-12">
                <h2>
                  {{$pedido->cliente->nombre_corto}} <BR/> @if($pedido->cliente_establecimiento_id != null) <strong> {{$pedido->establecimiento->sucursal}} </strong> @endif
                </h2>
                @if($pedido->cliente_establecimiento_id != null)
                  <h3>Zona: 
                  @if($pedido->establecimiento->zona == null)
                    <span class="text-muted"> N.D. </span>
                  @else
                    <span class="text-danger">{{ $pedido->establecimiento->zona->nombre }}</span>
                  @endif
                @endif
                </h3>
            </div>
        </div>
        <BR/>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left">
              <h3 class="text-success">Dirección</h3>
              @if($pedido->evento_calle != null || $pedido->cliente_establecimiento_id == null)
                @php
                    $evento_obj = (object) [
                        'calle' => $pedido->evento_calle,
                        'numero_exterior' => $pedido->evento_numero_exterior,
                        'numero_interior' => $pedido->evento_numero_interior,
                        'colonia' => $pedido->evento_colonia,
                        'codigo_postal' => $pedido->evento_codigo_postal,
                        'municipio' => $pedido->evento_municipio,
                        'estado' => $pedido->evento_estado,
                        'pais' => 'México',
                    ];
                @endphp
                {!! imprime_direccion($evento_obj) !!}
              @else
                {!! imprime_direccion($pedido->establecimiento) !!}
              @endif
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
              <h3 class="text-success">Indicaciones</h3>
              @if($pedido->evento_direccion_indicaciones != null)
                  <span class="text-primary">{!! nl2br(e($pedido->evento_direccion_indicaciones)) !!}</span>
              @else
                <span class="text-primary">N.D.</span>
              @endif
            </div>
        </div>
        <div class="row">
            @if($pedido->evento_latitud_longitud != null || $pedido->cliente_establecimiento_id == null)
              <input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$pedido->evento_latitud_longitud}}" />
            @else
              <input type="hidden" name="latitud_longitud" id="latitud_longitud" value="{{$pedido->establecimiento->latitud_longitud}}" />
            @endif
            <div id="map" style="height: 400px;"></div>
        </div>
        
        <div class="row m-t-lg">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
              <a style="width:100%;" class="btn btn-info" href="{{ url('/')}}"> 
                <i class="fa fa-home"></i> Regresar
              </a>
          </div>
        </div>
    </div>
</div>