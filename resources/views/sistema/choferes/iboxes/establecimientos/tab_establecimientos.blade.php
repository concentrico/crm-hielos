<div class="row">
    @if(count($pedidos) > 0)
        @foreach ($pedidos as $pedido)
            @if($pedido->cliente_establecimiento_id != null)
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <a href="{{ url('choferes/establecimientos/'.$pedido->id) }}">
                    <div class="widget lazur-bg p-md" style="background-color: #2F6EBA;">
                        <div class="row">
                            <div class="col-xs-10">
                                <h2>
                                    {{$pedido->cliente->nombre_corto}} <BR/> @if($pedido->cliente_establecimiento_id != null) {{$pedido->establecimiento->sucursal}} @endif
                                </h2>
                                <ul class="list-unstyled m-t-md">
                                    <li>
                                        <span class="fa fa-home m-r-xs"></span>
                                        <label>Dirección:</label><BR/>

                                        @if($pedido->evento_calle != null || $pedido->cliente_establecimiento_id == null)
                                        @php
                                            $evento_obj = (object) [
                                                'calle' => $pedido->evento_calle,
                                                'numero_exterior' => $pedido->evento_numero_exterior,
                                                'numero_interior' => $pedido->evento_numero_interior,
                                                'colonia' => $pedido->evento_colonia,
                                                'codigo_postal' => $pedido->evento_codigo_postal,
                                                'municipio' => $pedido->evento_municipio,
                                                'estado' => $pedido->evento_estado,
                                                'pais' => 'México',
                                            ];
                                        @endphp
                                            {!! imprime_direccion($evento_obj) !!}

                                        @else
                                            {!! imprime_direccion($pedido->establecimiento) !!}
                                        @endif
                                    </li>
                                    @if($pedido->cliente->es_preferencial != null)
                                        <li>
                                            <BR/>
                                            <span class="fa fa-star m-r-xs fa-2x" style="color:#ffea3e;"></span>
                                            <label>Cliente Preferencial</label>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="col-xs-2 text-right">
                                <br><br>
                                <i class="fa fa-angle-right fa-4x"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endif
        @endforeach
    @else
        <div class="col-xs-12 m-t-md">
              <div class="alert alert-warning text-center">
                <h4>No tiene entregas asignadas en establecimientos para hoy.</h4>
              </div>
        </div>
    @endif
</div>
