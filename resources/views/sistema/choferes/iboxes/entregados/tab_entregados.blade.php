<div class="row">
  @if(count($entregados) > 0)
    @foreach ($entregados as $pedido) 
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <a href="{{ url('choferes/entregados/'.$pedido->id) }}">
            <div class="widget yellow-bg p-md">
                <div class="row">
                    <div class="col-xs-10">
                        <h2>
                            {{$pedido->cliente->nombre_corto}} <BR/> @if($pedido->cliente_establecimiento_id != null) {{$pedido->establecimiento->sucursal}} @endif
                        </h2>
                        <ul class="list-unstyled m-t-md">
                            <li>
                                @foreach($pedido->productos as $pedido_producto)
                                    <span class="fa fa-snowflake-o m-r-xs"></span>
                                    <label style="font-size: 1.3em;">Bolsas {{(int)$pedido_producto->producto->peso}}kg - {{$pedido_producto->cantidad_requerida}}</label>
                                @endforeach
                            </li>
                            <li>
                              <span class="fa fa-clock-o m-r-xs"></span>
                              @php
                                $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
                              @endphp
                              <label style="font-size: 1.3em;">Hora: {{$hora_entrega}} </label>
                            </li>
                            @if($pedido->cliente->es_preferencial != null)
                                <li>
                                    <BR/>
                                    <span class="fa fa-star m-r-xs fa-2x" style="color:#ffea3e;"></span>
                                    <label>Cliente Preferencial</label>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-xs-2 text-right">
                        <br><br>
                        <i class="fa fa-angle-right fa-4x"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
  @else
    <div class="col-xs-12 m-t-md">
          <div class="alert alert-warning text-center">
              <h4>No tiene pedidos entregados el día de hoy.</h4>
          </div>
    </div>
  @endif
</div>