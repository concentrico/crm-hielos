<div class="ibox float-e-margin">

  <div class="ibox-title text-center">
    <h2 class="text-info"> {{$pedido->cliente->nombre_corto}} <BR/> @if($pedido->cliente_establecimiento_id != null){{$pedido->establecimiento->sucursal}} @endif</h2>
    
    @php
      $hora_entrega = Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->formatLocalized('%I:%M %p');
    @endphp

    <span class="badge badge-default p-xs"> 
        <span style="font-size: 1.3em;" class="fa fa-clock-o m-r-xs"></span>
        <label style="font-size: 1.3em; margin-bottom: 0px;">{{$hora_entrega}}</label>
    </span>
  </div>

  <div class="ibox-content" style="min-height: 400px;">


    <div class="row">
        <div class="col-xs-12">
            <div class="ibox-content" style="border: 0px; padding: 0px;">
                <div class="row">
                    <div class="col-xs-4">
                        <h3 class="text-success">Producto</h3>
                    </div>

                    <div class="col-xs-4">
                        <h3 class="text-success">Solicitado</h3>
                    </div>
                    <div class="col-xs-4">
                        <h3 class="text-success">Entregado</h3>
                    </div>
                </div>
            </div>

            @foreach($pedido->productos as $pedido_producto) 

                    <div class="ibox-content" style="padding: 0px;">
                          <div class="row">
                              <div class="col-xs-4">
                                @if($pedido_producto->precio_unitario == 0.00)
                                  <h4>{{ $pedido_producto->producto->nombre }} <BR/> <span class="text-info">Cortesía </span></h4>
                                @else
                                  <h3>{{ $pedido_producto->producto->nombre }}</h3>
                                @endif
                              </div>

                              <div class="col-xs-4">
                                  <h3 class="text-center">{{ $pedido_producto->cantidad_requerida }}</h3>
                              </div>
                              <div class="col-xs-4">
                                  <h3 class="text-center text-danger">{{ $pedido_producto->cantidad_recibida }} </h3>
                              </div>
                          </div>
                      </div>
            @endforeach
        </div>
    </div>

      <div class="row m-t-lg">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <h3 class="text-success">Firmado por:</h3>
              @if($pedido->firmado_otro != null)
                <h1>{{$pedido->firmado_otro}}</h1>
              @else
                <h3>{{$pedido->cliente_firma->nombre}} {{$pedido->cliente_firma->apellido_paterno}} {{$pedido->cliente_firma->apellido_materno}}</h3>
              @endif
          </div>

          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <h3 class="text-success">Firma:</h3>
              @if($pedido->firma_path != null)
                <img src="{{url($pedido->firma_path)}}" />
              @else
                <div class="product-imitation">[ N.D. ]</div>
              @endif
          </div>

      </div>

      <div class="row m-t-lg">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <a style="width:100%;" class="btn btn-info" href="{{ url('/')}}"> 
              <i class="fa fa-home"></i> Regresar
            </a>
        </div>
      </div>
</div>
</div>