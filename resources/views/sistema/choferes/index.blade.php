@extends('sistema.layouts.master')

@section('title', 'Pedidos del Día')

@section('attr_navlink_2', 'class="active"')

@section('breadcrumb_title', 'Pedidos del Dia')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Entregas</a></li>

<li class="active"><strong> 
	{{ fecha_to_human(Carbon::now(),false)}}
</strong></li>

@endsection

@section('content')
<style type="text/css" media="screen">
	@media (max-width: 992px) {
		.div-icon {	
			padding: 0px;
		}

	}
	@media (min-width: 1280px) {
		.widget h3 {	
			font-size: 25px!important;
		}

	}
</style>

<div class="row">
    
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <a data-toggle="tab" href="#tab-1" >
            <div class="widget style-custom">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-xs-12 text-center div-icon">
                        <i class="fa fa-truck fa-4x"></i>
                        <h3 class="visible-sm">Pendientes</h3>
                    </div>
                    <div class="col-xs-10 text-center visible-lg visible-md">
                        <span> &nbsp; </span>
                        <h3 class="font-bold no-margins text-right">Pendientes</h3>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <a data-toggle="tab" href="#tab-2" >
            <div class="widget style-custom">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-xs-12 text-center div-icon">
                        <i class="fa fa-map fa-4x" ></i>
                        <h3 class="visible-sm">Establecimientos</h3>
                    </div>
                    <div class="col-xs-10 text-center visible-lg visible-md">
                        <span> &nbsp; </span>
                        <h3 class="font-bold no-margins text-right">Establecimientos</h3>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <a data-toggle="tab" href="#tab-3" >
            <div class="widget style-custom">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-xs-12 text-center div-icon">
                        <i class="fa fa-check-circle-o fa-4x"></i>
                        <h3 class="visible-sm">Entregadas</h3>
                    </div>
                    <div class="col-xs-10 text-center visible-lg visible-md">
                        <span> &nbsp; </span>
                        <h3 class="font-bold no-margins text-right">Entregados</h3>
                    </div>
                </div>
            </div>
        </a>
    </div>

</div>


@php 
$bolsas=0;
if(count($pedidos) > 0){
    foreach ($pedidos as $pedido){
        foreach($pedido->productos as $pedido_producto){
            $bolsas+=$pedido_producto->cantidad_requerida;
        }
    }
}
@endphp

<div class="row">
    <div class="col-sm-12">
        <div class="tab-content">
	      	<div id="tab-1" class="tab-pane @if(Input::get('active_tab') == 1 || Input::has('active_tab') == false) active @endif">
                <h2 class="font-bold no-margins text-center visible-xs visible-sm">Pendientes</h2>
                <h3 class="font-bold no-margins text-center text-success visible-xs visible-sm">Total por Embarcar: <h3 class="font-bold no-margins text-center text-danger visible-xs visible-sm"> {{ $bolsas }} bolsas</h3> </h3>
                @include('sistema.choferes.iboxes.pedidos.tab_pendientes')
            </div>
			<div id="tab-2" class="tab-pane @if(Input::get('active_tab') == 2) active @endif">
                <h2 class="font-bold no-margins text-center visible-xs visible-sm">Establecimientos</h2>
                @include('sistema.choferes.iboxes.establecimientos.tab_establecimientos')
            </div>
			<div id="tab-3" class="tab-pane @if(Input::get('active_tab') == 3) active @endif">
                <h2 class="font-bold no-margins text-center visible-xs visible-sm">Entregas del Día</h2>
                @include('sistema.choferes.iboxes.entregados.tab_entregados')
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets_bottom')
    <script type="text/javascript"> 

        @if(session('entrega_pedido_success'))
            swal("Correcto!", "Se ha finalizado la entrega satisfactoriamente.", "success");
        @endif

    </script>
@endsection