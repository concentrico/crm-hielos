@extends('sistema.layouts.master')

@section('title', 'Lista de Choferes')

@section('attr_navlink_6', 'class="active"')

@section('breadcrumb_title', 'Listado de Choferes')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong>Choferes</strong></li>
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">
		@include('sistema.choferes.admin.iboxes.filtros')
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@include('sistema.choferes.admin.iboxes.tabla')
	</div>
</div>

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>	
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

@endsection