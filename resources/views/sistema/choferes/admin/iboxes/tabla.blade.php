<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><i class="fa fa-list"></i> Listado de Choferes</h5>
    </div>
    <div class="ibox-content">
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $usuarios->appends(Input::all())->appends(['active_tab' => '1'])->render() !!}
			</div>
	    </div>
        <div>
			<table class="table table-hover table-res" data-cascade="true">
				<thead>
					<tr>
						<th data-type="html">{!! getHeaderLink('NOMBRE', 'Nombre') !!}</th>
						<th data-type="html">{!! getHeaderLink('USERNAME', 'Usuario') !!}</th>
						<th data-type="html">Última Entrega</th>
						<th data-type="html" data-breakpoints="sm">{!! getHeaderLink('EMAIL',  'Correo Electrónico') !!}</th>
						<th data-type="html">{!! getHeaderLink('CELPHONE', 'Celular') !!}</th>
						<th data-type="html" data-breakpoints="sm"></th>
					</tr>
				</thead>
				<tbody>
                    @foreach ($usuarios as $usuario)
                    	

	                    <tr class="@if($usuario->deleted_at != NULL) warning @endif">
	                        <td>
		                    	<a href="{{url('choferes').'/'.$usuario->id }}">{{$usuario->nombre}}</a>
		                    </td>
	                        <td>{!! $usuario->username !!}</td>
	                        <td>{!! $usuario->ultima_entrega() !!}</td>
	                        <td>{{ $usuario->email }}</td>
	                        <td>{{ $usuario->celular }}</td>
	                       	<td class="text-center">
		                    	<a href="{{url('choferes').'/'.$usuario->id }}"><i class="fa fa-lg fa-eye"></i></a>
		                    </td>
	                    </tr>
					@endforeach
				</tbody>
			</table>            	        
        </div>
	    <div class="row">
		    <div class="col-sm-12 text-right">
				{!! $usuarios->appends(Input::all())->appends(['active_tab' => '1'])->render() !!}
			</div>
	    </div>
    </div>
</div>