<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-user"></i> Datos Generales </h5>
        <h3 class="text-info pull-right">{{ $usuario->nombre }}</h3>
    </div>
    
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="text-success">Rol</h4>
                {{ implode(', ', $usuario->roles->pluck('nombre')->toArray()) }}
            </div>
            <div class="col-sm-4">
                <h4 class="text-success">Email</h4>
                {{ $usuario->email }}
            </div>
            <div class="col-sm-4">
                <h4 class="text-success">Usuario</h4>
                {{ $usuario->username }}
            </div>
        </div>
        <BR/>
        <div class="row">
            <div class="col-sm-4">
                <h4 class="text-success">Fecha Nacimiento</h4>
                @if(isset($usuario->fecha_nacimiento)) 
                    {{ $usuario->fecha_nacimiento }}
                @else
                    No Aplica
                @endif
            </div>
            <div class="col-sm-4">
                <h4 class="text-success">Teléfono</h4>
                @if(isset($usuario->telefono)) 
                    {{ $usuario->telefono }}
                @else
                    No Aplica
                @endif
            </div>
            <div class="col-sm-4">
                <h4 class="text-success">Celular</h4>
                @if(isset($usuario->celular)) 
                    {{ $usuario->celular }}
                @else
                    No Aplica
                @endif
            </div>
        </div>
    </div>
</div>