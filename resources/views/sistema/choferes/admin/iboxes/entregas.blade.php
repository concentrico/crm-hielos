<style type="text/css" media="screen">
pre{margin:3px;}
</style>
<div class="ibox float-e-margin">
    <div class="ibox-title">
        <h5><i class="fa fa-truck"></i> Últimas Entregas </h5>
    </div>
    
    <div class="ibox-content" >

        @if(count($usuario->entregas) > 0)

            <!--<div class="table-responsive">-->
                <table class="table table-res table-hover issue-tracker" data-cascade="true" data-page-size="5" data-paging="true">
                    <thead>
                        <tr>
                            <th data-type="html">ID</th>
                            <th data-type="html">Cliente</th>
                            <th data-type="html">Establecimiento</th>
                            <th data-type="html">Fecha y Hora</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($usuario->entregas->sortByDesc('entregado_at') as $pedido)
                        <tr class="@if($pedido->deleted_at != NULL) warning @endif">
                            <td>
                                <a class="text-navy" href="{{ url('pedidos/'.$pedido->id) }}">Pedido #{{ $pedido->id }}</a>
                            </td>
                            <td><a href="{{ url('clientes/'.$pedido->cliente_id) }}">{{ $pedido->cliente->nombre_corto }}</a></td>
                            <td>@if($pedido->cliente_establecimiento_id!=null)<a href="{{ url('establecimientos/'.$pedido->id) }}">{{ $pedido->establecimiento->sucursal }}</a>@else N.A. @endif</td>
                            <td>
                                @if($pedido->entregado_at != null)
                                    {{fecha_to_human($pedido->entregado_at,false) }}
                                    -
                                    {{ Carbon::createFromFormat('Y-m-d H:i:s', $pedido->entregado_at)->format('h:i A') }}
                                @else
                                    Sin definir
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2">
                            <ul class="pagination pull-right"></ul>
                        </td>
                      </tr>
                    </tfoot>
                </table>
            <!--</div>-->
        @else
            <div class="alert alert-warning text-center">
                No tiene entregas disponibles
            </div>
        @endif
	    
    </div>
</div>
