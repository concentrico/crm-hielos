@extends('sistema.layouts.master')

@section('title', $usuario->nombre)

@section('attr_navlink_6', 'class="active"')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb_title', $usuario->nombre)

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li><a href="{{ url('choferes') }}">Choferes</a></li>
<li class="active"><strong>{{ $usuario->nombre }}</strong></li>
<li></li>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12 col-md-12" style="padding: 0 5px;">
      @include('sistema.choferes.admin.iboxes.datos')       
      @include('sistema.choferes.admin.iboxes.entregas')           
    </div>
    <div class="col-sm-12 col-md-6" style="padding: 0 5px;">    
    </div>
</div>

@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
@endsection