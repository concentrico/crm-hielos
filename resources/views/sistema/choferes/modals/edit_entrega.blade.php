<div class="modal inmodal" id="modalProductoPedidoEntregadoEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<button role="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-snowflake-o modal-icon"></i> <BR/>Editar Cantidad a Entregar</h4>
				<h3 class="text-success text-center" id="producto_nombre_editar"></h3>
				<input type="hidden" name="producto_id_editar" id="producto_id_editar" value="">
			</div>
				<div class="modal-body">
		            <div class="row">
			            <div class="form-group col-sm-6 col-sm-offset-3">
				            {!! Form::label('cantidad_editar', 'Cantidad Entregada', ['class' => 'control-label text-center']) !!}
			            	<div class="form-group" style="width: 100%;">
		                    	<input class="touchspin_cantidad positive-integer form-control" type="number" id="cantidad_recibida_edit" name="cantidad_recibida_edit" value=""/>
		                  	</div>
				        </div>
              		</div>
				</div>
				<div class="modal-footer">
					<button role="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
					<button role="button" class="btn btn-success" onclick="editarProductoPedidoEntrega()"><i class="fa fa-save"></i> Guardar</button>
				</div>
		</div>
	</div>
</div>

@section('assets_bottom')
	@parent
<script type="text/javascript">

	function showModalProductoPedidoEntregadoEdit(producto_id){

		$('#producto_nombre_editar').html(productos_pedido[producto_id]['producto_nombre']);
		$('#producto_id_editar').val(producto_id);

		$('#modalProductoPedidoEntregadoEdit').find('input[name=cantidad_recibida_edit]').val($('#cantidad_recibida_p'+producto_id).val());

		$('#modalProductoPedidoEntregadoEdit').modal('show');
	}
</script>
@endsection