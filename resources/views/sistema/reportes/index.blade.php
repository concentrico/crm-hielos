@extends('sistema.layouts.master')

@section('title', 'Reportes')

@section('assets_top_top')
<link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" />
@endsection

@section('attr_navlink_11', 'class="active"')

@section('breadcrumb_title', 'Reportes')

@section('breadcrumb_content')
<li><a href="{{ url('/') }}">Home</a></li>
<li class="active"><strong>Reportes</strong></li>
<li></li>
@endsection

@section('content')
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Reporte General de Pedidos</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-12 b-r">
                <small>Si selecciona un Rango de Fechas se omitirá el <b>mes</b> seleccionado.</small><br /><br />
                {!! Form::open(['method' => 'POST',  'url' => url('reportes/generar')] ) !!}

                    <div class="row">

                        <div class="col-sm-4">
                            {!! Form::label('mes_reporte', 'Mes', ['class' => 'control-label']) !!}
                            <div class="input-group" style="width: 100%;">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::select('mes_reporte', $meses, $mes_actual, ['class' => 'form-control selectdp', 'data-placeholder' => 'Mes'] ) !!}
                            </div>
                        </div>

                        <div class="col-sm-4">
                            {!! Form::label('cliente_reporte', 'Filtrar por Cliente', ['class' => 'control-label']) !!}
                            <div class="input-group" style="width: 100%;">
                                <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                {!! Form::select('cliente_reporte',\App\Models\Cliente::all()->sortBy('nombre_corto')->pluck('nombre_corto', 'id')->prepend('','')->toArray(), null, ['class' => 'form-control selectdp', 'data-placeholder' => 'Cliente'] ) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            {!! Form::label('fecha', 'Rango de Fechas', ['class' => 'control-label']) !!}
                            <div class="input-daterange input-group" style="width: 100%;">
                                {!! Form::text('fecha_ini', Input::get('fecha_ini'), ['class' => 'form-control', 'placeholder' => 'Fecha Inicio']) !!}
                                  <span class="input-group-addon">-</span>
                                {!! Form::text('fecha_fin', Input::get('fecha_fin'), ['class' => 'form-control', 'placeholder' => 'Fecha Fin']) !!}
                            </div>
                        </div>
                    </div>
                    <br />
                    <button class="btn btn-success">
                        <i class="fa fa-download"></i> Descargar reporte
                    </button>
                {!! Form::close() !!}
                <hr />
            </div>
        </div>
    </div>
</div>
@endsection

@section('assets_bottom')
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>  
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>    
<script type="text/javascript"> 

    $.fn.datepicker.dates['es-MX'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
        today: "Hoy",
        clear: "Borrar",
        format: "mm/dd/yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };
        
    $('.input-group.date').datepicker({
        startView: 0,
        todayHighlight: true,
        language: 'es-MX',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        endDate: new Date(),
        format: "yyyy-mm-dd"
    });

    $('.input-daterange').datepicker({
        todayBtn: "linked",
        language: 'es-MX',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        endDate: new Date(),
        format: "yyyy-mm-dd"
    });

</script>
@endsection