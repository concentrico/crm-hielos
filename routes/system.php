<?php

/*
|--------------------------------------------------------------------------
| Sistema Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

App::setLocale('es');

//Rutas Internas
if ($this->app->environment() == 'local')
	$subdomain = 'sistematest.';
else
	$subdomain = 'sistema.';

//Route::group(['namespace' => 'Sistema', 'domain' => $subdomain.env('APP_DOMAIN')], function () {
Route::group(['namespace' => 'Sistema', 'domain' => env('APP_DOMAIN')], function () {

	Route::get('password/reset', 			'Auth\ForgotPasswordController@showLinkRequestForm');
	Route::post('password/reset', 			'Auth\ResetPasswordController@reset');
	Route::get('password/reset/{token?}', 	'Auth\ResetPasswordController@showResetForm');
	Route::post('password/email', 			'Auth\ForgotPasswordController@sendResetLinkEmail');

	// Authentication routes...
	Route::get('login', 	'SessionController@showLoginForm');
	Route::post('login', 	'SessionController@login');
	Route::get('logout', 	'SessionController@logout');

	Route::group(['middleware' => 'auth'], function () {

	// Dashboard
	Route::get('/', 	'DashboardController@index');
	Route::get('/home', 'DashboardController@index');
	Route::get('/dashboard', 'DashboardController@indexDireccion');
	Route::get('/produccion', 'DashboardController@indexProduccion');
	Route::get('/distribucion', 'DashboardController@indexDistribucion');
 	Route::post('produccion/registrar', 'DashboardController@registrarProduccion');

	// Usuarios Routes
	Route::get('perfil', 	'UsuariosController@showPerfil');
 	Route::post('usuarios/{id}/reactivar', 							'UsuariosController@reactivar');
 	Route::post('usuarios/contacto', 								'UsuariosController@createFromContact');
	Route::put('usuarios/{id}/update2',								'UsuariosController@update2');
	Route::put('usuarios/perfil',									'UsuariosController@updatePerfil');
 	Route::resource('usuarios', 									'UsuariosController');

	/* Clientes */
	Route::post('clientes/{id}/datosf', 							'ClientesController@storeDatosFiscales');
	Route::put('clientes/{id}/datosf/{datosf_id}', 					'ClientesController@updateDatosFiscales');
	Route::delete('clientes/{id}/datosf/{datosf_id}', 				'ClientesController@destroyDatosFiscales');
	Route::post('clientes/{id}/contactos', 							'ClientesController@storeContacto');
	Route::put('clientes/{id}/contactos/{contacto}', 				'ClientesController@updateContacto');
	Route::delete('clientes/{id}/contactos/{contacto}', 			'ClientesController@destroyContacto');
	Route::put('clientes/{id}/update-acuerdos', 					'ClientesController@updateAcuerdosComerciales');
	Route::post('clientes/{id}/addProducto', 						'ClientesController@addProducto');
 	Route::post('clientes/{id}/reactivar', 							'ClientesController@reactivar');
	Route::resource('clientes',										'ClientesController');

	/* Establecimientos */
	Route::post('establecimientos/{cliente_id}', 					'EstablecimientosController@store');
	Route::post('establecimientos/{cliente_id}/store2', 			'EstablecimientosController@store2');
	Route::put('establecimientos/{establecimiento_id}', 			'EstablecimientosController@update');
	Route::delete('establecimientos/{establecimiento_id}', 			'EstablecimientosController@destroy');
	Route::post('establecimientos/{id}/datosf', 					'EstablecimientosController@storeDatosFiscales');
	Route::put('establecimientos/{id}/datosf/{datosf_id}', 			'EstablecimientosController@updateDatosFiscales');
	Route::delete('establecimientos/{id}/datosf/{datosf_id}', 		'EstablecimientosController@destroyDatosFiscales');
	Route::post('establecimientos/{id}/contactos', 					'EstablecimientosController@storeContacto');
	Route::put('establecimientos/{id}/contactos/{contacto}', 		'EstablecimientosController@updateContacto');
	Route::delete('establecimientos/{id}/contactos/{contacto}', 	'EstablecimientosController@destroyContacto');
	Route::post('establecimientos/{id}/asignar-conservadores', 		'EstablecimientosController@asignarConservadores');
	Route::put('establecimientos/{id}/cuartofrio/{cf_id}', 			'EstablecimientosController@updateCuartoFrio');
	Route::delete('establecimientos/{id}/cuartofrio/{cf_id}', 		'EstablecimientosController@destroyCuartoFrio');
	Route::put('establecimientos/{id}/update-acuerdos', 			'EstablecimientosController@updateAcuerdosComerciales');
	Route::post('establecimientos/{id}/realizar-corte', 			'EstablecimientosController@realizarCorte');
	Route::post('establecimientos/{id}/establecer-ubicacion', 		'EstablecimientosController@establecerUbicacion');
	Route::put('establecimientos/{id}/inventario', 					'EstablecimientosController@updateInventario');
 	Route::post('establecimientos/{id}/reactivar', 					'EstablecimientosController@reactivar');
	Route::resource('establecimientos',								'EstablecimientosController');

	/* Parametros  */
	Route::get('parametros', 							'ParametroController@index');
	Route::post('parametros', 							'ParametroController@store');
    Route::put('parametros/{id}', 						'ParametroController@update');
    Route::post('parametros/settings', 					'ParametroController@updateSettings');

    # Modelos Conservador
    Route::post('parametros/modelo', 					'ParametroController@storeModeloConservador');
    Route::put('parametros/modelo/{id}', 				'ParametroController@updateModeloConservador');
    Route::delete('parametros/modelo/{id}', 			'ParametroController@destroyModeloConservador');

    # Zonas de Entrega
    Route::post('parametros/zona', 						'ParametroController@storeZonaEntrega');
    Route::put('parametros/zona/{id}', 					'ParametroController@updateZonaEntrega');
    Route::delete('parametros/zona/{id}', 				'ParametroController@destroyZonaEntrega');

    # Condiciones de Pago
    Route::post('parametros/condicion', 				'ParametroController@storeCondicionPago');
    Route::put('parametros/condicion/{id}', 			'ParametroController@updateCondicionPago');
    Route::delete('parametros/condicion/{id}', 			'ParametroController@destroyCondicionPago');

    # Formas de Pago
    Route::post('parametros/formapago', 				'ParametroController@storeFormaPago');
    Route::put('parametros/formapago/{id}', 			'ParametroController@updateFormaPago');
    Route::delete('parametros/formapago/{id}', 			'ParametroController@destroyFormaPago');

    # Tipos Establecimiento
    Route::post('parametros/tipoestab', 				'ParametroController@storeTipoEstablecimiento');
    Route::put('parametros/tipoestab/{id}', 			'ParametroController@updateTipoEstablecimiento');
    Route::delete('parametros/tipoestab/{id}', 			'ParametroController@destroyTipoEstablecimiento');

	/* Conservadores */
	Route::post('conservadores/{id}/reactivar', 'ConservadorController@reactivar');
 	Route::resource('conservadores', 'ConservadorController');
 	Route::resource('conservadores','ConservadorController', ['only' => ['store', 'update', 'destroy']]);
 	Route::post('conservadores/{id}/mtto', 'ConservadorController@registrarMtto');
 	Route::put('conservadores/{id}/status', 'ConservadorController@cambiarStatus');

 	Route::post('conservadores/{id}/consignar', 'ConservadorController@consignarEstablecimiento');
 	Route::put('conservadores/{id}/establecimiento/{est_id}', 	'ConservadorController@updateAcuerdosComerciales');

 	Route::post('conservadores/{id}/reactivar', 'ConservadorController@reactivar');

 	/* Inventario */
 	// Inventario Interno
 	Route::get('inventario/interno', 'InventarioController@index');
 	Route::post('inventario/interno/{inventario_id}', 'InventarioController@registrarMovimientoInvInterno');
 	Route::get('inventario/entradas/{id}', 'InventarioController@showEntradas')->name('showEntradas');
 	Route::get('inventario/salidas/{id}', 'InventarioController@showSalidas')->name('showSalidas');
 	Route::put('inventario/entradas/{id}', 'InventarioController@updateEntrada')->name('updateEntrada');
 	Route::put('inventario/salidas/{id}', 'InventarioController@updateSalida')->name('updateSalida');

 	Route::post('inventario/entradas/{id}/delete', 'InventarioController@deleteEntrada')->name('deleteEntrada');
 	Route::post('inventario/salidas/{id}/delete', 'InventarioController@deleteSalida')->name('deleteSalida');
 	Route::post('inventario/entradas/{id}/reactivar', 'InventarioController@reactivarEntrada')->name('reactivarEntrada');
 	Route::post('inventario/salidas/{id}/reactivar', 'InventarioController@reactivarSalida')->name('reactivarSalida');

 	// Inventario Externo
 	Route::get('inventario/externo', 	'InventarioController@indexExterno')->name('indexExterno');
 	Route::get('inventario/externo/{id}', 	'InventarioController@showExterno')->name('showExterno');
 	Route::post('inventario/externo/{establecimiento_id}/realizar-corte', 'InventarioController@realizarCorte');
 	Route::post('inventario/externo/{id}/realizar-corte', 'InventarioController@realizarCorte');
 	Route::post('inventario/externo/{id}/editar-puntos', 'InventarioController@editarPuntosInventario');
	Route::post('inventario/externo/{id}/asignar-conservadores', 		'InventarioController@asignarConservadores');
	Route::put('inventario/externo/{id}/cuartofrio/{cf_id}', 			'InventarioController@updateCuartoFrio');
	Route::delete('inventario/externo/{id}/cuartofrio/{cf_id}', 		'InventarioController@destroyCuartoFrio');

 	Route::resource('inventario', 'InventarioController', ['only' => ['index']]);

 	/* Productos*/
 	Route::resource('productos', 						'ProductoController');
 	Route::post('productos/{id}/reactivar', 			'ProductoController@reactivar')->name('reactivar');
	Route::resource('productos',	 		            'ProductoController', ['only' => ['store', 'update', 'destroy']]);
	Route::put('productos/{id}/update2',				'ProductoController@update2');

	/* Choferes*/
    Route::post('choferes/{id}/{est_id}/realizar-corte', 				'ChoferesController@realizarCorte');
    Route::put('choferes/{id}/inventario', 			    		'ChoferesController@updateInventario');

    Route::get('choferes/entregas/{id}', 	'ChoferesController@showChofer')->name('showChofer');
    Route::put('choferes/entregas/{id}', 	'ChoferesController@entregarPedido')->name('entregarPedido');

    Route::resource('choferes/entregados', 						'ChoferesController@entregados');
    Route::resource('choferes/establecimientos', 				'ChoferesController@establecimientos');
  	Route::resource('choferes', 								'ChoferesController');

    /* Pedidos*/
	Route::post('pedidos/inventario-externo/{id}', 		'PedidoController@createFromInventario');
	Route::post('pedidos/cliente-est', 					'PedidoController@createFromClienteEst');
	Route::post('pedidos/cliente', 						'PedidoController@createFromCliente');
	Route::put('pedidos/{id}/direccion', 				'PedidoController@updateDireccion');
	Route::put('pedidos/{id}/status', 					'PedidoController@cambiarStatus');
	Route::put('pedidos/{id}/status-cobranza', 			'PedidoController@cambiarStatusCobranza');
	Route::put('pedidos/{id}/status-facturacion', 		'PedidoController@cambiarStatusFacturacion');
	Route::put('pedidos/{id}/producto/{producto_id}', 	'PedidoController@updateProductoPedido');
	Route::post('pedidos/{id}/producto', 				'PedidoController@addProductoPedido');
	Route::post('pedidos/{id}/cortesia', 				'PedidoController@addCortesiaPedido');
	Route::post('pedidos/{id}/pago',	 				'PedidoController@storePagoPedido');
	Route::put('pedidos/{id}/pago/{pago_id}', 			'PedidoController@updatePagoPedido');
	Route::delete('pedidos/{id}/pago/{pago_id}',		'PedidoController@destroyPagoPedido');
	Route::post('pedidos/{id}/factura',	 				'PedidoController@storeFacturaPedido');
	Route::put('pedidos/{id}/factura/{factura_id}',		'PedidoController@updateFacturaPedido');
	Route::delete('pedidos/{id}/factura/{factura_id}',	'PedidoController@destroyFacturaPedido');
	Route::get('pedidos/{id}/remision',	 				'PedidoController@generaRemision');
	Route::post('pedidos/{id}/sendEmail',	 			'PedidoController@sendEmail');
 	Route::resource('pedidos', 							'PedidoController');

 	/* Cobranza*/
	Route::put('cobranza/{id}/status-cobranza', 		'CobranzaController@cambiarStatusCobranza');
	Route::put('cobranza/{id}/status-facturacion', 		'CobranzaController@cambiarStatusFacturacion');
	Route::post('cobranza/{id}/pago',	 				'CobranzaController@storePagoPedido');
	Route::put('cobranza/{id}/pago/{pago_id}', 			'CobranzaController@updatePagoPedido');
	Route::delete('cobranza/{id}/pago/{pago_id}',		'CobranzaController@destroyPagoPedido');
	Route::post('cobranza/{id}/factura',	 				'CobranzaController@storeFacturaPedido');
	Route::put('cobranza/{id}/factura/{factura_id}',		'CobranzaController@updateFacturaPedido');
	Route::delete('cobranza/{id}/factura/{factura_id}',	'CobranzaController@destroyFacturaPedido');
 	Route::resource('cobranza', 					'CobranzaController');

 	/* Reportes */
	Route::post('reportes/generar',	 				'ReportesController@generarReporte');
 	Route::resource('reportes', 					'ReportesController');

	/* AJAX RESPONSES*/
	Route::post('ajax/dashboard', 									'AjaxController@ajaxDashboard');
	Route::post('ajax/establecimientos', 							'AjaxController@ajaxEstablecimientos');
	Route::post('ajax/productos', 									'AjaxController@ajaxProductos');
	Route::post('ajax/conservadores', 								'AjaxController@ajaxConservadores');
	Route::post('ajax/inventario', 									'AjaxController@ajaxInventario');

	Route::post('ajax/pedidos', 									'AjaxController@ajaxPedidos');
	Route::post('ajax/cobranza', 									'AjaxController@ajaxCobranza');

	Route::get('ajax/autocomplete-codigospostales', 				'AjaxController@ajaxCodigosPostales');

	});
});