<?php

/*
|--------------------------------------------------------------------------
| Cliente Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

App::setLocale('es');

//Rutas Internas
if ($this->app->environment() == 'local')
	$subdomain = 'clientestest.';
else
	$subdomain = 'clientes.';

Route::group(['namespace' => 'Clientes', 'domain' => $subdomain.env('APP_DOMAIN', 'concentrico.com.mx')], function () {

	Route::get('password/reset', 			'Auth\ForgotPasswordController@showLinkRequestForm');
	Route::post('password/reset', 			'Auth\ResetPasswordController@reset');
	Route::get('password/reset/{token?}', 	'Auth\ResetPasswordController@showResetForm');
	Route::post('password/email', 			'Auth\ForgotPasswordController@sendResetLinkEmail');

	// Authentication routes...
	Route::get('login', 	'SessionController@showLoginForm');
	Route::post('login', 	'SessionController@login');
	Route::get('logout', 	'SessionController@logout');

	Route::group(['middleware' => 'auth'], function () {

		/*Dashboard*/
		Route::get('/', 	'DashboardController@index');
		Route::get('/home', 'DashboardController@index');

		/* AJAX RESPONSES*/
		Route::post('ajax/dashboard', 		'AjaxClientesController@ajaxDashboard');
		Route::post('ajax/inventario', 		'AjaxClientesController@ajaxInventario');

		/* Perfil Usuario */
		Route::get('perfil', 	'PerfilController@showPerfil');
		Route::put('perfil',	'PerfilController@updatePerfil');

		/* Perfil Comercial */
		Route::get('/informacion-comercial', 'PerfilController@indexInfoComercial')->name('indexInfoComercial');

		/* Establecimientos */
		Route::get('/establecimientos', 'EstablecimientosClienteController@index')->name('index');
		Route::get('/establecimientos/{id}', 'EstablecimientosClienteController@show')->name('show');
		Route::post('establecimientos/{id}/realizar-corte', 'EstablecimientosClienteController@realizarCorte');
		Route::post('establecimientos/{id}/realizar-corte2', 'EstablecimientosClienteController@realizarCorte2');
		Route::resource('establecimientos','EstablecimientosClienteController', ['only' => ['index', 'show']]);

		/* Pedidos */
		Route::get('/pedidos', 'PedidosController@index')->name('index');
		Route::get('/pedidos/{id}', 'PedidosController@show')->name('show');
		Route::get('pedidos/{id}/remision',	 'PedidosController@generaRemision');
		Route::resource('pedidos','PedidosController', ['only' => ['index','show']]);

	 	/* REPORTES */
	 	Route::resource('reportes', 'ReportesController');
	 	Route::resource('reportes/generar', 'ReportesController@generarReporte');
	 	
	}); 

});