<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->integer('establecimiento_id')->unsigned();
            $table->integer('pedido_id')->unsigned();
            $table->string('folio')->nullable();
            $table->timestamp('fecha_facturacion')->nullable();
            $table->timestamp('fecha_vencimiento')->nullable();
            $table->decimal('subtotal', 15, 2);
            $table->decimal('iva', 15, 2);
            $table->decimal('total', 15, 2);
            $table->text('pdf_path')->nullable();
            $table->text('xml_path')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('establecimiento_id')->references('id')->on('cliente_establecimiento');
            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_facturas');
    }
}
