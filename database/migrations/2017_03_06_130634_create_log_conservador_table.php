
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogConservadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_conservador', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('conservador_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('tipo')->nullable(); //status, mantenimiento
            $table->string('comentarios')->nullable(); //comentarios
            $table->timestamps();

            $table->foreign('conservador_id')->references('id')->on('conservador');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_conservador');
    }
}
