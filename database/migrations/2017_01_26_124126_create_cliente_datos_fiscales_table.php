<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteDatosFiscalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_datos_fiscales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->string('persona');
            $table->string('razon_social');
            $table->string('representante_legal');
            $table->string('rfc');
            $table->string('calle');
            $table->string('numero_interior')->nullable();
            $table->string('numero_exterior');
            $table->string('colonia');
            $table->string('codigo_postal')->nullable();
            $table->string('municipio');
            $table->string('estado');
            $table->string('pais')->nullable()->default('México');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_datos_fiscales');
    }
}
