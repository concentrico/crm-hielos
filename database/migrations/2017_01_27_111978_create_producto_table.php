<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
    {
       Schema::create('producto', function (Blueprint $table) {
          $table->increments('id');
          $table->string('id_codigo')->unique()->nullable();
          $table->string('tipo'); //producto - servicio
          $table->integer('es_hielo')->nullable();
          $table->string('nombre');
          $table->decimal('precio_venta', 20, 2);
          $table->decimal('peso', 20, 2)->nullable(); //peso en kg (hielos)
          $table->text('comentarios')->nullable();
          $table->decimal('iva', 20, 2);
          $table->timestamps();
          $table->softDeletes();
          $table->integer('created_by')->unsigned();
          $table->integer('updated_by')->unsigned();

          $table->foreign('created_by')->references('id')->on('users');
          $table->foreign('updated_by')->references('id')->on('users');
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::dropIfExists('producto');
   }

}