<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('parametro', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('capacidad_cuartofrio', 20, 2)->nullable();
            $table->integer('produccion_diaria')->nullable();
            $table->string('site_name')->nullable()->default('CRM Hielos');
            $table->string('site_logo')->nullable();
            $table->string('site_phone')->nullable();
            $table->string('site_url')->nullable();
            $table->string('site_email')->nullable();
            $table->string('google_maps_api_key')->nullable();
            $table->string('google_recaptcha_api_key')->nullable();
            $table->timestamps();
        });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
  public function down()
    {
        Schema::dropIfExists('parametro');
    }
}

