<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConservadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conservador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_codigo')->unique()->nullable();
            $table->integer('cliente_id')->unsigned()->nullable();
            $table->integer('cliente_establecimiento_id')->unsigned()->nullable();
            $table->integer('modelo_id')->unsigned()->nullable();
            $table->string('numero_serie')->nullable()->unique();
            $table->integer('producto_id')->unsigned()->nullable();
            //$table->integer('capacidad')->nullable();
            $table->string('status');
            $table->string('frecuencia_mantenimiento');
            $table->timestamp('ultimo_mantenimiento')->nullable();
            //$table->timestamp('ultimo_corte')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            /*La capacidad será en vase a la sumatoria de la capacidad de la tabla pivote*/
            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('cliente_establecimiento_id')->references('id')->on('cliente_establecimiento');
            $table->foreign('modelo_id')->references('id')->on('modelo_conservador');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conservador');
    }
}
