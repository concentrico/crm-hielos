<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteEstablecimientoHasTipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_establecimiento_has_tipo', function (Blueprint $table) {
            $table->integer('establecimiento_id')->unsigned()->nullable();
            $table->integer('tipo_est_id')->unsigned()->nullable();
            $table->timestamps();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('establecimiento_id')->references('id')->on('cliente_establecimiento');
            $table->foreign('tipo_est_id')->references('id')->on('cliente_tipo_establecimiento');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_establecimiento_has_tipo');
    }
}
