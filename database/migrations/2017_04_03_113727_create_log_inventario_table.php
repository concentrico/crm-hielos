<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogInventarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_inventario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventario_id')->unsigned();
            $table->integer('entrada_id')->unsigned()->nullable();
            $table->integer('salida_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('log')->nullable(); //log text
            $table->text('comentarios')->nullable(); //comentarios cambio
            $table->timestamps();

            $table->foreign('inventario_id')->references('id')->on('inventario');
            $table->foreign('entrada_id')->references('id')->on('inventario_entradas');
            $table->foreign('salida_id')->references('id')->on('inventario_salidas');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_inventario');
    }
}
