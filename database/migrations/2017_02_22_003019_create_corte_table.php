<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corte', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('conservador_id')->unsigned()->nullable();
            //$table->integer('cuartofrio_id')->unsigned()->nullable();
            $table->integer('establecimiento_id')->unsigned()->nullable();
            $table->integer('producto_id')->unsigned();
            $table->integer('cantidad_actual')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('establecimiento_id')->references('id')->on('cliente_establecimiento');
            //$table->foreign('conservador_id')->references('id')->on('conservador');
            //$table->foreign('cuartofrio_id')->references('id')->on('establecimiento_cuartofrio');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corte');
    }
}
