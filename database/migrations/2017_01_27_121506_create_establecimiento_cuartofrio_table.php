<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablecimientoCuartoFrioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::disableForeignKeyConstraints();
        Schema::create('establecimiento_cuartofrio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique()->nullable();
            //$table->integer('cliente_id')->unsigned()->nullable();
            //$table->integer('establecimiento_id')->unsigned()->nullable();
            $table->timestamp('ultimo_corte')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            /*La capacidad será en vase a la sumatoria de la capacidad de la tabla pivote*/
            //$table->foreign('cliente_id')->references('id')->on('cliente');
            //$table->foreign('establecimiento_id')->references('id')->on('cliente_establecimiento');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('establecimiento_cuartofrio');
    }
}
