<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_corto');
            $table->boolean('es_preferencial')->default(false);
            $table->string('tipo');
            $table->text('comentarios')->nullable();
            $table->integer('forma_pago_id')->unsigned()->nullable();
            $table->integer('condicion_pago_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            $table->foreign('condicion_pago_id')->references('id')->on('condicion_pago');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
