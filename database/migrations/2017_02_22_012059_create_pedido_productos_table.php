<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedido_id')->unsigned()->nullable();
            $table->integer('producto_id')->unsigned()->nullable();
            //$table->integer('conservador_id')->unsigned()->nullable();
            //$table->integer('cuartofrio_id')->unsigned()->nullable();
            //$table->integer('capacidad_actual');
            $table->integer('cantidad_requerida');
            $table->integer('cantidad_recibida')->nullable();
            $table->decimal('precio_unitario', 20, 2);
            $table->decimal('subtotal', 20, 2);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->foreign('producto_id')->references('id')->on('producto');
            //$table->foreign('conservador_id')->references('id')->on('conservador');
            //$table->foreign('cuartofrio_id')->references('id')->on('establecimiento_cuartofrio');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_productos');
    }
}
