<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteContactoHasFuncionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_contacto_has_funcion', function (Blueprint $table) {
            $table->integer('cliente_contacto_id')->unsigned()->nullable();
            $table->integer('cliente_funcion_id')->unsigned()->nullable();
            $table->timestamps();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('cliente_contacto_id')->references('id')->on('cliente_contacto');
            $table->foreign('cliente_funcion_id')->references('id')->on('cliente_contacto_funcion');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_contacto_has_funcion');
    }
}
