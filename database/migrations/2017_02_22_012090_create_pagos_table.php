<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedido_id')->unsigned();
            $table->timestamp('fecha_pago')->nullable();
            $table->decimal('monto', 20, 2)->nullable();
            $table->integer('forma_pago_id')->unsigned()->nullable();
            $table->boolean('facturado')->default(false);
            $table->string('status')->nullable(); //pago_parcial,liquidado,cancelado
            $table->string('comprobante_path')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
