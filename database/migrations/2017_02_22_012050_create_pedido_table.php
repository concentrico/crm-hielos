<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned()->nullable();
            $table->integer('cliente_establecimiento_id')->unsigned()->nullable();
            $table->timestamp('fecha_entrega')->nullable();
            $table->timestamp('entregado_at')->nullable();
            $table->time('hora_deseada')->nullable();
            $table->decimal('subtotal', 20, 2)->nullable();
            $table->decimal('iva', 20, 2)->nullable();
            $table->decimal('total', 20, 2)->nullable();
            $table->boolean('incobrable')->default(false);
            $table->integer('forma_pago_id')->unsigned()->nullable();
            $table->integer('condicion_pago_id')->unsigned()->nullable();
            $table->integer('chofer_id')->unsigned()->nullable();
            $table->integer('status')->default(1); //1:Programado, 2:En ruta, 3:Entregado, 4:Devolución, 5:Cancelado
            $table->integer('status_cobranza')->default(1)->nullable(); //1: Pendiente, 2: Pago Parcial, 3: Liquidado, 4: Incobrable
            $table->integer('status_facturacion')->default(1)->nullable(); //1: No Facturado, 2: Faturado Parcialmente, 4: No Facturable
            $table->string('firma_path')->nullable();
            $table->string('remision_path')->nullable();
            $table->integer('firmado_por')->unsigned()->nullable();
            $table->string('firmado_otro')->nullable();
            //$table->integer('created_by')->unsigned();

            $table->boolean('es_evento')->default(false);
            $table->string('evento_latitud_longitud')->nullable();
            $table->string('evento_calle')->nullable();
            $table->string('evento_numero_interior')->nullable();
            $table->string('evento_numero_exterior')->nullable();
            $table->string('evento_colonia')->nullable();
            $table->string('evento_codigo_postal')->nullable();
            $table->string('evento_municipio')->nullable();
            $table->string('evento_estado')->nullable();
            $table->text('evento_direccion_indicaciones')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();

            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('cliente_establecimiento_id')->references('id')->on('cliente_establecimiento');
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            $table->foreign('condicion_pago_id')->references('id')->on('condicion_pago');
            $table->foreign('chofer_id')->references('id')->on('users');
            $table->foreign('firmado_por')->references('id')->on('cliente_contacto');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido');
    }
}
