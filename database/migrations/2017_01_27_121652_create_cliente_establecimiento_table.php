<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteEstablecimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_establecimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->integer('cliente_zona_id')->unsigned()->nullable();
            $table->integer('cliente_datos_fiscales_id')->unsigned()->nullable();
            $table->integer('forma_pago_id')->unsigned()->nullable();
            $table->integer('condicion_pago_id')->unsigned()->nullable();
            $table->string('dias_entrega')->nullable();     //lunes,martes,... "cualquiera"

            $table->integer('capacidad_actual')->nullable();
            $table->integer('capacidad_maxima')->nullable();
            $table->integer('capacidad_minima')->nullable();
            $table->integer('punto_reorden')->nullable();
            $table->integer('status_inventario')->nullable(); //0 minimo, 1 punto reorden, 2 maximo

            $table->integer('cuartofrio_id')->unsigned()->nullable();

            $table->string('sucursal');
            $table->string('calle');
            $table->string('numero_interior');
            $table->string('numero_exterior')->nullable();
            $table->string('colonia');
            $table->string('codigo_postal')->nullable();
            $table->string('municipio');
            $table->string('estado')->nullable();

            //For google maps
            $table->string('latitud_longitud')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();


            $table->foreign('cuartofrio_id')->references('id')->on('establecimiento_cuartofrio');
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            $table->foreign('condicion_pago_id')->references('id')->on('condicion_pago');
            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('cliente_zona_id')->references('id')->on('cliente_zona');
            $table->foreign('cliente_datos_fiscales_id')->references('id')->on('cliente_datos_fiscales');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_establecimiento');
    }
}
