<?php

use Illuminate\Database\Seeder;

use App\Models\FormaPago as FormaPago;

class FormaPagoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array(
		    [	
		    	'nombre' => 'Efectivo', 
		    ],
		    [	
		    	'nombre' => 'Transferencia', 
		    ],
		    [	
		    	'nombre' => 'Cheque', 
		    ],
		    [	
		    	'nombre' => 'Tarjeta de Crédito', 
		    ],
	    );
	    
	    foreach($items as $i){
			$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);

		    $forma = new FormaPago;
		    $forma->nombre = $i['nombre'];
		    $forma->created_by = $c_by;
		    $forma->updated_by = $u_by;
		    $forma->save();
	    }
    }
}
