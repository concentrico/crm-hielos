<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\Inventario;

class InventarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productos = Producto::all()->pluck('id')->toArray();

        for ($i=0; $i < count($productos); $i++) { 
			$faker = Faker\Factory::create();

			$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);
		    
		    $inventario = new Inventario;
		    $inventario->producto_id = $productos[$i];
		    $inventario->cantidad_disponible = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->cantidad_producir = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->ultima_salida = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->created_by = $c_by;
		    $inventario->updated_by = $u_by;
		    $inventario->save();


			echo 'Inventario ID: '.$inventario->id.' (Producto '.$productos[$i].')'.PHP_EOL;
		}

		$inventarios = Inventario::all()->pluck('id')->toArray();

    	for ($i=0; $i < count($inventarios); $i++) { 
			$faker = Faker\Factory::create();

			$inventario = Inventario::find($inventarios[$i]);
			$num_entradas = rand(1, 100);
			for ($j=0; $j < $num_entradas; $j++) { 
				$entradas = $inventario->entradas()->save(factory(App\Models\InventarioEntradas::class)->make());
			}
			echo 'Entradas (Inventario '.$inventarios[$i]. ') : '.$num_entradas. PHP_EOL;

			$num_salidas = rand(1, 100);
			for ($k=0; $k < $num_salidas; $k++) { 
				$salidas = $inventario->entradas()->save(factory(App\Models\InventarioSalidas::class)->make());
			}
			echo 'Salidas (Inventario '.$inventarios[$i]. ') : '.$num_salidas. PHP_EOL;
		}

    }
}
