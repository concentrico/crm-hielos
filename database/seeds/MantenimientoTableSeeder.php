<?php

use Illuminate\Database\Seeder;

class MantenimientoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Mantenimiento::class, 50)->create()->each(function ($mantenimiento) {
			echo 'Mantenimiento: '.$mantenimiento->id.' (Conservador '.$mantenimiento->conservador_id.')'. PHP_EOL;
	    });
    }
}
