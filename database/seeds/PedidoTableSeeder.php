<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\Cliente;

class PedidoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pedido::class, 100)->create()->each(function ($pedido) {
            $faker = Faker\Factory::create();

            // PRODUCTOS PEDIDO
            $productos_rand = [];
            $num_productos = rand(1, 7);

            $total_pedido = 0.0;
            for($i = 0; $i < $num_productos; $i++){

                if ($pedido->cliente_establecimiento_id != null) {
                    
                    $establecimiento = App\Models\ClienteEstablecimiento::find($pedido->cliente_establecimiento_id);

                    $conservadores = $establecimiento->conservadores()->pluck('id')->toArray();
                    $cuartofrio_id = $establecimiento->cuartofrio_id;

                    if ( count($conservadores) > 0 && $cuartofrio_id == null ) {

                        $conservador_rand_id = $conservadores[rand(0, count($conservadores)-1)];
                        $cuartofrio_rand_id = null;

                        $conservador_rand = App\Models\Conservador::find($conservador_rand_id);
                        $producto_id = $conservador_rand->producto_id;

                    } else if ( count($conservadores) == 0 && $cuartofrio_id != null ) {
                        
                        $conservador_rand_id = null;

                        $cuartofrio = App\Models\CuartoFrio::find($cuartofrio_id);
                        $productos = $cuartofrio->productos()->pluck('producto_id')->toArray();
                        $producto_id = $productos[rand(0, count($productos)-1)];

                    } else if ( count($conservadores) > 0 && $cuartofrio_id != null ) {

                        if ( rand(0, 1) == 1 ) {
                            # Conservador
                            $conservador_rand_id = $conservadores[rand(0, count($conservadores)-1)];
                            $cuartofrio_rand_id = null;
                            $conservador_rand = App\Models\Conservador::find($conservador_rand_id);
                            $producto_id = $conservador_rand->producto_id;
                        } else {
                            # Cuarto Frio Cliente
                            $conservador_rand_id = null;
                            
                            $cuartofrio = App\Models\CuartoFrio::find($cuartofrio_id);
                            $productos = $cuartofrio->productos()->pluck('producto_id')->toArray();
                            $producto_id = $productos[rand(0, count($productos)-1)];
                                
                        }

                    } else {
                        #El establecimiento no tiene ni conservadores ni pedido
                        $producto_id=null;
                        $conservador_rand_id=null;
                        $cuartofrio_rand_id=null;
                    }

                } else {
                    $producto_id = $faker->randomElement(App\Models\Producto::all()->pluck('id')->toArray());
                } 

                $precio = $faker->randomFloat(2, 0, rand(0, rand(100, rand(1000, 50000))));
                $cantidad = $faker->numberBetween($min = 1, $max = 200);
                $subtotal = $precio*$cantidad;

                $total_pedido+=$subtotal;
                if ( !in_array($producto_id, $productos_rand)) {


                    if ($pedido->status == 3 ) {
                        $producto_pedido = [
                        'producto_id'           => $producto_id,
                        'cantidad_requerida'    => $cantidad,
                        'cantidad_recibida'     => $cantidad,
                        'precio_unitario'       => $precio,
                        'subtotal'              => $subtotal,
                        'created_by'            => $pedido->created_by,
                        'updated_by'            => $pedido->updated_by,
                    ];
                    } else {
                        $producto_pedido = [
                        'producto_id'           => $producto_id,
                        'cantidad_requerida'    => $cantidad,
                        'precio_unitario'       => $precio,
                        'subtotal'              => $subtotal,
                        'created_by'            => $pedido->created_by,
                        'updated_by'            => $pedido->updated_by,
                    ];
                    }
                    
                    

                    if ($producto_id != null ){
                        $producto = new App\Models\PedidoProductos($producto_pedido);
                        $pedido->productos()->save($producto);
                        $productos_rand[] = $producto_id;
                    } else {
                        #eliminar pedido
                        $pedido->destroy($pedido->id);
                    }
                    
                }
                
            }
            $pedido->total = $total_pedido;
            $pedido->update();
            echo 'Pedido: ' . $pedido->id . PHP_EOL;
        });
    }
}