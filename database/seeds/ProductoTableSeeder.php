<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\ParametroProductos;
use App\Models\Inventario;

class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = array(
		    [	
		    	'id_codigo' 			=> 'BH05KG', 
		    	'tipo' 					=> 'producto', 
		    	'nombre'				=> 'Bolsa de Hielo 5 kg',
		    	'es_hielo'				=> '1',
		    	'precio_venta'			=> '30.00',
		    	'peso'					=> '5.00',
		    	'iva'					=> '0.00',
		    	'cap_max_inventario'	=> '1800',
		    ],
		    [	
		    	'id_codigo' 			=> 'BH10KG', 
		    	'tipo' 					=> 'producto', 
		    	'nombre'				=> 'Bolsa de Hielo 10 kg',
		    	'es_hielo'				=> '1',
		    	'precio_venta'			=> '50.00',
		    	'peso'					=> '10.00',
		    	'iva'					=> '0.00',
		    	'cap_max_inventario'	=> '800',
		    ],
	    );
	    
	    foreach($items as $i){
	    	$faker = Faker\Factory::create();
			$num_usuarios = App\User::count();
			$c_by = 1;
			$u_by = 1;
		    
		    $producto = new Producto;
		    $producto->id_codigo = $i['id_codigo'];
		    $producto->tipo = $i['tipo'];
		    $producto->nombre = $i['nombre'];
		    $producto->es_hielo = $i['es_hielo'];
		    $producto->precio_venta = $i['precio_venta'];
		    $producto->peso = $i['peso'];
		    $producto->iva = $i['iva'];
		    $producto->comentarios = $faker->randomElement(array('', $faker->realText(rand(50, 250))));
		    $producto->created_by = $c_by;
		    $producto->updated_by = $u_by;
		    $producto->save();

		    # Dar de alta en tabla Inventario
            $inventario = new Inventario;
            $inventario->producto_id = $producto->id;
            $inventario->cantidad_disponible = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->cantidad_producir = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->ultima_salida = $faker->numberBetween($min = 10, $max = 2000);
		    $inventario->created_by = $c_by;
		    $inventario->updated_by = $u_by;
            $inventario->save();

            #Dar de alta en Parámetro Produccion
            $capacidad_producto = new ParametroProductos;
            $capacidad_producto->parametro_id = 1;
            $capacidad_producto->producto_id = $producto->id;
            $capacidad_producto->capacidad = $i['cap_max_inventario'];
		    $capacidad_producto->created_by = $c_by;
		    $capacidad_producto->updated_by = $u_by;
            $capacidad_producto->save();

	    }
	    /*
	    factory(App\Models\Producto::class, 30)->create()->each(function ($producto) {
	    	$faker = Faker\Factory::create();
	    	$array_codigos = App\Models\Producto::all()->pluck('id_codigo')->toArray();
            
            if (count($array_codigos > 0)) {
                $id_codigo_rand = $faker->randomNumber(3);
                while (in_array($id_codigo_rand, $array_codigos)) {
                    $id_codigo_rand = $faker->randomNumber(3);
                }
            } else {
                $id_codigo_rand = $faker->randomNumber(3);
            }

            $producto->id_codigo = $id_codigo_rand;
            $producto->update();
			echo 'Fake Product: ' . $producto->id . PHP_EOL;
		});
		*/
    }
}
