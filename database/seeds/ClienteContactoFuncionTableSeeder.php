<?php

use Illuminate\Database\Seeder;
use App\Models\ContactoFuncion as ContactoFuncion;

class ClienteContactoFuncionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacto_funciones = array(
		    [ 'nombre' => 'Contacto Comercial',	'descripcion' => 'Es quien paga y tiene control de todos los Establecimientos (Compras Corporativo).'],
		    [ 'nombre' => 'Gerente Establecimiento',	'descripcion' => 'Es quien tiene control de cierto Establecimiento.'],
		    [ 'nombre' => 'Almacenista',	'descripcion' => 'Es quien recibe el producto y realiza el corte de inventario.'],
	    );
	    
	    foreach($contacto_funciones as $cf){
		    $contacto_funcion = new ContactoFuncion;
		    $contacto_funcion->nombre = $cf['nombre'];
		    $contacto_funcion->descripcion = $cf['descripcion'];
		    $contacto_funcion->created_by = 1;
		    $contacto_funcion->updated_by = 1;
		    $contacto_funcion->save();
	    }
    }
}