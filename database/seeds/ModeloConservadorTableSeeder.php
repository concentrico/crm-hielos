<?php

use Illuminate\Database\Seeder;

class ModeloConservadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ModeloConservador::class, 10)->create()->each(function ($modelo) {
			echo 'Modelo Conservador: ' . $modelo->id . PHP_EOL;
	    });
    }
}
