<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    
    	$faker = Faker\Factory::create();
	    
	    $users = array(
	    	[	
		    	'nombre' => 'Administrator', 
		    	'user_role_id' => array(1), 
		    	'username' => 'admin',
		    	'email' => 'dev@concentrico.com.mx',
		    	'pwd' => Hash::make('secret'),
		    ]
	    );
	    
	    foreach($users as $key => $u){
			$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);

		    $user = new User;
		    $user->tipo = 'sistema';
		    $user->nombre = $u['nombre'];
		    $user->username = $u['username'];
		    $user->email = $u['email'];
		    $user->password = $u['pwd'];
		    
		    if($key > 0){
			    $user->created_by = 1;
			    $user->updated_by = 1;			    
		    }
		    $user->save();

		    foreach($u['user_role_id'] as $rol){
				$user->roles()->attach($rol, ['created_by' => 1, 'updated_by' => 1]);
		    }
		    
	    }
	    
		$user = User::findOrFail(1);
	    $user->created_by = 1;
	    $user->updated_by = 1;			    
		$user->update();

    }
}