<?php

use Illuminate\Database\Seeder;
use App\Models\ClienteTipoEstablecimiento as ClienteTipoEstablecimiento;

class ClienteTipoEstablecimientoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_establecimientos = array(
		    [ 'nombre' => 'Restaurante',],
		    [ 'nombre' => 'Bar',],
		    [ 'nombre' => 'Antro',],
		    [ 'nombre' => 'Salón de Eventos',],
		    [ 'nombre' => 'Eventos Especiales',],
		    [ 'nombre' => 'Otro',],
	    );
	    
	    foreach($tipo_establecimientos as $te){
		    $tipo_est = new ClienteTipoEstablecimiento;
		    $tipo_est->nombre = $te['nombre'];
		    $tipo_est->created_by = 1;
		    $tipo_est->updated_by = 1;
		    $tipo_est->save();
	    }
    }
}