<?php

use Illuminate\Database\Seeder;

class CodigosPostalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = base_path('database/seeds/codigos_postales.sql');
        DB::unprepared(file_get_contents($path));
    }
}
