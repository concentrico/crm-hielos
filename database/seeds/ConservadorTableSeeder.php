<?php

use Illuminate\Database\Seeder;

class ConservadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Conservador::class, 100)->create()->each(function ($conservador) {
            $faker = Faker\Factory::create();

            $array_codigos = App\Models\Conservador::all()->pluck('id_codigo')->toArray();
            
            if (count($array_codigos > 0)) {
                $id_codigo_rand = $faker->randomNumber(3);
                while (in_array($id_codigo_rand, $array_codigos)) {
                    $id_codigo_rand = $faker->randomNumber(3);
                }
            } else {
                $id_codigo_rand = $faker->randomNumber(3);
            }

            $conservador->id_codigo = $id_codigo_rand;
            $conservador->update();

            if ($conservador->cliente_establecimiento_id != null) {
                $establecimiento = App\Models\ClienteEstablecimiento::find($conservador->cliente_establecimiento_id);
                #SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO

                if ($establecimiento->precios->count() > 0) {
                    if( !$establecimiento->precios->pluck('producto_id')->contains($conservador->producto_id) ) {
                        $producto = App\Models\Producto::findOrFail($conservador->producto_id);
                        $precios_producto = [
                            'cliente_id'            => $conservador->cliente_id,
                            'producto_id'           => $conservador->producto_id,
                            'precio_venta'          => $producto->precio_venta,
                            'created_by'            => $conservador->created_by,
                            'updated_by'            => $conservador->created_by,
                        ];
                        $precio_producto_est = new App\Models\EstablecimientoPrecios($precios_producto);
                        $establecimiento->precios()->save($precio_producto_est);
                    }
                } else {
                    $producto = App\Models\Producto::findOrFail($conservador->producto_id);
                    $precios_producto = [
                        'cliente_id'            => $conservador->cliente_id,
                        'producto_id'           => $conservador->producto_id,
                        'precio_venta'          => $producto->precio_venta,
                        'created_by'            => $conservador->created_by,
                        'updated_by'            => $conservador->created_by,
                    ];
                    $precio_producto_est = new App\Models\EstablecimientoPrecios($precios_producto);
                    $establecimiento->precios()->save($precio_producto_est);
                }
            }
			echo 'Conservador: ' . $conservador->id . PHP_EOL;
	    });
    }
}
