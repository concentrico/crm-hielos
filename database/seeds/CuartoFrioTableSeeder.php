<?php

use Illuminate\Database\Seeder;
use App\Models\CuartoFrioProductos;

class CuartoFrioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CuartoFrio::class, 30)->create()->each(function ($cuartofrio) {
	    	$faker = Faker\Factory::create();

            $num_productos = rand(1,5);
            $productos_array = App\Models\Producto::where('es_hielo', '=', '1')->pluck('id')->toArray();

            $productos_rand = [];
            $producto_id = 0;
            for($j = 0; $j < $num_productos; $j++){

                $producto_rand = $productos_array[rand(0, count($productos_array)-1)];
                $producto_id = $producto_rand;
                if ( !in_array($producto_rand, $productos_rand)) {
                    
                    $productos_cuartofrio = [
                        'producto_id'           => $producto_rand,

                        'capacidad'      => $faker->numberBetween($min = 10, $max = 200),

                        'created_by'            => $cuartofrio->created_by,
                        'updated_by'            => $cuartofrio->updated_by,
                    ];
                    $producto = new CuartoFrioProductos($productos_cuartofrio);
                    $cuartofrio->productos()->save($producto);
                    
                    $productos_rand[] = $producto_rand;
                } 
                
            }

            #Asignarlo a un establecimiento

            $establecimientos = App\Models\ClienteEstablecimiento::where('cuartofrio_id', null)->pluck('id')->toArray();
            
            $establecimiento = App\Models\ClienteEstablecimiento::find($faker->randomElement($establecimientos));
            $establecimiento->cuartofrio_id = $cuartofrio->id;
            $establecimiento->update();

            foreach ($cuartofrio->productos() as $cuartofrio_producto) {
                $producto = App\Models\Producto::find($cuartofrio_producto->producto_id);

                if ($establecimiento->precios->count() > 0) {
                    #SI NO EXISTE EL PRODUCTO EN LA TABLA PRECIOS DEL ESTABLECIMIENTO
                    if( !$establecimiento->precios->pluck('producto_id')->contains($producto->id) ) {
                        $precios_producto = [
                            'cliente_id'            => $establecimiento->cliente_id,
                            'producto_id'           => $producto->id,
                            'precio_venta'          => $producto->precio_venta,
                            'created_by'            => $cuartofrio->created_by,
                            'updated_by'            => $cuartofrio->created_by,
                        ];
                        $precio_producto_est = new App\Models\EstablecimientoPrecios($precios_producto);
                        $establecimiento->precios()->save($precio_producto_est);
                        #Actualizar Puntos de inventario
                        $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
                        $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
                        $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
                        $establecimiento->update();
                    }
                } else {
                    $precios_producto = [
                        'cliente_id'            => $establecimiento->cliente_id,
                        'producto_id'           => $producto->id,
                        'precio_venta'          => $producto->precio_venta,
                        'created_by'            => $cuartofrio->created_by,
                        'updated_by'            => $cuartofrio->created_by,
                    ];
                    $precio_producto_est = new App\Models\EstablecimientoPrecios($precios_producto);
                    $establecimiento->precios()->save($precio_producto_est);
                    #Actualizar Puntos de inventario
                    $establecimiento->capacidad_minima = $establecimiento->getCapacidadRecomendada('minima');
                    $establecimiento->capacidad_maxima = $establecimiento->getCapacidadRecomendada('maxima');
                    $establecimiento->punto_reorden = $establecimiento->getCapacidadRecomendada('punto_reorden');
                    $establecimiento->update();
                }
                
            }

            echo 'Cuarto Frio Establecimiento: ' . $cuartofrio->id . PHP_EOL;
	    });
    }
}
