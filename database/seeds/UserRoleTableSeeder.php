<?php

use Illuminate\Database\Seeder;

use App\Models\UserRole as UserRole;
use Carbon\Carbon;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $user_roles = array(
		    [ 'nombre' => 'Administrador', 			'descripcion' => 'Puede realizar y visualizar todo en el sistema.', 'deleted' => 0],
		    [ 'nombre' => 'Gerente', 	'descripcion' => 'Puede realizar y visualizar todo en el sistema, a excepción de usuarios.', 'deleted' => 0],
		    [ 'nombre' => 'Producción', 	'descripcion' => 'Puede acceder a inventario y acceso a Dashboard Producción.', 'deleted' => 1],
		    [ 'nombre' => 'Chofer', 	'descripcion' => 'Módulo de Choferes para entrega de pedidos.', 'deleted' => 0],
		    [ 'nombre' => 'Contacto Cliente', 'descripcion' => 'Firma Remisión.', 'deleted' => 1],
	    );
	    
	    foreach($user_roles as $ut){
		    $user_role = new UserRole;
		    $user_role->nombre = $ut['nombre'];
		    $user_role->descripcion = $ut['descripcion'];
		    if ($ut['deleted']) {
		    	$user_role->deleted_at = Carbon::now();
		    }
		    $user_role->save();
	    }
    }
}
