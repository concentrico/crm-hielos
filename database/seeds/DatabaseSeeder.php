<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ParametroTableSeeder::class);
        $this->call(CodigosPostalesTableSeeder::class);
        $this->call(FormaPagoTableSeeder::class);
        $this->call(CondicionPagoTableSeeder::class);
        $this->call(ClienteZonaTableSeeder::class);
        //$this->call(ClienteContactoFuncionTableSeeder::class);
        $this->call(ClienteTipoEstablecimientoTableSeeder::class);
        $this->call(ClienteTableSeeder::class);
        $this->call(ProductoTableSeeder::class);
        $this->call(ModeloConservadorTableSeeder::class);
        $this->call(ConservadorTableSeeder::class);
        //$this->call(CuartoFrioTableSeeder::class);
        $this->call(ChoferesSeeder::class);
        //$this->call(InventarioTableSeeder::class);
        //$this->call(CorteTableSeeder::class);
        $this->call(PedidoTableSeeder::class);
        $this->call(PagosTableSeeder::class);
        $this->call(MantenimientoTableSeeder::class);
    }
}
