<?php

use Illuminate\Database\Seeder;
use App\Models\Pedido;
use App\Models\Producto;
use App\Models\Cliente;

class PagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	factory(App\Models\Pago::class, 100)->create()->each(function ($pago) {
			echo 'Pago ID: ' . $pago->id . ' (Pedido '.$pago->pedido_id.')'.PHP_EOL;
		});
    }
}
