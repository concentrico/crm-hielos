<?php

use Illuminate\Database\Seeder;

class CorteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Corte::class, 50)->create()->each(function ($corte) {
			echo 'Corte Inventario: ' . $corte->id . PHP_EOL;
	    });
    }
}
