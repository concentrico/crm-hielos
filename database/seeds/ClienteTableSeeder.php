<?php

use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory(App\Models\Cliente::class, 50)->create()->each(function ($cliente) {
			echo 'Cliente: ' . $cliente->id . PHP_EOL;
	    	$faker = Faker\Factory::create();

			// DATOS FISCALES
			$num_datos_fiscales = rand(1, 4);
		    for($i = 0; $i < $num_datos_fiscales; $i++){
		        $cliente->datos_fiscales()->save(factory(App\Models\ClienteDatosFiscales::class)->make());
		    }

		    if ($cliente->tipo == 'horeca') {
			    // ESTABLECIMIENTOS
			    $num_establecimientos = rand(1,4);
			    for($i = 0; $i < $num_establecimientos; $i++){
		        	$establecimiento = $cliente->establecimientos()->save(factory(App\Models\ClienteEstablecimiento::class)->make());
		        	
		        	//Tipo de Establecimiento
					$tipo_est_arr = App\Models\ClienteTipoEstablecimiento::all()->pluck('id')->toArray();
			    	$num_tipos_est = rand(1,3);
			    	for($i = 0; $i < $num_tipos_est; $i++){
			    		$establecimiento->tipos()->attach($tipo_est_arr[rand(0, count($tipo_est_arr)-1)], ['created_by' => $establecimiento->created_by, 'updated_by' => $establecimiento->created_by]);
			    	}

			    	//Zona
			    	$zona_arr = App\Models\ClienteZona::all()->pluck('id')->toArray();
			    	$establecimiento->cliente_zona_id = $faker->randomElement($zona_arr);

			    	/*Datos Fiscales*/
			    	$datos_fiscales = $cliente->datos_fiscales()->pluck('id')->toArray();
			    	$establecimiento->cliente_datos_fiscales_id = $faker->randomElement($datos_fiscales);
			    	$establecimiento->update();

			    }
		    }

		    // CONTACTOS
		    $num_contactos = rand(1,5);
		    for($i = 0; $i < $num_contactos; $i++){
		    	$contacto = $cliente->contactos()->save(factory(App\Models\ClienteContacto::class)->make());
		    	
		    	if ($cliente->tipo == 'horeca') {
			    	//Contacto - Establecimiento
			    	if ( rand(0,1) ) {
			    		$est_arr = $cliente->establecimientos()->pluck('id')->toArray();
				    	$num_est = rand(1,4);
				    	for($i = 0; $i < $num_est; $i++){
				    		$contacto->establecimientos()->syncWithoutDetaching([$est_arr[rand(0, count($est_arr)-1)] => ['created_by' => $contacto->created_by, 'updated_by' => $contacto->created_by]]);
				    	}
			    	}
		    	}

				//CORREOS
				$num_emails = rand(0,3);
			    for($j = 0; $j < $num_emails; $j++){
		        	$contacto->emails()->save(factory(App\Models\ClienteContactoEmail::class)->make());
			    }

				//TELEFONOS
				$num_telefonos = rand(0,3);
			    for($j = 0; $j < $num_telefonos; $j++){
		        	$contacto->telefonos()->save(factory(App\Models\ClienteContactoTelefono::class)->make());
			    }
		    }
		    
	    });
    }
}
