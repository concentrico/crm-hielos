<?php

use Illuminate\Database\Seeder;
use App\Models\Parametro;


class ParametroTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $parametros = new Parametro;
        $parametros->capacidad_cuartofrio = 100000; //100 Toneladas
	    $parametros->produccion_diaria = 1000; // kilos por hora
        $parametros->google_maps_api_key = 'AIzaSyCfhLncymbc6xKthyErdcG4RW0wFvnPr9g';
        $parametros->google_recaptcha_api_key = '6LfrLlQUAAAAAJiRmXtQ3Mg0EU27m2F3R5uFihA-';
	    $parametros->save();
    }
}
