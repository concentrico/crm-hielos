<?php

use Illuminate\Database\Seeder;
use App\Models\ClienteZona as ClienteZona;

class ClienteZonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array(
		    [	
		    	'nombre' 			=> 'Centrito Valle', 
		    	'descripcion'		=> null
		    ],
		    [	
		    	'nombre' 			=> 'Barrio Antiguo', 
		    	'descripcion'		=> null
		    ],
		    [	
		    	'nombre' 			=> 'Valle Oriente', 
		    	'descripcion'		=> null
		    ],
		    [	
		    	'nombre' 			=> 'Carretera Nacional', 
		    	'descripcion'		=> null
		    ],
		    [	
		    	'nombre' 			=> 'Centro Monterrey', 
		    	'descripcion'		=> null
		    ],
		    [	
		    	'nombre' 			=> 'Cumbres', 
		    	'descripcion'		=> null
		    ],
	    );
	    
	    foreach($items as $i){
			$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);

		    $zona = new ClienteZona;
		    $zona->nombre = $i['nombre'];
		    $zona->descripcion = $i['descripcion'];
		    $zona->created_by = $c_by;
		    $zona->updated_by = $u_by;
		    $zona->save();
	    }
    }
}