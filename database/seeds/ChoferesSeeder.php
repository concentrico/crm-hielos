<?php

use Illuminate\Database\Seeder;

class ChoferesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) { 
        	$faker = Faker\Factory::create();
        	$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);

		    $user = new App\User;

		    $user->nombre = $faker->name;
		    $user->username = $faker->unique()->userName;
		    $user->email = $faker->unique()->safeEmail;
		    $user->password = Hash::make('secret');
		    $user->fecha_nacimiento = $faker->dateTimeThisCentury->format('d/m/Y');
		    $user->telefono = '(81)'.$faker->phoneNumber();
		    $user->celular = $faker->tollFreePhoneNumber();
		    $user->created_by = 1;
		    $user->updated_by = 1;

		    $user->save();
			$user->roles()->attach(4, ['created_by' => 1, 'updated_by' => 1]);
			echo 'Chofer: ' . $user->id . PHP_EOL;
        }
    }
}
