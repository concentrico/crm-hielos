<?php

use Illuminate\Database\Seeder;

use App\Models\CondicionPago as CondicionPago;

class CondicionPagoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array(
		    [	
		    	'nombre' 	=> 'Contado', 
		    	'dias'		=> 0
		    ],
		    [	
		    	'nombre' 	=> '30 Días', 
		    	'dias'		=> 30
		    ],
		    [	
		    	'nombre' 	=> '15 Días', 
		    	'dias'		=> 15
		    ],
	    );
	    
	    foreach($items as $i){
			$num_usuarios = App\User::count();
			$c_by = rand(1, $num_usuarios);
			$u_by = rand(1, $num_usuarios);

		    $condicion = new CondicionPago;
		    $condicion->nombre = $i['nombre'];
		    $condicion->dias = $i['dias'];
		    $condicion->created_by = $c_by;
		    $condicion->updated_by = $u_by;
		    $condicion->save();
	    }
    }
}
