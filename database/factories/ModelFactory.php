<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'fecha_nacimiento' => $faker->dateTimeThisCentury->format('d-m-Y'),
    ];
});

/** Cliente */
$factory->define(App\Models\Cliente::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    return [
	    'nombre_corto' 		=> $faker->company . ( $faker->randomElement(array('', $faker->companySuffix))),
	    'tipo' 				=> $faker->randomElement(array('standard','horeca')),
	    'es_preferencial' 	=> rand(0,1),
	    'comentarios' 		=> ( $faker->randomElement(array('', $faker->realText(rand(50, 300))))),
        'created_by' 		=> $c_by,
        'updated_by' 		=> $u_by,
    ];
});

/** ClienteEstablecimiento */
$factory->define(App\Models\ClienteEstablecimiento::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);
	
	$ejemplos_establecimientos = array('Matriz', 'Centro', 'San Pedro', 'Apodaca', 'Cumbres', 'San Nicolás', 'Guadalupe', 'Escobedo', 'García');

	$formas_pago = App\Models\FormaPago::count();
	$forma_pago_id = rand(1, $formas_pago);

	$condiciones_pago = App\Models\CondicionPago::count();
	$condicion_pago_id = rand(1, $condiciones_pago);

    return [
	    'sucursal' 					=> $faker->randomElement($ejemplos_establecimientos),
	    'forma_pago_id'				=> $forma_pago_id,
	    'condicion_pago_id' 		=> $condicion_pago_id,
	    'calle' 					=> $faker->streetName,
	    'numero_exterior' 			=> $faker->buildingNumber,
	    'numero_interior' 			=> rand(1,10),
	    'colonia' 					=> $faker->lastName,
	    //'codigo_postal' 			=> $faker->postcode,
	    'codigo_postal' 			=> $faker->randomNumber(6),
	    'municipio' 				=> $faker->city,
	    'estado' 					=> $faker->state,
	    'comentarios' 				=> $faker->randomElement(array('', $faker->realText(rand(50, 300)))),
        'created_by' 				=> $c_by,
        'updated_by' 				=> $u_by,
    ];
});

/** ClienteContacto */
$factory->define(App\Models\ClienteContacto::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    return [
	    'nombre' 					=> $faker->firstName,
	    'apellido_paterno'			=> $faker->lastName,
	    'apellido_materno' 			=> $faker->lastName,
	    'comentarios' 				=> $faker->randomElement(array('', $faker->realText(rand(50, 300)))),
        'created_by' 				=> $c_by,
        'updated_by' 				=> $u_by,
    ];
});

/** ClienteContactoEmail */
$factory->define(App\Models\ClienteContactoEmail::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);


    return [
	    'tipo' 			=> $faker->randomElement(array('trabajo', 'personal')),
	    'valor' 		=> $faker->safeEmail,
        'created_by' 	=> $c_by,
        'updated_by' 	=> $u_by,
    ];
});

/** ClienteContactoTelefono */
$factory->define(App\Models\ClienteContactoTelefono::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	$aux_telefono = $faker->phoneNumber;
	
	$aux_telefono = explode('x', $aux_telefono);

    return [
	    'tipo' 			=> $faker->randomElement(array('celular', 'casa', 'oficina')),
	    'valor' 		=> $aux_telefono[0],
	    'extension' 	=> (isset($aux_telefono[1]) ? $aux_telefono[1] : null),
        'created_by' 	=> $c_by,
        'updated_by' 	=> $u_by,
    ];
});

/** ClienteDatosFiscales */
$factory->define(App\Models\ClienteDatosFiscales::class, function (Faker\Generator $faker) {
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	$aux_persona = $faker->randomElement(array('fisica', 'moral'));
	
	if($aux_persona == 'fisica'){
		$aux_razon_social = $faker->name;
	}else{
		$aux_razon_social = $faker->company;
	}
	
	$aux_rfc = "AAAA010101AAA";

    return [
	    'persona' 				=> $aux_persona,
	    'razon_social'			=> $aux_razon_social,
	    'representante_legal'	=> $faker->name,
	    'rfc' 					=> $aux_rfc,
	    'calle' 				=> $faker->streetName,
	    'numero_exterior' 		=> $faker->buildingNumber,
	    'numero_interior' 		=> rand(1,10),
	    'colonia' 				=> $faker->lastName,
	    //'codigo_postal' 		=> $faker->postcode,
	    'codigo_postal' 		=> $faker->randomNumber(6),
	    'municipio' 			=> $faker->city,
	    'estado' 				=> $faker->state,
	    'pais' 					=> $faker->country,
        'created_by' 			=> $c_by,
        'updated_by' 			=> $u_by,
    ];
});

/** Producto */
$factory->define(App\Models\Producto::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	$tipo_rand = $faker->randomElement(['producto', 'servicio']);
	
	if ($tipo_rand == 'producto') 
		$es_hielo_rand = $faker->randomElement([0, 1]);
	else
		$es_hielo_rand = 0;

	if ($es_hielo_rand == 1) {
		$peso_rand = $faker->randomNumber(2);
		$nombre_rand = 'Bolsa de Hielo '.$peso_rand.' Kg';
	} else {
		$nombre_rand = $faker->sentence(2, true);
		$peso_rand = 0.0;
	}

    return [
	    'tipo'						=> $tipo_rand,
	    'nombre' 					=> $nombre_rand,
	    'es_hielo'					=> $es_hielo_rand,
	    'precio_venta' 				=> $faker->randomFloat(2, 0, rand(0, rand(100, rand(1000, 50000)))),
	    'peso' 						=> $peso_rand,
	    'iva' 						=> $faker->randomElement([0.16, 0.00]),
	    'comentarios' 				=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
        'created_by' 				=> $c_by,
        'updated_by' 				=> $u_by,
    ];
});

/** Modelo Conservador */
$factory->define(App\Models\ModeloConservador::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	$productos_array = App\Models\Producto::where('es_hielo', '=', '1')->pluck('id')->toArray();
	
    return [
	    'codigo'					=> $faker->ean8(),
	    'modelo'					=> $faker->numerify('XXX ###'),
	    'producto_id'				=> $productos_array[rand(0, count($productos_array)-1)],
	    'capacidad'					=> $faker->numberBetween($min = 200, $max = 400),
        'created_by' 				=> $c_by,
        'updated_by' 				=> $u_by,
    ];
});

/** Conservador */
$factory->define(App\Models\Conservador::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    $clientes = App\Models\Cliente::where('tipo','horeca')->pluck('id')->toArray();
    $cliente_id = $faker->randomElement($clientes);

	$cliente = App\Models\Cliente::find($cliente_id);
	$est_arr = $cliente->establecimientos()->pluck('id')->toArray();
	$establecimiento = App\Models\ClienteEstablecimiento::find($est_arr[rand(0, count($est_arr)-1)]);

	// MODELO CONSERVADOR
    $modelos_conservador = App\Models\ModeloConservador::all()->pluck('id')->toArray();
    $modelo_rand = App\Models\ModeloConservador::find($faker->randomElement($modelos_conservador));

    $ult_mtto_offset = rand(1, 30);
	$ult_corte_offset = rand(1, 10);
    
    $capacidad_rand = $faker->numberBetween($min = ($modelo_rand->capacidad-($modelo_rand->capacidad/3)), $max = $modelo_rand->capacidad);

    $status_rand = $faker->randomElement(['stand_by','en_consignacion','en_reparacion','fuera_servicio']);

    if ($status_rand != 'en_consignacion') {
    	$capacidad_rand = 0;
    }

    if ( $status_rand == 'stand_by' || $status_rand == 'fuera_servicio' ) {
    	return [
		    'cliente_id'					=> null,
		    'cliente_establecimiento_id'	=> null, 
		    'modelo_id'						=> $modelo_rand->id,
		    'producto_id'					=> $modelo_rand->producto_id,
		    //'capacidad_actual'				=> null,
		    'numero_serie'					=> $faker->ean13(),
		    'status' 						=> $status_rand,
		    'frecuencia_mantenimiento'		=> $faker->randomElement([15, 30]),
		    'ultimo_mantenimiento' 			=> null,
		    //'ultimo_corte' 					=> null,
		    'comentarios' 					=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 					=> $c_by,
	        'updated_by' 					=> $u_by,
	    ];
    } else {

    	return [
		    'cliente_id'					=> $cliente->id,
		    'cliente_establecimiento_id'	=> $establecimiento->id,
		    'modelo_id'						=> $modelo_rand->id,
		    'producto_id'					=> $modelo_rand->producto_id,
		    //'capacidad_actual'				=> $capacidad_rand,
		    'numero_serie'					=> $faker->ean13(),
		    'status' 						=> $status_rand,
		    'frecuencia_mantenimiento'		=> $faker->randomElement([15, 30]),
		    'ultimo_mantenimiento' 			=> $faker->dateTimeBetween($startDate = '-'.$ult_mtto_offset.' days', $endDate = 'now', $timezone = date_default_timezone_get()),
		    //'ultimo_corte' 					=> $faker->dateTimeBetween($startDate = '-'.$ult_corte_offset.' days', $endDate = 'now', $timezone = date_default_timezone_get()),
		    'comentarios' 					=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 					=> $c_by,
	        'updated_by' 					=> $u_by,
	    ];
    }
    
});

/** Cuarto Frio */
$factory->define(App\Models\CuartoFrio::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);
	/*
	$clientes = App\Models\Cliente::count();
	$cliente_id = rand(1, $clientes);

	$cliente = App\Models\Cliente::find($cliente_id);
	$est_arr = $cliente->establecimientos()->pluck('id')->toArray();
	*/
	$fecha_offset = rand(1,10);

    return [
	    'codigo'						=> $faker->ean8(),
	    //'cliente_id'					=> $cliente->id,
	    //'establecimiento_id'			=> $est_arr[rand(0, count($est_arr)-1)],
	    //'ultimo_corte' 					=> $faker->dateTimeBetween($startDate = '-'.$fecha_offset.' days', $endDate = 'now', $timezone = date_default_timezone_get()),
        'created_by' 					=> $c_by,
        'updated_by' 					=> $u_by,
    ];
});

/** Inventario Entradas */
$factory->define(App\Models\InventarioEntradas::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    $productos = App\Models\Producto::all()->pluck('id')->toArray();

    return [
	    'producto_id'			=> $productos[rand(0, count($productos)-1)],
	    'cantidad_entrada' 		=> $faker->numberBetween($min = 1, $max = 200),
	    'motivo' 				=> $faker->randomElement(['produccion', 'devolucion']),
        'created_by' 			=> $c_by,
        'updated_by'			=> $u_by,
    ];
});

/** Inventario Salidas */
$factory->define(App\Models\InventarioSalidas::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    $productos = App\Models\Producto::all()->pluck('id')->toArray();

    return [
	    'producto_id'			=> $productos[rand(0, count($productos)-1)],
	    'cantidad_salida' 		=> $faker->numberBetween($min = 1, $max = 200),
	    'motivo' 				=> $faker->randomElement(['pedido', 'merma']),
        'created_by' 			=> $c_by,
        'updated_by'			=> $u_by,
    ];
});

/** Corte */
$factory->define(App\Models\Corte::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	/*NICE TO HAVE*/
	#SELECT SOLO ESTABLECIMIENTOS QUE TIENEN CONSERVADORES ASIGNADOS O CUARTOFRIO

	$establecimientos = App\Models\ClienteEstablecimiento::all()->pluck('id')->toArray();
	$establecimiento_rand = App\Models\ClienteEstablecimiento::find($faker->randomElement($establecimientos));

	//dd($establecimiento_rand->capacidad_maxima());

	//$capacidad_rand = $faker->numberBetween($min = 0, $max = $establecimiento_rand->capacidad_maxima());

	//$productoid_rand = $faker->randomElement($establecimiento_rand->productos_disponibles());

	$productos_array = App\Models\Producto::where('es_hielo', '=', '1')->pluck('id')->toArray();
	$productoid_rand = $productos_array[rand(0, count($productos_array)-1)];

	return [
	    'establecimiento_id'	=> $establecimiento_rand->id,
	    'producto_id'			=> $productoid_rand,
	    //'cantidad_actual' 	=> $capacidad_rand,
	    'cantidad_actual'		=> $faker->numberBetween($min = 10, $max = 300),
	    'comentarios' 			=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
        'created_by' 			=> $c_by,
        'updated_by'			=> $u_by,
    ];

	/*
	# Corte Cuarto Frio
	$cuartosfrios = App\Models\CuartoFrio::all()->pluck('id')->toArray();
	$cuartofrio_rand = App\Models\CuartoFrio::find($faker->randomElement($cuartosfrios));
	$productos_array = $cuartofrio_rand->productos()->pluck('id')->toArray();
	$productoid_rand = $faker->randomElement($productos_array);

	$producto_rand = DB::table('establecimiento_cuartofrio_productos')->where([
	    ['producto_id', '=', $productoid_rand],
	    ['cuartofrio_id', '=', $cuartofrio_rand->id],
	])->get()
    ->pluck('capacidad_actual')
    ->toArray();

    if (count($producto_rand) > 0) {
    	$capacidad_rand = $faker->numberBetween($min = 0, $max = $producto_rand[0]);

    	return [
		    'cuartofrio_id'			=> $cuartofrio_rand->id,
		    'producto_id'			=> $productoid_rand,
		    'cantidad_actual' 		=> $capacidad_rand,
		    'comentarios' 			=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 			=> $c_by,
	        'updated_by'			=> $u_by,
	    ];

    } else {
    	#Corte a Conservador
    	# Corte Conservador
    	$conservadores = App\Models\Conservador::all()->pluck('id')->toArray();
    	$conservador_rand = App\Models\Conservador::find($faker->randomElement($conservadores));
    	$capacidad_rand = $faker->numberBetween($min = 0, $max = $conservador_rand->capacidad_actual);

    	return [
		    'conservador_id'		=> $conservador_rand->id,
		    'producto_id'			=> $conservador_rand->producto_id,
		    'cantidad_actual' 		=> $capacidad_rand,
		    'comentarios' 			=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 			=> $c_by,
	        'updated_by'			=> $u_by,
	    ];
    }
	*/
    
});

/** Pedido */
$factory->define(App\Models\Pedido::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

	$clientes = App\Models\Cliente::count();
	$cliente_id = rand(1, $clientes);

	$cliente = App\Models\Cliente::find($cliente_id);
	if ($cliente->tipo == 'horeca') {
		$est_arr = $cliente->establecimientos()->pluck('id')->toArray();
		$establecimiento_id = $est_arr[rand(0, count($est_arr)-1)];
	} else{
		$establecimiento_id = null;
	} 

	$formas_pago = App\Models\FormaPago::count();
	$forma_pago_id = rand(1, $formas_pago);

	$condiciones_pago = App\Models\CondicionPago::count();
	$condicion_pago_id = rand(1, $condiciones_pago);

	$choferes = DB::table('users')
            ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
            ->where('user_has_role.user_role_id', '=', 4)
            ->get()
            ->pluck('id')
            ->toArray();

    $fecha_entrega_offset = rand(1,150);
    $fecha_entrega_offset2 = rand(200,300);
	$status_rand = $faker->randomElement(['1','2','3','4','5']);

	if ($status_rand == '3' || $status_rand == '4') {
		# Generar Firma de recibido y nombre quien firma

		$firma_path = str_replace('public/', '', $faker->image('public/uploads/firmas', $faker->randomElement(array('200', '200')), $faker->randomElement(array('200', '200')), 'abstract'));

		//Gerente Establecimiento / Almacenista
		$contactos = $cliente->contactos()->pluck('id')->toArray();

		if ( rand(0, 1) ) {
			# Recibe pedido un contacto registrado en el sistema
			if (count($contactos) > 0 ) 
				$firmado_por = $contactos[rand(0, count($contactos)-1)];
			else 
				$firmado_por = null;
			$firmado_otro = null;
		}else {
			# Recibe el pedido otro contacto no registrada
			$firmado_por = null;
			$firmado_otro = $faker->name;
		}

		$fecha_entrega_fake = $faker->dateTimeBetween( $startDate = '-'.$fecha_entrega_offset.' days', $endDate = $fecha_entrega_offset2.' days', $timezone = date_default_timezone_get());

		$fecha_entregado = $fecha_entrega_fake;


	} else {
		$firma_path = null;
		$firmado_por = null;
		$firmado_otro = null;

		$fecha_entrega_fake = $faker->dateTimeBetween( $startDate = '-'.$fecha_entrega_offset.' days', $endDate = $fecha_entrega_offset2.' days', $timezone = date_default_timezone_get());

		$fecha_entregado = null;
	}

	$rand_subtotal = $faker->randomFloat(2, 0, rand(0, rand(100, rand(100, 5000))));
	if ( rand(0, 1) == 1 ) $rand_iva = $rand_subtotal*0.16; else $rand_iva = 0.00;
	$rand_total = $rand_subtotal+$rand_iva;

	$es_evento = rand(0, 1);

	if ( $es_evento || $establecimiento_id == null) {
		# Evento
		return [
		    'cliente_id'					=> $cliente->id,
		    'cliente_establecimiento_id'	=> $establecimiento_id,
		    'fecha_entrega' 				=> $fecha_entrega_fake,
		    'entregado_at' 					=> $fecha_entregado,
		    'subtotal'						=> $rand_subtotal,
		    'iva'							=> $rand_iva,
		    'total'							=> $rand_total,
	    	'incobrable' 					=> $faker->randomElement([0, 1]),
		    'forma_pago_id'					=> $forma_pago_id,
		    'condicion_pago_id'				=> $condicion_pago_id,
		    'chofer_id'						=> $choferes[rand(0, count($choferes)-1)],
		    'status' 						=> $status_rand,
		    'firma_path' 					=> $firma_path,
		    'firmado_por' 					=> $firmado_por,
		    'firmado_otro' 					=> $firmado_otro,
		    'es_evento'						=> rand(0, 1),
		    'evento_calle' 					=> $faker->streetName,
		    'evento_numero_exterior' 		=> $faker->buildingNumber,
		    'evento_numero_interior' 		=> rand(1,10),
		    'evento_colonia' 				=> $faker->lastName,
		    'evento_codigo_postal' 			=> $faker->randomNumber(6),
		    'evento_municipio' 				=> $faker->city,
		    'evento_estado' 				=> $faker->state,
		    'comentarios' 					=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 					=> $c_by,
	        'updated_by' 					=> $u_by,
	    ];
	} else {
		# Establecimiento
		return [
		    'cliente_id'					=> $cliente->id,
		    'cliente_establecimiento_id'	=> $establecimiento_id,
		    'fecha_entrega' 				=> $fecha_entrega_fake,
		    'entregado_at' 					=> $fecha_entregado,
		    'subtotal'						=> $rand_subtotal,
		    'iva'							=> $rand_iva,
		    'total'							=> $rand_total,
	    	'incobrable' 					=> $faker->randomElement([0, 1]),
		    'forma_pago_id'					=> $forma_pago_id,
		    'condicion_pago_id'				=> $condicion_pago_id,
		    'chofer_id'						=> $choferes[rand(0, count($choferes)-1)],
		    'status' 						=> $status_rand,
		    'firma_path' 					=> $firma_path,
		    'firmado_por' 					=> $firmado_por,
		    'firmado_otro' 					=> $firmado_otro,
		    'es_evento'						=> $es_evento,
		    'comentarios' 					=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
	        'created_by' 					=> $c_by,
	        'updated_by' 					=> $u_by,
	    ];
	}

});

/** Pagos (Cobranza) */
$factory->define(App\Models\Pago::class, function (Faker\Generator $faker) {
	
	$num_usuarios = App\User::count();
	$c_by = rand(1, $num_usuarios);
	$u_by = rand(1, $num_usuarios);

    $pedidos = App\Models\Pedido::all()->pluck('id')->toArray();

	$formas_pago = App\Models\FormaPago::count();
	$forma_pago_id = rand(1, $formas_pago);

    $fecha_offset = rand(1,10);
    return [
	    'pedido_id'				=> $pedidos[rand(0, count($pedidos)-1)],
	    'fecha_pago' 			=> $faker->dateTimeBetween($startDate = '-'.$fecha_offset.' days', $endDate = 'now', $timezone = date_default_timezone_get()),
	    'monto' 				=> $faker->randomFloat(2, 0, rand(0, rand(100, rand(1000, 50000)))),
	    'forma_pago_id' 		=> $forma_pago_id,
	    'facturado' 			=> $faker->randomElement([0, 1]),
	    'status' 				=> $faker->randomElement(['pago_parcial','liquidado','cancelado']),
	    'comentarios' 			=> $faker->randomElement(array('', $faker->realText(rand(50, 250)))),
        'created_by' 			=> $c_by,
        'updated_by'			=> $u_by,
    ];
});

/** Mantenimiento */
$factory->define(App\Models\Mantenimiento::class, function (Faker\Generator $faker) {
	
	$choferes = DB::table('users')
        ->leftJoin('user_has_role', 'users.id', '=', 'user_has_role.user_id')
        ->where('user_has_role.user_role_id', '=', 4)
        ->get()
        ->pluck('id')
        ->toArray();

	$c_by = $choferes[rand(0, count($choferes)-1)];
	$u_by = $choferes[rand(0, count($choferes)-1)];

    $conservadores = App\Models\Conservador::where('status','=','en_consignacion')->pluck('id')->toArray();
	$conservador_rand = App\Models\Conservador::find($faker->randomElement($conservadores));
	return [
	    'conservador_id'		=> $conservador_rand->id,
	    'establecimiento_id'	=> $conservador_rand->cliente_establecimiento_id,
	    'comentarios' 			=> $faker->randomElement(array('', $faker->realText(rand(50, 50)))),
        'created_by' 			=> $c_by,
        'updated_by'			=> $u_by,
    ];
});
